/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.digit.userdata.repositories;

import eu.europa.ec.digit.userdata.entities.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.stream.Stream;

public interface RoleRepository extends Repository<Role, String> {

	@Query(value = "SELECT DISTINCT(ROLE_NAME) FROM LEOS3_ROLE ORDER BY ROLE_NAME", nativeQuery = true)
    Stream<String> findAllRoles();
	
	Role findByRole(String role);
	
	
	@Modifying
    @Query(value = "INSERT INTO LEOS3_ROLE (ROLE_NAME, ROLE_DESC) VALUES (?1,?2)"    		
    		,nativeQuery = true)
    void newRole(String role_name, String role_desc);

    @Modifying
    @Query(value = "UPDATE LEOS3_ROLE SET ROLE_DESC=?2 WHERE LEOS3_ROLE.ROLE_NAME = ?1"    		
    		,nativeQuery = true)
    void editRole(String role_name, String role_desc);

    @Modifying
    @Query(value = "DELETE FROM LEOS3_ROLE WHERE LEOS3_ROLE.ROLE_NAME = ?1"    		
    		,nativeQuery = true)
    void deleteRole(String role_name);
    
    @Query(value = "SELECT LEOS3_ROLE.ROLE_NAME FROM LEOS3_USER \r\n"
    		+ "INNER JOIN LEOS3_USER_ROLE ON (LEOS3_USER.USER_LOGIN = LEOS3_USER_ROLE.USER_LOGIN) \r\n"
    		+ "INNER JOIN LEOS3_ROLE ON (LEOS3_USER_ROLE.ROLE_NAME = LEOS3_ROLE.ROLE_NAME)\r\n"
    		+ "WHERE LEOS3_USER.USER_PER_ID=?1"
    		,nativeQuery = true)
    Stream<String> getUserRoles(int userId);
  
}
