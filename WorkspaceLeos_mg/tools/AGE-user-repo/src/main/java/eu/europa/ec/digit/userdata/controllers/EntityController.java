/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.digit.userdata.controllers;

import eu.europa.ec.digit.userdata.entities.Entity;
import eu.europa.ec.digit.userdata.entities.User;
import eu.europa.ec.digit.userdata.entities.Role;
import eu.europa.ec.digit.userdata.repositories.EntityRepository;
import eu.europa.ec.digit.userdata.repositories.UserRepository;
import eu.europa.ec.digit.userdata.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
public class EntityController {

    private static int MAX_RECORDS = 100;

    @Autowired
    UserRepository userRepository;

    @Autowired
    EntityRepository entityRepository;
    
    @Autowired
    RoleRepository roleRepository;
    
    @RequestMapping(method = RequestMethod.GET, path = "/entities")
    @Transactional(readOnly = true)
    public Collection<String> getAllOrganizations() {
        return entityRepository.findAllOrganizations()
                .collect(Collectors.toList());
    }
    
    @RequestMapping(method = RequestMethod.GET, path = "/entities/{orgId}")
    @Transactional(readOnly = true)
    public Entity getEntity(
            @PathVariable(value = "orgId", required = true) String orgId) {
        return entityRepository.findById(orgId);
    }
    
    @RequestMapping(method = RequestMethod.GET, path = "/entities/{orgId}/users")
    @Transactional(readOnly = true)
    public Collection<User> searchUsersByOrganizationAndKey(
            @PathVariable(value = "orgId", required = false) String orgId,
    		@RequestParam(value = "searchKey", required = true) String searchKey){
        return userRepository
                .findUsersByOrganization(
                        searchKey.trim().replace(" ", "%").concat("%"),
                        orgId)
                .limit(MAX_RECORDS).collect(Collectors.toList());
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/entity/create")
    @Transactional(readOnly = false)
    public String newEntity(    		
    		@RequestParam(value = "entity_name", required = true) String entity_name
    		,@RequestParam(value = "entity_parent_id", required = true) int entity_parent_id
    		,@RequestParam(value = "entity_org_name", required = true) String entity_org_name
    	) {
    	entityRepository.newEntity(    			
    			entity_name.trim(),
    			entity_parent_id,
    			entity_org_name.trim()
    	);
    	return "{success: true}";
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/entity/edit")
    @Transactional(readOnly = false)
    public String editEntity(
    		@RequestParam(value = "entity_id", required = true) int entity_id
    		,@RequestParam(value = "entity_name", required = true) String entity_name
    		,@RequestParam(value = "entity_parent_id", required = true) int entity_parent_id
    		,@RequestParam(value = "entity_org_name", required = true) String entity_org_name
    	) {
    	entityRepository.editEntity(
    			entity_id,
    			entity_name.trim(),
    			entity_parent_id,
    			entity_org_name.trim()
    	);
    	return "{success: true}";
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/entity/delete")
    @Transactional(readOnly = false)
    public String deleteEntity(
    		@RequestParam(value = "entity_id", required = true) int entity_id    		
    	) {
    	entityRepository.deleteEntity(
    		entity_id   			
    	);
    	return "{success: true}";
    }
}
