/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.digit.userdata.repositories;

import eu.europa.ec.digit.userdata.entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.Repository;

import java.util.stream.Stream;

public interface UserRepository extends Repository<User, Long> {

    User findByLogin(String login);

    // FIXME: shift functions to DB later
    @Query(value = "SELECT * FROM LEOS3_USER " + " WHERE "
            + " dbo.deAccent(USER_LASTNAME + ' ' + USER_FIRSTNAME) LIKE dbo.deAccent(?1) "
            + " OR "
            + " dbo.deAccent(USER_FIRSTNAME + ' ' + USER_LASTNAME) LIKE dbo.deAccent(?1) "
            + " ORDER BY USER_LASTNAME, USER_FIRSTNAME ", nativeQuery = true)
    Stream<User> findUsersByKey(String key);

    // FIXME: shift functions to DB later
    @Query(value = "SELECT DISTINCT LEOS3_USER.* FROM LEOS3_USER INNER JOIN LEOS3_USER_ENTITY ON LEOS3_USER.USER_LOGIN = LEOS3_USER_ENTITY.USER_LOGIN "
            + " INNER JOIN LEOS3_ENTITY ON LEOS3_USER_ENTITY.ENTITY_ID = LEOS3_ENTITY.ENTITY_ID "
            + " WHERE "
            + " (dbo.deAccent(USER_LASTNAME + ' ' + USER_FIRSTNAME) LIKE dbo.deAccent(?1) "
            + " OR "
            + " dbo.deAccent(USER_FIRSTNAME + ' ' + USER_LASTNAME) LIKE dbo.deAccent(?1)) "
            + " AND "
            + " dbo.deAccent(LEOS3_ENTITY.ENTITY_ORG_NAME) LIKE dbo.deAccent(?2) "
            + " ORDER BY USER_LASTNAME, USER_FIRSTNAME ", nativeQuery = true)
    Stream<User> findUsersByKeyAndOrganization(String key, String organization);
    
    @Query(value = "SELECT DISTINCT LEOS3_USER.* FROM LEOS3_USER INNER JOIN LEOS3_USER_ENTITY ON LEOS3_USER.USER_LOGIN = LEOS3_USER_ENTITY.USER_LOGIN "
            + " INNER JOIN LEOS3_ENTITY ON LEOS3_USER_ENTITY.ENTITY_ID = LEOS3_ENTITY.ENTITY_ID "
            + " WHERE "
            + " (dbo.deAccent(USER_LASTNAME + ' ' + USER_FIRSTNAME) LIKE dbo.deAccent(?1) "
            + " OR "
            + " dbo.deAccent(USER_FIRSTNAME + ' ' + USER_LASTNAME) LIKE dbo.deAccent(?1)) "
            + " AND "
            + " LEOS3_ENTITY.ENTITY_ID = ?2 "
            + " ORDER BY USER_LASTNAME, USER_FIRSTNAME ", nativeQuery = true)
    Stream<User> findUsersByOrganization(String searchKey, String orgId);
    
    @Query(value = "SELECT DISTINCT LEOS3_USER.* FROM LEOS3_USER INNER JOIN LEOS3_USER_ROLE ON LEOS3_USER.USER_LOGIN = LEOS3_USER_ROLE.USER_LOGIN "
            + " INNER JOIN LEOS3_ROLE ON LEOS3_USER_ROLE.ROLE_NAME = LEOS3_ROLE.ROLE_NAME "
            + " WHERE "
            + " dbo.deAccent(LEOS3_ROLE.ROLE_NAME) = dbo.deAccent(?1) "
            + " ORDER BY USER_LASTNAME, USER_FIRSTNAME ", nativeQuery = true)
    Stream<User> findUsersByRole(String roleId);
    
    @Modifying
    @Query(value = "INSERT INTO LEOS3_USER (USER_LOGIN, USER_LASTNAME, USER_FIRSTNAME, USER_EMAIL) VALUES (?1,?2,?3,?4)"    		
    		,nativeQuery = true)
    void newUser(String user_login, String user_lastname, String user_firstname, String user_email);

    @Modifying
    @Query(value = "UPDATE LEOS3_USER SET USER_LOGIN=?2, USER_LASTNAME=?3, USER_FIRSTNAME=?4, USER_EMAIL=?5 WHERE LEOS3_USER.USER_PER_ID = ?1"    		
    		,nativeQuery = true)
    void editUser(int userId, String user_login, String user_lastname, String user_firstname, String user_email);

    @Modifying
    @Query(value = "DELETE FROM LEOS3_USER WHERE LEOS3_USER.USER_PER_ID = ?1)"    		
    		,nativeQuery = true)
    void deleteUser(int userId);
    
    @Modifying
    @Query(value = "INSERT INTO LEOS3_USER_ENTITY (USER_LOGIN, ENTITY_ID) VALUES (?1,?2)"    		
    		,nativeQuery = true)
    void addOrg(String user_login, int entity_id);
    
    @Modifying
    @Query(value = "DELETE FROM LEOS3_USER_ENTITY WHERE USER_LOGIN=?1 AND ENTITY_ID=?2"    		
    		,nativeQuery = true)
    void removeOrg(String user_login, int entity_id);
    
    @Modifying
    @Query(value = "INSERT INTO LEOS3_USER_ROLE (USER_LOGIN, ROLE_NAME) VALUES (?1,?2)"    		
    		,nativeQuery = true)
    void addRole(String user_login, String role_name);
    
    @Modifying
    @Query(value = "DELETE FROM LEOS3_USER_ROLE WHERE USER_LOGIN=?1 AND ROLE_NAME=?2"    		
    		,nativeQuery = true)
    void removeRole(String user_login, String role_name);

    @Query(value = "SELECT resultado = CASE WHEN COUNT(*)>0 THEN 'true' ELSE 'false' END FROM LEOS3_USER_ROLE WHERE LEOS3_USER_ROLE.USER_LOGIN=?1 AND LEOS3_USER_ROLE.ROLE_NAME=?2"    		
    		,nativeQuery = true)
    boolean hasRole(String user_login, String role_name);
    
    @Query(value = "SELECT resultado = CASE WHEN COUNT(*)>0 THEN 'true' ELSE 'false' END FROM LEOS3_USER_ENTITY WHERE LEOS3_USER_ENTITY.USER_LOGIN=?1 AND LEOS3_USER_ENTITY.ENTITY_ID=?2"    		
    		,nativeQuery = true)
    boolean hasOrg(String user_login, int entity_id);
    
    // FIXME: shift functions to DB later

}