/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.digit.userdata.controllers;

import eu.europa.ec.digit.userdata.entities.Entity;
import eu.europa.ec.digit.userdata.entities.User;
import eu.europa.ec.digit.userdata.entities.Role;
import eu.europa.ec.digit.userdata.repositories.EntityRepository;
import eu.europa.ec.digit.userdata.repositories.UserRepository;
import eu.europa.ec.digit.userdata.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
public class RoleController {

    private static int MAX_RECORDS = 100;

    @Autowired
    UserRepository userRepository;

    @Autowired
    EntityRepository entityRepository;
    
    @Autowired
    RoleRepository roleRepository;
    
    // CRUD de roles
    @RequestMapping(method = RequestMethod.POST, path = "/roles/create")
    @Transactional(readOnly = false)
    public String newRole(
    		@RequestParam(value = "role_name", required = true) String role_name
    		,@RequestParam(value = "role_desc", required = true) String role_desc
    	) {
    	Role r = roleRepository.findByRole(role_name);
    	if (r == null) {
	    	roleRepository.newRole(
	    			role_name.trim(),
	    			role_desc.trim()
	    	);
	    	return "{success: true}";
    	}
    	else {
    		return "{success: false, error:'Ya existe el rol'}";
    	}
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/roles/edit")
    @Transactional(readOnly = false)
    public String editRole(
    		@RequestParam(value = "role_name", required = true) String role_name
    		,@RequestParam(value = "role_desc", required = true) String role_desc
    	) {
    	Role r = roleRepository.findByRole(role_name);
    	if (r != null) {
	    	roleRepository.editRole(
	    			role_name.trim(),
	    			role_desc.trim()
	    	);
	    	return "{success: true}";
    	}
    	else {
    		return "{success: false, error:'No existe el rol'}";
    	}
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/roles/delete")
    @Transactional(readOnly = false)
    public String deleteRole(
    		@RequestParam(value = "role_name", required = true) String role_name
    	) {
    	Role r = roleRepository.findByRole(role_name);
    	if (r != null) {
	    	roleRepository.deleteRole(
	    			role_name.trim()    			
	    	);
	    	return "{success: true}";
    	}
    	else {
    		return "{success: false, error:'No existe el rol'}";
    	}
    }
    
    @RequestMapping(method = RequestMethod.GET, path = "/roles")
    @Transactional(readOnly = true)
    public Collection<String> getAllRoles() {
        return roleRepository.findAllRoles()
                .collect(Collectors.toList());
    }
    
    @RequestMapping(method = RequestMethod.GET, path = "/roles/{roleId}")
    @Transactional(readOnly = true)
    public Role getRole(
            @PathVariable(value = "roleId", required = true) String roleId) {
        return roleRepository.findByRole(roleId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/roles/{roleId}/users")
    @Transactional(readOnly = true)
    public Collection<User> searchUsersByOrganizationAndKey(
            @PathVariable(value = "roleId", required = false) String roleId
            ) {
        return userRepository
                .findUsersByRole(
                        roleId)
                .limit(MAX_RECORDS).collect(Collectors.toList());
    }
}
