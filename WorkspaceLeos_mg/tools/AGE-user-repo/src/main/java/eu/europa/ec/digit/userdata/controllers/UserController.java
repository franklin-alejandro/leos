/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.digit.userdata.controllers;

import eu.europa.ec.digit.userdata.entities.Entity;
import eu.europa.ec.digit.userdata.entities.User;
import eu.europa.ec.digit.userdata.entities.Role;
import eu.europa.ec.digit.userdata.repositories.EntityRepository;
import eu.europa.ec.digit.userdata.repositories.UserRepository;
import eu.europa.ec.digit.userdata.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.GrantedAuthority;

@RestController
public class UserController {

    private static int MAX_RECORDS = 100;

    @Autowired
    UserRepository userRepository;

    @Autowired
    EntityRepository entityRepository;
    
    @Autowired
    RoleRepository roleRepository;    
    
    @Value("${security.jwt.token.secret-key}")
    public String secretKey;
    
    @Value("${security.leos.admin.password}")
    private String adminKey;
    
    private String getJWTToken(String username) {
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ADMIN");
		
		String token = Jwts
				.builder()
				.setId("LEOS_JWT")
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512,
						secretKey.getBytes()).compact();

		return "Bearer " + token;
	}
    
    @RequestMapping(method = RequestMethod.POST, path = "/login")
    @Transactional(readOnly = true)
    public String login(
            @RequestParam(value = "username", required = true) String username,
            @RequestParam(value = "password", required = true) String password
    ) {
    	if (username.compareTo("admin") == 0 && 
    			password.compareTo(adminKey) == 0) {
	    	List<Role> roles = new ArrayList<Role>();
	    	roles.add(new Role("jwt admin","Administrador JWT"));
	        //return this.createToken(username, roles);
	    	return this.getJWTToken(username);
    	}
    	else {
    		return "{error:'Par usuario/clave incorrecto'}";
    	}
    }
    
    @RequestMapping(method = RequestMethod.GET, path = "/prueba")
    @Transactional(readOnly = true)
    public String prueba() {
    	System.out.println("Acción prueba");
    	return "Prueba!";
    }
    
    @RequestMapping(method = RequestMethod.GET, path = "/users")
    @Transactional(readOnly = true)
    public Collection<User> searchUsers(
            @RequestParam(value = "searchKey", required = true) String searchKey) {
        return userRepository
                .findUsersByKey(searchKey.trim().replace(" ", "%").concat("%"))
                .limit(MAX_RECORDS).collect(Collectors.toList());
    }

    @RequestMapping(method = RequestMethod.GET, path = "/users/{userId}")
    @Transactional(readOnly = true)
    public User getUser(
            @PathVariable(value = "userId", required = true) String userId) {
        return userRepository.findByLogin(userId);
    }   
    
       

    //@RequestMapping(method = RequestMethod.GET, path = "/entities/{userId}")
    @RequestMapping(method = RequestMethod.GET, path = "/users/getEntities/{userId}")
    @Transactional(readOnly = true)
    public Collection<Entity> getAllFullPathEntitiesForUser(
            @PathVariable(value = "userId", required = true) String userId) {
        return entityRepository
                .findAllFullPathEntities(getUser(userId).getEntities().stream()
                        .map(e -> e.getId()).collect(Collectors.toList()))
                .collect(Collectors.toList());
    }
    
    @RequestMapping(method = RequestMethod.GET, path = "/users/getRoles/{userId}")
    @Transactional(readOnly = true)
    public Collection<String> getUserRoles(
            @PathVariable(value = "userId", required = true) int userId) {    	
        return roleRepository
                .getUserRoles(userId)
                .collect(Collectors.toList());
    }    
    
    // CRUD de usuarios
    @RequestMapping(method = RequestMethod.POST, path = "/users/create")
    @Transactional(readOnly = false)
    public String newUser(
    		@RequestParam(value = "user_login", required = true) String user_login
    		,@RequestParam(value = "user_lastname", required = true) String user_lastname
    		,@RequestParam(value = "user_firstname", required = true) String user_firstname
    		,@RequestParam(value = "user_email", required = true) String user_email
    	) {
    	User u = userRepository.findByLogin(user_login);
    	if (u == null) {
	    	userRepository.newUser(
	    			user_login.trim(),
	    			user_lastname.trim(),
	    			user_firstname.trim(),
	    			user_email.trim()
	    	);
	    	return "{success: true}";
    	}
    	else {
    		return "{success: false, error:'Ya existe un usuario con ese login'}";
    	}
    	
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/users/edit")
    @Transactional(readOnly = false)
    public String editUser(
    		@RequestParam(value = "userId", required = true) int userId
    		,@RequestParam(value = "user_login", required = true) String user_login
    		,@RequestParam(value = "user_lastname", required = true) String user_lastname
    		,@RequestParam(value = "user_firstname", required = true) String user_firstname
    		,@RequestParam(value = "user_email", required = true) String user_email
    	) {
		User u = userRepository.findByLogin(user_login);
		if (u.getLogin() == user_login && u.getPerId() == userId) {
	    	userRepository.editUser(
	    			userId,
	    			user_login.trim(),
	    			user_lastname.trim(),
	    			user_firstname.trim(),
	    			user_email.trim()
	    	);
	    	return "{success: true}";    	
		}
		else {
			return "{success: false, error:'No se puede modificar el usuario'}";
		}
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/users/delete")
    @Transactional(readOnly = false)
    public String deleteUser(
    		@RequestParam(value = "userId", required = true) int userId
    	) {
    	userRepository.deleteUser(
    			userId
    	);
    	return "{success: true}";
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/users/addOrg")
    @Transactional(readOnly = true)
    public String userAddOrg(
    		@RequestParam(value = "user_login", required = true) String user_login
            ,@RequestParam(value = "entity_id", required = true) int entity_id
        ) {
    	
    	// Comprobamos que no tenga ya ese grupo
    	boolean has_group = userRepository.hasOrg(user_login, entity_id);
    	// Si ya lo tiene, no haremos nada
    	if (has_group) {
    		return "{success: false, error:'El usuario ya está en el grupo'}";
    	}
    	// Lo añadimos
    	userRepository
        	.addOrg(user_login, entity_id);
        has_group = userRepository.hasOrg(user_login, entity_id);
        // A ver si se ha añadido bien o no
        if (!has_group) {
    		return "{success: false, error:'No se pudo añadir el usuario al grupo'}";
    	}
        else {
        	return "{success: true}";
        }
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/users/removeOrg")
    @Transactional(readOnly = true)
    public String userRemoveOrg(
    		@RequestParam(value = "user_login", required = true) String user_login
            ,@RequestParam(value = "entity_id", required = true) int entity_id
        ) {
        // Comprobamos que si tiene el grupo
    	boolean has_org = userRepository.hasOrg(user_login, entity_id);
    	// Si no lo tiene, no haremos nada
    	if (!has_org) {
    		return "{success: false, error:'El usuario no está en el grupo'}";
    	}
        userRepository
        	.removeOrg(user_login, entity_id);
        has_org = userRepository.hasOrg(user_login, entity_id);
        if (has_org) {
        	return "{success: false, error:'No se pudo quitar el usuario del grupo'}";
        }
        else {
        	return "{success: true}";
        }
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/users/addRole")
    @Transactional(readOnly = true)
    public String userAddRole(
    		@RequestParam(value = "user_login", required = true) String user_login
            ,@RequestParam(value = "role_name", required = true) String role_name
        ) {
    	// Comprobamos que no tenga ya ese rol
    	boolean has_role = userRepository.hasRole(user_login, role_name);
    	// Si ya lo tiene, no haremos nada
    	if (has_role) {
    		return "{success: false, error:'El usuario ya tiene el rol'}";
    	}
    	// Lo añadimos
        userRepository
                .addRole(user_login, role_name);
        has_role = userRepository.hasRole(user_login, role_name);
        // A ver si se ha añadido bien o no
        if (!has_role) {
    		return "{success: false, error:'No se pudo añadir el rol'}";
    	}
        else {
        	return "{success: true}";
        }
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/users/removeRole")
    @Transactional(readOnly = true)
    public String userRemoveRole(
    		@RequestParam(value = "user_login", required = true) String user_login
            ,@RequestParam(value = "role_name", required = true) String role_name
        ) {
    	// Comprobamos que si tiene el rol
    	boolean has_role = userRepository.hasRole(user_login, role_name);
    	// Si no lo tiene, no haremos nada
    	if (!has_role) {
    		return "{success: false, error:'El usuario no tiene el rol'}";
    	}
        userRepository
                .removeRole(user_login, role_name);
        has_role = userRepository.hasRole(user_login, role_name);
        if (has_role) {
        	return "{success: false, error:'No se pudo quitar el rol'}";
        }
        else {
        	return "{success: true}";
        }
    }
    
    private boolean user_has_permission() {
    	//boolean has_role = userRepository.hasRole(user_login, role_name);
    	return true;
    }
        
}
