$filePath = $args[0]
    $wd = New-Object -ComObject Word.Application
$wd.Visible = $true
$txt = $wd.Documents.Open(
    $filePath,
    $false,
    $false,
    $false)

$wd.Documents[1].SaveAs($args[1])
$wd.Documents[1].Close()
$wd.Quit()
