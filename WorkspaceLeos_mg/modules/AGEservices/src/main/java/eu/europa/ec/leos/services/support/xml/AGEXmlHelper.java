/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.services.support.xml;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.ImmutableMap;

import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.services.support.IdGenerator;
import eu.europa.ec.leos.services.support.LeosUtil;
import eu.europa.ec.leos.vo.toc.OptionsType;
import eu.europa.ec.leos.vo.toc.TocItem;

public class AGEXmlHelper extends XmlHelper {
	    
    public static final String NUMBER_TYPE = "numbertype";
    public static final String REPORT = "report";
    
    public static final String MAINBODY = "mainBody";
    public static final String BOOK = "book";
    public static final String TITLE = "title";
    public static final String CHAPTER = "chapter";
    public static final String SECTION = "section";
    public static final String SUBSECTION = "subsection";
    public static final String SIGNATURE = "signature";
    public static final String PROVISO = "proviso";   
    public static final String DIVISION = "division"; 
    public static final String QUOTEDSTRUCTURE = "quotedStructure";
    public static final String THIS = "this";   
    //Evolutivo #2546
    public static final String ROLE = "role";
    public static final String PERSON = "person";
    public static final String RULE = "rule";
    public static final String MOD = "mod";
    public static final String ANALYSIS = "analysis";
    public static final String ACTIVEMODIFICATIONS = "activeModifications";
    public static final String REFERENCES = "references";
    public static final String DOCPURPOSE = "docPurpose";
    public static final String DOCTYPE = "docType";
    
    public static final String LEOS_ORIGIN_ATTR_CN = "cn";
    public static final String LEOS_ORIGIN_ATTR_EC = "ec";
    
    public static final List<String> ELEMENTS_TO_BE_PROCESSED_FOR_NUMBERING = Arrays.asList(ARTICLE, PARAGRAPH, POINT, SUBPOINT, CHAPTER, SECTION, SUBSECTION); 
    
    
    private static final String ID_PLACEHOLDER = "${id}";
    private static final String ID_PLACEHOLDER_ESCAPED = "\\Q${id}\\E";
    private static final String NUM_PLACEHOLDER = "${num}";
    private static final String NUM_PLACEHOLDER_ESCAPED = "\\Q${num}\\E";
    private static final String HEADING_PLACEHOLDER = "${heading}";
    private static final String HEADING_PLACEHOLDER_ESCAPED = "\\Q${heading}\\E";
    private static final String CONTENT_TEXT_PLACEHOLDER = "${default.content.text}";
    private static final String CONTENT_TEXT_PLACEHOLDER_ESCAPED = "\\Q${default.content.text}\\E";

    public static String getCitationTemplate(MessageHelper messageHelper) {
        String id = IdGenerator.generateId("cit_",7);
        StringBuilder template = new StringBuilder();
        template.append("            <citation refersTo=\"~legalBasis\" xml:id=\"").append(id).append("\" leos:editable=\"true\">");
        template.append("               <p>" + messageHelper.getMessage("template.paragraph.quote") + "</p>");
        template.append("            </citation>");
        return template.toString();
    }
    
    public static String getSignatureTemplate(MessageHelper messageHelper) {
        String id = IdGenerator.generateId("sign_",7);
        StringBuilder template = new StringBuilder();
        template.append("            <signature xml:id=\"").append(id).append("\" leos:editable=\"true\"").append(" leos:deletable=\"true\">");
        template.append("               <role xml:id=\"").append(id).append("_role\" refersTo=\"\">").append(messageHelper.getMessage("template.paragraph.signature.role")).append("</role>");        
        template.append("               <person xml:id=\"").append(id).append("_person\" refersTo=\"\">").append(messageHelper.getMessage("template.paragraph.signature.person")).append("</person>");
        template.append("            </signature>");
        return template.toString();
    }
    
    public static String getFormulaTemplate(MessageHelper messageHelper) {
        String id = IdGenerator.generateId("form_",7);
        StringBuilder template = new StringBuilder();
        template.append("            <formula xml:id=\"").append(id).append("\" name=\"formula_").append(id).append("\" leos:editable=\"true\"").append(" leos:deletable=\"true\">");
        template.append("               <p xml:id=\"").append(id).append("_p\">").append(messageHelper.getMessage("template.paragraph.formula")).append("</p>");
        template.append("            </formula>");
        return template.toString();
    }    
    public static String getRecitalTemplate(String num,MessageHelper messageHelper) {
        String id = IdGenerator.generateId("rec_",7);
        StringBuilder template = new StringBuilder();
        template.append("            <recital xml:id=\"").append(id).append("\" leos:editable=\"true\">");
        template.append("              <num leos:editable=\"false\">").append((num != null) ? num : "").append("</num>");
        template.append("              <p>" + messageHelper.getMessage("template.paragraph.considering") + "</p>");
        template.append("            </recital>");
        return template.toString();
    }
    
    public static String getArticleTemplate(String num, String heading, String leosTemplate, MessageHelper messageHelper, String modifyTemplate) {
        String id = IdGenerator.generateId("akn_art",7);
		String numeration = "1";
        StringBuilder template = new StringBuilder();
        boolean isModify = false;
        int number = 1;
        

        if (modifyTemplate!= null && modifyTemplate.contains(leosTemplate)) {
        	numeration = "Uno";
        	isModify = true;
        }
        //Evolutivo #2378
        if (isModify) {
        	template.append("            <article xml:id=\"").append(id).append("\" leos:editable=\"true\" leos:mod=\"true\" leos:deletable=\"true\">");
        } else {
        	template.append("            <article xml:id=\"").append(id).append("\" leos:editable=\"true\" leos:mod=\"false\" leos:deletable=\"true\">");
        }
        template.append("              <num leos:editable=\"false\">").append((num != null) ? num : "").append("</num>");
        template.append("              <heading>").append((heading != null) ? heading : "").append("</heading>");
        if (isModify) {
            template.append("              <paragraph xml:id=\"").append(id).append("-par" + number + "\">");
            template.append("              <subparagraph xml:id=\"").append(id).append("-subpar" + number + "\">");
            template.append("                <content>");
            template.append("                  <p>" + messageHelper.getMessage("template.paragraph.frametext") + "</p>");
            template.append("                </content>");
            template.append("              </subparagraph>");
            template.append("  			   	<rule xml:id=\"").append(id).append("-rule" + "\">");
            template.append("                	<content>");
            template.append("                  		<p>" + messageHelper.getMessage("template.paragraph.regulatory") + "</p>");
            template.append("                	</content>");
            template.append("              	</rule>");
            number++;
        } else {
        	 template.append("              <paragraph xml:id=\"").append(id).append("-par" + number + "\">");
             template.append("                <num>").append(numeration).append(".</num>");
             template.append("                <content>");
             template.append("                  <p>" + messageHelper.getMessage("template.paragraph.text") + "</p>");
             template.append("                </content>");
        }
        template.append("              </paragraph>");
        template.append("            </article>");
		
        return template.toString();
    }
    
    public static String getArticleAmendmentTemplateType1(String numArticle, String heading, String idArticle, String frameText, String quotedStructure, String quotedStructureId) {
    	String id = IdGenerator.generateId("akn_art_amend",9);
    	StringBuilder template = new StringBuilder();
    	template.append("\n\t\t\t\t<article xml:id=\"").append(idArticle).append("\" leos:editable=\"false\" leos:mod=\"false\" leos:deletable=\"false\" refersTo=\"~_ART_AMEND\">");
  //  	template.append("\n\t\t\t\t<article xml:id=\"").append(idArticle).append("\" leos:editable=\"false\" leos:mod=\"false\" leos:deletable=\"false\">");
    	template.append("\n\t\t\t\t\t<num leos:editable=\"false\">").append((numArticle != null) ? numArticle : "").append("</num>");
    	template.append("\n\t\t\t\t\t<heading>").append((heading != null) ? heading : "").append("</heading>");
    	template.append("\n\t\t\t\t\t<paragraph xml:id=\"").append(id).append("-par1\">");
    	template.append("\n\t\t\t\t\t\t<content>");
    	template.append("\n\t\t\t\t\t\t\t<p class=\"BlockAmendment\">");
    	template.append("\n\t\t\t\t\t\t\t\t<mod xml:id=\"").append(id).append("-mod1\">").append(frameText);
    	if (!quotedStructure.equals("")) {
        	template.append("\n\t\t\t\t\t\t\t\t\t<quotedStructure xml:id=\"").append(quotedStructureId).append("\"  startQuote=\"«\" endQuote=\"»\"  leos:editable=\"true\"  leos:deletable=\"true\">");
        	template.append("\n\t\t\t\t\t\t\t\t\t\t"+ deleteLastWhiteSpace(quotedStructure));
        	template.append("\n\t\t\t\t\t\t\t\t\t</quotedStructure>");
//        	template.append("\n\t\t\t\t\t\t\t\t\t<inline name=\"punctuationMod\">.</inline>");
    	}
    	template.append("\n\t\t\t\t\t\t\t\t</mod>");
    	template.append("\n\t\t\t\t\t\t\t</p>");
    	template.append("\n\t\t\t\t\t\t</content>");
    	template.append("\n\t\t\t\t\t</paragraph>");
    	template.append("\n\t\t\t\t</article>");
    	return template.toString();
    }

//    public static String getArticleAmendmentTemplateType2(String numArticle, String heading, String idArticle, String frameText, String generalFrameText, String quotedStructure, String quotedStructureId) {
//    	String id = IdGenerator.generateId("akn_art_amend",9);
//    	StringBuilder template = new StringBuilder();
//    	template.append("\n\t\t\t\t<article xml:id=\"").append(idArticle).append("\" leos:editable=\"true\" leos:mod=\"true\" leos:deletable=\"true\" refersTo=\"~_ART_AMEND\">");
//    	template.append("\n\t\t\t\t\t<num leos:editable=\"false\">").append("Article ").append((numArticle != null) ? numArticle : "").append("</num>");
//    	template.append("\n\t\t\t\t\t<heading>").append((heading != null) ? heading : "").append("</heading>");
//    	template.append("\n\t\t\t\t\t<paragraph xml:id=\"").append(id).append("-par1\">");
//    	template.append("\n\t\t\t\t\t\t<content>");
//    	template.append("\n\t\t\t\t\t\t\t<p>"+ generalFrameText);
//    	template.append("\n\t\t\t\t\t\t\t</p>");
//    	template.append("\n\t\t\t\t\t\t</content>");
//    	template.append("\n\t\t\t\t\t</paragraph>");
//    	template.append("\n\t\t\t\t\t<paragraph xml:id=\"").append(id).append("-par2\">");
//    	template.append("\n\t\t\t\t\t\t<num> ... </num>");
//    	template.append("\n\t\t\t\t\t\t<content>");
//    	template.append("\n\t\t\t\t\t\t\t<p class=\"BlockAmendment\">");
//    	template.append("\n\t\t\t\t\t\t\t\t<mod xml:id=\"").append(id).append("-mod1\">").append(frameText);
//    	if (!quotedStructure.equals("")) {
//        	template.append("\n\t\t\t\t\t\t\t\t\t<quotedStructure xml:id=\"").append(quotedStructureId).append("\"  startQuote=\"«\" endQuote=\"»\"  leos:editable=\"true\"  leos:deletable=\"true\">");
//        	template.append("\n\t\t\t\t\t\t\t\t\t\t"+ quotedStructure);
//        	template.append("\n\t\t\t\t\t\t\t\t\t</quotedStructure>");
//        	template.append("\n\t\t\t\t\t\t\t\t\t<inline name=\"punctuationMod\">.</inline>");
//    	}
//    	template.append("\n\t\t\t\t\t\t\t\t</mod>");
//    	template.append("\n\t\t\t\t\t\t\t</p>");
//    	template.append("\n\t\t\t\t\t\t</content>");
//    	template.append("\n\t\t\t\t\t</paragraph>");
//    	/// METER AQUÍ CUANDO SON VARIOS ELEMENTOS   	
//    	
//    	template.append("\n\t\t\t\t</article>");
//    	return template.toString();
//    }
    
    public static String getDispositionAmendmentTemplateType1(String numArticle, String heading, String idArticle, String frameText, String quotedStructure, String quotedStructureId) {
    	String id = IdGenerator.generateId("akn_art_amend",9);
    	StringBuilder template = new StringBuilder();
    	template.append("\n\t\t\t\t<proviso xml:id=\"").append(idArticle).append("\" leos:editable=\"false\" leos:mod=\"false\" leos:deletable=\"true\" refersTo=\"~_ART_AMEND\">");
    	template.append("\n\t\t\t\t\t<num leos:editable=\"false\">").append((numArticle != null) ? numArticle : "").append("</num>");
    	template.append("\n\t\t\t\t\t<heading>").append((heading != null) ? heading : "").append("</heading>");
    	template.append("\n\t\t\t\t\t<paragraph xml:id=\"").append(id).append("-par1\">");
    	template.append("\n\t\t\t\t\t\t<content>");
    	template.append("\n\t\t\t\t\t\t\t<p class=\"BlockAmendment\">");
    	template.append("\n\t\t\t\t\t\t\t\t<mod xml:id=\"").append(id).append("-mod1\">").append(frameText);
    	if (!quotedStructure.equals("")) {
        	template.append("\n\t\t\t\t\t\t\t\t\t<quotedStructure xml:id=\"").append(quotedStructureId).append("\"  startQuote=\"«\" endQuote=\"»\"  leos:editable=\"true\"  leos:deletable=\"true\">");
        	template.append("\n\t\t\t\t\t\t\t\t\t\t"+ deleteLastWhiteSpace2(quotedStructure));
        	template.append("\n\t\t\t\t\t\t\t\t\t</quotedStructure>");
        	template.append("\n\t\t\t\t\t\t\t\t\t<inline name=\"punctuationMod\">.</inline>");
    	}
    	template.append("\n\t\t\t\t\t\t\t\t</mod>");
    	template.append("\n\t\t\t\t\t\t\t</p>");
    	template.append("\n\t\t\t\t\t\t</content>");
    	template.append("\n\t\t\t\t\t</paragraph>");
    	template.append("\n\t\t\t\t</proviso>");
    	return template.toString();
    }

    public static String getDispotisionAmendmentTemplateType2(String numArticle, String heading, String idArticle, String frameText, String generalFrameText, String quotedStructure, String quotedStructureId) {
    	String id = IdGenerator.generateId("akn_art_amend",9);
    	StringBuilder template = new StringBuilder();
    	template.append("\n<proviso xml:id=\"").append(idArticle).append("\" leos:editable=\"true\" leos:mod=\"true\" leos:deletable=\"true\" refersTo=\"~_ART_AMEND\">");
    	template.append("\n\t\t\t\t\t<num leos:editable=\"false\">").append("proviso ").append((numArticle != null) ? numArticle : "").append("</num>");
    	template.append("\n\t\t\t\t\t<heading>").append((heading != null) ? heading : "").append("</heading>");
    	template.append("\n\t\t\t\t\t<paragraph xml:id=\"").append(id).append("-par1\">");
    	template.append("\n\t\t\t\t\t\t<content>");
    	template.append("\n\t\t\t\t\t\t\t<p>"+ generalFrameText);
    	template.append("\n\t\t\t\t\t\t\t</p>");
    	template.append("\n\t\t\t\t\t\t</content>");
    	template.append("\n\t\t\t\t\t</paragraph>");
    	template.append("\n\t\t\t\t\t<paragraph xml:id=\"").append(id).append("-par2\">");
    	template.append("\n\t\t\t\t\t\t<num> ... </num>");
    	template.append("\n\t\t\t\t\t\t<content>");
    	template.append("\n\t\t\t\t\t\t\t<p class=\"BlockAmendment\">");
    	template.append("\n\t\t\t\t\t\t\t\t<mod xml:id=\"").append(id).append("-mod1\">").append(frameText);
    	if (!quotedStructure.equals("")) {
        	template.append("\n\t\t\t\t\t\t\t\t\t<quotedStructure xml:id=\"").append(quotedStructureId).append("\"  startQuote=\"«\" endQuote=\"»\"  leos:editable=\"true\"  leos:deletable=\"true\">");
        	template.append("\n\t\t\t\t\t\t\t\t\t\t"+ quotedStructure);
        	template.append("\n\t\t\t\t\t\t\t\t\t</quotedStructure>");
        	template.append("\n\t\t\t\t\t\t\t\t\t<inline name=\"punctuationMod\">.</inline>");
    	}
    	template.append("\n\t\t\t\t\t\t\t\t</mod>");
    	template.append("\n\t\t\t\t\t\t\t</p>");
    	template.append("\n\t\t\t\t\t\t</content>");
    	template.append("\n\t\t\t\t\t</paragraph>");
    	/// METER AQUÍ CUANDO SON VARIOS ELEMENTOS   	
    	
    	template.append("\n</proviso>");
    	return template.toString();
    }
    
    public static String getParagraphAmendmentTemplate (String frameText, String quotedStructure, String quotedStructureId, String idModNew) {
    	String id = IdGenerator.generateId("akn_art_amend",9);
    	StringBuilder template = new StringBuilder();
    	template.append("\n\t\t\t\t\t<paragraph xml:id=\"").append(id).append("-par1\">");
    	template.append("\n\t\t\t\t\t\t<num>1. </num>");
    	template.append("\n\t\t\t\t\t\t<content>");
    	template.append("\n\t\t\t\t\t\t\t<p class=\"BlockAmendment\">");
    	template.append("\n\t\t\t\t\t\t\t\t<mod xml:id=\"").append(idModNew).append("\">").append(frameText);
    	template.append("\n\t\t\t\t\t\t\t\t\t<quotedStructure  xml:id=\"").append(quotedStructureId).append("\" startQuote=\"«\" endQuote=\"»\" leos:editable=\"true\"  leos:deletable=\"true\">");
    	template.append("\n\t\t\t\t\t\t\t\t\t\t"+ quotedStructure);
    	template.append("\n\t\t\t\t\t\t\t\t\t</quotedStructure>");
    	template.append("\n\t\t\t\t\t\t\t\t\t<inline name=\"punctuationMod\">.</inline>");
    	template.append("\n\t\t\t\t\t\t\t\t</mod>");
    	template.append("\n\t\t\t\t\t\t\t</p>");
    	template.append("\n\t\t\t\t\t\t</content>");
    	template.append("\n\t\t\t\t\t</paragraph>");
    	return template.toString(); 
    }
    
    public static String getParagraphTemplate2 (String generalFrameText) {
    	String id = IdGenerator.generateId("akn_art_amend",9);
    	StringBuilder template = new StringBuilder();
    	template.append("\n\t\t\t\t\t<paragraph xml:id=\"").append(id).append("-par1\">");
    	template.append("\n\t\t\t\t\t\t<content>");
    	template.append("\n\t\t\t\t\t\t\t<p>"+ generalFrameText);
    	template.append("\n\t\t\t\t\t\t\t</p>");
    	template.append("\n\t\t\t\t\t\t</content>");
    	template.append("\n\t\t\t\t\t</paragraph>");
    	return template.toString(); 
    }
    
    public static String getQuotedStructureAmendmentTemplate (String quotedStructure, String quotedStructureId) {
    	StringBuilder template = new StringBuilder();
    	template.append("\n\t\t\t\t\t\t\t\t\t<quotedStructure  xml:id=\"").append(quotedStructureId).append("\" startQuote=\"«\" endQuote=\"»\" leos:editable=\"true\"  leos:deletable=\"true\">");
    	template.append("\n\t\t\t\t\t\t\t\t\t\t"+ quotedStructure);
    	template.append("\n\t\t\t\t\t\t\t\t\t</quotedStructure>");
    	template.append("\n\t\t\t\t\t\t\t\t\t<inline name=\"punctuationMod\">.</inline>");
    	return template.toString(); 
    }

    public static String getMetaReferenceAmendmentTemplateType1 (String nameNorm, String eliDestination) {
    	StringBuilder template = new StringBuilder();
    	template.append("\n\t\t\t\t<activeRef href=\"").append(eliDestination).append("\"  showAs=\"").append(nameNorm).append("\"/>"); 	
    	return template.toString(); 	
    }
    
    public static String getMetaAnalysisAmendmentTemplateType1 (String eliDestination, String actionType, String idMod, String idQuotedS, String pos, String wid) {
    	StringBuilder template = new StringBuilder();
    	String [] type = actionType.split("-");
    	template.append("\n\t\t\t<analysis source=\"~AGE\">");
    	template.append("\n\t\t\t\t<activeModifications>");
    	if (type[0].equals("insertion")) {
    		template.append("\n\t\t\t\t\t<textualMod type=\"").append(type[0]).append("\">");
    	} else {
    		template.append("\n\t\t\t\t\t<textualMod type=\"").append(actionType).append("\">");
    	}
    	
    	template.append("\n\t\t\t\t\t\t<source href=\"").append(idMod).append("\"/>");
    	
    	if (actionType.contains("insertion")) {
    		template.append("\n\t\t\t\t\t\t<destination pos=\"").append(pos).append("\" leos:newNum=\"").append(wid).append("\" href=\"").append(eliDestination).append("\"/>");
    	}else {
    		template.append("\n\t\t\t\t\t\t<destination").append(" leos:newNum=\"").append(wid).append("\" href=\"").append(eliDestination).append("\"/>");
    	}
    	
    	template.append("\n\t\t\t\t\t\t<new href=\"").append(idQuotedS).append("\"/>");
    	template.append("\n\t\t\t\t\t</textualMod>");
    	template.append("\n\t\t\t\t</activeModifications>");
    	template.append("\n\t\t\t</analysis>");
    	return template.toString(); 	
    }
    
    public static String getMetaAnalysisAmendmentTemplateType2 (String eliDestination, String actionType, String idMod, String idQuotedS, String pos, String wid) {
    	StringBuilder template = new StringBuilder();
    	String [] type = actionType.split("-");
    	if (actionType.contains("insertion")) {
    		template.append("\n\t\t\t\t\t<textualMod type=\"").append(type[0]).append("\">");
    	} else {
    		template.append("\n\t\t\t\t\t<textualMod type=\"").append(actionType).append("\">");
    	}
    	template.append("\n\t\t\t\t\t\t<source href=\"").append(idMod).append("\"/>");
    	if (actionType.contains("insertion")) {
    		template.append("\n\t\t\t\t\t\t<destination pos=\"").append(pos).append("\" leos:newNum=\"").append(wid).append("\" href=\"").append(eliDestination).append("\"/>");
    	}else {
    		template.append("\n\t\t\t\t\t\t<destination").append(" leos:newNum=\"").append(wid).append("\" href=\"").append(eliDestination).append("\"/>");
    	}
    	template.append("\n\t\t\t\t\t\t<new href=\"").append(idQuotedS).append("\"/>");
    	template.append("\n\t\t\t\t\t</textualMod>");
    	return template.toString(); 	
    }

    
    public static String getDispositionTemplate(String numType, String num, String heading, String leosTemplate, MessageHelper messageHelper) {
        String id = IdGenerator.generateId("akn_dis",9);
        String numeration = "1";
        
        StringBuilder template = new StringBuilder();
        template.append("            <proviso xml:id=\"").append(id).append("\" leos:editable=\"true\" leos:mod=\"false\" leos:deletable=\"true\">");
        template.append("              <num leos:editable=\"false\">").append((numType != null) ? numType : "").append(" ").append((num != null) ? num : "").append("</num>");
        template.append("              <heading>").append((heading != null) ? heading : "").append("</heading>");
        template.append("              <paragraph xml:id=\"").append(id).append("-par1\">");
        template.append("                <num>").append(numeration).append(".</num>");
        template.append("                <content>");
        template.append("                  <p>" + messageHelper.getMessage("template.paragraph.text") + "</p>");
        template.append("                </content>");
        template.append("              </paragraph>");
        template.append("            </proviso>");
        return template.toString();    
    }
        
    public static String getParagraphTemplate(String num, MessageHelper messageHelper) {
        String id = IdGenerator.generateId("akn_art_para",9);
        StringBuilder template = new StringBuilder();
        template.append("              <paragraph xml:id=\"").append(id).append("\">");
        if(num != null) {
            template.append("                <num leos:editable=\"false\">").append(num).append("</num>");
        }
        template.append("                <content>");
        template.append("                  <p>"+messageHelper.getMessage("template.paragraph.text") +"</p>");
        template.append("                </content>");
        template.append("              </paragraph>");
        return template.toString();
    }

    public static String getSubParagraphTemplate(String content, MessageHelper messageHelper) {
        String id = IdGenerator.generateId("akn_art_subpara",9);
        StringBuilder template = new StringBuilder();
        template.append("              <subparagraph xml:id=\"").append(id).append("\">");
        
        if (content != null) {
        	if (content.indexOf("<content") != -1) {
        		template.append(content, content.indexOf("<content"), content.indexOf("</content>") + "</content>".length());
        	} else {
                template.append("                <content>");
                template.append("                  <p></p>");
                template.append("                </content>");
        	}
        } else {
            template.append("                <content>");
            template.append("                  <p>"+messageHelper.getMessage("template.paragraph.text") +"</p>");
            template.append("                </content>");
        }
        
        template.append("              </subparagraph>");
        return template.toString();
    }

    public static String getSubpointTemplate(String content, MessageHelper messageHelper) {
        String id = IdGenerator.generateId("akn_art_alinea",7);
        StringBuilder template = new StringBuilder();
        template.append("              <alinea xml:id=\"").append(id).append("\">");

        if (content != null) {
        	if (content.indexOf("<content") != -1) {
        		template.append(content, content.indexOf("<content"), content.indexOf("</content>") + "</content>".length());
        	} else {
                template.append("                <content>");
                template.append("                  <p></p>");
                template.append("                </content>");
        	}
        } else {
            template.append("                <content>");
            template.append("                  <p>"+messageHelper.getMessage("template.paragraph.text") +"</p>");
            template.append("                </content>");
        }
        
        template.append("              </alinea>");
        return template.toString();
    }

    public static String getPointTemplate(String num, MessageHelper messageHelper) {
        String id = IdGenerator.generateId("akn_art_point",7);
        StringBuilder template = new StringBuilder();
        template.append("              <point xml:id=\"").append(id).append("\">");
        template.append("                <num leos:editable=\"false\">").append(num).append("</num>");
        template.append("                <content>");
        template.append("                  <p>" +messageHelper.getMessage("template.paragraph.text") +"</p>");
        template.append("                </content>");
        template.append("              </point>");
        return template.toString();
    }
    
    public static String getAnnexTemplate(MessageHelper messageHelper) {
        String id = IdGenerator.generateId("akn_annex",9);
        StringBuilder template = new StringBuilder();
        template.append("         <division xml:id=\"").append(id).append("\" leos:editable=\"true\"  leos:deletable=\"true\">");
        template.append("                <content>");
        template.append("                  <p xml:id=\"").append(id).append("_par1\">");
        template.append("                     "+ messageHelper.getMessage("template.paragraph.text"));
        template.append("                  </p>");
        template.append("                </content>");
        template.append("         </division>");
        return template.toString();
    }
    
    public static String getReportTemplate(MessageHelper messageHelper) {
        String id = IdGenerator.generateId("akn_report",7);
        StringBuilder template = new StringBuilder();
        template.append("         <division xml:id=\"").append(id).append("\" leos:editable=\"true\"  leos:deletable=\"true\">");
        template.append("                <content>");
        template.append("                  <p xml:id=\"").append(id).append("_par1\">");
        template.append("                     "+ messageHelper.getMessage("template.paragraph.text"));
        template.append("                  </p>");
        template.append("                </content>");
        template.append("         </division>");
        return template.toString();
    }
    
    public static String getCantoDoradoConclusions(String year, String month, String day, String organism, String sign, String presid) {
    	StringBuilder template = new StringBuilder();
    	template.append("<conclusions xml:id=\"conclusions\">");
    	template.append("\n\t\t\t<formula xml:id=\"conclusions__formula_2\" name=\"formula_cit_conclusiones_1\" leos:editable=\"true\" leos:deletable=\"false\">");
    	template.append("\n\t\t\t\t<p xml:id=\"conclusions__date\">Dado en "); 
    	template.append("\n\t\t\t\t\t<location xml:id=\"cit_conclusions_1__p_location\" refersTo=\"~ESP_MAD\">Madrid</location>").append( ", " + day + " de, " + month + " de " + year);
    	template.append("\n\t\t\t\t</p>");
    	template.append("\n\t\t\t</formula>");
    	template.append("\n\t\t\t<block name=\"signatureBlock\" xml:id=\"conclusions__block_3\">");
    	template.append("\n\t\t\t\t<signature xml:id=\"conclusions__block_1__signature_1\">");
    	template.append("\n\t\t\t\t\t<person xml:id=\"conclusions__block_1__signature_1__person\" leos:editable=\"false\" leos:deletable=\"false\" refersTo=\"~SIGN\">FELIPE R.</person>");
    	template.append("\n\t\t\t\t</signature>");
    	template.append("\n\t\t\t\t<signature xml:id=\"conclusions__block_1__signature_2\">");
    	template.append("\n\t\t\t\t\t<role xml:id=\"conclusions__block_1__signature_2__role\" leos:editable=\"true\" leos:deletable=\"true\" refersTo=\"~PRESID\">" + presid +"</role>");
    	template.append("\n\t\t\t\t\t<person xml:id=\"conclusions__block_1__signature_2__person\" leos:editable=\"true\" leos:deletable=\"true\" refersTo=\"person\">" + sign + "</person>");
    	template.append("\n\t\t\t\t</signature>");
    	template.append("\n\t\t\t</block>");
    	template.append("\n\t\t</conclusions>");
    	return template.toString();
    }
    
    public static String getCantoDoradoPrefaceOrganism(String organism) {
    	String id = IdGenerator.generateId("akn_canto",9);
    	StringBuilder template = new StringBuilder();
    	template.append("        <container name=\"docLabel\" xml:id=\"coverpage__container_2\">");
    	template.append("         <p xml:id=\"").append(id).append("\" refersTo=\"~State\">" + organism + "</p>");
    	template.append("        </container>");
    	return template.toString();
    }
    
    public static String getCantoDoradoPrefaceFormula2(String intro) {
    	StringBuilder template = new StringBuilder();
    	template.append("<p xml:id=\"preface__formula_2__p\">Se somete a la firma de Vuestra Majestad " + intro + ":</p>");
    	return template.toString();
    }

    public static String getCantoDoradoPrefaceFormula3(String formula) {
    	StringBuilder template = new StringBuilder();
    	template.append("<p xml:id=\"preface__formula_3__p\"> " + formula + " </p>");
    	return template.toString();
    }
    
    public static String getCantoDoradoAnalysis (String analysis) {
    	StringBuilder template = new StringBuilder();
    	template.append("<analysis source=\"~AGE\">" + analysis + "</analysis>");
    	return template.toString();
    }
    
    public static String getCantoDoradoReferences(String references) {
    	StringBuilder template = new StringBuilder();
    	template.append("<references source=\"~AGE\">" + references + "</references>");
    	return template.toString();
    }
    
    public static String getCantoDoradoRecitals (String recitals) {
    	StringBuilder template = new StringBuilder();
    	template.append("<recitals xml:id=\"recs\" leos:editable=\"false\">" + recitals + "</recitals>");
    	return template.toString();
    }
    
    public static String getCantoDoradoMainBody (String mainBody) {
    	StringBuilder template = new StringBuilder();
    	template.append("<mainBody xml:id=\"body\" leos:editable=\"false\">" + mainBody + "</mainBody>");
    	return template.toString();
    }
    
    public static String getTemplate(TocItem tocItem, String num, String heading, MessageHelper messageHelper) {
        return getTemplate(tocItem, ImmutableMap.of(NUM, Collections.singletonMap(NUM_PLACEHOLDER_ESCAPED, StringUtils.isNotEmpty(num) && tocItem.isNumWithType() ? StringUtils.capitalize(extractMessage(tocItem.getAknTag().value(), messageHelper)) + " " + num : num),
                HEADING, Collections.singletonMap(HEADING_PLACEHOLDER_ESCAPED, heading), CONTENT, Collections.singletonMap(CONTENT_TEXT_PLACEHOLDER_ESCAPED, getDefaultContentText(tocItem.getAknTag().value(), messageHelper))));
    }
    
    //Correctivo #978 // Evolutivo #1051
   private static String extractMessage(String message, MessageHelper messageHelper) {
		if (message.equals("book") || message.equals("chapter") || message.equals("title") || message.equals("section") || message.equals("subsection")) {
			return messageHelper.getMessage("toc.item.type." + message).toUpperCase();
		}else {
			return messageHelper.getMessage("toc.item.type." + message);
		} 	
   }
    
    private static String getDefaultContentText(String tocTagName, MessageHelper messageHelper) {
        String defaultTextContent = messageHelper.getMessage("toc.item.template." + tocTagName + ".content.text");
        if (defaultTextContent.equals("toc.item.template." + tocTagName + ".content.text")) {
            defaultTextContent = messageHelper.getMessage("toc.item.template.default.content.text");
        }
        return defaultTextContent;
    }

    private static String getTemplate(TocItem tocItem, Map<String, Map<String, String>> templateItems) {
        StringBuilder template = tocItem.getTemplate() != null ? new StringBuilder(tocItem.getTemplate()) : getDefaultTemplate(tocItem);
        replaceAll(template, ID_PLACEHOLDER_ESCAPED, IdGenerator.generateId("akn_" + tocItem.getAknTag().value(), 7));

        replaceTemplateItems(template, NUM, tocItem.getItemNumber(), templateItems.get(NUM));
        replaceTemplateItems(template, HEADING, tocItem.getItemHeading(), templateItems.get(HEADING));
        replaceTemplateItems(template, CONTENT, OptionsType.MANDATORY, templateItems.get(CONTENT));

        return template.toString();
    }
    
    private static StringBuilder getDefaultTemplate(TocItem tocItem) {
        StringBuilder defaultTemplate = new StringBuilder("<" + tocItem.getAknTag().value() + " xml:id=\"" + ID_PLACEHOLDER + "\">");
        if (OptionsType.MANDATORY.equals(tocItem.getItemNumber()) || OptionsType.OPTIONAL.equals(tocItem.getItemNumber())) {
            defaultTemplate.append(tocItem.isNumberEditable() ? "<num>" + NUM_PLACEHOLDER + "</num>" : "<num leos:editable=\"false\">" + NUM_PLACEHOLDER + "</num>");
        }
        if (OptionsType.MANDATORY.equals(tocItem.getItemHeading()) || OptionsType.OPTIONAL.equals(tocItem.getItemHeading())) {
            defaultTemplate.append("<heading>" + HEADING_PLACEHOLDER + "</heading>");
        }
        defaultTemplate.append("<content><p>" + CONTENT_TEXT_PLACEHOLDER + "</p></content></" + tocItem.getAknTag().value() + ">");
        return defaultTemplate;
    }
    
    private static void replaceTemplateItems(StringBuilder template, String itemName, OptionsType itemOption, Map<String, String> templateItem) {
        if (OptionsType.MANDATORY.equals(itemOption)) {
            templateItem.forEach((itemPlaceHolder, itemValue) -> {
                replaceAll(template, itemPlaceHolder, StringUtils.isEmpty(itemValue) ? "" : itemValue);
            });
        } else if (OptionsType.OPTIONAL.equals(itemOption)) {
            templateItem.forEach((itemPlaceHolder, itemValue) -> {
                if (StringUtils.isEmpty(itemValue)) {
                    replaceAll(template, "<" + itemName + ".*?" + itemPlaceHolder + "</" + itemName + ">", "");
                } else {
                    replaceAll(template, itemPlaceHolder, itemValue);
                }
            });
        }
    }
    
    private static void replaceAll(StringBuilder sb, String toReplace, String replacement) {
        int start = 0;
        Matcher m = Pattern.compile(toReplace).matcher(sb);
        while (m.find(start)) {
            sb.replace(m.start(), m.end(), replacement);
            start = m.start() + replacement.length();
        }
    }

    
    private static final ArrayList<String> prefixTobeUsedForChildren= new ArrayList<String>(Arrays.asList(ARTICLE, RECITALS, CITATIONS));
    public static String determinePrefixForChildren(String tagName,String idOfNode,String parentPrefix){
        return prefixTobeUsedForChildren.contains(tagName)?idOfNode: parentPrefix;  //if(root Node Name is in Article/Reictals/Citations..set the prefix)
    }
    
    private static final ArrayList<String> parentEditableNodes= new ArrayList<String>(Arrays.asList(ARTICLE, RECITALS, CITATIONS, BLOCKCONTAINER));
    public static  boolean isParentEditableNode(String tagName){
        return parentEditableNodes.contains(tagName)?true: false;
    }

    
    private static String deleteLastWhiteSpace(String text) {
    	String result = text;
    	if(StringUtils.isNotBlank(text) && text.contains("</p>")) {
    		Integer position = text.lastIndexOf("</p>");
    	
    		String deletePart = text.substring(position-1, position);
    		if(" ".equals(deletePart)) {
    			String part1 = text.substring(0,position-1);
        		String part2 = text.substring(position);
    			result = part1 + part2;
    		}
    	}

    	return result;
    }
    private static String deleteLastWhiteSpace2(String text) {
    	String result = text;
    	if(StringUtils.isNotBlank(text) && text.contains("</heading>")) {
    		Integer position = text.lastIndexOf("</heading>");
    	
    		String deletePart = text.substring(position-1, position);
    		if(" ".equals(deletePart)) {
    			String part1 = text.substring(0,position-1);
        		String part2 = text.substring(position);
    			result = part1 + part2;
    		}
    	}

    	return result;
    }
}

