package eu.europa.ec.leos.services.export;

import java.io.File;

public interface AGEExportService extends ExportService {

	File exportToPdfCantoDorado(String proposalId);
}
