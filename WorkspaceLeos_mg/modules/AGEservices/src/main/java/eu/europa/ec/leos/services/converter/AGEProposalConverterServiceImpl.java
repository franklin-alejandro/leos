package eu.europa.ec.leos.services.converter;

import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.vo.DocumentVO;
import eu.europa.ec.leos.domain.vo.MetadataVO;
import eu.europa.ec.leos.services.export.ZipPackageUtil;
import eu.europa.ec.leos.services.store.TemplateService;
import eu.europa.ec.leos.services.support.xml.XmlContentProcessor;
import eu.europa.ec.leos.services.support.xml.AGEXmlNodeConfigHelper;
import eu.europa.ec.leos.services.support.xml.XmlNodeProcessor;
import eu.europa.ec.leos.vo.catalog.CatalogItem;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class AGEProposalConverterServiceImpl extends ProposalConverterServiceForMandateImpl {

	private static final Logger LOG = LoggerFactory.getLogger(AGEProposalConverterServiceImpl.class);
	
	private final XmlNodeProcessor xmlNodeProcessor;
    private final AGEXmlNodeConfigHelper xmlNodeConfigHelper;
    protected final XmlContentProcessor xmlContentProcessor;
    private final TemplateService templateService;

    private static final String ANNEX_FILE_PREFIX = "annex_";
    private static final String REPORT_FILE_PREFIX = "report_";
    private static final String CANTODORADO_FILE_PREFIX = "cantodorado_";
    private static final String BILL_FILE_PREFIX = "bill_";
    private static final String MEMORANDUM_FILE_PREFIX = "memorandum_";
    private static final String MEDIA_FILE_PREFIX = "media_";
    private static final String PROPOSAL_FILE = "main.xml";
    private static final String XML_DOC_EXT = ".xml";
	private static final String MAIN_FILE_PREFIX = "main_";

    private List<CatalogItem> templatesCatalog;
	
	@Autowired
    AGEProposalConverterServiceImpl(
            XmlNodeProcessor xmlNodeProcessor,
            AGEXmlNodeConfigHelper xmlNodeConfigHelper,
            XmlContentProcessor xmlContentProcessor,
            TemplateService templateService) {
		super(xmlNodeProcessor, xmlNodeConfigHelper, xmlContentProcessor, templateService);
		this.xmlNodeProcessor = xmlNodeProcessor;
        this.xmlNodeConfigHelper = xmlNodeConfigHelper;
        this.xmlContentProcessor = xmlContentProcessor;
        this.templateService = templateService;
    }
	
	@Override
	public DocumentVO createProposalFromLegFile(File file, final DocumentVO proposal, boolean canModifySource) {
        proposal.clean();
        proposal.setId(PROPOSAL_FILE);
        proposal.setCategory(LeosCategory.PROPOSAL);
        // unzip file
        Map<String, Object> unzippedFiles = ZipPackageUtil.unzipFiles(file, "/unzip/");
        try {
            templatesCatalog = templateService.getTemplatesCatalog();
            if (unzippedFiles.containsKey(PROPOSAL_FILE)) {
                List<DocumentVO> propChildDocs = new ArrayList<>();
                File proposalFile = (File) unzippedFiles.get(PROPOSAL_FILE);
                updateSource(proposal, proposalFile, canModifySource);
                updateMetadataVO(proposal);
                List<DocumentVO> billChildDocs = new ArrayList<>();
                DocumentVO billDoc = null;
                for (String docName : unzippedFiles.keySet()) {
                    File docFile = (File) unzippedFiles.get(docName);
                    DocumentVO doc = createDocument(docName, docFile, canModifySource);
                    if (doc != null) {
                        if (doc.getCategory() == LeosCategory.ANNEX || doc.getCategory() == LeosCategory.MEDIA) {
                            billChildDocs.add(doc);
                        } else if (doc.getCategory() == LeosCategory.BILL) {
                            billDoc = doc;
                        } else {
                            propChildDocs.add(doc);
                        }
                    }
                }
                if (billDoc != null) {
                    billDoc.setChildDocuments(billChildDocs);
                    propChildDocs.add(billDoc);
                }
                proposal.setChildDocuments(propChildDocs);
            }
        } catch (Exception e) {
            LOG.error("Error generating the map of the document: {}", e);
        } finally {
            deleteFiles(file, unzippedFiles);
        }
        return proposal;
    }
	
	private DocumentVO createDocument(String docName, File docFile, boolean canModifySource) {
        DocumentVO doc = null;
        LeosCategory category = identifyCategory(docName);
        if (category != null) {
            doc = new DocumentVO(category);
            doc.setId(docName);
            updateSource(doc, docFile, canModifySource);
            updateMetadataVO(doc);
        }
        return doc;
    }
	
	private LeosCategory identifyCategory(String docName) {
        LeosCategory category = null;
        if (docName.endsWith(XML_DOC_EXT)) {
            if (docName.startsWith(ANNEX_FILE_PREFIX)) {
                category = LeosCategory.ANNEX;
            } else if (docName.startsWith(REPORT_FILE_PREFIX)) {
                category = LeosCategory.REPORT;
            }  else if (docName.startsWith(CANTODORADO_FILE_PREFIX)) {
                category = LeosCategory.CANTODORADO;
            }else if (docName.startsWith(BILL_FILE_PREFIX)) {
                category = LeosCategory.BILL;
            } else if (docName.startsWith(MEMORANDUM_FILE_PREFIX)) {
                category = LeosCategory.MEMORANDUM;
            } else if (docName.startsWith(MEDIA_FILE_PREFIX)) {
                category = LeosCategory.MEDIA;
            } else if (docName.startsWith(MAIN_FILE_PREFIX)) {
                category = LeosCategory.MAIN;
            }
        }
        return category;
    }
	
	private void updateMetadataVO(final DocumentVO document) {
        if (document.getSource() != null) {
            try {
                MetadataVO metadata = document.getMetadata();
                Map<String, String> metadataVOMap = xmlNodeProcessor.getValuesFromXml(document.getSource(), new String[]{
                        AGEXmlNodeConfigHelper.DOC_PURPOSE_META,
                        AGEXmlNodeConfigHelper.DOC_STAGE_META,
                        AGEXmlNodeConfigHelper.DOC_TYPE_META,
                        AGEXmlNodeConfigHelper.DOC_LANGUAGE,
                        AGEXmlNodeConfigHelper.DOC_SPECIFIC_TEMPLATE,
                        AGEXmlNodeConfigHelper.DOC_TEMPLATE,
                        AGEXmlNodeConfigHelper.ANNEX_TITLE_META,
                        AGEXmlNodeConfigHelper.ANNEX_INDEX_META,
                        AGEXmlNodeConfigHelper.ANNEX_NUMBER_META,
                        AGEXmlNodeConfigHelper.REPORT_TITLE_META,
                        AGEXmlNodeConfigHelper.REPORT_INDEX_META,
                        AGEXmlNodeConfigHelper.REPORT_NUMBER_META,
                        AGEXmlNodeConfigHelper.CANTODORADO_TITLE_META,
                        AGEXmlNodeConfigHelper.CANTODORADO_INDEX_META,
                        AGEXmlNodeConfigHelper.CANTODORADO_NUMBER_META,
						AGEXmlNodeConfigHelper.MAIN_TITLE_META,
                        AGEXmlNodeConfigHelper.MAIN_INDEX_META,
                        AGEXmlNodeConfigHelper.MAIN_NUMBER_META,
                }, xmlNodeConfigHelper.getConfig(document.getCategory()));

                metadata.setDocPurpose(metadataVOMap.get(AGEXmlNodeConfigHelper.DOC_PURPOSE_META));
                metadata.setDocStage(metadataVOMap.get(AGEXmlNodeConfigHelper.DOC_STAGE_META));
                metadata.setDocType(metadataVOMap.get(AGEXmlNodeConfigHelper.DOC_TYPE_META));
                metadata.setLanguage(metadataVOMap.get(AGEXmlNodeConfigHelper.DOC_LANGUAGE));
                metadata.setDocTemplate(metadataVOMap.get(AGEXmlNodeConfigHelper.DOC_SPECIFIC_TEMPLATE));
                metadata.setTemplate(metadataVOMap.get(AGEXmlNodeConfigHelper.DOC_TEMPLATE));
                metadata.setTitle(metadataVOMap.get(AGEXmlNodeConfigHelper.ANNEX_TITLE_META));
                metadata.setIndex(metadataVOMap.get(AGEXmlNodeConfigHelper.ANNEX_INDEX_META));
                metadata.setNumber(metadataVOMap.get(AGEXmlNodeConfigHelper.ANNEX_NUMBER_META));
                metadata.setTitle(metadataVOMap.get(AGEXmlNodeConfigHelper.REPORT_TITLE_META));
                metadata.setIndex(metadataVOMap.get(AGEXmlNodeConfigHelper.REPORT_INDEX_META));
                metadata.setNumber(metadataVOMap.get(AGEXmlNodeConfigHelper.REPORT_NUMBER_META));
                metadata.setTitle(metadataVOMap.get(AGEXmlNodeConfigHelper.CANTODORADO_TITLE_META));
                metadata.setIndex(metadataVOMap.get(AGEXmlNodeConfigHelper.CANTODORADO_INDEX_META));
                metadata.setNumber(metadataVOMap.get(AGEXmlNodeConfigHelper.CANTODORADO_NUMBER_META));
				metadata.setTitle(metadataVOMap.get(AGEXmlNodeConfigHelper.MAIN_TITLE_META));
                metadata.setIndex(metadataVOMap.get(AGEXmlNodeConfigHelper.MAIN_INDEX_META));
                metadata.setNumber(metadataVOMap.get(AGEXmlNodeConfigHelper.MAIN_NUMBER_META));

                // if the template doesnt exist in the system we don't continue, we won't import it.
                metadata.setTemplateName(templateService.getTemplateName(templatesCatalog, metadata.getDocTemplate(), metadata.getLanguage()));
            } catch (Exception e) {
                LOG.error("Error parsing metadata {}", e);
            }
        }
    }
	
	private void deleteFiles(File mainFile, Map<String, Object> unzippedFiles) {
        mainFile.delete();
        List<String> parentFolders = new ArrayList<>();
        for (String docName : unzippedFiles.keySet()) {
            File unzippedFile = (File) unzippedFiles.get(docName);
            String parent = unzippedFile.getParent();
            if (!parentFolders.contains(parent)) {
                parentFolders.add(parent);
            }
            if (!unzippedFile.delete()) {
                LOG.info("File not deleted {}", unzippedFile.getPath());
            }
        }
        try {
            // we must clean also the folder.
            for (String parent : parentFolders) {
                FileUtils.deleteDirectory(new File(parent));
            }
        } catch (IOException e) {
            LOG.error("Error deleting the folder {}", e);
        }
    }
	
}
