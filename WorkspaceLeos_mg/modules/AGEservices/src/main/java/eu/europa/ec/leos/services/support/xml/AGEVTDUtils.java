package eu.europa.ec.leos.services.support.xml;

import com.ximpleware.AutoPilot;
import com.ximpleware.ModifyException;
import com.ximpleware.NavException;
import com.ximpleware.VTDNav;
import com.ximpleware.XMLModifier;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.ROLE;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.PERSON;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.SIGNATURE;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.NUM;

public class AGEVTDUtils extends VTDUtils {

	public static final String LEOS_MOD_ATTR = "leos:mod";
	
	static String extractNumber(String numberStr) {
        if(numberStr != null && numberStr.contains(WHITESPACE)) {
        	String[] num = numberStr.split(WHITESPACE);
        	if(num.length==2)
        		return num[1];
        	else if (num.length==3)
        		return num[2];
//       		return numberStr.substring(numberStr.indexOf(WHITESPACE) + (num.length >= 3 ? num[2].length() + 1 : 1), numberStr.length());
        	else if (num.length==4)//Evolutivo #2485
        		return num[2] + " " + num[3];
        }
        return numberStr;
    }
    
    static String extractNumberForType(String numberStr) {
    	if(numberStr != null && numberStr.contains(WHITESPACE)) {
        	String[] num = numberStr.split(WHITESPACE);
    		return num[1];
        }
        return numberStr;
    }
    
    static XMLModifier setupXMLModifier(VTDNav vtdNav, String tagName, String idAttributeValue) {

        XMLModifier xmlModifier;
        try {
            xmlModifier = new XMLModifier();
            xmlModifier.bind(vtdNav);
            AutoPilot autoPilot = new AutoPilot(vtdNav);
            //Evolutivo #2546
            tagName = (tagName.equalsIgnoreCase(ROLE) || tagName.equalsIgnoreCase(PERSON))? SIGNATURE : tagName;
            autoPilot.selectElement(tagName);
            while (autoPilot.iterate()) {
                int attIndex = vtdNav.getAttrVal(XMLID);
                String elementId;
                if (attIndex != -1) {
                    elementId = vtdNav.toString(attIndex);
                    if (idAttributeValue.equals(elementId)) {
                        return xmlModifier;
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Unexpected error occoured during setup of XML Modifier", e);
        }
        return null; // Element not found
    }
    
    public static StringBuilder updateAttributeValue(StringBuilder tagStr, String leosAttr, Object attrValue) {
        if (tagStr != null && leosAttr != null) {
            int editableAttrPos = tagStr.indexOf(leosAttr);
            if (editableAttrPos != -1) {
                int editableAttrValStartPos = tagStr.indexOf("=", editableAttrPos) + 2;
                int editableAttrValEndPos = tagStr.indexOf("\"", editableAttrValStartPos);
                if (attrValue != null) {
                    tagStr.replace(editableAttrValStartPos, editableAttrValEndPos, attrValue.toString());
                } else {
                    tagStr.replace(editableAttrPos, editableAttrValEndPos+1, EMPTY_STRING);
                }
            } else {
                int position = tagStr.indexOf(">");
                if (position >= 0) {
                    tagStr.insert(position, insertAttribute(leosAttr, attrValue));
                }
            }
        }
        return tagStr;
    }
	
}
