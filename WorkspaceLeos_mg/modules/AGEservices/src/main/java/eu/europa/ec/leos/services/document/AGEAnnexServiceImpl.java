/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.services.document;

import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.Annex;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.annex.AnnexStructureType;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.repository.document.AnnexRepository;
import eu.europa.ec.leos.services.document.util.DocumentVOProvider;
import eu.europa.ec.leos.services.support.xml.AGENumberProcessor;
import eu.europa.ec.leos.services.support.xml.NumberProcessor;
import eu.europa.ec.leos.services.support.xml.XmlContentProcessor;
import eu.europa.ec.leos.services.support.xml.XmlNodeConfigHelper;
import eu.europa.ec.leos.services.support.xml.XmlNodeProcessor;
import eu.europa.ec.leos.services.support.xml.XmlTableOfContentHelper;
import eu.europa.ec.leos.services.validation.ValidationService;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import static eu.europa.ec.leos.services.support.xml.XmlNodeConfigHelper.createValueMap;

import java.nio.charset.StandardCharsets;
import java.util.List;

@Service
@Primary
public class AGEAnnexServiceImpl extends AnnexServiceImpl {

    private static final Logger LOG = LoggerFactory.getLogger(AGEAnnexServiceImpl.class);

    AGENumberProcessor ageNumberingProcessor;
    
    @Autowired
    AGEAnnexServiceImpl(AnnexRepository annexRepository, XmlNodeProcessor xmlNodeProcessor,
                     XmlContentProcessor xmlContentProcessor, NumberProcessor numberingProcessor, AGENumberProcessor ageNumberingProcessor, XmlNodeConfigHelper xmlNodeConfigHelper,
                     ValidationService validationService, DocumentVOProvider documentVOProvider, XmlTableOfContentHelper xmlTableOfContentHelper,
                     MessageHelper messageHelper) {
        super(annexRepository, xmlNodeProcessor, xmlContentProcessor, numberingProcessor, xmlNodeConfigHelper, validationService, documentVOProvider, xmlTableOfContentHelper, messageHelper);
        this.ageNumberingProcessor = ageNumberingProcessor;
    }

    @Override
    public Annex saveTableOfContent(Annex annex, List<TableOfContentItemVO> tocList, AnnexStructureType structureType, String actionMsg, User user) {
        Validate.notNull(annex, "Annex is required");
        Validate.notNull(tocList, "Table of content list is required");
        byte[] newXmlContent;
        String newXmlContentString = null;
        
        newXmlContent = xmlContentProcessor.createDocumentContentWithNewTocList(tocList, getContent(annex), user);
        switch(structureType) {
            case ARTICLE:
                newXmlContent = this.ageNumberingProcessor.renumberArticles(newXmlContent);
                newXmlContent = this.ageNumberingProcessor.renumberProvisos(newXmlContent);
                break;
            case LEVEL:
                newXmlContent = ageNumberingProcessor.renumberLevel(newXmlContent);
                break;
        }
        
        newXmlContent = this.ageNumberingProcessor.renumberOthers(newXmlContent);
        newXmlContent = xmlContentProcessor.doXMLPostProcessing(newXmlContent);
        
        newXmlContentString = new String (newXmlContent, StandardCharsets.UTF_8);
        
        return updateAnnex(annex, newXmlContentString.getBytes(StandardCharsets.UTF_8), VersionType.MINOR, actionMsg);
    }

}
