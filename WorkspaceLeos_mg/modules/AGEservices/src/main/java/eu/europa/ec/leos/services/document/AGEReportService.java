/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.services.document;


import java.util.List;

import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.AGEReport;
import eu.europa.ec.leos.domain.cmis.document.Annex;
import eu.europa.ec.leos.domain.cmis.metadata.AGEReportMetadata;
import eu.europa.ec.leos.domain.common.TocMode;
import eu.europa.ec.leos.model.action.VersionVO;
import eu.europa.ec.leos.model.report.AGEReportStructureType;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;

public interface AGEReportService {

	AGEReport createReport(String templateId, String path, AGEReportMetadata metadata, String actionMessage, byte[] content);

	AGEReport createReportFromContent(String path, AGEReportMetadata metadata, String actionMessage, byte[] content);

    void deleteReport(AGEReport report);

    AGEReport updateReport(AGEReport report, AGEReportMetadata metadata, VersionType version, String comment);

    AGEReport updateReport(AGEReport report, byte[] updatedReportContent, boolean major, String comment);

    AGEReport updateReport(String reportId, AGEReportMetadata metadata);

    AGEReport updateReportWithMilestoneComments(AGEReport report, List<String> milestoneComments, VersionType versionType, String comment);

    AGEReport updateReportWithMilestoneComments(String reportId, List<String> milestoneComments);

    AGEReport findReport(String id);
    
    List<VersionVO> getAllVersions(String id, String documentId);
    
    AGEReport findReportByRef(String ref);

    AGEReport findReportVersion(String id);

    List<AGEReport> findVersions(String id);
    
   // List<TableOfContentItemVO> getTableOfContent(AGEReport document);

    AGEReport findReportByPackagePath(String path);

	List<TableOfContentItemVO> getTableOfContent(AGEReport report, TocMode mode);

	List<AGEReport> findAllMajors(String docRef, int startIndex, int maxResults);

	int findAllMinorsCountForIntermediate(String docRef, String currIntVersion);

	List<AGEReport> findAllMinorsForIntermediate(String docRef, String currIntVersion, int startIndex, int maxResults);

	Integer findAllMajorsCount(String docRef);

	Integer findRecentMinorVersionsCount(String documentId, String documentRef);

	List<AGEReport> findRecentMinorVersions(String documentId, String documentRef, int startIndex, int maxResults);

	AGEReport updateReport(AGEReport report, byte[] updatedReportContent, VersionType versionType, String comment);

	AGEReport createVersion(String id, VersionType versionType, String comment);

	AGEReport saveTableOfContent(AGEReport report, List<TableOfContentItemVO> tocList,
			AGEReportStructureType structureType, String actionMsg, User user);

	List<String> getAncestorsIdsForElementId(AGEReport report, List<String> elementIds);

	AGEReport updateReportWithMetadata(AGEReport report, byte[] updatedReportContent, AGEReportMetadata metadata,
			VersionType versionType, String comment);
}
