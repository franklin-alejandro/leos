/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.services.document;


import java.util.List;

import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.AGEMain;
import eu.europa.ec.leos.domain.cmis.document.Annex;
import eu.europa.ec.leos.domain.cmis.metadata.AGEMainMetadata;
import eu.europa.ec.leos.domain.common.TocMode;
import eu.europa.ec.leos.model.action.VersionVO;
import eu.europa.ec.leos.model.main.AGEMainStructureType;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;

public interface AGEMainService {

	AGEMain createMain(String templateId, String path, AGEMainMetadata metadata, String actionMessage, byte[] content);

	AGEMain createMainFromContent(String path, AGEMainMetadata metadata, String actionMessage, byte[] content);

    void deleteMain(AGEMain report);

    AGEMain updateMain(AGEMain report, AGEMainMetadata metadata, VersionType version, String comment);

    AGEMain updateMain(AGEMain report, byte[] updatedMainContent, boolean major, String comment);

    AGEMain updateMain(String reportId, AGEMainMetadata metadata);

    AGEMain updateMainWithMilestoneComments(AGEMain report, List<String> milestoneComments, VersionType versionType, String comment);

    AGEMain updateMainWithMilestoneComments(String reportId, List<String> milestoneComments);

    AGEMain findMain(String id);
    
    List<VersionVO> getAllVersions(String id, String documentId);
    
    AGEMain findMainByRef(String ref);

    AGEMain findMainVersion(String id);

    List<AGEMain> findVersions(String id);
    
   // List<TableOfContentItemVO> getTableOfContent(AGEMain document);

    AGEMain findMainByPackagePath(String path);

	List<TableOfContentItemVO> getTableOfContent(AGEMain report, TocMode mode);

	List<AGEMain> findAllMajors(String docRef, int startIndex, int maxResults);

	int findAllMinorsCountForIntermediate(String docRef, String currIntVersion);

	List<AGEMain> findAllMinorsForIntermediate(String docRef, String currIntVersion, int startIndex, int maxResults);

	Integer findAllMajorsCount(String docRef);

	Integer findRecentMinorVersionsCount(String documentId, String documentRef);

	List<AGEMain> findRecentMinorVersions(String documentId, String documentRef, int startIndex, int maxResults);

	AGEMain updateMain(AGEMain report, byte[] updatedMainContent, VersionType versionType, String comment);

	AGEMain createVersion(String id, VersionType versionType, String comment);

	AGEMain saveTableOfContent(AGEMain report, List<TableOfContentItemVO> tocList,
			AGEMainStructureType structureType, String actionMsg, User user);

	List<String> getAncestorsIdsForElementId(AGEMain report, List<String> elementIds);

	AGEMain updateMainWithMetadata(AGEMain report, byte[] updatedMainContent, AGEMainMetadata metadata,
			VersionType versionType, String comment);
}
