package eu.europa.ec.leos.services.converter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.common.InstanceType;
import eu.europa.ec.leos.domain.vo.DocumentVO;
import eu.europa.ec.leos.instance.Instance;
import eu.europa.ec.leos.services.store.TemplateService;
import eu.europa.ec.leos.services.support.xml.AGEXmlNodeConfigHelper;
import eu.europa.ec.leos.services.support.xml.XmlContentProcessor;
import eu.europa.ec.leos.services.support.xml.XmlNodeProcessor;

@Primary
@Service
@Instance(InstanceType.COUNCIL)
public class AGEProposalConverterServiceForMandateImpl extends AGEProposalConverterServiceImpl {
	
	private static final Logger LOG = LoggerFactory.getLogger(AGEProposalConverterServiceForMandateImpl.class);

	@Autowired
	AGEProposalConverterServiceForMandateImpl(XmlNodeProcessor xmlNodeProcessor,
			AGEXmlNodeConfigHelper xmlNodeConfigHelper, XmlContentProcessor xmlContentProcessor,
			TemplateService templateService) {
		super(xmlNodeProcessor, xmlNodeConfigHelper, xmlContentProcessor, templateService);
	}

	@Override
    protected void updateSource(final DocumentVO document, File documentFile, boolean canModifySource) {
        try {
            byte[] xmlBytes = Files.readAllBytes(documentFile.toPath());
            if (canModifySource) {
                if (document.getCategory() == LeosCategory.BILL) {
                    xmlBytes = xmlContentProcessor.removeElements(xmlBytes, "//coverPage", 0);
                    // We have to remove the references to the annexes, we will add them when importing
                    xmlBytes = xmlContentProcessor.removeElements(xmlBytes, "//attachments", 0);
                }
                if (document.getCategory() == LeosCategory.ANNEX) {
                    xmlBytes = xmlContentProcessor.removeElements(xmlBytes, "//coverPage", 0);
                }
                if (document.getCategory() == LeosCategory.REPORT) {
                    xmlBytes = xmlContentProcessor.removeElements(xmlBytes, "//coverPage", 0);
                }
                if (document.getCategory() == LeosCategory.CANTODORADO) {
                    xmlBytes = xmlContentProcessor.removeElements(xmlBytes, "//coverPage", 0);
                }
				if (document.getCategory() == LeosCategory.MAIN) {
                    xmlBytes = xmlContentProcessor.removeElements(xmlBytes, "//coverPage", 0);
					// We have to remove the references to the annexes, we will add them when importing
                    xmlBytes = xmlContentProcessor.removeElements(xmlBytes, "//attachments", 0);
                }
            }
            document.setSource(xmlBytes);
        } catch (IOException e) {
            LOG.error("Error updating the source of the document: {}", e);
            // the post validation will take care to analyse wether the source is there or not
            document.setSource(null);
        }
    }
	
}
