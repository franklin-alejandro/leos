package eu.europa.ec.leos.services.amendment;

import java.util.List;
import java.util.Set;

import eu.europa.ec.leos.amend.AmendActDetails;
import eu.europa.ec.leos.amend.AmendAction;
import eu.europa.ec.leos.domain.cmis.document.Bill;
import eu.europa.ec.leos.model.action.CheckinElement;

public interface AGEAmendPatternLabelService extends AmendPatternLabelService {

	String generateLabel(AmendAction amendAction, List<String> elementIds, String title, String eli, String numArticleCk, String typNorm, String elementType, String elementId, boolean isEditing);
	String generatePreview(List<String> elementAmendmentSelect, String parentElement);
	byte[] insertNewMetadaElementActiveRef(Bill document, String eliDestination);
	String extractIdModAmendment(String element);
	byte[] insertNewMetadaElementAnalisis(Bill document, String idMod, String eliDestination, String actionType);
	AmendActDetails parseDetails(String href);
	String convertRefElementMeta (String eli);
	byte[] editingMetadataElementAnalisis(Bill document, String idModNew, String quotedStructureId, String eliDestination,
			String actionType);
	byte [] elementAmendmentSeveral (byte[] newXmlContent, String elementId, String idModNew, String quotedStructureId, String elementType, String actionType)  throws Exception;
	byte[] deleteElementModifi(Bill document, String elementId, String quotedId, String modId, String granParentId, String elementType);
	List<String> extracIdsMod(Set<CheckinElement> updatedElements, Bill bill);
	byte[] checkModifAndDelete(List<String> updatedElements, Bill bill);
}
