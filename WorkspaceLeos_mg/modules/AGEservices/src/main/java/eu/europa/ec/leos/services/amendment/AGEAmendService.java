package eu.europa.ec.leos.services.amendment;

import eu.europa.ec.leos.amend.AmendActDetails;
import eu.europa.ec.leos.amend.AmendAction;
import eu.europa.ec.leos.domain.cmis.document.Bill;
import eu.europa.ec.leos.model.user.User;

public interface AGEAmendService {

    AmendActDetails openAmendmentEditor(Bill bill, String articleId) throws Exception;

    void createAmendmentModification(Bill bill, String id, AmendActDetails amendActDetails, AmendAction actionType, byte[] selectedElement, String articleId) throws Exception;

	AmendActDetails openAmendmentEditor(Bill bill, String articleId, String type) throws Exception;
}
