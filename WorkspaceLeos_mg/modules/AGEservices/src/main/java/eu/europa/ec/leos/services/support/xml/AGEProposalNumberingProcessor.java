package eu.europa.ec.leos.services.support.xml;

import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.PROVISO;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.ARTICLE;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.ARTICLE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import eu.europa.ec.leos.domain.common.InstanceType;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.instance.Instance;

@Primary
@Component
@Instance(instances = {InstanceType.COMMISSION, InstanceType.OS})
public class AGEProposalNumberingProcessor extends ProposalNumberingProcessor implements AGENumberProcessor {
	
	public AGEProposalNumberingProcessor(ElementNumberingHelper elementNumberingHelper, MessageHelper messageHelper) {
		super(elementNumberingHelper, messageHelper);
		// TODO Auto-generated constructor stub
	}

	private static final Logger LOG = LoggerFactory.getLogger(ProposalNumberingProcessor.class);
	
	@Autowired
 	private MessageHelper messageHelper;
	
	@Autowired
    AGEElementNumberingHelper elementNumberingHelper;

    @Override
    public byte[] renumberArticles(byte[] xmlContent) {
        LOG.trace("Start renumberArticles ");
        String elementNumber = "Article ";
        
        try {
        LOG.debug("IMPRESION2: " + new String(xmlContent,"UTF-8"));
            return elementNumberingHelper.renumberElements(ARTICLE,elementNumber, xmlContent, messageHelper);
        } catch (Exception e) {
            throw new RuntimeException("Unable to perform the renumberArticles operation", e);
        }
    }
	
	
	@Override
	public String renumberImportedArticle(String xmlContent, String language) {
		String elementNumber = "Article ";
		String updatedElements = null;
		elementNumberingHelper.setImportAticleDefaultProperties();
		try {
			// Evolutivo #2421
			updatedElements = new String(elementNumberingHelper.renumberElements(ARTICLE, elementNumber, xmlContent),
					"UTF8");
		} catch (Exception e) {
			throw new RuntimeException("Unable to perform the renumberArticles operation", e);
		} finally {
			elementNumberingHelper.resetImportAticleDefaultProperties();
		}

		return updatedElements;
	}

	public String renumberImportedProviso(String xmlContent, String language) {
		String elementNumber = "Proviso";
		String updatedElements = null;
		elementNumberingHelper.setImportProvisoDefaultProperties();
		try {
			// Evolutivo #2421
			updatedElements = new String(elementNumberingHelper.renumberElements(PROVISO, elementNumber, xmlContent),
					"UTF8");
		} catch (Exception e) {
			throw new RuntimeException("Unable to perform the renumberProvisos operation", e);
		} finally {
			elementNumberingHelper.resetImportProvisoDefaultProperties();
		}

		return updatedElements;
	}	
	
	@Override
	public byte[] renumberProvisos(byte[] xmlContent) {
		LOG.trace("Start renumberProvisos ");
		String numberType = "";
		try {
			byte[] updatedElements = elementNumberingHelper.renumberProvisos(PROVISO, numberType, xmlContent,
					messageHelper);
			return updatedElements;

		} catch (Exception e) {
			throw new RuntimeException("Unable to perform the renumberProvisos operation", e);
		}
	}

	// Evolutivo #1052
	@Override
	public byte[] renumberOthers(byte[] xmlContent) {
		LOG.trace("Start renumberOthers");
		try {
			return elementNumberingHelper.renumberOthers(xmlContent, messageHelper);
		} catch (Exception e) {
			throw new RuntimeException("Unable to perform the renumberRecitals operation", e);
		}
	}

}
