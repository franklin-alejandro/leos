/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.services.document;

import static eu.europa.ec.leos.services.support.xml.XmlHelper.DOC;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.google.common.base.Stopwatch;

import cool.graph.cuid.Cuid;
import eu.europa.ec.leos.domain.cmis.Content;
import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.AGEMain;
import eu.europa.ec.leos.domain.cmis.document.Annex;
import eu.europa.ec.leos.domain.cmis.metadata.AGEMainMetadata;
import eu.europa.ec.leos.domain.cmis.metadata.AnnexMetadata;
import eu.europa.ec.leos.domain.common.TocMode;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.action.VersionVO;
import eu.europa.ec.leos.model.main.AGEMainStructureType;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.repository.document.AGEMainRepository;
import eu.europa.ec.leos.repository.store.PackageRepository;
import eu.europa.ec.leos.services.document.util.DocumentVOProvider;
import eu.europa.ec.leos.services.support.VersionsUtil;
import eu.europa.ec.leos.services.support.xml.AGEXmlNodeConfigHelper;
import eu.europa.ec.leos.services.support.xml.NumberProcessor;
import eu.europa.ec.leos.services.support.xml.XmlContentProcessor;
import eu.europa.ec.leos.services.support.xml.XmlNodeConfigHelper;
import eu.europa.ec.leos.services.support.xml.XmlNodeProcessor;
import eu.europa.ec.leos.services.support.xml.XmlTableOfContentHelper;
import eu.europa.ec.leos.services.validation.ValidationService;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;


@Service
public class AGEMainServiceImpl implements AGEMainService {

	private static final Logger LOG = LoggerFactory.getLogger(AGEMainServiceImpl.class);

	private static final String REPORT_NAME_PREFIX = "report_";
	private static final String REPORT_DOC_EXTENSION = ".xml";

	private final AGEMainRepository reportRepository;
	private final XmlNodeProcessor xmlNodeProcessor;
	private final XmlContentProcessor xmlContentProcessor;
	private final AGEXmlNodeConfigHelper xmlNodeConfigHelper;
	private final DocumentVOProvider documentVOProvider;
	private final ValidationService validationService;
	private final PackageRepository packageRepository;
	private final NumberProcessor numberingProcessor;
	private final XmlTableOfContentHelper xmlTableOfContentHelper;
	private final MessageHelper messageHelper;
	
	@Autowired
	AGEMainServiceImpl(
			AGEMainRepository reportRepository,
			PackageRepository packageRepository,
			XmlNodeProcessor xmlNodeProcessor,
			XmlContentProcessor xmlContentProcessor,
			NumberProcessor numberingProcessor,
			AGEXmlNodeConfigHelper xmlNodeConfigHelper,
			XmlTableOfContentHelper xmlTableOfContentHelper,
			ValidationService validationService,
			DocumentVOProvider documentVOProvider,
			MessageHelper messageHelper
		) {
		this.reportRepository = reportRepository;
		this.xmlNodeProcessor = xmlNodeProcessor;
		this.xmlContentProcessor = xmlContentProcessor;
		this.xmlNodeConfigHelper = xmlNodeConfigHelper;
		this.numberingProcessor = numberingProcessor;
		this.validationService = validationService;
		this.documentVOProvider = documentVOProvider;
		this.packageRepository = packageRepository;
		this.xmlTableOfContentHelper = xmlTableOfContentHelper;
		this.messageHelper = messageHelper;
	}

	@Override
	public AGEMain createMain(String templateId, String path, AGEMainMetadata metadata, String actionMessage,
			byte[] content) {
		LOG.trace("Creating Main... [templateId={}, path={}, metadata={}]", templateId, path, metadata);
		String name = generateMainName();
		metadata = metadata.withRef(name);// FIXME: a better scheme needs to be devised
		AGEMain report = reportRepository.createMain(templateId, path, name, metadata);
		byte[] updatedBytes = updateDataInXml((content == null) ? getContent(report) : content, metadata);
		return reportRepository.updateMain(report.getId(), metadata, updatedBytes, VersionType.MINOR, actionMessage);
	}
	
    @Override
    public AGEMain findMainByRef(String ref) {
        LOG.trace("Finding Main by ref... [ref=" + ref + "]");
        return reportRepository.findMainByRef(ref);
    }

	@Override
	public AGEMain createMainFromContent(String path, AGEMainMetadata metadata, String actionMessage, byte[] content) {
		LOG.trace("Creating Main From Content... [path={}, metadata={}]", path, metadata);
		String name = generateMainName();
		metadata = metadata.withRef(name);// FIXME: a better scheme needs to be devised
		AGEMain report = reportRepository.createMainFromContent(path, name, metadata, content);
		return reportRepository.updateMain(report.getId(), metadata, content, VersionType.MINOR, actionMessage);
	}

	@Override
	public void deleteMain(AGEMain report) {
		LOG.trace("Deleting Main... [id={}]", report.getId());
		reportRepository.deleteMain(report.getId());
	}

	@Override
	public AGEMain findMain(String id) {
		LOG.trace("Finding Main... [it={}]", id);
		return reportRepository.findMainById(id, true);
	}

	@Override
	@Cacheable(value = "docVersions", cacheManager = "cacheManager")
	public AGEMain findMainVersion(String id) {
		LOG.trace("Finding Main version... [it={}]", id);
		return reportRepository.findMainById(id, false);
	}

	@Override
	public AGEMain updateMain(AGEMain report, byte[] updatedMainContent, boolean major, String comment) {
		LOG.trace("Updating Main Xml Content... [id={}]", report.getId());

		report = reportRepository.updateMain(report.getId(), updatedMainContent, VersionType.MAJOR, comment);

		// call validation on document with updated content
		validationService.validateDocumentAsync(documentVOProvider.createDocumentVO(report, updatedMainContent));

		return report;
	}

    @Override
    public List<VersionVO> getAllVersions(String documentId, String docRef) {
        // TODO temporary call. paginated loading will be implemented in the future Story
        List<AGEMain> majorVersions = findAllMajors(docRef, 0, 9999);
        LOG.trace("Found {} majorVersions for [id={}]", majorVersions.size(), documentId);
        
        List<VersionVO> majorVersionsVO = VersionsUtil.buildVersionVO(majorVersions, messageHelper);
        return majorVersionsVO;
    }
    
    @Override
    public AGEMain updateMain(AGEMain report, byte[] updatedMainContent, VersionType versionType, String comment) {
        LOG.trace("Updating Annex Xml Content... [id={}]", report.getId());
        
        report = reportRepository.updateMain(report.getId(), updatedMainContent, versionType, comment); 
        
        //call validation on document with updated content
        validationService.validateDocumentAsync(documentVOProvider.createDocumentVO(report, updatedMainContent));

        return report;
    }
    
    @Override
    public int findAllMinorsCountForIntermediate(String docRef, String currIntVersion) {
        final String prevVersion = calculatePreviousVersion(currIntVersion);
        return reportRepository.findAllMinorsCountForIntermediate(docRef, currIntVersion, prevVersion);
    }
    
    private String calculatePreviousVersion(String currIntVersion) {
        final String prevVersion;
        String[] str = currIntVersion.split("\\.");
        if (str.length != 2) {
            throw new IllegalArgumentException("CMIS Version number should be in the format x.y");
        } else {
            int curr = Integer.parseInt(str[0]);
            int prev = curr - 1;
            prevVersion = prev + "." + "0";
        }
        return prevVersion;
    }
    
    @Override
    public List<AGEMain> findAllMinorsForIntermediate(String docRef, String currIntVersion, int startIndex, int maxResults) {
        final String prevIntVersion = calculatePreviousVersion(currIntVersion);
        return reportRepository.findAllMinorsForIntermediate(docRef, currIntVersion, prevIntVersion, startIndex, maxResults);
    }
    
    @Override
    public List<AGEMain> findAllMajors(String docRef, int startIndex, int maxResults) {
        return reportRepository.findAllMajors(docRef, startIndex, maxResults);
    }
    
    @Override
    public Integer findAllMajorsCount(String docRef) {
        return reportRepository.findAllMajorsCount(docRef);
    }
    
    @Override
    public Integer findRecentMinorVersionsCount(String documentId, String documentRef) {
        return reportRepository.findRecentMinorVersionsCount(documentId, documentRef);
    }

    @Override
    public List<AGEMain> findRecentMinorVersions(String documentId, String documentRef, int startIndex, int maxResults) {
        return reportRepository.findRecentMinorVersions(documentId, documentRef, startIndex, maxResults);
    }
	
	@Override
	public AGEMain updateMain(String reportId, AGEMainMetadata updatedMetadata) {
		LOG.trace("Updating Main... [id={}, updatedMetadata={}]", reportId, updatedMetadata);
		return reportRepository.updateMain(reportId, updatedMetadata);
	}

	@Override
	public AGEMain updateMainWithMilestoneComments(AGEMain report, List<String> milestoneComments, VersionType versionType,
			String comment) {
		LOG.trace("Updating Main... [id={}, milestoneComments={}, major={}, comment={}]", report.getId(), milestoneComments, versionType, comment);
		final byte[] updatedBytes = getContent(report);
		report = reportRepository.updateMilestoneComments(report.getId(), milestoneComments, updatedBytes, versionType, comment);
		return report;
	}

	@Override
	public AGEMain updateMainWithMilestoneComments(String reportId, List<String> milestoneComments) {
		LOG.trace("Updating Main... [id={}, milestoneComments={}]", reportId, milestoneComments);
		return reportRepository.updateMilestoneComments(reportId, milestoneComments);
	}

	@Override
	public List<AGEMain> findVersions(String id) {
		LOG.trace("Finding Main versions... [id={}]", id);
		// LEOS-2813 We have memory issues is we fetch the content of all versions.
		return reportRepository.findMainVersions(id, false);
	}

	@Override
	public AGEMain createVersion(String id, VersionType versionType, String comment) {
		LOG.trace("Creating Main version... [id={}, major={}, comment={}]", id, versionType, comment);
		final AGEMain report = findMain(id);
		final AGEMainMetadata metadata = report.getMetadata().getOrError(() -> "Main metadata is required!");
		final Content content = report.getContent().getOrError(() -> "Main content is required!");
		final byte[] contentBytes = content.getSource().getBytes();
		return reportRepository.updateMain(id, metadata, contentBytes, VersionType.MAJOR, comment);
	}

    @Override
    public List<TableOfContentItemVO> getTableOfContent(AGEMain report, TocMode mode) {
        Validate.notNull(report, "Main is required");
        final Content content = report.getContent().getOrError(() -> "Main content is required!");
        final byte[] reportContent = content.getSource().getBytes();
        return xmlTableOfContentHelper.buildTableOfContent(DOC, reportContent, mode);
    }

	@Override
	public AGEMain saveTableOfContent(AGEMain report, List<TableOfContentItemVO> tocList, AGEMainStructureType structureType, String actionMsg, User user) {
		Validate.notNull(report, "Main is required");
		Validate.notNull(tocList, "Table of content list is required");
		byte[] newXmlContent;

		newXmlContent = xmlContentProcessor.createDocumentContentWithNewTocList(tocList, getContent(report), user);
		switch(structureType) {
		case ARTICLE:
			newXmlContent = numberingProcessor.renumberArticles(newXmlContent);
			break;
		case LEVEL:
			newXmlContent = numberingProcessor.renumberLevel(newXmlContent);
			break;
		}
		newXmlContent = xmlContentProcessor.doXMLPostProcessing(newXmlContent);

		return updateMain(report, newXmlContent, false, actionMsg);
	}
	
    @Override
    public List<String> getAncestorsIdsForElementId(AGEMain report, List<String> elementIds) {
        Validate.notNull(report, "Annex is required");
        Validate.notNull(elementIds, "Element id is required");
        List<String> ancestorIds = new ArrayList<String>();
        byte[] content = getContent(report);
        for (String elementId : elementIds) {
            ancestorIds.addAll(xmlContentProcessor.getAncestorsIdsForElementId(content, elementId));
        }
        return ancestorIds;
    }
    
    @Override
    public AGEMain updateMainWithMetadata(AGEMain report, byte[] updatedMainContent, AGEMainMetadata metadata, VersionType versionType, String comment) {
        LOG.trace("Updating Annex... [id={}, updatedMetadata={}, versionType={}, comment={}]", report.getId(), metadata, versionType, comment);
        Stopwatch stopwatch = Stopwatch.createStarted();
        updatedMainContent = updateDataInXml(updatedMainContent, metadata);
        
        report = reportRepository.updateMain(report.getId(), metadata, updatedMainContent, versionType, comment);
        
        //call validation on document with updated content
        validationService.validateDocumentAsync(documentVOProvider.createDocumentVO(report, updatedMainContent));
        
        LOG.trace("Updated Annex ...({} milliseconds)", stopwatch.elapsed(TimeUnit.MILLISECONDS));
        return report;
    }

	@Override
	public AGEMain findMainByPackagePath(String path) {
		LOG.trace("Finding Main by package path... [path={}]", path);
		// FIXME can be improved, now we don't fetch ALL docs because it's loaded later
		// the one needed,
		// this can be improved adding a page of 1 item or changing the method/query.
		List<AGEMain> docs = packageRepository.findDocumentsByPackagePath(path, AGEMain.class, false);
		AGEMain report = findMain(docs.get(0).getId());
		return report;
	}

	private byte[] getContent(AGEMain report) {
		final Content content = report.getContent().getOrError(() -> "Main content is required!");
		return content.getSource().getBytes();
	}

	private byte[] updateDataInXml(final byte[] content, AGEMainMetadata dataObject) {
		byte[] updatedBytes = xmlNodeProcessor.setValuesInXml(content, xmlNodeConfigHelper.createValueMap(dataObject),
				xmlNodeConfigHelper.getConfig(dataObject.getCategory()));
		return xmlContentProcessor.doXMLPostProcessing(updatedBytes);
	}

	private String generateMainName() {
		return REPORT_NAME_PREFIX + Cuid.createCuid() + REPORT_DOC_EXTENSION;
	}

	@Override
	public AGEMain updateMain(AGEMain report, AGEMainMetadata metadata, VersionType version, String comment) {
        LOG.trace("Updating Main... [id={}, updatedMetadata={}, major={}, comment={}]", report.getId(), metadata, version, comment);
        Stopwatch stopwatch = Stopwatch.createStarted();
        byte[] updatedBytes = updateDataInXml(getContent(report), metadata); //FIXME: Do we need latest data again??
        
        report = reportRepository.updateMain(report.getId(), metadata, updatedBytes, version, comment);
        
        //call validation on document with updated content
        validationService.validateDocumentAsync(documentVOProvider.createDocumentVO(report, updatedBytes));
        
        LOG.trace("Updated Main ...({} milliseconds)", stopwatch.elapsed(TimeUnit.MILLISECONDS));
        return report;
	}

}
