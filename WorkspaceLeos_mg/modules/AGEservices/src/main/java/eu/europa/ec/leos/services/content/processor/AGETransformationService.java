package eu.europa.ec.leos.services.content.processor;

import eu.europa.ec.leos.security.LeosPermission;

import java.io.InputStream;
import java.util.List;

public interface AGETransformationService extends TransformationService{
	
	String toImportProjectXml(InputStream documentStream, String contextPath, List<LeosPermission> permissions);
	
	String toImportBOEXml(InputStream documentStream, String contextPath, List<LeosPermission> permissions);

	String toAmendmentDOUEXml(InputStream documentStream, String contextPath, List<LeosPermission> permissions);

}
