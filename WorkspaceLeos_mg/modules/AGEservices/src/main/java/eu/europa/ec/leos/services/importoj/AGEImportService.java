package eu.europa.ec.leos.services.importoj;

import java.util.ArrayList;
import java.util.List;

import com.ximpleware.NavException;

import eu.europa.ec.leos.domain.cmis.document.Annex;

public interface AGEImportService extends ImportService {

    byte[] insertSelectedProjectElements(byte[] originalContent, byte[] importedContent, List<String> elementIds, String language);

	byte[] insertSelectedElementsBOE(byte[] documentContent, byte[] importedContent, List<String> elementIds,String language) throws NavException;

	String getBOEDocument(String type, int year, int month, int day, int number, String accion);

	boolean getBOEDocumentoDerogado(String reference);
	
	boolean getBOEDocumentoDerogado(String type, int year, int month, int day, int number);

	String getBOEDocument(String reference, String accion);

	ArrayList<String> getNumberBOEModified(byte[] importedContent, List<String> elementIds, String language);

	boolean getElementRepealed(byte[] importedContent, List<String> elementIds);
	
	boolean getBOEElementReferenced(byte[] importedContent, List<String> elementIds);

	byte[] insertSelectedElements(Annex annex, byte[] importedContent, List<String> elementIds, String language);

	byte[] insertSelectedElementsAnnexBOE(byte[] documentContent, byte[] importedContent, List<String> elementIds,
			String language) throws NavException;

	byte[] insertSelectedProjectElementsAnnex(byte[] originalContent, byte[] importedContent, List<String> elementIds,
			String language);
}
