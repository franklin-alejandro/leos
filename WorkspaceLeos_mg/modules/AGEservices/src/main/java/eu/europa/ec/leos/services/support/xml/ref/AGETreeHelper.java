package eu.europa.ec.leos.services.support.xml.ref;

import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.BOOK;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.TITLE;

import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ximpleware.VTDNav;

import eu.europa.ec.leos.services.support.xml.VTDUtils;
import eu.europa.ec.leos.services.support.xml.XmlHelper;


public class AGETreeHelper extends TreeHelper {
	
    private static final Logger LOG = LoggerFactory.getLogger(AGETreeHelper.class);
    
    /* protected */
    private static final Pattern idPattern = Pattern.compile("\\s(xml:id)(\\s)*=(\\s)*\"(.+?)\"");
	
    static TreeNode createNode(VTDNav vtdNav, TreeNode parent, int depth) throws Exception {
        try {
            int currentIndex = vtdNav.getCurrentIndex();
            int tokenType = vtdNav.getTokenType(currentIndex);

            String tagContent = getTagContent(vtdNav);
            String tagName = null;
            String tagId = null;
            if (tokenType == VTDNav.TOKEN_STARTING_TAG) {
                tagName = vtdNav.toString(currentIndex);
                Matcher idMatcher = idPattern.matcher(tagContent);
                tagId = idMatcher.find() ? idMatcher.group(4) : null;
            } else if (tokenType == VTDNav.TOKEN_CHARACTER_DATA) {//if textNode
                tagName = "text";
            }

            //find num 
            String numValue = findNum(vtdNav, tagName);
            if (tagName.length() > 0 && tagName != null) {
            	if (tagName.contentEquals(BOOK)) {
            		numValue = numValue.toLowerCase();
            	} else if (tagName.contentEquals(TITLE) && numValue.equals("PRELIMINAR")) {
            		numValue = numValue.toLowerCase();
            	}
            }
            int childSeq = findSeq(vtdNav, tagName);
            return new TreeNode(tagName, depth, childSeq, tagId, numValue, vtdNav.getCurrentIndex(), parent, null);
        } catch (Exception ex) {
            LOG.error("Unexpected error. Consuming and continuing!!", ex);
            throw ex;
        }
    }
    
    /* protected */
    private static String getTagContent(VTDNav vtdNav) throws Exception {
        int offset = (int) vtdNav.getElementFragment();
        long token = (long) vtdNav.getContentFragment();
        int offsetContent = (int) token;
        String tagContent = (offsetContent > 0)
                ? new String(vtdNav.getXML().getBytes(offset, (offsetContent - offset)))
                : null;
        return tagContent;
    }

    /* protected */
    private static String findNum(VTDNav contentNavigator, String tagName) {
        contentNavigator.push();
        try {
            if (contentNavigator.toElement(VTDNav.FIRST_CHILD)) {
                do {
                    String childTag = contentNavigator.toString(contentNavigator.getCurrentIndex());
                    if (XmlHelper.NUM.equalsIgnoreCase(childTag)) {
                        //get content
                        return parseNum(getContent(contentNavigator));
                    }
                } while (contentNavigator.toElement(VTDNav.NEXT_SIBLING));
            }
        } catch (Exception e) {
            LOG.error("Error while getting num content", e);
        } finally {
            contentNavigator.pop();
        }
        return null;
    }
    
    /* protected */
    private static String parseNum(String xmlNum) {
        //remove type if part/section/
        String[] s = xmlNum.split(" ", 2);
        String num = s.length > 1 ? s[1] : s[0];
        //clean spaces, .,(), etc
        return num.replaceAll("[\\s+|\\(|\\)|\\.]", "");
    }

    /* protected */
    private static String getContent(VTDNav vtdNav) throws Exception {
        long token = vtdNav.getContentFragment();
        int offsetContent = (int) token;
        int length = (int) (token >> 32);
        String tagContent = (offsetContent > 0)
                ? new String(vtdNav.getXML().getBytes(offsetContent, length), Charset.forName("UTF-8"))
                : null;
        return tagContent;
    }
    
    /* protected */
    private static int findSeq(VTDNav vtdNav, String type) {
        vtdNav.push();
        int childSeq = 0;
        try {
            if (VTDUtils.toBeSkippedForNumbering(vtdNav)) {
                childSeq++;
            }
            do {
                if (type == null || type.equals(vtdNav.toString(vtdNav.getCurrentIndex()))) {
                    //this considers elements of same type only.
                    if (!VTDUtils.toBeSkippedForNumbering(vtdNav)) {
                        childSeq++;
                    }
                }
            } while (vtdNav.toElement(VTDNav.PREV_SIBLING));
        } catch (Exception e) {
            LOG.error("Error while getting child seq content", e);
        } finally {
            vtdNav.pop();
        }
        return childSeq;
    }



}
