package eu.europa.ec.leos.services.support.xml;

public interface AGENumberProcessor extends NumberProcessor {
	
    byte[] renumberProvisos(byte[] xmlContent);
    
    String renumberImportedProviso(String content, String language);

    byte[] renumberOthers(byte[] xmlContent);

}
