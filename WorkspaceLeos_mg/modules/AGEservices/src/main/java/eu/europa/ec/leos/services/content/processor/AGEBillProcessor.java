package eu.europa.ec.leos.services.content.processor;

import java.util.List;
import eu.europa.ec.leos.services.support.xml.AGEExtRefUpdateOutput;

import org.springframework.security.access.prepost.PreAuthorize;

import com.ximpleware.NavException;

import eu.europa.ec.leos.domain.cmis.document.Bill;

public interface AGEBillProcessor extends BillProcessor {

	@PreAuthorize("hasPermission(#document, 'CAN_UPDATE')")
	byte[] insertNewElement(Bill document, String elementId, boolean before, String tagName, String numberType);
//	
//	@PreAuthorize("hasPermission(#document, 'CAN_UPDATE')")
//	public byte[] insertNewMetadaElement(Bill document, String idMod, String nameNorm, String eliDestination, String tagName, String actionType);

	String addRef2linkReferences(String content, List<List<String>> refs);
	
	AGEExtRefUpdateOutput updatedExternalMetaRefs(byte[] content);
}