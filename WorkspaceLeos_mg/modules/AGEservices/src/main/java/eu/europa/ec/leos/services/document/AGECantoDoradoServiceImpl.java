package eu.europa.ec.leos.services.document;

import static eu.europa.ec.leos.services.support.xml.XmlHelper.DOC;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.INTRO;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.google.common.base.Stopwatch;

import cool.graph.cuid.Cuid;
import eu.europa.ec.leos.domain.cmis.Content;
import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.AGECantoDorado;
import eu.europa.ec.leos.domain.cmis.metadata.AGECantoDoradoMetadata;
import eu.europa.ec.leos.domain.common.TocMode;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.action.VersionVO;
import eu.europa.ec.leos.model.cantodorado.AGECantoDoradoStructureType;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.repository.document.AGECantoDoradoRepository;
import eu.europa.ec.leos.repository.store.PackageRepository;
import eu.europa.ec.leos.services.document.util.DocumentVOProvider;
import eu.europa.ec.leos.services.support.LeosUtil;
import eu.europa.ec.leos.services.support.VersionsUtil;
import eu.europa.ec.leos.services.support.xml.AGENumberProcessor;
import eu.europa.ec.leos.services.support.xml.AGEXmlHelper;
import eu.europa.ec.leos.services.support.xml.AGEXmlNodeConfigHelper;
import eu.europa.ec.leos.services.support.xml.XmlContentProcessor;
import eu.europa.ec.leos.services.support.xml.XmlNodeProcessor;
import eu.europa.ec.leos.services.support.xml.XmlTableOfContentHelper;
import eu.europa.ec.leos.services.validation.ValidationService;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;

import static eu.europa.ec.leos.services.support.xml.XmlHelper.BODY;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.REFERENCES;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.ANALYSIS;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.RECITALS;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.META;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.MAINBODY;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.ACTIVEMODIFICATIONS;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.DOCPURPOSE;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.DOCTYPE;
@Service
public class AGECantoDoradoServiceImpl implements AGECantoDoradoService{
	
	private static final Logger LOG = LoggerFactory.getLogger(AGECantoDoradoServiceImpl.class);

	private static final String CANTODORADO_NAME_PREFIX = "cantoDorado_";
	private static final String CANTODORADO_DOC_EXTENSION = ".xml";

	private final AGECantoDoradoRepository cantoDoradoRepository;
	private final XmlNodeProcessor xmlNodeProcessor;
	private final XmlContentProcessor xmlContentProcessor;
	private final AGEXmlNodeConfigHelper xmlNodeConfigHelper;
	private final DocumentVOProvider documentVOProvider;
	private final ValidationService validationService;
	private final PackageRepository packageRepository;
	private final AGENumberProcessor numberingProcessor;
	private final XmlTableOfContentHelper xmlTableOfContentHelper;
	private final MessageHelper messageHelper;

	@Autowired
	AGECantoDoradoServiceImpl(
			AGECantoDoradoRepository cantoDoradoRepository,
			PackageRepository packageRepository,
			XmlNodeProcessor xmlNodeProcessor,
			XmlContentProcessor xmlContentProcessor,
			AGENumberProcessor numberingProcessor,
			AGEXmlNodeConfigHelper xmlNodeConfigHelper,
			XmlTableOfContentHelper xmlTableOfContentHelper,
			ValidationService validationService,
			DocumentVOProvider documentVOProvider,
			MessageHelper messageHelper
		) {
		this.cantoDoradoRepository = cantoDoradoRepository;
		this.xmlNodeProcessor = xmlNodeProcessor;
		this.xmlContentProcessor = xmlContentProcessor;
		this.xmlNodeConfigHelper = xmlNodeConfigHelper;
		this.numberingProcessor = numberingProcessor;
		this.validationService = validationService;
		this.documentVOProvider = documentVOProvider;
		this.packageRepository = packageRepository;
		this.xmlTableOfContentHelper = xmlTableOfContentHelper;
		this.messageHelper = messageHelper;
	}

	
	@Override
	public AGECantoDorado createCantoDorado(String templateId, String path, AGECantoDoradoMetadata metadata,
			String actionMessage, byte[] content) {
		LOG.trace("Creating CantoDorado... [templateId={}, path={}, metadata={}]", templateId, path, metadata);
		String name = generateCantoDoradoName();
		metadata = metadata.withRef(name);// FIXME: a better scheme needs to be devised
		AGECantoDorado cantoDorado = cantoDoradoRepository.createCantoDorado(templateId, path, name, metadata);
		byte[] updatedBytes = updateDataInXml((content == null) ? getContent(cantoDorado) : content, metadata);
//		System.out.println(new String("PRIMERO-----" +getContent(cantoDorado)));
//		updatedBytes = xmlContentProcessor.deleteElementByTagNameAndId(updatedBytes, INTRO, "reason_exposition");
//		System.out.println(new String("SEGUNDO-----" + getContent(cantoDorado)));
		return cantoDoradoRepository.updateCantoDorado(cantoDorado.getId(), metadata, updatedBytes, VersionType.MINOR, actionMessage);
	}
	
	@Override
	public AGECantoDorado findCantoDoradoByRef(String ref) {
        LOG.trace("Finding CantoDorado by ref... [ref=" + ref + "]");
        return cantoDoradoRepository.findCantoDoradoByRef(ref);
	}

	@Override
	public AGECantoDorado createCantoDoradoFromContent(String path, AGECantoDoradoMetadata metadata,
			String actionMessage, byte[] content) {
		LOG.trace("Creating CantoDorado From Content... [path={}, metadata={}]", path, metadata);
		String name = generateCantoDoradoName();
		metadata = metadata.withRef(name);// FIXME: a better scheme needs to be devised
		AGECantoDorado cantoDorado = cantoDoradoRepository.createCantoDoradoFromContent(path, name, metadata, content);
		return cantoDoradoRepository.updateCantoDorado(cantoDorado.getId(), metadata, content, VersionType.MINOR, actionMessage);
	}

	@Override
	public void deleteCantoDorado(AGECantoDorado cantoDorado) {
		LOG.trace("Deleting cantoDorado... [id={}]", cantoDorado.getId());
		cantoDoradoRepository.deleteCantoDorado(cantoDorado.getId());
		
	}
	
	@Override
	public AGECantoDorado findCantoDorado(String id) {
		LOG.trace("Finding CantoDorado... [it={}]", id);
		return cantoDoradoRepository.findCantoDoradoById(id, true);
	}
	
	@Override
	@Cacheable(value = "docVersions", cacheManager = "cacheManager")
	public AGECantoDorado findCantoDoradoVersion(String id) {
		LOG.trace("Finding CantoDorado version... [it={}]", id);
		return cantoDoradoRepository.findCantoDoradoById(id, false);
	}


	@Override
	public AGECantoDorado updateCantoDorado(AGECantoDorado cantoDorado, byte[] updatedCantoDoradoContent, boolean major, String comment) {
		LOG.trace("Updating CantoDorado Xml Content... [id={}]", cantoDorado.getId());

		cantoDorado = cantoDoradoRepository.updateCantoDorado(cantoDorado.getId(), updatedCantoDoradoContent, VersionType.MAJOR, comment);

		// call validation on document with updated content
		validationService.validateDocumentAsync(documentVOProvider.createDocumentVO(cantoDorado, updatedCantoDoradoContent));

		return cantoDorado;
	}

	@Override
	public List<VersionVO> getAllVersions(String documentId, String docRef) {
        List<AGECantoDorado> majorVersions = findAllMajors(docRef, 0, 9999);
        LOG.trace("Found {} majorVersions for [id={}]", majorVersions.size(), documentId);
        
        List<VersionVO> majorVersionsVO = VersionsUtil.buildVersionVO(majorVersions, messageHelper);
        return majorVersionsVO;
	}
	
	@Override
	public AGECantoDorado updateCantoDorado(AGECantoDorado cantoDorado, byte[] updatedCantoDoradoContent, VersionType versionType, String comment) {
        LOG.trace("Updating Annex Xml Content... [id={}]", cantoDorado.getId());
        
        cantoDorado = cantoDoradoRepository.updateCantoDorado(cantoDorado.getId(), updatedCantoDoradoContent, versionType, comment); 
        
        //call validation on document with updated content
        validationService.validateDocumentAsync(documentVOProvider.createDocumentVO(cantoDorado, updatedCantoDoradoContent));

        return cantoDorado;
	}
	
	@Override
	public int findAllMinorsCountForIntermediate(String docRef, String currIntVersion) {
        final String prevVersion = calculatePreviousVersion(currIntVersion);
        return cantoDoradoRepository.findAllMinorsCountForIntermediate(docRef, currIntVersion, prevVersion);
	}

    private String calculatePreviousVersion(String currIntVersion) {
        final String prevVersion;
        String[] str = currIntVersion.split("\\.");
        if (str.length != 2) {
            throw new IllegalArgumentException("CMIS Version number should be in the format x.y");
        } else {
            int curr = Integer.parseInt(str[0]);
            int prev = curr - 1;
            prevVersion = prev + "." + "0";
        }
        return prevVersion;
    }
    
	@Override
	public List<AGECantoDorado> findAllMinorsForIntermediate(String docRef, String currIntVersion, int startIndex, int maxResults) {
        final String prevIntVersion = calculatePreviousVersion(currIntVersion);
        return cantoDoradoRepository.findAllMinorsForIntermediate(docRef, currIntVersion, prevIntVersion, startIndex, maxResults);
	}

	@Override
	public List<AGECantoDorado> findAllMajors(String docRef, int startIndex, int maxResults) {
		return cantoDoradoRepository.findAllMajors(docRef, startIndex, maxResults);
	}
	
	@Override
	public Integer findAllMajorsCount(String docRef) {
		 return cantoDoradoRepository.findAllMajorsCount(docRef);
	}
	
	@Override
	public Integer findRecentMinorVersionsCount(String documentId, String documentRef) {
		return cantoDoradoRepository.findRecentMinorVersionsCount(documentId, documentRef);
	}
	
	@Override
	public List<AGECantoDorado> findRecentMinorVersions(String documentId, String documentRef, int startIndex, int maxResults) {
		return cantoDoradoRepository.findRecentMinorVersions(documentId, documentRef, startIndex, maxResults);
	}
	
	@Override
	public AGECantoDorado updateCantoDorado(String cantoDoradoId, AGECantoDoradoMetadata metadata) {
		LOG.trace("Updating Canto Dorado... [id={}, updatedMetadata={}]", cantoDoradoId, metadata);
		return cantoDoradoRepository.updateCantoDorado(cantoDoradoId, metadata);
	}
	
	@Override
	public AGECantoDorado updateCantoDoradoWithMilestoneComments(AGECantoDorado cantoDorado, List<String> milestoneComments, VersionType versionType, String comment) {
		LOG.trace("Updating CantoDorado... [id={}, milestoneComments={}, major={}, comment={}]", cantoDorado.getId(), milestoneComments, versionType, comment);
		final byte[] updatedBytes = getContent(cantoDorado);
		cantoDorado = cantoDoradoRepository.updateMilestoneComments(cantoDorado.getId(), milestoneComments, updatedBytes, versionType, comment);
		return cantoDorado;
	}
	
	@Override
	public AGECantoDorado updateCantoDoradotWithMilestoneComments(String cantoDoradoId, List<String> milestoneComments) {
		LOG.trace("Updating Canto Dorado... [id={}, milestoneComments={}]", cantoDoradoId, milestoneComments);
		return cantoDoradoRepository.updateMilestoneComments(cantoDoradoId, milestoneComments);
	}
	

	@Override
	public List<AGECantoDorado> findVersions(String id) {
		LOG.trace("Finding Canto Dorado versions... [id={}]", id);
		// LEOS-2813 We have memory issues is we fetch the content of all versions.
		return cantoDoradoRepository.findCantoDoradoVersions(id, false);
	}
	
	@Override
	public AGECantoDorado createVersion(String id, VersionType versionType, String comment) {
		LOG.trace("Creating Canto Dorado version... [id={}, major={}, comment={}]", id, versionType, comment);
		final AGECantoDorado cantoDorado = findCantoDorado(id);
		final AGECantoDoradoMetadata metadata = cantoDorado.getMetadata().getOrError(() -> "cantoDorado metadata is required!");
		final Content content = cantoDorado.getContent().getOrError(() -> "cantoDorado content is required!");
		final byte[] contentBytes = content.getSource().getBytes();
		return cantoDoradoRepository.updateCantoDorado(id, metadata, contentBytes, VersionType.MAJOR, comment);
	}
	
	@Override
	public List<TableOfContentItemVO> getTableOfContent(AGECantoDorado cantoDorado, TocMode mode) {
		Validate.notNull(cantoDorado, "Canto Dorado is required");
		final Content content = cantoDorado.getContent().getOrError(() -> "cantoDorado content is required!");
		final byte[] cantoDoradoContent = content.getSource().getBytes();
		return xmlTableOfContentHelper.buildTableOfContent(DOC, cantoDoradoContent, mode);//cuarentena
	}

	@Override
	public AGECantoDorado saveTableOfContent(AGECantoDorado cantoDorado, List<TableOfContentItemVO> tocList, AGECantoDoradoStructureType structureType, String actionMsg, User user) {
		Validate.notNull(cantoDorado, "CantoDorado is required");
		Validate.notNull(tocList, "Table of content list is required");
		byte[] newXmlContent;

		newXmlContent = xmlContentProcessor.createDocumentContentWithNewTocList(tocList, getContent(cantoDorado), user);
		switch(structureType) {
		case ARTICLE:
			newXmlContent = numberingProcessor.renumberArticles(newXmlContent);
			break;
		case PROVISO:
			//CUARENTENA
			newXmlContent = numberingProcessor.renumberProvisos(newXmlContent);
			break;
		}
		newXmlContent = xmlContentProcessor.doXMLPostProcessing(newXmlContent);

		return updateCantoDorado(cantoDorado, newXmlContent, false, actionMsg);
	}
	
	@Override
	public List<String> getAncestorsIdsForElementId(AGECantoDorado cantoDorado, List<String> elementIds) {
        Validate.notNull(cantoDorado, "cantoDorado is required");
        Validate.notNull(elementIds, "Element id is required");
        List<String> ancestorIds = new ArrayList<String>();
        byte[] content = getContent(cantoDorado);
        for (String elementId : elementIds) {
            ancestorIds.addAll(xmlContentProcessor.getAncestorsIdsForElementId(content, elementId));
        }
        return ancestorIds;
	}

	@Override
	public AGECantoDorado updateCantoDoradotWithMetadata(AGECantoDorado cantoDorado, byte[] updatedCantoDoradoContent, AGECantoDoradoMetadata metadata, VersionType versionType, String comment) {
        LOG.trace("Updating cantoDorado... [id={}, updatedMetadata={}, versionType={}, comment={}]", cantoDorado.getId(), metadata, versionType, comment);
        Stopwatch stopwatch = Stopwatch.createStarted();
        updatedCantoDoradoContent = updateDataInXml(updatedCantoDoradoContent, metadata);
        
        cantoDorado = cantoDoradoRepository.updateCantoDorado(cantoDorado.getId(), metadata, updatedCantoDoradoContent, versionType, comment);
        
        //call validation on document with updated content
        validationService.validateDocumentAsync(documentVOProvider.createDocumentVO(cantoDorado, updatedCantoDoradoContent));
        
        LOG.trace("Updated cantoDorado ...({} milliseconds)", stopwatch.elapsed(TimeUnit.MILLISECONDS));
        return cantoDorado;
	}

	@Override
	public AGECantoDorado findCantoDoradoByPackagePath(String path) {
		LOG.trace("Finding Canto Dorado by package path... [path={}]", path);
		AGECantoDorado cantoDorado = null;
		// FIXME can be improved, now we don't fetch ALL docs because it's loaded later
		// the one needed,
		// this can be improved adding a page of 1 item or changing the method/query.
		List<AGECantoDorado> docs = packageRepository.findDocumentsByPackagePath(path, AGECantoDorado.class, false);
		if (docs.size() > 0) {
			cantoDorado = findCantoDorado(docs.get(0).getId());
		}		
		return cantoDorado;
	}
	
	private byte[] getContent(AGECantoDorado cantoDorado) {
		final Content content = cantoDorado.getContent().getOrError(() -> "Canto Dorado content is required!");
		return content.getSource().getBytes();
	}

	@Override
	public AGECantoDorado updateCantoDorado(AGECantoDorado cantoDorado, AGECantoDoradoMetadata metadata, VersionType version, String comment) {
        LOG.trace("Updating Canto Dorado... [id={}, updatedMetadata={}, major={}, comment={}]", cantoDorado.getId(), metadata, version, comment);
        Stopwatch stopwatch = Stopwatch.createStarted();
        byte[] updatedBytes = updateDataInXml(getContent(cantoDorado), metadata); //FIXME: Do we need latest data again??
        
        cantoDorado = cantoDoradoRepository.updateCantoDorado(cantoDorado.getId(), metadata, updatedBytes, version, comment);
        
        //call validation on document with updated content
        validationService.validateDocumentAsync(documentVOProvider.createDocumentVO(cantoDorado, updatedBytes));
        
        LOG.trace("Updated cantoDorado ...({} milliseconds)", stopwatch.elapsed(TimeUnit.MILLISECONDS));
        return cantoDorado;
	}

	private byte[] updateDataInXml(final byte[] content, AGECantoDoradoMetadata dataObject) {
		byte[] updatedBytes = xmlNodeProcessor.setValuesInXml(content, xmlNodeConfigHelper.createValueMap(dataObject),
				xmlNodeConfigHelper.getConfig(dataObject.getCategory()));
		return xmlContentProcessor.doXMLPostProcessing(updatedBytes);
	}
	
	private String generateCantoDoradoName() {
		return CANTODORADO_NAME_PREFIX + Cuid.createCuid() + CANTODORADO_DOC_EXTENSION;
	}


	@Override
	public byte[] updateCantoDoradoFromBill(byte[] billContent, byte[] cantoDoradoContent, String number, String year,  String month, String day,
			String organism, boolean newCantoDorado) {
		String[] organismArr = organism.split("::");
		
		String path = "//";
		String analysis = xmlContentProcessor.getElementContentFragmentByPath(billContent, path + ANALYSIS, true);
		String references = xmlContentProcessor.getElementContentFragmentByPath(billContent, path + REFERENCES, true); 
		String recitals = xmlContentProcessor.getElementContentFragmentByPath(billContent, path + RECITALS, true); 
		String mainBody = xmlContentProcessor.getElementContentFragmentByPath(billContent, path + BODY, true);
		String docPurpose = xmlContentProcessor.getElementContentFragmentByPath(billContent, path + DOCPURPOSE, true);
		String docType = xmlContentProcessor.getElementContentFragmentByPath(billContent, path + DOCTYPE, true);
		String formula = StringUtils.capitalize(docType.toLowerCase()) + " " + number + "/" + year + ", de " + month + ", " + docPurpose;
		
		if (analysis != null) {
			if (newCantoDorado) {
				cantoDoradoContent = xmlContentProcessor.appendElementInsideTagByXPath(cantoDoradoContent, path + ANALYSIS, analysis, true);
			} else {
				analysis  =  AGEXmlHelper.getCantoDoradoAnalysis(analysis);
				cantoDoradoContent = xmlContentProcessor.replaceElement(cantoDoradoContent, path + ANALYSIS, true, analysis);
			}
		}		
		
		if (references != null) {
			if (newCantoDorado) {
				cantoDoradoContent = xmlContentProcessor.appendElementInsideTagByXPath(cantoDoradoContent, path + REFERENCES, references, true);
			} else {
				references = AGEXmlHelper.getCantoDoradoReferences(references);
				cantoDoradoContent = xmlContentProcessor.replaceElement(cantoDoradoContent,  path + REFERENCES, true, references);
			}
		}
			
		if (recitals != null) {
			if (newCantoDorado) {
				cantoDoradoContent = xmlContentProcessor.appendElementInsideTagByXPath(cantoDoradoContent, path + RECITALS, recitals, true);
			} else {
				recitals = AGEXmlHelper.getCantoDoradoRecitals(recitals);
				cantoDoradoContent = xmlContentProcessor.replaceElement(cantoDoradoContent,  path + RECITALS, true, recitals);
			}
			cantoDoradoContent = xmlContentProcessor.deleteElementByTagNameAndId(cantoDoradoContent, INTRO, "reason_exposition");
		}
		
		if (mainBody != null)
			if (newCantoDorado) {
				cantoDoradoContent = xmlContentProcessor.appendElementInsideTagByXPath(cantoDoradoContent, path + MAINBODY, mainBody, true);
			} else {
				mainBody = AGEXmlHelper.getCantoDoradoMainBody(mainBody);
				cantoDoradoContent = xmlContentProcessor.replaceElement(cantoDoradoContent,  path + MAINBODY, true, mainBody);
			}
		
		
		//MINISTERIO DE ......
		path = "//container[@xml:id = 'coverpage__container_2']";
		String newContent = AGEXmlHelper.getCantoDoradoPrefaceOrganism(organismArr[0]);
		cantoDoradoContent = xmlContentProcessor.replaceElement(cantoDoradoContent, path, true, newContent);
		path = "//formula/p[@xml:id = 'preface__formula_2__p']";
		String intro =LeosUtil.getIntroLey(docType);
		newContent = AGEXmlHelper.getCantoDoradoPrefaceFormula2(intro);
		cantoDoradoContent = xmlContentProcessor.replaceElement(cantoDoradoContent, path, true, newContent);
		//texto con el título completo con el nuevo número y fecha
		path = "//formula/p[@xml:id = 'preface__formula_3__p']";
		newContent = AGEXmlHelper.getCantoDoradoPrefaceFormula3(formula);
		cantoDoradoContent = xmlContentProcessor.replaceElement(cantoDoradoContent, path, true, newContent);
		//conclusiones y firmas
		path = "//conclusions";
		newContent = AGEXmlHelper.getCantoDoradoConclusions(year, month, day, organismArr[0], organismArr[1], organismArr[2] + " " + organismArr[3] );
		cantoDoradoContent = xmlContentProcessor.replaceElement(cantoDoradoContent, path, true, newContent);
		return cantoDoradoContent;
	}

}
