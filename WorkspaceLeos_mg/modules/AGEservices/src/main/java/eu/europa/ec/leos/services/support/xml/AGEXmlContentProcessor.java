package eu.europa.ec.leos.services.support.xml;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ximpleware.NavException;

public interface AGEXmlContentProcessor extends XmlContentProcessor {
	
    /**
     * returns a Array String with id mref updated content.
     * @param content
     * @return
     */
    ArrayList<String> doImportedMrefProjectPreProcessing(ArrayList<String[]> elementMref);
    
    /**
     * returns a String with new article or new proviso id as key and the updated content.
     * @param content
     * @param elementType
     * @return
     */
    String doImportedElementProjectPreProcessing(String content, String elementType, ArrayList<String> mrefId);
    
    Map<String, List<String>> bloqueVersion(byte[] xmlContent);

	List<String> crearConsolidado(byte[] eli, String type);
	
	boolean getReferencedBOE(byte[] content,String elementType, String id, String numElement);

	String extractIdModAmendment(String xmlContent);
	
	String equalActiveRefAmendment (byte[] xmlContent, String nameNorm, String eliDestination);
	
	String createNewNumInsertAmendment  (String insertType, String elementSelect, String elementParent, String typeElementSelect);

	AGEExtRefUpdateOutput updatedExternalMetaReferences(byte[] xmlContent);

	byte[] addExternalMetaReference(byte[] xmlContent, String eId, String hRef);
	
	String[] getNormVersion(String ELI);

	byte[] appendElementInsideTagByXPathAmend(byte[] newXmlContent, String xPath, String template, boolean b);

	 Map<String, List<String>>  createFrameTextAmend(byte[] xmlContent, String xPath);

	byte[] appendElementInsideModAmend(byte[] xmlContent, String xPath, String newContent);
	
	byte[] deleteRefNormElementAmend(byte[] xmlContent, String xPath, String refNorm) ;
	
	int countSeveralMod (byte[] xmlContent, String xPath);

	String[] typeActiveMoficationAmendmentNew(byte[] xmlContent, String idMod, String actionType, String quotedStructureId, String eliDestination, String pos, String wid);

	String[] typeActiveMoficationAmendmentMod(byte[] xmlContent, String idModNew, String actionType, String quotedStructureId, String eliDestination, String pos, String wid);

	String convertActionType(String actionType);

	byte[] appendElementXPathAmend(byte[] xmlContent, String xPath, String newContent);
	
	byte[] deleteElementModifi(byte[] xmlContent, String tagName, String idAttributeValue);
	
	String getIdModEditingModification (byte[] newXmlContent, String xPath);
	
	String getParentNumEditingModification (byte[] newXmlContent, String xPath);
	
	byte[] deleteAllTextualMod  (byte[] newXmlContent, String xPath);
	
	String getActiveRefAmendment (byte[] xmlContentDocument, String xpath);
	
	byte[] deleteElementByTagNameAndIdParentMod (byte[] xmlContent, String tagName, String idAttributeValue);
	
	int countSeveralQuoted(byte[] xmlContent, String xPath, String modId);
	
	List <String> getListElementIdByPath(byte[] xmlContent, String xPath) ;
	
}