package eu.europa.ec.leos.services.support.xml;


import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.PROVISO;
import static eu.europa.ec.leos.services.support.xml.AGEXmlTableOfContentHelper.extractOrBuildHeaderElement;
import static eu.europa.ec.leos.services.support.xml.AGEXmlTableOfContentHelper.extractOrBuildNumElement;
import static eu.europa.ec.leos.services.support.xml.AGEXmlTableOfContentHelper.navigateToFirstTocElment;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.getStartTag;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.insertOrUpdateAttributeValue;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.setupVTDNav;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.setupXMLModifier;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.toByteArray;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.updateOriginAttribute;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.getFragmentAsString;
import static eu.europa.ec.leos.services.support.xml.AGEVTDUtils.setupXMLModifier;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_DEPTH_ATTR;

import eu.europa.ec.leos.integration.ExternalDocumentProvider;

import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_ORIGIN_ATTR;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_SOFT_ACTION_ATTR;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.XMLID;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.ARTICLE;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.MREF;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.EC;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.INDENT;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.LEVEL;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.LIST;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.PARAGRAPH;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.POINT;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.RECITAL;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.REF;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.SUBPARAGRAPH;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.SUBPOINT;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.PERSON;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.ROLE;
import static eu.europa.ec.leos.services.support.xml.XmlTableOfContentHelper.extractIndexedNonTocElements;
import static eu.europa.ec.leos.services.support.xml.XmlTableOfContentHelper.extractLevelNonTocItems;
import static eu.europa.ec.leos.services.support.xml.XmlTableOfContentHelper.extractOrBuildHeaderElement;
import static java.nio.charset.StandardCharsets.UTF_8;
import eu.europa.ec.leos.services.support.xml.AGEXmlHelper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.google.common.base.Stopwatch;
import com.ximpleware.AutoPilot;
import com.ximpleware.ModifyException;
import com.ximpleware.NavException;
import com.ximpleware.TranscodeException;
import com.ximpleware.VTDNav;
import com.ximpleware.XMLModifier;
import com.ximpleware.XPathEvalException;
import com.ximpleware.XPathParseException;

import eu.europa.ec.leos.domain.common.InstanceType;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.instance.Instance;
import eu.europa.ec.leos.model.action.SoftActionType;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.services.support.ByteArrayBuilder;
import eu.europa.ec.leos.services.support.IdGenerator;
import eu.europa.ec.leos.services.support.xml.ref.Ref;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;
import eu.europa.ec.leos.vo.toc.NumberingConfig;
import eu.europa.ec.leos.vo.toc.OptionsType;
import eu.europa.ec.leos.vo.toc.TocItem;

@Primary
@Component
@Instance(instances = {InstanceType.COMMISSION, InstanceType.OS})
public class AGEVtdXmlContentProcessorForProposal extends AGEVtdXmlContentProcessor implements AGEXmlContentProcessor{
	
	private static final Logger LOG = LoggerFactory.getLogger(AGEVtdXmlContentProcessorForProposal.class);
	
	//RM-Correctivo #2902
    @Value("${leos.paragraph.templates.modify}")
    private String modifyTemplate;
	
	@Autowired
    protected MessageHelper messageHelper;
	
	@Autowired
	protected ExternalDocumentProvider externalDocumentProvider;

    @Override
    public byte[] createDocumentContentWithNewTocList(List<TableOfContentItemVO> tableOfContentItemVOs, byte[] content, User user) {
        LOG.trace("Start building the document content for the new toc list");
        long startTime = System.currentTimeMillis();
        try {
            List<TocItem> tocItems = structureContextProvider.get().getTocItems();
            List<NumberingConfig> numberingConfigs = structureContextProvider.get().getNumberingConfigs();
            Map<TocItem, List<TocItem>> tocRules = structureContextProvider.get().getTocRules();

            ByteArrayBuilder mergedContent = new ByteArrayBuilder();
            VTDNav contentNavigator = setupVTDNav(content);
            int docLength = content.length;
            int endOfContent = 0;

            if (!tableOfContentItemVOs.isEmpty() && navigateToFirstTocElment(tableOfContentItemVOs, contentNavigator)) {
                int index = contentNavigator.getCurrentIndex();

                // append everything up until the first toc element
                long contentFragment = contentNavigator.getElementFragment();
                int offset = (int) contentFragment;
                int length = (int) (contentFragment >> 32);
                mergedContent.append(contentNavigator.getXML().getBytes(0, offset));
                
                for (TableOfContentItemVO tocVo : tableOfContentItemVOs) {
                    index = tocVo.getVtdIndex();
                    mergedContent.append(buildTocItemContent(tocItems, numberingConfigs, tocRules, contentNavigator, tocVo, user));
                }
                
                contentNavigator.recoverNode(index);
                contentFragment = contentNavigator.getElementFragment();
                offset = (int) contentFragment;
                length = (int) (contentFragment >> 32);
                
                endOfContent = offset + length;
            }
            // append everything after the content
            mergedContent.append(contentNavigator.getXML().getBytes(endOfContent, docLength - (endOfContent)));

            LOG.trace("Build the document content for the new toc list completed in {} ms", (System.currentTimeMillis() - startTime));
            return mergedContent.getContent();

        } catch (Exception e) {
            LOG.error("Unable to save the Table of content item list", e);
            throw new RuntimeException("Unable to save the Table of content item list", e);
        }
    }
    
    @Override
    public List<String> getListElementIdByPath(byte[] xmlContent, String xPath) {
        List<String> elementsId = new ArrayList<>();
        Stopwatch watch = Stopwatch.createStarted();
        try {
            VTDNav vtdNav = setupVTDNav(xmlContent);
            AutoPilot autoPilot = new AutoPilot(vtdNav);
            autoPilot.selectXPath(xPath);
            while (autoPilot.evalXPath() != -1) {
				elementsId.add(vtdNav.toString(vtdNav.getAttrVal(XMLID)));
            }
        } catch (Exception e) {
            LOG.error("Unexpected error occurred while finding the element id by path", e);
        }
        LOG.trace("Found last element id in {} ms", watch.elapsed(TimeUnit.MILLISECONDS));
        return elementsId;
    }
    
    protected byte[] buildTocItemContent(List<TocItem> tocItems, List<NumberingConfig> numberingConfigs, Map<TocItem, List<TocItem>> tocRules, VTDNav contentNavigator, TableOfContentItemVO tableOfContentItemVO, User user)
            throws NavException, UnsupportedEncodingException, XPathParseException, XPathEvalException {
        ByteArrayBuilder tocItemContent = new ByteArrayBuilder();

        tocItemContent.append(extractOrBuildNumElement(contentNavigator, tableOfContentItemVO, messageHelper));
        tocItemContent.append(extractOrBuildHeaderElement(contentNavigator, tableOfContentItemVO));
        if (tableOfContentItemVO.getIntroTagIndex() != null) {
            tocItemContent.append(extractIndexedNonTocElements(contentNavigator, tableOfContentItemVO.getIntroTagIndex()));
        }

        for (TableOfContentItemVO child : tableOfContentItemVO.getChildItemsView()) {
            tocItemContent.append(buildTocItemContent(tocItems, numberingConfigs, tocRules, contentNavigator, child, user));
        }

        byte[] startTag = new byte[0];
        String tocTagName = tableOfContentItemVO.getTocItem().getAknTag().value();

        if (tableOfContentItemVO.getVtdIndex() != null) {
            contentNavigator.recoverNode(tableOfContentItemVO.getVtdIndex());
            startTag = getStartTag(contentNavigator);
            tocItemContent.append(extractLevelNonTocItems(tocItems, tocRules, contentNavigator));
            if (tableOfContentItemVO.getItemDepth() > 0) {
               startTag = insertOrUpdateAttributeValue(new StringBuilder(new String(startTag, "UTF-8")), LEOS_DEPTH_ATTR, tableOfContentItemVO.getItemDepth()).toString().getBytes(UTF_8);
            }
        } else if (tableOfContentItemVO.getChildItemsView().isEmpty()) {
            byte[] tocItemTag;
            if (tableOfContentItemVO.getItemDepth() > 0) {
            	if (!tableOfContentItemVO.getTocItem().getAknTag().name().toLowerCase().contentEquals(PROVISO)) {
            		tocItemTag = AGEXmlHelper.getTemplate(tableOfContentItemVO.getTocItem(), tableOfContentItemVO.getNumber(), tableOfContentItemVO.getHeading(), messageHelper).getBytes();
            	} else {
            		tocItemTag = AGEXmlHelper.getTemplate(tableOfContentItemVO.getTocItem(),tableOfContentItemVO.getNumberType() + " " + tableOfContentItemVO.getNumber(), tableOfContentItemVO.getHeading(), messageHelper).getBytes();
            	}
                tocItemTag = insertOrUpdateAttributeValue(new StringBuilder(new String(tocItemTag)), LEOS_DEPTH_ATTR, tableOfContentItemVO.getItemDepth()).toString().getBytes(UTF_8);
            } else {
                tocItemTag = AGEXmlHelper.getTemplate(tableOfContentItemVO.getTocItem(), tableOfContentItemVO.getNumber(), tableOfContentItemVO.getHeading(), messageHelper).getBytes(UTF_8);
            }
            if (tableOfContentItemVO.getTocItem().getItemHeading() == OptionsType.OPTIONAL) {
                tocItemTag = removeEmptyHeading(new String(tocItemTag, UTF_8)).getBytes(UTF_8);
            }
            return tocItemTag;
        } else {
            String startTagStr = "<" + tocTagName + " xml:id=\"" + IdGenerator.generateId(tocTagName.substring(0, 3), 7) + "\">";
            startTag = updateOriginAttribute(startTagStr.getBytes(UTF_8), tableOfContentItemVO.getOriginAttr());
        }
        return XmlHelper.buildTag(startTag, tocTagName.getBytes(UTF_8), tocItemContent.getContent());
    }

        
    @Override
    public byte[] replaceElementByTagNameAndId(byte[] xmlContent, String newContent, String tagName, String idAttributeValue) {
        LOG.trace("Start updating the tag {} having id {} with the updated content", tagName, idAttributeValue);
        long startTime = System.currentTimeMillis();
        byte[] updatedContent = null;

        try {
            VTDNav vtdNav = setupVTDNav(xmlContent);
            //Evolutivo #2546
            if (tagName.equalsIgnoreCase(ROLE) || tagName.equalsIgnoreCase(PERSON)) {
            	idAttributeValue = getParentElementId(xmlContent, tagName, idAttributeValue);
            }
            XMLModifier xmlModifier = setupXMLModifier(vtdNav, tagName, idAttributeValue);
            if (xmlModifier != null) {
                xmlModifier.remove();

                if (newContent != null) {
                    xmlModifier.insertBeforeElement(newContent.getBytes(UTF_8));
                }

                updatedContent = toByteArray(xmlModifier);
                updatedContent = doXMLPostProcessing(updatedContent);
            }
        } catch (Exception e) {
            throw new RuntimeException("Unexpected error occoured during updation of element", e);
        }
        LOG.trace("Tag content replacement completed in {} ms", (System.currentTimeMillis() - startTime));
        
        return updatedContent;
    }
    
    @Override
    public String getElementIdByPath(byte[] xmlContent, String xPath) {
        String elementId = null;
        Stopwatch watch = Stopwatch.createStarted();
        try {
            VTDNav vtdNav = setupVTDNav(xmlContent);
            AutoPilot autoPilot = new AutoPilot(vtdNav);
            autoPilot.selectXPath(xPath);
            while (autoPilot.evalXPath() != -1) {
                elementId = vtdNav.toString(vtdNav.getAttrVal(XMLID));
            }
        } catch (Exception e) {
            LOG.error("Unexpected error occoured while finding the element id by path", e);
        }
        LOG.trace("Found last element id in {} ms", watch.elapsed(TimeUnit.MILLISECONDS));
        return elementId;
    }

    @Override
    public ArrayList<String> doImportedMrefProjectPreProcessing(ArrayList<String[]> elementMref) {
    	ArrayList<String> arrayRef = new ArrayList<>();
    	VTDNav vtdNav;
    	for( int i = 0; i < elementMref.size(); i++ ) {
    		for( int j = 0; j < elementMref.get(i).length; j++ ) {
    			if (j == 2) {
    				try {
    					vtdNav =setupVTDNav(elementMref.get(i)[j].getBytes(StandardCharsets.UTF_8), false);
    					AutoPilot autoPilot = new AutoPilot(vtdNav);
    					autoPilot.selectXPath("//@href");

    					while(autoPilot.evalXPath() != -1) {
    						int idAttrValue = vtdNav.getAttrVal("href");
    						String idPrefix = new StringBuilder("").append( doImportedId(elementMref.get(i)[1])).toString();
    		    			String newIdAttrValue = IdGenerator.generateId(idPrefix, 9);
    		    			arrayRef.add(vtdNav.toNormalizedString(idAttrValue) + "-" + elementMref.get(i)[0] + "-" +newIdAttrValue);
    					}
    				} catch (Exception e) {
    					LOG.error("Unable to perform the doImportedElementProjectPreProcessing operation", e);
    					throw new RuntimeException("Unable to perform the doImportedElementProjectPreProcessing operation", e);
    				}
    			} 
    		}
    	}

    	return arrayRef;
    }
    
    private String extractIdModAmendmentModifi (byte[] xmlContent, String eliDestination) {
    	String idMod= "";
    	try {
    		VTDNav vtdNav =setupVTDNav(xmlContent, false);
    		AutoPilot autoPilot = new AutoPilot(vtdNav);
    		autoPilot.selectXPath("//destination[@href='" + eliDestination + "']");
    		if(autoPilot.evalXPath() != -1) {
    			vtdNav.toElement(VTDNav.PREV_SIBLING);
    			int idModNum = vtdNav.getAttrVal("href");
    			idMod = vtdNav.toNormalizedString(idModNum);
    		}   		
    	} catch (Exception e) {
    		LOG.error("Unable to perform the extractIdModAmendmentModifi operation", e);
    		throw new RuntimeException("Unable to perform the extractIdModAmendmentModifi operation", e);
    	}    	
    	return idMod;
    }
    
    @Override
    public String extractIdModAmendment (String xmlContent) {
    	String idMod= "";
    	try {
    		VTDNav vtdNav =setupVTDNav(xmlContent.getBytes(StandardCharsets.UTF_8), false);
    		AutoPilot autoPilot = new AutoPilot(vtdNav);
    		autoPilot.selectXPath("//mod");
    		if(autoPilot.evalXPath() != -1) {
    			int idModNum = vtdNav.getAttrVal("id");
    			idMod = vtdNav.toNormalizedString(idModNum);
    		}   		
    	} catch (Exception e) {
    		LOG.error("Unable to perform the extractIdModAmendment operation", e);
    		throw new RuntimeException("Unable to perform the extractIdModAmendment operation", e);
    	}   	
    	return idMod;
    }
    
    @Override
    public String equalActiveRefAmendment (byte[] xmlContentDocument, String nameNorm, String eliDestination) {
    	try {
    		boolean newActiveRef = true;
    		VTDNav vtdNav =setupVTDNav(xmlContentDocument, false);
    		AutoPilot autoPilot = new AutoPilot(vtdNav);
    		autoPilot.selectXPath("//activeRef");
    		while (autoPilot.evalXPath() != -1) {
    			int hrefEliNum = vtdNav.getAttrVal("href");
    			String hrefEli = vtdNav.toNormalizedString(hrefEliNum);
    			if (hrefEli.equals(eliDestination)) {
        			newActiveRef = false;
    			} 
    		}
    		if (newActiveRef) {
    			return AGEXmlHelper.getMetaReferenceAmendmentTemplateType1(nameNorm, eliDestination);
    		}
    	} catch (Exception e) {
    		LOG.error("Unable to perform the equalActiveRefAmendment operation", e);
    		throw new RuntimeException("Unable to perform the equalActiveRefAmendment operation", e);
    	}
    	
    	return null;
    }
    
    @Override
    public String getActiveRefAmendment (byte[] xmlContentDocument, String xpath) {
    	String href = "";
    	try {
    		VTDNav vtdNav =setupVTDNav(xmlContentDocument, false);
    		AutoPilot autoPilot = new AutoPilot(vtdNav);
    		autoPilot.selectXPath(xpath);
    		if (autoPilot.evalXPath() != -1) {
    			vtdNav.toElement(VTDNav.NEXT_SIBLING);
    			int hrefiNum = vtdNav.getAttrVal("href");
    			href = vtdNav.toNormalizedString(hrefiNum);
    		}
    	} catch (Exception e) {
    		LOG.error("Unable to perform the equalActiveRefAmendment operation", e);
    		throw new RuntimeException("Unable to perform the equalActiveRefAmendment operation", e);
    	}
    	
    	return href;
    }

	
	@Override
	public String[] typeActiveMoficationAmendmentMod(byte[] xmlContent, String idModNew, String actionType, String quotedStructureId, String eliDestination, String pos, String wid) {
		String [] template = new String[2];
		try {
			VTDNav vtdNavAct = setupVTDNav(xmlContent);
			AutoPilot autoPilotTextualMod= new AutoPilot(vtdNavAct);

			String xpath= "//textualMod/destination[@href='" + eliDestination + "']";
			autoPilotTextualMod.selectXPath(xpath);
			if (autoPilotTextualMod.evalXPath() != -1) {
				return null;
			}
			
			xpath= "//textualMod/destination[contains(@href, '" + eliDestination + "')]";
			autoPilotTextualMod.selectXPath(xpath);
			if (autoPilotTextualMod.evalXPath() != -1) {
				return null;
			} 
			String[] auxEliNew = eliDestination.split("/");
			String[] auxEliNewFin = auxEliNew[auxEliNew.length - 1].split("\\.");
			String sEliDestinationAux = eliDestination;
			int lenght;
			if (auxEliNewFin.length <= 3) {
				lenght = auxEliNewFin.length-1;
				for(int i= lenght; i>0; i--) {				
					sEliDestinationAux = sEliDestinationAux.replace(auxEliNewFin[i], "");
					
//					//prueba quito estas lineas a ver si así entra por el filtro. 
//					String aux = sEliDestinationAux.substring(sEliDestinationAux.length() - 1);
//					if (aux.equals(".")) {
//						sEliDestinationAux = sEliDestinationAux.substring(0, sEliDestinationAux.length()-1);   		
//					}
					xpath = "//textualMod/destination[@href='" + sEliDestinationAux + "']";
					autoPilotTextualMod.selectXPath(xpath);
					if (autoPilotTextualMod.evalXPath() != -1) {
						return null;
					}			
				}
			} else {
				lenght = auxEliNewFin.length-2;
				for(int i= lenght; i>0; i--) {		
					if (lenght == i) {
						sEliDestinationAux = sEliDestinationAux.replace(auxEliNewFin[i]+ "." + auxEliNewFin[i+1], "");
					} else {
						sEliDestinationAux = sEliDestinationAux.replace(auxEliNewFin[i], "");	
					}
					
					String aux = sEliDestinationAux.substring(sEliDestinationAux.length() - 1);
					if (aux.equals(".")) {
						sEliDestinationAux = sEliDestinationAux.substring(0, sEliDestinationAux.length()-1);   		
					}
					xpath = "//textualMod/destination[@href='" + sEliDestinationAux + "']";
					autoPilotTextualMod.selectXPath(xpath);
					if (autoPilotTextualMod.evalXPath() != -1) {
						return null;
					}			
				}
			}
			xpath = "//textualMod/destination[contains(@href, '" + sEliDestinationAux + "')]";
			autoPilotTextualMod.selectXPath(xpath);
			while (autoPilotTextualMod.evalXPath() != -1) {
				int href = vtdNavAct.getAttrVal("href");
				String hrefAux = vtdNavAct.toNormalizedString(href);		
				template[1] = extractIdModAmendmentModifi(xmlContent, hrefAux);
				template[0] = AGEXmlHelper.getMetaAnalysisAmendmentTemplateType2(eliDestination, actionType, template[1], quotedStructureId, pos, wid);				
				return	template;
			}				
			template[1] = idModNew;
			template[0] = AGEXmlHelper.getMetaAnalysisAmendmentTemplateType2(eliDestination, actionType, idModNew, quotedStructureId, pos, wid);				

			return template;
		} catch (Exception e) {
			LOG.error("Unable to perform the equalActiveRefAmendment operation", e);
			throw new RuntimeException("Unable to perform the equalActiveRefAmendment operation", e);
		}
	}
	
	
	@Override
	public String[] typeActiveMoficationAmendmentNew(byte[] xmlContent, String idMod, String actionType, String quotedStructureId, String eliDestination, String pos, String wid) {
		String [] template = new String[2];
		try {
			VTDNav vtdNav =setupVTDNav(xmlContent, false);
			AutoPilot autoPilot = new AutoPilot(vtdNav);
			autoPilot.selectXPath("//activeModifications");
			if (autoPilot.evalXPath() != -1) {
				template[0] = "activeModifications";				
				template[1] = AGEXmlHelper.getMetaAnalysisAmendmentTemplateType2(eliDestination, convertActionType(actionType),idMod, quotedStructureId, pos, wid);		
			}  else { //NO EXISTE NINGUN ARTÍCULO MODIFICATIVO PLANTILLA COMPLETA <analysis>
				template[0] = "//identification";
				template[1] = AGEXmlHelper.getMetaAnalysisAmendmentTemplateType1(eliDestination, convertActionType(actionType), idMod, quotedStructureId, pos, wid);
			}
			return	template;
		} catch (Exception e) {
			LOG.error("Unable to perform the equalActiveRefAmendment operation", e);
			throw new RuntimeException("Unable to perform the equalActiveRefAmendment operation", e);
		}
	}
	
	
	
	@Override
	public String createNewNumInsertAmendment  (String insertType, String elementSelect, String elementParent, String typeElementSelect) {
		String valueId = "";
		String numSeq = "";
		boolean nextSibling = false;
		try {
			VTDNav vtdNav = setupVTDNav(elementSelect.getBytes(StandardCharsets.UTF_8), false);
			AutoPilot autoPilot= new AutoPilot(vtdNav);
			autoPilot.selectXPath("//@id");
			if(autoPilot.evalXPath() != -1) {
				int idAttrValue = vtdNav.getAttrVal("id");
				valueId = vtdNav.toNormalizedString(idAttrValue);
			}
			
			vtdNav = setupVTDNav(elementSelect.getBytes(StandardCharsets.UTF_8), false);
			autoPilot= new AutoPilot(vtdNav);
			if (vtdNav.toElement(VTDNav.FIRST_CHILD)) {
				numSeq = vtdNav.toString((int) vtdNav.getContentFragment(),
						(int) (vtdNav.getContentFragment() >> 32)).trim();
			}
			if (numSeq.contains("bis")) {
				return numSeq;
			}
			if (elementParent != null) {
				elementParent = elementParent.replace("&nbsp;", " ");
				vtdNav = setupVTDNav(elementParent.getBytes(StandardCharsets.UTF_8), false);
				autoPilot= new AutoPilot(vtdNav);
				//xpath del elemento seleccionado utilizando el id en todo el articulo/proviso
				autoPilot.selectXPath("//" + typeElementSelect + "[@id='" + valueId + "']");
				if(autoPilot.evalXPath() != -1) {
					//preguntamos si tiene hermano, entonces bis, ter, quarter...etc
					if (vtdNav.toElement(VTDNav.NEXT_SIBLING)) {
						nextSibling = true;
					} 
				}				
				Pattern numLevel3 = Pattern.compile("^(\\d+.(º))"); //1.º
				Matcher matcherLevel3 = numLevel3.matcher(numSeq);
				if (matcherLevel3.find()) {
					if (nextSibling) { // bis, ter, quarter..
						return numSeq + " bis";
					} else { // es el último, recalcular
						int aux = Integer.parseInt(numSeq.replace(".º", ""));
						return String.valueOf(++aux) + ".º";
					}

				}				
				Pattern numLevel1 = Pattern.compile("(\\d+\\.)");
				Matcher matcherLevel1 = numLevel1.matcher(numSeq);
				if (matcherLevel1.find()) {
					if (nextSibling) { // bis, ter, quarter..
						return numSeq + " bis";
					} else { // es el último, recalcular
						int aux = Integer.parseInt(numSeq.replace(".", ""));
						return String.valueOf(++aux) + "." ;
					}

				}
				Pattern numLevel2 = Pattern.compile("^((\\w{1,2}|ñ)\\))");//level2 a)
				Matcher matcherLevel2 = numLevel2.matcher(numSeq.trim());
				if (matcherLevel2.find() || numSeq.startsWith("ñ")) {
					if (nextSibling) { // bis, ter, quarter..
						return numSeq.replace(")", " bis)");
					} else { // es el último, recalcular
						return  calculateNextNumberLetters(numSeq.replace(")", ""));
					}
				} 				
				Pattern numLevel4 = Pattern.compile("^((xc|xl|l?x{0,3})(ix|iv|v?i{0,3})\\))");//level i)
				Matcher matcherLevel4 = numLevel4.matcher(numSeq.trim());
				if (matcherLevel4.find()) {
					if (nextSibling) { // bis, ter, quarter..
						return numSeq.replace(")", " bis)");
					} else { // es el último, recalcular
						return  calculateNextNumberRomans(numSeq.replace(")", ""));
					}
				} 
			} else {
				return numSeq.replace(".", " bis");
			}		
	} catch (Exception e) {
			LOG.error("Unable to perform the createNewNumInsertAmendment operation", e);
			throw new RuntimeException("Unable to perform the createNewNumInsertAmendment operation", e);
		}
		return numSeq;
	}
	
	private String calculateNextNumberLetters (String num) {
		String arrayLetters[] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "aa", "ab",
				"ac", "ad", "ae", "af", "ag", "ah", "ai", "aj", "ak", "al", "am", "an", "añ", "ao", "ap", "aq", "ar", "as", "at", "au", "av", "aw", "ax", "ay", "az"};

		for (int i = 0; i < arrayLetters.length; i++) {
			if (arrayLetters[i].equals(num)) {
				return arrayLetters[i+1].toLowerCase() + ")";
			}
		}

		return null;
	}
	
	private String calculateNextNumberRomans (String num) {
		String [] numNivel4romanos = {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX"};

		for (int i = 0; i < numNivel4romanos.length; i++) {
			if (numNivel4romanos[i].equalsIgnoreCase(num)) {
				return numNivel4romanos[i+1].toLowerCase() + ")";
			}
		}
		return null;
	}  
	
	@Override
    public String convertActionType (String actionType) {
    	switch (actionType) {
		case "INSERT_TOP":
		case "INSERT_BOTTOM":
			return "insertion";
		case "REPLACE":
			return "substitution";
		case "DELETE":
			return "repeal";
		default:
			break;
		}
    	
    	return null;
    }
    
    @Override
    public String doImportedElementProjectPreProcessing(String xmlContent, String elementType , ArrayList<String> mrefId) {
    	LOG.trace("Start import article or proviso doImportedElementProjectPreProcessing ");
    	Stopwatch watch = Stopwatch.createStarted();
    	String updatedElement = null;
    	try {
    		VTDNav vtdNav =setupVTDNav(xmlContent.getBytes(StandardCharsets.UTF_8), false);
    		XMLModifier xmlModifier = new XMLModifier(vtdNav);
    		AutoPilot autoPilot = new AutoPilot(vtdNav);
    		autoPilot.selectXPath("//@id");
    		while(autoPilot.evalXPath() != -1) {
    			boolean insert = false;
    			int idAttrValue = vtdNav.getAttrVal("id");
    			String value = vtdNav.toNormalizedString(idAttrValue);
    			String[] mrefIdsAux;
    			for (String v: mrefId) {
    				mrefIdsAux = v.split("-");
    				if (mrefIdsAux[0].equals(value)) {
    					xmlModifier.updateToken(idAttrValue, mrefIdsAux[2].getBytes(UTF_8));
    					insert = true;
    				}
    			}
    			if (!insert) {
    				String idPrefix = new StringBuilder("").append( doImportedId(elementType)).toString();
    				String newIdAttrValue = IdGenerator.generateId(idPrefix, 9);
    				xmlModifier.updateToken(idAttrValue, newIdAttrValue.getBytes(UTF_8));
    			}
    		}
    		autoPilot.selectXPath("//@href");
    		while(autoPilot.evalXPath() != -1) {
    			int idAttrValue = vtdNav.getAttrVal("href");
    			String value = vtdNav.toNormalizedString(idAttrValue);
    			String[] mrefIdsAux;
    			for (String v: mrefId) {
    				mrefIdsAux = v.split("-");
    				if (mrefIdsAux[0].equals(value)) {
    					xmlModifier.updateToken(idAttrValue, mrefIdsAux[2].getBytes(UTF_8));
    				}
    			}
    		}
    		updatedElement = removeSelfClosingElements(new String(toByteArray(xmlModifier), UTF_8));
    	} catch (Exception e) {
    		LOG.error("Unable to perform the doImportedElementProjectPreProcessing operation", e);
    		throw new RuntimeException("Unable to perform the doImportedElementProjectPreProcessing operation", e);
    	}
    	LOG.trace("Finished doImportedElementProjectPreProcessing: TotalTime taken{}", watch.elapsed(TimeUnit.MILLISECONDS));
    	return updatedElement;
    }
    
    public String doImportedId (String type) {
    	String id = null;
    	switch (type) {
		case ARTICLE:
			id = "akn_art_imp";
			break;
		case RECITAL:
			id = "akn_rec_imp";
			break;
		case PROVISO:
			id = "akn_dis_imp";
			break;
		}
    	return id;
    }
    
    // LEOS-2639: replace XML self-closing tags not supported in HTML
    private String removeSelfClosingElements(String fragment) {
        String removeSelfClosingRegex = "<([^>^\\s]+)([^>]*)/>";
        return fragment.replaceAll(removeSelfClosingRegex, "<$1$2></$1>");
    }
    
    @Override
    public void specificInstanceXMLPostProcessing(XMLModifier xmlModifier) {
    }

    @Override
    public String[] getSplittedElement(byte[] xmlContent, String tagName, String idAttributeValue) {
        return null;
    }

    @Override
    public String[] getMergeOnElement(byte[] xmlContent, String content, String tagName, String idAttributeValue) {
        Map<String, String> attributes = getElementAttributesByPath(content.getBytes(UTF_8), "/" + tagName, false);
        if (isSoftDeletedOrMovedTo(attributes) || !isPContent(content, tagName)) {
            return null;
        }

        String[] mergeOnElement = getSiblingElement(xmlContent, tagName, idAttributeValue, Arrays.asList(tagName, LIST), true);
        if ((mergeOnElement == null) || ((mergeOnElement != null) &&
                (isSoftDeletedOrMovedTo(getElementAttributesByPath(mergeOnElement[2].getBytes(UTF_8), "/" + mergeOnElement[1], false)) ||
                        !isPContent(mergeOnElement[2], mergeOnElement[1])))) {
            return null;
        }

        if (!isProposalElement(attributes)) {
            String[] parentElement = getParentElement(xmlContent, mergeOnElement[1], mergeOnElement[0]);
            if (Arrays.asList(PARAGRAPH, LEVEL, POINT, INDENT).contains(parentElement[1]) && getChildElement(xmlContent, parentElement[1], parentElement[0], Arrays.asList(SUBPARAGRAPH, SUBPOINT, LIST), 3) == null) {
                return parentElement;
            }
        }

        return mergeOnElement;
    }
    
    private boolean isProposalElement(Map<String, String> attributes) {
        return ((attributes.get(LEOS_ORIGIN_ATTR) != null) && attributes.get(LEOS_ORIGIN_ATTR).equals(EC));
    }    
    
    private boolean isSoftDeletedOrMovedTo(Map<String, String> attributes) {
        return ((attributes.get(LEOS_SOFT_ACTION_ATTR) != null) && (attributes.get(LEOS_SOFT_ACTION_ATTR).equals(SoftActionType.DELETE.getSoftAction()) ||
                attributes.get(LEOS_SOFT_ACTION_ATTR).equals(SoftActionType.MOVE_TO.getSoftAction())));
    }
    
    private boolean isPContent(String content, String tagName) {
        return getElementContentFragmentByPath(content.getBytes(UTF_8), "/" + tagName + "/content/p", false) != null;
    }


    @Override
    public byte[] mergeElement(byte[] xmlContent, String content, String tagName, String idAttributeValue) {
        return null;
    }
    @Override
    public Map<String, List<String>> bloqueVersion (byte[] xmlContent) {
    	Map<String, List<String>> element =new LinkedHashMap<>();
    	List<String> auxP = null; 
    	String auxB = "";
    	String auxV = "";
    	String tipo = "";
    	int contBloque = 0;
    	String replaceAll = "<a[^>]+>|<a>|</a>|<em>|</em>|<ins>|</ins>|<strong>|</strong>|<img[^>]+>|<img>|</img>|<span[^>]+>|<span>|</span>|<thead>|<thead[^>]+>|</thead>|<col[^>]+>|<col>|</col>|<colgroup>|<colgroup[^>]+>|</colgroup>";
        Stopwatch watch = Stopwatch.createStarted();
        try {
            VTDNav vtdNavB = setupVTDNav(xmlContent);
            String xPath ="//bloque";
            AutoPilot autoPilotBloque = new AutoPilot(vtdNavB);
            autoPilotBloque.selectXPath(xPath);
            while (autoPilotBloque.evalXPath() != -1) {
            	contBloque ++;
            	boolean tabla = false;
            	auxB = getFragmentAsString(vtdNavB, vtdNavB.getElementFragment(), false);
            	tipo = contBloque + "-" + vtdNavB.toString(vtdNavB.getAttrVal("tipo"));
            	if (!tipo.contains("nota_inicial") && !tipo.contains("firma")) {
            	VTDNav vtdNavV = setupVTDNav(auxB.getBytes(StandardCharsets.UTF_8));
            	AutoPilot autoPilotV = new AutoPilot(vtdNavV);
            	xPath = "//version[last()]";
            	autoPilotV.selectXPath(xPath);
            	if (autoPilotV.evalXPath() != -1) {
            		auxP = new ArrayList<>();
            		auxV = getFragmentAsString(vtdNavV, vtdNavV.getElementFragment(), false);
            		VTDNav vtdNavP = setupVTDNav(auxV.getBytes(StandardCharsets.UTF_8));
            		List<String> textoTablas = new ArrayList<>();
            		List<String> textoPrevSiblings = new ArrayList<>();
            		int numTabla = 0;
            		VTDNav vtdNavTable = setupVTDNav(auxV.getBytes(StandardCharsets.UTF_8));
            		AutoPilot autoPilotTable = new AutoPilot(vtdNavTable);
            		xPath = "//table";
            		autoPilotTable.selectXPath(xPath);            		
                	while (autoPilotTable.evalXPath() != -1) {
                		tabla = true;
                		textoTablas.add(getFragmentAsString(vtdNavTable, vtdNavTable.getElementFragment(), false));
                		VTDNav vtdNavTableAtributesT = setupVTDNav(textoTablas.get(numTabla).getBytes(StandardCharsets.UTF_8));
                		XMLModifier xm = new XMLModifier(vtdNavTableAtributesT);
                		AutoPilot ap = new AutoPilot(vtdNavTableAtributesT);
                		ap.selectXPath("@*");// select all attr node of table element
                        int i=0;
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        while((i=ap.evalXPath())!=-1){
                            if (vtdNavTableAtributesT.toString(i).equals("class")) {
                            	 xm.removeAttribute(i); 
                            }
                        }
                        xm.output(baos);
                        textoTablas.set(numTabla,baos.toString("UTF-8"));                 
                        VTDNav vtdNavTableAtributesP = setupVTDNav(textoTablas.get(numTabla).getBytes(StandardCharsets.UTF_8));
                        xm = new XMLModifier(vtdNavTableAtributesP);
                		ap = new AutoPilot(vtdNavTableAtributesP);
                        ap.selectXPath("//p/@*"); // select all attr node of p element
                        i=0;
                        ByteArrayOutputStream baosP = new ByteArrayOutputStream();
                        while((i=ap.evalXPath())!=-1){
                        	xm.removeAttribute(i);
                        }
                        xm.output(baosP);
                        textoTablas.set(numTabla,baosP.toString("UTF-8"));
                        textoTablas.set(numTabla, textoTablas.get(numTabla).replaceAll("\\s+", " ").replaceAll(replaceAll, "").trim());
                		if (vtdNavTable.toElement(VTDNav.PREV_SIBLING)) {
                			textoPrevSiblings.add(getFragmentAsString(vtdNavTable, vtdNavTable.getElementFragment(), false));
                			textoPrevSiblings.set(numTabla, textoPrevSiblings.get(numTabla).replaceFirst("<[^>]+>", "").replaceFirst("</p>", ""));
                			textoPrevSiblings.set(numTabla, textoPrevSiblings.get(numTabla).replaceAll("\\s+", " ").trim());
                			textoPrevSiblings.set(numTabla, textoPrevSiblings.get(numTabla).replaceAll(replaceAll, ""));
                		}
            		numTabla++;
                	}
                	AutoPilot autoPilotP= new AutoPilot(vtdNavP);
                	xPath = "/version/p"; // sólo los p que cuegan directamente de versión
                	autoPilotP.selectXPath(xPath);
                	int numTabla2 = 0;
                	while (autoPilotP.evalXPath() != -1) {
                		String textoSibling = "No hay tablas en este <p>";
                		if ((textoPrevSiblings.size() > 0) && (numTabla2<textoPrevSiblings.size()) ) {
                			textoSibling = textoPrevSiblings.get(numTabla2);
                		}
                		String textoAux = getFragmentAsString(vtdNavP, vtdNavP.getElementFragment(), false);
                		if (!textoAux.startsWith("<p>")) {
                			String classP = vtdNavP.toString(vtdNavP.getAttrVal("class"));
                    		textoAux = textoAux.replaceFirst("<[^>]+>", "").replaceFirst("</p>", "");
                    		textoAux =textoAux.replaceAll("\\s+", " ").trim();
                    		textoAux = textoAux.replaceAll(replaceAll, "");
                    		if (!classP.contains("imagen")) {
                    			if (tabla && textoAux.equals(textoSibling)) {                    			
                    				classP = "tabla_parrafo";
                    				auxP.add(classP + "---" + textoAux);
                    				auxP.add("tabla" + "---" + textoTablas.get(numTabla2));
                    				numTabla2++;
                    			} else {
                    				auxP.add(classP + "---" + textoAux);
                    			}		
                    		}
                		}
                	}
                		element.put(tipo, auxP);
            	}
            }
            }
        } catch (Exception e) {
            LOG.error("Unexpected error occoured while finding the element id by path", e);
            throw new RuntimeException("Unable to perform the bloqueVersion operation", e);
        }
        LOG.trace("Found last element id in {} ms", watch.elapsed(TimeUnit.MILLISECONDS));
    	return element;
    }
    
    @Override
    public List<String> crearConsolidado (byte[] eli, String type) {
    	List<String> auxP = new ArrayList<>();
    	String aux = "";
    	String tipo = "";
    	String xPath = "";
    	Stopwatch watch = Stopwatch.createStarted();
    	try {
    		VTDNav vtdNavTexto = setupVTDNav(eli,false);
    		AutoPilot autoPilotTexto = new AutoPilot(vtdNavTexto);
    		if (type.contentEquals("ELI")){
    			xPath ="//texto";
    		} else {
    			xPath ="//TEXTO";
    		}
    		autoPilotTexto.selectXPath(xPath);
    		if (autoPilotTexto.evalXPath() != -1) {
    			aux = getFragmentAsString(vtdNavTexto, vtdNavTexto.getElementFragment(), false);
    			VTDNav vtdNavP = setupVTDNav(aux.getBytes(StandardCharsets.UTF_8));
    			AutoPilot autoPilotP= new AutoPilot(vtdNavP);
    			xPath = "//p";
    			autoPilotP.selectXPath(xPath);
    			while (autoPilotP.evalXPath() != -1) {
    				try {
	    				tipo = vtdNavP.toString(vtdNavP.getAttrVal("class"));
	    				String textoAux = getFragmentAsString(vtdNavP, vtdNavP.getElementFragment(), false);
	    				String textoRule = textoAux.replaceFirst("<p[^>]+>", "").replaceFirst("</p>", "");
	    				if (textoRule.startsWith("«")) {
	    					tipo= "parrafo";
	    				}
	    				auxP.add(tipo + "---" + textoAux);
    				}
    				catch (Exception e) {
    					LOG.debug("Line without typical structure");
    				}
    			}
    		} 		
    	} catch (Exception e) {
    		LOG.error("Unexpected error occoured while finding the element id by path", e);
    		throw new RuntimeException("Unable to perform the crearConsolidado operation", e);
    	}
    	LOG.trace("Found last element id in {} ms", watch.elapsed(TimeUnit.MILLISECONDS));
    	return auxP;
    }

	@Override
	public boolean getReferencedBOE(byte[] xmlContent, String elementType, String id, String numElement) {
		VTDNav vtdNav = setupVTDNav(xmlContent);		
		numElement = numElement.replaceAll("\\u00a0", " ");//UNICODE
		String numElementAux = numElement.toLowerCase().replace(".", "");
		String []numeroAux = numElementAux.split("\\s+");
		String xPathArticle ="//article";
		String xPathProviso ="//proviso";
		AutoPilot autoPilotA= new AutoPilot(vtdNav);
		AutoPilot autoPilotP= new AutoPilot(vtdNav);
		try {
			autoPilotA.selectXPath(xPathArticle);
			while (autoPilotA.evalXPath() != -1) {
				int attIndex = vtdNav.getAttrVal(XMLID);
				String elementId;
				if (attIndex != -1) {
					elementId = vtdNav.toString(attIndex);
					if (!id.equals(elementId)) {
						String textoAux = getFragmentAsString(vtdNav, vtdNav.getElementFragment(), false).toLowerCase();
						if (textoAux.contains(numElementAux) && !textoAux.contains(numElementAux + " de la ley")) {
							return true;
						} 
						Pattern pat = Pattern.compile("artículos .* " + numeroAux[1]);
						Matcher mat = pat.matcher(textoAux);
						if (mat.find()) {
							return true;
						}
					} 
				}
			}

			autoPilotP.selectXPath(xPathProviso);
			while (autoPilotP.evalXPath() != -1) {
				int attIndex = vtdNav.getAttrVal(XMLID);
				String elementId;
				if (attIndex != -1) {
					elementId = vtdNav.toString(attIndex);
					if (!id.equals(elementId)) {
						String textoAux = getFragmentAsString(vtdNav, vtdNav.getElementFragment(), false).toLowerCase();
						if (textoAux.contains(numElementAux) && !textoAux.contains(numElementAux + " de la ley") && !textoAux.contains("disposición derogatoria")) {
							return true;
						}
						Pattern pat = Pattern.compile("artículos .* " + numeroAux[1]);
						Matcher mat = pat.matcher(textoAux);
						if (mat.find()) {
							return true;
						}
					}	
				}
			}
		} catch (Exception e) {
			LOG.error("error in getReferencedBOE,",  e);
		}
		return false;
	}
	
	@Override
	public byte[] addExternalMetaReference(byte[] xmlContent, String eId, String hRef) {
    	String[] normInfo = getNormVersion(hRef);
    	String specificHref = normInfo[0];
    	String label = normInfo[1];
		String template = "<TLCReference xml:id=\"" + eId + "\" name=\"citation\" href=\"" + specificHref + "\" showAs=\"" + label + "\"/>";
		
		String lastTLCReferenceId = getElementIdByPath(xmlContent, "//TLCReference");

		return insertElementByTagNameAndId(xmlContent, template, "TLCReference", lastTLCReferenceId , true);
	}

	protected String findRefRefersTo(VTDNav vtdNav)  throws Exception {
        String refersTo;
		int currentIndex = vtdNav.getCurrentIndex();
        try {
            if (vtdNav.toElement(VTDNav.FIRST_CHILD, REF)) {
                int index = vtdNav.getAttrVal("refersTo");
                if (index != -1) {
                	refersTo = vtdNav.toString(index);
                }
                else {
                	return "refersTo in ref not found";
                }
            } else {
                return "ref no fount";
            }
        } finally {
            vtdNav.recoverNode(currentIndex);
        }
        return refersTo;		
	}
	
    protected List<List<String>> findTLCReferences(byte[] xmlContent) {
    	VTDNav vtdNav = setupVTDNav(xmlContent);
    	List<String> TLCIds = new ArrayList<>();
        List<String> TLCHrefs = new ArrayList<>();
        List<String> TLCnames = new ArrayList<>();
        List<String> TLCChecks= new ArrayList<>();
        List<List<String>> strArray = new ArrayList<List<String>>();
		AutoPilot autoPilotT= new AutoPilot(vtdNav);
		String xPathTLCReference ="//TLCReference";
        try {
			autoPilotT.selectXPath(xPathTLCReference);
			while (autoPilotT.evalXPath() != -1) {
				int attIndex = vtdNav.getAttrVal(XMLID);
				if (attIndex != -1) {
					TLCIds.add(vtdNav.toString(attIndex));
				}
				else {
					TLCIds.add("null");
				}
				int attIndex2 = vtdNav.getAttrVal("href");
				if (attIndex2 != -1) {
					TLCHrefs.add(vtdNav.toString(attIndex2));
				}
				else {
					TLCHrefs.add("null");
				}
				int attIndex3 = vtdNav.getAttrVal("name");
				if (attIndex3 != -1) {
					TLCnames.add(vtdNav.toString(attIndex3));
				}
				else {
					TLCnames.add("null");
				}
				TLCChecks.add("unknown");
			}
        } catch (Exception e) {
			LOG.error("TLCReferences not present in document");
		}        
        strArray.add(TLCIds);
        strArray.add(TLCHrefs);
        strArray.add(TLCnames);
        strArray.add(TLCChecks);
        return strArray;
    }
    
    @Override
    public byte[] appendElementInsideTagByXPathAmend(byte[] xmlContent, String xPath, String newContent, boolean asFirstChild) {
        XMLModifier xmlModifier = new XMLModifier();
        try {
            VTDNav vtdNav = setupVTDNav(xmlContent, true);
            AutoPilot autoPilot = new AutoPilot(vtdNav);
            autoPilot.declareXPathNameSpace("xml", "http://www.w3.org/XML/1998/namespace");
            autoPilot.declareXPathNameSpace("leos", "urn:eu:europa:ec:leos");
            autoPilot.selectXPath(xPath);
            if (autoPilot.evalXPath() != -1) {
                xmlModifier.bind(vtdNav);
                if (asFirstChild) {
                	 vtdNav.toElement(VTDNav.LAST_CHILD);
                	xmlModifier.insertAfterElement(newContent.getBytes(UTF_8)); 
                } else {
                	vtdNav.toElement(VTDNav.PARENT);
                    vtdNav.toElement(VTDNav.LAST_CHILD);
                	xmlModifier.insertAfterElement(newContent.getBytes(UTF_8));
                }
            } else {
                throw new IllegalArgumentException("No element found with path " + xPath);
            }
            return toByteArray(xmlModifier);
        } catch (XPathParseException | XPathEvalException | NavException | ModifyException | TranscodeException | IOException e) {
            throw new RuntimeException("Unable to perform the replace operation", e);
        }
    }
    
    @Override
    public byte[] appendElementInsideModAmend(byte[] xmlContent, String xPath, String newContent) {
    	XMLModifier xmlModifier = new XMLModifier();
    	try {
    		VTDNav vtdNav = setupVTDNav(xmlContent, true);
    		AutoPilot autoPilot = new AutoPilot(vtdNav);
    		autoPilot.declareXPathNameSpace("xml", "http://www.w3.org/XML/1998/namespace");
    		autoPilot.declareXPathNameSpace("leos", "urn:eu:europa:ec:leos");
    		autoPilot.selectXPath(xPath);
    		if (autoPilot.evalXPath() != -1) {
    			int p = vtdNav.getText(); 
    			xmlModifier.bind(vtdNav);
    			xmlModifier.updateToken(p, newContent); 

    		} else {
    			throw new IllegalArgumentException("No element found with path " + xPath);
    		}
    		return toByteArray(xmlModifier);
    	} catch (XPathParseException | XPathEvalException | NavException | ModifyException | TranscodeException | IOException e) {
    		throw new RuntimeException("Unable to perform the replace operation", e);
    	}
    }
    
    @Override
    public byte[] appendElementXPathAmend(byte[] xmlContent, String xPath, String newContent) {
        XMLModifier xmlModifier = new XMLModifier();
        try {
            VTDNav vtdNav = setupVTDNav(xmlContent, true);
            AutoPilot autoPilot = new AutoPilot(vtdNav);
            autoPilot.declareXPathNameSpace("xml", "http://www.w3.org/XML/1998/namespace");
            autoPilot.declareXPathNameSpace("leos", "urn:eu:europa:ec:leos");
            autoPilot.selectXPath(xPath);
            if (autoPilot.evalXPath() != -1) {
                xmlModifier.bind(vtdNav);
                xmlModifier.insertAfterElement(newContent.getBytes(UTF_8)); 

            } else {
                throw new IllegalArgumentException("No element found with path " + xPath);
            }
            return toByteArray(xmlModifier);
        } catch (XPathParseException | XPathEvalException | NavException | ModifyException | TranscodeException | IOException e) {
            throw new RuntimeException("Unable to perform the replace operation", e);
        }
    }
    @Override
    public String getIdModEditingModification (byte[] newXmlContent, String xPath) {
    	String idMod = null;
    	try {
    		VTDNav vtdNav = setupVTDNav(newXmlContent, true);
    		AutoPilot autoPilot = new AutoPilot(vtdNav);
    		autoPilot.declareXPathNameSpace("xml", "http://www.w3.org/XML/1998/namespace");
    		autoPilot.declareXPathNameSpace("leos", "urn:eu:europa:ec:leos");
    		autoPilot.selectXPath(xPath);
    		if (autoPilot.evalXPath() != -1) {
    			idMod = vtdNav.toString(vtdNav.getAttrVal(XMLID));
    		}
    		return idMod;
    	}catch (Exception e) {
    		throw new RuntimeException("Unable to perform the replace operation", e);
		}
    		
    }
    
    @Override
    public String getParentNumEditingModification (byte[] newXmlContent, String xPath) {
    	String href = null;
    	String parentNumS = "";
    	String [] parentNum;
    	try {
    		VTDNav vtdNav = setupVTDNav(newXmlContent, true);
    		AutoPilot autoPilot = new AutoPilot(vtdNav);
    		autoPilot.declareXPathNameSpace("xml", "http://www.w3.org/XML/1998/namespace");
    		autoPilot.declareXPathNameSpace("leos", "urn:eu:europa:ec:leos");
    		autoPilot.selectXPath(xPath);
    		if (autoPilot.evalXPath() != -1) {
    			if (vtdNav.toElement(VTDNav.NEXT_SIBLING)) {          		
            		int hrefNum = vtdNav.getAttrVal("href");
            		if (hrefNum != -1) {
            			href = vtdNav.toNormalizedString(hrefNum);
            			parentNum = href.split("/");
            			parentNumS = parentNum[parentNum.length -1];
            			parentNum = parentNumS.split("\\.");
            			parentNumS = parentNum[0];
            			if (parentNumS.contains("art_")) {
            				parentNumS = parentNumS.replace("art_", "artículo ");
            			} else {
            				parentNumS = parentNumS.replace("prov_", "disposición ");
            			}
            		}           		
            	}
    		}
    		return parentNumS;
    	}catch (Exception e) {
    		throw new RuntimeException("Unable to perform the replace operation", e);
		}
    		
    }
    
    @Override
    public byte[] deleteRefNormElementAmend(byte[] xmlContent, String xPath, String newFrameText) {
    	XMLModifier xmlModifier = new XMLModifier();
    	try {
    		VTDNav vtdNav = setupVTDNav(xmlContent, true);
    		AutoPilot autoPilot = new AutoPilot(vtdNav);
    		autoPilot.declareXPathNameSpace("xml", "http://www.w3.org/XML/1998/namespace");
    		autoPilot.declareXPathNameSpace("leos", "urn:eu:europa:ec:leos");
    		autoPilot.selectXPath(xPath);
    		if (autoPilot.evalXPath() != -1) {
    			xmlModifier.bind(vtdNav);
    			String textoAux = getFragmentAsString(vtdNav, vtdNav.getElementFragment(), false);
    			textoAux = textoAux.replaceAll(">(.*?)minos:", ">" + newFrameText + ", queda redactado en los siguientes términos:");
    			xmlModifier.remove();
    			if (textoAux != null) {
                    xmlModifier.insertBeforeElement(textoAux.getBytes(UTF_8));
                }
    		} else {
    			throw new IllegalArgumentException("No element found with path " + xPath);
    		}
    		return toByteArray(xmlModifier);
    	} catch (XPathParseException | XPathEvalException | NavException | ModifyException | TranscodeException | IOException e) {
    		throw new RuntimeException("Unable to perform the replace operation", e);
    	}
    }
    
    @Override
    public byte[] deleteElementByTagNameAndIdParentMod (byte[] xmlContent, String tagName, String idAttributeValue) {
        LOG.trace("Start deleting the tag {} having id {}", tagName, idAttributeValue);
        long startTime = System.currentTimeMillis();
        byte[] updatedContent;

        try {
            VTDNav vtdNav = setupVTDNav(xmlContent);
            XMLModifier xmlModifier = setupXMLModifier(vtdNav, tagName, idAttributeValue);
            vtdNav.toElement(VTDNav.PARENT);
           	vtdNav.toElement(VTDNav.PARENT);
        	vtdNav.toElement(VTDNav.PARENT);
            xmlModifier.remove();
          

            updatedContent = toByteArray(xmlModifier);
        } catch (Exception e) {
            throw new RuntimeException("Unexpected error occurred during deletion of element", e);
        }
        LOG.trace("Tag content replacement completed in {} ms", (System.currentTimeMillis() - startTime));

        return updatedContent;
    }
    
    
	@Override
	public int countSeveralMod(byte[] xmlContent, String xPath) {
		int cont = 0;
    	try {
		VTDNav vtdNav = setupVTDNav(xmlContent, true);
		AutoPilot autoPilot = new AutoPilot(vtdNav);
		autoPilot.declareXPathNameSpace("xml", "http://www.w3.org/XML/1998/namespace");
		autoPilot.declareXPathNameSpace("leos", "urn:eu:europa:ec:leos");
		autoPilot.selectXPath(xPath);
		while (autoPilot.evalXPath() != -1) {
    			cont ++;			
		}
    	}catch (Exception e) {
    		throw new RuntimeException("Unable to perform the replace operation", e);
		}
		return cont;
	}	
	
	@Override
	public int countSeveralQuoted(byte[] xmlContent, String xPath, String modId) {
		int cont = 0;
    	try {
		VTDNav vtdNav = setupVTDNav(xmlContent, true);
		AutoPilot autoPilot = new AutoPilot(vtdNav);
		autoPilot.declareXPathNameSpace("xml", "http://www.w3.org/XML/1998/namespace");
		autoPilot.declareXPathNameSpace("leos", "urn:eu:europa:ec:leos");
		autoPilot.selectXPath(xPath);
		while (autoPilot.evalXPath() != -1) {
    		int idNum = vtdNav.getAttrVal(XMLID);
    		String id = vtdNav.toNormalizedString(idNum);
    		if (!isActionRepeal(xmlContent, id)) {
    			cont++;
    		}
    		
		}
    	}catch (Exception e) {
    		throw new RuntimeException("Unable to perform the replace operation", e);
		}
		return cont;
	}	
	
	private boolean isActionRepeal (byte[] xmlContent, String quotedId ) {
		try {
			String xpathQuoted = "//new[@href='" + quotedId + "']";
			VTDNav vtdNav = setupVTDNav(xmlContent, true);
			AutoPilot autoPilot = new AutoPilot(vtdNav);
			autoPilot.declareXPathNameSpace("xml", "http://www.w3.org/XML/1998/namespace");
			autoPilot.declareXPathNameSpace("leos", "urn:eu:europa:ec:leos");
			autoPilot.selectXPath(xpathQuoted);
			if (autoPilot.evalXPath() != -1) {
				vtdNav.toElement(VTDNav.PARENT);
	    		int typeNum = vtdNav.getAttrVal("type");
	    		String type = vtdNav.toNormalizedString(typeNum);
	    		if (!type.equals("repeal")) {
	    			return false;
	    		} 			
			}
	    	}catch (Exception e) {
	    		throw new RuntimeException("Unable to perform the replace operation", e);
			}
		return true;
	}
	
    @Override
    public Map<String, List<String>> createFrameTextAmend(byte[] xmlContent, String idModOld) {
        List<String> auxHref;
        Map<String, List<String>> actionTypes = new HashMap<>();
        String actions[] = {"substitution","insertion","repeal"};
        
        int auxType;
        String sAuxType = null;        
        try {
            VTDNav vtdNav = setupVTDNav(xmlContent, true);
            AutoPilot autoPilot = new AutoPilot(vtdNav);
            autoPilot.declareXPathNameSpace("xml", "http://www.w3.org/XML/1998/namespace");
            autoPilot.declareXPathNameSpace("leos", "urn:eu:europa:ec:leos");
            for (int i = 0; i < actions.length; i++) {
            	String xPath = "//textualMod[@type='" + actions[i] + "']/source[@href='" + idModOld + "']";
                autoPilot.selectXPath(xPath);
                auxHref = new ArrayList<>();
                while (autoPilot.evalXPath() != -1) {
                	
                	while (vtdNav.toElement(VTDNav.NEXT_SIBLING)) {          		
                		int idWid = vtdNav.getAttrVal("leos:newNum");
                		if (idWid != -1) {
                			auxHref.add(vtdNav.toNormalizedString(idWid));
                		}           		
                	}
               	 if (vtdNav.toElement(VTDNav.PARENT)) {
                  	auxType = vtdNav.getAttrVal("type");
                  	sAuxType = vtdNav.toNormalizedString(auxType);
             	 }
               	
                }
                if (auxHref != null && !auxHref.isEmpty())
                actionTypes.put(sAuxType, auxHref);
			}
            
            return actionTypes;
        } catch (XPathParseException | XPathEvalException | NavException  e) {
            throw new RuntimeException("Unable to perform the createFrameTextAmend operation", e);
        }
    }
        
    @Override
    public String[] getNormVersion(String ELI) {
     	String[] arr = new String[2];
    	arr[0]="";
    	arr[1]="";
    	if (!ELI.contains("www.boe.es/eli")) {
    		arr[0]=ELI;
    		arr[1]="";
    		return arr;
    	}
    	
    	String documentELIXML= externalDocumentProvider.getDocumentBOEXmlDof(ELI);
    	byte[] xmlContent = documentELIXML.getBytes(StandardCharsets.UTF_8);
    	VTDNav vtdNav = setupVTDNav(xmlContent);
    	AutoPilot autoPilotT= new AutoPilot(vtdNav);
    	autoPilotT.declareXPathNameSpace("eli", "http://data.europa.eu/eli/ontology#");
    	VTDNav vtdNav2 = setupVTDNav(xmlContent);
    	AutoPilot autoPilotS= new AutoPilot(vtdNav2);
    	
    	String lastELI;
    	List<String> eliConsolidated= new ArrayList<>();
    	List<Integer> eliConsolidatedDates= new ArrayList<>();
		String inVtdNav = new String(xmlContent);
		String outVtdNav = new String(vtdNav.getXML().getBytes());
		
		String xPathTLCReference ="//eli:consolidated_by";
        try {
			autoPilotT.selectXPath(xPathTLCReference);
			while (autoPilotT.evalXPath() != -1) {
				int attIndex = vtdNav.getAttrVal("rdf:resource");
				if (attIndex != -1) {
					lastELI = vtdNav.toString(attIndex);
					eliConsolidated.add(lastELI);
					try {
						eliConsolidatedDates.add(Integer.parseInt(lastELI.substring(lastELI.lastIndexOf("/")+1)));
					}
					catch (NumberFormatException nfe) {
						eliConsolidatedDates.add(0);
					}
				}			
				else {
					eliConsolidated.add("null");
					eliConsolidatedDates.add(0);
				}
			}
        } catch (Exception e) {
			LOG.error("eli:consolidated_by element not present in document", e);
		}

        try {
	        autoPilotS.selectElement("titulo");
	        if (autoPilotS.iterate()) {
	            int text = vtdNav2.getText();
	            if (text != -1) {
	                arr[1] = vtdNav2.toNormalizedString(text);
	            }
	        }
	    } catch (NavException e) {
	        LOG.error("title element not present in document", e);
	    }
        
        if (eliConsolidated.size()>0) {
        	Integer dateCons = Collections.max(eliConsolidatedDates);
        	if (dateCons > 0) {
        		arr[0]= eliConsolidated.get(eliConsolidatedDates.indexOf(dateCons));
        		arr[1] = arr[1] + " - Consolidado publicado el " + dateCons;
        		return arr;
        	}
        	else {
        		arr[0]= ELI + "/dof";
        		arr[1] = arr[1] + " - Texto inicial";
        		return arr;
        	}
        }
        else {
    		arr[0]= ELI + "/dof";
    		arr[1] = arr[1] + " - Texto inicial";
    		return arr;
        }
    }
	
	@Override
    public AGEExtRefUpdateOutput updatedExternalMetaReferences(byte[] xmlContent) {
    	byte[] updatedContent = xmlContent;
    	VTDNav vtdNav = setupVTDNav(xmlContent);
    	List<String> RefersTo;
    	List<List<String>> TLCReferences = findTLCReferences(xmlContent);
        String sourceRef = getRef(vtdNav);
        AutoPilot autoPilot = new AutoPilot(vtdNav);
		String xPathMref ="//" + MREF;
		String iDoc = new String(xmlContent);
		List<String> listUpdates = new ArrayList<>();
        try {
			autoPilot.selectXPath(xPathMref);
			while (autoPilot.evalXPath() != -1) {
	                List<Ref> refs = findReferences(vtdNav, sourceRef);
	                if (!refs.isEmpty()) {
	                	String refId = refs.get(0).getId();
	                	String refString = getElementById(xmlContent,refId)[2];
	                	String refersTo = findRefRefersTo(vtdNav);
	                	String BaseHref = refs.get(0).getHref();
	                	String[] normInfo = getNormVersion(BaseHref);
	                	String TLCHref = normInfo[0];
	                	String showAs = normInfo[1];
	                	if (BaseHref.contains("http")) {
	                		// Comprobamos si el ref ya tiene su TLCReference vinculado a través de su refersTo                		
	                		if (TLCReferences.get(0).contains(refersTo)) {
	                			Integer TLCIdIndex = TLCReferences.get(0).indexOf(refersTo);
	                			String existingTLCHref = TLCReferences.get(1).get(TLCIdIndex);
	                			TLCReferences.get(3).set(TLCIdIndex,refId);
	                			// Si lo tiene, solo habrá que comprobar si está actualizado y sino actualizarlo
	                			if (!(TLCHref.equals(existingTLCHref))) {
	                				updatedContent = deleteElementByTagNameAndId(updatedContent,"TLCReference",refersTo);
	                				updatedContent = addExternalMetaReference(updatedContent,refersTo,BaseHref);
	                				listUpdates.add("Durante la redacción de esta norma se ha publicado en el BOE una nueva versión de la norma: " + showAs);
	                			}
	                		}
	                		else {
	                			// Si no lo tiene, añadiremos uno si no está o apuntaremos a uno existente
		                		// Si el TLCReference  no existe, crearlo utilizando como xml:id el refersTo del <ref>
		                		if (!TLCReferences.get(1).contains(TLCHref)) {	                			
		                			updatedContent = addExternalMetaReference(updatedContent,refersTo,BaseHref);
		                		}
		                		// Si el TLCReference SI existe, sustituir en el <ref> el refersTo por el xml:id del TLCReference
		                		else {
		                			Integer TLCindex = TLCReferences.get(1).indexOf(TLCHref);
		                			String newRefersTo=TLCReferences.get(0).get(TLCindex);
		                			String newRef = refString.replaceAll("refersTo=\"" + refersTo + "\"","refersTo=\"" + newRefersTo + "\"");
		                			TLCReferences.get(3).set(TLCindex,refId);
		                			updatedContent = replaceElementByTagNameAndId(updatedContent,newRef,REF,refs.get(0).getId());
		                		}
	                		}
	                	}
	                }
            }
        } catch (Exception ex) {
            LOG.error("error adding external references TLCReference,", ex);
        }
        
        for (int i = 0; i < TLCReferences.get(3).size(); i++) {
        	if ((TLCReferences.get(3).get(i).contentEquals("unknown")) && (TLCReferences.get(2).get(i).contentEquals("citation"))) {
        		updatedContent = deleteElementByTagNameAndId(updatedContent, "TLCReference", TLCReferences.get(0).get(i));
        	}
        }     
        return new AGEExtRefUpdateOutput(updatedContent,listUpdates);
    }

    
    @Override
    public byte[] deleteElementModifi(byte[] xmlContent, String tagName, String idAttributeValue) {
        LOG.trace("Start deleting the tag {} having id {}", tagName, idAttributeValue);
        long startTime = System.currentTimeMillis();
        byte[] updatedContent;

        try {
            VTDNav vtdNav = setupVTDNav(xmlContent);
            XMLModifier xmlModifier = setupXMLModifier(vtdNav, tagName, idAttributeValue);
            xmlModifier.remove();

            updatedContent = toByteArray(xmlModifier);

        } catch (Exception e) {
            throw new RuntimeException("Unexpected error occurred during deletion of element", e);
        }
        LOG.trace("Tag content replacement completed in {} ms", (System.currentTimeMillis() - startTime));

        return updatedContent;
    }
    
    @Override
    public byte[] deleteAllTextualMod  (byte[] newXmlContent, String xPath) {
    	long startTime = System.currentTimeMillis();
    	byte[] updatedContent;

    	try {
    		VTDNav vtdNav = setupVTDNav(newXmlContent);
    		XMLModifier xmlModifier;
    		xmlModifier = new XMLModifier();
    		xmlModifier.bind(vtdNav);
    		AutoPilot autoPilot = new AutoPilot(vtdNav);
    		autoPilot.selectXPath(xPath);
    		while (autoPilot.evalXPath() != -1) {
    			vtdNav.toElement(VTDNav.PARENT);
    			xmlModifier.remove();
    			if (!vtdNav.toElement(VTDNav.NEXT_SIBLING)) {
    				if (!vtdNav.toElement(VTDNav.PREV_SIBLING)) {
            			vtdNav.toElement(VTDNav.PARENT);
            			xmlModifier.remove();
            			vtdNav.toElement(VTDNav.PARENT);
            			xmlModifier.remove();
    				}
    			}
    		}
    		updatedContent = toByteArray(xmlModifier);

    	} catch (Exception e) {
    		throw new RuntimeException("Unexpected error occurred during deletion of element", e);
    	}
    	LOG.trace("Tag content replacement completed in {} ms", (System.currentTimeMillis() - startTime));

    	return updatedContent;

    }
    

}
