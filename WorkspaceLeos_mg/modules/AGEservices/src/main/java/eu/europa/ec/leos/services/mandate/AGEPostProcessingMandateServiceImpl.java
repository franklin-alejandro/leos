package eu.europa.ec.leos.services.mandate;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.common.ErrorCode;
import eu.europa.ec.leos.domain.common.Result;
import eu.europa.ec.leos.domain.vo.DocumentVO;
import eu.europa.ec.leos.services.support.xml.XmlContentProcessor;

import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_DELETABLE_ATTR;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_EDITABLE_ATTR;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_ORIGIN_ATTR;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.ARTICLE;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.BILL;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.BODY;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.CITATIONS;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.DOC;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.EC;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.RECITALS;

@Primary
@Service
public class AGEPostProcessingMandateServiceImpl extends PostProcessingMandateServiceImpl {

	 private final XmlContentProcessor xmlContentProcessor;
	
	AGEPostProcessingMandateServiceImpl(XmlContentProcessor xmlContentProcessor) {
		super(xmlContentProcessor);
		this.xmlContentProcessor = xmlContentProcessor;
	}

	@Override
	public Result<String> processMandate(DocumentVO documentVO) {
        if (documentVO.getCategory().equals(LeosCategory.PROPOSAL)) {
            for (DocumentVO doc : documentVO.getChildDocuments()) {
                try {
                    if (!doc.getCategory().equals(LeosCategory.PROPOSAL)) {
                        byte[] docContent = doc.getSource();
                        if (doc.getCategory().equals(LeosCategory.BILL)) {
                            
                            byte[] updatedDocContent = xmlContentProcessor.setAttributeForAllChildren(docContent, BILL, Collections.emptyList(), LEOS_ORIGIN_ATTR, EC);
                            updatedDocContent = xmlContentProcessor.setAttributeForAllChildren(updatedDocContent, BODY, Arrays.asList(ARTICLE),
                                    LEOS_DELETABLE_ATTR, "false");
                            updatedDocContent = xmlContentProcessor.setAttributeForAllChildren(updatedDocContent, BILL,
                                    Arrays.asList(CITATIONS, RECITALS, ARTICLE), LEOS_EDITABLE_ATTR, "false");
                            
                            doc.setSource(updatedDocContent);
                            
                            for (DocumentVO annex : doc.getChildDocuments()) {
                                byte[] annexContent = annex.getSource();
                                byte[] updatedDocContentAnnex = xmlContentProcessor.setAttributeForAllChildren(annexContent, DOC, Collections.emptyList(), LEOS_ORIGIN_ATTR, EC);
                                annex.setSource(updatedDocContentAnnex);
                            }
                            for (DocumentVO report : doc.getChildDocuments()) {
                                byte[] reportContent = report.getSource();
                                byte[] updatedDocContentReport = xmlContentProcessor.setAttributeForAllChildren(reportContent, DOC, Collections.emptyList(), LEOS_ORIGIN_ATTR, EC);
                                report.setSource(updatedDocContentReport);
                            }
                            for (DocumentVO cantoDorado : doc.getChildDocuments()) {
                                byte[] cantoDoradoContent = cantoDorado.getSource();
                                byte[] updatedDocContentCantoDorado = xmlContentProcessor.setAttributeForAllChildren(cantoDoradoContent, DOC, Collections.emptyList(), LEOS_ORIGIN_ATTR, EC);
                                cantoDorado.setSource(updatedDocContentCantoDorado);
                            }
                            for (DocumentVO main : doc.getChildDocuments()) {
                                byte[] mainContent = main.getSource();
                                byte[] updatedDocContentmain = xmlContentProcessor.setAttributeForAllChildren(mainContent, DOC, Collections.emptyList(), LEOS_ORIGIN_ATTR, EC);
                                main.setSource(updatedDocContentmain);
                            }
                        } else {
                            byte[] updatedDocContent = xmlContentProcessor.setAttributeForAllChildren(docContent, DOC, Collections.emptyList(), LEOS_ORIGIN_ATTR, EC);
                            doc.setSource(updatedDocContent);
                        }
                    }
                } catch (Exception e) {
                    return new Result<String>(e.getMessage(), ErrorCode.EXCEPTION);
                }
            }
        }
        return new Result<String>("OK", null);
    }
	
}
