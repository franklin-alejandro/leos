package eu.europa.ec.leos.services.support.xml.ref;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AGETreeNode extends TreeNode{

    private String type;        //tag name to used as type of tag
    private int depth;          //depth in xml tree
    private int siblingNumber;  //children order in xml
    private String identifier;  //xml Id of the node
    private String refId;       //xml Id of the node
    private String num;         //num associated with TreeNode in XML
    private int vtdIndex;       //vtd index of node
    private AGETreeNode parent;    //parent in TreeNode
    private String documentRef;
    private List<AGETreeNode> children = new ArrayList<>();
    private List<String> contents = new ArrayList<>();
    private String content;
    private boolean change = false;
	
	public AGETreeNode(String type, int depth, int siblingNumber, String identifier, String num, int vtdIndex,
			AGETreeNode parent, String documentRef) {
		super(type, depth, siblingNumber, identifier, num, vtdIndex, parent, documentRef);
	}
	

    public AGETreeNode(String type, int depth, int siblingNumber, String identifier, String num, String content, AGETreeNode parent, boolean change) {
    	super(type, depth, siblingNumber, identifier, num, 0, parent, null);
		this.type = type;
		this.depth = depth;
		this.siblingNumber = siblingNumber;
		this.identifier = identifier;
		this.num = num;
		this.parent = parent;
		this.content = content;
		this.change = change;
    }
    
    public List<String> getContents() {
		return contents;
	}

	public void setContents(String content) {
		this.contents.add(content);
	}


	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getType() {
        return type;
    }

    public int getDepth() {
        return depth;
    }

    public int getSiblingNumber() {
        return siblingNumber;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getNum() {
        return num;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public int getVtdIndex() {
        return vtdIndex;
    }

    public AGETreeNode getParent() {
        return parent;
    }

    public List<AGETreeNode> getAGEChildren() {
        return children;//Intentionally returning the ref
    }

    public void setParent(AGETreeNode parent) {
        this.parent = parent;
    }

    public void addChildren(AGETreeNode child) {
        this.children.add(child);
    }

    public void setNum(String num) {
		this.num = num;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public void setSiblingNumber(int siblingNumber) {
		this.siblingNumber = siblingNumber;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public void setVtdIndex(int vtdIndex) {
		this.vtdIndex = vtdIndex;
	}

	public void setChildren(List<AGETreeNode> children) {
		this.children = children;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TreeNode)) return false;
        AGETreeNode node = (AGETreeNode) o;
        return vtdIndex == node.vtdIndex &&
                Objects.equals(type, node.type) &&
                Objects.equals(identifier, node.identifier) &&
                Objects.equals(num, node.num);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, identifier, num, vtdIndex);
    }

    private String getDecoratedType(String type, String num) {
        if("-".equals(num)) {
            type = "indent";
        } else if(type.equals("alinea")) {
            type = "sub-point";
        }
        return type;
    }


}
