package eu.europa.ec.leos.services.support.xml;

import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.PROVISO;
import static eu.europa.ec.leos.services.support.xml.AGEXmlTableOfContentHelper.extractOrBuildHeaderElement;
import static eu.europa.ec.leos.services.support.xml.AGEXmlTableOfContentHelper.extractOrBuildNumElement;
import static eu.europa.ec.leos.services.support.xml.AGEVTDUtils.updateAttributeValue;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.EMPTY_STRING;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_ORIGIN_ATTR;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_SOFT_ACTION_ATTR;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_SOFT_ACTION_ROOT_ATTR;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_SOFT_DATE_ATTR;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_SOFT_MOVED_LABEL_ATTR;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_SOFT_MOVE_FROM;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_SOFT_MOVE_TO;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_SOFT_TRANS_FROM;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_SOFT_USER_ATTR;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.SOFT_DELETE_PLACEHOLDER_ID_PREFIX;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.SOFT_MOVE_PLACEHOLDER_ID_PREFIX;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.TOGGLED_TO_NUM;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.XMLID;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.getFragmentAsString;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.getStartTag;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.insertAffectedAttribute;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.removeAttribute;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.restoreOldId;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.setupVTDNav;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.toByteArray;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.updateOriginAttribute;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.updateSoftInfo;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.updateSoftTransFromAttribute;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.updateXMLIDAttribute;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.updateXMLIDAttributesInElementContent;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.LEOS_ORIGIN_ATTR_CN;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.LEOS_ORIGIN_ATTR_EC;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.ARTICLE;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.LIST;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.PARAGRAPH;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.POINT;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.RECITAL;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.SUBPARAGRAPH;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.SUBPOINT;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.CITATION;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.INDENT;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.LEVEL;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.RECITALS;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.PREAMBLE;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.CONTENT;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.DIVISION;
import static eu.europa.ec.leos.services.support.xml.XmlTableOfContentHelper.extractIndexedNonTocElements;
import static eu.europa.ec.leos.services.support.xml.XmlTableOfContentHelper.extractLevelNonTocItems;
import static eu.europa.ec.leos.services.support.xml.XmlTableOfContentHelper.getTagWithContent;
import static eu.europa.ec.leos.services.support.xml.XmlTableOfContentHelper.navigateToFirstTocElment;
import eu.europa.ec.leos.services.support.xml.AGEExtRefUpdateOutput;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.google.common.base.Stopwatch;
import com.ximpleware.AutoPilot;
import com.ximpleware.NavException;
import com.ximpleware.VTDNav;
import com.ximpleware.XMLModifier;
import com.ximpleware.XPathEvalException;
import com.ximpleware.XPathParseException;

import eu.europa.ec.leos.domain.common.InstanceType;
import eu.europa.ec.leos.domain.common.Result;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.instance.Instance;
import eu.europa.ec.leos.model.action.SoftActionType;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.services.content.ReferenceLabelService;
import eu.europa.ec.leos.services.support.ByteArrayBuilder;
import eu.europa.ec.leos.services.support.IdGenerator;
import eu.europa.ec.leos.services.support.xml.ref.Ref;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;
import eu.europa.ec.leos.vo.toc.NumberingConfig;
import eu.europa.ec.leos.vo.toc.NumberingType;
import eu.europa.ec.leos.vo.toc.TocItem;
import eu.europa.ec.leos.vo.toc.TocItemUtils;

@Primary
@Component
@Instance(InstanceType.COUNCIL)
public class AGEVtdXmlContentProcessorForMandate extends VtdXmlContentProcessorForMandate implements AGEXmlContentProcessor{
	
	private static final Logger LOG = LoggerFactory.getLogger(AGEVtdXmlContentProcessorForMandate.class);
	
	//RM-Correctivo #2902
    @Value("${leos.paragraph.templates.modify}")
    private String modifyTemplate;
	
	@Autowired
    protected MessageHelper messageHelper;
	
	@Override
    public byte[] createDocumentContentWithNewTocList(List<TableOfContentItemVO> tableOfContentItemVOs, byte[] content, User user) {
        LOG.trace("Start building the document content for the new toc list");
        long startTime = System.currentTimeMillis();
        try {
        	
            List<TocItem> tocItems = structureContextProvider.get().getTocItems();
            List<NumberingConfig> numberingConfigs = structureContextProvider.get().getNumberingConfigs();
            Map<TocItem, List<TocItem>> tocRules = structureContextProvider.get().getTocRules();

            ByteArrayBuilder mergedContent = new ByteArrayBuilder();
            VTDNav contentNavigator = setupVTDNav(content);
            int docLength = content.length;
            int endOfContent = 0;

            if (!tableOfContentItemVOs.isEmpty() && navigateToFirstTocElment(tableOfContentItemVOs, contentNavigator)) {
                int index = contentNavigator.getCurrentIndex();

                // append everything up until the first toc element
                long contentFragment = contentNavigator.getElementFragment();
                int offset = (int) contentFragment;
                int length = (int) (contentFragment >> 32);
                mergedContent.append(contentNavigator.getXML().getBytes(0, offset));

                for (TableOfContentItemVO tocVo : tableOfContentItemVOs) {
                    index = tocVo.getVtdIndex();
                    mergedContent.append(buildTocItemContent(tocItems, numberingConfigs, tocRules, contentNavigator, tocVo, user));
                }

                contentNavigator.recoverNode(index);
                contentFragment = contentNavigator.getElementFragment();
                offset = (int) contentFragment;
                length = (int) (contentFragment >> 32);

                endOfContent = offset + length;
            }
            // append everything after the content
            mergedContent.append(contentNavigator.getXML().getBytes(endOfContent, docLength - (endOfContent)));

            LOG.trace("Build the document content for the new toc list completed in {} ms", (System.currentTimeMillis() - startTime));
            return mergedContent.getContent();

        } catch (Exception e) {
            LOG.error("Unable to save the Table of content item list", e);
            throw new RuntimeException("Unable to save the Table of content item list", e);
        }
    }
	
//	//@Override
//    protected byte[] buildTocItemContent(List<TocItem> tocItems, List<NumberingConfig> numberingConfigs, Map<TocItem, List<TocItem>> tocRules, VTDNav contentNavigator, TableOfContentItemVO tocItemVO, User user)
//            throws NavException, UnsupportedEncodingException, XPathParseException, XPathEvalException {
//
//    	// LEOS-AGE take template name
//		String leosTemplate = "";
//		contentNavigator.push();
//    	AutoPilot ap = new AutoPilot();
//    	ap.declareXPathNameSpace("leos", "urn:eu:europa:ec:leos");
//        ap.selectXPath("/akomaNtoso/bill/meta/proprietary/leos:template");
//        ap.bind(contentNavigator);
//        while ((ap.evalXPath()) != -1) {
//        	int val = contentNavigator.getText();
//            if (val != -1) {
//            	leosTemplate = contentNavigator.toNormalizedString(val);
//            }
//        }			
//      	contentNavigator.pop();
//    	//
//      	String tocTagName = tocItemVO.getTocItem().getAknTag().value();
//        ByteArrayBuilder tocItemContent = new ByteArrayBuilder();
//        if (!(tocTagName.equals(PARAGRAPH) && skipParagraphContent(tocItemVO)) &&
//                !(tocTagName.equals(POINT) && skipPointContent(tocItemVO))) {
//            byte[] numTag = extractOrBuildNumElement(contentNavigator, tocItemVO, messageHelper);
//
//            //this method does the num toggle processing
//            numTag = numberElementToggleProcessing(tocItemVO, numTag);
//
//            tocItemContent.append(numTag);
//        }
//        tocItemContent.append(extractOrBuildHeaderElement(contentNavigator, tocItemVO));
//        if (tocItemVO.getPreambleFormula1TagIndex() != null) {
//            tocItemContent.append(extractIndexedNonTocElements(contentNavigator, tocItemVO.getPreambleFormula1TagIndex()));
//        }
//        if (tocItemVO.getRecitalsIntroIndex() != null) {
//            tocItemContent.append(extractIndexedNonTocElements(contentNavigator, tocItemVO.getRecitalsIntroIndex()));
//        }
//
//        for (TableOfContentItemVO child : tocItemVO.getChildItemsView()) {
//            tocItemContent.append(buildTocItemContent(tocItems, numberingConfigs, tocRules, contentNavigator, child, user));
//        }
//
//        byte[] startTag = new byte[0];
//        int configuredIndentNumConfigDepth = TocItemUtils.getDepthByNumberingType(numberingConfigs, NumberingType.INDENT);
//        if (tocItemVO.getVtdIndex() != null) {
//            contentNavigator.recoverNode(tocItemVO.getVtdIndex());
//            if ((tocTagName.equals(PARAGRAPH) || tocTagName.equals(LEVEL)) && skipParagraphContent(tocItemVO)) {
//                startTag = updateOriginAttribute(getStartTag(contentNavigator), tocItemVO.getOriginAttr());
//                tocItemContent = buildParagraphContent(contentNavigator, tocItemVO, user, tocItemContent);
//            } else if (tocTagName.equals(POINT) || tocTagName.equals(INDENT)){
//            	boolean isIndent  = getPointDepthInToc(tocItemVO,1) == configuredIndentNumConfigDepth ;
//                startTag = updateOriginAttribute(getStartTag(contentNavigator), tocItemVO.getOriginAttr());
//                if (skipPointContent(tocItemVO)) {
//                    tocItemContent = buildPointContent(contentNavigator, tocItemVO, user, tocItemContent);
//                } else if (shouldWrapWithList(tocItemVO.getParentItem())) {
//                    tocItemContent.append(tocItemVO.getContent().getBytes(UTF_8));
//                    //byte[] pointTag = wrapWithPoint(tocItemVO, user, tocItemContent.getContent());
//                    byte[] pointTag = wrapWithPoint(tocItemVO, tocItemContent.getContent(), isIndent);
//
//                    pointTag = updateSoftInfo(pointTag, tocItemVO.getSoftActionAttr(), tocItemVO.isSoftActionRoot(), user,
//                    		tocItemVO.getOriginAttr(), tocItemVO.getSoftMoveFrom(), false, tocItemVO.getTocItem());
//                    return constructListStructure(tocItemVO, user, pointTag);
//                } else {
//                    startTag = buildExistingXmlNode(tocItems, tocRules, contentNavigator, tocItemVO, tocItemContent);
//                }
//            } else if ((tocTagName.equals(SUBPARAGRAPH) ||
//            		tocTagName.equals(SUBPOINT)) && isSingleSubElement(tocItemVO) &&
//                    !isSoftDeletedOrMoved(tocItemVO)) {
//                return extractSubElementContent(contentNavigator, tocItemVO);
//            } else if (tocTagName.equals(LIST) && isEmptyElement(tocItemVO)) {
//                return "".getBytes(UTF_8); // remove list content if there is no child
//            } else {
//                startTag = buildExistingXmlNode(tocItems, tocRules, contentNavigator, tocItemVO, tocItemContent);
//            }
//
//            if (SoftActionType.MOVE_TO.equals(tocItemVO.getSoftActionAttr())) {
//                startTag = updateXMLIDAttribute(startTag, tocItemVO.getId());
//                tocItemContent = updateXMLIDAttributesInElementContent(tocItemContent, SOFT_MOVE_PLACEHOLDER_ID_PREFIX, false);
//            } else if (SoftActionType.DELETE.equals(tocItemVO.getSoftActionAttr())) {
//                startTag = updateXMLIDAttribute(startTag, tocItemVO.getId());
//                tocItemContent = updateXMLIDAttributesInElementContent(tocItemContent, SOFT_DELETE_PLACEHOLDER_ID_PREFIX, false);
//            } else if ((tocTagName.equals(PARAGRAPH) || tocTagName.equals(POINT)) &&
//                    !isEmptyElement(tocItemVO) && isSingleSubElement(tocItemVO.getChildItems().get(0)) &&
//                    !isSoftDeletedOrMoved(tocItemVO.getChildItems().get(0))) {
//                startTag = updateSoftTransFromAttribute(startTag, tocItemVO.getChildItems().get(0).getId());
//            }
//        } else if (tocTagName.equals(CITATION)) {
//            byte[] citationTag = AGEXmlHelper.getCitationTemplate(messageHelper).getBytes(UTF_8);
//            citationTag = updateSoftInfo(citationTag, tocItemVO.getSoftActionAttr(), tocItemVO.isSoftActionRoot(), user,
//            		tocItemVO.getOriginAttr(), null, tocItemVO.isUndeleted(), tocItemVO.getTocItem());
//            return updateOriginAttribute(citationTag, tocItemVO.getOriginAttr());
//        } else if (tocTagName.equals(RECITAL)) {
//            byte[] recitalTag = AGEXmlHelper.getRecitalTemplate(tocItemVO.getNumber(),messageHelper).getBytes(UTF_8);
//            recitalTag = updateSoftInfo(recitalTag, tocItemVO.getSoftActionAttr(), tocItemVO.isSoftActionRoot(), user,
//            		tocItemVO.getOriginAttr(), null, tocItemVO.isUndeleted(), tocItemVO.getTocItem());
//            return updateOriginAttribute(recitalTag, tocItemVO.getOriginAttr());
//        } else if (tocTagName.equals(ARTICLE)) {
//            if(tocItemVO.getChildItemsView().isEmpty()) {
//                byte[] articleTag = AGEXmlHelper.getArticleTemplate(tocItemVO.getNumber(), tocItemVO.getHeading(), leosTemplate, messageHelper, modifyTemplate).getBytes(UTF_8);
//                articleTag = updateSoftInfo(articleTag, tocItemVO.getSoftActionAttr(), tocItemVO.isSoftActionRoot(), user,
//                		tocItemVO.getOriginAttr(), null, tocItemVO.isUndeleted(), tocItemVO.getTocItem());
//                return updateOriginAttribute(articleTag, tocItemVO.getOriginAttr());
//            } else {
//                String startTagStr = "<" + tocTagName + " xml:id=\"" + IdGenerator.generateId(tocTagName.substring(0, 3), 7)
//                        + "\" leos:editable=\"true\"  leos:deletable=\"true\">";
//                startTag = updateOriginAttribute(startTagStr.getBytes(UTF_8), tocItemVO.getOriginAttr());
//            }
//        } else if (tocTagName.equals(PROVISO)) {
//            if(tocItemVO.getChildItemsView().isEmpty()) {
//                byte[] dispTag = AGEXmlHelper.getDispositionTemplate(tocItemVO.getNumberType(), tocItemVO.getNumber(), tocItemVO.getHeading(), leosTemplate, messageHelper).getBytes(UTF_8);
//                dispTag = updateSoftInfo(dispTag, tocItemVO.getSoftActionAttr(), tocItemVO.isSoftActionRoot(), user,
//                		tocItemVO.getOriginAttr(), null, tocItemVO.isUndeleted(), tocItemVO.getTocItem());
//                return updateOriginAttribute(dispTag, tocItemVO.getOriginAttr());
//            } else {
//                String startTagStr = "<" + tocTagName + " xml:id=\"" + IdGenerator.generateId(tocTagName.substring(0, 3), 7)
//                        + "\" leos:editable=\"true\"  leos:deletable=\"true\">";
//                startTag = updateOriginAttribute(startTagStr.getBytes(UTF_8), tocItemVO.getOriginAttr());
//            }
//        } else if (tocTagName.equals(PARAGRAPH)) {
//            if (tocItemVO.getChildItemsView().isEmpty()) {
//                byte[] paragraphTag = AGEXmlHelper.getParagraphTemplate(tocItemVO.getNumber(), messageHelper).getBytes(UTF_8);
//                paragraphTag = updateSoftInfo(paragraphTag, tocItemVO.getSoftActionAttr(), tocItemVO.isSoftActionRoot(), user,
//                		tocItemVO.getOriginAttr(), null, tocItemVO.isUndeleted(), tocItemVO.getTocItem());
//                return updateOriginAttribute(paragraphTag, tocItemVO.getOriginAttr());
//            } else {
//                String startTagStr = "<" + tocTagName + " xml:id=\"" + IdGenerator.generateId(tocTagName.substring(0, 3), 7) + "\">";
//                startTag = updateOriginAttribute(startTagStr.getBytes(UTF_8), tocItemVO.getOriginAttr());
//                tocItemContent = buildParagraphContent(contentNavigator, tocItemVO, user, tocItemContent);
//            }
//        } else if (tocTagName.equals(SUBPARAGRAPH)) {
//            byte[] subParagraphTag = AGEXmlHelper.getSubParagraphTemplate(null, messageHelper).getBytes(UTF_8);
//            subParagraphTag = updateSoftInfo(subParagraphTag, tocItemVO.getSoftActionAttr(), tocItemVO.isSoftActionRoot(), user,
//            		tocItemVO.getOriginAttr(), null, tocItemVO.isUndeleted(), tocItemVO.getTocItem());
//            return updateOriginAttribute(subParagraphTag, tocItemVO.getOriginAttr());
//        } else if (tocTagName.equals(POINT)) {
//            if (tocItemVO.getChildItemsView().isEmpty()) {
//                byte[] pointTag = AGEXmlHelper.getPointTemplate(tocItemVO.getNumber(), messageHelper).getBytes(UTF_8);
//                pointTag = updateSoftInfo(pointTag, tocItemVO.getSoftActionAttr(), tocItemVO.isSoftActionRoot(), user,
//                		tocItemVO.getOriginAttr(), null, tocItemVO.isUndeleted(), tocItemVO.getTocItem());
//                pointTag = updateOriginAttribute(pointTag, tocItemVO.getOriginAttr());
//
//                return constructListStructure(tocItemVO, user, pointTag);
//            } else {
//                byte[] pointTag = buildPointContent(contentNavigator, tocItemVO, user, tocItemContent).getContent();
//                boolean isIndent = getPointDepthInToc(tocItemVO, 1) == configuredIndentNumConfigDepth;
//                //pointTag = wrapWithPoint(tocItemVO, user, pointTag);
//                pointTag = wrapWithPoint(tocItemVO, tocItemContent.getContent(), isIndent);
//                pointTag = updateSoftInfo(pointTag, tocItemVO.getSoftActionAttr(), tocItemVO.isSoftActionRoot(), user,
//                		tocItemVO.getOriginAttr(), null, tocItemVO.isUndeleted(), tocItemVO.getTocItem());
//                return constructListStructure(tocItemVO, user, pointTag);
//            }
//        } else if (tocTagName.equals(DIVISION)) {//CUARENTENA
//            byte[] annexTag = AGEXmlHelper.getAnnexTemplate(messageHelper).getBytes(UTF_8);
//            annexTag = updateSoftInfo(annexTag, tocItemVO.getSoftActionAttr(), tocItemVO.isSoftActionRoot(), user,
//                    tocItemVO.getOriginAttr(), tocItemVO.getSoftMoveFrom(), false, tocItemVO.getTocItem());
//            return updateOriginAttribute(annexTag, tocItemVO.getOriginAttr());
//            
//        } else if (tocTagName.equals(DIVISION)) {//CUARENTENA
//            byte[] reportTag = AGEXmlHelper.getReportTemplate(messageHelper).getBytes(UTF_8);
//            reportTag = updateSoftInfo(reportTag, tocItemVO.getSoftActionAttr(), tocItemVO.isSoftActionRoot(), user,
//                    tocItemVO.getOriginAttr(), tocItemVO.getSoftMoveFrom(), false, tocItemVO.getTocItem());
//            return updateOriginAttribute(reportTag, tocItemVO.getOriginAttr());
//        } else {
//            String startTagStr = "<" + tocTagName + " xml:id=\"" + IdGenerator.generateId(tocTagName.substring(0, 3), 7) + "\">";
//            startTag = updateOriginAttribute(startTagStr.getBytes(UTF_8), tocItemVO.getOriginAttr());
//        }
//        
//        final String moveId;
//        if (tocItemVO.getSoftActionAttr() != null && tocItemVO.getSoftActionAttr().equals(SoftActionType.MOVE_TO)) {
//            moveId = tocItemVO.getSoftMoveTo();
//        } else if (tocItemVO.getSoftActionAttr() != null && tocItemVO.getSoftActionAttr().equals(SoftActionType.MOVE_FROM)) {
//            moveId = tocItemVO.getSoftMoveFrom();
//        } else {
//            moveId = null;
//        }
//        startTag = updateSoftInfo(startTag, tocItemVO.getSoftActionAttr(), tocItemVO.isSoftActionRoot(), user,
//        		tocItemVO.getOriginAttr(), moveId, tocItemVO.isUndeleted(), tocItemVO.getTocItem());
//        
//        startTag = insertAffectedAttribute(startTag, tocItemVO.isAffected());
//        return XmlHelper.buildTag(startTag, tocTagName.getBytes(UTF_8), tocItemContent.getContent());
//    }
	
	private ByteArrayBuilder buildParagraphContent(VTDNav contentNavigator, TableOfContentItemVO tableOfContentItemVO, User user,
            ByteArrayBuilder tocItemContent)
            throws NavException, UnsupportedEncodingException {
        ByteArrayBuilder composedContent = new ByteArrayBuilder();
        composedContent.append(extractOrBuildNumElement(contentNavigator, tableOfContentItemVO, messageHelper));
        composedContent.append(convertToSubparagraph(tableOfContentItemVO, user));
        composedContent.append(tocItemContent.getContent());
        return composedContent;
    }

    private byte[] convertToSubparagraph(TableOfContentItemVO tableOfContentItemVO, User user) throws UnsupportedEncodingException {
        byte[] subParaTag = AGEXmlHelper.getSubParagraphTemplate(tableOfContentItemVO.getContent(), messageHelper).getBytes(UTF_8);
        return updateSoftInfo(subParaTag, SoftActionType.ADD, Boolean.TRUE, user, LEOS_ORIGIN_ATTR_CN, null, false, tableOfContentItemVO.getTocItem());
    }

    private  ByteArrayBuilder buildPointContent(VTDNav contentNavigator, TableOfContentItemVO tableOfContentItemVO, User user,
            ByteArrayBuilder tocItemContent)
            throws NavException, UnsupportedEncodingException {
        ByteArrayBuilder composedContent = new ByteArrayBuilder();
        composedContent.append(extractOrBuildNumElement(contentNavigator, tableOfContentItemVO, messageHelper));
        composedContent.append(convertToSubpoint(tableOfContentItemVO, user));
        composedContent.append(tocItemContent.getContent());
        return composedContent;
    }

    private byte[] convertToSubpoint(TableOfContentItemVO tableOfContentItemVO, User user) throws UnsupportedEncodingException {
        byte[] subPointTag = AGEXmlHelper.getSubpointTemplate(tableOfContentItemVO.getContent(), messageHelper).getBytes(UTF_8);
        return updateSoftInfo(subPointTag, SoftActionType.ADD, Boolean.TRUE, user, LEOS_ORIGIN_ATTR_CN, null, false, tableOfContentItemVO.getTocItem());
    }
    
    /* protected */
    @Override
	protected boolean skipParagraphContent(TableOfContentItemVO tableOfContentItemVO) {
        List<TableOfContentItemVO> childList = tableOfContentItemVO.getChildItems();
        if (childList != null && !childList.isEmpty()) {
            for (TableOfContentItemVO child : childList) {
                if ((child.getVtdIndex() != null) && !child.isMovedOnEmptyParent() && child.getTocItem().getAknTag().value().equals(SUBPARAGRAPH)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    /* protected */
    @Override
	protected  byte[] numberElementToggleProcessing(TableOfContentItemVO tableOfContentItemVO, byte[] numTag) {
        if (tableOfContentItemVO.getTocItem().getAknTag().value().equals(PARAGRAPH)) {
            if (tableOfContentItemVO.getParentItem().isNumberingToggled() != null) {
                if (tableOfContentItemVO.getParentItem().isNumberingToggled()) {
                    if (isNumberSoftDeleted(tableOfContentItemVO)) {// if a para is soft deleted and numbering is toggled
                        numTag = updateSoftActionOnNumElement(new String(numTag), null, TOGGLED_TO_NUM);
                    } else {
                        numTag = updateSoftActionOnNumElement(new String(numTag), SoftActionType.ADD, TOGGLED_TO_NUM);
                    }
                } else {
                    numTag = updateSoftActionOnNumElement(new String(numTag), SoftActionType.DELETE, null);
                }
            } else if (tableOfContentItemVO.getNumSoftActionAttr() != null){//in case paragraph is moved which was toggled to num before, removing soft attributes from num element
                    numTag = updateSoftActionOnNumElement(new String(numTag), tableOfContentItemVO.getNumSoftActionAttr(), "");
            } else {
                numTag = updateSoftActionOnNumElement(new String(numTag), null, "");
            }
        }
        return numTag;
    }

    /* protected */
    @Override
	protected  byte[] updateSoftActionOnNumElement(String content,SoftActionType softAction, String setToggledToNum) {
        StringBuilder tagStr = new StringBuilder(content);

        if (softAction != null) {
            updateSoftAttributes(softAction, tagStr);
            if(SoftActionType.DELETE.equals(softAction)){
                tagStr = new StringBuilder(new String(updateXMLIDAttributesInElementContent(new ByteArrayBuilder(tagStr.toString().getBytes(UTF_8)), SOFT_DELETE_PLACEHOLDER_ID_PREFIX, true).getContent(), UTF_8));
            }
        } else if (setToggledToNum != null) {
            removeSoftAttributes(tagStr);
        }

        if(TOGGLED_TO_NUM.equals(setToggledToNum)){
            updateAttributeValue(tagStr, setToggledToNum, Boolean.TRUE.toString());
        }

        return new String(tagStr).getBytes(UTF_8);
    }
    
    /* protected */
    @Override
	protected void removeSoftAttributes(StringBuilder tagStr) {
        removeAttribute(tagStr, LEOS_SOFT_ACTION_ATTR);
        removeAttribute(tagStr, LEOS_SOFT_ACTION_ROOT_ATTR);
        removeAttribute(tagStr, LEOS_SOFT_USER_ATTR);
        removeAttribute(tagStr, LEOS_SOFT_DATE_ATTR);
        restoreOldId(tagStr);
    }

    /* protected */
    @Override
	protected void updateSoftAttributes(SoftActionType softAction, StringBuilder tagStr) {
        updateAttributeValue(tagStr, LEOS_SOFT_ACTION_ATTR, softAction.getSoftAction());
        updateAttributeValue(tagStr, LEOS_SOFT_ACTION_ROOT_ATTR, Boolean.TRUE.toString());
        updateAttributeValue(tagStr, LEOS_SOFT_USER_ATTR, getUserName());
        try {
            updateAttributeValue(tagStr, LEOS_SOFT_DATE_ATTR, getXMLFormatDate());
        } catch (DatatypeConfigurationException e) {
            updateAttributeValue(tagStr, LEOS_SOFT_DATE_ATTR, null);
        }
    }
    
    /* protected */
    @Override
	protected String getUserName() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        } else {
            return principal.toString();
        }
    }
    
    /* protected */
    @Override
	protected boolean skipPointContent(TableOfContentItemVO tableOfContentItemVO) {
        List<TableOfContentItemVO> childList = tableOfContentItemVO.getChildItems();
        if (childList != null && !childList.isEmpty()) {
            TableOfContentItemVO child = childList.get(0);
            if (child.getTocItem().getAknTag().value().equals(POINT) || child.getTocItem().getAknTag().value().equals(LIST)) {
                return true;
            }
        }
        return false;
    }
    
    /* protected */
    @Override
	protected boolean shouldWrapWithList(TableOfContentItemVO parentItem) {
        boolean wrapWithList = true;
        List<TableOfContentItemVO> childItems = parentItem.getChildItems();
        if (!childItems.isEmpty()) {
            switch (parentItem.getTocItem().getAknTag().value()) {
                case PARAGRAPH:
                    wrapWithList = !parentItem.containsItem(LIST);
                    break;
                case LIST:
                    wrapWithList = false;
                    break;
            }
        }
        return wrapWithList;
    }
    
    /* protected */
    @Override
	protected byte[] wrapWithPoint(TableOfContentItemVO tableOfContentItemVO, byte[] pointContent, boolean isIndent)  {
        // Build the point block
        String startTagStr = "<" + POINT + " xml:id=\"" + tableOfContentItemVO.getId() + "\">";
        byte[] pointTag = insertAffectedAttribute(startTagStr.getBytes(UTF_8), tableOfContentItemVO.isAffected());
        pointTag = updateOriginAttribute(pointTag, tableOfContentItemVO.getOriginAttr());
        return XmlHelper.buildTag(pointTag, POINT.getBytes(UTF_8), pointContent);
    }

    /* protected */
    protected byte[] buildExistingXmlNode(List<TocItem> tocItems, Map<TocItem, List<TocItem>> tocRules, VTDNav contentNavigator, TableOfContentItemVO tableOfContentItemVO,
            ByteArrayBuilder tocItemContent) throws NavException, UnsupportedEncodingException {
    	String tocTagName = tableOfContentItemVO.getTocItem().getAknTag().value();
        byte[] startTag = updateOriginAttribute(XmlTableOfContentHelper.getStartTagAndRemovePrefix(contentNavigator, tableOfContentItemVO), tableOfContentItemVO.getOriginAttr());
        if (tocTagName.equals(PREAMBLE) && tableOfContentItemVO.getPreambleFormula2TagIndex() != null) {
            tocItemContent.append(extractIndexedNonTocElements(contentNavigator, tableOfContentItemVO.getPreambleFormula2TagIndex()));
        } else if (!tocTagName.equals(RECITALS)) { // Recitals contains intro non TOC item and it already has been added
            tocItemContent.append(extractLevelNonTocItems(tocItems, tocRules, contentNavigator, tableOfContentItemVO));
        }
        return startTag;
    }
    
    /* protected */
    @Override
	protected byte[] constructListStructure(TableOfContentItemVO tableOfContentItemVO, User user, byte[] pointTag) {
        TableOfContentItemVO parentItem = tableOfContentItemVO.getParentItem();
        List<TableOfContentItemVO> childItems = parentItem.getChildItems();
        List<TableOfContentItemVO> childItemsOfType = constructChildListWithType(childItems, tableOfContentItemVO.getTocItem().getAknTag().value());

        if (tableOfContentItemVO.getId().equals(childItemsOfType.get(0).getId()) && shouldWrapWithList(parentItem)) {
            return wrapWithList(tableOfContentItemVO, user, childItemsOfType, pointTag);
        } else if (tableOfContentItemVO.getId().equals(childItemsOfType.get(childItemsOfType.size() - 1).getId()) && (parentItem.getVtdIndex() == null ||
                !parentItem.getTocItem().getAknTag().value().equals(LIST))) {
            return closeListTag(pointTag);
        } else {
            return pointTag;
        }
    }

    /* protected */
    @Override
   protected List<TableOfContentItemVO> constructChildListWithType(List<TableOfContentItemVO> childItems, String type) {
        List<TableOfContentItemVO> childItemsOfType = new ArrayList<>();
        for (TableOfContentItemVO child : childItems) {
            if (child.getTocItem().getAknTag().value().equals(type)) {
                childItemsOfType.add(child);
            }
        }
        return childItemsOfType;
    }

    /* protected */
    @Override
   protected byte[] closeListTag(byte[] pointTag) {
        ByteArrayBuilder composedList = new ByteArrayBuilder(pointTag);
        String closeListTag = "</" + LIST + ">";
        composedList.append(closeListTag.getBytes(UTF_8));
        return composedList.getContent();
    }
    
    /* protected */
    @Override
   protected byte[] wrapWithList(TableOfContentItemVO tableOfContentItemVO, User user, List<TableOfContentItemVO> childItems, byte[] pointTag) {
        // Build the list block
        String startTagStr = "<" + LIST + " xml:id=\"" + IdGenerator.generateId(LIST.substring(0, 3), 7) + "\">";
        byte[] listTag = updateSoftInfo(startTagStr.getBytes(UTF_8), SoftActionType.ADD, Boolean.TRUE, user,
                tableOfContentItemVO.getOriginAttr(), null, false, tableOfContentItemVO.getTocItem());
        listTag = updateOriginAttribute(listTag, LEOS_ORIGIN_ATTR_CN);
        byte[] listCloseTag = childItems.size() == 1 ? LIST.getBytes(UTF_8) : null;
        return XmlHelper.buildTag(listTag, listCloseTag, pointTag);
    }
    
    /* protected */
    @Override
   protected boolean isSoftDeletedOrMoved(TableOfContentItemVO tableOfContentItemVO) {
        return SoftActionType.DELETE.equals(tableOfContentItemVO.getSoftActionAttr()) ||
                SoftActionType.MOVE_FROM.equals(tableOfContentItemVO.getSoftActionAttr()) ||
                SoftActionType.MOVE_TO.equals(tableOfContentItemVO.getSoftActionAttr());
    }

    /* protected */
    @Override
   protected boolean isEmptyElement(TableOfContentItemVO tableOfContentItemVO) {
        List<TableOfContentItemVO> childList = tableOfContentItemVO.getChildItems();
        return childList == null || childList.isEmpty();
    }
    
    /* protected */
    @Override
   protected boolean isSingleSubElement(TableOfContentItemVO tableOfContentItemVO) {
        boolean isSingle = false;
        List<TableOfContentItemVO> childList = tableOfContentItemVO.getParentItem().getChildItems();
        if (childList != null && !childList.isEmpty()) {
            TableOfContentItemVO firstChild = childList.get(0);
            switch (childList.size()) {
                case 1:
                    // If only single subparagraph or subpoint is remaining in the paragraph
                    if ((firstChild.getVtdIndex() != null && !firstChild.isMovedOnEmptyParent()) &&
                            (firstChild.getTocItem().getAknTag().value().equals(SUBPARAGRAPH) ||
                                    firstChild.getTocItem().getAknTag().value().equals(SUBPOINT))) {
                        isSingle = true;
                    }
                    break;
                case 2:
                    // If point inside list is deleted and empty list remaining in the paragraph.
                    TableOfContentItemVO secondChild = childList.get(1);
                    if (secondChild.getVtdIndex() != null && secondChild.getTocItem().getAknTag().value().equals(LIST) &&
                            secondChild.getChildItems().isEmpty()) {
                        isSingle = true;
                    }
                    break;
                default:
                    isSingle = false;
            }
        }
        return isSingle;
    }

    /* protected */
    @Override
    protected byte[] extractSubElementContent(VTDNav contentNavigator, TableOfContentItemVO tableOfContentItemVO)
            throws NavException {
        contentNavigator.recoverNode(tableOfContentItemVO.getVtdIndex());
        contentNavigator.toElement(VTDNav.FIRST_CHILD, CONTENT);
        return getTagWithContent(contentNavigator);
    }

    
    @Override
    public ArrayList<String> doImportedMrefProjectPreProcessing(ArrayList<String[]> elementMref) {
    	ArrayList<String> arrayRef = new ArrayList<>();
    	VTDNav vtdNav;
    	for( int i = 0; i < elementMref.size(); i++ ) {
    		for( int j = 0; j < elementMref.get(i).length; j++ ) {
    			if (j == 2) {
    				try {
    					vtdNav =setupVTDNav(elementMref.get(i)[j].getBytes(StandardCharsets.UTF_8), false);
    					AutoPilot autoPilot = new AutoPilot(vtdNav);
    					autoPilot.selectXPath("//@href");

    					while(autoPilot.evalXPath() != -1) {
    						int idAttrValue = vtdNav.getAttrVal("href");
    						String idPrefix = new StringBuilder("").append( doImportedId(elementMref.get(i)[1])).toString();
    		    			String newIdAttrValue = IdGenerator.generateId(idPrefix, 9);
    		    			arrayRef.add(vtdNav.toNormalizedString(idAttrValue) + "-" + elementMref.get(i)[0] + "-" +newIdAttrValue);
    					}
    				} catch (Exception e) {
    					LOG.error("Unable to perform the doImportedElementProjectPreProcessing operation", e);
    					throw new RuntimeException("Unable to perform the doImportedElementProjectPreProcessing operation", e);
    				}
    			} 
    		}
    	}

    	return arrayRef;
    }
    
    @Override
    public String doImportedElementProjectPreProcessing(String xmlContent, String elementType , ArrayList<String> mrefId) {
    	LOG.trace("Start import article or proviso doImportedElementProjectPreProcessing ");
    	Stopwatch watch = Stopwatch.createStarted();
    	String updatedElement = null;
    	try {
    		VTDNav vtdNav =setupVTDNav(xmlContent.getBytes(StandardCharsets.UTF_8), false);
    		XMLModifier xmlModifier = new XMLModifier(vtdNav);
    		AutoPilot autoPilot = new AutoPilot(vtdNav);
    		autoPilot.selectXPath("//@id");
    		while(autoPilot.evalXPath() != -1) {
    			boolean insert = false;
    			int idAttrValue = vtdNav.getAttrVal("id");
    			String value = vtdNav.toNormalizedString(idAttrValue);
    			String[] mrefIdsAux;
    			for (String v: mrefId) {
    				mrefIdsAux = v.split("-");
    				if (mrefIdsAux[0].equals(value)) {
    					xmlModifier.updateToken(idAttrValue, mrefIdsAux[2].getBytes(UTF_8));
    					insert = true;
    				}
    			}
    			if (!insert) {
    				String idPrefix = new StringBuilder("").append( doImportedId(elementType)).toString();
    				String newIdAttrValue = IdGenerator.generateId(idPrefix, 9);
    				xmlModifier.updateToken(idAttrValue, newIdAttrValue.getBytes(UTF_8));
    			}
    		}
    		autoPilot.selectXPath("//@href");
    		while(autoPilot.evalXPath() != -1) {
    			int idAttrValue = vtdNav.getAttrVal("href");
    			String value = vtdNav.toNormalizedString(idAttrValue);
    			String[] mrefIdsAux;
    			for (String v: mrefId) {
    				mrefIdsAux = v.split("-");
    				if (mrefIdsAux[0].equals(value)) {
    					xmlModifier.updateToken(idAttrValue, mrefIdsAux[2].getBytes(UTF_8));
    				}
    			}
    		}
    		updatedElement = removeSelfClosingElements(new String(toByteArray(xmlModifier), UTF_8));
    	} catch (Exception e) {
    		LOG.error("Unable to perform the doImportedElementProjectPreProcessing operation", e);
    		throw new RuntimeException("Unable to perform the doImportedElementProjectPreProcessing operation", e);
    	}
    	LOG.trace("Finished doImportedElementProjectPreProcessing: TotalTime taken{}", watch.elapsed(TimeUnit.MILLISECONDS));
    	return updatedElement;
    }
    
    public String doImportedId (String type) {
    	String id = null;
    	switch (type) {
		case ARTICLE:
			id = "akn_art_imp";
			break;
		case RECITAL:
			id = "akn_rec_imp";
			break;
		case PROVISO:
			id = "akn_dis_imp";
			break;
		}
    	return id;
    }
    
    // LEOS-2639: replace XML self-closing tags not supported in HTML
    private String removeSelfClosingElements(String fragment) {
        String removeSelfClosingRegex = "<([^>^\\s]+)([^>]*)/>";
        return fragment.replaceAll(removeSelfClosingRegex, "<$1$2></$1>");
    }
    
    @Override
    public void specificInstanceXMLPostProcessing(XMLModifier xmlModifier) throws Exception {
        updateSoftMoveLabelAttribute(xmlModifier, LEOS_SOFT_MOVE_TO);
        updateSoftMoveLabelAttribute(xmlModifier, LEOS_SOFT_MOVE_FROM);
        updateNewElements(xmlModifier, RECITAL, null);
        updateNewElements(xmlModifier, ARTICLE, null);
        updateNewElements(xmlModifier, PARAGRAPH, SUBPARAGRAPH);
        updateNewElements(xmlModifier, POINT, SUBPOINT);
    }

    private void updateSoftMoveLabelAttribute(XMLModifier xmlModifier, String attr) throws Exception {
        VTDNav vtdNav = xmlModifier.outputAndReparse();
        xmlModifier.bind(vtdNav);
        vtdNav.toElement(VTDNav.ROOT);
        AutoPilot autoPilot = new AutoPilot(vtdNav);
        autoPilot.declareXPathNameSpace("leos", "urn:eu:europa:ec:leos");// required
        autoPilot.declareXPathNameSpace("xml", "http://www.w3.org/XML/1998/namespace");
        autoPilot.selectXPath(String.format("//*[@%s]", attr));
        int currentIndex;
        if (autoPilot.evalXPathToBoolean()) {
            while (autoPilot.evalXPath() != -1) {
                currentIndex = vtdNav.getCurrentIndex();
                try {
                    String updatedMovedReferenceContent;
                    Result<String> labelResult = referenceLabelService.generateSoftmoveLabel(getRefFromSoftMovedElt(vtdNav, attr),
                    		getParentId(vtdNav), xmlModifier, attr, getRef(vtdNav));
                    vtdNav.recoverNode(currentIndex);
                    if (labelResult.isOk()) {
                        updatedMovedReferenceContent = labelResult.get();
                        autoPilot.selectAttr(LEOS_SOFT_MOVED_LABEL_ATTR);
                        int indexAttr = autoPilot.iterateAttr();
                        if (indexAttr != -1) {
                            xmlModifier.removeAttribute(indexAttr);
                        }
                        xmlModifier.insertAttribute(new StringBuilder(" ").append(LEOS_SOFT_MOVED_LABEL_ATTR).append("=\"")
                                .append(updatedMovedReferenceContent).append("\"").toString());
                    }
                } catch (Exception ex) {
                    String movedEltContent = getFragmentAsString(vtdNav, vtdNav.getContentFragment(), false);
                    LOG.error("Soft moved element can not be updated.Skipping. Soft Moved Element Content: {},", movedEltContent, ex);
                }
            }
        }
    }
    
    private Ref getRefFromSoftMovedElt(VTDNav vtdNav, String attr) throws Exception {
        String id = null, href = null, documentRef = null;
        int index = vtdNav.getAttrVal(XMLID);
        if (index != -1) {
            id = vtdNav.toString(index);
        }
        index = vtdNav.getAttrVal(attr);
        if (index != -1) {
            href = vtdNav.toString(index);
        }
        index = vtdNav.getAttrVal("documentref");
        if (index != -1) {
            documentRef = vtdNav.toString(index);
        }
        return new Ref(id, href, documentRef);
    }
    
    private void updateNewElements(XMLModifier xmlModifier, String elementTagName, String subElementTagName) throws Exception {
        VTDNav vtdNav = xmlModifier.outputAndReparse();
        xmlModifier.bind(vtdNav);
        vtdNav.toElement(VTDNav.ROOT);

        AutoPilot autoPilot = new AutoPilot(vtdNav);
        autoPilot.selectElement(elementTagName);

        while (autoPilot.iterate()) {
            int currentIndex = vtdNav.getCurrentIndex();
            String elementId = vtdNav.toString(vtdNav.getAttrVal(XMLID));
            String elementOrigin = (vtdNav.getAttrVal(LEOS_ORIGIN_ATTR) != -1) ? vtdNav.toString(vtdNav.getAttrVal(LEOS_ORIGIN_ATTR)) : LEOS_ORIGIN_ATTR_CN;
            if (elementOrigin.equals(LEOS_ORIGIN_ATTR_CN) &&
                    ((vtdNav.getAttrVal(LEOS_ORIGIN_ATTR) == -1) || (vtdNav.getAttrVal(LEOS_SOFT_ACTION_ATTR) == -1))) {
                xmlModifier.insertAttribute((vtdNav.getAttrVal(LEOS_ORIGIN_ATTR) == -1 ? generateOriginInfo(LEOS_ORIGIN_ATTR_CN) : EMPTY_STRING) +
                        (vtdNav.getAttrVal(LEOS_SOFT_ACTION_ATTR) == -1 ? generateSoftInfo(SoftActionType.ADD) : EMPTY_STRING));
            }
            if ((subElementTagName != null) && (vtdNav.toElement(VTDNav.FIRST_CHILD, subElementTagName))) {
                boolean isFirstSubElement = true;
                do {
                    if (isFirstSubElement && elementOrigin.equals(LEOS_ORIGIN_ATTR_EC) && (vtdNav.getAttrVal(LEOS_ORIGIN_ATTR) == -1)) {
                        if (vtdNav.getAttrVal(LEOS_SOFT_ACTION_ATTR) == -1) {
                            xmlModifier.insertAttribute(generateOriginInfo(LEOS_ORIGIN_ATTR_EC) + generateSoftInfo(SoftActionType.TRANSFORM));
                        } else {
                            xmlModifier.insertAttribute(generateOriginInfo(LEOS_ORIGIN_ATTR_EC));
                            xmlModifier.updateToken(vtdNav.getAttrVal(LEOS_SOFT_ACTION_ATTR), SoftActionType.TRANSFORM.getSoftAction());
                        }
                        xmlModifier.updateToken(vtdNav.getAttrVal(XMLID), SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX + elementId);
                    } else {
                        String subElementOrigin = (vtdNav.getAttrVal(LEOS_ORIGIN_ATTR) != -1) ? vtdNav.toString(vtdNav.getAttrVal(LEOS_ORIGIN_ATTR))
                                : LEOS_ORIGIN_ATTR_CN;
                        if (subElementOrigin.equals(LEOS_ORIGIN_ATTR_CN) &&
                                ((vtdNav.getAttrVal(LEOS_ORIGIN_ATTR) == -1) || (vtdNav.getAttrVal(LEOS_SOFT_ACTION_ATTR) == -1))) {
                            xmlModifier.insertAttribute((vtdNav.getAttrVal(LEOS_ORIGIN_ATTR) == -1 ? generateOriginInfo(LEOS_ORIGIN_ATTR_CN) : EMPTY_STRING) +
                                    (vtdNav.getAttrVal(LEOS_SOFT_ACTION_ATTR) == -1 ? generateSoftInfo(SoftActionType.ADD) : EMPTY_STRING));
                        }
                    }
                    isFirstSubElement = false;
                } while (vtdNav.toElement(VTDNav.NEXT_SIBLING, subElementTagName));
                vtdNav.recoverNode(currentIndex); // Remove attribute LEOS_SOFT_TRANS_FROM from parent
                removeAttribute(vtdNav, xmlModifier, LEOS_SOFT_TRANS_FROM);
            }
            vtdNav.recoverNode(currentIndex);
        }
    }
    
    private String generateOriginInfo(String origin) {
        return new StringBuffer(" ").append(LEOS_ORIGIN_ATTR).append("=\"").append(origin).append("\"").toString();
    }

    private String generateSoftInfo(SoftActionType softAction) throws DatatypeConfigurationException {
        return new StringBuffer(" ").append(LEOS_SOFT_ACTION_ATTR).append("=\"").append(softAction.getSoftAction()).append("\"")
                .append(" ").append(LEOS_SOFT_USER_ATTR).append("=\"").append(getUserName()).append("\"")
                .append(" ").append(LEOS_SOFT_DATE_ATTR).append("=\"").append(getXMLFormatDate()).append("\"").toString();
    }

	@Override
	public Map<String, List<String>> bloqueVersion(byte[] xmlContent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> crearConsolidado(byte[] eli, String type) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean getReferencedBOE(byte[] content, String elementType, String id, String numElement) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public String extractIdModAmendment (String xmlContent) {
		return null;
	}
	
	@Override
	public String equalActiveRefAmendment(byte[] xmlContent, String nameNorm, String eliDestination) {
		// TODO Auto-generated method stub
		return null;
	}
	
//	@Override
//	public String[] typeActiveMoficationAmendment(byte[] xmlContent, String idModOld, String idModNew, String actionType, String eliDestination, String pos, String wid) {
//		// TODO Auto-generated method stub
//		return null;
//	}
	
	@Override
	public String createNewNumInsertAmendment  (String insertType, String elementSelect, String elementParent, String typeElementSelect) {
		return null;
	}

	@Override
    public AGEExtRefUpdateOutput updatedExternalMetaReferences(byte[] xmlContent) {
		return null;
	}
	
	@Override
	public byte[] addExternalMetaReference(byte[] xmlContent, String eId, String hRef) {
		return null;
	}

	@Override
	public String[] getNormVersion(String ELI) {
		// TODO Auto-generated method stub
		return null;
	} 

	@Override
	public byte[] appendElementInsideTagByXPathAmend(byte[] newXmlContent, String xPath, String template, boolean b) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, List<String>> createFrameTextAmend(byte[] xmlContent, String xPath){
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] appendElementInsideModAmend(byte[] xmlContent, String xPath, String newContent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] typeActiveMoficationAmendmentMod(byte[] xmlContent,String idModNew, String actionType, String quotedStructureId, String eliDestination, String pos, String wid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String convertActionType(String actionType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] deleteRefNormElementAmend(byte[] xmlContent, String xPath, String refNorm) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int countSeveralMod(byte[] xmlContent, String xPath) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public byte[] appendElementXPathAmend(byte[] xmlContent, String xPath, String newContent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] deleteElementModifi(byte[] xmlContent, String tagName, String idAttributeValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] typeActiveMoficationAmendmentNew(byte[] xmlContent, String idMod, String actionType,
			String quotedStructureId, String eliDestination, String pos, String wid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getIdModEditingModification(byte[] newXmlContent, String xPath) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getParentNumEditingModification(byte[] newXmlContent, String xPath) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] deleteAllTextualMod(byte[] newXmlContent, String xPath) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getActiveRefAmendment(byte[] xmlContentDocument, String xpath) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] deleteElementByTagNameAndIdParentMod(byte[] xmlContent, String tagName, String idAttributeValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int countSeveralQuoted(byte[] xmlContent, String xPath, String modId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<String> getListElementIdByPath(byte[] xmlContent, String xPath) {
		// TODO Auto-generated method stub
		return null;
	}

}
