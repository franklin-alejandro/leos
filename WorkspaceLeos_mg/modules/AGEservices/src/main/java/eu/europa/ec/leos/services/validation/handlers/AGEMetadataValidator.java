package eu.europa.ec.leos.services.validation.handlers;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.common.ErrorCode;
import eu.europa.ec.leos.domain.vo.DocumentVO;
import eu.europa.ec.leos.domain.vo.ErrorVO;
import eu.europa.ec.leos.domain.vo.MetadataVO;
import eu.europa.ec.leos.services.store.TemplateService;

@Primary
@Component
public class AGEMetadataValidator extends MetadataValidator {

	private final TemplateService templateService;

    @Autowired
    AGEMetadataValidator(TemplateService templateService) {
    	super(templateService);
        this.templateService = templateService;
    }

    @Override
    public void validate(DocumentVO documentVO, final List<ErrorVO> result) throws Exception {
        validateMetadata(documentVO, result);
    }

    /**
     * Method to check the metadata of the document.
     * @param documentVO
     */
    private void validateMetadata(DocumentVO documentVO, final List<ErrorVO> errors) {
        String docId = documentVO.getId();
        MetadataVO metadata = documentVO.getMetadata();
        // check valid docpurpose
        if (StringUtils.isEmpty(metadata.getDocPurpose())) {
            errors.add(new ErrorVO(ErrorCode.DOCUMENT_PURPOSE_NOT_FOUND, docId));
        }
        // check is valid template
        try {
            if (templateService.getTemplate(metadata.getDocTemplate()) == null) {
                errors.add(new ErrorVO(ErrorCode.DOCUMENT_TEMPLATE_NOT_FOUND, docId));
            }
        } catch (Exception e) {
            errors.add(new ErrorVO(ErrorCode.DOCUMENT_TEMPLATE_NOT_FOUND, docId));
        }
        // check valid templateName
        if (StringUtils.isEmpty(metadata.getTemplateName())) {
            errors.add(new ErrorVO(ErrorCode.DOCUMENT_PROPOSAL_TEMPLATE_NOT_FOUND, docId));
        }
        if (StringUtils.isEmpty(metadata.getTemplate())) {
            errors.add(new ErrorVO(ErrorCode.DOCUMENT_PROPOSAL_TEMPLATE_NOT_FOUND, docId));
        }

        if (documentVO.getCategory() == LeosCategory.ANNEX) {
            // checks for type annex
            if (StringUtils.isEmpty(metadata.getIndex())) {
                errors.add(new ErrorVO(ErrorCode.DOCUMENT_ANNEX_INDEX_NOT_FOUND, docId));
            }
            //LEOS-2840 can be empty but not null. The tag leos:annexTitle must be in the xml.
            if (metadata.getTitle() == null) {
                errors.add(new ErrorVO(ErrorCode.DOCUMENT_ANNEX_TITLE_NOT_FOUND, docId));
            }
            if (StringUtils.isEmpty(metadata.getNumber())) {
                errors.add(new ErrorVO(ErrorCode.DOCUMENT_ANNEX_NUMBER_NOT_FOUND, docId));
            }
        }
        
        if (documentVO.getCategory() == LeosCategory.REPORT) {
            // checks for type annex
            if (StringUtils.isEmpty(metadata.getIndex())) {
                errors.add(new ErrorVO(ErrorCode.DOCUMENT_REPORT_INDEX_NOT_FOUND, docId));
            }
            //LEOS-2840 can be empty but not null. The tag leos:annexTitle must be in the xml.
            if (metadata.getTitle() == null) {
                errors.add(new ErrorVO(ErrorCode.DOCUMENT_REPORT_TITLE_NOT_FOUND, docId));
            }
            if (StringUtils.isEmpty(metadata.getNumber())) {
                errors.add(new ErrorVO(ErrorCode.DOCUMENT_REPORT_NUMBER_NOT_FOUND, docId));
            }
        }
        if (documentVO.getCategory() == LeosCategory.CANTODORADO) {
            // checks for type annex
            if (StringUtils.isEmpty(metadata.getIndex())) {
                errors.add(new ErrorVO(ErrorCode.DOCUMENT_CANTODORADO_INDEX_NOT_FOUND, docId));
            }
            //LEOS-2840 can be empty but not null. The tag leos:annexTitle must be in the xml.
            if (metadata.getTitle() == null) {
                errors.add(new ErrorVO(ErrorCode.DOCUMENT_CANTODORADO_TITLE_NOT_FOUND, docId));
            }
            if (StringUtils.isEmpty(metadata.getNumber())) {
                errors.add(new ErrorVO(ErrorCode.DOCUMENT_CANTODORADO_NUMBER_NOT_FOUND, docId));
            }
        }
        if (documentVO.getCategory() == LeosCategory.MAIN) {
            // checks for type annex
            if (StringUtils.isEmpty(metadata.getIndex())) {
                errors.add(new ErrorVO(ErrorCode.DOCUMENT_MAIN_INDEX_NOT_FOUND, docId));
            }
            //LEOS-2840 can be empty but not null. The tag leos:annexTitle must be in the xml.
            if (metadata.getTitle() == null) {
                errors.add(new ErrorVO(ErrorCode.DOCUMENT_MAIN_TITLE_NOT_FOUND, docId));
            }
            if (StringUtils.isEmpty(metadata.getNumber())) {
                errors.add(new ErrorVO(ErrorCode.DOCUMENT_MAIN_NUMBER_NOT_FOUND, docId));
            }
        }
    }
	
}
