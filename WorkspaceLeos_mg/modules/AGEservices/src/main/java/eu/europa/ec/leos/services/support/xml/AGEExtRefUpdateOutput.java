package eu.europa.ec.leos.services.support.xml;

import java.util.List;

public class AGEExtRefUpdateOutput {
	
    private final byte[] doc;
    private final List<String> updates;

    public AGEExtRefUpdateOutput(byte[] doc, List<String> updates) {
        this.doc = doc;
        this.updates = updates;
    }

    public byte[] getDoc() {
        return doc;
    }

    public List<String> getUpdates() {
        return updates;
    }
}
