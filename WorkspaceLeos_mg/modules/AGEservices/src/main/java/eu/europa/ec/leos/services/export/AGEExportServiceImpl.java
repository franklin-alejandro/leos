package eu.europa.ec.leos.services.export;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.itextpdf.forms.PdfPageFormCopier;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.html2pdf.resolver.font.DefaultFontProvider;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;

import eu.europa.ec.leos.domain.cmis.Content;
import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.cmis.LeosPackage;
import eu.europa.ec.leos.domain.cmis.document.AGECantoDorado;
import eu.europa.ec.leos.domain.cmis.document.AGEMain;
import eu.europa.ec.leos.domain.cmis.document.Annex;
import eu.europa.ec.leos.domain.cmis.document.Bill;
import eu.europa.ec.leos.domain.cmis.document.LeosDocument;
import eu.europa.ec.leos.domain.cmis.document.Memorandum;
import eu.europa.ec.leos.domain.cmis.document.Proposal;
import eu.europa.ec.leos.domain.cmis.document.XmlDocument;
import eu.europa.ec.leos.domain.cmis.metadata.AGECantoDoradoMetadata;
import eu.europa.ec.leos.domain.cmis.metadata.AGEMainMetadata;
import eu.europa.ec.leos.domain.cmis.metadata.AnnexMetadata;
import eu.europa.ec.leos.domain.cmis.metadata.ProposalMetadata;
import eu.europa.ec.leos.domain.vo.DocumentVO;
import eu.europa.ec.leos.domain.vo.MetadataVO;
import eu.europa.ec.leos.security.SecurityContext;
import eu.europa.ec.leos.services.content.processor.TransformationService;
import eu.europa.ec.leos.services.document.AGECantoDoradoService;
import eu.europa.ec.leos.services.document.AnnexService;
import eu.europa.ec.leos.services.document.BillService;
import eu.europa.ec.leos.services.store.LegService;
import eu.europa.ec.leos.services.store.PackageService;


@Primary
@Service
public class AGEExportServiceImpl extends ExportServiceImpl implements AGEExportService {
	
    private static final Logger LOG = LoggerFactory.getLogger(AGEExportServiceImpl.class);

    private static final String TEMP_FILE_NAME = "tmp";
    private static final String TEMP_FINAL_FILE_NAME = "tmp_final";
    private static final String PDF_EXTENSION = "pdf";
    private static final String CONTENT = "{content}";
    private static final String BASIC_CSS = "{basicCSS}";

    private static final String EXPORT_TEMPLATE_FILE = "eu/europa/ec/leos/html/templates/export/pdf.html";
    private static final String CSS_EXTENSION = "css";
    
    private Set<String> docVersionSeriesIds;
    
    @Autowired
    AGECantoDoradoService cantoDoradoService;

    @Autowired
	AGEExportServiceImpl(LegService legService, PackageService packageService, SecurityContext securityContext,
			ExportHelper exportHelper, BillService billService, AnnexService annexService,
			TransformationService transformationService) {
		super(legService, packageService, securityContext, exportHelper, billService, annexService, transformationService);
	}
	
    @Override
    public File exportToPdfCantoDorado(String proposalId) {
        List<File> fileList = new ArrayList<>();

        ConverterProperties converterProperties = new ConverterProperties();
        converterProperties.setTagWorkerFactory(new CustomTagWorkerFactory());
        converterProperties.setCssApplierFactory(new CustomCssApplierFactory());
        converterProperties.setFontProvider(new DefaultFontProvider(true, true, true));

        List<String> htmlSourceList = getHTMLContent(proposalId);
        htmlSourceList.forEach(htmlSource -> {
            String fileName = String.format("%s_%s.%s", TEMP_FINAL_FILE_NAME, (new Date()).getTime(), PDF_EXTENSION);
            File pdfDest = new File(fileName);

            try {
                HtmlConverter.convertToPdf(htmlSource, new FileOutputStream(pdfDest), converterProperties);
                fileList.add(pdfDest);
            } catch (Exception e) {
                LOG.error("An error occurred when export to Pdf! {}", e.getMessage(), e);
            }
        });
        File file = mergeDocuments(fileList);
        removeTemporalFile(fileList);
        return file;
       	
    }
    
    private File mergeDocuments(List<File> files) {
        String fileName = String.format("%s_%s.%s", TEMP_FILE_NAME, (new Date()).getTime(), PDF_EXTENSION);
        File pdfDest = new File(fileName);

        PdfDocument pdfDoc;
        try {
        	
            pdfDoc = new PdfDocument(new PdfWriter(pdfDest));
            pdfDoc.initializeOutlines();
            files.forEach(file -> {
                try {
                    PdfReader reader = new PdfReader(file);
                    PdfDocument readerDoc = new PdfDocument(reader);
                    readerDoc.copyPagesTo(1, readerDoc.getNumberOfPages(), pdfDoc, new PdfPageFormCopier());
                    readerDoc.close();
                    reader.close();
                } catch (IOException e) {
                    LOG.error("An error occurred when merging documents! {}", e.getMessage(), e);
                }
            });
            pdfDoc.close();
        } catch (FileNotFoundException e) {
            LOG.error("An error occurred when merging documents! {}", e.getMessage(), e);
        }
        return pdfDest;
    }
    
    private List<String> getHTMLContent(String proposalId) {
        LeosPackage leosPackage = packageService.findPackageByDocumentId(proposalId);
        List<XmlDocument> documents = packageService.findDocumentsByPackagePath(leosPackage.getPath(),
                XmlDocument.class, false);

        DocumentVO proposalVO = createViewObject(documents);
        List<Entry<LeosCategory, String>> documentHtmlList = getHTMLContent(proposalVO, new ArrayList<>());

        return fillTemplate(documentHtmlList);
    }
    
    private DocumentVO createViewObject(List<XmlDocument> documents) {
        DocumentVO proposalVO = new DocumentVO(LeosCategory.PROPOSAL);
        List<DocumentVO> annexVOList = new ArrayList<>();
        docVersionSeriesIds = new HashSet<>();
        // We have the latest version of the document, no need to search for them again
        for (XmlDocument document : documents) {
            switch (document.getCategory()) {
                case PROPOSAL: {
                    Proposal proposal = (Proposal) document;
                    MetadataVO metadataVO = createMetadataVO(proposal);
                    proposalVO.setMetaData(metadataVO);
                    proposalVO.addCollaborators(proposal.getCollaborators());
                    break;
                }
                case CANTODORADO: {
                    AGECantoDorado cantoDorado = (AGECantoDorado) document;
                    DocumentVO cantDoradoVO = createCantoDoradoVO(cantoDorado);
                    proposalVO.addChildDocument(cantDoradoVO);
                    cantDoradoVO.addCollaborators(cantoDorado.getCollaborators());
                    cantDoradoVO.getMetadata().setInternalRef(cantoDorado.getMetadata().getOrError(() -> "Annex metadata is not available!").getRef());
                    //annexVOList.add(cantDoradoVO);
                    cantDoradoVO.setVersionSeriesId(cantoDorado.getVersionSeriesId());
                    docVersionSeriesIds.add(cantoDorado.getVersionSeriesId());
                	break;
                }
                case MAIN: {
                    AGEMain main = (AGEMain) document;
                    DocumentVO mainVO = createMainVO(main);
                    proposalVO.addChildDocument(mainVO);
                    mainVO.addCollaborators(main.getCollaborators());
                    mainVO.getMetadata().setInternalRef(main.getMetadata().getOrError(() -> "Main metadata is not available!").getRef());
                    mainVO.setVersionSeriesId(main.getVersionSeriesId());
                    docVersionSeriesIds.add(main.getVersionSeriesId());
                	break;
                }
//                case MEMORANDUM: {
//                    Memorandum memorandum = (Memorandum) document;
//                    DocumentVO memorandumVO = getMemorandumVO(memorandum);
//                    proposalVO.addChildDocument(memorandumVO);
//                    memorandumVO.addCollaborators(memorandum.getCollaborators());
//                    memorandumVO.getMetadata().setInternalRef(memorandum.getMetadata().getOrError(() -> "Memorandum metadata is not available!").getRef());
//                    memorandumVO.setVersionSeriesId(memorandum.getVersionSeriesId());
//                    docVersionSeriesIds.add(memorandum.getVersionSeriesId());
//                    break;
//                }
//                case BILL: {
//                    Bill bill = (Bill) document;
//                    DocumentVO billVO = getLegalTextVO(bill);
//                    proposalVO.addChildDocument(billVO);
//                    billVO.addCollaborators(bill.getCollaborators());
//                    billVO.getMetadata().setInternalRef(bill.getMetadata().getOrError(() -> "Legal text metadata is not available!").getRef());
//                    billVO.setVersionSeriesId(bill.getVersionSeriesId());
//                    docVersionSeriesIds.add(bill.getVersionSeriesId());
//                    break;
//                }
                case ANNEX: {
                    Annex annex = (Annex) document;
                    DocumentVO annexVO = createAnnexVO(annex);
                    annexVO.addCollaborators(annex.getCollaborators());
                    annexVO.getMetadata().setInternalRef(annex.getMetadata().getOrError(() -> "Annex metadata is not available!").getRef());
                    annexVOList.add(annexVO);
                    annexVO.setVersionSeriesId(annex.getVersionSeriesId());
                    docVersionSeriesIds.add(annex.getVersionSeriesId());
                    break;
                }
                default:
                    LOG.debug("Do nothing for rest of the categories like MEDIA, CONFIG & LEG");
                    break;
            }
        }

        annexVOList.sort(Comparator.comparingInt(DocumentVO::getDocNumber));
//        DocumentVO cantoDorado = proposalVO.getChildDocument(LeosCategory.CANTODORADO);
//        if (cantoDorado != null) {
//            for (DocumentVO annexVO : annexVOList) {
//            	cantoDorado.addChildDocument(annexVO);
//            }
//        }
//        
        DocumentVO cantoDoradoText = proposalVO.getChildDocument(LeosCategory.CANTODORADO);
        if (cantoDoradoText != null) {
            for (DocumentVO annexVO : annexVOList) {
            	cantoDoradoText.addChildDocument(annexVO);
            }
        }

        return proposalVO;
    }
    
    private MetadataVO createMetadataVO(Proposal proposal) {
        ProposalMetadata metadata = proposal.getMetadata().getOrError(() -> "Proposal metadata is not available!");
        return new MetadataVO(metadata.getStage(), metadata.getType(), metadata.getPurpose(), metadata.getTemplate(), metadata.getLanguage());
    }
    
    private DocumentVO createCantoDoradoVO(AGECantoDorado cantoDorado) {
        DocumentVO cantoDoradoVO = new DocumentVO(cantoDorado.getId(),
        		cantoDorado.getMetadata().exists(m -> m.getLanguage() != null) ? cantoDorado.getMetadata().get().getLanguage() : "EN",
                LeosCategory.CANTODORADO,
                cantoDorado.getLastModifiedBy(),
                Date.from(cantoDorado.getLastModificationInstant()));

        if (cantoDorado.getMetadata().isDefined()) {
            AGECantoDoradoMetadata metadata = cantoDorado.getMetadata().get();
            cantoDoradoVO.setDocNumber(metadata.getIndex());
            cantoDoradoVO.setTitle(metadata.getTitle());
        }

        return cantoDoradoVO;
    }

    private DocumentVO createMainVO(AGEMain main) {
        DocumentVO mainVO = new DocumentVO(main.getId(),
        		main.getMetadata().exists(m -> m.getLanguage() != null) ? main.getMetadata().get().getLanguage() : "EN",
                LeosCategory.MAIN,
                main.getLastModifiedBy(),
                Date.from(main.getLastModificationInstant()));

        if (main.getMetadata().isDefined()) {
            AGEMainMetadata metadata = main.getMetadata().get();
            mainVO.setDocNumber(metadata.getIndex());
            mainVO.setTitle(metadata.getTitle());
        }

        return mainVO;
    }    
    
    private DocumentVO createAnnexVO(Annex annex) {
        DocumentVO annexVO = new DocumentVO(annex.getId(),
                annex.getMetadata().exists(m -> m.getLanguage() != null) ? annex.getMetadata().get().getLanguage() : "EN",
                LeosCategory.ANNEX,
                annex.getLastModifiedBy(),
                Date.from(annex.getLastModificationInstant()));

        if (annex.getMetadata().isDefined()) {
            AnnexMetadata metadata = annex.getMetadata().get();
            annexVO.setDocNumber(metadata.getIndex());
            annexVO.setTitle(metadata.getTitle());
        }

        return annexVO;
    }
    
    private List<Entry<LeosCategory, String>> getHTMLContent(DocumentVO documentVO, List<Entry<LeosCategory, String>> htmlList) {
        for (DocumentVO d : documentVO.getChildDocuments()) {
            String documentId = d.getId();
            String html = getHtmlContent(documentId, d.getDocumentType());
            Entry<LeosCategory, String> htmlEntry = new SimpleEntry<>(d.getDocumentType(), html);
            htmlList.add(htmlEntry);
            if (!d.getChildDocuments().isEmpty()) {
                htmlList = getHTMLContent(d, htmlList);
            }
        };

        return htmlList;
    }
    
    private String getHtmlContent(String documentId, LeosCategory category) {
        String html = "";
        try {
            if (documentId != null) {
//                if (category == LeosCategory.BILL) {
//                    Bill bill = billService.findBill(documentId);
//                    html = getHtmlContent(bill, category);
//                } else 
                if (category == LeosCategory.ANNEX) {
                    Annex annex = annexService.findAnnex(documentId);
                    html = getHtmlContent(annex, category);
                } else if (category == LeosCategory.CANTODORADO) {
                    AGECantoDorado cantoDorado = cantoDoradoService.findCantoDorado(documentId);
                    html = getHtmlContent(cantoDorado, category);
                }
            }
        } catch (IllegalArgumentException iae) {
            LOG.debug("Document {} cannot be retrieved due to exception {}, Rejecting view", documentId,
                    iae.getMessage(), iae);
        }
        return html;
    }
    
    private List<Entry<LeosCategory, String>> getHTMLContent(LeosDocument document, List<Entry<LeosCategory, String>> htmlList) {
    	//String documentId = documentVO.getId();
    	String html = getHtmlContent(document, LeosCategory.CANTODORADO);
    	Entry<LeosCategory, String> htmlEntry = new SimpleEntry<>(LeosCategory.CANTODORADO, html);
    	htmlList.add(htmlEntry);   
    	return htmlList;
    }
    
    private void removeTemporalFile(List<File> files) {
        for (File f : files) {
            if (f.exists()) {
                f.delete();
            }
        }
    }

    private List<String> fillTemplate(List<Entry<LeosCategory, String>> htmlContentList) {
    	List<String> templateFilledList = new ArrayList<>();

    	htmlContentList.forEach(entry -> {
    		String templateContent = getFileContent(EXPORT_TEMPLATE_FILE);
    		String cssFileName = String.format("%s.%s", entry.getKey().name().toLowerCase(), CSS_EXTENSION);
    		String basicCss = getFileContent("META-INF/resources/assets/css/AGECantoDoradoreport.css");
//    		 if (cssFileName.contains("annex") ) { //AGE-CORE-3.0.0
//    			 basicCss = getFileContent("META-INF/resources/assets/css/AGE" + "bill.css");
//    		 }
    		if (!"".equals(entry.getValue())) {
    			templateContent = templateContent.replace(BASIC_CSS, basicCss);
    			templateContent = templateContent.replace(CONTENT, entry.getValue());
    			templateFilledList.add(templateContent);
    		}

    	});
    	return templateFilledList;
    }
    
    private String getFileContent(String fileName) {
        InputStream is = getClass().getClassLoader().getResourceAsStream(fileName);
        if (is != null) {
            try (BufferedReader buffer = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
                return buffer.lines().collect(Collectors.joining(""));
            } catch (IOException e) {
                LOG.error("An error occurred when getting file content! {}", e.getMessage(), e);
            }
        }
        return "";
    }
       
    private String getHtmlContent(LeosDocument document, LeosCategory category) {
        return transformationService.toEditableXml(new ByteArrayInputStream(getContent(document)), null, category,
                securityContext.getPermissions(document));
    }
    
    private byte[] getContent(LeosDocument doc) {
        final Content content = doc.getContent().getOrError(() -> "Document content is required!");
        return content.getSource().getBytes();
    }

}
