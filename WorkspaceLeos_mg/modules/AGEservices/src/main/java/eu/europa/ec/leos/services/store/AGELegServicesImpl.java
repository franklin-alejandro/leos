package eu.europa.ec.leos.services.store;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Provider;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import eu.europa.ec.leos.domain.cmis.Content;
import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.cmis.LeosPackage;
import eu.europa.ec.leos.domain.cmis.document.AGEReport;
import eu.europa.ec.leos.domain.cmis.document.Annex;
import eu.europa.ec.leos.domain.cmis.document.Bill;
import eu.europa.ec.leos.domain.cmis.document.MediaDocument;
import eu.europa.ec.leos.domain.cmis.document.Memorandum;
import eu.europa.ec.leos.domain.cmis.document.Proposal;
import eu.europa.ec.leos.domain.cmis.document.XmlDocument;
import eu.europa.ec.leos.domain.cmis.metadata.AGEReportMetadata;
import eu.europa.ec.leos.domain.common.TocMode;
import eu.europa.ec.leos.domain.vo.DocumentVO;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.repository.store.PackageRepository;
import eu.europa.ec.leos.repository.store.WorkspaceRepository;
import eu.europa.ec.leos.security.LeosPermissionAuthorityMapHelper;
import eu.europa.ec.leos.services.Annotate.AnnotateService;
import eu.europa.ec.leos.services.compare.ContentComparatorService;
import eu.europa.ec.leos.services.content.processor.AttachmentProcessor;
import eu.europa.ec.leos.services.converter.ProposalConverterService;
import eu.europa.ec.leos.services.document.AGEReportService;
import eu.europa.ec.leos.services.document.AnnexService;
import eu.europa.ec.leos.services.document.BillService;
import eu.europa.ec.leos.services.document.MemorandumService;
import eu.europa.ec.leos.services.export.ExportOptions;
import eu.europa.ec.leos.services.export.ExportResource;
import eu.europa.ec.leos.services.export.LegPackage;
import eu.europa.ec.leos.services.export.ZipPackageUtil;
import eu.europa.ec.leos.services.rendition.HtmlRenditionProcessor;
import eu.europa.ec.leos.services.store.LegServiceImpl;
import eu.europa.ec.leos.services.support.xml.AGEXmlNodeConfigHelper;
import eu.europa.ec.leos.services.support.xml.XmlNodeConfig;
import eu.europa.ec.leos.services.support.xml.XmlNodeConfigHelper;
import eu.europa.ec.leos.services.support.xml.XmlNodeProcessor;
import eu.europa.ec.leos.services.toc.StructureContext;
import io.atlassian.fugue.Option;

@Primary
@Service
public class AGELegServicesImpl extends LegServiceImpl {

	private static final Logger LOG = LoggerFactory.getLogger(AGELegServicesImpl.class);

	@Autowired
	AGEReportService reportService;
	
	@Autowired
	AGELegServicesImpl(PackageRepository packageRepository, WorkspaceRepository workspaceRepository,
			AttachmentProcessor attachmentProcessor, XmlNodeProcessor xmlNodeProcessor,
			XmlNodeConfigHelper xmlNodeConfigHelper, AnnotateService annotateService,
			HtmlRenditionProcessor htmlRenditionProcessor, ProposalConverterService proposalConverterService,
			LeosPermissionAuthorityMapHelper authorityMapHelper, ContentComparatorService compareService,
			MessageHelper messageHelper, Provider<StructureContext> structureContextProvider, BillService billService,
			MemorandumService memorandumService, AnnexService annexService) {
		super(packageRepository, workspaceRepository, attachmentProcessor, xmlNodeProcessor, xmlNodeConfigHelper,
				annotateService, htmlRenditionProcessor, proposalConverterService, authorityMapHelper, compareService,
				messageHelper, structureContextProvider, billService, memorandumService, annexService);
	}
	

	@Override
	public LegPackage createLegPackage(File legFile, ExportOptions exportOptions) throws IOException {
		// legFile will be deleted after createProposalFromLegFile(), so we save the
		// bytes in a temporary file
		File legFileTemp = File.createTempFile("RENDITION_", ".leg");
		FileUtils.copyFile(legFile, legFileTemp);

		final DocumentVO proposalVO = proposalConverterService.createProposalFromLegFile(legFile,
				new DocumentVO(LeosCategory.PROPOSAL), false);
		final byte[] proposalXmlContent = proposalVO.getSource();
		ExportResource proposalExportResource = new ExportResource(LeosCategory.PROPOSAL);
		final Map<String, String> proposalRefsMap = buildProposalExportResource(proposalExportResource,
				proposalXmlContent);
		proposalExportResource.setExportOptions(exportOptions);
		final DocumentVO memorandumVO = proposalVO.getChildDocument(LeosCategory.MEMORANDUM);
		final byte[] memorandumXmlContent = memorandumVO.getSource();
		final ExportResource memorandumExportResource = buildExportResourceMemorandum(proposalRefsMap,
				memorandumXmlContent);
		proposalExportResource.addChildResource(memorandumExportResource);

		// add reports to proposalExportResource
		final List<DocumentVO> reportsVO = proposalVO.getChildDocuments(LeosCategory.REPORT);
		reportsVO.forEach((reportVO) -> {
			final byte[] reportXmlContent = reportVO.getSource();
			final int docNumber = Integer.parseInt(reportVO.getMetadata().getIndex());
			final String resourceId = proposalRefsMap.entrySet().stream()
					.filter(e -> e.getKey().equals(reportVO.getId())).map(Map.Entry::getValue).findFirst().get();
			final ExportResource reportExportResource = buildExportResourceAnnex(docNumber, resourceId,
					reportXmlContent);
			proposalExportResource.addChildResource(reportExportResource);
		});

		final DocumentVO billVO = proposalVO.getChildDocument(LeosCategory.BILL);
		final byte[] billXmlContent = billVO.getSource();
		final ExportResource billExportResource = buildExportResourceBill(proposalRefsMap, billXmlContent);

		// add annexes to billExportResource
		final Map<String, String> attachmentIds = attachmentProcessor.getAttachmentsIdFromBill(billXmlContent);
		final List<DocumentVO> annexesVO = billVO.getChildDocuments(LeosCategory.ANNEX);
		annexesVO.forEach((annexVO) -> {
			final byte[] annexXmlContent = annexVO.getSource();
			final int docNumber = Integer.parseInt(annexVO.getMetadata().getIndex());
			final String resourceId = attachmentIds.entrySet().stream().filter(e -> e.getKey().equals(annexVO.getId()))
					.map(Map.Entry::getValue).findFirst().get();
			final ExportResource annexExportResource = buildExportResourceAnnex(docNumber, resourceId, annexXmlContent);
			billExportResource.addChildResource(annexExportResource);
		});

		proposalExportResource.addChildResource(billExportResource);

		LegPackage legPackage = new LegPackage();
		legPackage.setFile(legFileTemp);
		legPackage.setExportResource(proposalExportResource);
		return legPackage;
	}

//    protected Map<String, String> buildProposalExportResource(ExportResource exportResource, byte[] xmlContent) {
//        Map<String, XmlNodeConfig> config = new HashMap<>();
//        config.putAll(xmlNodeConfigHelper.getProposalComponentsConfig(LeosCategory.MEMORANDUM, "xml:id"));
//        config.putAll(xmlNodeConfigHelper.getProposalComponentsConfig(LeosCategory.MEMORANDUM, "href"));
//        config.putAll(xmlNodeConfigHelper.getProposalComponentsConfig(LeosCategory.BILL, "xml:id"));
//        config.putAll(xmlNodeConfigHelper.getProposalComponentsConfig(LeosCategory.BILL, "href"));
//        config.putAll(xmlNodeConfigHelper.getProposalComponentsConfig(LeosCategory.REPORT, "xml:id"));
//        config.putAll(xmlNodeConfigHelper.getProposalComponentsConfig(LeosCategory.REPORT, "href"));
//        config.putAll(xmlNodeConfigHelper.getConfig(LeosCategory.PROPOSAL));
//        Map<String, String> proposalRefsMap = xmlNodeProcessor.getValuesFromXml(xmlContent,
//                new String[]{XmlNodeConfigHelper.PROPOSAL_DOC_COLLECTION, XmlNodeConfigHelper.DOC_REF_COVER,
//                        LeosCategory.MEMORANDUM.name() + "_xml:id",
//                        LeosCategory.MEMORANDUM.name() + "_href",
//                        LeosCategory.BILL.name() + "_xml:id",
//                        LeosCategory.BILL.name() + "_href",
//                        LeosCategory.REPORT.name() + "_xml:id",
//                        LeosCategory.REPORT.name() + "_href"
//                },
//                config);
//        
//        Map<String, String> proposalComponentRefs = new HashMap<>();
//        proposalComponentRefs.put(XmlNodeConfigHelper.DOC_REF_COVER, proposalRefsMap.get(XmlNodeConfigHelper.DOC_REF_COVER));
//        
//        exportResource.setResourceId(proposalRefsMap.get(XmlNodeConfigHelper.PROPOSAL_DOC_COLLECTION));
//        exportResource.setComponentsIdsMap(proposalComponentRefs);
//        return proposalRefsMap;
//    }

	/**
	 * Creates the LegPackage, which is the logical representation of the leg file,
	 * for the given proposalId.
	 *
	 * @param proposalId       proposalId for which we need to create the LegPackage
	 * @param exportOptions    exportOptions to select what needs to be exported
	 * @param versionToCompare Required in case ExportOptions.ComparisonType.DOUBLE
	 *                         is selected for export of double comparison result
	 * @return LegPackage used to be sent to Toolbox for PDF/LegisWrite generation.
	 */
	@Override
	public LegPackage createLegPackage(String proposalId, ExportOptions exportOptions, XmlDocument versionToCompare)
			throws IOException {
		LOG.trace("Creating Leg Package... [documentId={}]", proposalId);

		final LegPackage legPackage = new LegPackage();
		final LeosPackage leosPackage = packageRepository.findPackageByDocumentId(proposalId);
		final Map<String, Object> contentToZip = new HashMap<>();
		final ExportResource exportProposalResource = new ExportResource(LeosCategory.PROPOSAL);
		exportProposalResource.setExportOptions(exportOptions);

		// 1. Add Proposal to package
		final Proposal proposal = workspaceRepository.findDocumentById(proposalId, Proposal.class, true);
		final Map<String, String> proposalRefsMap = enrichZipWithProposal(contentToZip, exportProposalResource,
				proposal);

		// 2. Depending on ExportOptions FileType add documents to package
		Bill bill;
		ExportResource exportBillResource;
		switch (exportOptions.getFileType()) {
		case "LEGALTEXT": // TODO: Get from LeosCategory once Kotlin code is removed
			bill = packageRepository.findDocumentByPackagePathAndName(leosPackage.getPath(),
					proposalRefsMap.get(LeosCategory.BILL.name() + "_href"), Bill.class);
			addBillToPackage(contentToZip, exportProposalResource, proposalRefsMap, bill, versionToCompare);
			legPackage.addContainedFile(bill.getVersionedReference());
			break;
		case "MEMORANDUM":
			if (proposalRefsMap.get(LeosCategory.MEMORANDUM.name() + "_href") != null) {
				addMemorandumToPackage(leosPackage, contentToZip, exportProposalResource, proposalRefsMap, legPackage);
			}
			break;
		case "ANNEX":
			bill = packageRepository.findDocumentByPackagePathAndName(leosPackage.getPath(),
					proposalRefsMap.get(LeosCategory.BILL.name() + "_href"), Bill.class);

			Content content = bill.getContent().getOrNull();
			exportBillResource = buildExportResourceBill(proposalRefsMap, content.getSource().getBytes());
			exportBillResource.setExportOptions(exportOptions);
			exportProposalResource.addChildResource(exportBillResource);
			legPackage.addContainedFile(bill.getVersionedReference());
			addAnnexToPackage(leosPackage, bill, contentToZip, ((Annex) versionToCompare), exportBillResource,
					legPackage);
			break;
		case "REPORT":
//                bill = packageRepository.findDocumentByPackagePathAndName(leosPackage.getPath(),
//                        proposalRefsMap.get(LeosCategory.BILL.name() + "_href"), Bill.class);
//                
//                Content contentRep = bill.getContent().getOrNull();
//                exportBillResource = buildExportResourceBill(proposalRefsMap, contentRep.getSource().getBytes());
//                exportBillResource.setExportOptions(exportOptions);
//                exportProposalResource.addChildResource(exportBillResource);
//                legPackage.addContainedFile(bill.getVersionedReference());
//                addReportToPackage(leosPackage, bill, contentToZip, ((AGEReport) versionToCompare), exportBillResource, legPackage);
//                
//                
//    			if (proposalRefsMap.get(LeosCategory.REPORT.name() + "_href") != null) {
//    				addReportToPackage(leosPackage, contentToZip, exportProposalResource, proposalRefsMap, legPackage);
//    			}
//                final List<DocumentVO> reportsVO = proposalVO.getChildDocuments(LeosCategory.REPORT);
//        		reportsVO.forEach((reportVO) -> {
//        			final byte[] reportXmlContent = reportVO.getSource();
//        			final int docNumber = Integer.parseInt(reportVO.getMetadata().getIndex());
//        			final String resourceId = proposalRefsMap.entrySet().stream().filter(e -> e.getKey().equals(reportVO.getId()))
//        					.map(Map.Entry::getValue).findFirst().get();
//        			final ExportResource reportExportResource = buildExportResourceAnnex(docNumber, resourceId,
//        					reportXmlContent);
//        			proposalExportResource.addChildResource(reportExportResource);
//        		});

			break;
		default:
			bill = packageRepository.findDocumentByPackagePathAndName(leosPackage.getPath(),
					proposalRefsMap.get(LeosCategory.BILL.name() + "_href"), Bill.class);
			exportBillResource = addBillToPackage(contentToZip, exportProposalResource, proposalRefsMap, bill,
					versionToCompare);
			legPackage.addContainedFile(bill.getVersionedReference());
			if (proposalRefsMap.get(LeosCategory.MEMORANDUM.name() + "_href") != null) {
				addMemorandumToPackage(leosPackage, contentToZip, exportProposalResource, proposalRefsMap, legPackage);
			}
			addAnnexToPackage(leosPackage, bill, contentToZip, null, exportBillResource, legPackage);
			addReportToPackage(leosPackage, contentToZip, null, exportProposalResource, legPackage);
//			if (proposalRefsMap.get(LeosCategory.REPORT.name() + "_href") != null) {
//    			addReportToPackage(leosPackage, contentToZip, exportProposalResource, proposalRefsMap, legPackage);
//			}
		}

		// 3. Add TOC
		enrichZipWithToc(contentToZip);

		// 4. Add media
		final List<MediaDocument> mediaDocs = packageRepository.findDocumentsByPackagePath(leosPackage.getPath(),
				MediaDocument.class, true);
		enrichZipWithMedia(contentToZip, mediaDocs);
		legPackage.setFile(ZipPackageUtil
				.zipFiles(proposalRefsMap.get(XmlNodeConfigHelper.PROPOSAL_DOC_COLLECTION) + ".leg", contentToZip));
		legPackage.setExportResource(exportProposalResource);
		return legPackage;
	}

	// addToPackage

	@Override
	protected void addAnnexToPackage(final LeosPackage leosPackage, Bill bill, final Map<String, Object> contentToZip,
			Annex versionToCompare, ExportResource exportProposalResource, LegPackage legPackage) {
		Content content = bill.getContent().getOrError(() -> "Bill content is required!");
		String annexId = versionToCompare != null ? versionToCompare.getMetadata().get().getRef() : null;
		byte[] xmlContent = content.getSource().getBytes();
		final Map<String, String> attachmentIds = attachmentProcessor.getAttachmentsIdFromBill(xmlContent);
		final String annexStyleSheet = LeosCategory.AGE.name().toUpperCase() + LeosCategory.ANNEX.name().toLowerCase() + STYLE_SHEET_EXT;
		attachmentIds.forEach((href, id) -> {
			if (annexId == null || href.equals(annexId)) {
				final Annex annex = packageRepository.findDocumentByPackagePathAndName(leosPackage.getPath(), href,
						Annex.class);
				enrichZipWithAnnex(contentToZip, exportProposalResource, annexStyleSheet, annex, versionToCompare, id,
						href);
				legPackage.addContainedFile(annex.getVersionedReference());
			}
		});
		if (!attachmentIds.isEmpty()) {
			// Add annex style only if at least one is present
			addResourceToZipContent(contentToZip, annexStyleSheet, STYLES_SOURCE_PATH, STYLE_DEST_DIR);
		}
	}

	protected void addReportToPackage(final LeosPackage leosPackage, final Map<String, Object> contentToZip,
			AGEReport versionToCompare, ExportResource exportProposalResource, LegPackage legPackage) {

		List<AGEReport> reports = packageRepository.findDocumentsByPackagePath(leosPackage.getPath(), AGEReport.class,
				false);
		final String reportStyleSheet = LeosCategory.AGE.name().toUpperCase() + LeosCategory.REPORT.name().toLowerCase() + STYLE_SHEET_EXT;
		// Obtenemos solo el primero y lo zipeamos y añadimos al package
//		AGEReport report = reportService.findReport(reports.get(0).getId());
//		enrichZipWithReport(contentToZip, exportProposalResource, reportStyleSheet, report, versionToCompare, report.getId());
//		legPackage.addContainedFile(report.getVersionedReference());
		// si no obtenemos solo el primero y ponemos varios habría que hacer un bucle,
		// voy a probar a poner solo el primero.
		reports.forEach((r) -> {
			AGEReport report = reportService.findReport(r.getId());
			enrichZipWithReport(contentToZip, exportProposalResource, reportStyleSheet, report, versionToCompare,
					report.getId());
			legPackage.addContainedFile(report.getVersionedReference());
		});
		if (!reports.isEmpty()) {
			// Add annex style only if at least one is present
			addResourceToZipContent(contentToZip, reportStyleSheet, STYLES_SOURCE_PATH, STYLE_DEST_DIR);
		}
	}

	// EnrichZip

	private void enrichZipWithReport(final Map<String, Object> contentToZip, ExportResource exportProposalResource,
			String reportStyleSheet, AGEReport report, AGEReport versionToCompare, String resourceId) {

		ExportOptions exportOptions = exportProposalResource.getExportOptions();
		final Content reportContent = report.getContent().getOrError(() -> "Report content is required!");
		byte[] xmlReportContent = reportContent.getSource().getBytes();
		xmlReportContent = addObjectIdToReport(xmlReportContent, report);

		contentToZip.put(report.getName(), xmlReportContent);
		if (exportOptions.isConvertAnnotations()) {
			String reportAnnotations = annotateService.getAnnotations(report.getName());
			addAnnotateToZipContent(contentToZip, report.getName(), reportAnnotations);
		}

		addResourceToZipContent(contentToZip, reportStyleSheet, STYLES_SOURCE_PATH, STYLE_DEST_DIR);
		String reportTocJson = getTocAsJson(reportService.getTableOfContent(report, TocMode.SIMPLIFIED_CLEAN));
		addHtmlRendition(contentToZip, report, reportStyleSheet, reportTocJson);

		int docNumber = report.getMetadata().get().getIndex();
		final ExportResource reportExportResource = buildExportResourceReport(docNumber, resourceId, xmlReportContent);
		exportProposalResource.addChildResource(reportExportResource);

	}

	private ExportResource buildExportResourceReport(int docNumber, String resourceId, byte[] xmlContent) {
		ExportResource reportExportResource = new ExportResource(LeosCategory.REPORT);
		reportExportResource.setResourceId(resourceId);
		reportExportResource.setDocNumber(docNumber);
		setComponentsRefs(LeosCategory.REPORT, reportExportResource, xmlContent);
		return reportExportResource;
	}

	private byte[] addObjectIdToReport(byte[] xmlContent, AGEReport report) {
		Option<AGEReportMetadata> metadataOption = report.getMetadata();
		AGEReportMetadata metadata = metadataOption.get().withObjectId(report.getId());
		xmlContent = xmlNodeProcessor.setValuesInXml(xmlContent, AGEXmlNodeConfigHelper.createValueMap(metadata),
				xmlNodeConfigHelper.getConfig(metadata.getCategory()));
		return xmlContent;
	}

}
