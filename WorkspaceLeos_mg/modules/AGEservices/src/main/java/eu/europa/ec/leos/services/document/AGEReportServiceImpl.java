/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.services.document;

import static eu.europa.ec.leos.services.support.xml.XmlHelper.DOC;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.google.common.base.Stopwatch;

import cool.graph.cuid.Cuid;
import eu.europa.ec.leos.domain.cmis.Content;
import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.AGEReport;
import eu.europa.ec.leos.domain.cmis.document.Annex;
import eu.europa.ec.leos.domain.cmis.metadata.AGEReportMetadata;
import eu.europa.ec.leos.domain.cmis.metadata.AnnexMetadata;
import eu.europa.ec.leos.domain.common.TocMode;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.action.VersionVO;
import eu.europa.ec.leos.model.report.AGEReportStructureType;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.repository.document.AGEReportRepository;
import eu.europa.ec.leos.repository.store.PackageRepository;
import eu.europa.ec.leos.services.document.util.DocumentVOProvider;
import eu.europa.ec.leos.services.support.VersionsUtil;
import eu.europa.ec.leos.services.support.xml.AGEXmlNodeConfigHelper;
import eu.europa.ec.leos.services.support.xml.NumberProcessor;
import eu.europa.ec.leos.services.support.xml.XmlContentProcessor;
import eu.europa.ec.leos.services.support.xml.XmlNodeConfigHelper;
import eu.europa.ec.leos.services.support.xml.XmlNodeProcessor;
import eu.europa.ec.leos.services.support.xml.XmlTableOfContentHelper;
import eu.europa.ec.leos.services.validation.ValidationService;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;


@Service
public class AGEReportServiceImpl implements AGEReportService {

	private static final Logger LOG = LoggerFactory.getLogger(AGEReportServiceImpl.class);

	private static final String REPORT_NAME_PREFIX = "report_";
	private static final String REPORT_DOC_EXTENSION = ".xml";

	private final AGEReportRepository reportRepository;
	private final XmlNodeProcessor xmlNodeProcessor;
	private final XmlContentProcessor xmlContentProcessor;
	private final AGEXmlNodeConfigHelper xmlNodeConfigHelper;
	private final DocumentVOProvider documentVOProvider;
	private final ValidationService validationService;
	private final PackageRepository packageRepository;
	private final NumberProcessor numberingProcessor;
	private final XmlTableOfContentHelper xmlTableOfContentHelper;
	private final MessageHelper messageHelper;
	
	@Autowired
	AGEReportServiceImpl(
			AGEReportRepository reportRepository,
			PackageRepository packageRepository,
			XmlNodeProcessor xmlNodeProcessor,
			XmlContentProcessor xmlContentProcessor,
			NumberProcessor numberingProcessor,
			AGEXmlNodeConfigHelper xmlNodeConfigHelper,
			XmlTableOfContentHelper xmlTableOfContentHelper,
			ValidationService validationService,
			DocumentVOProvider documentVOProvider,
			MessageHelper messageHelper
		) {
		this.reportRepository = reportRepository;
		this.xmlNodeProcessor = xmlNodeProcessor;
		this.xmlContentProcessor = xmlContentProcessor;
		this.xmlNodeConfigHelper = xmlNodeConfigHelper;
		this.numberingProcessor = numberingProcessor;
		this.validationService = validationService;
		this.documentVOProvider = documentVOProvider;
		this.packageRepository = packageRepository;
		this.xmlTableOfContentHelper = xmlTableOfContentHelper;
		this.messageHelper = messageHelper;
	}

	@Override
	public AGEReport createReport(String templateId, String path, AGEReportMetadata metadata, String actionMessage,
			byte[] content) {
		LOG.trace("Creating Report... [templateId={}, path={}, metadata={}]", templateId, path, metadata);
		String name = generateReportName();
		metadata = metadata.withRef(name);// FIXME: a better scheme needs to be devised
		AGEReport report = reportRepository.createReport(templateId, path, name, metadata);
		byte[] updatedBytes = updateDataInXml((content == null) ? getContent(report) : content, metadata);
		return reportRepository.updateReport(report.getId(), metadata, updatedBytes, VersionType.MINOR, actionMessage);
	}
	
    @Override
    public AGEReport findReportByRef(String ref) {
        LOG.trace("Finding Report by ref... [ref=" + ref + "]");
        return reportRepository.findReportByRef(ref);
    }

	@Override
	public AGEReport createReportFromContent(String path, AGEReportMetadata metadata, String actionMessage, byte[] content) {
		LOG.trace("Creating Report From Content... [path={}, metadata={}]", path, metadata);
		String name = generateReportName();
		metadata = metadata.withRef(name);// FIXME: a better scheme needs to be devised
		AGEReport report = reportRepository.createReportFromContent(path, name, metadata, content);
		return reportRepository.updateReport(report.getId(), metadata, content, VersionType.MINOR, actionMessage);
	}

	@Override
	public void deleteReport(AGEReport report) {
		LOG.trace("Deleting Report... [id={}]", report.getId());
		reportRepository.deleteReport(report.getId());
	}

	@Override
	public AGEReport findReport(String id) {
		LOG.trace("Finding Report... [it={}]", id);
		return reportRepository.findReportById(id, true);
	}

	@Override
	@Cacheable(value = "docVersions", cacheManager = "cacheManager")
	public AGEReport findReportVersion(String id) {
		LOG.trace("Finding Report version... [it={}]", id);
		return reportRepository.findReportById(id, false);
	}

	@Override
	public AGEReport updateReport(AGEReport report, byte[] updatedReportContent, boolean major, String comment) {
		LOG.trace("Updating Report Xml Content... [id={}]", report.getId());

		report = reportRepository.updateReport(report.getId(), updatedReportContent, VersionType.MAJOR, comment);

		// call validation on document with updated content
		validationService.validateDocumentAsync(documentVOProvider.createDocumentVO(report, updatedReportContent));

		return report;
	}

    @Override
    public List<VersionVO> getAllVersions(String documentId, String docRef) {
        // TODO temporary call. paginated loading will be implemented in the future Story
        List<AGEReport> majorVersions = findAllMajors(docRef, 0, 9999);
        LOG.trace("Found {} majorVersions for [id={}]", majorVersions.size(), documentId);
        
        List<VersionVO> majorVersionsVO = VersionsUtil.buildVersionVO(majorVersions, messageHelper);
        return majorVersionsVO;
    }
    
    @Override
    public AGEReport updateReport(AGEReport report, byte[] updatedReportContent, VersionType versionType, String comment) {
        LOG.trace("Updating Annex Xml Content... [id={}]", report.getId());
        
        report = reportRepository.updateReport(report.getId(), updatedReportContent, versionType, comment); 
        
        //call validation on document with updated content
        validationService.validateDocumentAsync(documentVOProvider.createDocumentVO(report, updatedReportContent));

        return report;
    }
    
    @Override
    public int findAllMinorsCountForIntermediate(String docRef, String currIntVersion) {
        final String prevVersion = calculatePreviousVersion(currIntVersion);
        return reportRepository.findAllMinorsCountForIntermediate(docRef, currIntVersion, prevVersion);
    }
    
    private String calculatePreviousVersion(String currIntVersion) {
        final String prevVersion;
        String[] str = currIntVersion.split("\\.");
        if (str.length != 2) {
            throw new IllegalArgumentException("CMIS Version number should be in the format x.y");
        } else {
            int curr = Integer.parseInt(str[0]);
            int prev = curr - 1;
            prevVersion = prev + "." + "0";
        }
        return prevVersion;
    }
    
    @Override
    public List<AGEReport> findAllMinorsForIntermediate(String docRef, String currIntVersion, int startIndex, int maxResults) {
        final String prevIntVersion = calculatePreviousVersion(currIntVersion);
        return reportRepository.findAllMinorsForIntermediate(docRef, currIntVersion, prevIntVersion, startIndex, maxResults);
    }
    
    @Override
    public List<AGEReport> findAllMajors(String docRef, int startIndex, int maxResults) {
        return reportRepository.findAllMajors(docRef, startIndex, maxResults);
    }
    
    @Override
    public Integer findAllMajorsCount(String docRef) {
        return reportRepository.findAllMajorsCount(docRef);
    }
    
    @Override
    public Integer findRecentMinorVersionsCount(String documentId, String documentRef) {
        return reportRepository.findRecentMinorVersionsCount(documentId, documentRef);
    }

    @Override
    public List<AGEReport> findRecentMinorVersions(String documentId, String documentRef, int startIndex, int maxResults) {
        return reportRepository.findRecentMinorVersions(documentId, documentRef, startIndex, maxResults);
    }
	
	@Override
	public AGEReport updateReport(String reportId, AGEReportMetadata updatedMetadata) {
		LOG.trace("Updating Report... [id={}, updatedMetadata={}]", reportId, updatedMetadata);
		return reportRepository.updateReport(reportId, updatedMetadata);
	}

	@Override
	public AGEReport updateReportWithMilestoneComments(AGEReport report, List<String> milestoneComments, VersionType versionType,
			String comment) {
		LOG.trace("Updating Report... [id={}, milestoneComments={}, major={}, comment={}]", report.getId(), milestoneComments, versionType, comment);
		final byte[] updatedBytes = getContent(report);
		report = reportRepository.updateMilestoneComments(report.getId(), milestoneComments, updatedBytes, versionType, comment);
		return report;
	}

	@Override
	public AGEReport updateReportWithMilestoneComments(String reportId, List<String> milestoneComments) {
		LOG.trace("Updating Report... [id={}, milestoneComments={}]", reportId, milestoneComments);
		return reportRepository.updateMilestoneComments(reportId, milestoneComments);
	}

	@Override
	public List<AGEReport> findVersions(String id) {
		LOG.trace("Finding Report versions... [id={}]", id);
		// LEOS-2813 We have memory issues is we fetch the content of all versions.
		return reportRepository.findReportVersions(id, false);
	}

	@Override
	public AGEReport createVersion(String id, VersionType versionType, String comment) {
		LOG.trace("Creating Report version... [id={}, major={}, comment={}]", id, versionType, comment);
		final AGEReport report = findReport(id);
		final AGEReportMetadata metadata = report.getMetadata().getOrError(() -> "Report metadata is required!");
		final Content content = report.getContent().getOrError(() -> "Report content is required!");
		final byte[] contentBytes = content.getSource().getBytes();
		return reportRepository.updateReport(id, metadata, contentBytes, VersionType.MAJOR, comment);
	}

    @Override
    public List<TableOfContentItemVO> getTableOfContent(AGEReport report, TocMode mode) {
        Validate.notNull(report, "Report is required");
        final Content content = report.getContent().getOrError(() -> "Report content is required!");
        final byte[] reportContent = content.getSource().getBytes();
        return xmlTableOfContentHelper.buildTableOfContent(DOC, reportContent, mode);
    }

	@Override
	public AGEReport saveTableOfContent(AGEReport report, List<TableOfContentItemVO> tocList, AGEReportStructureType structureType, String actionMsg, User user) {
		Validate.notNull(report, "Report is required");
		Validate.notNull(tocList, "Table of content list is required");
		byte[] newXmlContent;

		newXmlContent = xmlContentProcessor.createDocumentContentWithNewTocList(tocList, getContent(report), user);
		switch(structureType) {
		case ARTICLE:
			newXmlContent = numberingProcessor.renumberArticles(newXmlContent);
			break;
		case LEVEL:
			newXmlContent = numberingProcessor.renumberLevel(newXmlContent);
			break;
		}
		newXmlContent = xmlContentProcessor.doXMLPostProcessing(newXmlContent);

		return updateReport(report, newXmlContent, false, actionMsg);
	}
	
    @Override
    public List<String> getAncestorsIdsForElementId(AGEReport report, List<String> elementIds) {
        Validate.notNull(report, "Annex is required");
        Validate.notNull(elementIds, "Element id is required");
        List<String> ancestorIds = new ArrayList<String>();
        byte[] content = getContent(report);
        for (String elementId : elementIds) {
            ancestorIds.addAll(xmlContentProcessor.getAncestorsIdsForElementId(content, elementId));
        }
        return ancestorIds;
    }
    
    @Override
    public AGEReport updateReportWithMetadata(AGEReport report, byte[] updatedReportContent, AGEReportMetadata metadata, VersionType versionType, String comment) {
        LOG.trace("Updating Annex... [id={}, updatedMetadata={}, versionType={}, comment={}]", report.getId(), metadata, versionType, comment);
        Stopwatch stopwatch = Stopwatch.createStarted();
        updatedReportContent = updateDataInXml(updatedReportContent, metadata);
        
        report = reportRepository.updateReport(report.getId(), metadata, updatedReportContent, versionType, comment);
        
        //call validation on document with updated content
        validationService.validateDocumentAsync(documentVOProvider.createDocumentVO(report, updatedReportContent));
        
        LOG.trace("Updated Annex ...({} milliseconds)", stopwatch.elapsed(TimeUnit.MILLISECONDS));
        return report;
    }

	@Override
	public AGEReport findReportByPackagePath(String path) {
		LOG.trace("Finding Report by package path... [path={}]", path);
		// FIXME can be improved, now we don't fetch ALL docs because it's loaded later
		// the one needed,
		// this can be improved adding a page of 1 item or changing the method/query.
		List<AGEReport> docs = packageRepository.findDocumentsByPackagePath(path, AGEReport.class, false);
		AGEReport report = findReport(docs.get(0).getId());
		return report;
	}

	private byte[] getContent(AGEReport report) {
		final Content content = report.getContent().getOrError(() -> "Report content is required!");
		return content.getSource().getBytes();
	}

	private byte[] updateDataInXml(final byte[] content, AGEReportMetadata dataObject) {
		byte[] updatedBytes = xmlNodeProcessor.setValuesInXml(content, xmlNodeConfigHelper.createValueMap(dataObject),
				xmlNodeConfigHelper.getConfig(dataObject.getCategory()));
		return xmlContentProcessor.doXMLPostProcessing(updatedBytes);
	}

	private String generateReportName() {
		return REPORT_NAME_PREFIX + Cuid.createCuid() + REPORT_DOC_EXTENSION;
	}

	@Override
	public AGEReport updateReport(AGEReport report, AGEReportMetadata metadata, VersionType version, String comment) {
        LOG.trace("Updating Report... [id={}, updatedMetadata={}, major={}, comment={}]", report.getId(), metadata, version, comment);
        Stopwatch stopwatch = Stopwatch.createStarted();
        byte[] updatedBytes = updateDataInXml(getContent(report), metadata); //FIXME: Do we need latest data again??
        
        report = reportRepository.updateReport(report.getId(), metadata, updatedBytes, version, comment);
        
        //call validation on document with updated content
        validationService.validateDocumentAsync(documentVOProvider.createDocumentVO(report, updatedBytes));
        
        LOG.trace("Updated Report ...({} milliseconds)", stopwatch.elapsed(TimeUnit.MILLISECONDS));
        return report;
	}

}
