package eu.europa.ec.leos.services.content.processor;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.google.common.base.Stopwatch;

import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.security.LeosPermission;
import eu.europa.ec.leos.services.support.xml.freemarker.XmlNodeModelHandler;
import freemarker.ext.dom.NodeModel;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateHashModel;

@Primary
@Service
public class AGETransformationServiceImpl extends TransformationServiceImpl implements AGETransformationService{

    private static final Logger LOG = LoggerFactory.getLogger(AGETransformationServiceImpl.class);
    
    @Value("${leos.freemarker.ftl.documentView}")
    private String editableXHtmlTemplate;
    
    @Value("${leos.freemarker.ftl.import.project}")
    private String importProjectXHtmlTemplate;
    
    @Value("${leos.freemarker.ftl.amendment}")
    private String amendmentXHtmlTemplate;

    private Configuration freemarkerConfiguration;

    private TemplateHashModel enumModels;

    @Autowired
    public AGETransformationServiceImpl(Configuration freemarkerConfiguration, TemplateHashModel enumModels){
        super(freemarkerConfiguration, enumModels);
        this.freemarkerConfiguration = freemarkerConfiguration;
        this.enumModels = enumModels;
    }
    
    @Override
    public String toEditableXml(final InputStream documentStream, String contextPath, LeosCategory category, List<LeosPermission> permissions) {
        String template;
        switch (category){
            case ANNEX:
                template = editableXHtmlTemplate;
                break;
            case REPORT:
                template = editableXHtmlTemplate;
                break;
            case CANTODORADO:
                template = editableXHtmlTemplate;
                break;
            case MEMORANDUM:
                template = editableXHtmlTemplate;
                break;
            case BILL:
                template = editableXHtmlTemplate;
                break;
			case MAIN:
                template = editableXHtmlTemplate;
                break;
            default:
                throw new UnsupportedOperationException("No transformation supported for this category");
        }
        return transform(documentStream, template, contextPath, permissions);
    }
    
    /**
     *  Transforms a documentStream using a freemarker template 
     * @param documentStream
     * @param templateName
     * @param contextPath
     * @param permissions list of actions (permissions) that a user can perform on the given document.
     * @return
     */
    private String transform(InputStream documentStream, String templateName, String contextPath, List<LeosPermission> permissions) {
        LOG.trace("Transforming document using {} template...", templateName);
        Stopwatch stopwatch = Stopwatch.createStarted();
        try {
            StringWriter outputWriter = new StringWriter();
            Template template = freemarkerConfiguration.getTemplate(templateName);

            NodeModel nodeModel = XmlNodeModelHandler.parseXmlStream(documentStream);

            Map<String, Object> headers = new HashMap<>();
            headers.put("contextPath", contextPath);
            headers.put("userPermissions", (permissions != null) ? permissions : Collections.emptyList());
            headers.put("LeosPermission", enumModels.get(LeosPermission.class.getName()));

            Map<String, Object> root = new HashMap<>();
            root.put("xml_data", nodeModel);
            root.put("headers", headers);

            template.process(root, outputWriter);
            return outputWriter.getBuffer().toString();
        } catch (Exception ex) {
            LOG.error("Transformation error!", ex);
            throw new RuntimeException(ex);
        } finally {
            try {
                documentStream.close();
            } catch (IOException ioe){
                 //omitted
            }
            stopwatch.stop();
            LOG.trace("Transformation finished! ({} milliseconds)", stopwatch.elapsed(TimeUnit.MILLISECONDS));
        }
    }
    
    @Override
    public String toImportProjectXml(InputStream documentStream, String contextPath, List<LeosPermission> permissions) {
        return transform(documentStream, importProjectXHtmlTemplate, contextPath, permissions);
    }

    @Override
    public String toImportBOEXml(InputStream documentStream, String contextPath, List<LeosPermission> permissions) {
        return transform(documentStream, importProjectXHtmlTemplate, contextPath, permissions);
    }
    
    @Override
    public String toAmendmentDOUEXml(InputStream documentStream, String contextPath, List<LeosPermission> permissions) {
        return transform(documentStream, amendmentXHtmlTemplate, contextPath, permissions);
    }

}