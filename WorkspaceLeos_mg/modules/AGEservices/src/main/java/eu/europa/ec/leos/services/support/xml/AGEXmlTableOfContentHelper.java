package eu.europa.ec.leos.services.support.xml;

import static eu.europa.ec.leos.services.support.xml.AGEVTDUtils.extractNumberForType;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.LEOS_ORIGIN_ATTR_CN;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.LEOS_ORIGIN_ATTR_EC;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.PROVISO;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.HEADING_BYTES;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.HEADING_START_TAG;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.NUM_BYTES;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.NUM_START_TAG;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.XMLID;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.extractNumber;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.getFragmentAsString;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.getNumSoftActionAttribute;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.getOriginAttribute;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.getSoftActionAttribute;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.getSoftActionRootAttribute;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.getSoftDateAttribute;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.getSoftMovedFromAttribute;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.getSoftMovedToAttribute;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.getSoftUserAttribute;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.navigateToElementByNameAndId;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.updateOriginAttribute;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.HEADING;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.INDENT;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.INTRO;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.LEVEL;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.LEVEL_NUM_SEPARATOR;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.LIST;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.NUM;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.PARAGRAPH;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.POINT;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.SECTION;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.ximpleware.NavException;
import com.ximpleware.VTDNav;

import eu.europa.ec.leos.domain.common.TocMode;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.action.SoftActionType;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;
import eu.europa.ec.leos.vo.toc.TocItem;
import eu.europa.ec.leos.vo.toc.TocItemUtils;

@Component
@Primary
public class AGEXmlTableOfContentHelper extends XmlTableOfContentHelper {

    private static final Logger LOG = LoggerFactory.getLogger(AGEXmlTableOfContentHelper.class);
   
    public static List<TableOfContentItemVO> getAllChildTableOfContentItems(List<TocItem> tocItems, Map<TocItem, List<TocItem>> tocRules, VTDNav contentNavigator, TocMode mode)
            throws NavException {
        int currentIndex = contentNavigator.getCurrentIndex();
        List<TableOfContentItemVO> itemVOList = new ArrayList<>();
        try {
            if (contentNavigator.toElement(VTDNav.FIRST_CHILD)) {
                addTocItemVoToList(tocItems, tocRules, contentNavigator, itemVOList, mode);
                while (contentNavigator.toElement(VTDNav.NEXT_SIBLING)) {
                    addTocItemVoToList(tocItems, tocRules, contentNavigator, itemVOList, mode);
                }
            }
        } finally {
            contentNavigator.recoverNode(currentIndex);
        }

        return itemVOList;
    }
    
    static boolean navigateToFirstTocElment(List<TableOfContentItemVO> tableOfContentItemVOs, VTDNav vtdNav) throws NavException {
        TableOfContentItemVO firstTocVO = tableOfContentItemVOs.listIterator().next();
        return navigateToElementByNameAndId(firstTocVO.getTocItem().getAknTag().value(), null, vtdNav);
    }

    
	private static void addTocItemVoToList(List<TocItem> tocItems, Map<TocItem, List<TocItem>> tocRules, VTDNav contentNavigator, List<TableOfContentItemVO> itemVOList, TocMode mode)
            throws NavException {
        TableOfContentItemVO tableOfContentItemVO = buildTableOfContentsItemVO(tocItems, contentNavigator);
        if (tableOfContentItemVO != null) {
            List<TableOfContentItemVO> itemVOChildrenList = getAllChildTableOfContentItems(tocItems, tocRules, contentNavigator, mode);
            if ((!TocMode.SIMPLIFIED_CLEAN.equals(mode) || (TocMode.SIMPLIFIED_CLEAN.equals(mode) && tableOfContentItemVO.getTocItem().isDisplay()))
                    && shouldItemBeAddedToToc(tocItems, tocRules, contentNavigator, tableOfContentItemVO.getTocItem())) {
                if (TocMode.SIMPLIFIED.equals(mode) || TocMode.SIMPLIFIED_CLEAN.equals(mode)) {
                    if (getTagValueFromTocItemVo(tableOfContentItemVO).equals(LIST) && !itemVOList.isEmpty()) {
                        tableOfContentItemVO = itemVOList.get(itemVOList.size() - 1);
                        tableOfContentItemVO.addAllChildItems(itemVOChildrenList);
                        return;
                    } else if (Arrays.asList(PARAGRAPH, POINT, INDENT, LEVEL).contains(getTagValueFromTocItemVo(tableOfContentItemVO))) {
                        if ((itemVOChildrenList.size() > 1) && (itemVOChildrenList.get(0).getChildItems().isEmpty())) {
                            tableOfContentItemVO.setId(itemVOChildrenList.get(0).getId());
                            itemVOChildrenList.remove(0);
                        } else if (itemVOChildrenList.size() == 1) {
                            tableOfContentItemVO.setId(itemVOChildrenList.get(0).getId());
                            itemVOChildrenList = itemVOChildrenList.get(0).getChildItems();
                        }
                    }
                }
                itemVOList.add(tableOfContentItemVO);
                tableOfContentItemVO.addAllChildItems(itemVOChildrenList);
            } else if (tableOfContentItemVO.getParentItem() != null) {
                tableOfContentItemVO.getParentItem().addAllChildItems(itemVOChildrenList);
            } else {
                itemVOChildrenList.forEach(childItem -> itemVOList.add(childItem));
            }
        }
    }

    
    private static TableOfContentItemVO buildTableOfContentsItemVO(List<TocItem> tocItems, VTDNav contentNavigator) throws NavException {

        int originalNavigationIndex = contentNavigator.getCurrentIndex();

        // get the tocItem
        String tagName = contentNavigator.toString(contentNavigator.getCurrentIndex());
        TocItem tocItem = TocItemUtils.getTocItemByName(tocItems, tagName);

        if (tocItem == null) {
            // unsupported tag name
            return null;
        }

        // get the id
        int attIndex = contentNavigator.getAttrVal(XMLID);
        String elementId = null;
        if (attIndex != -1) {
            elementId = contentNavigator.toString(attIndex);
        }

        //get the leos:origin attr
        String originAttr = getOriginAttribute(contentNavigator);
        SoftActionType softActionAttr = getSoftActionAttribute(contentNavigator);
        Boolean isSoftActionRoot = getSoftActionRootAttribute(contentNavigator);
        String softUserAttr = getSoftUserAttribute(contentNavigator);
        GregorianCalendar softDateAttr = getSoftDateAttribute(contentNavigator);
        String softMovedFrom = getSoftMovedFromAttribute(contentNavigator);
        String softMovedTo = getSoftMovedToAttribute(contentNavigator);

        // get the intro
        Integer introTagIndex = null;
        if (contentNavigator.toElement(VTDNav.FIRST_CHILD, INTRO)) {
            introTagIndex = contentNavigator.getCurrentIndex();
            contentNavigator.recoverNode(originalNavigationIndex);
        }

        // get the num
        String number = null;
        Integer numberTagIndex = null;
        String originNumAttr = null;
        String numberType= null;
        SoftActionType numSoftActionAttribute=null;
        if (contentNavigator.toElement(VTDNav.FIRST_CHILD, NUM)) {
            numberTagIndex = contentNavigator.getCurrentIndex();
            //get the leos:origin attr
            originNumAttr =  getOriginAttribute(contentNavigator);
            numSoftActionAttribute =  SoftActionType.of(getNumSoftActionAttribute(contentNavigator));
            long contentFragment = contentNavigator.getContentFragment();
            number = extractNumber(getFragmentAsString(contentNavigator, contentFragment, true));
            numberType = extractNumberForType(getFragmentAsString(contentNavigator, contentFragment, true));
            contentNavigator.recoverNode(originalNavigationIndex);
        }

        // get the heading
        String heading = null;
        Integer headingTagIndex = null;
        if (contentNavigator.toElement(VTDNav.FIRST_CHILD, HEADING)) {
            headingTagIndex = contentNavigator.getCurrentIndex();
            long contentFragment = contentNavigator.getContentFragment();
            heading = getFragmentAsString(contentNavigator, contentFragment, true);
            contentNavigator.recoverNode(originalNavigationIndex);
        }
        
        String list = null;
        Integer listTagIndex = null;
        if(contentNavigator.toElement(VTDNav.FIRST_CHILD, LIST)) {
            listTagIndex = contentNavigator.getCurrentIndex();
            long contentFragment = contentNavigator.getContentFragment();
            list = getFragmentAsString(contentNavigator, contentFragment, true);
            contentNavigator.recoverNode(originalNavigationIndex);
        }
        
        int elementDepth = getElementDepth(contentNavigator);
        contentNavigator.recoverNode(originalNavigationIndex);
        
        //get the content
        String content = extractContentForTocItemsExceptNumAndHeadingAndIntro(tocItems, contentNavigator);
        contentNavigator.recoverNode(originalNavigationIndex);

        // build the table of content item and return it
        return new TableOfContentItemVO(tocItem, elementId, originAttr, number, originNumAttr, heading, numberTagIndex, headingTagIndex,
                introTagIndex, contentNavigator.getCurrentIndex(), list, listTagIndex, content, softActionAttr,
                isSoftActionRoot, softUserAttr, softDateAttr, softMovedFrom, softMovedTo, false, numSoftActionAttribute, elementDepth, numberType);
    }
    
    private static Integer getPreambleNonTocElements(int direction, String element, VTDNav contentNavigator) throws NavException {
        Integer elementIndex = null;
        if (contentNavigator.toElement(direction, element)) {
            elementIndex = contentNavigator.getCurrentIndex();
        }
        return elementIndex;
    }
    
    
    private static byte[] createNumBytes(VTDNav contentNavigator, TableOfContentItemVO parentTOC, MessageHelper messageHelper) throws NavException {
        byte[] element;
        //StringBuilder item = new StringBuilder(StringUtils.capitalize(parentTOC.getTocItem().getAknTag().value()));
        StringBuilder item = new StringBuilder(StringUtils.capitalize(extractMessage(parentTOC.getTocItem().getAknTag().value(),messageHelper)));
        byte[] numBytes;
        if (parentTOC.getTocItem().isNumWithType()) {
            numBytes = item.append(" ").append(extractNumber(parentTOC.getNumber())).toString().getBytes(UTF_8);
        } else {
            numBytes = (extractNumber(parentTOC.getNumber())).getBytes(UTF_8);
        }
        if (parentTOC.getNumTagIndex() != null) {
            contentNavigator.recoverNode(parentTOC.getNumTagIndex());
            byte[] numTag = getStartTagAndRemovePrefix(contentNavigator, parentTOC);
            numTag = updateOriginAttribute(numTag, parentTOC.getOriginNumAttr());
            element = XmlHelper.buildTag(numTag, NUM_BYTES, numBytes);
        } else {
            byte[] numStartTag = updateOriginAttribute(NUM_START_TAG, parentTOC.getOriginNumAttr());
            element = XmlHelper.buildTag(numStartTag, NUM_BYTES, numBytes);
        }
        return element;
    }

    
    static byte[] extractOrBuildNumElement(VTDNav contentNavigator, TableOfContentItemVO tableOfContentItemVO, MessageHelper messageHelper ) throws NavException {
        byte[] element = new byte[0];
        Boolean toggleFlag = checkIfNumberingIsToggled(tableOfContentItemVO);

        if (toggleFlag != null && toggleFlag
                && (tableOfContentItemVO.getNumber() == null || tableOfContentItemVO.getNumber() == "")) {

            if (LEOS_ORIGIN_ATTR_CN.equals(tableOfContentItemVO.getOriginAttr())) {
                tableOfContentItemVO.setNumber("#");
                tableOfContentItemVO.setOriginNumAttr(LEOS_ORIGIN_ATTR_CN);

            } else {
                List<TableOfContentItemVO> proposalParaVO = new ArrayList<>();
                for (TableOfContentItemVO itemVO : tableOfContentItemVO.getParentItem().getChildItems()) {
                    if (LEOS_ORIGIN_ATTR_EC.equals(itemVO.getOriginAttr())) {
                        proposalParaVO.add(itemVO);
                    }
                }
                tableOfContentItemVO.setNumber((proposalParaVO.indexOf(tableOfContentItemVO) + 1) + ".");
                tableOfContentItemVO.setOriginNumAttr(LEOS_ORIGIN_ATTR_EC);
            }

        } else if ((toggleFlag != null && !toggleFlag)
                && (tableOfContentItemVO.getNumSoftActionAttr() != null && SoftActionType.ADD.equals(tableOfContentItemVO.getNumSoftActionAttr())
                || LEOS_ORIGIN_ATTR_CN.equals(tableOfContentItemVO.getOriginNumAttr() != null ? tableOfContentItemVO.getOriginNumAttr() : tableOfContentItemVO.getOriginAttr()))) {
            tableOfContentItemVO.setNumber(null);
            return element;
        }
        if (tableOfContentItemVO.getNumber() != null ) {
            element = createNumBytes(contentNavigator, tableOfContentItemVO, messageHelper, !PROVISO.equals(tableOfContentItemVO.getTocItem().getAknTag()));
        }
        return element;
    }
    
    private static int getElementDepth(VTDNav vtdNav) throws NavException {
        int depth = 0;
        if (vtdNav.toElement(VTDNav.FIRST_CHILD, NUM)) {
            long contentFragment = vtdNav.getContentFragment();
            String elementNumber = new String(vtdNav.getXML().getBytes((int) contentFragment, (int) (contentFragment >> 32)));
            if(elementNumber.contains(".")) {
                String[] levelArr = StringUtils.split(elementNumber, LEVEL_NUM_SEPARATOR);
                depth = levelArr.length;
            }
        }
        return depth;
    }

    private static boolean shouldItemBeAddedToToc(List<TocItem> tocItems, Map<TocItem, List<TocItem>> tocRules, VTDNav vtdNav, TocItem tocItem) throws NavException {
        boolean addItemToToc = false;
        if (tocItem.isRoot()) {
            addItemToToc = tocItem.isDisplay();
        } else {        
            TocItem parentTocItem = TocItemUtils.getTocItemByName(tocItems, getParentTagName(vtdNav));
            if ((parentTocItem != null) && (tocRules.get(parentTocItem) != null)) {
                addItemToToc = tocRules.get(parentTocItem).contains(tocItem);
            }
        }
        return addItemToToc;
    }

    private static String getParentTagName(VTDNav vtdNav) throws NavException {
        String elementTagName = null;
        int currentIndex = vtdNav.getCurrentIndex();
        try {
            if (vtdNav.toElement(VTDNav.PARENT)) {
                elementTagName = vtdNav.toString(vtdNav.getCurrentIndex());
            }
        } finally {
            vtdNav.recoverNode(currentIndex);
        }
        return elementTagName;
    }
    
    private static byte[] createNumBytes(VTDNav contentNavigator, TableOfContentItemVO parentTOC, MessageHelper messageHelper, boolean needNumberExtraction) throws NavException {
        byte[] element = null;
        //#978
        StringBuilder item = new StringBuilder(StringUtils.capitalize(extractMessage(parentTOC.getTocItem().getAknTag().value(),messageHelper)));
        byte[] numBytes;
        if (parentTOC.getTocItem().isNumWithType() && parentTOC.getTocItem().isNumberTypeDisplayed()) {
        	if (parentTOC.getNumber().equals("#")) {
        		item.append(item.toString().replace("proviso", "Disposición"));
        		numBytes = item.append(" ").append(parentTOC.getNumberType()).append(" ").append(parentTOC.getNumber()).toString().getBytes(UTF_8);
        	} else {
        		numBytes = item.append(" ").append(parentTOC.getNumber()).toString().getBytes(UTF_8);
        	}
        }
        else if (parentTOC.getTocItem().isNumWithType()) {
        	numBytes = item.append(" ").append(needNumberExtraction ? extractNumber(parentTOC.getNumber()) : parentTOC.getNumber()).toString().getBytes(UTF_8);
        }
        else {
        	numBytes = (needNumberExtraction ? extractNumber(parentTOC.getNumber()) : parentTOC.getNumber()).getBytes(UTF_8);
        }
        if (parentTOC.getNumTagIndex() != null) {
            contentNavigator.recoverNode(parentTOC.getNumTagIndex());
            byte[] numTag = getStartTagAndRemovePrefix(contentNavigator, parentTOC);
            numTag = updateOriginAttribute(numTag, parentTOC.getOriginNumAttr());
            element = XmlHelper.buildTag(numTag, NUM_BYTES, numBytes);
        } else {
            byte[] numStartTag = updateOriginAttribute(NUM_START_TAG, parentTOC.getOriginNumAttr());
            element = XmlHelper.buildTag(numStartTag, NUM_BYTES, numBytes);
        }
        return element;
    }
    
    //Correctivo #978 // Evolutivo #1051
   private static String extractMessage(String message, MessageHelper messageHelper) {
		if (message.equals("book") || message.equals("chapter") || message.equals("title") || message.equals("section") || message.equals("subsection")) {
			return messageHelper.getMessage("toc.item.type." + message).toUpperCase();
		}else {
			return messageHelper.getMessage("toc.item.type." + message);
		} 	
   }

    static byte[] extractOrBuildHeaderElement(VTDNav contentNavigator, TableOfContentItemVO parentTOC) throws NavException, UnsupportedEncodingException {
        byte[] element = new byte[0];
        //Evolutivo #1218
        String headingAux;
        if (parentTOC.getHeading() != null) {
        	if (parentTOC.getTocItem().getAknTag().equals(SECTION)) {
        		headingAux = parentTOC.getHeading().toUpperCase();
        	} else {
        		headingAux = parentTOC.getHeading();	
        	}
            if (parentTOC.getHeadingTagIndex() != null) {
                contentNavigator.recoverNode(parentTOC.getHeadingTagIndex());
                byte[] headingTag = getStartTagAndRemovePrefix(contentNavigator, parentTOC);
                
                element = XmlHelper.buildTag(headingTag, HEADING_BYTES, headingAux.getBytes(UTF_8));
            } else {
                element = XmlHelper.buildTag(HEADING_START_TAG, HEADING_BYTES, headingAux.getBytes(UTF_8));
            }
        }
        return element;
    }
    
    private static String extractContentForTocItemsExceptNumAndHeadingAndIntro(List<TocItem> tocItems, VTDNav vtdNav) throws NavException {
        String tagName = vtdNav.toString(vtdNav.getCurrentIndex());
        TocItem tocItem = TocItemUtils.getTocItemByName(tocItems, tagName);
        String elementName = vtdNav.toString(vtdNav.getCurrentIndex());
        
        if (elementName.equalsIgnoreCase(tocItem.getAknTag().value()) &&
                (vtdNav.toElement(VTDNav.FIRST_CHILD, HEADING) || vtdNav.toElement(VTDNav.FIRST_CHILD, NUM)
                        || vtdNav.toElement(VTDNav.FIRST_CHILD, INTRO))) {
            if(!vtdNav.toElement(VTDNav.NEXT_SIBLING))
                return StringUtils.EMPTY;
            LOG.trace("extractContentForTocItemsExceptNumAndHeading - Skipping {} tag for {}", vtdNav.toString(vtdNav.getCurrentIndex()), tocItem.getAknTag().value());
        } 
        return new String(getTagWithContent(vtdNav), UTF_8);
    }
    
}  
