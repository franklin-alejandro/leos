package eu.europa.ec.leos.services.content.processor;

import static eu.europa.ec.leos.services.compare.ContentComparatorService.ATTR_NAME;
import static eu.europa.ec.leos.services.compare.ContentComparatorService.CONTENT_SOFT_ADDED_CLASS;
import static eu.europa.ec.leos.services.compare.ContentComparatorService.CONTENT_SOFT_REMOVED_CLASS;

import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import eu.europa.ec.leos.domain.cmis.document.Annex;
import eu.europa.ec.leos.domain.cmis.document.Bill;
import eu.europa.ec.leos.domain.cmis.document.AGECantoDorado;
import eu.europa.ec.leos.domain.cmis.document.AGEReport;
import eu.europa.ec.leos.domain.cmis.document.XmlDocument;
import eu.europa.ec.leos.domain.common.InstanceType;
import eu.europa.ec.leos.instance.Instance;
import eu.europa.ec.leos.security.SecurityContext;
import eu.europa.ec.leos.services.compare.ContentComparatorContext;
import eu.europa.ec.leos.services.compare.ContentComparatorService;
import eu.europa.ec.leos.services.document.AnnexService;
import eu.europa.ec.leos.services.document.BillService;
import eu.europa.ec.leos.services.document.AGECantoDoradoService;
import eu.europa.ec.leos.services.document.AGEReportService;

@Primary
@Service
@Instance(InstanceType.COUNCIL)
public class AGEMandateDocumentContentServiceImpl extends MandateDocumentContentServiceImpl {
	
	private TransformationService transformationService;
    private ContentComparatorService compareService;
    private AnnexService annexService;
    private BillService billService;
    private AGEReportService reportService;
    private AGECantoDoradoService cantoDoradoService;

    @Autowired
    public AGEMandateDocumentContentServiceImpl(TransformationService transformationService,
            ContentComparatorService compareService, AnnexService annexService,AGEReportService reportService, AGECantoDoradoService cantoDoradoService, BillService billService) {
    	super(transformationService, compareService, annexService, billService);
    	this.transformationService = transformationService;
    	this.compareService = compareService;
    	this.annexService = annexService;
    	this.billService = billService;
        this.reportService = reportService;
        this.cantoDoradoService = cantoDoradoService;
        
    }

    @Override
    public String toEditableContent(XmlDocument xmlDocument, String contextPath, SecurityContext securityContext) {
        String currentDocumentEditableXml = transformationService.toEditableXml(getContentInputStream(xmlDocument), contextPath, xmlDocument.getCategory(), securityContext.getPermissions(xmlDocument));
        XmlDocument originalDocument;

        switch (xmlDocument.getCategory()){
            case MEMORANDUM:
                return currentDocumentEditableXml;
            case ANNEX:
                originalDocument = getOriginalAnnex(xmlDocument);
                break;
            case REPORT:
                originalDocument = getOriginalReport(xmlDocument);
                break;
            case CANTODORADO:
                originalDocument = getOriginalCantoDorado(xmlDocument);
                break;
            case BILL:
                originalDocument = getOriginalBill(xmlDocument);
                break;
            default:
                throw new UnsupportedOperationException("No transformation supported for this category");
        }

        String originalDocumentEditableXml = transformationService.toEditableXml(getContentInputStream(originalDocument), contextPath, originalDocument.getCategory(), securityContext.getPermissions(originalDocument));
        return compareService.compareContents(new ContentComparatorContext.Builder(originalDocumentEditableXml, currentDocumentEditableXml)
                .withAttrName(ATTR_NAME)
                .withRemovedValue(CONTENT_SOFT_REMOVED_CLASS)
                .withAddedValue(CONTENT_SOFT_ADDED_CLASS)
                .withDisplayRemovedContentAsReadOnly(Boolean.TRUE)
                .build());
    }

    private XmlDocument getOriginalAnnex(XmlDocument xmlDocument) {
        List<Annex> annexVersions = annexService.findVersions(xmlDocument.getId());
        annexVersions.sort(Comparator.comparing(Annex::getCreationInstant));
        return annexVersions.isEmpty() ? xmlDocument : annexService.findAnnexVersion(annexVersions.get(0).getId());
    }
    
    private XmlDocument getOriginalReport(XmlDocument xmlDocument) {
        List<AGEReport> reportsVersions = reportService.findVersions(xmlDocument.getId());
        reportsVersions.sort(Comparator.comparing(AGEReport::getCreationInstant));
        return reportsVersions.isEmpty() ? xmlDocument : reportService.findReportVersion(reportsVersions.get(0).getId());
    }
    
    private XmlDocument getOriginalCantoDorado(XmlDocument xmlDocument) {
        List<AGECantoDorado> cantoDoradoVersions = cantoDoradoService.findVersions(xmlDocument.getId());
        cantoDoradoVersions.sort(Comparator.comparing(AGECantoDorado::getCreationInstant));
        return cantoDoradoVersions.isEmpty() ? xmlDocument : cantoDoradoService.findCantoDoradoVersion(cantoDoradoVersions.get(0).getId());
    }

    private XmlDocument getOriginalBill(XmlDocument xmlDocument) {
        List<Bill> billVersions = billService.findVersions(xmlDocument.getId());
        billVersions.sort(Comparator.comparing(Bill::getCreationInstant));
        return billVersions.isEmpty() ? xmlDocument : billService.findBillVersion(billVersions.get(0).getId());
    }

}