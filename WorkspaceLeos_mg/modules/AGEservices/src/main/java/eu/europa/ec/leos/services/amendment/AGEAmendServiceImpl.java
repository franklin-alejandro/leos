package eu.europa.ec.leos.services.amendment;

import eu.europa.ec.leos.amend.AmendActDetails;
import eu.europa.ec.leos.amend.AmendAction;
import eu.europa.ec.leos.domain.cmis.document.Bill;
import eu.europa.ec.leos.domain.cmis.document.XmlDocument;
import eu.europa.ec.leos.domain.common.Result;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.action.ActionType;
import eu.europa.ec.leos.model.action.CheckinCommentVO;
import eu.europa.ec.leos.model.action.CheckinElement;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.services.content.ReferenceLabelService;
import eu.europa.ec.leos.services.content.processor.ElementProcessor;
import eu.europa.ec.leos.services.document.BillService;
import eu.europa.ec.leos.services.document.util.CheckinCommentUtil;
import eu.europa.ec.leos.services.support.xml.ElementNumberingHelper;
import eu.europa.ec.leos.services.support.xml.VTDUtils;
import eu.europa.ec.leos.services.support.xml.XmlContentProcessor;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.Arrays;

import static eu.europa.ec.leos.services.support.xml.XmlHelper.ARTICLE;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.REFERENCES;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.REFERS_TO;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.REFERS_TO_ART_AMEND;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.POINT;

@Primary
@Service
public class AGEAmendServiceImpl extends AmendServiceImpl implements AGEAmendService {

    private final ElementProcessor elementProcessor;
    private final MessageHelper messageHelper;
    private final ReferenceLabelService referenceLabelService;
    private final BillService billService;
    private final XmlContentProcessor xmlContentProcessor;
    private final ElementNumberingHelper elementNumberingHelper;

    @Autowired
    public AGEAmendServiceImpl(ElementProcessor elementProcessor, MessageHelper messageHelper, ReferenceLabelService referenceLabelService,
                            BillService billService, XmlContentProcessor xmlContentProcessor, ElementNumberingHelper elementNumberingHelper 
                            , AmendPatternLabelService amendPatternLabelService) {
    	super(elementProcessor, messageHelper, referenceLabelService, billService, xmlContentProcessor, elementNumberingHelper);
        this.elementProcessor = elementProcessor;
        this.messageHelper = messageHelper;
        this.referenceLabelService = referenceLabelService;
        this.billService = billService;
        this.xmlContentProcessor = xmlContentProcessor;
        this.elementNumberingHelper = elementNumberingHelper;
    }

    @Override
    public AmendActDetails openAmendmentEditor(Bill bill, String elementId, String type) throws Exception {
        AmendActDetails amendActDetails = null;
        byte[] xmlContent = bill.getContent().get().getSource().getBytes();

        try
        {
            if (isAmendmentArticle(elementId, xmlContent, type)) { // "refersTo=\"~_ART_AMEND\""
                String xPath = "//" + type + "[@xml:id='" + elementId + "']/paragraph/content/p/mod/affectedDocument";
                String actHref = VTDUtils.getAttributeByXPath(xmlContent, xPath, "href");
                amendActDetails = AmendActDetails.parseDetails(actHref);
            }
        }catch (Exception e) {
        	String xPath = "//" + type + "[@xml:id='" + elementId + "']/paragraph/content/p/affectedDocument";
            String actHref = VTDUtils.getAttributeByXPath(xmlContent, xPath, "href");
            amendActDetails = AmendActDetails.parseDetails(actHref);
		}
        return amendActDetails;
    }

    private boolean isAmendmentArticle(String elementId, byte[] xmlContent, String type) throws Exception {
        return VTDUtils.getAttribute(xmlContent, type, elementId, REFERS_TO) != null;
    }
}
