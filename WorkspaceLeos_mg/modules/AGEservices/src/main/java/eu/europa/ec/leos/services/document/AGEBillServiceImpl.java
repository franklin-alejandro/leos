package eu.europa.ec.leos.services.document;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import eu.europa.ec.leos.domain.cmis.document.Bill;
import eu.europa.ec.leos.domain.cmis.metadata.BillMetadata;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.repository.document.BillRepository;
import eu.europa.ec.leos.repository.store.PackageRepository;
import eu.europa.ec.leos.services.content.processor.AttachmentProcessor;
import eu.europa.ec.leos.services.document.util.DocumentVOProvider;
import eu.europa.ec.leos.services.support.IdGenerator;
import eu.europa.ec.leos.services.support.xml.AGENumberProcessor;
import eu.europa.ec.leos.services.support.xml.NumberProcessor;
import eu.europa.ec.leos.services.support.xml.XmlContentProcessor;
import eu.europa.ec.leos.services.support.xml.XmlNodeConfigHelper;
import eu.europa.ec.leos.services.support.xml.XmlNodeProcessor;
import eu.europa.ec.leos.services.support.xml.XmlTableOfContentHelper;
import eu.europa.ec.leos.services.validation.ValidationService;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;

@Primary
@Service
public class AGEBillServiceImpl extends BillServiceImpl{
	
	AGENumberProcessor ageNumberingProcessor;
	
	 @Autowired
	 AGEBillServiceImpl(BillRepository billRepository, PackageRepository packageRepository,
			XmlNodeProcessor xmlNodeProcessor, XmlContentProcessor xmlContentProcessor,
			XmlNodeConfigHelper xmlNodeConfigHelper, AttachmentProcessor attachmentProcessor,
			ValidationService validationService, DocumentVOProvider documentVOProvider,
			NumberProcessor numberingProcessor, AGENumberProcessor ageNumberingProcessor, MessageHelper messageHelper, XmlTableOfContentHelper xmlTableOfContentHelper) {
		super(billRepository, packageRepository, xmlNodeProcessor, xmlContentProcessor, xmlNodeConfigHelper,
				attachmentProcessor, validationService, documentVOProvider, numberingProcessor, messageHelper, xmlTableOfContentHelper);
		this.ageNumberingProcessor = ageNumberingProcessor;
	}

	@Override
	 public Bill saveTableOfContent(Bill bill, List<TableOfContentItemVO> tocList, String actionMsg, User user) {
        Validate.notNull(bill, "Bill is required");
        Validate.notNull(tocList, "Table of content list is required");
        final BillMetadata metadata = bill.getMetadata().getOrError(() -> "Document metadata is required!");
        String newXmlContentString = null;
        byte[] newXmlContent;
        newXmlContent = xmlContentProcessor.createDocumentContentWithNewTocList(tocList, getContent(bill), user);
        newXmlContent = this.ageNumberingProcessor.renumberArticles(newXmlContent);
        newXmlContent = this.ageNumberingProcessor.renumberRecitals(newXmlContent);
        newXmlContent = this.ageNumberingProcessor.renumberProvisos(newXmlContent);
        //Evolutivo #1052	
        newXmlContent = this.ageNumberingProcessor.renumberOthers(newXmlContent);
        newXmlContent = xmlContentProcessor.doXMLPostProcessing(newXmlContent);
        
        newXmlContentString = new String (newXmlContent, StandardCharsets.UTF_8);
 
		return updateBill(bill, newXmlContentString.getBytes(StandardCharsets.UTF_8), actionMsg);
    }

}
