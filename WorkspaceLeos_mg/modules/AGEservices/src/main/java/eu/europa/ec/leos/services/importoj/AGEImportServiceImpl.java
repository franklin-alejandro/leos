package eu.europa.ec.leos.services.importoj;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.ximpleware.NavException;
import com.ximpleware.ParseException;
import com.ximpleware.VTDGen;
import com.ximpleware.VTDNav;

import eu.europa.ec.leos.domain.cmis.Content;
import eu.europa.ec.leos.domain.cmis.document.Annex;
import eu.europa.ec.leos.domain.cmis.document.Bill;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.integration.ExternalDocumentProvider;
import eu.europa.ec.leos.services.support.IdGenerator;
import eu.europa.ec.leos.services.support.xml.AGEElementNumberingHelper;
import eu.europa.ec.leos.services.support.xml.AGENumberProcessor;
import eu.europa.ec.leos.services.support.xml.AGEXmlContentProcessor;
import eu.europa.ec.leos.services.support.xml.NumberProcessor;
import eu.europa.ec.leos.services.support.xml.XmlContentProcessor;
import eu.europa.ec.leos.services.support.xml.ref.AGETreeNode;
import eu.europa.ec.leos.services.support.xml.ref.TreeNode;

import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.ARTICLE;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.RECITAL;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.TITLE;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.ARTICLE;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.BODY;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.RECITAL;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.RECITALS;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.CHAPTER;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.SECTION;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.SUBSECTION;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.BOOK;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.PROVISO;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.BODY;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.BILL;

@Primary
@Service
public class AGEImportServiceImpl extends ImportServiceImpl implements AGEImportService{
	
	private static final Logger LOG = LoggerFactory.getLogger(AGEImportServiceImpl.class);
	
	@Autowired
	private ExternalDocumentProvider externalDocumentProvider;
	
	@Autowired
	private AGENumberProcessor numberProcessor;

	@Autowired
	private AGEXmlContentProcessor xmlContentProcessor;
	
    @Autowired
    private MessageHelper messageHelper;    
    
	private String encabezadoType = "";
	private int contArt = 0;
	private int contProv = 0;
	private int contRecital= 0;

	Pattern patternNum = Pattern.compile("(<num.*>)(.*)(</num>)");
    
	private final String numNivel2arrayLetters[] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", 
			"aa", "ab", "ac", "ad", "ae", "af", "ag", "ah", "ai", "aj", "ak", "al", "am", "an", "añ", "ao", "ap", "aq", "ar", "as", "at", "au", "av", "aw", "ax", "ay", "az"};
	private final String [] numNivel4romanos = {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX"};
	private final String [] numNivel3 = {"1.ª", "2.ª", "3.ª", "4.ª", "5.ª", "6.ª", "7.ª", "8.ª", "9.ª", "10.ª", "11.ª", "12.ª", "13.ª", "14.ª", "15.ª", "16.ª", "17.ª", "18.ª", "19.ª", "20.ª"};
	
	@Autowired
	AGEImportServiceImpl(ExternalDocumentProvider externalDocumentProvider, ConversionHelper conversionHelper,
			XmlContentProcessor xmlContentProcessor, NumberProcessor numberProcessor) {
		super(externalDocumentProvider, conversionHelper, xmlContentProcessor, numberProcessor);
		//this.xmlContentProcessor = xmlContentProcessor;
	}
	
	@Override
    public byte[] insertSelectedElements(Bill bill, byte[] importedContent, List<String> elementIds, String language) {
		byte[] documentContent = super.insertSelectedElements(bill, importedContent, elementIds, language);
        documentContent = this.numberProcessor.renumberProvisos(documentContent);
        documentContent = xmlContentProcessor.doXMLPostProcessing(documentContent);
        return documentContent;
    }
	
    @Override
    public byte[] insertSelectedElements(Annex annex, byte[] importedContent, List<String> elementIds, String language) {
        byte[] documentContent = getContent(annex);
        for (String id : elementIds) {
            String[] element = xmlContentProcessor.getElementById(importedContent, id);

            // Get id of the last element in the document
            String xPath = "//" + element[1] + "[last()]";
            String elementId = xmlContentProcessor.getElementIdByPath(documentContent, xPath);
            String elementType = element[1];

            // Do pre-processing on the selected elements
            String updatedElement = xmlContentProcessor.doImportedElementPreProcessing(element[2], elementType);
            if (elementType.equalsIgnoreCase(ARTICLE)) {
                updatedElement = this.numberProcessor.renumberImportedArticle(updatedElement, language);
            } else if (elementType.equalsIgnoreCase(RECITAL)) {
                updatedElement = this.numberProcessor.renumberImportedRecital(updatedElement);
            }

            // Insert selected element to the document
            if (elementId != null) {
                documentContent = xmlContentProcessor.insertElementByTagNameAndId(documentContent, updatedElement,
                        element[1], elementId, checkIfLastArticleIsEntryIntoForce(documentContent, element[1], elementId, language));
            } else if (elementType.equalsIgnoreCase(ARTICLE)) {
                documentContent = xmlContentProcessor.appendElementToTag(documentContent, BODY, updatedElement, true);
            } else if (elementType.equalsIgnoreCase(RECITAL)) {
                documentContent = xmlContentProcessor.appendElementToTag(documentContent, RECITALS, updatedElement, true);
            }
        }
        // Renumber
        //documentContent = this.numberProcessor.renumberRecitals(documentContent);
        documentContent = this.numberProcessor.renumberArticles(documentContent);
        documentContent = this.numberProcessor.renumberProvisos(documentContent);
        documentContent = xmlContentProcessor.doXMLPostProcessing(documentContent);
        return documentContent;
    }
    
    private byte[] getContent(Annex annex) {
        final Content content = annex.getContent().getOrError(() -> "Annex content is required!");
        return content.getSource().getBytes();
    }
	
    @Override
    public byte[] insertSelectedProjectElements(byte[] originalContent, byte[] importedContent, List<String> elementIds, String language) {

    	ArrayList<String[]> elements = new ArrayList<>();
    	for (String id : elementIds) {
    		elements.add(xmlContentProcessor.getElementById(importedContent, id));
    	}

    	ArrayList<String> mrefId = xmlContentProcessor.doImportedMrefProjectPreProcessing(elements);

    	for( int i = 0; i < elements.size(); i++ ) {
    		String elementType = elements.get(i)[1];
    		String xPath = "//" + elementType + "[last()]";
    		String elementId = xmlContentProcessor.getElementIdByPath(originalContent, xPath);     	
    		if (elementType.equals(PROVISO) && elementId == null) {
    			elementType = ARTICLE;
    			xPath = "//" + elementType + "[last()]";
    			elementId = xmlContentProcessor.getElementIdByPath(originalContent, xPath);     	
    		}
    		if (elementId != null) {
    			// do pre-processing on the selected element
    			String updatedElement = xmlContentProcessor.doImportedElementProjectPreProcessing(elements.get(i)[2], elementType, mrefId );
    			if (updatedElement != null) {
    				// insert selected element to the document
    				originalContent = xmlContentProcessor.insertElementByTagNameAndId(originalContent, updatedElement,
    						elements.get(i)[1],
    						elementId, checkIfLastArticleIsEntryIntoForce(originalContent, elements.get(i)[1], elementId, language));
    			}
    		}
    	}
    	// renumber
    	originalContent = this.numberProcessor.renumberRecitals(originalContent);
    	originalContent = this.numberProcessor.renumberArticles(originalContent);
    	originalContent = this.numberProcessor.renumberProvisos(originalContent);
    	originalContent = xmlContentProcessor.doXMLPostProcessing(originalContent);
    	return originalContent;
    }
    
    @Override
    public byte[] insertSelectedProjectElementsAnnex(byte[] originalContent, byte[] importedContent, List<String> elementIds, String language) {

    	ArrayList<String[]> elements = new ArrayList<>();
    	for (String id : elementIds) {
    		elements.add(xmlContentProcessor.getElementById(importedContent, id));
    	}

    	ArrayList<String> mrefId = xmlContentProcessor.doImportedMrefProjectPreProcessing(elements);

    	for( int i = 0; i < elements.size(); i++ ) {
    		boolean isNull = false;
    		String elementType = elements.get(i)[1];
    		String xPath = "//" + elementType + "[last()]";
    		String elementId = xmlContentProcessor.getElementIdByPath(originalContent, xPath);     	
    		if (elementType.equals(PROVISO) && elementId == null) {
    			isNull = true;
    			xPath = "//" + ARTICLE + "[last()]";
    			elementId = xmlContentProcessor.getElementIdByPath(originalContent, xPath);     	
    		}
    		if (elementId != null) {
    			// do pre-processing on the selected element
    			String updatedElement = xmlContentProcessor.doImportedElementProjectPreProcessing(elements.get(i)[2], elementType, mrefId );
    			if (updatedElement != null) {
    				// insert selected element to the document
    				originalContent = xmlContentProcessor.insertElementByTagNameAndId(originalContent, updatedElement,
    						!isNull ? elements.get(i)[1] : ARTICLE,
    						elementId, false);
    			}
    		}
    	}
    	// renumber
    	//originalContent = this.numberProcessor.renumberRecitals(originalContent);
    	originalContent = this.numberProcessor.renumberArticles(originalContent);
    	originalContent = this.numberProcessor.renumberProvisos(originalContent);
    	originalContent = xmlContentProcessor.doXMLPostProcessing(originalContent);
    	return originalContent;
    }
    
    // check if the last article in the document has heading Entry into force, if yes articles imported before EIF article
    private boolean checkIfLastArticleIsEntryIntoForce(byte[] documentContent, String element, String elementId,
            String language) {
        boolean isLastElementEIF = false;
        String lastElement = xmlContentProcessor.getElementByNameAndId(documentContent, element, elementId);
        String headingElementValue = xmlContentProcessor.getElementValue(lastElement.getBytes(StandardCharsets.UTF_8),
                "//heading[1]", false); // Disable namespace parsing for VTD as xml fragment passed may not contain namespace information
        if (checkIfHeadingIsEntryIntoForce(headingElementValue, language)) {
            isLastElementEIF = true;
        }
        return isLastElementEIF;
    }

    // Gets the heading message from locale
    private boolean checkIfHeadingIsEntryIntoForce(String headingElementValue, String language) {
        boolean isHeadingMatched = false;
        if (headingElementValue != null && !headingElementValue.isEmpty()) {
            isHeadingMatched = messageHelper.getMessage("legaltext.article.entryintoforce.heading").replaceAll("\\h+", "")
                    .equalsIgnoreCase(StringUtils.trimAllWhitespace(headingElementValue.replaceAll("\\h+", "")));
        }
        return isHeadingMatched;
    }

	private String getTituloBOE(String content) {
		String identificadorBOE = null;
		Pattern numArtDis = Pattern.compile("<tit>(.*?)</tit>");
		Matcher matcherArtDis = numArtDis.matcher(content);
		if (matcherArtDis.find()) {
			identificadorBOE = matcherArtDis.group(1);
		} else {
			 numArtDis = Pattern.compile("<TIT>(.*?)</TIT>");
			 matcherArtDis = numArtDis.matcher(content);
			 if (matcherArtDis.find()) {
			 identificadorBOE = matcherArtDis.group(1);
			 }
		}
		return identificadorBOE;	
	}
    
    
	private String getIdentificadorBOE(String content) {
		String identificadorBOE = null;
		Pattern numArtDis = Pattern.compile("<identificador>(.*?)</identificador>");
		Matcher matcherArtDis = numArtDis.matcher(content);
		if (matcherArtDis.find()) {
			identificadorBOE = matcherArtDis.group(1);
		}
		return identificadorBOE;	
	}

	@Override
	public byte[] insertSelectedElementsBOE(byte[] documentContent, byte[] importedContent, List<String> elementIds, String language) throws NavException {
		AGEElementNumberingHelper elementNumberingHelper = null;
		VTDNav vtdNav = null;
		String OrderType = "CARDINAL_NUMBER";
		String LEOS_ORDERING_ATTR = "leos:ordering";
		Locale languageLocale = new Locale("ES");
		Map<String,String> bodyAttributes = xmlContentProcessor.getElementAttributesByPath(documentContent, BILL + "/" + BODY, false);
		OrderType = bodyAttributes.get(LEOS_ORDERING_ATTR);
		if (OrderType == null) {
			OrderType = "CARDINAL_NUMBER";
		}
		LOG.trace("Article Reordering Type: {} ", OrderType);
		
		for (String id : elementIds) {
			String[] element = xmlContentProcessor.getElementById(importedContent, id);
			String xPath = "//" + element[1] + "[last()]";
			String elementId = xmlContentProcessor.getElementIdByPath(documentContent, xPath); 
			String elementType = element[1];
			if (elementId != null) {
				String updatedElement = element[2].replace("<tbody>", "").replace("</tbody>", "");
				if (OrderType.contentEquals("ORDINAL_LETTER") && (languageLocale.getLanguage().equals("es"))) {
					updatedElement = element[2].replace("leos:mod=\"false\"", "leos:mod=\"true\"");
				}				
				if (elementType.equalsIgnoreCase(ARTICLE)) {
					updatedElement = this.numberProcessor.renumberImportedArticle(updatedElement,language);
				} else if (elementType.equalsIgnoreCase(PROVISO)) {
					updatedElement = this.numberProcessor.renumberImportedProviso(updatedElement,language);
				} else if (elementType.equalsIgnoreCase(RECITAL)) {
					updatedElement = this.numberProcessor.renumberImportedRecital(updatedElement);
				}
				if (updatedElement != null) {
					documentContent = xmlContentProcessor.insertElementByTagNameAndId(documentContent, updatedElement,
							element[1],
							elementId, checkIfLastArticleIsEntryIntoForce(documentContent, element[1], elementId, language));
				}
			} else {
				// TODO:handle case when no desired element exists in the document.
			}
		}
		// renumber
		documentContent = this.numberProcessor.renumberRecitals(documentContent);
		documentContent = this.numberProcessor.renumberArticles(documentContent);
		documentContent = this.numberProcessor.renumberProvisos(documentContent);
		documentContent = xmlContentProcessor.doXMLPostProcessing(documentContent);
		return documentContent;
	}
	
	@Override
	public byte[] insertSelectedElementsAnnexBOE(byte[] documentContent, byte[] importedContent, List<String> elementIds, String language) throws NavException {
		AGEElementNumberingHelper elementNumberingHelper = null;
		VTDNav vtdNav = null;
		String OrderType = "CARDINAL_NUMBER";
		String LEOS_ORDERING_ATTR = "leos:ordering";
		Locale languageLocale = new Locale("ES");
		Map<String,String> bodyAttributes = xmlContentProcessor.getElementAttributesByPath(documentContent, BILL + "/" + BODY, false);
		OrderType = bodyAttributes.get(LEOS_ORDERING_ATTR);
		if (OrderType == null) {
			OrderType = "CARDINAL_NUMBER";
		}
		LOG.trace("Article Reordering Type: {} ", OrderType);
		
		for (String id : elementIds) {
			boolean isNull = false;
			String[] element = xmlContentProcessor.getElementById(importedContent, id);
			String xPath = "//" + element[1] + "[last()]";
			String elementId = xmlContentProcessor.getElementIdByPath(documentContent, xPath); 
			String elementType = element[1];
			if (elementType.equals(PROVISO) && elementId == null) {
				isNull = true;
				xPath = "//" + ARTICLE + "[last()]";
				elementId = xmlContentProcessor.getElementIdByPath(documentContent, xPath);
			}			
			if (elementId != null) {
				String updatedElement = element[2].replace("<tbody>", "").replace("</tbody>", "");
				if (OrderType.contentEquals("ORDINAL_LETTER") && (languageLocale.getLanguage().equals("es"))) {
					updatedElement = element[2].replace("leos:mod=\"false\"", "leos:mod=\"true\"");
				}				
				if (elementType.equalsIgnoreCase(ARTICLE)) {
					updatedElement = this.numberProcessor.renumberImportedArticle(updatedElement,language);
				} else if (elementType.equalsIgnoreCase(PROVISO)) {
					updatedElement = this.numberProcessor.renumberImportedProviso(updatedElement,language);
				} else if (elementType.equalsIgnoreCase(RECITAL)) {
					updatedElement = this.numberProcessor.renumberImportedRecital(updatedElement);
				}
				if (updatedElement != null) {
					documentContent = xmlContentProcessor.insertElementByTagNameAndId(documentContent, updatedElement,
							!isNull ? element[1] : ARTICLE,
							elementId, !isNull ? false : checkIfLastArticleIsEntryIntoForce(documentContent, !isNull ? element[1] : ARTICLE, elementId, language));
				}
			} else {
				// TODO:handle case when no desired element exists in the document.
			}
		}
		// renumber
		//documentContent = this.numberProcessor.renumberRecitals(documentContent);
		documentContent = this.numberProcessor.renumberArticles(documentContent);
		documentContent = this.numberProcessor.renumberProvisos(documentContent);
		documentContent = xmlContentProcessor.doXMLPostProcessing(documentContent);
		return documentContent;
	}
	
	@Override
	public ArrayList<String> getNumberBOEModified(byte[] importedContent,
			List<String> elementIds, String language) {
		ArrayList<String> idsAlert = new ArrayList<String>();
		for (String id : elementIds) {
			String[] element = xmlContentProcessor.getElementById(importedContent, id);
			String elementType = element[1];

			String updatedElement = element[2];
			if (elementType.equalsIgnoreCase(ARTICLE) || elementType.equalsIgnoreCase(PROVISO)) {
				updatedElement = this.numberProcessor.renumberImportedArticle(updatedElement,language);
			} 
			if (updateNumber(element[2], updatedElement)) {
				
				Matcher matcherNum = patternNum.matcher(element[2]);
				if (matcherNum.find()) {
					idsAlert.add(id + ":" + matcherNum.group(2));
				} else {
					idsAlert.add(id);
				}				
			}

		}
		return idsAlert;
	}
	
	@Override
	public boolean getBOEElementReferenced(byte[] importedContent, List<String> elementIds) {
		for (String id : elementIds) {
			String[] element = xmlContentProcessor.getElementById(importedContent, id);
			String elementType = element[1];
			if (elementType.equalsIgnoreCase(ARTICLE) || elementType.equalsIgnoreCase(PROVISO)) {
				Matcher matcherNum = patternNum.matcher(element[2]);
				if (matcherNum.find()) {
					//System.out.println(matcherNum.group(2));
					return xmlContentProcessor.getReferencedBOE(importedContent, elementType, id, matcherNum.group(2));
				}
			}
		}
		return false;
	}

	@Override
	public boolean getElementRepealed(byte[] importedContent, List<String> elementIds) {
		for (String id : elementIds) {
			String[] element = xmlContentProcessor.getElementById(importedContent, id);
			String elementType = element[1];

			String updatedElement = element[2].toLowerCase();
			if (elementType.equalsIgnoreCase(ARTICLE) || elementType.equalsIgnoreCase(PROVISO)) {
				if(updatedElement.contains("(derogado)") || updatedElement.contains("(derogada)") 
						|| updatedElement.contains("(anulado)") ||	updatedElement.contains("(párrafo anulado)")) {
					return true;
				}
			} 
		}
		return false;
	}
	
	private boolean updateNumber (String oldElement, String newElement) {
		int cont = 0;
		ArrayList<String> elementOld = new ArrayList<String>();
		ArrayList<String> elementNew = new ArrayList<String>();
		
		Pattern pattern1 = Pattern.compile("(<num.*>)(.*)(</num>)");
		Matcher matcherOld = pattern1.matcher(oldElement);
		Matcher matcherNew= pattern1.matcher(newElement);
		while (matcherOld.find()) {
			if (cont!=0) {
				elementOld.add(matcherOld.group(2).trim());
			}
			cont++;
		}
		cont = 0;
		while (matcherNew.find()) {
			if (cont!=0) {
				elementNew.add(matcherNew.group(2).trim());
			}
			cont++;
		}	
		for (int pos = 0; elementOld.size() > pos; pos++) {
			if (!elementOld.get(pos).equals(elementNew.get(pos))){
				return true;
			}
		}
		return false;
	}
	
	@Override
	@Cacheable(value = "aknCache", cacheManager = "cacheManager")
	public String getBOEDocument(String type, int year, int month, int day, int number, String accion) {
		String documentBOEELI = null;
		String documentBOEConsolidado = null;
		String aknDocument = null;
		String titulo = "";
		String eli = "";
		String identificadorBOE = null;
		contArt = 0;
		contProv = 0;
		contRecital= 0;
		try {
			//1 LLAMAMOS AL DOCUMENTO ELI
			documentBOEELI = externalDocumentProvider.getBOEDocument(type, year, month, day, number);
			//2 RECOGEMOS EL IDENTIFICADOR DEL DOCUMENTO ELI
			identificadorBOE = getIdentificadorBOE(documentBOEELI);
			//3 Recogemos el documento BOE consolidado
			documentBOEConsolidado =externalDocumentProvider.getDocumentBOEConsolidado(identificadorBOE);
			if (documentBOEConsolidado.startsWith("Error") && !documentBOEELI.contains("<!DOCTYPE html>")) { // ELI
				byte[] contentBytesUTF8 = makeFromELIorIBERLEXAlt(documentBOEELI.getBytes(StandardCharsets.UTF_8),"ELI");
				aknDocument = makeAknDocumentConso(contentBytesUTF8, identificadorBOE);
				if (accion.equals("AMENDMENT")) {
					titulo = getTituloBOE(documentBOEELI);
					eli = getEliBOEIberlex(identificadorBOE);
					aknDocument = titulo + ":::" + aknDocument + ":::" + eli;
				}
				
			} else if (!documentBOEConsolidado.startsWith("Error")){ // CONSOLIDADO
				String decodedToUTF8 = new String(documentBOEConsolidado.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
				byte[] contentBytesUTF8 =decodedToUTF8.getBytes(StandardCharsets.UTF_8);
				aknDocument = makeAknDocumentConso(contentBytesUTF8, identificadorBOE);
				if (accion.equals("AMENDMENT")) {
					titulo = getTituloBOE(decodedToUTF8);
					eli = getEliBOEIberlex(identificadorBOE);
					aknDocument = titulo + ":::" + aknDocument + ":::" + eli;
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Unable to get the document in formex format", e);
		}
		return aknDocument;
	}

	@Override
	//@Cacheable(value = "aknCache", cacheManager = "cacheManager")
	public String getBOEDocument(String reference, String accion) {
		String documentBOEIBERLEX = null;
		String documentBOEConsolidado = null;
		String aknDocument = null;
		String titulo = "";
		String eli = "";
		String decodedToUTF8 = "";
		contArt = 0;
		contProv = 0;
		contRecital= 0;
		try {
			// 1 .MIRAMOS SI TIENE CONSOLIDADO CON LA REFERENCIA
			documentBOEConsolidado =externalDocumentProvider.getDocumentBOEConsolidado(reference);
			if (documentBOEConsolidado.startsWith("Error")) {// IBERLEX
				//1 LLAMAMOS AL DOCUMENTO IBERLEX
				documentBOEIBERLEX= externalDocumentProvider.getBOEDocument(reference);
				//byte[] contentBytesUTF8 = makeFromELIorIBERLEX(documentBOEIBERLEX.getBytes(StandardCharsets.UTF_8),"IBERLEX");
				decodedToUTF8 = new String(documentBOEIBERLEX.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
				if (decodedToUTF8.startsWith("No se")) {
					return aknDocument;
				}
				byte[] contentBytesUTF8 = makeFromELIorIBERLEXAlt(decodedToUTF8.getBytes(StandardCharsets.UTF_8), "IBERLEX");
				aknDocument = makeAknDocumentConso(contentBytesUTF8, reference);
				if (accion.equals("AMENDMENT")) {
					titulo = getTituloBOE(decodedToUTF8);
					eli = getEliBOEIberlex(reference);
					aknDocument = titulo + ":::" + aknDocument + ":::" + eli;
				}
			} else if (!documentBOEConsolidado.startsWith("Error")){  // CONSOLIDADO
				decodedToUTF8 = new String(documentBOEConsolidado.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
				byte[] contentBytesUTF8 = decodedToUTF8.getBytes(StandardCharsets.UTF_8);
				aknDocument = makeAknDocumentConso(contentBytesUTF8, reference);
				if (accion.equals("AMENDMENT")) {
					titulo = getTituloBOE(decodedToUTF8);
					eli = getEliBOEIberlex(reference);
					aknDocument = titulo + ":::" + aknDocument + ":::" + eli;
				}
			}

		} catch (Exception e) {
			throw new RuntimeException("Unable to get the document in formex format", e);
		}
		return aknDocument;
	}
	//https://www.boe.es/eli/es/l/2015/10/01/39/dof/spa/xml ELI
	//https://boe.es/buscar/mpr.php?documento=BOE-A-2015-10565 IBERLEX
	private String getEliBOEIberlex (String reference ) { //cuarentena!!!
		String documentBOEIBERLEX = "";
		String tipoNorma = "";
		String jurisdiccion = "";
		String fecha = "";
		String numero = "";
		String identificadorELI = "https://www.boe.es/eli";
		documentBOEIBERLEX= externalDocumentProvider.getBOEDocument(reference);
		String decodedToUTF8 = new String(documentBOEIBERLEX.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
		
		//tipo de normativa 
		Pattern rng = Pattern.compile("<RNG>(.*?)</RNG>"); 
		Matcher matcherRng= rng.matcher(decodedToUTF8);
		if (matcherRng.find()) {
			tipoNorma = matcherRng.group(1);
			if (tipoLey(tipoNorma) != null) {
				tipoNorma = "/" + tipoLey(matcherRng.group(1));
			} else {
				return "No soportado----" + tipoNorma;
			}
		} 	
		
		// jurisdiccion
		Pattern dem = Pattern.compile("<DEM>(.*?)</DEM>"); 
		Matcher matcherDem= dem.matcher(decodedToUTF8);
		if (matcherDem.find()) {
			jurisdiccion += "/" + jurisdiccion(matcherDem.group(1));
		}
		
		//fecha-año/mes/dia
		Pattern fap = Pattern.compile("<FAP>(.*?)</FAP>");
		Matcher matcherFap= fap.matcher(decodedToUTF8);
		if (matcherFap.find()) {
			fecha += "/" + new StringBuilder(matcherFap.group(1)).insert(4, "/").insert(7, "/").toString();
		}
		//nº ley
		Pattern nof = Pattern.compile("<NOF>(.*?)</NOF>");
		Matcher matcherNof= nof.matcher(decodedToUTF8);
		if (matcherNof.find()) {
			String[] numeroLey= matcherNof.group(1).split("/");
			if (numeroLey.length == 2) {
				numero = "/" + numeroLey[0];
			} else if (numeroLey.length == 3) {
				numero = "/" + numeroLey[0].toLowerCase() +  numeroLey[1].toLowerCase();
			}
		} else {
			numero = "/" + "(1)";
		}
		
		identificadorELI += jurisdiccion + tipoNorma + fecha + numero + ":::" + tipoNorma.replace("/", "");
		
		return identificadorELI;
	}
	
	private String tipoLey (String tipo) {
		Map<String,String> listaLeyesIn = new HashMap<String,String>();
		//Map<String,String> listaLeyesOut = new HashMap<String,String>();
		listaLeyesIn.put("Ley", "l");
		listaLeyesIn.put("Ley Orgánica", "lo");
		listaLeyesIn.put("Ley Foral", "lf");
		listaLeyesIn.put("Real Decreto", "rd");	
		listaLeyesIn.put("Real Decreto-ley", "rdl");
		listaLeyesIn.put("Real Decreto Legislativo", "rdlg");
		listaLeyesIn.put("Decreto-ley", "dl");
		listaLeyesIn.put("Decreto", "d");
		listaLeyesIn.put("Decreto Foral", "df");
		listaLeyesIn.put("Decreto-ley Foral", "dlf");
		listaLeyesIn.put("Decreto Legislativo", "dlg");
		listaLeyesIn.put("Decreto Foral Legislativo", "dflg");
		listaLeyesIn.put("Reglamento", "reg");
		listaLeyesIn.put("Acuerdo", "a");
		listaLeyesIn.put("Constitución", "c");
		listaLeyesIn.put("Orden", "o");
		listaLeyesIn.put("Orden Foral", "of");
		listaLeyesIn.put("Resolución", "res");
//		listaLeyesOut.put("Reforma", "ref");
//		listaLeyesOut.put("Instrucción", "ins");
//		listaLeyesOut.put("Circular", "cir");
//		listaLeyesOut.put("Corrección (errores o erratas)", "l");
		
		return listaLeyesIn.get(tipo);
	}
	
	private String jurisdiccion(String jurisdiccion) {
		Map<String,String> listaJurisdiccion = new HashMap<String,String>();
//		listaJurisdiccion.put("Estado", "es");	
//		listaJurisdiccion.put("Ministerio de Hacienda", "es");	
		listaJurisdiccion.put("Comunidad Autónoma de Andalucía", "es-an");	
		listaJurisdiccion.put("Comunidad Autónoma de Aragón", "es-ar");	
		listaJurisdiccion.put("Comunidad Autónoma del Principado de Asturias", "es-as");	
		listaJurisdiccion.put("Comunidad Autónoma de Canarias", "es-cn");	
		listaJurisdiccion.put("Comunidad Autónoma de Cantabria", "es-cb");	
		listaJurisdiccion.put("Comunidad de Castilla y León", "es-cl");	
		listaJurisdiccion.put("Comunidad Autónoma de Castilla-La Mancha", "es-cm");	
		listaJurisdiccion.put("Comunidad Autónoma de Cataluña", "es-ct");	
		listaJurisdiccion.put("Comunidad Autónoma de Extremadura", "es-ex");	
		listaJurisdiccion.put("Comunidad Autónoma de Galicia", "es-ga");
		listaJurisdiccion.put("Comunidad Autónoma de las Illes Balears", "es-ib");
		listaJurisdiccion.put("Comunidad Autónoma de la Rioja", "es-ri");		
		listaJurisdiccion.put("Comunidad de Madrid", "es-md");	
		listaJurisdiccion.put("Comunidad Autónoma de la Región de Murcia", "es-mc");		
		listaJurisdiccion.put("Comunidad Foral de Navarra", "es-nc");	
		listaJurisdiccion.put("Comunidad Autónoma del País Vasco", "es-pv");
		listaJurisdiccion.put("Comunitat Valenciana", "es-vc");

		if (listaJurisdiccion.get(jurisdiccion) != null) {
			return listaJurisdiccion.get(jurisdiccion);
		} else {
			return "es";
		}
	}
	

	@Override
	public boolean getBOEDocumentoDerogado(String reference) {
		String documentBOEConsolidado =externalDocumentProvider.getDocumentBOEConsolidado(reference);
		if (documentBOEConsolidado.startsWith("Error")) {// IBERLEX
			//1 LLAMAMOS AL DOCUMENTO IBERLEX
			String documentBOEIBERLEX;
			documentBOEIBERLEX= externalDocumentProvider.getBOEDocument(reference);
			//byte[] contentBytesUTF8 = makeFromELIorIBERLEX(documentBOEIBERLEX.getBytes(StandardCharsets.UTF_8),"IBERLEX");
			String decodedToUTF8 = new String(documentBOEIBERLEX.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
			if(lawDerogative(decodedToUTF8)) {
				return true;
			}
		} else if (!documentBOEConsolidado.startsWith("Error")){  // CONSOLIDADO
			String decodedToUTF8 = new String(documentBOEConsolidado.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
			if(lawDerogative(decodedToUTF8)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean getBOEDocumentoDerogado(String type, int year, int month, int day, int number) {
		String documentBOEELI = externalDocumentProvider.getBOEDocument(type, year, month, day, number);
		if(lawDerogative(documentBOEELI)) {
			return true;
		}
		return false;
	}
	
	private boolean lawDerogative(String document) {
   	 if (!document.equals("") && (document.contains("<derog>S</derog>")||document.contains("<DEROG>S</DEROG>"))) {
   		 return true;
   	 }
		return false;
	}
	
	private String makeAknDocumentConso (byte [] contentBytes, String identificador) {
		Map<String, List<String>> arrayVersion =  makeAllConso(contentBytes);
		Map<String, ArrayList<AGETreeNode>> treeNodeMap = new LinkedHashMap<String, ArrayList<AGETreeNode>>() ;
		String dispoType = "noDispo";
		try {
			Set<String> keys = arrayVersion.keySet();
			for (Iterator<String> i = keys.iterator(); i.hasNext();) {
				String key = i.next();
				List<String> value =  arrayVersion.get(key);
				//Correccion origen cuando etiqueta elementos jerárquicos como precepto en lugar de cabecera
				String primerP = "vacio";
				String Parte1PrimerP = primerP.toLowerCase().split("---")[0];
				if (Parte1PrimerP.equals("libro") || Parte1PrimerP.equals("titulo") || Parte1PrimerP.equals("capitulo") || Parte1PrimerP.equals("seccion") || Parte1PrimerP.equals("subseccion") ||
					Parte1PrimerP.equals("libro_num") || Parte1PrimerP.equals("titulo_num") || Parte1PrimerP.equals("capitulo_num") || Parte1PrimerP.equals("seccion_num") || Parte1PrimerP.equals("subseccion_num")) {
					key = key.replace("precepto", "encabezado");
				}
				//Fin corrección origen
				//Tratamiento Disposiciones en Leyes antiguas. Ejemplo: Constitución Española
				if (value.size() > 0) {
					primerP = value.get(0);
				}
				if (primerP.toLowerCase().contains("disposiciones transitorias")) {
					dispoType = "Trans";
				}
				if (primerP.toLowerCase().contains("disposiciones adicionales")) {
					dispoType = "Adic";
				}
				if (primerP.toLowerCase().contains("disposiciones finales")) {
					dispoType = "Final";
				}
				if (primerP.toLowerCase().contains("disposiciones derogatorias")) {
					dispoType = "Derog";
				}
				if (value.size() > 0) {
					if (dispoType.equals("Trans")) {
						value.set(0, value.get(0).replace("articulo---" + value.get(0).split("---")[1], "articulo---Disposición transitoria " + value.get(0).split("---")[1].toLowerCase()));
					}
					if (dispoType.equals("Adic")) {
						value.set(0, value.get(0).replace("articulo---" + value.get(0).split("---")[1], "articulo---Disposición adicional " + value.get(0).split("---")[1].toLowerCase()));
					}
					if (dispoType.equals("Final")) {
						value.set(0, value.get(0).replace("articulo---" + value.get(0).split("---")[1], "articulo---Disposición final " + value.get(0).split("---")[1].toLowerCase()));
					}
					if (dispoType.equals("Derog")) {
						value.set(0, value.get(0).replace("articulo---" + value.get(0).split("---")[1], "articulo---Disposición derogatoria " + value.get(0).split("---")[1].toLowerCase()));
					}
					if (primerP.toLowerCase().equals("capitulo---disposición transitoria") || primerP.toLowerCase().equals("capitulo---disposicion transitoria")) {
						key = key.replace("encabezado", "precepto");
						value.set(0, value.get(0).replace("DISPOSICION TRANSITORIA", "Disposición transitoria única").replace("DISPOSICIÓN TRANSITORIA", "Disposición transitoria única"));
					}
					if (primerP.toLowerCase().equals("capitulo---disposición adicional") || primerP.toLowerCase().equals("capitulo---disposicion adicional")) {
						key = key.replace("encabezado", "precepto");
						value.set(0, value.get(0).replace("DISPOSICION ADICIONAL", "Disposición adicional única").replace("DISPOSICIÓN ADICIONAL", "Disposición adicional única"));
					}
					if (primerP.toLowerCase().equals("capitulo---disposición final") || primerP.toLowerCase().equals("capitulo---disposicion final")) {
						key = key.replace("encabezado", "precepto");
						value.set(0, value.get(0).replace("DISPOSICION FINAL", "Disposición final única").replace("DISPOSICIÓN FINAL", "Disposición final única"));
					}
					if (primerP.toLowerCase().equals("capitulo---disposición derogatoria") || primerP.toLowerCase().equals("capitulo---disposicion derogatoria")) {
						value.set(0, value.get(0).replace("DISPOSICION DEROGATORIA", "Disposición derogatoria única").replace("DISPOSICIÓN DEROGATORIA", "Disposición derogatoria única"));
						key = key.replace("encabezado", "precepto");
					}
				}
				
				//Fin Tratamiento Disposiciones en Leyes antiguas
				ArrayList<AGETreeNode> tree = (ArrayList<AGETreeNode>) crearJerarquiaConso(key, value, identificador);		
				if (tree != null && !tree.isEmpty()) {
					treeNodeMap.put(key, tree);
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Unable to get the document in makeAknDocumentConso", e);
		}
		return transformBOEConso(treeNodeMap);
	}
	
	private Map<String, List<String>>  makeAllConso (byte [] texto) {
		Map<String, List<String>> textos;
		textos = xmlContentProcessor.bloqueVersion(texto);
		return textos;
	}
	
	public List<AGETreeNode> crearJerarquiaConso (String tipo, List<String> texto, String identificador) {
 		List<AGETreeNode> arbol = new ArrayList<>();
		String[] tipoSplit = tipo.split("-");
		switch (tipoSplit[1]) {
		case "preambulo":
			return crearPreambuloConso(texto, identificador);
		case "encabezado":
			return crearEncabezadoConso(texto);
		case "precepto":
			return crearPreceptoConsoAlt(texto, identificador);		
		}
		return arbol;
	}
	
	private byte [] makeFromELIorIBERLEX (byte [] document, String type) {
		List<String> auxP = xmlContentProcessor.crearConsolidado(document, type);
		StringBuilder result = new StringBuilder(); 
		boolean seccion = false;
		boolean finalTexto = false;
		String [] arrayP;
		result.append("<texto>");
		result.append("<bloque tipo=\"preambulo\">");
		result.append("<version>");
		for (String string : auxP) {
			arrayP = string.split("---");
			String aux = arrayP[1].replaceFirst("<p[^>]+>", "").replaceFirst("</p>", "");
			if ((arrayP[0].startsWith("titulo_num") || arrayP[0].startsWith("capitulo_num"))) {
				if (!seccion) {
					result.append("</version>");
					result.append("</bloque>");
					seccion = false;
				}
				result.append("<bloque tipo=\"encabezado\">");
				result.append("<version>");
				result.append(arrayP[1]);
			}  else if (arrayP[0].startsWith("seccion")) {
				seccion = true;
				result.append("</version>");
				result.append("</bloque>");
				result.append("<bloque tipo=\"encabezado\">");
				result.append("<version>");
				result.append(arrayP[1]);
				result.append("</version>");
				result.append("</bloque>");
			} else if(arrayP[0].contains("articulo")) {
				if (!seccion) {
					result.append("</version>");
					result.append("</bloque>");
					seccion = false;
				}
				result.append("<bloque tipo=\"precepto\">");
				result.append("<version>");
				result.append(arrayP[1]);
			}else if(aux.startsWith("Por tanto,") || aux.startsWith("Dado en Madrid,")) {
				result.append("</version>");
				result.append("</bloque>");
				result.append("</texto>");
				break;
			}else if (!finalTexto) {
				result.append(arrayP[1]);
			}
				
		}
		String kk = result.toString();
		return result.toString().getBytes(StandardCharsets.UTF_8);
	}

	private byte [] makeFromELIorIBERLEXAlt (byte [] document, String type) {
		List<String> auxP = xmlContentProcessor.crearConsolidado(document, type);
		if (auxP.size() == 0) {
			auxP.add("parrafo---<p class=\"parrafo\">El texto consultado no se encuentra disponible en BOE en un formato estructurado</p>");
		}
		StringBuilder result = new StringBuilder(); 
		boolean insideElement = false;
		boolean insideTextoRegulatorio = false;
		boolean finalTexto = false;
		String [] arrayP;
		result.append("<texto>");
		result.append("<bloque tipo=\"preambulo\">");
		result.append("<version>");
		insideElement = true;
		int numP = 0;
		for (String string : auxP) {
			arrayP = string.split("---");
			String aux = arrayP[1].replaceFirst("<p[^>]+>", "").replaceFirst("</p>", "");
			if (aux.startsWith("«")) {
				insideTextoRegulatorio = true;
			}
			if (!insideTextoRegulatorio) {
				if (arrayP[0].startsWith("titulo_num") || arrayP[0].startsWith("capitulo_num") || arrayP[0].startsWith("seccion") || arrayP[0].startsWith("capitulo")) {
					if (insideElement) {
						result.append("</version>");
						result.append("</bloque>");
						insideElement = false;
					}
					result.append("<bloque tipo=\"encabezado\">");
					result.append("<version>");
					result.append(arrayP[1]);
					insideElement = true;
				} else if(arrayP[0].contains("articulo")) {
					if (insideElement) {
						result.append("</version>");
						result.append("</bloque>");
						insideElement = false;
					}
					result.append("<bloque tipo=\"precepto\">");
					result.append("<version>");
					result.append(arrayP[1]);
					insideElement = true;
				} else if(aux.toLowerCase().startsWith("por tanto,") || aux.toLowerCase().startsWith("dado en ")
						|| aux.toLowerCase().startsWith("anexo ") || aux.toLowerCase().startsWith("apendice ") || aux.toLowerCase().startsWith("apéndice ")
						|| arrayP[0].toLowerCase().equals("publicado")) {
					if (insideElement) {
						result.append("</version>");
						result.append("</bloque>");
						result.append("</texto>");
					}
					break;
				} else if (numP == (auxP.size() - 1) ) {					
					if (insideElement) {
						result.append(arrayP[1]);
						result.append("</version>");
						result.append("</bloque>");
						result.append("</texto>");
					}
					break;				
				} else if (!finalTexto) {
					result.append(arrayP[1]);
				}
			}
			else {
				result.append(arrayP[1]);
				if (aux.endsWith("»") || aux.endsWith("».")) {
					insideTextoRegulatorio = false;
				}				
			}
			numP++;
		}
		String kk = result.toString();
		return result.toString().getBytes(StandardCharsets.UTF_8);
	}	
	
	private String transformBOEConso(Map<String, ArrayList<AGETreeNode>> treeNodos) {
		StringBuilder result = new StringBuilder();
		Set<String> keys = treeNodos.keySet();
		try {
			for (Iterator<String> i = keys.iterator(); i.hasNext();) {
				String key = i.next();
				List<AGETreeNode> nodos = treeNodos.get(key);
				String [] tipos = key.split("-");
						switch (tipos[1]) {
						case "encabezado":
							result.append(transformBOEEncabezadoConso(nodos));
							break;
						case "preambulo": 
							result.append(transformBOEPreambuloConso(nodos));
							break;
						case "precepto":
							result.append(transformBOEPreceptoConsoAlt(nodos));
							break;
						}
					}
			
		} catch (Exception e) {
			System.out.println ("transformBOEConso: " + e.toString());
			throw new RuntimeException("Unable to get the document in transformBOEConso", e);
		}
		result.append("</aknBody>\n");
		result.append("</bill>\n");
		result.append("</akomaNtoso>\n");
		// LOG.error(result.toString());
		return result.toString();
	}

	private List<AGETreeNode> crearPreambuloConso (List<String> preambulo, String identificador) {
		List<AGETreeNode> treeNode = new ArrayList<>();
		String type = null;
		String[] auxTipo;
		int cont = 0;
		int contReverse = 0;
		String ident = null;
		AGETreeNode nodo = null;
		AGETreeNode nodoFinal1 = null;
		AGETreeNode nodoFinal2 = null;
		int numLineas = preambulo.size();
		try {
			for(int i = (numLineas-1); (i > (numLineas-3)) && (i>0); i--) {
				String auxReverse = preambulo.get(i);
				auxTipo = auxReverse.split("---");
				if (auxTipo[1].contains("DISPONGO") || auxTipo[1].contains("D I S P O N G O")) {
					preambulo.remove(i);
				} else if (auxTipo[1].toLowerCase().contains("en su virtud") || auxTipo[1].toLowerCase().contains("consejo de Ministros")
						|| auxTipo[1].toLowerCase().contains("el pueblo español ratifica")) {
					if (nodoFinal2 == null) {
						type = "preambulo-formula_final_2";
						nodoFinal2 = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_prea_", 9), "0", auxTipo[1] ,  null, false);
					}
					else {
						type = "preambulo-formula_final_1";
						nodoFinal1 = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_prea_", 9), "0", auxTipo[1] ,  null, false);
					}
					preambulo.remove(i);
				} else if (auxTipo[1].toLowerCase().contains("constitución")) {
					if (nodoFinal2 == null) {
						type = "preambulo-formula_final_2";
						nodoFinal2 = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_prea_", 9), "0", auxTipo[1] ,  null, false);
					}
					else {
						type = "preambulo-formula_final_1";
						nodoFinal1 = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_prea_", 9), "0", auxTipo[1] ,  null, false);
					}
					preambulo.remove(i);
				}
			}
			for(String aux : preambulo) {
				auxTipo = aux.split("---");
				if (contRecital == 0 ) {
					if (auxTipo[1].toLowerCase().contains("felipe") || auxTipo[1].toLowerCase().contains("juan") || auxTipo[1].toLowerCase().contains("franco") || auxTipo[1].toLowerCase().contains("isabel")) {
						type = "preambulo-preamble_king";
						nodo = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_prea_", 9), "0", auxTipo[1] ,  null, false);	
					} else if (auxTipo[1].toLowerCase().contains("rey") || auxTipo[1].toLowerCase().contains("president") || auxTipo[1].toLowerCase().contains("franco") || auxTipo[1].toLowerCase().contains("lehendakari")) {
						type = "preambulo-preamble_king_role";
						nodo = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_prea_", 9), "0", auxTipo[1] ,  null, false);	
					} else if (auxTipo[1].toLowerCase().contains("vieren y entendieren")) {
						type = "preambulo-formula_1";
						nodo = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_prea_", 9), "0", auxTipo[1] ,  null, false);	
					} else if (auxTipo[1].toLowerCase().contains("han aprobado") || auxTipo[1].toLowerCase().contains("ha aprobado")
							|| auxTipo[1].toLowerCase().contains("han decretado") || auxTipo[1].toLowerCase().contains("gracia de dios")
							|| auxTipo[1].equals("El texto consultado no se encuentra disponible en BOE en un formato estructurado")) {
						type = "preambulo-formula_2";
						nodo = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_prea_", 9), "0", auxTipo[1] ,  null, false);	
					} else if (auxTipo[1].contains("EXPOSICIÓN") || auxTipo[1].contains("EXPOSICION") || auxTipo[1].contains("PREÁMBULO") || auxTipo[1].contains("DISPONGO")) {
						nodo = null;
					}
					else if (auxTipo[1].length() < 6 && auxTipo[1].equalsIgnoreCase("I")) {
						type = "preambulo-recital_num";
						contRecital ++;
						ident = "rec_" + identificador + "_" + contRecital;
						nodo = new AGETreeNode(type, 1, 0, ident, "0", "I" ,  null, false);
					} else {
						type = "preambulo-recital_num";
						contRecital ++;
						ident = "rec_" + identificador + "_" + contRecital;
						nodo = new AGETreeNode(type, 1, 0, ident, "0", "I" ,  null, false);
						treeNode.add(nodo);
						type = "preambulo-recital_p";
						nodo = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_prea_", 9), "0", auxTipo[1] ,  null, false);						
					}
				} else {	
					if (calculatePreaumbuloNum(auxTipo[1])) {
						type = "preambulo-recital_num";
						contRecital ++;
						ident = "rec_" + identificador + "_" + contRecital;
						nodo = new AGETreeNode(type, 1, 0, ident, "0", auxTipo[1] ,  null, false);
					} else {
						type = "preambulo-recital_p";
						nodo = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_prea_", 9), "0", auxTipo[1] ,  null, false);
					}
				}
				if(nodo != null) {
					treeNode.add(nodo);
					cont++;
				}
			}
			if (nodoFinal1 != null) {
				treeNode.add(nodoFinal1);
			}
			if (nodoFinal2 != null) {
				treeNode.add(nodoFinal2);
			}			
		}catch (Exception e) {
			throw new RuntimeException("Unable to get the document in crearPreambuloConso", e);
		}
		return treeNode;
	}
	
	
	private List<AGETreeNode> crearEncabezadoConso (List<String> encabezado) {
		List<AGETreeNode> treeNode = new ArrayList<>();
		String[] tipos = null;
		String type = null;
		String typeClass = null;
		int cont = 0;
		try {
			for(String aux : encabezado) {
				AGETreeNode nodo = null;
				if (cont==0) {
					tipos = aux.split("---");
					typeClass=tipos[0];
					tipos = tipos[1] .trim().split(" ");
					if (tipos[0].startsWith("B")) {
						type = "encabezado-libro";
					} else if (tipos[0].startsWith("T")) {
						type = "encabezado-titulo";
					} else if (tipos[0].startsWith("C")) {
						type = "encabezado-capitulo";
					} else if (tipos[0].startsWith("L")) {
						type = "encabezado-libro";
					} else if (tipos[0].startsWith("SEC")) {
						type = "encabezado-seccion";
					} else if (tipos[0].startsWith("SUB")) {
						type = "encabezado-subseccion";
					} else if (tipos[0].startsWith("DISPO")) { //Caso Constitucion Española y Leyes antiguas
						type = "encabezado-disposicion";	
					} else if (tipos[0].startsWith("A")) {
						return null;
					}
					if (type == null) {
						if (typeClass.contains("libro")) {
							type = "encabezado-libro";
						}
						else if (typeClass.contains("titulo")) {
							type = "encabezado-titulo";
						}
						else if (typeClass.contains("capitulo")) {
							type = "encabezado-capitulo";
						}
						else if (typeClass.contains("subseccion")) {
							type = "encabezado-subseccion";
						}
						else if (typeClass.contains("seccion")) {
							type = "encabezado-seccion";
						}
					}
					if (type != null) {
						if (tipos.length == 1) {
							String aux2 = tipos[0].replaceAll("\\u00a0", " ");//UNICODE
							String []tiposAux = aux2 .trim().split(" ");
							if (tiposAux.length == 2) {
								nodo = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_encabe_", 9), tiposAux[1], aux2.replace(tiposAux[1], aux2),  null, false);
								treeNode.add(nodo);	
							}
						} else {
							if (encabezado.size() == 1) {
								tipos = aux.split("---");
								nodo = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_encabe_", 9), tipos[1], tipos [1],  null, false);
								treeNode.add(nodo);	
							} else {
								nodo = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_encabe_", 9), tipos[1], tipos[0] + " " + tipos[1],  null, false);
								treeNode.add(nodo);	
							}
							
						}
					}
					else {
						return null;
					}
				} else {
					type = "encabezado-cuerpo";
					tipos = aux.split("---");
					nodo = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_encabe_", 9), "1", tipos[1],  null, false);
					treeNode.add(nodo);	
				}
				cont++;			
			}   
		}catch (Exception e) {
			throw new RuntimeException("Unable to get the document in crearPreambuloConso", e);
		}
		return treeNode;
	}
	
	private boolean calculatePreaumbuloNum (String aux) {
		for (int i = 0; i < numNivel4romanos.length; i++) {
			if (aux.length() < 6 && aux.equalsIgnoreCase(numNivel4romanos[i]))
				return true;
		}
		return false;
	}
	
	private StringBuilder transformBOEEncabezadoConso (List<AGETreeNode> treeNodes) {
		StringBuilder result = new StringBuilder();
		String[] tipos;
		try {
			for (AGETreeNode treeNode : treeNodes) {
				if (treeNode != null) {
					tipos = treeNode.getType().split("-");	
					switch (tipos[1]) {
					case "titulo":
						encabezadoType = "T";
						result.append("<aknTitle xml:id=\"" + treeNode.getIdentifier() + "\">\n"); 
						result.append("<num xml:id=\"" + IdGenerator.generateId("boe_title_", 9).trim() + "\">" + treeNode.getContent() + "</num>\n"); 
						if (treeNodes.size() == 1) {
							result.append(getEncabezadoType());
						}
						break;
					case "cuerpo":
						result.append("<heading xml:id=\""+ IdGenerator.generateId("boe_heading_", 9).trim() + "\">" + treeNode.getContent() + "</heading>\n");
						if (treeNode == treeNodes.get(treeNodes.size()-1)) {
							result.append(getEncabezadoType());
						}
						break;
					case "capitulo":
						encabezadoType = "C";
						result.append("<chapter xml:id=\"" + treeNode.getIdentifier() + "\">\n"); 
						result.append("<num xml:id=\"" + IdGenerator.generateId("boe_chap_", 9).trim() + "\">" + treeNode.getContent() + "</num>\n");
						if (treeNodes.size() == 1) {
							result.append(getEncabezadoType());
						}
						break;
					case "libro":
						encabezadoType = "B";
						result.append("<book xml:id=\"" + treeNode.getIdentifier() + "\">\n"); 
						result.append("<num xml:id=\"" + IdGenerator.generateId("boe_book_", 9).trim() + "\">" + treeNode.getContent() + "</num>\n");
						if (treeNodes.size() == 1) {
							result.append(getEncabezadoType());
						}
						break;
					case "seccion":
						encabezadoType = "SEC";
						String num = treeNode.getContent().substring(0,treeNode.getContent().indexOf('ª')+1);
						result.append("<section xml:id=\"" + treeNode.getIdentifier() + "\">\n"); 
						result.append("<num xml:id=\"" + IdGenerator.generateId("boe_sect_", 9).trim() + "\">" + num + "</num>\n");
						if (treeNodes.size() == 1) {
							result.append("<heading xml:id=\""+ IdGenerator.generateId("boe_heading_", 9).trim() + "\">" + treeNode.getContent().replace(num, "").trim()+ " </heading>\n");
							result.append("</section>");	
						}
						break;
					case "subseccion":
						encabezadoType = "SUB";
						String num2 = treeNode.getContent().substring(0,treeNode.getContent().indexOf('ª')+1);
						result.append("<subsection xml:id=\"" + treeNode.getIdentifier() + "\">\n"); 
						result.append("<num xml:id=\"" + IdGenerator.generateId("boe_sect_", 9).trim() + "\">" + num2 + "</num>\n");
						if (treeNodes.size() == 1) {
							result.append("<heading xml:id=\""+ IdGenerator.generateId("boe_heading_", 9).trim() + "\">" + treeNode.getContent().replace(num2, "").trim()+ " </heading>\n");
							result.append("</subsection>");	
						}
						break;
					case "disposicion":  //Caso Constitucion española y leyes antiguas
						encabezadoType = "C";
						result.append("<chapter xml:id=\"" + treeNode.getIdentifier() + "\">\n"); 
						result.append("<num xml:id=\"" + IdGenerator.generateId("boe_chap_", 9).trim() + "\">" + treeNode.getContent() + "</num>\n");
						if (treeNodes.size() == 1) {
							result.append(getEncabezadoType());
						}
						break;
					}
				}
			}
		}catch (Exception e) {
			System.out.println ("transformBOEEncabezadoConso: " + e.toString());
			throw new RuntimeException("Unable to get the document in transformBOEEncabezadoConso", e);
		}
		return result;
	}
	
	/*
	 * LEVELS : 0-> Artículo/Disposición
	 * 			1 -> 1./Uno./treinta y Cinco.
	 * 			2 -> a)/b)/...zz)
	 * 			3 -> 1.º/2.º/.....n.º
	 * 			4 -> i)/ii)/.....n)
	 * 			5 -> "Sin nada"
	 */
	private List<AGETreeNode> crearPreceptoConso (List<String> precepto, String identificador) {
		List<AGETreeNode> arbol = new ArrayList<>();
		AGETreeNode[] ultimosNodos= new AGETreeNode[6];
		AGETreeNode parentAux = null;
		String [] string;
		int total = 0;
		AGETreeNode nodo = null;
		String num, type;
		boolean errorLevel3 = false;
		boolean rule = false;
		for (String stringAux : precepto) {
			string = stringAux.split("---");
			try {
				if ( (string[1].startsWith("Art") ||string[1].startsWith("Disposici")) && total == 0 ) {//level0
					num =string[1].substring(0, string[1].indexOf('.')+1);
					String ident = null;
					type = string[1].startsWith("Art")? "precepto-article":"precepto-proviso";
					if (string[1].startsWith("Art")) {
						contArt++;
						ident = "art_" + identificador + "_" + contArt;
					} else {
						contProv++;
						ident = "dis_" + identificador + "_" + contProv;
					}	
					nodo = new AGETreeNode(type, 0, 0, ident, num, string[1].replace(num, "").trim() ,  null, false);
					arbol.add(nodo);
					ultimosNodos[0] = nodo;
					total++;
				} else {
					if (total >= 1) {
						if (string[1].startsWith("«") || rule) {
							rule = true;
							type = "precepto-rule";
							nodo = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_rule_", 9), "rule", string[1],  ultimosNodos[1], false);
							arbol.add(nodo);
							if (string[1].endsWith("»") || string[1].substring(string[1].length()-2, string[1].length()-1).contentEquals("»")) {
								rule = false;
								nodo.setType("precepto-rulefin");
							}
							if (nodo.getParent() != null) {
								nodo.getParent().addChildren(nodo);
							}	
						} else {
							Pattern numLevel1 = Pattern.compile("^(\\d+\\.[^(º|ª)])");
							Matcher matcherLevel1 = numLevel1.matcher(string[1].trim());
							if (matcherLevel1.find()) {//level1 1.
								errorLevel3 = false;
								num = matcherLevel1.group(1);
								type = "precepto-paragraph";
								nodo = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_para_", 9), num, string[1].replace(num, "").trim() ,  ultimosNodos[0], false);
								arbol.add(nodo);
								ultimosNodos[1] = nodo;
								if (nodo.getParent() != null) {
									nodo.getParent().addChildren(nodo);
								}
								total++;
							} else {
								Pattern numLevel2 = Pattern.compile("^((\\w{1,2}|ñ)\\))");
								Matcher matcherLevel2 = numLevel2.matcher(string[1].trim());
								Pattern numLevel1Letras = Pattern.compile("^([A-Za-z]*\\.)");
								Matcher matcherlevel1Letras  = numLevel1Letras.matcher(string[1].trim());
								if (matcherLevel2.find() || string[1].startsWith("ñ")) {//level2 a)
									num = matcherLevel2.group(1);
									String anterior = null;
									if (ultimosNodos[2] != null) {
										anterior = ultimosNodos[2].getNum();
									} 
									if (comprobarLetra(num, anterior) && !arbol.get(arbol.size()-1).isChange()){								
										errorLevel3 = false;
										type = "precepto-point";
										nodo = new AGETreeNode(type, 2, 0, IdGenerator.generateId("boe_para_", 9),num.toLowerCase(), string[1].replace(num, "").trim() ,  ultimosNodos[1], false);
										arbol.add(nodo);
										ultimosNodos[nodo.getDepth()] = nodo;
										if (nodo.getParent() != null) {
											nodo.getParent().addChildren(nodo);
										}						
										total++;
									}else {
										if (arbol.get(arbol.size()-1).getDepth()!= 4 || arbol.get(arbol.size()-1).getDepth()!= 3 ) {
											if (anterior != null && !anterior.equals("vacio")) {
											String nuevaLetra =	recalcularLetra(anterior) + ")";
											errorLevel3 = false;
											type = "precepto-point";
											nodo = new AGETreeNode(type, 2, 0, IdGenerator.generateId("boe_para_", 9),nuevaLetra.toLowerCase(), string[1].replace(num, "").trim() ,  ultimosNodos[1], true);
											arbol.add(nodo);
											ultimosNodos[nodo.getDepth()] = nodo;
											if (nodo.getParent() != null) {
												nodo.getParent().addChildren(nodo);
											}						
											total++;
											}
										} else {
											Pattern numLevel4 = Pattern.compile("^(M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\\))");
											Matcher matcherLevel4 = numLevel4.matcher(string[1].trim().toUpperCase());
											if (matcherLevel4.find()) {//level4 ii)
												errorLevel3 = false;
												num = matcherLevel4.group(1);
												type = "precepto-point";
												nodo = new AGETreeNode(type, 4, 0, IdGenerator.generateId("boe_point_", 9), num.toLowerCase(), string[1].replace(num, "").trim() ,  ultimosNodos[3], false);
												arbol.add(nodo);
												ultimosNodos[4] = nodo;
												if (nodo.getParent() != null) {
													nodo.getParent().addChildren(nodo);
												}	
												total++;
											}
										}
									}
								} else if (matcherlevel1Letras.find()) { // level1 uno.
									errorLevel3 = false;
									num = matcherlevel1Letras.group(1);
									type = "precepto-paragraph";
									nodo = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_para_", 9), num, string[1].replace(num, "").trim() ,  ultimosNodos[0], false);
									arbol.add(nodo);
									ultimosNodos[1] = nodo;
									if (nodo.getParent() != null) {
										nodo.getParent().addChildren(nodo);
									}
									total++;
								} else {
									Pattern numLevel3 = Pattern.compile("^(\\d.(º|ª))"); 
									Matcher matcherLevel3 = numLevel3.matcher(string[1].trim());
									if (matcherLevel3.find()) {//level3 1.ª
										num = matcherLevel3.group(1);
										string[1] = string[1].replace(num, "").trim();
										num = num.replace("º", "ª");
										type = "precepto-point";
										if (ultimosNodos[2] != null && (arbol.get(arbol.size()-1).getDepth() == 2 || arbol.get(arbol.size()-1).getDepth() == 3) && !errorLevel3) {
											nodo = new AGETreeNode(type, 3, 0, IdGenerator.generateId("boe_point_", 9), num, string[1],  ultimosNodos[2], false);
											ultimosNodos[3] = nodo;
										} else if ( errorLevel3 ){
											int cont = 0;
											for (String numero : numNivel3) {
												if (numero.equals(num)) {
													num = numNivel2arrayLetters[cont] + ")";
													break;
												}
												cont++;
											}
											nodo = new AGETreeNode(type, 2, 0, IdGenerator.generateId("boe_point_", 9), num, string[1],  parentAux, false);
											ultimosNodos[2] = nodo;
										} else if ( arbol.get(arbol.size()-1).getDepth() == 1 ){
											int cont = 0;
											for (String numero : numNivel3) {
												if (numero.equals(num)) {
													num = numNivel2arrayLetters[cont] + ")";
													break;
												}
												cont++;
											}
											parentAux = arbol.get(arbol.size()-1);
											nodo = new AGETreeNode(type, 2, 0, IdGenerator.generateId("boe_point_", 9), num, string[1],  parentAux , true);
											ultimosNodos[2] = nodo;
											errorLevel3 = true;

										}

										arbol.add(nodo);
										if (nodo.getParent() != null) {
											nodo.getParent().addChildren(nodo);
										}	
										total++;
									} else {
										if (total == 1) {//level1
											errorLevel3 = false;
											num = "vacio";
											type = "precepto-paragraph";
											nodo = new AGETreeNode(type, 1, 0, IdGenerator.generateId("boe_para_", 9), num, string[1].trim() , ultimosNodos[0], false);
											arbol.add(nodo);
											ultimosNodos[1] = nodo;
											if (nodo.getParent() != null) {
												nodo.getParent().addChildren(nodo);
											}	
											total++;
										} else {
											nodo = arbol.get(arbol.size()-1);
											nodo.setContents(string[1]);
										}
									}
								}
							}
						}
					}	
				}

			}catch (Exception e) {
				throw new RuntimeException("Unable to get the document in crearPreceptoConso", e);
			}
		}
		return arbol;

	}

	private List<AGETreeNode> crearPreceptoConsoAlt (List<String> precepto, String identificador) {

		List<AGETreeNode> arbol = new ArrayList<>();
		int nivel=0;
		int total=0;
		String ident=null;
		boolean rule=false;
		String [] string;
		String contenidoP;
		String contenidoPAux;
		String firstWord;
		String num;
		String type;
		String xmlid;
		Integer nPatron = null;
		AGETreeNode nodo;
		Integer checkPrimerPatron;
		boolean esHijo;
		boolean esAncestro;
		boolean containsRule = false;
		int numError1=0;
		int numError2=0;

		List<String> patronesPrimer = new ArrayList<>();
		List<Boolean> patronesCaseSensitive = new ArrayList<>();
		List<String> patronesRegExp = new ArrayList<>();
		
		Integer[] tipoNumPorNivel = new Integer[10];
		AGETreeNode[] ultimoPorNivel= new AGETreeNode[10];
		
		//En orden de los patrones es MUY relevante
		//Los patrones deben ser lo mas precisos y exclusivos posible
		//Un valor de patronesPrimer no debe ser 'cumplido' por un patrón de expresión regular previo.
		//Por ejemplo, si una cadena patronesPrimer cumple la 8º expresión, no debería poder cumplir la 6ª patronesRegExp
		//Las excepciones a esta regla deben ser tratadas en el algoritmo
		//Es el caso de i), v), l), x)
		//Si una cadena cumple varios patronesPrimer (Ej.: 1.1.1), deberán estar primero en la lista la cadena mas larga 

		patronesPrimer.add("1.º");
		patronesCaseSensitive.add(true);
		patronesRegExp.add("^(\\d+.(º))");
		
		patronesPrimer.add("1.ª");
		patronesCaseSensitive.add(true);
		patronesRegExp.add("^(\\d+.(ª))");

		patronesPrimer.add("\\d+.\\d+.1");
		patronesCaseSensitive.add(true);
		patronesRegExp.add("^(\\d+\\.\\d+\\.\\d+)");

		patronesPrimer.add("\\d+.1");
		patronesCaseSensitive.add(true);
		patronesRegExp.add("^(\\d+\\.\\d+)(?!(\\.\\d))(?!(\\d+\\.\\d))");		
		
		patronesPrimer.add("1.");
		patronesCaseSensitive.add(true);
		patronesRegExp.add("^(\\d+( bis| ter| quater| quáter| quinquies| sexies)*\\.)(?!º)(?!ª)(?!\\d)");
		
		patronesPrimer.add("a)");
		patronesCaseSensitive.add(true);
		patronesRegExp.add("^((a{0,1}[a-z]{0,1}'{0,1}|ñ|añ)( bis| ter| quater| quáter| quinquies| sexies)*\\))");
		
		patronesPrimer.add("A)");
		patronesCaseSensitive.add(true);
		patronesRegExp.add("^((A{0,1}[A-Z]{0,1}|Ñ|AÑ)\\))");		
		
		patronesPrimer.add("i)");
		patronesCaseSensitive.add(true);
		patronesRegExp.add("^((xc|xl|l?x{0,3})(ix|iv|v?i{0,3})\\))");
		
		patronesPrimer.add("I)");
		patronesCaseSensitive.add(true);
		patronesRegExp.add("^((XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\\))");
		
		patronesPrimer.add("primer");
		patronesCaseSensitive.add(false);
		patronesRegExp.add("^((primer.?|segund.|tercer.?|cuart.|quint.|sext.|séptim.|septim.|octav.|noven.|decim.|undécim.|undecim.|duodécim.|duodecim.|décimo.*|decimo.*|vigésimo.*|vigesimo.*\\trige.*|cuadra.*|quincua.*|sexa.*|septua.*|octo.*|nona.*)( bis| ter| quater| quáter| quinquies| sexies)*(\\.|:|$))");

		patronesPrimer.add("uno.");
		patronesCaseSensitive.add(false);
		patronesRegExp.add("^((uno|dos|tres|cuatro|cinco|seis|siete|ocho|nueve|diez|once|doce|trece|catorce|quince|dieci.*|veinte|veinti.*|treinta.*|cuarenta.*|cincuenta.*|sesenta.*|setenta.*|ochenta.*|noventa.*)(\\.|:|$))");

		patronesPrimer.add("–");
		patronesCaseSensitive.add(true);
		patronesRegExp.add("^(–)"); 

		patronesPrimer.add("-");
		patronesCaseSensitive.add(true);
		patronesRegExp.add("^(-)"); 		
		
		String patronPrimerModificativo=("uno\\.|primero\\.|1\\.");
		
		for (String stringAux : precepto) {
			string = stringAux.split("---");
			contenidoP=string[1];
			if (contenidoP.indexOf(' ') >=0 ) {
				firstWord=contenidoP.substring(0, contenidoP.indexOf(' ')).trim();
			}
			else {
				firstWord=contenidoP;
			}
			if (firstWord.startsWith("«")) {
				containsRule = true;
			}
		}
		
		for (String stringAux : precepto) {
			string = stringAux.split("---");
			String tipoP=string[0];
			contenidoP=string[1];
			contenidoPAux = contenidoP.replace(" bis","_bis").replace("tercero","XXterceroXX").replace(" ter","_ter").replace(" quater","_quater").replace(" quáter","_quáter").replace(" quinquies","_quinquies").replace(" sexies","_sexies").replace("XXterceroXX", "tercero");
			if (contenidoPAux.indexOf(' ') >=0 ) {
				Integer posicionEspacio = contenidoPAux.indexOf(' ');
				firstWord=contenidoP.substring(0, posicionEspacio).trim();
			}
			else {
				firstWord=contenidoP;
			}
			try {
			switch(total) {
				case 0:
					if (contenidoP.startsWith("Art") ||contenidoP.toLowerCase().startsWith("disposici")) {//level0
						num =contenidoP.substring(0, contenidoP.indexOf('.')+1);
						if (num.equals("")) {
							num = contenidoP;
						}
						type = contenidoP.startsWith("Art")? "precepto-article":"precepto-proviso";
						if (contenidoP.startsWith("Art")) {
							contArt++;
//							ident = "art_" + identificador + "_" + contArt + "_" + getAlphaNumericString(2);
							ident = "art_" + identificador + "_" + contArt;
						} else {
							contProv++;
//							ident = "dis_" + identificador + "_" + contProv + "_" + getAlphaNumericString(2);
							ident = "dis_" + identificador + "_" + contProv;
						}	
						nodo = new AGETreeNode(type, nivel, 0, ident, num, contenidoP.replace(num, "").trim() ,  null, false);
						arbol.add(nodo);
						ultimoPorNivel[nivel] = nodo;
						total++;
/*						LOG.debug(contenidoP);
						if (contenidoP.contains("Artículo 169")) {
							LOG.debug("ENCONTRADO");
						}  */
					}
					else {
						numError1++;
						LOG.info( "Error 1"  + ": La primera línea del precepto no contenía la palabra artículo o disposición" + identificador + " - " + contenidoP);
					}
				break;
				case 1:
						nivel++;
						type = "precepto-paragraph";					
						checkPrimerPatron=buscarPrimerPatron(firstWord, patronesPrimer, patronesCaseSensitive);
						tipoNumPorNivel[nivel]=checkPrimerPatron;						
						if (tipoNumPorNivel[nivel] == null ) { //Precepto de apartado único (sin numerar)
							num="vacio";
							nodo = new AGETreeNode(type, nivel, 0, IdGenerator.generateId("boe_para_", 9), num, contenidoP.trim() ,  ultimoPorNivel[nivel-1], false);
						}
						else {		//Precepto con varios apartados numerados
							num=devuelveNum(firstWord, checkPrimerPatron, patronesRegExp, patronesCaseSensitive);
							nodo = new AGETreeNode(type, nivel, 0, IdGenerator.generateId("boe_para_", 9), num, contenidoP.replaceFirst(Pattern.quote(num), "").trim() ,  ultimoPorNivel[nivel-1], false);
						}
						arbol.add(nodo);
						ultimoPorNivel[nivel] = nodo;
						if (nodo.getParent() != null) {
							nodo.getParent().addChildren(nodo);
						}
						total++;
				break;
				default:
						if (!firstWord.startsWith("«") && !rule) {
							checkPrimerPatron=buscarPrimerPatron(firstWord, patronesPrimer, patronesCaseSensitive);											
							if (checkPrimerPatron != null) { //Nodo Hijo
								esHijo=true;
								num=devuelveNum(firstWord, checkPrimerPatron, patronesRegExp, patronesCaseSensitive);
								//Check caso especial precepto modificativo
								Pattern wordPatternMod = Pattern.compile(patronPrimerModificativo,Pattern.CASE_INSENSITIVE);
								Matcher wordMatcherMod = wordPatternMod.matcher(firstWord);
								if (wordMatcherMod.find() && (total == 2) && containsRule) {
									esHijo=false;
									tipoNumPorNivel[nivel]=	checkPrimerPatron;
								}
								//Check caso especial cuando i) o I) pertenece a nivel a), b), ...
								if (((num.equals("i)") ) && (ultimoPorNivel[nivel].getNum().equals("h)")))
									|| ((num.equals("I)") ) && (ultimoPorNivel[nivel].getNum().equals("H)")))) {
									esHijo=false;
								}
								//Check casos de enumeraciones con guiones, asteriscos, ...
								if (num.equals(ultimoPorNivel[nivel].getNum())) {
									esHijo=false;
								}
								if (esHijo == true) { //Caso general Nodo Hijo
									nivel++;
									tipoNumPorNivel[nivel]=	checkPrimerPatron;
								}
								if (nivel == 1)  {
									type = "precepto-paragraph";
									xmlid = IdGenerator.generateId("boe_para_", 9);
								}
								else {
									type = "precepto-point";
									xmlid = IdGenerator.generateId("boe_point_", 9);
								}							
								nodo = new AGETreeNode(type, nivel, 0, xmlid, num, contenidoP.replaceFirst(Pattern.quote(num),"").trim() ,  ultimoPorNivel[nivel-1], false);
								arbol.add(nodo);
								ultimoPorNivel[nivel] = nodo;
								if (nodo.getParent() != null) {
									nodo.getParent().addChildren(nodo);
								}
							}
						else {
							nPatron=buscarPatron(firstWord, patronesRegExp, patronesCaseSensitive);
							num=devuelveNum(firstWord, nPatron, patronesRegExp, patronesCaseSensitive);
							if ((nPatron == tipoNumPorNivel[nivel]) && (nPatron != null)) { //Nodo Hermano
								if (nivel == 1)  {
									type = "precepto-paragraph";
									xmlid = IdGenerator.generateId("boe_para_", 9);
								}
								else {
									type = "precepto-point";
									xmlid = IdGenerator.generateId("boe_point_", 9);
								}												
								nodo = new AGETreeNode(type, nivel, 0, xmlid, num, contenidoP.replaceFirst(Pattern.quote(num), "").trim() ,  ultimoPorNivel[nivel-1], false);
								arbol.add(nodo);
								ultimoPorNivel[nivel] = nodo;
								if (nodo.getParent() != null) {
									nodo.getParent().addChildren(nodo);
								}								
							}
							if ((nPatron != tipoNumPorNivel[nivel]) && (nPatron != null) ) {
								boolean encontrado = false;
								int indexNivel=0;
								for (Integer indexPatron: tipoNumPorNivel) {									
									if ((nPatron == indexPatron) && (encontrado == false)) { // Nodo Ancestro
										encontrado = true;
										esAncestro=true;
										//Tratamiento v), d) y x)
										if (((num.equals("v)") ) && (ultimoPorNivel[nivel].getNum().equals("iv)")))
											|| ((num.equals("V)") ) && (ultimoPorNivel[nivel].getNum().equals("IV)")))
											|| ((num.equals("l)") ) && (ultimoPorNivel[nivel].getNum().equals("xlix)")))
											|| ((num.equals("L)") ) && (ultimoPorNivel[nivel].getNum().equals("XLIX)")))
											|| ((num.equals("x)") ) && (ultimoPorNivel[nivel].getNum().equals("ix)")))
											|| ((num.equals("X)") ) && (ultimoPorNivel[nivel].getNum().equals("IX)")))) {
											esAncestro=false;
										}
										if (esAncestro) {
											nivel=indexNivel;
										}
										if (nivel == 1)  {
											type = "precepto-paragraph";
											xmlid = IdGenerator.generateId("boe_para_", 9);
										}
										else {
											type = "precepto-point";
											xmlid = IdGenerator.generateId("boe_point_", 9);
										}												
										nodo = new AGETreeNode(type, nivel, 0, xmlid, num, contenidoP.replaceFirst(Pattern.quote(num), "").trim() ,  ultimoPorNivel[nivel-1], false);
										arbol.add(nodo);
										ultimoPorNivel[nivel] = nodo;
										if (nodo.getParent() != null) {
											nodo.getParent().addChildren(nodo);
										}															
									}
									indexNivel++;
								}
								if (encontrado == false) {
									numError2++;
									LOG.info("Error 2" + ": Algo ha ido mal, cumple una expresión que no se corresponde con un nivel ya comenzado" + identificador + " - " + contenidoP);
									nodo=ultimoPorNivel[nivel];
									nodo.setContents(contenidoP.trim());
								}
							}
							if ((nPatron == null) && (rule == false)) { //Es un parrafo que corresponde al último nodo tratado
								nodo=ultimoPorNivel[nivel];
								nodo.setContents(contenidoP.trim());
							}							
						}
					}
					else {
						if (rule == false) {
							rule = true;
							nivel++;
						}
						type = "precepto-rule";
						xmlid = IdGenerator.generateId("boe_rule_", 9);
						num="vacio";
						nodo = new AGETreeNode(type, nivel, 0, xmlid, num, contenidoP.trim() ,  ultimoPorNivel[nivel-1], false);
						arbol.add(nodo);
						if (contenidoP.endsWith("»") || contenidoP.substring(contenidoP.length()-2, contenidoP.length()-1).contentEquals("»")) {
							rule = false;
							nivel--;
							nodo.setType("precepto-rulefin");
							}
						if (nodo.getParent() != null) {
								nodo.getParent().addChildren(nodo);
						}
					}
					total++;
				}
			} catch (Exception e) {
				throw new RuntimeException("Unable to get the document in crearPreceptoConso", e);
			}
		}
		if ((numError1>0) || (numError2>0)) {
			LOG.info(identificador + ": Encontrados " + numError1 + " errores Tipo 1 y " + numError2 + " errores Tipo 2");
		}
		return arbol;
	}
	
    static String getAlphaNumericString(int n) 
    { 
  
        // chose a Character random from this String 
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                    + "0123456789"
                                    + "abcdefghijklmnopqrstuvxyz"; 
  
        // create StringBuffer size of AlphaNumericString 
        StringBuilder sb = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            // generate a random number between 
            // 0 to AlphaNumericString variable length 
            int index 
                = (int)(AlphaNumericString.length() 
                        * Math.random()); 
  
            // add Character one by one in end of sb 
            sb.append(AlphaNumericString 
                          .charAt(index)); 
        } 
     
        return sb.toString(); 
    }
	
	static Integer buscarPrimerPatron(String iword, List<String> patronesPrimer, List<Boolean> patronesCaseSensitive) {
		int index = 0;
		for (String primerAux : patronesPrimer) {
			String primerAuxPattern = "^(" + primerAux.replace(")","\\)").replace(".","\\.") + ")" + "(?!(\\.\\d))(?!(\\d))"; // En cuarentena
//			String primerAuxPattern = "(" + primerAux.replace(")","\\)").replace(".","\\.") + ")";
			Pattern wordPattern = Pattern.compile("^" + primerAuxPattern);
			Matcher wordMatcher = wordPattern.matcher(iword);
			if (patronesCaseSensitive.get(index) == false) {
				wordMatcher = wordPattern.matcher(iword.toLowerCase());
			}
			if (wordMatcher.find()) {
					return index;
			}
			index++;
		}
		return null;
	}
	
	static Integer buscarPatron(String oword, List<String> patronesRegExp, List<Boolean> patronesCaseSensitive) {
		int index2 = 0;
		for (String primerAux2 : patronesRegExp) {
			Pattern patternCheck = Pattern.compile(patronesRegExp.get(index2));
			if (!patronesCaseSensitive.get(index2)) {
				patternCheck = Pattern.compile(patronesRegExp.get(index2), Pattern.CASE_INSENSITIVE);		
			}
			Matcher matcherCheck = patternCheck.matcher(oword);
			if (matcherCheck.find()) {
				return index2;
			}
			index2++;
		}
		return null;
	}
	
	static String devuelveNum(String word, Integer RegExpIndex, List<String> patronesRegExp, List<Boolean> patronesCaseSensitive) {
		if (RegExpIndex == null) {
			return null;
		}
		Pattern patternCheck = Pattern.compile(patronesRegExp.get(RegExpIndex));
		if (!patronesCaseSensitive.get(RegExpIndex)) {
			patternCheck = Pattern.compile(patronesRegExp.get(RegExpIndex), Pattern.CASE_INSENSITIVE);		
		}
		Matcher matcherCheck = patternCheck.matcher(word);
		if (matcherCheck.find()){
			return matcherCheck.group();
		}
		else {
			return null;
		}
	}	
	
	private StringBuilder transformBOEPreambuloConso (List<AGETreeNode> treeNodes) {
		StringBuilder result = new StringBuilder();
		boolean abiertoBloqueRecital = false;
		boolean abiertoFormula = false;
		boolean abiertoFormulaFinal = false;
		boolean abiertoRecital = false;
		result.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		result.append("<akomaNtoso xmlns=\"http://docs.oasis-open.org/legaldocml/ns/akn/3.0\" xmlns:leos=\"urn:eu:europa:ec:leos\">\n");
		result.append("<bill>\n");
		result.append("<preamble xml:id=\"preamble\">\n");
		String formula_final_1 = null;
		String formula_final_2 = null;
		try {
			for (AGETreeNode treeNode : treeNodes) {
				if (treeNode != null) {
					String[] tipos = treeNode.getType().split("-");
					switch (tipos[1]) {
					case "preamble_king":
						if (!abiertoFormula) {
							result.append("<formula xml:id=\"preamble__formula_1\" name=\"initial_formula\" leos:editable=\"true\" leos:deletable=\"false\">\n");
							abiertoFormula = true;
						}
						result.append("<p xml:id=\"preamble_king\" refersTo=\"~SIGN\">"+ treeNode.getContent() + " </p>\n");
						break;
					case "preamble_king_role":
						if (!abiertoFormula) {
							result.append("<formula xml:id=\"preamble__formula_1\" name=\"initial_formula\" leos:editable=\"true\" leos:deletable=\"false\">\n");
							abiertoFormula = true;
						}
						result.append("<p xml:id=\"preamble_king_role\" refersTo=\"~SIGN\">"+ treeNode.getContent() + " </p>\n");
						break;
					case "formula_1":
						if (!abiertoFormula) {
							result.append("<formula xml:id=\"preamble__formula_1\" name=\"initial_formula\" leos:editable=\"true\" leos:deletable=\"false\">\n");
							abiertoFormula = true;
						}
						result.append("<p xml:id=\"formula_1\">"+ treeNode.getContent() + " </p>\n");
						break;
					case "formula_2":
						if (!abiertoFormula) {
							result.append("<formula xml:id=\"preamble__formula_1\" name=\"initial_formula\" leos:editable=\"true\" leos:deletable=\"false\">\n");
							abiertoFormula = true;
						}
						result.append("<p xml:id=\"formula_2\">"+ treeNode.getContent() +" </p>\n");
						break;
					case "recital_num":
						if (abiertoFormula) {
							result.append("</formula>\n");
							abiertoFormula=false;
						}
						if (!abiertoBloqueRecital) {
							result.append("<citations xml:id=\"cits\" leos:editable=\"true\">\n");		
							result.append("<citation refersTo=\"~legalBasis\" xml:id=\"cit_fake\" leos:editable=\"false\" leos:deletable=\"false\">\n");
							result.append("<p xml:id=\"cit_fake__p\"></p>\n");
							result.append("</citation>\n");
							result.append("</citations>\n");
							result.append("<recitals xml:id=\"recs\" leos:editable=\"false\">\n");
							result.append("<intro xml:id=\"reason_exposition\">\n");
							result.append("<p xml:id=\"recs_exposition\">EXPOSICIÓN DE MOTIVOS</p>\n");
							result.append("</intro>\n");
							abiertoBloqueRecital=true;
						}
						if (!abiertoRecital) {
							result.append("<recital xml:id=\"" + treeNode.getIdentifier() +"\" leos:editable=\"true\">\n");
							result.append("<num xml:id=\"" + IdGenerator.generateId("rec_", 9) + "\">" + treeNode.getContent() + " </num>\n");	
							abiertoRecital=true;
						} else {
							result.append("</recital>\n");	
							result.append("<recital xml:id=\"" + treeNode.getIdentifier() +"\" leos:editable=\"true\">\n");
							result.append("<num xml:id=\"" + IdGenerator.generateId("rec_", 9) + "\">" + treeNode.getContent() + " </num>\n");
						}
						break;
					case "recital_p":
						result.append("<p xml:id=\"" + IdGenerator.generateId("rec_", 9) + "\">" + treeNode.getContent() + " </p>\n");
						break;
					case "formula_final_1":
						formula_final_1 = treeNode.getContent();
						break;
					case "formula_final_2":
						formula_final_2 = treeNode.getContent();
						break;				
					}
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Unable to get the document in transformBOEPreambuloConso", e);
		}
		if (abiertoFormula) {
			result.append("</formula>\n");
			abiertoFormula=false;
		}
		if (abiertoRecital) {
			result.append("</recital>\n");
			abiertoRecital=false;
		}
		if (abiertoBloqueRecital) {
			result.append("</recitals>\n");
			abiertoBloqueRecital=false;
		}
		abiertoBloqueRecital=false;
		if (formula_final_1 != null) {
			if (!abiertoFormulaFinal) {
				result.append("<formula xml:id=\"preamble__formula_final_1\" name=\"final_formula\" leos:editable=\"true\" leos:deletable=\"false\">\n");
				abiertoFormulaFinal = true;
			}
			result.append("<p xml:id=\"formula_2\">"+ formula_final_1 +" </p>\n");
		}
		if (formula_final_2 != null) {
			if (!abiertoFormulaFinal) {
				result.append("<formula xml:id=\"preamble__formula_final_1\" name=\"final_formula\" leos:editable=\"true\" leos:deletable=\"false\">\n");
				abiertoFormulaFinal = true;
			}
			result.append("<p xml:id=\"formula_2\">"+ formula_final_2 +" </p>\n");
		}
		if (abiertoFormulaFinal) {
			result.append("</formula>\n");
			abiertoFormulaFinal=false;
		}		
		result.append("</preamble>\n");
		result.append("<aknBody xml:id=\"body\">\n");
		return result;
	}
	
	private StringBuilder transformBOEPreceptoConso (List<AGETreeNode> treeNodes) {
		StringBuilder result = new StringBuilder();
		List <String> contents;
		String type = null;
		int posicionEntreHermanos = -1;
		try {
			for (AGETreeNode treeNode : treeNodes) {
				if (treeNode != null) {
					String[] tipos = treeNode.getType().split("-");	
					String num = treeNode.getNum().equals("vacio") ? "" : treeNode.getNum();
					switch (treeNode.getDepth()) {
					case 0: //Articulo
						if (tipos[1].equals(ARTICLE)) {
							type = "article";
							result.append("<article leos:deletable=\"true\" leos:editable=\"true\" leos:mod=\"false\" xml:id=\"" + treeNode.getIdentifier() + "\">\n");
							result.append("<num leos:editable=\"false\" xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() + "\">" + treeNode.getNum() + " </num>\n");
							result.append("<heading xml:id=\"" + IdGenerator.generateId("boe_art_head_", 9) + "\">" + treeNode.getContent() + " </heading>\n");	
						} else {
							type = "proviso";
							result.append("<proviso leos:deletable=\"true\" leos:editable=\"true\" leos:mod=\"false\" xml:id=\"" + treeNode.getIdentifier() + "\">\n");
							result.append("<num leos:editable=\"false\" xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() + "\">" + num + " </num>\n");
							result.append("<heading xml:id=\"" + IdGenerator.generateId("boe_art_head_", 9) + "\">" + treeNode.getContent() + " </heading>\n");	
						}
						break;
					case 1: //1.
						if (tipos[1].contains("rule")) {
							result.append("<rule  xml:id=\"" + IdGenerator.generateId("boe_subp_", 9) + "\">\n");
							result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\"><p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  treeNode.getContent() + " </p></content>\n");
							result.append("</rule>\n");
							posicionEntreHermanos = calcularPosicionHermano(treeNode);
							if (tipos[1].contains("fin") && treeNode.getParent().getChildren().size() == posicionEntreHermanos) {
								result.append("</paragraph>\n");
							}
						} else if (treeNode.getChildren().isEmpty()) { 
							result.append("<paragraph xml:id=\"" + treeNode.getIdentifier() + "\">\n");
							//RM - Correctivo #2766 - RM - Correctivo #2767
							if (treeNodes.size() != 2) {
								result.append("<num leos:editable=\"false\" xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() + "\">" + num+  " </num>\n");
							}							
							if (!treeNode.getContents().isEmpty() ) {
								result.append("<subparagraph  xml:id=\"" + IdGenerator.generateId("boe_subp_", 9) + "\">\n");
								result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\"><p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  treeNode.getContent()  + " </p></content>\n");
								result.append("</subparagraph>\n");
								contents=treeNode.getContents();
								for (String contentsAux : contents) {
									result.append("<subparagraph  xml:id=\"" + IdGenerator.generateId("boe_subp_", 9) + "\">\n");
									result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">");
									if (contentsAux.startsWith("<table")) {
										result.append(contentsAux);	
									}else {
										result.append("<p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  contentsAux + " </p>");
									}
									result.append("</content>\n");
									result.append("</subparagraph>\n");
								}
							} else {
								result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\"><p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  treeNode.getContent() + " </p></content>\n");
							}
							result.append("</paragraph>\n");
						} else { 
							result.append("<paragraph xml:id=\"" + treeNode.getIdentifier() + "\">\n");
							result.append("<num leos:editable=\"false\" xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() + "\">" + num+  " </num>\n");
							result.append("<subparagraph  xml:id=\"" + IdGenerator.generateId("boe_subp_", 9) + "\">\n");
							result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\"><p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  treeNode.getContent() + " </p></content>\n");
							result.append("</subparagraph>\n");
							if (!treeNode.getContents().isEmpty() ) {
								contents=treeNode.getContents();
								for (String contentsAux : contents) {
									result.append("<subparagraph  xml:id=\"" + IdGenerator.generateId("boe_subp_", 9) + "\">\n");
									result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">");
									if (contentsAux.startsWith("<table")) {
										result.append(contentsAux);	
									}else {
										result.append("<p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  contentsAux + " </p>");
									}
									result.append("</content>\n");
									result.append("</subparagraph>\n");
								}
							} 
							if (!treeNode.getChildren().get(0).getType().contains("rule")) {
								result.append("<list  xml:id=\"" + IdGenerator.generateId("boe_list_", 9) + "\">\n");	
							} 
						}  
						break;
					case 2: // a)
					case 3: //1.º
					case 4: // i)
						if (treeNode.getChildren().isEmpty()) {
							result.append("<point xml:id=\"" + treeNode.getIdentifier() + "\">\n");
							result.append("<num leos:editable=\"false\" xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() + "\">" + num + " </num>\n");
							result.append("<alinea xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() +"\">");
							result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\"><p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  treeNode.getContent() + " </p></content>\n");
							result.append("</alinea>\n");
							if (!treeNode.getContents().isEmpty() ) {
								contents=treeNode.getContents();
								for (String contentsAux : contents) {
									result.append("<alinea xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() +"\">");
									result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">");
									if (contentsAux.startsWith("<table")) {
										result.append(contentsAux);	
									}else {
										result.append("<p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  contentsAux + " </p>");
									}
									result.append("</content>\n");
									result.append("</alinea>\n");
								}
							} 
							result.append("</point>\n");
							if (treeNode.getDepth() == 2) {
								posicionEntreHermanos = calcularPosicionHermano(treeNode);
								if (treeNode.getParent().getChildren().size() == posicionEntreHermanos) {
									result.append("</list>\n");
									result.append("</paragraph>\n");
								}

							} else if (treeNode.getDepth() == 3) {
								posicionEntreHermanos = calcularPosicionHermano(treeNode);
								if (treeNode.getParent().getChildren().size() == posicionEntreHermanos) {
									result.append("</list>\n");
									result.append("</point>\n");
									posicionEntreHermanos = calcularPosicionHermano(treeNode.getParent());
									if (treeNode.getParent().getParent().getChildren().size() == posicionEntreHermanos) {
										result.append("</list>\n");
										result.append("</paragraph>\n");
									}
								}
							}  else if (treeNode.getDepth() == 4) {
								posicionEntreHermanos = calcularPosicionHermano(treeNode);
								if (treeNode.getParent().getChildren().size() == posicionEntreHermanos) {
									result.append("</list>\n");
									result.append("</point>\n");
									posicionEntreHermanos = calcularPosicionHermano(treeNode.getParent());
									if (treeNode.getParent().getParent().getChildren().size() == posicionEntreHermanos) {
										result.append("</point>\n");
										result.append("</list>\n");
										posicionEntreHermanos = calcularPosicionHermano(treeNode.getParent().getParent());
										if (treeNode.getParent().getParent().getParent().getChildren().size() == posicionEntreHermanos) {
											result.append("</point>\n");
											result.append("</list>\n");
											result.append("</paragraph>\n");
										}
									}
								}
							}

						} else {
							result.append("<point xml:id=\"" + treeNode.getIdentifier() + "\">\n");
							result.append("<num leos:editable=\"false\" xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() + "\">" + num + " </num>\n");
							result.append("<alinea xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() +"\">");
							result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\"><p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  treeNode.getContent() + " </p></content>\n");
							result.append("</alinea>\n");	
							if (!treeNode.getContents().isEmpty() ) {
								for (String contentsAux : treeNode.getContents()) {
									result.append("<alinea xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() +"\">");
									result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">");
									if (contentsAux.startsWith("<table")) {
										result.append(contentsAux);	
									}else {
										result.append("<p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  contentsAux + " </p>");
									}
									result.append("</content>\n");
									result.append("</alinea>\n");
								}
							} 
							result.append("<list  xml:id=\"" + IdGenerator.generateId("boe_list_", 9) + "\">\n");
						}
						break;
					}
				}
			}	

		}catch (Exception e) {
			System.out.println("transformBOEPreceptoConso " + e.toString());
		}
		result.append("</"+type+">\n");
		return result;
	}

	private StringBuilder transformBOEPreceptoConsoAlt (List<AGETreeNode> treeNodes) {
		StringBuilder result = new StringBuilder();
		List <String> contents;
		String type = null;
		int posicionEntreHermanos = -1;
		int numNivel1 = 0;
		boolean checkMod = containsRule(treeNodes);
		try {
			for (AGETreeNode treeNode : treeNodes) {
				if (treeNode != null) {
					String[] tipos = treeNode.getType().split("-");	
					String num = treeNode.getNum().equals("vacio") ? "" : treeNode.getNum();
/*					if (num.contains("Artículo 50.")) {
						LOG.debug("PARADA");
					} */
					switch (treeNode.getDepth()) {
					case 0: //Articulo o Disposicion
						if (tipos[1].equals(ARTICLE)) {
							type = "article";
							if (checkMod == false) {
								result.append("<article leos:deletable=\"true\" leos:editable=\"true\" leos:mod=\"false\" xml:id=\"" + treeNode.getIdentifier() + "\">\n");
							}
							else {
								result.append("<article leos:deletable=\"true\" leos:editable=\"true\" leos:mod=\"true\" xml:id=\"" + treeNode.getIdentifier() + "\">\n");
							}
							result.append("<num leos:editable=\"false\" xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() + "\">" + treeNode.getNum() + " </num>\n");
							result.append("<heading xml:id=\"" + IdGenerator.generateId("boe_art_head_", 9) + "\">" + treeNode.getContent() + " </heading>\n");	
						} else {
							type = "proviso";
							if (checkMod == false) {
								result.append("<proviso leos:deletable=\"true\" leos:editable=\"true\" leos:mod=\"false\" xml:id=\"" + treeNode.getIdentifier() + "\">\n");							
							}
							else {
								result.append("<proviso leos:deletable=\"true\" leos:editable=\"true\" leos:mod=\"true\" xml:id=\"" + treeNode.getIdentifier() + "\">\n");
							}
							result.append("<num leos:editable=\"false\" xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() + "\">" + num + " </num>\n");
							result.append("<heading xml:id=\"" + IdGenerator.generateId("boe_art_head_", 9) + "\">" + treeNode.getContent() + " </heading>\n");	
						}
						numNivel1 = treeNode.getAGEChildren().size();
						break;
					case 1: //Apartado
							if (treeNode.getAGEChildren().isEmpty()) { 
							result.append("<paragraph xml:id=\"" + treeNode.getIdentifier() + "\">\n");
							//RM - Correctivo #2766 - RM - Correctivo #2767
							if (numNivel1 > 1) {
								result.append("<num leos:editable=\"false\" xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() + "\">" + num+  " </num>\n");
							}							
							if (!treeNode.getContents().isEmpty() ) {
								result.append("<subparagraph  xml:id=\"" + IdGenerator.generateId("boe_subp_", 9) + "\">\n");
								result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\"><p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  treeNode.getContent()  + " </p></content>\n");
								result.append("</subparagraph>\n");
								contents=treeNode.getContents();
								for (String contentsAux : contents) {
									result.append("<subparagraph  xml:id=\"" + IdGenerator.generateId("boe_subp_", 9) + "\">\n");
									result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">");
									if (contentsAux.startsWith("<table")) {
										result.append(contentsAux);	
									}else {
										result.append("<p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  contentsAux + " </p>");
									}
									result.append("</content>\n");
									result.append("</subparagraph>\n");
								}
							} else {
								result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\"><p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  treeNode.getContent() + " </p></content>\n");
							}
							result.append("</paragraph>\n");
						} else { 
							result.append("<paragraph xml:id=\"" + treeNode.getIdentifier() + "\">\n");
							if (numNivel1 > 1) {
								result.append("<num leos:editable=\"false\" xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() + "\">" + num+  " </num>\n");
							}
							result.append("<subparagraph  xml:id=\"" + IdGenerator.generateId("boe_subp_", 9) + "\">\n");
							result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\"><p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  treeNode.getContent() + " </p></content>\n");
							result.append("</subparagraph>\n");
							if (!treeNode.getContents().isEmpty() ) {
								contents=treeNode.getContents();
								for (String contentsAux : contents) {
									result.append("<subparagraph  xml:id=\"" + IdGenerator.generateId("boe_subp_", 9) + "\">\n");
									result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">");
									if (contentsAux.startsWith("<table")) {
										result.append(contentsAux);	
									}else {
										result.append("<p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  contentsAux + " </p>");
									}
									result.append("</content>\n");
									result.append("</subparagraph>\n");
								}
							} 
							if (!treeNode.getAGEChildren().get(0).getType().contains("rule")) {
								result.append("<list  xml:id=\"" + IdGenerator.generateId("boe_list_", 9) + "\">\n");	
							} 
						}  
						break;
					default: // Subapartados y rule
						if (tipos[1].contains("rule")) { //Tratamiento <rule>
							result.append("<rule  xml:id=\"" + IdGenerator.generateId("boe_subp_", 9) + "\">\n");
							result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\"><p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  treeNode.getContent() + " </p></content>\n");
							result.append("</rule>\n");
							posicionEntreHermanos = calcularPosicionHermano(treeNode);
							if (treeNode.getDepth() == 2) {
								posicionEntreHermanos = calcularPosicionHermano(treeNode);
								if ((treeNode.getParent().getAGEChildren().size() == posicionEntreHermanos)) {
									result.append("</paragraph>\n");
								}

							} else if (treeNode.getDepth() == 3) {
								posicionEntreHermanos = calcularPosicionHermano(treeNode);
								if ((treeNode.getParent().getAGEChildren().size() == posicionEntreHermanos)) {
									result.append("</point>\n");
									posicionEntreHermanos = calcularPosicionHermano(treeNode.getParent());
									if ((treeNode.getParent().getParent().getAGEChildren().size() == posicionEntreHermanos)) {
										result.append("</list>\n");
										result.append("</paragraph>\n");
									}
								}
							}  else if (treeNode.getDepth() == 4) {
								posicionEntreHermanos = calcularPosicionHermano(treeNode);
								if ((treeNode.getParent().getAGEChildren().size() == posicionEntreHermanos)) {
									result.append("</point>\n");
									posicionEntreHermanos = calcularPosicionHermano(treeNode.getParent());
									if ((treeNode.getParent().getParent().getAGEChildren().size() == posicionEntreHermanos)) {
										result.append("</list>\n");
										result.append("</point>\n");
										posicionEntreHermanos = calcularPosicionHermano(treeNode.getParent().getParent());
										if ((treeNode.getParent().getParent().getParent().getAGEChildren().size() == posicionEntreHermanos)) {
											result.append("</list>\n");
											result.append("</paragraph>\n");
										}
									}
								}
							}
							break;
						}
						if (treeNode.getAGEChildren().isEmpty()) {
							result.append("<point xml:id=\"" + treeNode.getIdentifier() + "\">\n");
							result.append("<num leos:editable=\"false\" xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() + "\">" + num + " </num>\n");
							if (!treeNode.getContents().isEmpty() ) {
								result.append("<alinea xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() +"\">");
								result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\"><p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  treeNode.getContent() + " </p></content>\n");
								result.append("</alinea>\n");
								contents=treeNode.getContents();
								for (String contentsAux : contents) {
									result.append("<alinea xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() +"\">");
									result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">");
									if (contentsAux.startsWith("<table")) {
										result.append(contentsAux);	
									}else {
										result.append("<p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  contentsAux + " </p>");
									}
									result.append("</content>\n");
									result.append("</alinea>\n");
								}
							}
							else {
								result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\"><p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  treeNode.getContent() + " </p></content>\n");
							}
							result.append("</point>\n");
							if (treeNode.getDepth() == 2) {
								posicionEntreHermanos = calcularPosicionHermano(treeNode);
								if (treeNode.getParent().getAGEChildren().size() == posicionEntreHermanos) {
									result.append("</list>\n");
									result.append("</paragraph>\n");
								}

							} else if (treeNode.getDepth() == 3) {
								posicionEntreHermanos = calcularPosicionHermano(treeNode);
								if (treeNode.getParent().getAGEChildren().size() == posicionEntreHermanos) {
									result.append("</list>\n");
									result.append("</point>\n");
									posicionEntreHermanos = calcularPosicionHermano(treeNode.getParent());
									if (treeNode.getParent().getParent().getAGEChildren().size() == posicionEntreHermanos) {
										result.append("</list>\n");
										result.append("</paragraph>\n");
									}
								}
							}  else if (treeNode.getDepth() == 4) {
								posicionEntreHermanos = calcularPosicionHermano(treeNode);
								if (treeNode.getParent().getAGEChildren().size() == posicionEntreHermanos) {
									result.append("</list>\n");
									result.append("</point>\n");
									posicionEntreHermanos = calcularPosicionHermano(treeNode.getParent());
									if (treeNode.getParent().getParent().getAGEChildren().size() == posicionEntreHermanos) {
										result.append("</list>\n");
										result.append("</point>\n");
										posicionEntreHermanos = calcularPosicionHermano(treeNode.getParent().getParent());
										if (treeNode.getParent().getParent().getParent().getAGEChildren().size() == posicionEntreHermanos) {
											result.append("</list>\n");
											result.append("</paragraph>\n");
										}
									}
								}
							} else if (treeNode.getDepth() == 5) {
								posicionEntreHermanos = calcularPosicionHermano(treeNode);
								if (treeNode.getParent().getAGEChildren().size() == posicionEntreHermanos) {
									result.append("</list>\n");
									result.append("</point>\n");
									posicionEntreHermanos = calcularPosicionHermano(treeNode.getParent());
									if (treeNode.getParent().getParent().getAGEChildren().size() == posicionEntreHermanos) {
										result.append("</list>\n");
										result.append("</point>\n");
										posicionEntreHermanos = calcularPosicionHermano(treeNode.getParent().getParent());
										if (treeNode.getParent().getParent().getParent().getAGEChildren().size() == posicionEntreHermanos) {
											result.append("</list>\n");
											result.append("</point>\n");
											posicionEntreHermanos = calcularPosicionHermano(treeNode.getParent().getParent().getParent());
											if (treeNode.getParent().getParent().getParent().getParent().getAGEChildren().size() == posicionEntreHermanos) {
												result.append("</list>\n");
												result.append("</paragraph>\n");
											}
										}
									}
								}
							}
						} else {
							result.append("<point xml:id=\"" + treeNode.getIdentifier() + "\">\n");
							result.append("<num leos:editable=\"false\" xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() + "\">" + num + " </num>\n");
							result.append("<alinea xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() +"\">");
							result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\"><p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  treeNode.getContent() + " </p></content>\n");
							result.append("</alinea>\n");	
							if (!treeNode.getContents().isEmpty() ) {
								for (String contentsAux : treeNode.getContents()) {
									result.append("<alinea xml:id=\"" + IdGenerator.generateId("boe_art_num_", 9).trim() +"\">");
									result.append("<content xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">");
									if (contentsAux.startsWith("<table")) {
										result.append(contentsAux);	
									}else {
										result.append("<p xml:id=\"" + IdGenerator.generateId("boe_content_", 9) + "\">" +  contentsAux + " </p>");
									}
									result.append("</content>\n");
									result.append("</alinea>\n");
								}
							}
							if (!treeNode.getAGEChildren().get(0).getType().contains("rule")) {
								result.append("<list  xml:id=\"" + IdGenerator.generateId("boe_list_", 9) + "\">\n");
							}
						}
						break;
					}
				}
			}	

		}catch (Exception e) {
			System.out.println("transformBOEPreceptoConso " + e.toString());
		}
		result.append("</"+type+">\n");
		return result;
	}	
	
	private boolean containsRule (List<AGETreeNode> treeNodes) {
		for (AGETreeNode treeNode : treeNodes) {
			if (treeNode != null) {
				String[] tipos = treeNode.getType().split("-");
				if (tipos[1].contains("rule")) {
					return true;
				}
			}			
		}
		return false;
	}
	
	private String getEncabezadoType () {
		String type = "";
		switch (encabezadoType) {
		case "B":
			type = "</book>";
			break;
		case "T":
			type = "</aknTitle>";
			break;
		case "C":
			type = "</chapter>";
			break;
		case "SEC":
			type = "</section>";
			break;
		case "SUB":
			type = "</subsection>";
			break;
		}
		return type;
	}
	
	private int calcularPosicionHermano (AGETreeNode nodo) {
		int pos=1;
		try {
			List<AGETreeNode> identificadores = nodo.getParent().getAGEChildren(); 
			for (AGETreeNode treeNode : identificadores) {
				if (nodo.getIdentifier().equals(treeNode.getIdentifier())) {
					return pos;
				}
				pos++;
			}
		}catch (Exception e) {
			System.out.println(e.toString());
		}

		return pos;
	}
	
	private boolean comprobarLetra (String letra, String anterior) {
		letra = letra.substring(0, letra.length() - 1);
		if (anterior != null && !anterior.equals("vacio") && !letra.equals("a")) {
			anterior = anterior.substring(0, anterior.length() - 1);			
			for (int pos = 0; pos < numNivel2arrayLetters.length-1;pos++) {
				if (numNivel2arrayLetters[pos].contentEquals(letra)) {
					if (numNivel2arrayLetters[pos-1].equals(anterior)) {
						return true;
					}
				}
			}	
		} else {
			return true;
		}
		return false;
	}
	
	private String recalcularLetra (String letra) {
		String letraAux = null;
		letra = letra.substring(0, letra.length() - 1);
		for (int pos = 0; pos < numNivel2arrayLetters.length-1;pos++) {
			if (numNivel2arrayLetters[pos].equals(letra)) {
				letraAux = numNivel2arrayLetters[pos+1];
			}
		}	
		return letraAux;		
	}

}
