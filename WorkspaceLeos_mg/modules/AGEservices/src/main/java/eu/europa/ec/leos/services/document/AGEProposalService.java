package eu.europa.ec.leos.services.document;

import java.util.Map;

import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.cmis.document.Proposal;

public interface AGEProposalService extends ProposalService {
	
	Proposal updateComponentRef(Proposal proposal, Map<String, String> hrefs, LeosCategory leosCategory);

}
