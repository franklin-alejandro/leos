package eu.europa.ec.leos.services.content.processor;

import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.PROVISO;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.SIGNATURE;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.QUOTEDSTRUCTURE;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.ARTICLE;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.BODY;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.CITATION;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.CLAUSE;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.FORMULA;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.RECITAL;

import eu.europa.ec.leos.services.support.xml.AGEExtRefUpdateOutput;

import java.io.UnsupportedEncodingException;
import java.util.List;
import javax.inject.Provider;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.ximpleware.NavException;

import eu.europa.ec.leos.domain.cmis.Content;
import eu.europa.ec.leos.domain.cmis.document.Bill;
import eu.europa.ec.leos.domain.cmis.metadata.BillMetadata;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.services.support.xml.NumberProcessor;
import eu.europa.ec.leos.services.support.IdGenerator;
import eu.europa.ec.leos.services.support.xml.AGENumberProcessor;
import eu.europa.ec.leos.services.support.xml.AGEXmlHelper;
import eu.europa.ec.leos.services.support.xml.XmlContentProcessor;
import eu.europa.ec.leos.services.support.xml.AGEXmlContentProcessor;
import eu.europa.ec.leos.services.toc.StructureContext;

@Primary
@Service
public class AGEBillProcessorImpl extends BillProcessorImpl implements AGEBillProcessor {

	//RM-Correctivo #2902
    @Value("${leos.paragraph.templates.modify}")
    private String modifyTemplate;
	
	private MessageHelper messageHelper;
	
	private AGEXmlContentProcessor AGExmlContentProcessor;
	
	@Autowired
	AGENumberProcessor numberProcessor;

    @Autowired
    AGEBillProcessorImpl(XmlContentProcessor xmlContentProcessor,
            NumberProcessor numberProcessor, MessageHelper messageHelper, Provider<StructureContext> structureContextProvider, AGEXmlContentProcessor AGExmlContentProcessor) {
        super(xmlContentProcessor, numberProcessor, messageHelper, structureContextProvider);
        this.messageHelper = messageHelper;
        this.AGExmlContentProcessor = AGExmlContentProcessor;
    }

    @Override 
    public byte[] insertNewElement(Bill document, String elementId, boolean before, String tagName,String numberType) {
        Validate.notNull(document, "Document is required.");
        Validate.notNull(elementId, "Element id is required.");
        final BillMetadata metadata = document.getMetadata().getOrError(() -> "Document metadata is required!");

        final String template;
        byte[] updatedContent;

        switch (tagName) {
            case CITATION:
                template = AGEXmlHelper.getCitationTemplate(messageHelper);
                updatedContent = customInsertNewElementWithTemplate(document, elementId, before, tagName, template);
                break;
            case RECITAL:
                template = AGEXmlHelper.getRecitalTemplate("#",messageHelper);
                updatedContent = customInsertNewElementWithTemplate(document, elementId, before, tagName, template);
                updatedContent = this.numberProcessor.renumberRecitals(updatedContent);
                break;
            case ARTICLE:
                template = AGEXmlHelper.getArticleTemplate("#", messageHelper.getMessage("template.heading.article"), metadata.getTemplate(), messageHelper, modifyTemplate);
                updatedContent = customInsertNewElementWithTemplate(document, elementId, before, tagName, template);
                updatedContent = this.numberProcessor.renumberArticles(updatedContent);
                break;
            case PROVISO:
                template = AGEXmlHelper.getDispositionTemplate(numberType, "#", messageHelper.getMessage("template.heading.disposition"),metadata.getTemplate(), messageHelper);
                updatedContent = customInsertNewElementWithTemplate(document, elementId, before, tagName, template);
                updatedContent = this.numberProcessor.renumberProvisos(updatedContent);
                break;
            case SIGNATURE:
            	template = AGEXmlHelper.getSignatureTemplate(messageHelper);
                updatedContent = customInsertNewElementWithTemplate(document, elementId, before, tagName, template);
                break;
            case FORMULA:
            	template = AGEXmlHelper.getFormulaTemplate(messageHelper);
                updatedContent = customInsertNewElementWithTemplate(document, elementId, before, tagName, template);
                break;
            default:
                throw new UnsupportedOperationException("Unsupported operation for tag: " + tagName);
        }

        updatedContent = xmlContentProcessor.doXMLPostProcessing(updatedContent);
        return updatedContent;
    }     

    public byte[] deleteElement(Bill document, String elementId, String tagName, User user) {
        Validate.notNull(document, "Document is required.");
        Validate.notNull(elementId, "Element id is required.");
        byte[] updatedContent;
        switch (tagName) {
            case CLAUSE:
            case SIGNATURE:
            	updatedContent = customDeleteElement(document, elementId, tagName);
                break;
            case FORMULA:
           	 	updatedContent = customDeleteElement(document, elementId, tagName);
                break;
            case CITATION:
                updatedContent = customDeleteElement(document, elementId, tagName);
                break;
            case RECITAL:
                updatedContent = customDeleteElement(document, elementId, tagName);
                updatedContent = this.numberProcessor.renumberRecitals(updatedContent);
                break;
            case ARTICLE:
                updatedContent = customDeleteElement(document, elementId, tagName);
                final BillMetadata metadata = document.getMetadata().getOrError(() -> "Document metadata is required!");
                updatedContent = this.numberProcessor.renumberArticles(updatedContent);
                break;
            case PROVISO:
                updatedContent = customDeleteElement(document, elementId, tagName);
                updatedContent = this.numberProcessor.renumberProvisos(updatedContent);
                break;
//            case QUOTEDSTRUCTURE:
//                updatedContent = customDeleteElement(document, elementId, tagName);
//                break;
            default:
                throw new UnsupportedOperationException("Unsupported operation for tag: " + tagName);
        }

        updatedContent = xmlContentProcessor.doXMLPostProcessing(updatedContent);
        return updatedContent;
    }

    private byte[] customInsertNewElementWithTemplate(Bill document, String elementId, boolean before, String tagName, String template) {
        final Content content = document.getContent().getOrError(() -> "Document content is required!");
        final byte[] contentBytes = content.getSource().getBytes();
        return xmlContentProcessor.insertElementByTagNameAndId(contentBytes, template, tagName, elementId, before);
    }
    
    private byte[] customDeleteElement(Bill document, String elementId, String tagName) {
        final Content content = document.getContent().getOrError(() -> "Document content is required!");
        final byte[] bytes = content.getSource().getBytes();
        return xmlContentProcessor.deleteElementByTagNameAndId(bytes, tagName, elementId);
    }


	public String addRef2linkReferences(String content, List<List<String>> refs) {
		List<String> matchs = refs.get(0);
		List<String> elements = refs.get(1);
		String newContent = content;
		for (int index = 0; matchs != null && index < matchs.size(); index++) {
        	String href=elements.get(index).substring(elements.get(index).indexOf("href"));
        	href=href.substring(0,href.indexOf("\"",6)+1);
			String newElement = "<mref><ref " + href + " refersTo=\"" + "extRef_" + IdGenerator.generateId(5) + "\">" + matchs.get(index) + "</ref></mref>";
        	String matchIndex = matchs.get(index);
        	matchIndex = matchIndex.replace("(", "\\(").replace(")", "\\)");        	
			String regExp = "(?!<ref>)(" + matchIndex + ")(?!<\\/ref>)";       	
			newContent = newContent.replaceAll(regExp,newElement);
        }
		return newContent;		
	}    

	public AGEExtRefUpdateOutput updatedExternalMetaRefs(byte[] content) {
		return AGExmlContentProcessor.updatedExternalMetaReferences(content);
		}
}
