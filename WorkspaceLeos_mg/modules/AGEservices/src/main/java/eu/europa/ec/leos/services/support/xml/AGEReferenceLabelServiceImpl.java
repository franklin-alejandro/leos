package eu.europa.ec.leos.services.support.xml;

import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.BOOK;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.CHAPTER;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.SECTION;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.SUBSECTION;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.THIS;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.TITLE;
import static eu.europa.ec.leos.services.support.xml.ref.TreeHelper.createTree;
import static eu.europa.ec.leos.services.support.xml.ref.TreeHelper.findCommonAncestor;
import static eu.europa.ec.leos.services.support.xml.ref.TreeHelper.getLeaves;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.ximpleware.VTDNav;

import eu.europa.ec.leos.domain.common.ErrorCode;
import eu.europa.ec.leos.domain.common.Result;
import eu.europa.ec.leos.i18n.LanguageHelper;
import eu.europa.ec.leos.services.support.xml.ReferenceLabelServiceImpl.RefValidator;
import eu.europa.ec.leos.services.support.xml.ref.AGETreeHelper;
import eu.europa.ec.leos.services.support.xml.ref.LabelHandler;
import eu.europa.ec.leos.services.support.xml.ref.Ref;
import eu.europa.ec.leos.services.support.xml.ref.TreeHelper;
import eu.europa.ec.leos.services.support.xml.ref.TreeNode;

abstract class AGEReferenceLabelServiceImpl extends ReferenceLabelServiceImpl {
	
	@Autowired
    private LanguageHelper languageHelper;       
//    @Autowired
//    private List<LabelHandler> labelHandlers;

    public String createLabel(List<TreeNode> refs, List<TreeNode> mrefCommonNodes, TreeNode sourceNode, String docType, boolean withAnchor) {
        labelHandlers.sort(Comparator.comparingInt(LabelHandler::getOrder));
        StringBuffer accumulator = new StringBuffer();
        for (LabelHandler rule : labelHandlers) {
            if(rule.canProcess(refs)) {
                rule.addPreffix(accumulator, docType);
                rule.process(refs, mrefCommonNodes, sourceNode, accumulator, languageHelper.getCurrentLocale(), withAnchor);
                break;
            }
        }
     // Evolutivo #1109
        return convertLabel(accumulator.toString());
    }
    
    @Override
    public Result<String> generateLabelStringRef(List<String> refsString, String sourceDocumentRef, byte[] sourceBytes){
        final List refs = buildRefs(refsString, sourceDocumentRef);
        return generateLabel(refs, sourceBytes);
    }
    
    @Override
    public Result<String> generateLabel(List<Ref> refs, String sourceDocumentRef, String sourceRefId, byte[] sourceBytes, byte[] targetBytes, String targetDocType, boolean withAnchor) {
        try {
            VTDNav targetVtdNav = VTDUtils.setupVTDNav(targetBytes);
            TreeNode targetTree = createTree(targetVtdNav, null, refs);
            List<TreeNode> targetNodes = getLeaves(targetTree);//in xml order

            //Validate
            boolean valid = RefValidator.validate(targetNodes, refs);
            //If invalid references, mark mref as broken and return
            if (!valid) {
                return new Result<>("", ErrorCode.DOCUMENT_REFERENCE_NOT_VALID);
            }

            VTDNav sourceVtdNav = VTDUtils.setupVTDNav(sourceBytes, true);
            TreeNode sourceTree = createTree(sourceVtdNav, null, Arrays.asList(new Ref("", sourceRefId, sourceDocumentRef)));
            TreeNode sourceNode = TreeHelper.find(sourceTree, TreeNode::getIdentifier, sourceRefId);
    
            List<TreeNode> commonNodes = findCommonAncestor(sourceVtdNav, sourceRefId, targetTree);
            //If Valid, generate, Tree is already sorted
            return new Result<>(createLabel(targetNodes, commonNodes, sourceNode, targetDocType, withAnchor), null);
        } catch (IllegalStateException e) {
            return new Result<>("", ErrorCode.DOCUMENT_REFERENCE_NOT_VALID);
        } catch (Exception e) {
            throw new RuntimeException("Unexpected error generation Labels for sourceRefId: " + sourceRefId + " and references: " + refs, e);
        }
    }
    
    @Override
    public Result<String> generateLabel(List<Ref> refs, byte[] sourceBytes){
        return generateLabel(refs, "", "", sourceBytes, sourceBytes, null, false);
    }
    
	//Evolutivo #1109-Evolutivo #1231
    private String convertLabel (String label) {
    	if (label.toLowerCase().contains(BOOK) || label.toLowerCase().contains(TITLE) || label.toLowerCase().contains(CHAPTER) || label.toLowerCase().contains(SECTION) || label.toLowerCase().contains(SUBSECTION) ) {
    		return convertHighLevel(label);
    	}else {
    		return convertArticleLevel(label);
    	}
    }
   
    private String convertArticleLevel (String label) {
    	String[] labelArray = label.split(",");
    	String pathFinal = "";
    	String[] auxArray;   	
    	for (int i=0; i < labelArray.length; i++) {
    		if (labelArray[i] != null && labelArray[i].length() >0 && !labelArray[i].equals(" ")) {
    			auxArray = labelArray[i].split(" ", 2);
    			if (auxArray[0].toLowerCase().contains(THIS)) {
    				pathFinal = String.format("%s ", messageHelper.getMessage("toc.item.type.ref.this", "e", messageHelper.getMessage("toc.item.type.ref.article")));
    			} else if (auxArray[0].contains("Article")) {
    				if (auxArray[1].contains("y ")) {
    					String[] aux2Array = auxArray[1].split("y ");
    					if (aux2Array[0].contains("<")) {
    						aux2Array[0]=cleanPathFinal(aux2Array[0]);
    					}
    					pathFinal = String.format("%s %s%s", messageHelper.getMessage("toc.item.type.ref.articles"), aux2Array[0]," y " + aux2Array[0].substring(0,aux2Array[0].indexOf("<")) + aux2Array[1].trim());
    				}else {
    					if (auxArray[1].contains("<") && labelArray.length > 1) {
    						if (auxArray[1].substring(auxArray[1].lastIndexOf("<")-1, auxArray[1].lastIndexOf("<")).equals(".")) {
    							pathFinal = String.format("%s %s", messageHelper.getMessage("toc.item.type.ref.article"), cleanPathFinal(auxArray[1]));
    						} else {
    							//TODO AQUI PONER EL PLURAL EN ARTÍCULOS
        						if (auxArray[0].contains("Articles")) {
        							pathFinal = String.format("%s %s", messageHelper.getMessage("toc.item.type.ref.articles"), auxArray[1]);
        						} else {
        							pathFinal = String.format("%s %s", messageHelper.getMessage("toc.item.type.ref.article"), auxArray[1]);
        						}
    						}
    					}else {
    						pathFinal = String.format("%s %s", messageHelper.getMessage("toc.item.type.ref.article"), auxArray[1]);
    					}
    				}
    			} else if (auxArray[0].equals("paragraph")) {
    				String aux = auxArray[1].replaceAll("[(]", "");
    				aux = aux.replaceAll("[)]", ".");
    				pathFinal += String.format("%s", aux);
    			} else if (auxArray[0].contains("point")) {
    				String aux = auxArray[1].replaceAll("[(]", "");
    				int position;
    				StringBuilder sB = new StringBuilder(aux);
    				position = aux.indexOf(")");
    				sB.insert(position + 1, '.');
    				if (aux.contains("ª")) {
    					position = aux.lastIndexOf("ª");
    					sB.setCharAt(position + 2, '.');
    				}
    				if (sB.toString().contains("y ")) {
    					String[] aux2Array = sB.toString().split("y ");
    					if (aux2Array[0].contains("<")) {
    						aux2Array[0]=cleanPathFinal(aux2Array[0]);
    					}
    					pathFinal += String.format("%s%s", aux2Array[0].trim()," y " + pathFinal.substring(pathFinal.indexOf("  ")).trim() + aux2Array[1].trim());
    				} else {
    					pathFinal += String.format("%s ", sB.toString());    

    				}							
    			} else if (auxArray[0].contains("subparagraph")) {
    				if (pathFinal.length()>=2 && pathFinal.substring(pathFinal.length()-2).contains(".")) {
    					pathFinal = pathFinal.substring(0, pathFinal.length()-1);
    					pathFinal += String.format( " %s %s", messageHelper.getMessage("toc.item.type.ref.paragraph"), auxArray[1]);
    				} else {
    					pathFinal += String.format( " %s %s", messageHelper.getMessage("toc.item.type.ref.paragraph"), auxArray[1]);
    				}
    			} else if (auxArray[1].contains("sub-point")) {
    				auxArray[1] = auxArray[1].replace("sub-point", "");
    				int position = pathFinal.lastIndexOf(".");
    				StringBuilder sB = new StringBuilder(pathFinal);
    				sB.setCharAt(position, ' ');
    				pathFinal = String.format("%s %s %s ",sB.toString().trim(), messageHelper.getMessage("toc.item.type.ref.paragraph"),auxArray[1].trim());
    			}  
    			else if (auxArray.length > 1 && auxArray[1].contains("indent")) {
    				auxArray = labelArray[i].substring(1).split(" ", 2);
    				String aux="";    				
    				if (pathFinal.substring(pathFinal.length()-2).contains(".")) {
    					pathFinal = pathFinal.substring(0, pathFinal.length()-2);
    					if (auxArray[1].contains("y ")) {
    						String[] aux2Array = auxArray[1].split("y ");
    						aux = pathFinal.substring(pathFinal.indexOf("  ")).trim();
        					pathFinal += String.format( ", %s %s", messageHelper.getMessage("toc.item.type.ref.paragraph") + " " + aux2Array[0] + " y ", aux + ", " + messageHelper.getMessage("toc.item.type.ref.paragraph") + " " + aux2Array[1]);
    					} else {
    						pathFinal += String.format( " %s %s", messageHelper.getMessage("toc.item.type.ref.paragraph"), auxArray[1]);
    					}    				
    				}else {
    					if (auxArray[1].contains("y ")) {
    						String[] aux2Array = auxArray[1].split("y ");
    						aux = pathFinal.substring(pathFinal.indexOf("  ")).trim();
    					pathFinal += String.format( "%s %s", messageHelper.getMessage("toc.item.type.ref.paragraph"), auxArray[1]);
    					}
    				}
    			} else if (labelArray[i].contains("y ") && (i == labelArray.length - 1 || labelArray[labelArray.length - 1].equals(" "))) {
    				auxArray = label.split(",");
    				for (int x=0; x <= auxArray.length-1; x++) {
    					if (auxArray[x] != null && auxArray.length > 0 && !auxArray[x].equals(" ")) {
    						if (!auxArray[x].contains("Article") && !auxArray[x].toLowerCase().contains("paragraph") && !auxArray[x].toLowerCase().contains("point") && !auxArray[x].toLowerCase().contains("subparagraph") && !auxArray[x].toLowerCase().contains("sub-point") && !auxArray[x].toLowerCase().contains("indent") ){
    							if (!pathFinal.contains("est")) {
    								pathFinal = cleanPathFinal(pathFinal);
    								String aux = pathFinal.substring(pathFinal.indexOf(" "),pathFinal.indexOf("<")).trim();
    								if (aux.contains("párrafo")) {
										aux = aux + " ";
									}
    								if (auxArray[x].contains("y ")) {
    									String[] aux2Array = auxArray[x].split("y ");
    									pathFinal += String.format(", %s %s", aux + cleanPathFinal(aux2Array[0].replace("(", "")),"y " + aux + aux2Array[1].replace("(", "").trim());
    								} else {
    									pathFinal +=  String.format(", %s",  aux + cleanPathFinal(auxArray[x].replace("(", "")));
    								}
    							}
    						} 
    					}
    				}
    			} 
    		}
    	}
    	if (pathFinal.contains("<")) {
    		if (pathFinal.substring(pathFinal.lastIndexOf("<")-1, pathFinal.lastIndexOf("<")).equals(".")) {
    			pathFinal = cleanPathFinal(pathFinal);
    		}
    	}   	
    	return pathFinal;
    }
    
    private String cleanPathFinal(String path) {
    	if (path.substring(path.lastIndexOf("<")-1, path.lastIndexOf("<")).equals(".")) {
	    	int position = path.lastIndexOf("<")-1;
			StringBuilder sB = new StringBuilder(path);
			sB.deleteCharAt(position);
			path = String.format("%s ", sB.toString());	
    	}
    	return path.trim();
    }
    
    private String convertHighLevel (String label) {
    	String[] labelArray = label.split(",");
    	String pathFinal = "";
    	String[] auxArray;
    	List<String> listRefAux = new ArrayList<>();
    	for (int i= labelArray.length-1; i>=0; i--) {
    		if (labelArray[i] != null && labelArray[i].length() >0 && !labelArray[i].equals(" ")) {
    			auxArray = labelArray[i].trim().split(" ", 2);
    			if (auxArray[0].toLowerCase().contains(THIS)) {
    				String aux= "";
    				switch (auxArray[1]) {
    				case BOOK:
    					if(labelArray.length>1) {
    						pathFinal += String.format("%s ","de " + messageHelper.getMessage("toc.item.type.ref.this", "e", messageHelper.getMessage("toc.item.type.ref.book")));
    					} else {
    						pathFinal += String.format("%s ", messageHelper.getMessage("toc.item.type.ref.this", "e", messageHelper.getMessage("toc.item.type.ref.book")));
    					}	
    					break;
    				case TITLE:
    					if (i == 0 && labelArray.length > 1) {
    						pathFinal += String.format("%s ","de " + messageHelper.getMessage("toc.item.type.ref.this", "e", messageHelper.getMessage("toc.item.type.ref.title")));
    					} else {
    						pathFinal += String.format("%s ", messageHelper.getMessage("toc.item.type.ref.this", "e", messageHelper.getMessage("toc.item.type.ref.title")));
    					}
    					break;
    				case CHAPTER:
    					if (i == 0 && labelArray.length > 1) {
    						pathFinal += String.format("%s ","de " + messageHelper.getMessage("toc.item.type.ref.this", "e", messageHelper.getMessage("toc.item.type.ref.chapter")));
    					}else {
    						pathFinal += String.format("%s ", messageHelper.getMessage("toc.item.type.ref.this", "e", messageHelper.getMessage("toc.item.type.ref.chapter")));	
    					}
    					break;
    				case SECTION:
    					if (i == 0 && labelArray.length > 1) {
    						pathFinal += String.format("%s ", "de " + messageHelper.getMessage("toc.item.type.ref.this", "a", messageHelper.getMessage("toc.item.type.ref.section")));
    					} else {
    						pathFinal += String.format("%s ", messageHelper.getMessage("toc.item.type.ref.this", "a", messageHelper.getMessage("toc.item.type.ref.section")));
    					}
    					break;
    				case SUBSECTION:
    					pathFinal += String.format("%s ", messageHelper.getMessage("toc.item.type.ref.this", "a", messageHelper.getMessage("toc.item.type.ref.subsection")));
    					break;
    				}
    			}  else if (auxArray[0].contains(SUBSECTION)) {
    				String aux = auxArray[1].replaceAll("[(]", "");
    				aux = aux.replaceAll("[)]", "");
    				if (aux.contains("ª")) {
    					aux =aux.replace("ª", ".ª");
    				}		
    				if (labelArray.length-1 == i) {
    					if (auxArray[1].contains("y ")) {
    						pathFinal += String.format("%s %s", messageHelper.getMessage("toc.item.type.ref.subsections"), aux + " ");
    					} else {
    						pathFinal += String.format("%s %s", messageHelper.getMessage("toc.item.type.ref.subsection"), aux + " ");
    					}
    				} else {
    					pathFinal += String.format("%s %s","de la " + messageHelper.getMessage("toc.item.type.ref.subsection"), aux + " ");
    				}
    			}  else if (auxArray[0].toLowerCase().contains(SECTION)) {
    				if (auxArray[1].contains("ª")) {
    					auxArray[1] = auxArray[1].replace("ª", ".ª");
    				}		
    				if (labelArray.length-1 == i) {
    					if (auxArray[1].contains("y ")) {
        					pathFinal += String.format("%s %s", messageHelper.getMessage("toc.item.type.ref.sections"), auxArray[1] + " ");
    					} else {
        					pathFinal += String.format("%s %s", messageHelper.getMessage("toc.item.type.ref.section"), auxArray[1] + " ");
    					}
    				} else {
    					pathFinal += String.format("%s %s","de la " + messageHelper.getMessage("toc.item.type.ref.section"), auxArray[1] + " ");
    				}
    			}  else if (auxArray[0].toLowerCase().contains(CHAPTER)) {
    				if (labelArray.length-1 == i) {
    					if (auxArray[1].contains("y ")) {
    						pathFinal += String.format("%s %s", messageHelper.getMessage("toc.item.type.ref.chapters"), auxArray[1] + " ");
    					} else {
    						pathFinal += String.format("%s %s", messageHelper.getMessage("toc.item.type.ref.chapter"), auxArray[1] + " ");
    					}
    				} else {
    					pathFinal += String.format("%s %s", "del " + messageHelper.getMessage("toc.item.type.ref.chapter"), auxArray[1] + " ");
    				}
    			}  else if (auxArray[0].toLowerCase().contains(TITLE)) {	
    				if (labelArray.length-1 == i) {
    					if (auxArray[1].contains("y ")) {
    						pathFinal += String.format("%s %s",  messageHelper.getMessage("toc.item.type.ref.titles"), auxArray[1] + " ");
    					} else {
    						pathFinal += String.format("%s %s",  messageHelper.getMessage("toc.item.type.ref.title"), auxArray[1] + " ");
    					}
    				} else {
    					pathFinal += String.format("%s %s", "del " + messageHelper.getMessage("toc.item.type.ref.title"), auxArray[1] + " ");
    				}
    			}  else if (auxArray[0].toLowerCase().contains(BOOK)) {
    				String aux = auxArray[1].replaceAll("[(]", "");
    				aux = aux.replaceAll("[)]", "");
    				if (labelArray.length-1 == i) {
    					if (auxArray[1].contains("y ")) {
    						pathFinal += String.format("%s %s", messageHelper.getMessage("toc.item.type.ref.books"), aux + " ");
    					} else {
    						pathFinal += String.format("%s %s", messageHelper.getMessage("toc.item.type.ref.book"), aux + " ");
    					}
    				} else {
    					pathFinal += String.format("%s %s", "del " + messageHelper.getMessage("toc.item.type.ref.book"), aux + " ");
    				}
    			} else if (labelArray[i].contains("y ") && i == labelArray.length - 1) {
    				auxArray = label.split(",");
    				int cont = 0;
    				for (int x=auxArray.length-1; x >= 0; x--) {
    					if (auxArray[x] != null && auxArray.length > 0 && !auxArray[x].equals(" ")) {
    						if (!auxArray[x].toLowerCase().contains(BOOK) && !auxArray[x].toLowerCase().contains(TITLE) && !auxArray[x].toLowerCase().contains(CHAPTER) && !auxArray[x].toLowerCase().contains(SECTION) && !auxArray[x].toLowerCase().contains(SUBSECTION) ){
    							listRefAux.add(0, ", " + auxArray[x].trim() + " ");
    							cont ++;
    						} else {
    								listRefAux.add(0, convertHighLevel(auxArray[x]).trim());
    							break;
    						}
    					}
    				}
    				for(String aux:listRefAux) {
    					pathFinal += aux;
    				}
    				i -= cont;
    			}
    		}
    	}	
    	return pathFinal;
    }

}
