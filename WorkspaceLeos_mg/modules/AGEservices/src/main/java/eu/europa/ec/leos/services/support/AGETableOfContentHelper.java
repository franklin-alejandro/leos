package eu.europa.ec.leos.services.support;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.action.SoftActionType;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;
import eu.europa.ec.leos.vo.toc.TocItem;
import eu.europa.ec.leos.vo.toc.TocItemUtils;

import static eu.europa.ec.leos.services.support.xml.XmlHelper.PARAGRAPH;

public class AGETableOfContentHelper extends TableOfContentHelper {
	
	public static final String RULE = "rule";
	public static final List<String> ELEMENTS_WITHOUT_CONTENT = Collections.unmodifiableList(Arrays.asList("article", "subsection", "section", "chapter", "title", "book", "part"));
	
	/* protected */
	private static final String MOVE_LABEL_SPAN_START_TAG = "<span class=\"leos-soft-move-label\">";
    private static final String MOVED_TITLE_SPAN_START_TAG = "<span class=\"leos-soft-move-title\">";
    private static final String SPAN_END_TAG = "</span>";
    private static final String SPACE = " ";
    private static final int MOVED_LABEL_SIZE = MOVED_TITLE_SPAN_START_TAG.length() + SPACE.length() + MOVE_LABEL_SPAN_START_TAG.length() + 2 * SPAN_END_TAG.length();
    private static final List<String> ELEMENTS_TO_REMOVE_FROM_CONTENT = Arrays.asList("inline", "authorialNote");
   
	public static String buildItemCaption(TableOfContentItemVO tocItem, int captionMaxSize, MessageHelper messageHelper) {
        Validate.notNull(tocItem.getTocItem(), "Type should not be null");

        boolean shoudlAddMovedLabel = shouldAddMoveLabel(tocItem);

        StringBuilder itemDescription = tocItem.getTocItem().isItemDescription()
                ? new StringBuilder(getDisplayableTocItem(tocItem.getTocItem(), messageHelper)).append(SPACE)
                : new StringBuilder();

        if (shoudlAddMovedLabel) {
            itemDescription.insert(0, MOVED_TITLE_SPAN_START_TAG + SPACE);
        }

        if (!StringUtils.isEmpty(tocItem.getNumber()) && !StringUtils.isEmpty(tocItem.getHeading())) {
            itemDescription.append(tocItem.getNumber());
            if (shoudlAddMovedLabel) {
                itemDescription.append(SPAN_END_TAG).append(getMovedLabel(messageHelper));
            }
            //cuarentena
            itemDescription.append(tocItem.getTocItem().getItemHeading()).append(tocItem.getHeading());
//            itemDescription.append(tocItem.getHeading());
        } else if (!StringUtils.isEmpty(tocItem.getNumber())) {
            SoftActionType softAction = tocItem.getNumSoftActionAttr();

            if (PARAGRAPH.equals(tocItem.getTocItem().getAknTag().name()) && (softAction != null
                    && SoftActionType.DELETE.equals(softAction)) && !SoftActionType.MOVE_TO.equals(tocItem.getSoftActionAttr())) {
                itemDescription.append("<span class=\"leos-soft-num-removed\">" + tocItem.getNumber() + "</span>");
            } else if (PARAGRAPH.equals(tocItem.getTocItem().getAknTag().name()) && softAction != null
                    && SoftActionType.ADD.equals(softAction) && !SoftActionType.MOVE_TO.equals(tocItem.getSoftActionAttr())) {
                itemDescription.append("<span class=\"leos-soft-num-new\">" + tocItem.getNumber() + "</span>");
            } else {
                itemDescription.append(tocItem.getNumber());
                if (shoudlAddMovedLabel) {
                    itemDescription.append(SPAN_END_TAG).append(getMovedLabel(messageHelper));
                }
            }
        } else if (!StringUtils.isEmpty(tocItem.getHeading())) {
            itemDescription.append(tocItem.getHeading());
            if (shoudlAddMovedLabel) {
                itemDescription.append(SPAN_END_TAG).append(getMovedLabel(messageHelper));
            }
        } else if (shoudlAddMovedLabel) {
            itemDescription.append(SPAN_END_TAG).append(getMovedLabel(messageHelper));
        }        

        if (tocItem.getTocItem().isContentDisplayed()) {
            itemDescription.append(itemDescription.length() > 0 ? TocItemUtils.CONTENT_SEPARATOR : "").append(removeTag(tocItem.getContent()));
        }
        
        return StringUtils.abbreviate(itemDescription.toString(), shoudlAddMovedLabel ? captionMaxSize + MOVED_LABEL_SIZE : captionMaxSize);
    }
	
    public static String getDisplayableTocItem(TocItem tocItem, MessageHelper messageHelper) {
        return messageHelper.getMessage("toc.item.type." + tocItem.getAknTag().value().toLowerCase());
    }

}
