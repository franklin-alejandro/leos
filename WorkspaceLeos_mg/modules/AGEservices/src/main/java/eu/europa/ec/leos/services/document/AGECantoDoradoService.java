/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.services.document;


import java.util.List;

import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.AGECantoDorado;
import eu.europa.ec.leos.domain.cmis.metadata.AGECantoDoradoMetadata;
import eu.europa.ec.leos.domain.common.TocMode;
import eu.europa.ec.leos.model.action.VersionVO;
import eu.europa.ec.leos.model.cantodorado.AGECantoDoradoStructureType;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;

public interface AGECantoDoradoService {

	AGECantoDorado createCantoDorado(String templateId, String path, AGECantoDoradoMetadata metadata, String actionMessage, byte[] content);

	AGECantoDorado createCantoDoradoFromContent(String path, AGECantoDoradoMetadata metadata, String actionMessage, byte[] content);

    void deleteCantoDorado(AGECantoDorado cantoDorado);

    AGECantoDorado updateCantoDorado(AGECantoDorado cantoDorado, AGECantoDoradoMetadata metadata, VersionType version, String comment);

    AGECantoDorado updateCantoDorado(AGECantoDorado cantoDorado, byte[] updatedCantoDoradoContent, boolean major, String comment);

    AGECantoDorado updateCantoDorado(String cantoDoradoId, AGECantoDoradoMetadata metadata);

    AGECantoDorado updateCantoDoradoWithMilestoneComments(AGECantoDorado cantoDorado, List<String> milestoneComments, VersionType versionType, String comment);

    AGECantoDorado updateCantoDoradotWithMilestoneComments(String cantoDoradoId, List<String> milestoneComments);

    AGECantoDorado findCantoDorado(String id);
    
    List<VersionVO> getAllVersions(String id, String documentId);
    
    AGECantoDorado findCantoDoradoByRef(String ref);

    AGECantoDorado findCantoDoradoVersion(String id);

    List<AGECantoDorado> findVersions(String id);

    AGECantoDorado findCantoDoradoByPackagePath(String path);

	List<TableOfContentItemVO> getTableOfContent(AGECantoDorado cantoDorado, TocMode mode);

	List<AGECantoDorado> findAllMajors(String docRef, int startIndex, int maxResults);

	int findAllMinorsCountForIntermediate(String docRef, String currIntVersion);

	List<AGECantoDorado> findAllMinorsForIntermediate(String docRef, String currIntVersion, int startIndex, int maxResults);

	Integer findAllMajorsCount(String docRef);

	Integer findRecentMinorVersionsCount(String documentId, String documentRef);

	List<AGECantoDorado> findRecentMinorVersions(String documentId, String documentRef, int startIndex, int maxResults);

	AGECantoDorado updateCantoDorado(AGECantoDorado cantoDorado, byte[] updatedCantoDoradoContent, VersionType versionType, String comment);

	AGECantoDorado createVersion(String id, VersionType versionType, String comment);

	AGECantoDorado saveTableOfContent(AGECantoDorado cantoDorado, List<TableOfContentItemVO> tocList,
			AGECantoDoradoStructureType structureType, String actionMsg, User user);

	List<String> getAncestorsIdsForElementId(AGECantoDorado cantoDorado, List<String> elementIds);

	AGECantoDorado updateCantoDoradotWithMetadata(AGECantoDorado cantoDorado, byte[] updatedCantoDoradoContent, AGECantoDoradoMetadata metadata,
			VersionType versionType, String comment);
	
	byte[] updateCantoDoradoFromBill(byte[] billContent, byte[] cantoDoradoContent, String number, String year, String month, String day,
			String organism, boolean newCantoDorado);
	
}
