package eu.europa.ec.leos.services.document;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.google.common.base.Stopwatch;

import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.cmis.document.Proposal;
import eu.europa.ec.leos.repository.document.ProposalRepository;
import eu.europa.ec.leos.repository.store.PackageRepository;
import eu.europa.ec.leos.services.support.xml.XmlContentProcessor;
import eu.europa.ec.leos.services.support.xml.XmlNodeConfigHelper;
import eu.europa.ec.leos.services.support.xml.XmlNodeProcessor;

@Primary
@Service
public class AGEProposalServiceImpl extends ProposalServiceImpl implements AGEProposalService {
	
	private static final Logger LOG = LoggerFactory.getLogger(ProposalServiceImpl.class);
	
	private final ProposalRepository proposalRepository;
    private final XmlContentProcessor xmlContentProcessor;
	
	AGEProposalServiceImpl(ProposalRepository proposalRepository, XmlNodeProcessor xmlNodeProcessor,
			XmlContentProcessor xmlContentProcessor, XmlNodeConfigHelper xmlNodeConfigHelper,
			PackageRepository packageRepository) {
		super(proposalRepository, xmlNodeProcessor, xmlContentProcessor, xmlNodeConfigHelper, packageRepository);
		this.proposalRepository = proposalRepository;
        this.xmlContentProcessor = xmlContentProcessor;
	}

	@Override
	public Proposal updateComponentRef(Proposal proposal, Map<String, String> hrefs, LeosCategory leosCategory) {
		LOG.trace("Updating components in Proposal ... [id={}", proposal.getId());
        Stopwatch stopwatch = Stopwatch.createStarted();

        byte[] xmlBytes = proposal.getContent().get().getSource().getBytes();        
        byte[] updatedBytes =  xmlContentProcessor.updateReferedAttributes(xmlBytes, hrefs);

        //save updated xml
        proposal = proposalRepository.updateProposal(proposal.getId(), updatedBytes);
        LOG.trace("Update components in Proposal ...({} milliseconds)", stopwatch.elapsed(TimeUnit.MILLISECONDS));
        return proposal;
	}

}
