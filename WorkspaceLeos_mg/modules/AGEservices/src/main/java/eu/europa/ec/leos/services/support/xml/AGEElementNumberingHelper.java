package eu.europa.ec.leos.services.support.xml;

import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.BOOK;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.CHAPTER;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.PROVISO;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.SECTION;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.SUBSECTION;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.TITLE;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_AFFECTED_ATTR;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_ORIGIN_ATTR;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.LEOS_SOFT_ACTION_ATTR;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.TOGGLED_TO_NUM;
import static eu.europa.ec.leos.services.support.xml.AGEVTDUtils.LEOS_MOD_ATTR;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.buildNumElement;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.setupVTDNav;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.toByteArray;
import static eu.europa.ec.leos.services.support.xml.VTDUtils.getStartTag;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.ARTICLE;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.BODY;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.LIST;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.NUM;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.PARAGRAPH;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.POINT;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.RECITAL;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.SUBPOINT;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.isExcludedNode;
import static java.lang.String.join;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.nCopies;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import javax.inject.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.ibm.icu.text.RuleBasedNumberFormat;
// LEOS-AGE
import com.ximpleware.AutoPilot;
import com.ximpleware.EOFException;
import com.ximpleware.EncodingException;
import com.ximpleware.EntityException;
import com.ximpleware.ModifyException;
// LEOS-AGE
import com.ximpleware.NavException;
import com.ximpleware.ParseException;
import com.ximpleware.PilotException;
import com.ximpleware.TranscodeException;
import com.ximpleware.VTDNav;
import com.ximpleware.XMLModifier;

import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.action.SoftActionType;
import eu.europa.ec.leos.services.toc.StructureContext;
import eu.europa.ec.leos.vo.toc.NumberingConfig;
import eu.europa.ec.leos.vo.toc.NumberingType;
import eu.europa.ec.leos.vo.toc.TocItem;
import eu.europa.ec.leos.vo.toctype.numbertype.AGENumberTypeProviso;
import static eu.europa.ec.leos.vo.toc.TocItemUtils.getNumberingByName;
import static eu.europa.ec.leos.vo.toc.TocItemUtils.getNumberingConfig;
import static eu.europa.ec.leos.vo.toc.TocItemUtils.getNumberingTypeByDepth;
import static eu.europa.ec.leos.vo.toc.TocItemUtils.getTocItemByName;

@Primary
@Component
public class AGEElementNumberingHelper extends ElementNumberingHelper{
	
	private static final String LEOS_ORIGIN_ATTR_EC = "ec";
    static final byte[] NUM_BYTES = NUM.getBytes(UTF_8);
    static final byte[] NUM_START_TAG = "<".concat(NUM).concat(">").getBytes(UTF_8);
	private static final Logger LOG = LoggerFactory.getLogger(AGEElementNumberingHelper.class);
	
    private List<TocItem> tocItems;
    private List<NumberingConfig> numberingConfigs;

	private boolean isDefaultEditable = false;
	private boolean isNameSpaceEnabled = true;
	private int numberTitle = 0;

	private TreeMap<Integer, String> romanEquivalent;
	
    private MessageHelper messageHelper;
    private Provider<StructureContext> structureContextProvider;
	
    @Autowired
    public AGEElementNumberingHelper(MessageHelper messageHelper, Provider<StructureContext> structureContextProvider) {
    	super(messageHelper, structureContextProvider);
        this.messageHelper = messageHelper;
        this.structureContextProvider = structureContextProvider;
        initRomanEquivalentMap();
    }
	

	byte[] renumberElements(String element, String elementNumber, Object data)
			throws NavException, ModifyException, TranscodeException, IOException {

		VTDNav vtdNav = createVTDNav(data, element);
		XMLModifier xmlModifier = null;
		try {
			xmlModifier = new XMLModifier(vtdNav);
		} catch (ModifyException e) {
			throw new RuntimeException("Unable to perform the renumber " + element + " operation", e);
		}
		return renumberElements(element, vtdNav, xmlModifier, elementNumber);

	}
	
	private byte[] renumberElements(String element, VTDNav vtdNav, XMLModifier xmlModifier, String elementNumber)
			throws NavException, ModifyException, TranscodeException, IOException {

		AutoPilot autoPilot = new AutoPilot(vtdNav);
		autoPilot.selectElement(element);
		char alphaNumber = 'a';
		List<Integer> indexList = new ArrayList<Integer>();
		boolean foundProposalElement = false;
		int parentIndex = getParentIndex(vtdNav, element);

		if (!isProposalElement(vtdNav) && (element == PARAGRAPH || element == POINT)) {
			while (autoPilot.iterate()) {
				switch (element) {
				case PARAGRAPH:
					updatePointNumbersDefault(vtdNav, xmlModifier);
					if (hasAffectedAttribute(vtdNav, xmlModifier)) {
						if (vtdNav.toElement(VTDNav.FIRST_CHILD, LIST)) {
							renumberElements(POINT, vtdNav, xmlModifier, "");
						}
					}
					break;
				case POINT:
					updatePointNumbersDefault(vtdNav, xmlModifier);
					if (hasAffectedAttribute(vtdNav, xmlModifier)) {
						if (vtdNav.toElement(VTDNav.FIRST_CHILD, LIST)) {
							renumberElements(POINT, vtdNav, xmlModifier, "");
						} 
//						else if (vtdNav.toElement(VTDNav.FIRST_CHILD, SUBPOINT)) {
//							renumberElements(SUBPOINT, vtdNav, xmlModifier, "");
//						}
					}
					break;
				}
			}
		} else {
			while (autoPilot.iterate()) {
				int tempIndex = getParentIndex(vtdNav, element);
				if (tempIndex != parentIndex && (element == PARAGRAPH || element == POINT)) {
					continue;
				}

				if (foundProposalElement || isElementOriginEC(vtdNav)) {
					if (isElementOriginEC(vtdNav)) {
						foundProposalElement = true;
						alphaNumber = 'a';
						// set the series number for manual numbering of Articles
						int index = vtdNav.getCurrentIndex();
						if (vtdNav.toElement(VTDNav.FIRST_CHILD, NUM)) {
							long contentFragment = vtdNav.getContentFragment();
							elementNumber = new String(
									vtdNav.getXML().getBytes((int) contentFragment, (int) (contentFragment >> 32)));
						}
						vtdNav.recoverNode(index);
						// Check if elemetns added to proposal elements
						if (hasAffectedAttribute(vtdNav, xmlModifier) || hasNumToggledAttribute(vtdNav, xmlModifier)) {
							switch (element) {
							case ARTICLE:
								renumberElements(PARAGRAPH, vtdNav, xmlModifier, "");
								break;
							case PROVISO:
								renumberElements(PARAGRAPH, vtdNav, xmlModifier, "");
								break;
							case PARAGRAPH:
								if (vtdNav.toElement(VTDNav.FIRST_CHILD, LIST)) {
									updateMandatePointsNumbering(vtdNav, xmlModifier);
									renumberElements(POINT, vtdNav, xmlModifier, "");
								}
								break;
							case POINT:
								if (vtdNav.toElement(VTDNav.FIRST_CHILD, LIST)) {
									updateMandatePointsNumbering(vtdNav, xmlModifier);
									renumberElements(POINT, vtdNav, xmlModifier, "");
								}
								break;
							}
						}
					} else { // adding alpha number series to added article
						switch (element) {
						case ARTICLE:
							alphaNumber = updateArticleNumbers(elementNumber, vtdNav, xmlModifier, alphaNumber, false);
							if (hasAffectedAttribute(vtdNav, xmlModifier)
									|| hasNumToggledAttribute(vtdNav, xmlModifier)) {
								AGEXmlContentProcessor xmlContentProcessor = null;
								AutoPilot ap = new AutoPilot(vtdNav);
								Map<String, String> attrs = getElementAttributes(vtdNav, ap);
								String attrVal = attrs.get(LEOS_MOD_ATTR);
								if (attrVal.equals("true")) {
									updateParagraphNumbersMod(vtdNav, xmlModifier);
								}
								else {
									updateParagraphNumbersDefault(vtdNav, xmlModifier);
								}
								renumberElements(PARAGRAPH, vtdNav, xmlModifier, "");
							}
							break;
						case PROVISO:
							// alphaNumber = updateProvisoNumbers(elementNumber, vtdNav, xmlModifier, alphaNumber, false);
							if (hasAffectedAttribute(vtdNav, xmlModifier)
									|| hasNumToggledAttribute(vtdNav, xmlModifier)) {
								updateParagraphNumbersDefault(vtdNav, xmlModifier);
								renumberElements(PARAGRAPH, vtdNav, xmlModifier, "");
							}
							break;
						case PARAGRAPH:
							alphaNumber = updateParagraphNumbers(elementNumber, vtdNav, xmlModifier, alphaNumber,
									false);
							if (hasAffectedAttribute(vtdNav, xmlModifier)) {
								if (vtdNav.toElement(VTDNav.FIRST_CHILD, LIST)) {
									updateMandatePointsNumbering(vtdNav, xmlModifier);
									renumberElements(POINT, vtdNav, xmlModifier, "");
								}
							}
							break;
						case POINT:
							alphaNumber = updatePointNumbers("", elementNumber, vtdNav, xmlModifier, alphaNumber,
									false);
							if (hasAffectedAttribute(vtdNav, xmlModifier)) {
								if (vtdNav.toElement(VTDNav.FIRST_CHILD, LIST)) {
									updateMandatePointsNumbering(vtdNav, xmlModifier);
									renumberElements(POINT, vtdNav, xmlModifier, "");
								}
							}
							break;
						case RECITAL:
							alphaNumber = updateRecitalNumbers(elementNumber, vtdNav, xmlModifier, alphaNumber, false);
							break;
						case SUBPOINT:
							alphaNumber = updatePointNumbers("", elementNumber, vtdNav, xmlModifier, alphaNumber,
									false);
							if (hasAffectedAttribute(vtdNav, xmlModifier)) {
								if (vtdNav.toElement(VTDNav.FIRST_CHILD, SUBPOINT)) {
									updateMandatePointsNumbering(vtdNav, xmlModifier);
									renumberElements(SUBPOINT, vtdNav, xmlModifier, "");
								}
							}
							break;
						}
					}
				} else {
					indexList.add(vtdNav.getCurrentIndex());
				}
			}
			// -ve numbering
			long negativeNumber = -(indexList.size());
			// For Points -ve numbering get the type of sequence
			String numSeq = getNegativeSidePointsNumSequence(element, vtdNav, indexList);

			for (int negativeNumIndex : indexList) {
				vtdNav.recoverNode(negativeNumIndex);
				switch (element) {
				case ARTICLE:
					updateArticleNumbers("Article " + negativeNumber++, vtdNav, xmlModifier, Character.MIN_VALUE, true);
					if (hasAffectedAttribute(vtdNav, xmlModifier) || hasNumToggledAttribute(vtdNav, xmlModifier)) {
						AGEXmlContentProcessor xmlContentProcessor = null;
						AutoPilot ap = new AutoPilot(vtdNav);
						Map<String, String> attrs = getElementAttributes(vtdNav, ap);
						String attrVal = attrs.get(LEOS_MOD_ATTR);
						if (attrVal == null) {
							attrVal="false";
						}
						if (attrVal.equals("true")) {
							updateParagraphNumbersMod(vtdNav, xmlModifier);
						}
						else {
							updateParagraphNumbersDefault(vtdNav, xmlModifier);
						}						
						renumberElements(PARAGRAPH, vtdNav, xmlModifier, "");
					}
					break;	
				case PROVISO:
					// updateProvisoNumbers("Proviso " + negativeNumber++, vtdNav, xmlModifier, Character.MIN_VALUE, true);
					if (hasAffectedAttribute(vtdNav, xmlModifier) || hasNumToggledAttribute(vtdNav, xmlModifier)) {
						updateParagraphNumbersDefault(vtdNav, xmlModifier);
						renumberElements(PARAGRAPH, vtdNav, xmlModifier, "");
					}
					break;
				case PARAGRAPH:
					updateParagraphNumbers("" + negativeNumber++, vtdNav, xmlModifier, Character.MIN_VALUE, true);
					pointNumProcessing(vtdNav, xmlModifier);
					break;
				case POINT:
					updatePointNumbers(numSeq, "" + negativeNumber++, vtdNav, xmlModifier, alphaNumber, true);
					pointNumProcessing(vtdNav, xmlModifier);
					break;
				case RECITAL:
					updateRecitalNumbers("" + negativeNumber++, vtdNav, xmlModifier, Character.MIN_VALUE, true);
					break;
				}
			}
		}
		return toByteArray(xmlModifier);
	}

	private void pointNumProcessing(VTDNav vtdNav, XMLModifier xmlModifier)
			throws NavException, ModifyException, TranscodeException, IOException {
		if (hasAffectedAttribute(vtdNav, xmlModifier)) {
			if (vtdNav.toElement(VTDNav.FIRST_CHILD, LIST)) {
				int currentIndex = vtdNav.getCurrentIndex();
				renumberPoints(vtdNav, xmlModifier);
				vtdNav.recoverNode(currentIndex);
				renumberElements(POINT, vtdNav, xmlModifier, "");
			}
		}
	}

	private void updateMandatePointsNumbering(VTDNav vtdNav, XMLModifier xmlModifier)
			throws NavException, ModifyException {
		if (!isProposalElement(vtdNav)) {
			int currentIndex = vtdNav.getCurrentIndex();
			renumberPoints(vtdNav, xmlModifier);
			vtdNav.recoverNode(currentIndex);
		}
	}

	private boolean isProposalElement(VTDNav vtdNav) throws NavException {
		boolean isProposalElement = false;
		int attributeIndex = vtdNav.getAttrVal(LEOS_ORIGIN_ATTR);
		if (attributeIndex != -1 && vtdNav.toNormalizedString(attributeIndex).equals(LEOS_ORIGIN_ATTR_EC)) {
			isProposalElement = true;
		}
		return isProposalElement;
	}

	private String getNegativeSidePointsNumSequence(String element, VTDNav vtdNav, List<Integer> indexList)
			throws NavException {
		String seqType = null;
		if (element.equals(POINT) && indexList.size() > 0) {
			vtdNav.recoverNode(indexList.get(indexList.size() - 1));
			if (vtdNav.toElement(VTDNav.NEXT_SIBLING)) {
				if (vtdNav.toElement(VTDNav.FIRST_CHILD, NUM)) {
					seqType = vtdNav.toString((int) vtdNav.getContentFragment(),
							(int) (vtdNav.getContentFragment() >> 32));
				}
			}
		}
		return seqType;
	}

	private byte[] updatePointNumbersDefault(VTDNav vtdNav, XMLModifier xmlModifier)
			throws NavException, ModifyException, IOException, TranscodeException {
		LOG.info("points default numbering");
		int currentIndex = vtdNav.getCurrentIndex();
		if (vtdNav.toElement(VTDNav.FIRST_CHILD, LIST)) {
			renumberPoints(vtdNav, xmlModifier);
			vtdNav.recoverNode(currentIndex);
		}
		return toByteArray(xmlModifier);
	}

	private void renumberPoints(VTDNav vtdNav, XMLModifier xmlModifier)
			throws NavException, PilotException, ModifyException {
		long number = 1L;
		int parentIndex = getParentIndex(vtdNav, POINT);
		String seqType = getPointNumSequence(vtdNav);
		if (vtdNav.toElement(VTDNav.FIRST_CHILD, POINT)) {
			do {
				int tempIndex = getParentIndex(vtdNav, POINT);
				if (tempIndex != parentIndex) {
					break;
				}

				// check if it has num element
				String num = getPointNumber((int) number++, seqType);
				byte[] elementeNum = seqType == "-" ? num.getBytes(UTF_8) : (num + ")").getBytes(UTF_8);
				int index = vtdNav.getCurrentIndex();
				elementeNum = buildNumElement(vtdNav, xmlModifier, elementeNum);
				vtdNav.recoverNode(index);
				xmlModifier.insertAfterHead(elementeNum);
				if (vtdNav.toElement(VTDNav.FIRST_CHILD, LIST)) {
					if (vtdNav.toElement(VTDNav.FIRST_CHILD, POINT)) {
						long numberAux = 1L;
						do {
							//							tempIndex = getParentIndex(vtdNav, POINT);
							//							if (tempIndex != parentIndex) {
							//								break;
							//							}
							String seqTypeAux = getPointNumSequence(vtdNav);
							num = getPointNumber((int) numberAux++, seqTypeAux);
							//elementeNum = seqTypeAux == "-" ? num.getBytes(UTF_8) : (num + ")").getBytes(UTF_8);
							elementeNum = num.getBytes(UTF_8);
							int indexAux = vtdNav.getCurrentIndex();
							elementeNum = buildNumElement(vtdNav, xmlModifier, elementeNum);
							vtdNav.recoverNode(indexAux);
							xmlModifier.insertAfterHead(elementeNum);
							if (vtdNav.toElement(VTDNav.FIRST_CHILD, LIST)) {
								if (vtdNav.toElement(VTDNav.FIRST_CHILD, POINT)) {
									long numberAux2 = 1L;
									do {
										String seqTypeAux2 = getPointNumSequence(vtdNav);
										num = getPointNumber((int) numberAux2++, seqTypeAux2);
										elementeNum = num.getBytes(UTF_8);
										int indexAux2 = vtdNav.getCurrentIndex();
										elementeNum = buildNumElement(vtdNav, xmlModifier, elementeNum);
										vtdNav.recoverNode(indexAux2);
										xmlModifier.insertAfterHead(elementeNum);

									} while (vtdNav.toElement(VTDNav.NEXT_SIBLING));
								}vtdNav.recoverNode(indexAux);
							}
						} while (vtdNav.toElement(VTDNav.NEXT_SIBLING));
					}vtdNav.recoverNode(index);
				}
			} while (vtdNav.toElement(VTDNav.NEXT_SIBLING));
		}
	}

	private int getParentIndex(VTDNav vtdNav, String element) throws NavException {
		int index = vtdNav.getCurrentIndex();
		AutoPilot ap = new AutoPilot(vtdNav);
		ap.selectElement(element);
		int parentIndex = 0;
		while (ap.iterate()) {
			if (vtdNav.toElement(VTDNav.PARENT)) {
				parentIndex = vtdNav.getCurrentIndex();
				break;
			}
		}
		vtdNav.recoverNode(index);
		return parentIndex;
	}

	private String getPointNumSequence(VTDNav vtdNav) throws NavException {
		int index = vtdNav.getCurrentIndex();
		int pointDepth = 1;
		String pointSequence = "#";
		boolean isNumSequencePresent = false;
		// If points are already there than need to check the num sequence of first
		// point

		if (vtdNav.toElement(VTDNav.FIRST_CHILD, POINT)) {
			if (vtdNav.toElement(VTDNav.FIRST_CHILD, NUM)) {
				String numSeq = vtdNav.toString((int) vtdNav.getContentFragment(),
						(int) (vtdNav.getContentFragment() >> 32));
				//Evolutivo #2448
				if (numSeq.equals("a)") || numSeq.equals("i)") || numSeq.equals("-") || numSeq.equals("1.º")) {
					pointSequence = numSeq;
					isNumSequencePresent = true;
				}
			}

		}
		vtdNav.recoverNode(index);
		// If there are no num sequence is present then have to check for level of
		// points
		if (!isNumSequencePresent) {
			while (true) {
				if (vtdNav.toElement(VTDNav.PARENT)) {
					if (vtdNav.matchElement(LIST)) {
						pointDepth++;
					} else if (vtdNav.matchElement(PARAGRAPH)) {
						break;
					}
				}
			}
			if (pointDepth == 1) {
				pointSequence = "a)";
			} else if (pointDepth == 3) {
				pointSequence = "1.º";
			} else if (pointDepth == 4) {
				pointSequence = "i)";
			} else {
				pointSequence = "-";
			}
		}
		vtdNav.recoverNode(index);
		return pointSequence;
	}

	private byte[] updateParagraphNumbersDefault(VTDNav vtdNav, XMLModifier xmlModifier)
			throws NavException, ModifyException, IOException, TranscodeException {
		int currentIndex = vtdNav.getCurrentIndex();
		long number = 1L;
		AutoPilot autoPilot = new AutoPilot(vtdNav);
		autoPilot.selectElement(PARAGRAPH);
		while (autoPilot.iterate()) {
			if (isNumElementExists(vtdNav)) {
				byte[] elementeNum = ((number++) + ".").getBytes(UTF_8);
				int index = vtdNav.getCurrentIndex();
				elementeNum = buildNumElement(vtdNav, xmlModifier, elementeNum);
				vtdNav.recoverNode(index);
				xmlModifier.insertAfterHead(elementeNum);
			}
		}
		vtdNav.recoverNode(currentIndex);
		return toByteArray(xmlModifier);
	}

	private byte[] updateParagraphNumbersMod(VTDNav vtdNav, XMLModifier xmlModifier)
			throws NavException, ModifyException, IOException, TranscodeException {
		int currentIndex = vtdNav.getCurrentIndex();
		int number = 1;
		byte[] elementeNum = null;
		AutoPilot autoPilot = new AutoPilot(vtdNav);
		autoPilot.selectElement(PARAGRAPH);
		Locale languageLocale = new Locale("ES");
		RuleBasedNumberFormat esFormatter = new RuleBasedNumberFormat(languageLocale, RuleBasedNumberFormat.SPELLOUT);
		while (autoPilot.iterate()) {
			if (isNumElementExists(vtdNav)) {
				switch (number) {
				case 1:
					elementeNum = "".getBytes(UTF_8);
					break;
				case 2:
					elementeNum = "Uno.".getBytes(UTF_8);
					break;
				default:
					String cardinalNum=esFormatter.format(number-1, "%spellout-cardinal-masculine") + ".";
					elementeNum = (cardinalNum.substring(0,1).toUpperCase() + cardinalNum.substring(1)).getBytes(UTF_8);
				}
				number++;
				int index = vtdNav.getCurrentIndex();
				elementeNum = buildNumElement(vtdNav, xmlModifier, elementeNum);
				vtdNav.recoverNode(index);
				xmlModifier.insertAfterHead(elementeNum);
			}
		}
		vtdNav.recoverNode(currentIndex);
		return toByteArray(xmlModifier);
	}	
	
	private char updateArticleNumbers(String elementNumber, VTDNav vtdNav, XMLModifier xmlModifier, char alphaNumber,
			boolean onNegativeSide) throws NavException, ModifyException {
		byte[] articleNum;
		if (onNegativeSide) {
			articleNum = elementNumber.getBytes(UTF_8);
		} else {
			articleNum = (elementNumber + (alphaNumber <= 'z' ? alphaNumber++ : '#')).getBytes(UTF_8);
		}
		updateNumElement(vtdNav, xmlModifier, articleNum);
		return alphaNumber;
	}

	private char updateProvisoNumbers(String elementNumber, VTDNav vtdNav, XMLModifier xmlModifier, char alphaNumber,
			boolean onNegativeSide) throws NavException, ModifyException {
		byte[] provisoNum;
		if (onNegativeSide) {
			provisoNum = elementNumber.getBytes(UTF_8);
		} else {
			provisoNum = (elementNumber + (alphaNumber <= 'z' ? alphaNumber++ : '#')).getBytes(UTF_8);
		}
		updateNumElement(vtdNav, xmlModifier, provisoNum);
		return alphaNumber;
	}	
	
	private char updateRecitalNumbers(String elementNumber, VTDNav vtdNav, XMLModifier xmlModifier, char alphaNumber,
			boolean onNegativeSide) throws NavException, ModifyException {
		byte[] recitalNum;
		if (onNegativeSide) {
			recitalNum = ("(" + elementNumber + ")").getBytes(UTF_8);
		} else {
			recitalNum = (elementNumber.replace(')', (alphaNumber <= 'z' ? alphaNumber++ : '#')) + ")").getBytes(UTF_8);
		}
		updateNumElement(vtdNav, xmlModifier, recitalNum);
		return alphaNumber;
	}

	private char updateParagraphNumbers(String elementNumber, VTDNav vtdNav, XMLModifier xmlModifier, char alphaNumber,
			boolean onNegativeSide) throws NavException, ModifyException {
		byte[] paraNum;
		if (onNegativeSide) {
			paraNum = (elementNumber + ".").getBytes(UTF_8);
		} else {
			paraNum = (elementNumber.replace('.', (alphaNumber <= 'z' ? alphaNumber++ : '#')) + ".").getBytes(UTF_8);
		}
		if (isNumElementExists(vtdNav)) {
			updateNumElement(vtdNav, xmlModifier, paraNum);
		}
		return alphaNumber;
	}

	private char updatePointNumbers(String seqType, String elementNumber, VTDNav vtdNav, XMLModifier xmlModifier,
			char alphaNumber, boolean onNegativeSide) throws NavException, ModifyException {
		String pointNum;
		if (onNegativeSide && seqType != null) {
			String num = getPointNumber(Math.abs(Integer.parseInt(elementNumber)), seqType);
			pointNum = ("(" + (num == "-" ? num : "-" + num + ")"));
		} else {
			if (elementNumber != null && elementNumber != "") {
				pointNum = (elementNumber.replace(')', (alphaNumber <= 'z' ? alphaNumber++ : '#')) + ")");
				if (elementNumber.indexOf("-") != -1) {
					pointNum = pointNum.substring(0, pointNum.length() - 1);
				}
			} else {
				pointNum = ("(" + (alphaNumber <= 'z' ? alphaNumber++ : '#') + ")");
			}
		}
		updateNumElement(vtdNav, xmlModifier, pointNum.getBytes(UTF_8));
		return alphaNumber;
	}

	public String getPointNumber(int number, String seqType) {
		String pointNum = "#";
		if (seqType != null) {
			switch (seqType) {
			case "1.º":
				pointNum = number + ".º";
				break;
			case "a)":
//				pointNum = number > 0 && number < 27 ? String.valueOf((char) (number + 96)) : "#";
				pointNum = number > 0 && number < 56 ? generateAlpha(number) : "#";
				break;
			case "-":
				pointNum = "";
				break;
			case "i)":
				pointNum = join("", nCopies(number, "i")).replace("iiiii", "v").replace("iiii", "iv").replace("vv", "x")
				.replace("viv", "ix").replace("xxxxx", "l").replace("xxxx", "xl").replace("ll", "c")
				.replace("lxl", "xc").replace("ccccc", "d").replace("cccc", "cd").replace("dd", "m")
				.replace("dcd", "cm");
				pointNum += ")";
				break;
			}
		}
		return pointNum;
	}
	
	 private String generateAlpha(double sequenceNumber) {
		 	sequenceNumber--;
	    	String arrayLetters[] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "aa", "ab", "ac", "ad", "ae", "af", "ag", "ah", "ai", "aj", "ak", "al", "am", "an", "añ", "ao", "ap", "aq", "ar", "as", "at", "au", "av", "aw", "ax", "ay", "az"};
	    	int startOfAlfaNumerical = 0;
	        int endOfAlfaNumerical = 55;
	        int sequenceBase = endOfAlfaNumerical - startOfAlfaNumerical;
	        int currentSequenceBase = 0;
	        String sequence= "";
	        while (true) {
	            String currentLetter = "";
	            if (currentSequenceBase > 0) {
	                int currentBaseCount = (int) (sequenceNumber / (Math.pow(sequenceBase, currentSequenceBase)));
	                if (currentBaseCount == 0) {
	                    break;
	                }
	                currentLetter = arrayLetters[currentBaseCount + startOfAlfaNumerical - 1];
	            }

	            if (currentSequenceBase == 0) {
	            	currentLetter = arrayLetters[(int) ((sequenceNumber % sequenceBase) + startOfAlfaNumerical)];
	            }
	            currentSequenceBase++;
	            sequence = currentLetter;
	        }
	        return sequence;
	    }

	private void updateNumElement(VTDNav vtdNav, XMLModifier xmlModifier, byte[] elementNum)
			throws NavException, ModifyException {
		byte[] element;
		int currentIndex = vtdNav.getCurrentIndex();
		element = buildNumElement(vtdNav, xmlModifier, elementNum);
		vtdNav.recoverNode(currentIndex);
		xmlModifier.insertAfterHead(element);
	}

	private boolean isElementOriginEC(VTDNav vtdNav) throws NavException {
		int currentIndex = vtdNav.getCurrentIndex();
		boolean isOriginEC = false;
		if (vtdNav.toElement(VTDNav.FIRST_CHILD, NUM)) {
			int attributeIndex = vtdNav.getAttrVal(LEOS_ORIGIN_ATTR);
			if (attributeIndex != -1 && vtdNav.toNormalizedString(attributeIndex).equals(LEOS_ORIGIN_ATTR_EC)) {
				isOriginEC = true;
			}
		} else {
			int attributeIndex = vtdNav.getAttrVal(LEOS_ORIGIN_ATTR);
			if (attributeIndex != -1 && vtdNav.toNormalizedString(attributeIndex).equals(LEOS_ORIGIN_ATTR_EC)) {
				isOriginEC = true;
			}
		}
		vtdNav.recoverNode(currentIndex);
		return isOriginEC;
	}

	private boolean hasAffectedAttribute(VTDNav vtdNav, XMLModifier xmlModifier) throws NavException, ModifyException {
		int index = vtdNav.getCurrentIndex();
		boolean flag;
		int attributeIndex = vtdNav.getAttrVal(LEOS_AFFECTED_ATTR);

		flag = setFlagAndRemoveAttribute(vtdNav, xmlModifier, LEOS_AFFECTED_ATTR, attributeIndex);

		vtdNav.recoverNode(index);
		return flag;
	}
	
	private boolean hasNumToggledAttribute(VTDNav vtdNav, XMLModifier xmlModifier)
			throws NavException, ModifyException {

		int index = vtdNav.getCurrentIndex();
		boolean flag = this.isDefaultEditable;
		int attributeIndex;
		if (vtdNav.toElement(VTDNav.FIRST_CHILD, PARAGRAPH)) {
			do {
				int paraIndex = vtdNav.getCurrentIndex();
				if (vtdNav.toElement(VTDNav.FIRST_CHILD, NUM)) {
					attributeIndex = vtdNav.getAttrVal(TOGGLED_TO_NUM);
					if (!flag) {
						flag = setFlagAndRemoveAttribute(vtdNav, xmlModifier, TOGGLED_TO_NUM, attributeIndex);
					} else {
						setFlagAndRemoveAttribute(vtdNav, xmlModifier, TOGGLED_TO_NUM, attributeIndex);
					}
				}
				vtdNav.recoverNode(paraIndex);
			} while (vtdNav.toElement(VTDNav.NEXT_SIBLING, PARAGRAPH));
		}

		vtdNav.recoverNode(index);
		return flag;

	}

	private boolean setFlagAndRemoveAttribute(VTDNav vtdNav, XMLModifier xmlModifier, String affectedAttribute,
			int attributeIndex) throws NavException, ModifyException {
		boolean flag = false;
		if (attributeIndex != -1 && vtdNav.toNormalizedString(attributeIndex).equals("true")) {
			flag = true;
			AutoPilot ap = new AutoPilot(vtdNav);
			ap.selectAttr(affectedAttribute);
			int attrIndex = ap.iterateAttr();
			xmlModifier.removeAttribute(attrIndex);
		}
		return flag;
	}

	private boolean isNumElementExists(VTDNav vtdNav) throws NavException {
		int index = vtdNav.getCurrentIndex();
		boolean exists = false;
		if (vtdNav.toElement(VTDNav.FIRST_CHILD, NUM)) {
			// check if Num is soft deleted
			exists = true;
			int attributeIndex = vtdNav.getAttrVal(LEOS_SOFT_ACTION_ATTR);
			if (attributeIndex != -1
					&& vtdNav.toNormalizedString(attributeIndex).equals(SoftActionType.DELETE.getSoftAction())) {
				exists = false;
			}
		}
		vtdNav.recoverNode(index);
		return exists;
	}

	private VTDNav createVTDNav(Object data, String element) {
		switch (element) {
		case ARTICLE:
			try {
				if (data instanceof String) {

					return setupVTDNav(((String) data).getBytes(UTF_8), isNameSpaceEnabled);

				} else if (data instanceof byte[]) {
					return setupVTDNav((byte[]) data, isNameSpaceEnabled);
				}
			} catch (Exception e) {
				throw new RuntimeException("Unable to perform the renumberArticles operation", e);
			}
			break;
		case RECITAL:
			try {
				if (data instanceof String) {

					return setupVTDNav(((String) data).getBytes(UTF_8));

				} else if (data instanceof byte[]) {
					return setupVTDNav((byte[]) data);
				}
			} catch (Exception e) {
				throw new RuntimeException("Unable to perform the renumberRecitals operation", e);
			}
			break;
		case PROVISO:
			try {
				if (data instanceof String) {

					return setupVTDNav(((String) data).getBytes(UTF_8), isNameSpaceEnabled);

				} else if (data instanceof byte[]) {
					return setupVTDNav((byte[]) data, isNameSpaceEnabled);
				}
			} catch (Exception e) {
				throw new RuntimeException("Unable to perform the renumberProvisos operation", e);
			}
			break;
			//Evolutivo #1052
    	case BOOK:
    		try {
    			if(data instanceof String) {

    				return setupVTDNav(((String)data).getBytes(UTF_8));

    			}else if(data instanceof byte[]) {
    				return setupVTDNav((byte[])data);
    			}
    		} catch (Exception e) {
    			throw new RuntimeException("Unable to perform the renumberRecitals operation", e);
    		}
    		break; 
    	}
		return null;
	}

	byte[] renumberElements(String element, String elementNumber, byte[] xmlContent, MessageHelper messageHelper) {
		this.isNameSpaceEnabled = false;
		VTDNav vtdNav = createVTDNav(xmlContent, element);
		long number = 1L;
		byte[] updatedElement;
		XMLModifier xmlModifier = null;

		// LEOS-AGE
		Locale languageLocale = new Locale("ES");
		String OrderType = "CARDINAL_NUMBER";
		RuleBasedNumberFormat esFormatter = new RuleBasedNumberFormat(languageLocale, RuleBasedNumberFormat.SPELLOUT);
		long count = 0L;
		//

		try {
			xmlModifier = new XMLModifier(vtdNav);

			// LEOS-AGE
			if (element.equals(ARTICLE)) {
				vtdNav.push();
				AutoPilot autoPilot_count = new AutoPilot(vtdNav);
				autoPilot_count.selectElement(ARTICLE);

				while (autoPilot_count.iterate()) {
					count++;
				}
				vtdNav.pop();

				vtdNav.push();
				LOG.trace("Checking por reordering characteristics");
				if (vtdNav.toElement(VTDNav.FIRST_CHILD)) {
					if (vtdNav.toElement(VTDNav.FIRST_CHILD, BODY)) {
						OrderType = getOrderingAttributeForNode(vtdNav).toString();
						LOG.trace("Article Reordering Type: {} ", OrderType);
					}
				}
				vtdNav.pop();
			}

			if (element.equals(PROVISO)) {
				vtdNav.push();
				AutoPilot autoPilot_count = new AutoPilot(vtdNav);
				autoPilot_count.selectElement(PROVISO);

				while (autoPilot_count.iterate()) {
					count++;
				}
				vtdNav.pop();

				vtdNav.push();
				LOG.trace("Checking por reordering characteristics");
				if (vtdNav.toElement(VTDNav.FIRST_CHILD)) {
					if (vtdNav.toElement(VTDNav.FIRST_CHILD, BODY)) {
						OrderType = getOrderingAttributeForNode(vtdNav).toString();
						LOG.trace("Proviso Reordering Type: {} ", OrderType);
					}
				}
				vtdNav.pop();
			}			
			
			AutoPilot autoPilot = new AutoPilot(vtdNav);
			autoPilot.selectElement(element);
			byte[] elementeNum = null;
			String recitalNum = null;
			while (autoPilot.iterate()) {
				if (element.equals(ARTICLE)) {
					// LEOS-AGE
					// if ((count==1) && (languageLocale.getLanguage().equals("es")) ) {
					if ((count == 1) && (languageLocale.getLanguage().equals("es"))) {
						elementeNum = messageHelper.getMessage("legaltext.article.num", new Object[] { "único" })
								.getBytes(UTF_8);
					} else {
						if (OrderType == "ORDINAL_LETTER" && (languageLocale.getLanguage().equals("es"))) {
							elementeNum = messageHelper
									.getMessage("legaltext.article.num",
											new Object[] {
													(esFormatter.format(number++, "%spellout-ordinal-masculine")) })
									.getBytes(UTF_8);
							LOG.info("Article number: {} {}", number,
									esFormatter.format(number, "%spellout-ordinal-masculine"));
						} else {
							elementeNum = messageHelper.getMessage("legaltext.article.num", new Object[] { (number++) })
									.getBytes(UTF_8);
						}
					}
					//
				} else if (element.equals(RECITAL)) {
					// LEOS-AGE Modified for numbering in Romans
					recitalNum = integerToRomanNumeral(number++);
					//
					elementeNum = recitalNum.getBytes(UTF_8);
				}

				int currentIndex = vtdNav.getCurrentIndex();
				updatedElement = buildNumElement(vtdNav, xmlModifier, elementeNum);
				vtdNav.recoverNode(currentIndex);
				xmlModifier.insertAfterHead(updatedElement);
			}
			this.isNameSpaceEnabled = true;
			return toByteArray(xmlModifier);
		} catch (Exception e) {
			throw new RuntimeException("Unable to perform the renumber" + element + " operation", e);
		}

	}

	byte[] renumberProvisos(String element, String elementNumber, byte[] xmlContent, MessageHelper messageHelper) {
		this.isNameSpaceEnabled = false;
		VTDNav vtdNavAux = createVTDNav(xmlContent, element);
		VTDNav vtdNav = createVTDNav(xmlContent, element);
		byte[] updatedElement;
		XMLModifier xmlModifier = null;
		try {
			xmlModifier = new XMLModifier(vtdNav);
			String numberTypeProvisoActual = "";
			byte[] elementeNum = null;
			long numberAux = 0L;
			
			//Know if the proviso is unique or not
			Map<String, Long> numbersTypesAux = new HashMap<>();			
			for (AGENumberTypeProviso numberType : AGENumberTypeProviso.values()) {
				numbersTypesAux.put(
						messageHelper.getMessage(String.format("legaltext.proviso.numtype.%s", numberType.getTitle())), numberAux);
			}
			AutoPilot autoPilotCount = new AutoPilot(vtdNavAux);
			autoPilotCount.selectElement(PROVISO);
			while(autoPilotCount.iterate()) {
				if(vtdNavAux.toElement(VTDNav.FIRST_CHILD,NUM)) {
					int indexNumberType = vtdNavAux.getText();
					if(indexNumberType != -1) {
						numberTypeProvisoActual = vtdNavAux.toString(indexNumberType).split(" ")[1];
					}
					numberAux = numbersTypesAux.get(numberTypeProvisoActual) + 1;
					numbersTypesAux.put(numberTypeProvisoActual, numberAux);					
				}
			}
			long number = 1L; 
			Map<String, Long> numbersTypes = new HashMap<>();
			for (AGENumberTypeProviso numberType : AGENumberTypeProviso.values()) {
				numbersTypes.put(
						messageHelper.getMessage(String.format("legaltext.proviso.numtype.%s", numberType.getTitle())), number);
			}			
			AutoPilot autoPilotIterate = new AutoPilot(vtdNav);
			autoPilotIterate.selectElement(PROVISO);
			while(autoPilotIterate.iterate()) {
				if(vtdNav.toElement(VTDNav.FIRST_CHILD,NUM)) {
					int indexNumberType = vtdNav.getText();
					if(indexNumberType != -1) {
						numberTypeProvisoActual = vtdNav.toString(indexNumberType).split(" ")[1];
					}
					//If the proviso type is unique
					
					if(numbersTypesAux.get(numberTypeProvisoActual)==1L)
						elementeNum = messageHelper.getMessage("legaltext.proviso.num", numberTypeProvisoActual,
								messageHelper.getMessage("legaltext.proviso.num.unique")+".").getBytes(UTF_8);//Evolutivo #2418
					else {
						String ordinalNum = numberToOrdinal(numbersTypes.get(numberTypeProvisoActual),messageHelper);
						elementeNum = messageHelper.getMessage("legaltext.proviso.num", numberTypeProvisoActual,
								ordinalNum+".").getBytes(UTF_8);//Evolutivo #2418
					}
					number = numbersTypes.get(numberTypeProvisoActual) + 1;					
					numbersTypes.put(numberTypeProvisoActual, number);
				}
				vtdNav.toElement(VTDNav.PARENT);
				int currentIndex = vtdNav.getCurrentIndex();
				updatedElement = buildNumElement(vtdNav, xmlModifier, elementeNum);
				vtdNav.recoverNode(currentIndex);
				xmlModifier.insertAfterHead(updatedElement);
			}
			this.isNameSpaceEnabled = true;
			return toByteArray(xmlModifier);
		} catch (Exception e) {
			throw new RuntimeException("Unable to perform the renumber" + element + " operation", e);
		}
	} 

      //Evolutivo #1052
        void createNumberFormat (String tipo, XMLModifier xmlModifier, long number, VTDNav vn, MessageHelper messageHelper ) {
        	Locale languageLocale = new Locale("ES");
        	RuleBasedNumberFormat esFormatter = new RuleBasedNumberFormat(languageLocale, RuleBasedNumberFormat.SPELLOUT);
        	byte[] updatedElement = null;
        	byte[] elementeNum =  null;
        	String auxNum = null;
        	int currentIndex;
        	try {
        		switch (tipo) {
        		case BOOK:
        			auxNum = esFormatter.format(number++, "%spellout-ordinal-masculine");
        			elementeNum =  messageHelper.getMessage("legaltext.book.num", new Object[]{(auxNum)}).toUpperCase().getBytes(UTF_8);
        			break;
        		case CHAPTER:
					auxNum = integerToRomanNumeral(number++);
					elementeNum = messageHelper.getMessage("legaltext.chapter.num", new Object[]{(auxNum)}).toUpperCase().getBytes(UTF_8);
        			break;
//        		case TITLE:
//        			auxNum = integerToRomanNumeral(number++);
//					elementeNum = messageHelper.getMessage("legaltext.title.num", new Object[]{(auxNum)}).toUpperCase().getBytes(UTF_8);
//        			break;
        		case SECTION:
        			auxNum = number++ + ".ª ";
					elementeNum = messageHelper.getMessage("legaltext.section.num", new Object[]{(auxNum)}).toUpperCase().getBytes(UTF_8);	
        			break;
        		case SUBSECTION:
        			auxNum = number++ + ".ª ";
					elementeNum = messageHelper.getMessage("legaltext.subsection.num", new Object[]{(auxNum)}).toUpperCase().getBytes(UTF_8);	
        			break;
        		} 
    			currentIndex = vn.getCurrentIndex();
    			updatedElement = buildNumElement(vn, xmlModifier, elementeNum);
    			vn.recoverNode(currentIndex);
    			xmlModifier.insertAfterHead(updatedElement);
        	}catch (Exception e) {
        		throw new RuntimeException("Unable to perform the renumberOthers operation", e);
        	}
        }

        /*
         * METODO PREPARADO PARA RENUMERAR TITULOS
         */
//        byte[] buildNumElementNew(VTDNav vtdNav, XMLModifier xmlModifier, byte[] numBytes) throws NavException, ModifyException {
//            byte[] element;
//
//            if (vtdNav.toElement(VTDNav.FIRST_CHILD, NUM)) {
//            	int aux2 = vtdNav.getText();
//            	if (vtdNav.toString(aux2).contains("PRELIMINAR")) {
//            		numberTitle++;
//            		 element = XmlHelper.buildTag(NUM_START_TAG, NUM_BYTES, "PRELIMINAR".getBytes());
//            	} else {
//                    byte[] numTag = getStartTag(vtdNav);
//                    element = XmlHelper.buildTag(numTag, NUM_BYTES, numBytes);
//                    xmlModifier.remove();
//            	}
//            } else {
//                // build num if not exists
//                element = XmlHelper.buildTag(NUM_START_TAG, NUM_BYTES, numBytes);
//            }
//            return element;
//        }
        
        
        byte[] renumberOthers(byte[] xmlContent, MessageHelper messageHelper) throws EncodingException, EOFException, EntityException, ParseException {
        	VTDNav vn = createVTDNav(xmlContent, BOOK);
        	XMLModifier xmlModifier = null;
        	try {
        		xmlModifier = new XMLModifier(vn);	
        		long numBook = 1L;
        		long numChapter = 1L;
        		long numSection = 1L;
        		long numSubsection = 1L;
        		AutoPilot ap = new AutoPilot(vn);
        		AutoPilot ap2 = new AutoPilot(vn);
        		ap.selectXPath("//body | //mainBody");
        		while (ap.evalXPath() != -1) {
        			if(vn.toElement(VTDNav.FIRST_CHILD, BOOK)){
        				do {
        					createNumberFormat(BOOK, xmlModifier, numBook++, vn, messageHelper);
        					if(vn.toElement(VTDNav.FIRST_CHILD, TITLE)){
        						do {
        							if(vn.toElement(VTDNav.FIRST_CHILD,CHAPTER)) {
        								do {
        									createNumberFormat(CHAPTER, xmlModifier, numChapter++, vn, messageHelper);
        									if(vn.toElement(VTDNav.FIRST_CHILD,SECTION)) {
        										do {
        											createNumberFormat(SECTION, xmlModifier, numSection++, vn, messageHelper);
        											if(vn.toElement(VTDNav.FIRST_CHILD,SUBSECTION)) {
        												do {
        													createNumberFormat(SUBSECTION, xmlModifier, numSubsection++, vn, messageHelper);
        												}while (vn.toElement(VTDNav.NEXT_SIBLING, SUBSECTION));
        												vn.toElement(VTDNav.PARENT);
        											}numSubsection=1L;
        										}while (vn.toElement(VTDNav.NEXT_SIBLING, SECTION));
        										vn.toElement(VTDNav.PARENT);
        									}numSection=1L;
        								}while (vn.toElement(VTDNav.NEXT_SIBLING, CHAPTER));
        								vn.toElement(VTDNav.PARENT);
        							}numChapter=1L;
        						}while(vn.toElement(VTDNav.NEXT_SIBLING, TITLE));
        						vn.toElement(VTDNav.PARENT);
        					}
        				} while(vn.toElement(VTDNav.NEXT_SIBLING, BOOK));
        			}
        		}
        		ap.selectXPath("//body/title | //mainBody/title");
        		while (ap.evalXPath() != -1) {
        			if(vn.toElement(VTDNav.FIRST_CHILD,CHAPTER)) {
        				do {
        					createNumberFormat(CHAPTER, xmlModifier, numChapter++, vn, messageHelper);
        					if(vn.toElement(VTDNav.FIRST_CHILD,SECTION)) {
        						do {
        							createNumberFormat(SECTION, xmlModifier, numSection++, vn, messageHelper);
        							if(vn.toElement(VTDNav.FIRST_CHILD,SUBSECTION)) {
        								do {
        									createNumberFormat(SUBSECTION, xmlModifier, numSubsection++, vn, messageHelper);
        								}while (vn.toElement(VTDNav.NEXT_SIBLING, SUBSECTION));
        								vn.toElement(VTDNav.PARENT);
        							}numSubsection=1L;
        						}while (vn.toElement(VTDNav.NEXT_SIBLING, SECTION));
        						vn.toElement(VTDNav.PARENT);
        					}numSection=1L;

        				}while (vn.toElement(VTDNav.NEXT_SIBLING, CHAPTER));
        				vn.toElement(VTDNav.PARENT);
        			}numChapter=1L;
        		}
        		ap.selectXPath("//body/chapter | //mainBody/chapter");
        		while (ap.evalXPath() != -1) {
        			createNumberFormat(CHAPTER, xmlModifier, numChapter++, vn, messageHelper);
        			if(vn.toElement(VTDNav.FIRST_CHILD,SECTION)) {
        				do {
        					createNumberFormat(SECTION, xmlModifier, numSection++, vn, messageHelper);
        					if(vn.toElement(VTDNav.FIRST_CHILD,SUBSECTION)) {
        						do {
        							createNumberFormat(SUBSECTION, xmlModifier, numSubsection++, vn, messageHelper);
        						}while (vn.toElement(VTDNav.NEXT_SIBLING, SUBSECTION));
        						vn.toElement(VTDNav.PARENT);
        					}numSubsection=1L;
        				}while (vn.toElement(VTDNav.NEXT_SIBLING, SECTION));
        				vn.toElement(VTDNav.PARENT);
        			}numSection=1L;
        		}numChapter=1L;
        		ap.selectXPath("//body/section | //mainBody/section");
        		while (ap.evalXPath() != -1) {
        			createNumberFormat(SECTION, xmlModifier, numSection++, vn, messageHelper);
        			if(vn.toElement(VTDNav.FIRST_CHILD,SUBSECTION)) {
        				do {
        					createNumberFormat(SUBSECTION, xmlModifier, numSubsection++, vn, messageHelper);
        				}while (vn.toElement(VTDNav.NEXT_SIBLING, SUBSECTION));
        				vn.toElement(VTDNav.PARENT);
        			}numSubsection=1L;
        		}	
        	return toByteArray(xmlModifier);

        }  catch (Exception e) {
        		throw new RuntimeException("Unable to perform the renumberOthers operation", e);
        	}
        }
	
        /**
         * Se deja método preparado para renumerar los títulos
         * @author ext_desarrollo36
         *
         */
//        byte[] renumberOthersBis(byte[] xmlContent, MessageHelper messageHelper) throws EncodingException, EOFException, EntityException, ParseException {
//        	VTDNav vn = createVTDNav(xmlContent, BOOK);
//        	XMLModifier xmlModifier = null;
//        	try {
//        		xmlModifier = new XMLModifier(vn);	
//        		long numBook = 1L;
//        		long numChapter = 1L;
//        		long numSection = 1L;
//        		long numSubsection = 1L;
//        		long numTitle = 1L;
//        		AutoPilot ap = new AutoPilot(vn);
//        		ap.selectXPath("//body");
//        		while (ap.evalXPath() != -1) {
//        			if(vn.toElement(VTDNav.FIRST_CHILD, BOOK)){
//        				do {
//        					createNumberFormat(BOOK, xmlModifier, numBook++, vn, messageHelper);
//        					if(vn.toElement(VTDNav.FIRST_CHILD, TITLE)){
//        						createNumberFormat(TITLE, xmlModifier, numTitle++ - numberTitle, vn, messageHelper);
//        						do {
//        							if(vn.toElement(VTDNav.FIRST_CHILD,CHAPTER)) {
//        								do {
//        									createNumberFormat(CHAPTER, xmlModifier, numChapter++, vn, messageHelper);
//        									if(vn.toElement(VTDNav.FIRST_CHILD,SECTION)) {
//        										do {
//        											createNumberFormat(SECTION, xmlModifier, numSection++, vn, messageHelper);
//        											if(vn.toElement(VTDNav.FIRST_CHILD,SUBSECTION)) {
//        												do {
//        													createNumberFormat(SUBSECTION, xmlModifier, numSubsection++, vn, messageHelper);
//        												}while (vn.toElement(VTDNav.NEXT_SIBLING, SUBSECTION));
//        												vn.toElement(VTDNav.PARENT);
//        											}numSubsection=1L;
//        										}while (vn.toElement(VTDNav.NEXT_SIBLING, SECTION));
//        										vn.toElement(VTDNav.PARENT);
//        									}numSection=1L;
//        								}while (vn.toElement(VTDNav.NEXT_SIBLING, CHAPTER));
//        								vn.toElement(VTDNav.PARENT);
//        							}numChapter=1L;
//        						}while(vn.toElement(VTDNav.NEXT_SIBLING, TITLE));
//        						vn.toElement(VTDNav.PARENT);
//        					}numTitle = 1L;
//        				} while(vn.toElement(VTDNav.NEXT_SIBLING, BOOK));
//        			}
//        		}
//        		ap.selectXPath("//body/title");
//        		while (ap.evalXPath() != -1) {
//            			createNumberFormat(TITLE, xmlModifier, numTitle++ - numberTitle, vn, messageHelper);
//            			if(vn.toElement(VTDNav.FIRST_CHILD,CHAPTER)) {
//            				do {
//            					createNumberFormat(CHAPTER, xmlModifier, numChapter++, vn, messageHelper);
//            					if(vn.toElement(VTDNav.FIRST_CHILD,SECTION)) {
//            						do {
//            							createNumberFormat(SECTION, xmlModifier, numSection++, vn, messageHelper);
//            							if(vn.toElement(VTDNav.FIRST_CHILD,SUBSECTION)) {
//            								do {
//            									createNumberFormat(SUBSECTION, xmlModifier, numSubsection++, vn, messageHelper);
//            								}while (vn.toElement(VTDNav.NEXT_SIBLING, SUBSECTION));
//            								vn.toElement(VTDNav.PARENT);
//            							}numSubsection=1L;
//            						}while (vn.toElement(VTDNav.NEXT_SIBLING, SECTION));
//            						vn.toElement(VTDNav.PARENT);
//            					}numSection=1L;
//
//            				}while (vn.toElement(VTDNav.NEXT_SIBLING, CHAPTER));
//            				vn.toElement(VTDNav.PARENT);
//            			}numChapter=1L;	
//        		}numTitle=1L;
//        		ap.selectXPath("//body/chapter");
//        		while (ap.evalXPath() != -1) {
//        			createNumberFormat(CHAPTER, xmlModifier, numChapter++, vn, messageHelper);
//        			if(vn.toElement(VTDNav.FIRST_CHILD,SECTION)) {
//        				do {
//        					createNumberFormat(SECTION, xmlModifier, numSection++, vn, messageHelper);
//        					if(vn.toElement(VTDNav.FIRST_CHILD,SUBSECTION)) {
//        						do {
//        							createNumberFormat(SUBSECTION, xmlModifier, numSubsection++, vn, messageHelper);
//        						}while (vn.toElement(VTDNav.NEXT_SIBLING, SUBSECTION));
//        						vn.toElement(VTDNav.PARENT);
//        					}numSubsection=1L;
//        				}while (vn.toElement(VTDNav.NEXT_SIBLING, SECTION));
//        				vn.toElement(VTDNav.PARENT);
//        			}numSection=1L;
//        		}numChapter=1L;
//        		ap.selectXPath("//body/section");
//        		while (ap.evalXPath() != -1) {
//        			createNumberFormat(SECTION, xmlModifier, numSection++, vn, messageHelper);
//        			if(vn.toElement(VTDNav.FIRST_CHILD,SUBSECTION)) {
//        				do {
//        					createNumberFormat(SUBSECTION, xmlModifier, numSubsection++, vn, messageHelper);
//        				}while (vn.toElement(VTDNav.NEXT_SIBLING, SUBSECTION));
//        				vn.toElement(VTDNav.PARENT);
//        			}numSubsection=1L;
//        		}	
//        	return toByteArray(xmlModifier);
//
//        }  catch (Exception e) {
//        		throw new RuntimeException("Unable to perform the renumberOthers operation", e);
//        	}
//        }

	// LEOS-AGE

	public enum OrderingAttributeValue {
		ORDINAL_LETTER, CARDINAL_LETTER, ORDINAL_NUMBER, CARDINAL_NUMBER, UNDEFINED, MANUAL;
	}

	private static final String LEOS_ORDERING_ATTR = "leos:ordering";

	public OrderingAttributeValue getOrderingAttributeForNode(VTDNav vtdNav) throws NavException {
		AutoPilot ap = new AutoPilot(vtdNav);
		Map<String, String> attrs = getElementAttributes(vtdNav, ap);
		// LOG.trace("Attrs kk: {} ",attrs.toString());
		String tagName = vtdNav.toString(vtdNav.getCurrentIndex());
		String attrVal = attrs.get(LEOS_ORDERING_ATTR);
		// LOG.trace("Attrs: {} ",attrVal);

		if (isExcludedNode(tagName)) {
			return OrderingAttributeValue.UNDEFINED;
		} else if (attrVal == null) {
			return OrderingAttributeValue.UNDEFINED;
		} else if (attrVal.equalsIgnoreCase("ordinal_letter")) {
			return OrderingAttributeValue.ORDINAL_LETTER;
		} else if (attrVal.equalsIgnoreCase("cardinal_letter")) {
			return OrderingAttributeValue.CARDINAL_LETTER;
		} else if (attrVal.equalsIgnoreCase("ordinal_number")) {
			return OrderingAttributeValue.ORDINAL_NUMBER;
		} else if (attrVal.equalsIgnoreCase("cardinal_number")) {
			return OrderingAttributeValue.CARDINAL_NUMBER;
		} else if (attrVal.equalsIgnoreCase("manual")) {
			return OrderingAttributeValue.MANUAL;
		} else {
			return OrderingAttributeValue.UNDEFINED;
		}
	}

	private Map<String, String> getElementAttributes(VTDNav vtdNav, AutoPilot ap) {
		Map<String, String> attrs = new HashMap<String, String>();
		int i = -1;
		try {
			ap.selectAttr("*");
			while ((i = ap.iterateAttr()) != -1) {
				attrs.put(vtdNav.toString(i), vtdNav.toString(i + 1));
			}
		} catch (Exception e) {
			LOG.error("Unexpected error occured while getting elements", e);
		}
		return attrs;
	}

	String integerToRomanNumeral(long number) {
		int integerNumber = Integer.parseInt(String.valueOf(number));
		int comprobationNumber = this.romanEquivalent.floorKey(integerNumber);
		if (number == comprobationNumber) {
			return this.romanEquivalent.get(integerNumber);
		}
		return this.romanEquivalent.get(comprobationNumber) + integerToRomanNumeral(number - comprobationNumber);
	}
	//

	private void initRomanEquivalentMap() {
		this.romanEquivalent = new TreeMap<>();
		this.romanEquivalent.put(1000, "M");
		this.romanEquivalent.put(900, "CM");
		this.romanEquivalent.put(500, "D");
		this.romanEquivalent.put(400, "CD");
		this.romanEquivalent.put(100, "C");
		this.romanEquivalent.put(90, "XC");
		this.romanEquivalent.put(50, "L");
		this.romanEquivalent.put(40, "XL");
		this.romanEquivalent.put(10, "X");
		this.romanEquivalent.put(9, "IX");
		this.romanEquivalent.put(5, "V");
		this.romanEquivalent.put(4, "IV");
		this.romanEquivalent.put(1, "I");
	}
	
	private String numberToOrdinal(long num,MessageHelper messageHelper) { 
      String[] units = arrayUnits(messageHelper);
      String[] tens = arrayTens(messageHelper);
      String[] hundreds = arrayHundreds(messageHelper);      
      int u=(int) (num%10);
      int d=(int) ((num/10)%10);
      int c=(int) (num/100);
      if(num>=100){
          return (hundreds[c]+" "+tens[d]+" "+units[u]);
      }else{
    	  if(num>10){
    		  return (tens[d]+" "+units[u]);
    	  } else if (num == 10) {
    		  return (tens[d]);
    	  }else{
    		  return (units[(int) num]);
    	  }
      }
    }
	
	private String[] arrayUnits(MessageHelper messageHelper) {
		String units[]={"", messageHelper.getMessage("legaltext.proviso.ordinalnum.first"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.second"), 
				messageHelper.getMessage("legaltext.proviso.ordinalnum.third"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.fourth"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.fifth"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.sixth"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.seventh"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.eighth"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.nineth")};
		return units;
	}
	
	private String[] arrayTens(MessageHelper messageHelper){
		String tens[]={"",messageHelper.getMessage("legaltext.proviso.ordinalnum.tenth"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.twentieth"), 
				messageHelper.getMessage("legaltext.proviso.ordinalnum.thirtieth"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.fortieth"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.fiftieth"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.sixtieth"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.seventieth"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.eightieth"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.ninetieth")};
		return tens;
	}
	
	private String[] arrayHundreds(MessageHelper messageHelper){
		String hundreds[]={"",messageHelper.getMessage("legaltext.proviso.ordinalnum.onehundred"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.twohundred"), 
				messageHelper.getMessage("legaltext.proviso.ordinalnum.threehundred"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.fourhundred"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.fivehundred"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.sixhundred"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.sevenhundred"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.eighthundred"),
				messageHelper.getMessage("legaltext.proviso.ordinalnum.ninehundred")};
		return hundreds;
	}

        void setImportAticleDefaultProperties() {
            this.isDefaultEditable = true;
            this.isNameSpaceEnabled = false;

        };

        void setImportProvisoDefaultProperties() {
            this.isDefaultEditable = true;
            this.isNameSpaceEnabled = false;

        };        
        
        void resetImportAticleDefaultProperties() {
            this.isDefaultEditable = false;
            this.isNameSpaceEnabled = true;
        }

        void resetImportProvisoDefaultProperties() {
            this.isDefaultEditable = false;
            this.isNameSpaceEnabled = true;
        }
        
        @Override
        public byte[] renumberChildrenByXPath(String xPath, byte[] xmlContent, String tagNameToRenumber) throws Exception {
            final VTDNav vtdNav = setupVTDNav(xmlContent, true);

            AutoPilot autoPilot = new AutoPilot(vtdNav);
            autoPilot.declareXPathNameSpace("xml", "http://www.w3.org/XML/1998/namespace");
            autoPilot.declareXPathNameSpace("leos", "urn:eu:europa:ec:leos");
            autoPilot.selectXPath(xPath);

            int number = 1;
            byte[] updatedElement;
            XMLModifier xmlModifier;
            Locale languageLocale = new Locale("ES");
            RuleBasedNumberFormat esFormatter = new RuleBasedNumberFormat(languageLocale, RuleBasedNumberFormat.SPELLOUT);
            try {
                xmlModifier = new XMLModifier(vtdNav);
                byte[] elementNum = null;
                while (autoPilot.evalXPath() != -1) {
                    switch (tagNameToRenumber) {
                        case PARAGRAPH:
                        	switch (number) {
            				case 1:
            					elementNum = "Uno.".getBytes(UTF_8);
            					break;
            				default:
            					if (number == 2) {
            						elementNum = "Uno.".getBytes(UTF_8);
            					} else {
            						String cardinalNum=esFormatter.format(number - 1, "%spellout-cardinal-masculine") + ".";
                					elementNum = (cardinalNum.substring(0,1).toUpperCase() + cardinalNum.substring(1)).getBytes(UTF_8);
            					}
                                break;
							}
                        	number++;
                    }
                    int currentIndex = vtdNav.getCurrentIndex();
                    if (number != 2) {
                        updatedElement = buildNumElement(vtdNav, xmlModifier, elementNum);
                        vtdNav.recoverNode(currentIndex);
                        xmlModifier.insertAfterHead(updatedElement);
                    }
                }
                return toByteArray(xmlModifier);
            } catch (Exception e) {
                throw new RuntimeException("Unable to perform the renumberElement operation", e);
            }
        }
               
}
