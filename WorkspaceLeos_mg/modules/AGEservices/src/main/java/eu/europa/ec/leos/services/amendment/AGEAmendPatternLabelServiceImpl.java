package eu.europa.ec.leos.services.amendment;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import eu.europa.ec.leos.amend.AmendActDetails;
import eu.europa.ec.leos.amend.AmendAction;
import eu.europa.ec.leos.domain.cmis.Content;
import eu.europa.ec.leos.domain.cmis.document.Bill;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.action.CheckinElement;
import eu.europa.ec.leos.services.content.ReferenceLabelService;
import eu.europa.ec.leos.services.support.IdGenerator;
import eu.europa.ec.leos.services.support.xml.AGENumberProcessor;
import eu.europa.ec.leos.services.support.xml.AGEXmlContentProcessor;
import eu.europa.ec.leos.services.support.xml.AGEXmlHelper;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.ACTIVEMODIFICATIONS;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.REFERENCES;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.QUOTEDSTRUCTURE;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.MOD;
import eu.europa.ec.leos.services.support.xml.ElementNumberingHelper;

@Primary
@Service
public class AGEAmendPatternLabelServiceImpl extends AmendPatternLabelServiceImpl implements AGEAmendPatternLabelService {
	
	final private ReferenceLabelService referenceLabelService;
	
	public AGEAmendPatternLabelServiceImpl(ReferenceLabelService referenceLabelService) {
		super(referenceLabelService);
		this.referenceLabelService = referenceLabelService;
	}

	@Autowired
	private AGEXmlContentProcessor xmlContentProcessor;
	
	@Autowired
	private AGENumberProcessor numberProcessor;
	
	@Autowired
	private ElementNumberingHelper elementNumberingHelper;
	
	@Autowired
    protected MessageHelper messageHelper;
	
	private final static String PROVISO = "proviso";
	private final static String DISPOSICION = "Disposici";
	private final static String ARTICLE = "article";
	private final static String ARTICULO = "Artículo";
	private final static String PARAGRAPH = "paragraph";
	private final static String POINT = "point";
	private final static String BOOK = "book";
	private final static String CHAPTER = "chapter";
	private final static String TITLE = "akntitle";
//	private final static String SECTION = "section";
//	private final static String SUBSECTION = "subsection";
	private final static String HEADING = "heading";	
	private final static String APAR_SING = "el apartado ";
	private final static String INTRO_INSER = "Se inserta ";
	private final static String INTRO_DELETE = "Se suprime ";
	private final static String APAR_SIN_NUM = "el apartado único ";
	private final static String APARTADO_ARTICLE = " del ";
	private final static String APARTADO_ARTICLE_FRAME = " en el ";
	private final static String SELEC_ARTICLE = "El ";
	private final static String APARTADO_PROVISO = " de la ";
	private final static String APARTADO_PROVISO_FRAME = " en la ";
	private final static String SELEC_PROVISO = "La ";
	private final static String APARTADO_TITULO = "El título ";
	private final static String SALTO_LINEA_HTML = "<br>";
	private final static String FINAL = ", que queda redactado en los siguientes términos:";
	private final static String [] LIST_NORM = {"rd", "rdl", "rdlg", "dl", "dlf", "dlg", "dflg", "reg", "d", "df", "a"} ; 
	
	private String typeSelectedElement = "";
	private String refElement="";
	private String elementQuotedStructure = "";
	private String [] elementNumSplit;
	private String parentElement = "";
	private String parentNum;
	private String [] template;
	private String cadenaNewNum;
	private String pos;
	private String refNorm="";
	private String templateGeneralFrameText=  "";
	private String idModOld;
	private String typeNorm;
	private String quotedStructureId;
	
	@Override
	public String generateLabel(AmendAction amendAction, List<String> elements, String title, String eli, String numArticleCk, String typeNorm, String elementType, String elementId, boolean isEditing) {
		String name_norm = title.substring(0, title.length() - 1);
		String idArticleTarget = elementId;
		String virtualUrlEli = eli;
		this.typeNorm = typeNorm;
		cadenaNewNum = "";
		quotedStructureId = IdGenerator.generateId("akn_quoted_amend",9);
		String [] elementsAux = elements.get(0).split("--");
		refNorm = "<affectedDocument href=\'"+ virtualUrlEli + "\'>"+ name_norm + "</affectedDocument>";
		String templateArticleHeading = "Modificación " + (containTypeNorm(typeNorm)?APARTADO_ARTICLE:APARTADO_PROVISO) + name_norm;
		switch (amendAction.toString()) {
		case "INSERT_TOP":
		case "INSERT_BOTTOM":
			if (elements.size() == 1) {
				cadenaNewNum = generatePositionInsert(amendAction.toString(), elementQuotedStructure, parentElement, elementsAux[0]);
				switch (elementsAux[0]) {
				case POINT:
					String[] auxNewNumsplitAux = elementsAux[2].split("::");
					if (elementNumSplit.length == 3) {
						if(elementNumSplit[1].contains("primero")) {
							refElement = refElement.replace(elementNumSplit[0].replace(".", ""), cadenaNewNum).replace("primero.", "");
							cadenaNewNum = auxNewNumsplitAux[1].trim() + cadenaNewNum.trim();
							this.elementQuotedStructure = generateQuotedInsert(cadenaNewNum, elementsAux[0]);
						}else {
							refElement = refElement.replace(elementNumSplit[0].replace(".", ""), cadenaNewNum);
							cadenaNewNum = auxNewNumsplitAux[1].trim() + cadenaNewNum.trim();
							String [] auxCadena = cadenaNewNum.split("\\.");
							this.elementQuotedStructure = generateQuotedInsert(auxCadena[1], elementsAux[0]);
						}
		
						
					} else if (elementNumSplit.length == 4) {
						
						if(elementNumSplit[2].contains("primero")) {
							refElement = refElement.replace(elementNumSplit[0].replace(".", ""), cadenaNewNum).replace("primero.", "");
							cadenaNewNum = auxNewNumsplitAux[2].trim() + auxNewNumsplitAux[1].trim() + "." + cadenaNewNum.trim();
							this.elementQuotedStructure = generateQuotedInsert(cadenaNewNum, elementsAux[0]);
						}else {
							refElement = refElement.replace(elementNumSplit[0].trim(), cadenaNewNum);
							this.elementQuotedStructure = generateQuotedInsert(cadenaNewNum, elementsAux[0]);
							cadenaNewNum = auxNewNumsplitAux[2].trim() + auxNewNumsplitAux[1].trim() + "." + cadenaNewNum.trim();
						}
						
					
					}
		
					templateGeneralFrameText = INTRO_INSER + refElement.replace(":", "").replace("del", "al") +  (containTypeNorm(typeNorm)?APARTADO_ARTICLE:APARTADO_PROVISO) + refNorm
							+ FINAL;
					break;
				case PARAGRAPH:
					refElement = refElement.replace(elementNumSplit[0].replace(".", ""), cadenaNewNum.replace(".", ""));	
					this.elementQuotedStructure = generateQuotedInsert(cadenaNewNum, elementsAux[0]);
					templateGeneralFrameText = INTRO_INSER + refElement.replace(":", "").replace("del", "al") +  (containTypeNorm(typeNorm)?APARTADO_ARTICLE:APARTADO_PROVISO) + refNorm
							+ FINAL;
					break;
				case ARTICLE:
					refElement =  "el " + cadenaNewNum;
					this.elementQuotedStructure = generateQuotedInsert(cadenaNewNum, elementsAux[0]);
					templateGeneralFrameText = INTRO_INSER + refElement + " a la " + refNorm
							+ FINAL;
					break;
				case PROVISO:
					refElement = "la " + cadenaNewNum;
					this.elementQuotedStructure = generateQuotedInsert(cadenaNewNum, elementsAux[0]);
					templateGeneralFrameText = INTRO_INSER + refElement.toLowerCase() +  " a la " + refNorm
							+ FINAL;
					break;
				default:
					break;
				}
				if (cadenaNewNum != null && cadenaNewNum.contains("bis")) {
					pos = "after";
				} else {
					pos = "end";
				}
			} else if (elements.size() > 1) {
				
			}
			break;
		case "REPLACE":
			if (elements.size() == 1) {
				elementQuotedStructure = elementsAux[3];
				elementQuotedStructure = elementQuotedStructure.replace("id=", "xml:id=");
				elementQuotedStructure = elementQuotedStructure.replace("aknp", "p");
				elementQuotedStructure = elementQuotedStructure.replace("&nbsp;", " ");
				switch (elementsAux[0]) {
				case POINT:
					if (elementNumSplit.length == 3) {
						if(!elementNumSplit[1].contains("primero")) {
							cadenaNewNum =elementNumSplit[1].trim() + elementNumSplit[0].trim();
							refElement = APAR_SING + elementNumSplit[1].trim() + elementNumSplit[0].trim() + " " + (elementNumSplit[2].contains(DISPOSICION) ?  APARTADO_PROVISO:APARTADO_ARTICLE)  + elementNumSplit[2].replace(".", ":");

						}else {
							cadenaNewNum =elementNumSplit[1].trim() + elementNumSplit[0].trim();
							refElement = APAR_SING + elementNumSplit[0].trim() + " " + (elementNumSplit[2].contains(DISPOSICION) ?  APARTADO_PROVISO:APARTADO_ARTICLE)  + elementNumSplit[2].replace(".", ":");

						}
					} else if (elementNumSplit.length == 4) {
						if(!elementNumSplit[2].contains("primero")) {
							cadenaNewNum =  elementNumSplit[2].trim() + elementNumSplit[1].trim()+ "." + elementNumSplit[0].trim();
							refElement = APAR_SING + elementNumSplit[2].trim() + elementNumSplit[1].trim() + "." + elementNumSplit[0] + (elementNumSplit[3].contains(DISPOSICION) ?  APARTADO_PROVISO:APARTADO_ARTICLE)  + elementNumSplit[3].replace(".", ":");
				
						}else {
							cadenaNewNum =  elementNumSplit[2].trim() + elementNumSplit[1].trim()+ "." + elementNumSplit[0].trim();
							refElement = APAR_SING + elementNumSplit[1].trim() + "." + elementNumSplit[0] + (elementNumSplit[3].contains(DISPOSICION) ?  APARTADO_PROVISO:APARTADO_ARTICLE)  + elementNumSplit[3].replace(".", ":");
			
							}		
						}					
					break;
				case PARAGRAPH:
					cadenaNewNum = elementNumSplit[0].replace(".", "");
					if(elementNumSplit[0].contains("primero")) {
						refElement = APAR_SING + (elementNumSplit[1].contains(DISPOSICION) ?  APARTADO_PROVISO:APARTADO_ARTICLE) + elementNumSplit[1].replace(".", ":");
					}else {
						refElement = APAR_SING + elementNumSplit[0].replace(".", "") + (elementNumSplit[1].contains(DISPOSICION) ?  APARTADO_PROVISO:APARTADO_ARTICLE) + elementNumSplit[1].replace(".", ":");
					}
					break;
				case ARTICLE:
				case PROVISO:
					cadenaNewNum = elementNumSplit[0].replace(".", "");
					refElement = (elementNumSplit[0].contains("Art")? SELEC_ARTICLE : SELEC_PROVISO) + elementNumSplit[0].replace(".", "");
					break;
				}
				refElement = refElement.replace(":", "") ;
				pos = "";
				templateGeneralFrameText = StringUtils.capitalize(refElement.replace(":", "")) +  (!isEditing?(containTypeNorm(typeNorm)?APARTADO_ARTICLE:APARTADO_PROVISO) + refNorm : "")
						+ " queda redactad" + (refElement.contains("artículo")?"a":"o") +  " en los siguientes términos: ";
			} else if (elements.size() > 1) {
				
			}			
			break;
		case "DELETE":
			if (elements.size() == 1) {
				pos = "";
				switch (elementsAux[0]) {
				case POINT:
					if (elementNumSplit.length == 3) {
						cadenaNewNum =  elementNumSplit[1].trim() + elementNumSplit[0].trim();
						refElement = APAR_SING + elementNumSplit[1].trim() + elementNumSplit[0].trim() + " " + (elementNumSplit[2].contains(DISPOSICION) ?  APARTADO_PROVISO:APARTADO_ARTICLE)  + elementNumSplit[2].replace(".", ":");
					} else if (elementNumSplit.length == 4) {
						cadenaNewNum =  elementNumSplit[2].trim() + elementNumSplit[1].trim()+ "." + elementNumSplit[0].trim();
						refElement = APAR_SING + elementNumSplit[2].trim() + elementNumSplit[1].trim() + "." + elementNumSplit[0] + (elementNumSplit[3].contains(DISPOSICION) ?  APARTADO_PROVISO:APARTADO_ARTICLE)  + elementNumSplit[3].replace(".", ":");
					}
					break;
				case PARAGRAPH:
					cadenaNewNum = elementNumSplit[0].replace(".", "");
					if(cadenaNewNum.contains("primero")) {
						refElement = APAR_SING + (elementNumSplit[1].contains(DISPOSICION) ?  APARTADO_PROVISO:APARTADO_ARTICLE) + elementNumSplit[1].replace(".", ":");
					}else {
						refElement = APAR_SING + elementNumSplit[0].replace(".", "") + (elementNumSplit[1].contains(DISPOSICION) ?  APARTADO_PROVISO:APARTADO_ARTICLE) + elementNumSplit[1].replace(".", ":");
					}
					break;
				case ARTICLE:
				case PROVISO:
					cadenaNewNum = elementNumSplit[0].replace(".", "");
					refElement = elementNumSplit[0].replace(".", "");
					break;
				}
				templateGeneralFrameText = INTRO_DELETE + refElement.replace(":", "") +  (containTypeNorm(typeNorm)?APARTADO_ARTICLE:APARTADO_PROVISO) + refNorm;
			} else if (elements.size() > 1) {
				
			}
			break;
		default:
			break;
		}
		if (elementType.equals(ARTICLE)) {
			return AGEXmlHelper.getArticleAmendmentTemplateType1(numArticleCk, templateArticleHeading, idArticleTarget, templateGeneralFrameText , amendAction.toString().equals("DELETE") ? "" :elementQuotedStructure, quotedStructureId);	
		} else {
			return AGEXmlHelper.getDispositionAmendmentTemplateType1(numArticleCk, templateArticleHeading, idArticleTarget, templateGeneralFrameText ,  amendAction.toString().equals("DELETE") ? "" :elementQuotedStructure, quotedStructureId);	
		}
		
	}
	
	private String generateQuotedInsert (String newNum, String typeElement) {
		String element = "";
		switch (typeElement) {
		case ARTICLE:
			return AGEXmlHelper.getArticleTemplate(newNum, messageHelper.getMessage("template.heading.article"), "", messageHelper, null);			
		case PROVISO:
			return AGEXmlHelper.getDispositionTemplate(null, newNum, messageHelper.getMessage("template.heading.disposition"), "", messageHelper);		
		case PARAGRAPH:
			return AGEXmlHelper.getParagraphTemplate(newNum, messageHelper);		
		case POINT:
			return AGEXmlHelper.getPointTemplate(newNum, messageHelper);		
		default:
			break;
		}
		return element;
	}

	@Override
	public String generatePreview(List<String> elementAmendmentSelect, String parentElement) {
		String label = "";
		this.parentElement = parentElement;
		if (elementAmendmentSelect.size() == 1) {
			String element = elementAmendmentSelect.get(0).replace("&nbsp;", " ");
			String[] elementsArray = element.split("--");
			elementNumSplit= elementsArray[2].split("::");
			typeSelectedElement = elementsArray[0];
			elementQuotedStructure = elementsArray[3];
			elementQuotedStructure = elementQuotedStructure.replace("id=", "xml:id=");
			elementQuotedStructure = elementQuotedStructure.replace("aknp", "p");
			elementQuotedStructure = elementQuotedStructure.replace("&nbsp;", " ");
			switch (typeSelectedElement) {
			case ARTICLE:
				refElement = SELEC_ARTICLE + elementsArray[2].replace(".", ":");
				parentNum = elementNumSplit[0].replace(".", "");
				break;
			case PROVISO:
				refElement = SELEC_PROVISO + elementsArray[2].replace(".", ":");
				parentNum = elementNumSplit[0].replace(".", "");
				break;
			case PARAGRAPH:
				if (elementNumSplit.length > 1) {
					if(elementNumSplit[0].contains("primero")) {
						refElement = APAR_SING + (elementNumSplit[1].contains(DISPOSICION) ?  APARTADO_PROVISO:APARTADO_ARTICLE) + elementNumSplit[1].replace(".", ":");

					}else {
						refElement = APAR_SING + elementNumSplit[0].replace(".", "") + (elementNumSplit[1].contains(DISPOSICION) ?  APARTADO_PROVISO:APARTADO_ARTICLE) + elementNumSplit[1].replace(".", ":");

					}
				} else {
					cadenaNewNum = "único";
					refElement =  APAR_SIN_NUM + (elementNumSplit[0].contains(DISPOSICION) ?  APARTADO_PROVISO:APARTADO_ARTICLE) + elementNumSplit[0].replace(".", ":");
				}	
				parentNum = elementNumSplit[1].replace(".", "");
				break;
			case POINT:
				if (elementNumSplit.length == 3) {
					refElement = APAR_SING + elementNumSplit[1].trim() + elementNumSplit[0].trim() + " " + (elementNumSplit[2].contains(DISPOSICION) ?  APARTADO_PROVISO:APARTADO_ARTICLE)  + elementNumSplit[2].replace(".", ":");
					parentNum = elementNumSplit[2].replace(".", "");
				} else if (elementNumSplit.length == 4) {
					refElement = APAR_SING + elementNumSplit[2].trim() + elementNumSplit[1].trim() + "." + elementNumSplit[0].trim() + " " + (elementNumSplit[3].contains(DISPOSICION) ?  APARTADO_PROVISO:APARTADO_ARTICLE)  + elementNumSplit[3].replace(".", ":");
					parentNum = elementNumSplit[3].replace(".", "");
				}
				
				break;
			case HEADING:
				if (elementsArray[4].equals(ARTICLE) || elementsArray[4].equals(BOOK) || elementsArray[4].equals(TITLE) || elementsArray[4].equals(CHAPTER)   ) {
					refElement = APARTADO_TITULO + APARTADO_ARTICLE + elementsArray[2].replace(".", ":");
				} else {
					label = APARTADO_TITULO + APARTADO_PROVISO + elementsArray[2].replace(".", ":")  + SALTO_LINEA_HTML + elementsArray[3];
					refElement = APARTADO_TITULO + APARTADO_PROVISO + elementsArray[2].replace(".", ":");
				}
				break;			
			default:
				break;
			}
		} else { // Selección múltiple

		}
		label = refElement + SALTO_LINEA_HTML + elementQuotedStructure;
		return label;
	}
	
	private boolean containTypeNorm (String type) {
		for (int i = 0; i < LIST_NORM.length; i++) {
			if (LIST_NORM[i].equals(type)) {
				return true;
			}
		}
		return false;
	}
	
	@Override 
	public String extractIdModAmendment (String element) {		
		return xmlContentProcessor.extractIdModAmendment(element);
	}
	
    @Override 
    public byte[] insertNewMetadaElementActiveRef(Bill document, String eliDestination) {
    	Validate.notNull(document, "Document is required.");
        final Content content = document.getContent().getOrError(() -> "Document content is required!");
        final byte[] contentBytes = content.getSource().getBytes();
        String template = null;
    	byte[] updatedContent = null;

    	String[] arr=xmlContentProcessor.getNormVersion(eliDestination);
    	template = xmlContentProcessor.equalActiveRefAmendment(contentBytes, arr[1], arr[0]);
    	if (template == null) {
    		return null;
    	}
        updatedContent = xmlContentProcessor.appendElementToTag(contentBytes, REFERENCES, template, true);

    	return updatedContent;
    }
    
    @Override 
    public byte[] insertNewMetadaElementAnalisis(Bill document, String idMod, String eliDestination, String actionType) {
    	Validate.notNull(document, "Document is required.");
    	
        final Content content = document.getContent().getOrError(() -> "Document content is required!");
        final byte[] contentBytes = content.getSource().getBytes();
    	byte[] updatedContent = null;
    	String[] arr=xmlContentProcessor.getNormVersion(eliDestination);
    	template = xmlContentProcessor.typeActiveMoficationAmendmentNew(contentBytes, idMod, actionType, quotedStructureId, convertRefElementMeta(arr[0]), pos, cadenaNewNum);
    	if (template[0].equals("activeModifications")) {
    		updatedContent = xmlContentProcessor.appendElementToTag(contentBytes, template[0],  template[1], false);
    	} else {
    		updatedContent = xmlContentProcessor.appendElementToTagPath(contentBytes, template[0],  template[1]);
    	}       
    	return updatedContent;
    }
    
    @Override 
    public byte[] editingMetadataElementAnalisis(Bill document, String idModNew, String quotedStructureId, String eliDestination, String actionType) {
    	Validate.notNull(document, "Document is required.");
    	
        final Content content = document.getContent().getOrError(() -> "Document content is required!");
        final byte[] contentBytes = content.getSource().getBytes();
    	byte[] updatedContent = null;
    	String[] arr=xmlContentProcessor.getNormVersion(eliDestination);
    	actionType =xmlContentProcessor.convertActionType(actionType);
    	template = xmlContentProcessor.typeActiveMoficationAmendmentMod(contentBytes, idModNew, actionType, quotedStructureId, convertRefElementMeta(arr[0]), pos, cadenaNewNum);
    	if (template != null) {
    		this.idModOld = template[1];
        	updatedContent = xmlContentProcessor.appendElementToTag(contentBytes, ACTIVEMODIFICATIONS,  template[0], false);
        	        	
    	}    	   	
    	return updatedContent;    	
    }
    
    @Override
    public byte[] deleteElementModifi(Bill document, String elementId, String quotedId, String modId, String granParentId, String elementType) {
    	final Content content = document.getContent().getOrError(() -> "Document content is required!");
        byte[] xmlContent = content.getSource().getBytes();
        
        try {
        	 String xPath = "//mod[@xml:id='" + modId + "']/quotedStructure";
             int countQuoted = xmlContentProcessor.countSeveralQuoted(xmlContent, xPath, modId);
             if (countQuoted == 1) {
             
             	xPath = "//" + elementType + "[@xml:id='" + granParentId + "']/paragraph/content/p/mod";
             	int countMod = xmlContentProcessor.countSeveralMod(xmlContent, xPath);
             	if (countMod == 1) {       		
             		xPath = "//textualMod/source[@href='" + modId + "']";
             		String href = xmlContentProcessor.getActiveRefAmendment(xmlContent, xPath);
             		String[] auxEliNew = href.split("/");
         			href = href.replace(auxEliNew[auxEliNew.length - 1], "");
         			href = href.substring(0, href.length()-1);
             		xPath = "//activeRef[@href='" + href + "']";
             		xmlContent = xmlContentProcessor.removeElements(xmlContent, xPath);
             		xPath = "//textualMod/source[@href='" + modId + "']";
             		xmlContent = xmlContentProcessor.deleteAllTextualMod(xmlContent, xPath);
             		xPath = "//" + elementType + "[@xml:id='" + granParentId + "']";
             		xmlContent = xmlContentProcessor.removeElements(xmlContent, xPath);
             		if (elementType.equals(ARTICLE)) {
             			xmlContent = numberProcessor.renumberArticles(xmlContent);
             		} else {                		
                 		xmlContent = numberProcessor.renumberProvisos(xmlContent);	
             		}
             	} else {
             		xmlContent = xmlContentProcessor.deleteElementByTagNameAndIdParentMod(xmlContent, MOD, modId);
                 	if (countMod == 2) {
                 		xPath = "//textualMod/source[@href='" + modId + "']";
                 		xmlContent = xmlContentProcessor.deleteAllTextualMod(xmlContent, xPath);
                 		xPath = "//" + elementType + "[@xml:id='" + granParentId + "']/paragraph/content/p/affectedDocument";
                 		String refNorm = xmlContentProcessor.getElementFragmentByPath(xmlContent, xPath, true);
                 		xPath = "//" + elementType + "[@xml:id='" + granParentId + "']/paragraph[1]";
                 		xmlContent = xmlContentProcessor.removeElement(xmlContent, xPath, true);
                 		xPath = "//" + elementType + "[@xml:id='" + granParentId + "']/paragraph/num";
                 		xmlContent = xmlContentProcessor.removeElement(xmlContent, xPath, true);
                 		xPath = "//" + elementType + "[@xml:id='" + granParentId + "']/paragraph/content/p/mod";
                 		String idMod = xmlContentProcessor.getIdModEditingModification(xmlContent, xPath);
                		Map<String, List<String>>   actionTypes = xmlContentProcessor.createFrameTextAmend(xmlContent, idMod);          			
            			xPath = "//textualMod/source[@href='" + idMod + "']";
            			String parentOldNum = xmlContentProcessor.getParentNumEditingModification(xmlContent, xPath);
                 		String href = xmlContentProcessor.getActiveRefAmendment(xmlContent, xPath);
                 		String[] auxEliNew = href.split("/");
            			String newFrameText = createFrameTextElement(actionTypes, refNorm, parentOldNum, auxEliNew[5], true) +
            					refNorm + FINAL;
                		xPath = "//mod[@xml:id='" + idMod + "']";
                		xmlContent = xmlContentProcessor.appendElementInsideModAmend(xmlContent, xPath, newFrameText);

                 	} else {
                 		xPath = "//textualMod/source[@href='" + modId + "']";
                 		xmlContent = xmlContentProcessor.deleteAllTextualMod(xmlContent, xPath); 
                 		xPath = "//" + elementType + "[@xml:id='" + granParentId + "']/paragraph";
                 		xmlContent= elementNumberingHelper.renumberChildrenByXPath(xPath, xmlContent, PARAGRAPH);
                 	}
             	}
             } else {
         		xPath = "//textualMod/new[@href='" + quotedId + "']";
         		xmlContent = xmlContentProcessor.deleteAllTextualMod(xmlContent, xPath);
         		xmlContent = xmlContentProcessor.deleteElementByTagNameAndId(xmlContent, QUOTEDSTRUCTURE, quotedId);
         		xmlContent = newTextoMarco(xmlContent, modId, elementType, granParentId);             
             }
        } catch (Exception e) {
        	 throw new RuntimeException("Unexpected error occurred during deletion of element", e);
		}
		return xmlContent;   	
    }
    
    private byte [] newTextoMarco (byte[] newXmlContent, String idMod, String elementType, String granParentId ) {
		Map<String, List<String>>   actionTypes = xmlContentProcessor.createFrameTextAmend(newXmlContent, idMod);
		String xPath = "//textualMod/source[@href='" + idMod + "']";
		String parentOldNum = xmlContentProcessor.getParentNumEditingModification(newXmlContent, xPath);
		String newFrameText = createFrameTextElement(actionTypes, refNorm, parentOldNum, typeNorm, false);
		xPath = "//" + elementType + "[@xml:id='" + granParentId + "']/paragraph/content/p/mod[@xml:id='" + idMod + "']";
		newXmlContent = xmlContentProcessor.deleteRefNormElementAmend(newXmlContent, xPath, newFrameText);
		return newXmlContent;
    }
    
    @Override
    public byte [] elementAmendmentSeveral (byte[] newXmlContent, String elementId, String idModNew, String quotedStructureId, String elementType, String actionType) throws Exception {    	
    	Validate.notNull(newXmlContent, "newXmlContent is required.");
    	String templateS = null;
    	if (actionType.contentEquals("DELETE")) {
    		elementQuotedStructure = "";
    	} else {
    		elementQuotedStructure = elementQuotedStructure.replace("&nbsp;", " ");
    	}
    	if (idModNew != null && template[1].equals(idModNew)){
    		String xPath = "//" + elementType + "[@xml:id='" + elementId + "']/paragraph/content/p/mod";
    		int count = xmlContentProcessor.countSeveralMod(newXmlContent, xPath);
    		if (count == 1) {
    			templateS = AGEXmlHelper.getParagraphTemplate2((containTypeNorm(typeNorm)?SELEC_ARTICLE:SELEC_PROVISO) + refNorm +
    					" queda redactad" + (containTypeNorm(typeNorm)?"o":"a") +  " en los siguientes términos: ");
    			xPath = "//" + elementType + "[@xml:id='" + elementId + "']/heading";
    			newXmlContent = xmlContentProcessor.appendElementXPathAmend(newXmlContent, xPath, templateS);
    			xPath = "//" + elementType + "[@xml:id='" + elementId + "']/paragraph/content/p/mod";
    			String idMod = xmlContentProcessor.getIdModEditingModification(newXmlContent, xPath);
    			Map<String, List<String>>   actionTypes = xmlContentProcessor.createFrameTextAmend(newXmlContent, idMod);
    			xPath = "//textualMod/source[@href='" + idMod + "']";
    			String parentOldNum = xmlContentProcessor.getParentNumEditingModification(newXmlContent, xPath);
    			String newFrameText = createFrameTextElement(actionTypes, refNorm, parentOldNum, typeNorm, false);
    			xPath = "//" + elementType + "[@xml:id='" + elementId + "']/paragraph/content/p/mod[@xml:id='" + idMod + "']";
    			newXmlContent = xmlContentProcessor.deleteRefNormElementAmend(newXmlContent, xPath, newFrameText);
    		}
    	   	if (actionType.contentEquals("DELETE")) {
    	   		templateS = AGEXmlHelper.getParagraphAmendmentTemplate(convertActionType(actionType) + (refElement.contains("el") ? "Se elimina "+ refElement.trim().toLowerCase().replace(":", "") : "Se elimina el"+ refElement.trim().toLowerCase().replace(":", ""))+ ".", elementQuotedStructure, quotedStructureId, idModNew);	
        		xPath = "//" + elementType + "[@xml:id='" + elementId + "']/paragraph[last()]";
        	}  else   if (actionType.contentEquals("REPLACE")) {
        		templateS = AGEXmlHelper.getParagraphAmendmentTemplate(convertActionType(actionType) + refElement.trim().replace(":", "").toLowerCase() +
        				", que queda redactad" + (refElement.contains("artículo")?"a":"o") +  " en los siguientes términos: ", deleteLastWhiteSpace(elementQuotedStructure), quotedStructureId, idModNew);	
        		xPath = "//" + elementType + "[@xml:id='" + elementId + "']/paragraph[last()]";
        	} else {
        		templateS = AGEXmlHelper.getParagraphAmendmentTemplate(convertActionType(actionType) + refElement.trim().replace(":", "").toLowerCase() +
        				", que queda redactad" + (refElement.contains("artículo") || refElement.contains("Disposición") ?"a":"o") +  " en los siguientes términos: ", deleteLastWhiteSpace(elementQuotedStructure), quotedStructureId, idModNew);	
        		xPath = "//" + elementType + "[@xml:id='" + elementId + "']/paragraph[last()]";
        	}
    	
    		newXmlContent = xmlContentProcessor.appendElementInsideTagByXPathAmend(newXmlContent, xPath, templateS, false);

    		xPath = xPath.replace("[last()]", "");
    		newXmlContent = elementNumberingHelper.renumberChildrenByXPath(xPath, newXmlContent, PARAGRAPH);
    	} else {
    		templateS = AGEXmlHelper.getQuotedStructureAmendmentTemplate(elementQuotedStructure, quotedStructureId);
    		String xPath = "//mod" + "[@xml:id='" + idModOld + "']";
    		newXmlContent = xmlContentProcessor.appendElementInsideTagByXPathAmend(newXmlContent, xPath, templateS, true);
    		xPath = "//" + elementType + "[@xml:id='" + elementId + "']/paragraph/content/p/mod";
    		int count = xmlContentProcessor.countSeveralMod(newXmlContent, xPath);

    		Map<String, List<String>>   actionTypes = xmlContentProcessor.createFrameTextAmend(newXmlContent, idModOld);
    		String newFrameText = "";
    		if (count > 1) {
    			newFrameText = createFrameTextElement(actionTypes, refNorm, parentNum, typeNorm, false);
    			newFrameText = newFrameText + FINAL;
    		} else {
    			newFrameText = createFrameTextElement(actionTypes, refNorm, parentNum, typeNorm, true);
    		}
    		xPath = "//mod[@xml:id='" + idModOld + "']";
    		newXmlContent = xmlContentProcessor.appendElementInsideModAmend(newXmlContent, xPath, newFrameText);

    	}
    	return newXmlContent;
    }
    
    private String convertActionType (String actionType) {
    	String inicio = "";
    	switch (actionType) {
		case "INSERT_TOP":
		case "INSERT_BOTTOM":
			inicio = "Se inserta ";
			break;
		case "REPLACE":	
			inicio = "Se modifica ";
			break;
		case "REPEAL":	
			inicio = "Se suprime ";
			break;
		default:
			break;
		}
    	return inicio;
    }
    
    private String createFrameTextElement (Map<String, List<String>> actionTypes, String refNorm, String parentNum, String typeNorm, boolean isRefNorm) {
    	String newMod = "";
    	String newText = "";
    	int cont = 0;
    	int contKey = 0;
    	Set<String> keys = actionTypes.keySet();
    	for ( String key : keys ) {
    		contKey++;
    	}   	
    	for (String clave : actionTypes.keySet()) {
    		List<String> elements = actionTypes.get(clave);
    		if (cont == 0) {
    			newMod += "Se";
    		} else {
    			if (contKey == (cont + 1)) {
    				newMod += " y se";
    			} else {
    				newMod += ", se";	
    			}  			
    		}
    		switch (clave) {
    		case "substitution":
    			if (elements.size() > 1) {
    				newMod = newMod + " modifican los apartados ";
    			} else {
    				newMod = newMod + " modifica el apartado ";
    			}			
    			break;
    		case "insertion":
    			if (elements.size() > 1) {
    				newMod = newMod + " insertan los apartados ";
    			} else {
    				newMod = newMod + " inserta el apartado ";
    			}				
    			break;
    		case "repeal":
    			if (elements.size() > 1) {
    				newMod = newMod + " eliminan los apartados ";
    			} else {
    				newMod = newMod + " elimina el apartado ";
    			}				
    			break;

    		default:
    			break;
    		}  	    
    		int x = 1;
    		for (String element : elements) {
    			newMod += element;
    			if (elements.size() == x+1) {
    				newMod += " y ";
    			}else if (elements.size() > x) {
    				newMod += ", ";
				}
    			x++;
    		}
    		cont ++;
    	}
    	if(newMod.contains(").") || newMod.contains(".º")) {
    	   	newText = newMod + ((parentNum.contains(ARTICULO)||parentNum.contains(ARTICULO.toLowerCase()))?APARTADO_ARTICLE_FRAME:APARTADO_PROVISO_FRAME) + parentNum.toLowerCase().trim() + 
        			(isRefNorm ? (containTypeNorm(typeNorm)?APARTADO_ARTICLE:APARTADO_PROVISO): "");
    	}else {
    		newText = newMod.replace(".", "") + ((parentNum.contains(ARTICULO)||parentNum.contains(ARTICULO.toLowerCase()))?APARTADO_ARTICLE_FRAME:APARTADO_PROVISO_FRAME) + parentNum.toLowerCase().trim() + 
    			(isRefNorm ? (containTypeNorm(typeNorm)?APARTADO_ARTICLE:APARTADO_PROVISO): "");
    	}
    	return newText;
    }
    
    @Override 
    public AmendActDetails parseDetails(String href) {
        Validate.isTrue(href != null, "Eli's reference should be in the format https://www.boe.es/eli/es/lo/1983/07/15/9");
        String[] arr = href.split("/");
        Validate.isTrue(arr.length > 3 , "Eli's reference should be in the format https://www.boe.es/eli/es/lo/1983/07/15/9");
        return new AmendActDetails(arr[arr.length - 5],Integer.parseInt(arr[arr.length - 4]),Integer.parseInt(arr[arr.length - 3]), Integer.parseInt(arr[arr.length - 2]), Integer.parseInt(arr[arr.length - 1]) );
    }
    
    private String generatePositionInsert (String insertType, String elementSelect, String elementParent, String typeElementSelect) {
    	return xmlContentProcessor.createNewNumInsertAmendment (insertType, elementSelect, elementParent, typeElementSelect);  	
    }
    
    @Override  
    public String convertRefElementMeta (String eli) {
    	String number="";
    	eli +=  "/" ;
    	switch (typeSelectedElement) {
    	case ARTICLE:
    	case PROVISO:
    		number = elementNumSplit[0].contains(ARTICULO) ? elementNumSplit[0].replace("Artículo ", "art_").trim(): elementNumSplit[0].replace("Disposición ", "prv_").trim();
    		number = number.contains(" ") ? number.replace(" ", "_") : number;
    		eli += number;
    		break;
    	case PARAGRAPH:
    		number = (elementNumSplit[1].contains(ARTICULO) ? elementNumSplit[1].replace("Artículo ", "art_").trim(): elementNumSplit[1].replace("Disposición ", "prv_").trim());
    		number = number.contains(" ") ? number.replace(" ", "_") : number;
    		eli += number + "par_" + elementNumSplit[0].trim();
    		break;
    	case POINT:
    		if (elementNumSplit.length == 3) {
    			number = (elementNumSplit[2].contains(ARTICULO) ? elementNumSplit[2].replace("Artículo ", "art_").trim(): elementNumSplit[2].replace("Disposición ", "prv_").trim());
    			number = number.contains(" ") ? number.replace(" ", "_") : number;
    			eli += number + "par_" + elementNumSplit[1].trim() + "pnt_" + elementNumSplit[0].trim();
    		} else {
    			number = (elementNumSplit[3].contains(ARTICULO)? elementNumSplit[3].replace("Artículo ", "art_").trim(): elementNumSplit[3].replace("Disposición ", "prv_").trim());
    			number = number.contains(" ") ? number.replace(" ", "_") : number;
    			eli += number + "par_" + elementNumSplit[2].trim() + "pnt_" + elementNumSplit[1].trim() + ".pnt_" + elementNumSplit[0].trim();
    		}
    		break;
    	case HEADING:
    		number = elementNumSplit[0].contains(ARTICULO) ? elementNumSplit[0].replace("Artículo ", "art_").trim(): elementNumSplit[0].replace("Disposición ", "prv_").trim();
    		number = number.contains(" ") ? number.replace(" ", "_") : number;
    		eli += number + "hdg";
    		break;

    	default:
    		break;
    	}
    	String aux = eli.substring(eli.length() - 1);
    	if (aux.equals(".")) {
    		eli = eli.substring(0, eli.length()-1);   		
    	}
    	return StringUtils.stripAccents(eli);
    }
    
    @Override
    public byte[] checkModifAndDelete (List<String> updatedElements, Bill bill) {
    	final Content content = bill.getContent().getOrError(() -> "Document content is required!");
    	byte[] xmlContent = content.getSource().getBytes();
    	System.out.println(new String(xmlContent));
    	String xPath = "";
    	if (updatedElements.size() > 0) {
    		int cont = 0;
    		for (String id: updatedElements) {
    			if (cont == 0) {
    				xPath = "//textualMod/source[@href='" + id + "']";
    				String href = xmlContentProcessor.getActiveRefAmendment(xmlContent, xPath);
    				if (!href.isEmpty()) {
    					String[] auxEliNew = href.split("/");
    					href = href.replace(auxEliNew[auxEliNew.length - 1], "");
    					href = href.substring(0, href.length()-1);
    					xPath = "//activeRef[@href='" + href + "']";
    					xmlContent = xmlContentProcessor.removeElements(xmlContent, xPath);
    				}
    			}
    			xPath = "//textualMod/source[@href='" + id + "']";
    			xmlContent = xmlContentProcessor.deleteAllTextualMod(xmlContent, xPath);
    			cont ++;
    		} 
    	}           		
    	return xmlContent;
    }
    
    
    @Override
    public List<String> extracIdsMod (Set<CheckinElement> updatedElements, Bill bill) {
    	final Content content = bill.getContent().getOrError(() -> "Document content is required!");
        byte[] xmlContent = content.getSource().getBytes();
        List<String> modIds = null;
        
        Iterator<CheckinElement> item = updatedElements.iterator();
        while(item.hasNext()){
        	CheckinElement element = item.next();
        	String actionType = element.getActionType().toString();
        	if (actionType.equals("DELETED")) {
        		String tagName = element.getElementTagName();
        		if (tagName.toLowerCase().equals(ARTICLE) || tagName.toLowerCase().equals(PROVISO)) {
            		String ItemId = element.getElementId();
            		String xPath = "//" + tagName.toLowerCase() + "[@xml:id='" + ItemId + "']/paragraph/content/p/mod";
            		modIds = xmlContentProcessor.getListElementIdByPath(xmlContent, xPath);
        		}
        	}
        }
    
    return modIds;
    }
    
    private static String deleteLastWhiteSpace(String text) {
    	String result = text;
    	if(StringUtils.isNotBlank(text) && text.contains("</p>")) {
    		Integer position = text.lastIndexOf("</p>");
    	
    		String deletePart = text.substring(position-1, position);
    		if(" ".equals(deletePart)) {
    			String part1 = text.substring(0,position-1);
        		String part2 = text.substring(position);
    			result = part1 + part2;
    		}
    	}else    if(StringUtils.isNotBlank(text) && text.contains("</heading>")) {
    		Integer position = text.lastIndexOf("</heading>");
    	
    		String deletePart = text.substring(position-1, position);
    		if(" ".equals(deletePart)) {
    			String part1 = text.substring(0,position-1);
        		String part2 = text.substring(position);
    			result = part1 + part2;
    		}
    	}

    	return result;
    }
}

