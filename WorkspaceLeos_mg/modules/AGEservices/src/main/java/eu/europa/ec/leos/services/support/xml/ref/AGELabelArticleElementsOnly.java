package eu.europa.ec.leos.services.support.xml.ref;

import static eu.europa.ec.leos.services.support.xml.ref.NumFormatter.isUnnumbered;

import static eu.europa.ec.leos.services.support.xml.XmlHelper.ARTICLE;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.INDENT;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.LEVEL;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.PARAGRAPH;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.POINT;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.SUBPARAGRAPH;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.SUBPOINT_LABEL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
public class AGELabelArticleElementsOnly extends LabelArticleElementsOnly {
	
    @Override
    public void process(List<TreeNode> refs, List<TreeNode> mrefCommonNodes, TreeNode sourceNode, StringBuffer label, Locale locale, boolean withAnchor) {
         TreeNode ref = refs.get(0); //first from the selected nodes
        String refType = ref.getType();//node type of the first selected node
        String documentRef = ref.getDocumentRef();
        Map<String, LabelKey> bufferLabels = new LinkedHashMap<>(); //util buffer to group by the numbers by element

        // 1. add selected node in the buffer
        if (showThisLabel(refs, mrefCommonNodes, sourceNode)) {
            bufferLabels.put(ref.getType(), new LabelKey(ref.getType(), THIS_REF, true, documentRef));
        } else {
            StringBuilder sb = createAllAnchors(refs, locale, withAnchor);
            bufferLabels.put(ref.getType(), new LabelKey(ref.getType(), sb.toString(), isUnnumbered(ref), documentRef));
        }

        //2. add rest of nodes, starting from the leaf, going up to parents until it reach Article
        while (!ARTICLE.equals(ref.getType()) && ref.getParent()!= null) {
            ref = ref.getParent();
            processOtherNodesLabel(bufferLabels, ref, mrefCommonNodes.contains(ref), locale);
        }

        // 3. build the label based on the bufferLabels
        List<String> listLabels = new ArrayList<>();
        List<String> reverseOrderedKeys = new ArrayList<>(bufferLabels.keySet());
        Collections.reverse(reverseOrderedKeys);
        /**
         * The order of the words depends if is numbered, unnumbered or the word "this". Ex:
         * - Point (1)
         * - first indent
         * - this indent
         */
        boolean articleProcessed = false;
        for (String key : reverseOrderedKeys) {
            LabelKey val = bufferLabels.get(key);
            if (val.getLabelNumber().equals(THIS_REF) && key.equals(refType)) {
                // we print "this" only for the clicked node
                listLabels.add(String.format("%s %s, ", val.getLabelNumber(), val.getLabelName()));
            } else if (!val.getLabelNumber().equals(THIS_REF) && !"".equals(val.getLabelNumber())) {
                // when we are here we are sure the node is not "this" nor with an empty number string.
                    if (key.equals(ARTICLE)) {
                        articleProcessed = true;
                        //Evolutivo #1109
                        listLabels.add(String.format("%s %s", "Article ", val.getLabelNumber()));
                        continue;
                    }
                    // in case of paragraph node we write the word "paragraph" only if the previous word "Article" is missing
                    // Otherwise we only show the number, example: Article 1(1)
                    if (key.equals(PARAGRAPH) && articleProcessed) {
                    	 //Evolutivo #1109
                    	String  r = ("[()]");
                    	Pattern p = Pattern.compile(r);
                    	Matcher matcher = p.matcher(val.getLabelNumber());

                    	if(matcher.find()){
                    		listLabels.add(String.format("%s,", val.getLabelNumber().replaceAll("[()]", ".")));	
                    	}else {
                    		listLabels.add(String.format(".%s.,", val.getLabelNumber()));	
                    	}
                    	
                        continue;
                    }
                    
                    if (key.equals(INDENT) && articleProcessed) {
                   	 //Evolutivo #1109
                       if (val.getLabelNumber().contains("<")) {
                    	   String aux = val.getLabelNumber().substring(val.getLabelNumber().indexOf("<"), val.getLabelNumber().length());
                           listLabels.add(String.format("%s %s, ", val.getLabelName(), aux));
                       } else {
                    	   listLabels.add(String.format("%s %s, ", val.getLabelName(), val.getLabelNumber()));
                       }                    
                       continue;
                   }
                    // general nodes
                    listLabels.add(String.format("%s %s, ", val.getLabelName(), val.getLabelNumber()));
                }
            }
        

        label.append(String.join("", listLabels));
    }
    
    private StringBuilder createAllAnchors(List<TreeNode> refs, Locale locale, boolean withAnchor) {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < refs.size(); i++) {
            if (i != 0 && i == refs.size() - 1) {
                sb.append(" y ");
            } else if (i > 0) {
                sb.append(", ");
            }
            sb.append(createAnchor(refs.get(i), locale, withAnchor));
        }
        return sb;
    }
    
    /* protected */
    private boolean showThisLabel(List<TreeNode> refs, List<TreeNode> mrefCommonNodes, TreeNode sourceNode) {
        final TreeNode ref = refs.get(0);
        return ref.getChildren().isEmpty()
                && refs.size() == 1
                && mrefCommonNodes.indexOf(ref) != -1
                && (sourceNode.getDepth() - 1 <= ref.getDepth());
    }

    /* protected */
    private void processOtherNodesLabel(Map<String, LabelKey> buffers, TreeNode ref, boolean isThis, Locale locale) {
        // keep the old value if is sameType as the child. Last iterated parent will add the element name.
        // In the end of iteration will have something like: Point (a)(1)(i)(ii)
        String oldnum = "";
        if (ref.getChildren().get(0).getType().equals(ref.getType())) {
            if (buffers.get(ref.getType()) != null) {
                oldnum = buffers.get(ref.getType()).getLabelNumber();
            }
        }

        String labelName;
        String labelNumber;
        if (isThis) {
            labelName = ref.getType();
            labelNumber = oldnum;
        } else {
            if (ref.getType().equals(ARTICLE)) {
                labelName = StringUtils.capitalize(ref.getType());
            } else {
                labelName = ref.getType();
            }
            labelNumber = NumFormatter.formattedNum(ref, locale) + oldnum;
        }

        buffers.put(ref.getType(),  new LabelKey(labelName, labelNumber, isUnnumbered(ref), ref.getDocumentRef()));
    }

}
