/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.usecases.document;

import static eu.europa.ec.leos.domain.cmis.LeosCategory.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Provider;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.cmis.LeosPackage;
import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.AGEReport;
import eu.europa.ec.leos.domain.cmis.document.AGECantoDorado;
import eu.europa.ec.leos.domain.cmis.document.AGEMain;
import eu.europa.ec.leos.domain.cmis.document.Bill;
import eu.europa.ec.leos.domain.cmis.document.Memorandum;
import eu.europa.ec.leos.domain.cmis.document.Proposal;
import eu.europa.ec.leos.domain.cmis.document.XmlDocument;
import eu.europa.ec.leos.domain.cmis.metadata.ProposalMetadata;
import eu.europa.ec.leos.domain.vo.DocumentVO;
import eu.europa.ec.leos.domain.vo.MetadataVO;
import eu.europa.ec.leos.services.document.AGECantoDoradoService;
import eu.europa.ec.leos.services.document.AGEMainService;
import eu.europa.ec.leos.services.document.AGEProposalService;
import eu.europa.ec.leos.services.document.AGEReportService;
import eu.europa.ec.leos.services.document.ProposalService;
import eu.europa.ec.leos.services.store.PackageService;
import eu.europa.ec.leos.services.store.TemplateService;
import io.atlassian.fugue.Option;


@Component
@Scope("prototype")
@Primary
public class AGECollectionContext extends CollectionContext{

    private static final Logger LOG = LoggerFactory.getLogger(AGECollectionContext.class);

    private final TemplateService templateService;
    private final PackageService packageService;
    private final AGEProposalService proposalService;

    private final Provider<MemorandumContext> memorandumContextProvider;
    private final Provider<AGEMainContext> mainContextProvider;
    private final Provider<BillContext> billContextProvider;
    private final Provider<AGEReportContext> reportContextProvider;
    private final Provider<AGECantoDoradoContext> cantoDoradoContextProvider;

    private final Map<LeosCategory, XmlDocument> categoryTemplateMap;

    private final Map<ContextAction, String> actionMsgMap;

    private Proposal proposal = null;
    private String purpose;
    private String versionComment;
    private String milestoneComment;

    private DocumentVO propDocument;
    private String propChildDocument;
    private String proposalComment;
    
	private String reportId;
	private String cantoDoradoId;
	private DocumentVO reportDocument;
	private String reportTemplate;
	private String mainTemplate;
	private String cantoDoradoTemplate;
	private final AGEReportService reportService;
	private final AGEMainService mainService;
	private final AGECantoDoradoService cantoDoradoService;
	private String moveDirection = null;
	
	private LeosPackage leosPackage = null;

	private static final String REPORT_TITLE_PREFIX = "Report";
	private static final String CANTODORADO_TITLE_PREFIX = "Canto_Dorado";

    AGECollectionContext(TemplateService templateService,
            PackageService packageService,
            AGEProposalService proposalService,
            AGEReportService reportService,
            AGECantoDoradoService cantoDoradoService,
            AGEMainService mainService,
            Provider<MemorandumContext> memorandumContextProvider,
            Provider<BillContext> billContextProvider,
            Provider<AGEReportContext> reportContextProvider,
            Provider<AGEMainContext> mainContextProvider,
            Provider<AGECantoDoradoContext> cantoDoradoContextProvider) {
    	
    	super(templateService, packageService, proposalService, memorandumContextProvider, billContextProvider);
    	
        this.templateService = templateService;
        this.packageService = packageService;
        this.proposalService = proposalService;
        this.reportService = reportService;
        this.mainService = mainService;
        this.cantoDoradoService = cantoDoradoService;
        this.memorandumContextProvider = memorandumContextProvider;
        this.billContextProvider = billContextProvider;
        this.reportContextProvider = reportContextProvider;
        this.mainContextProvider = mainContextProvider;
        this.cantoDoradoContextProvider = cantoDoradoContextProvider;
        this.categoryTemplateMap = new HashMap<>();
        this.actionMsgMap = new HashMap<>();
    }
    
	public void useTemplate(String name) {
        Validate.notNull(name, "Template name is required!");
        XmlDocument template = templateService.getTemplate(name);
        Validate.notNull(template, "Template not found! [name=%s]", name);

        LOG.trace("Using {} template... [id={}, name={}]", template.getCategory(), template.getId(), template.getName());
        categoryTemplateMap.put(template.getCategory(), template);
    }
	
    public void useActionMessage(ContextAction action, String actionMsg) {
        Validate.notNull(actionMsg, "Action message is required!");
        Validate.notNull(action, "Context Action not found! [name=%s]", action);

        LOG.trace("Using action message... [action={}, name={}]", action, actionMsg);
        actionMsgMap.put(action, actionMsg);
    }
    
    public void useProposal(String id) {
        Validate.notNull(id, "Proposal identifier is required!");
        LOG.trace("Using Proposal... [id={}]", id);
        proposal = proposalService.findProposal(id);
        Validate.notNull(proposal, "Proposal not found! [id=%s]", id);
    }
    
    public void usePurpose(String purpose) {
        Validate.notNull(purpose, "Proposal purpose is required!");
        LOG.trace("Using Proposal purpose... [purpose={}]", purpose);
        this.purpose = purpose;
    }
    
    public void useDocument(DocumentVO document) {
        Validate.notNull(document, "Proposal document is required!");
        propDocument = document;
    }
    
    public void useVersionComment(String comment) {
        Validate.notNull(comment, "Version comment is required!");
        this.versionComment = comment;
    }
    
    public void useMilestoneComment(String milestoneComment) {
        Validate.notNull(milestoneComment, "milestoneComment is required!");
        this.milestoneComment = milestoneComment;
    }
    
    public void useChildDocument(String documentId) {
        Validate.notNull(documentId, "Proposal child document is required!");
        propChildDocument = documentId;
    }
    
    public void useActionComment(String comment){
        Validate.notNull(comment, "Proposal comment is required!");
        proposalComment = comment;
    }
    
	public Map<ContextAction, String> getActionMsgMap() {
		return actionMsgMap;
	}
	
	public void usePackage(LeosPackage leosPackage) {
		Validate.notNull(leosPackage, "Bill package is required!");
		LOG.trace("Using Bill package... [id={}, path={}]", leosPackage.getId(), leosPackage.getPath());
		this.leosPackage = leosPackage;
	}
    
	public void useTemplate(Proposal proposal) {
		Validate.notNull(proposal, "Proposal template is required!");
		LOG.trace("Using Proposal template... [id={}, name={}]", proposal.getId(), proposal.getName());
		this.proposal = proposal;
		
	}
    
	public void useMoveDirection(String moveDirection) {
		Validate.notNull(moveDirection, "Bill 'moveDirection' is required!");
		LOG.trace("Using Bill 'move direction'... [moveDirection={}]", moveDirection);
		this.moveDirection = moveDirection;
	}
	
    public void executeImportProposal() {
        LOG.trace("Executing 'Import Proposal' use case...");
        MetadataVO propMeta = propDocument.getMetadata();
        Validate.notNull(propMeta, "Proposal metadata is required!");
        Validate.notNull(propDocument.getChildDocuments(), "Proposal must contain child documents to import!");
        // create package
        LeosPackage leosPackage = packageService.createPackage();
        // use template
        Proposal proposalTemplate = cast(categoryTemplateMap.get(PROPOSAL));
        Validate.notNull(proposalTemplate, "Proposal template is required!");

        // get metadata from template
        Option<ProposalMetadata> metadataOption = proposalTemplate.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "Proposal metadata is required!");
        purpose = propMeta.getDocPurpose();
        Validate.notNull(purpose, "Proposal purpose is required!");
        // TODO right now it's only needed the docPurpose and docRef, we will have to add more in the future.
        ProposalMetadata metadata = metadataOption.get().withPurpose(purpose);

        Validate.notNull(propDocument.getSource(), "Proposal xml is required!");
        proposal = proposalService.createProposalFromContent(leosPackage.getPath(), metadata, propDocument.getSource());

        // create child element
        for (DocumentVO docChild : propDocument.getChildDocuments()) {
            if ((docChild.getCategory() == MEMORANDUM) && (cast(categoryTemplateMap.get(MEMORANDUM)) != null)) {
                MemorandumContext memorandumContext = memorandumContextProvider.get();
                memorandumContext.usePackage(leosPackage);
                // use template
                memorandumContext.useTemplate(cast(categoryTemplateMap.get(MEMORANDUM)));
                // We want to use the same purpose that was set in the wizard for all the documents.
                memorandumContext.usePurpose(purpose);
                memorandumContext.useDocument(docChild);
                memorandumContext.useActionMessageMap(actionMsgMap);
                memorandumContext.useType(metadata.getType());
                memorandumContext.usePackageTemplate(metadata.getTemplate());
                Memorandum memorandum = memorandumContext.executeImportMemorandum();
                proposal = proposalService.addComponentRef(proposal, memorandum.getName(), LeosCategory.MEMORANDUM);
			} else if ((docChild.getCategory() == REPORT) && (cast(categoryTemplateMap.get(REPORT)) != null)) {
				AGEReportContext reportContext = reportContextProvider.get();
				reportContext.usePackage(leosPackage);
				// use template
				reportContext.useTemplate(reportTemplate);
				// We want to use the same purpose that was set in the wizzard for all the
				// documents.
				reportContext.usePurpose(purpose);
				reportContext.useDocument(docChild);
				reportContext.useActionMessageMap(actionMsgMap);
				reportContext.useType(metadata.getType());
				reportContext.usePackageTemplate(metadata.getTemplate());
				AGEReport report = reportContext.executeImportReport();
				proposal = proposalService.addComponentRef(proposal, report.getName(), LeosCategory.REPORT);
			} else if ((docChild.getCategory() == MAIN) && (cast(categoryTemplateMap.get(MAIN)) != null)) {
				AGEMainContext mainContext = mainContextProvider.get();
				mainContext.usePackage(leosPackage);
				// use template
				mainContext.useTemplate(mainTemplate);
				// We want to use the same purpose that was set in the wizzard for all the
				// documents.
				mainContext.usePurpose(purpose);
				mainContext.useDocument(docChild);
				mainContext.useActionMessageMap(actionMsgMap);
				mainContext.useType(metadata.getType());
				mainContext.usePackageTemplate(metadata.getTemplate());
				AGEMain main = mainContext.executeImportMain();
				proposal = proposalService.addComponentRef(proposal, main.getName(), LeosCategory.MAIN);
			} else if ((docChild.getCategory() == CANTODORADO) && (cast(categoryTemplateMap.get(CANTODORADO)) != null)) {
				AGECantoDoradoContext cantoDoradoContext = cantoDoradoContextProvider.get();
				cantoDoradoContext.usePackage(leosPackage);
				// use template
				cantoDoradoContext.useTemplate(reportTemplate);
				// We want to use the same purpose that was set in the wizzard for all the
				// documents.
				cantoDoradoContext.usePurpose(purpose);
				cantoDoradoContext.useDocument(docChild);
				cantoDoradoContext.useActionMessageMap(actionMsgMap);
				cantoDoradoContext.useType(metadata.getType());
				cantoDoradoContext.usePackageTemplate(metadata.getTemplate());
				AGECantoDorado cantoDorado = cantoDoradoContext.executeImporCantoDorado();
				proposal = proposalService.addComponentRef(proposal, cantoDorado.getName(), LeosCategory.CANTODORADO);
            } else if (docChild.getCategory() == BILL) {
                BillContext billContext = billContextProvider.get();
                billContext.usePackage(leosPackage);
                // use template
                billContext.useTemplate(cast(categoryTemplateMap.get(BILL)));
                billContext.usePurpose(purpose);
                billContext.useDocument(docChild);
                billContext.useActionMessageMap(actionMsgMap);
                Bill bill = billContext.executeImportBill();
                proposal = proposalService.addComponentRef(proposal, bill.getName(), LeosCategory.BILL);
            }
        }
        proposal = proposalService.createVersion(proposal.getId(), VersionType.INTERMEDIATE, actionMsgMap.get(ContextAction.DOCUMENT_CREATED));
    }
    
	/**
	 * @param list
	 *            of Reports currently added
	 * @return result if first Report is numbered or not
	 */
	private AGEReport getFirstIndexReport(List<AGEReport> reports) {
		AGEReport firstReport = null;
		for (AGEReport report : reports) {
			if (report.getMetadata().get().getIndex() == 1) {
				firstReport = report;
			}
		}
		return firstReport;
	}
	
	public  Map<LeosCategory, XmlDocument> getCategoryTemplateMap () {
		return  categoryTemplateMap;
	}
    
    
    public void executeCreateProposal() {
        LOG.trace("Executing 'Create Proposal' use case...");

        LeosPackage leosPackage = packageService.createPackage();

        Proposal proposalTemplate = cast(categoryTemplateMap.get(PROPOSAL));
        Validate.notNull(proposalTemplate, "Proposal template is required!");

        Option<ProposalMetadata> metadataOption = proposalTemplate.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "Proposal metadata is required!");

        Validate.notNull(purpose, "Proposal purpose is required!");
        ProposalMetadata metadata = metadataOption.get().withPurpose(purpose);

        Proposal proposal = proposalService.createProposal(proposalTemplate.getId(), leosPackage.getPath(), metadata, null);
        
		if (cast(categoryTemplateMap.get(REPORT)) != null) {
			Validate.notNull(purpose, "Purpose is required!");
			AGEReportContext reportContext = reportContextProvider.get();
			reportContext.usePackage(leosPackage);
			reportContext.usePurpose(purpose);
			reportContext.useTemplate(cast(categoryTemplateMap.get(REPORT).getName()));
			reportContext.useType(metadata.getType());
			reportContext.usePackageTemplate(metadata.getTemplate());
			// We dont need to fetch the content here, the executeUpdateAnnexMetadata gets
			// the latest version of the report by id
			List<AGEReport> reports = packageService.findDocumentsByPackagePath(leosPackage.getPath(), AGEReport.class,
					false);
			int reportIndex = reports.size() + 1;
			String reportNumber = AGEReportNumberGenerator.getReportNumber(reports.size() == 0 ? reports.size() : reportIndex);
			reportContext.useIndex(reportIndex);
			reportContext.useActionMessageMap(actionMsgMap);
			reportContext.useReportNumber(reportNumber);
			reportContext.useCollaborators(proposal.getCollaborators());
			AGEReport report = reportContext.executeCreateReport();
			proposal = proposalService.addComponentRef(proposal, report.getName(), LeosCategory.REPORT);

			// updating the first annex number if not done already
			AGEReport firstReport = getFirstIndexReport(reports);
			if (firstReport != null && REPORT_TITLE_PREFIX.equals(firstReport.getMetadata().get().getNumber())) {
				int firstIndex = firstReport.getMetadata().get().getIndex();
				reportContext.useReportId(firstReport.getId());
				reportContext.useIndex(firstIndex);
				reportContext.useActionMessageMap(actionMsgMap);
				String firstReportNumber = AGEReportNumberGenerator.getReportNumber(firstIndex);
				reportContext.useReportNumber(firstReportNumber);
				reportContext.executeUpdateReportIndex();
				HashMap<String, String> attachmentsElements = new HashMap<>();
				attachmentsElements.put(firstReport.getName(), firstReportNumber);
			}

		}
		
		if (cast(categoryTemplateMap.get(CANTODORADO)) != null) {
			Validate.notNull(purpose, "Purpose is required!");
			AGECantoDoradoContext cantoDoradoContext = cantoDoradoContextProvider.get();
			cantoDoradoContext.usePackage(leosPackage);
			cantoDoradoContext.usePurpose(purpose);
			cantoDoradoContext.useTemplate(cast(categoryTemplateMap.get(CANTODORADO).getName()));
			cantoDoradoContext.useType(metadata.getType());
			cantoDoradoContext.usePackageTemplate(metadata.getTemplate());
			// We dont need to fetch the content here, the executeUpdateAnnexMetadata gets
			// the latest version of the report by id
			List<AGECantoDorado> cantoDorados = packageService.findDocumentsByPackagePath(leosPackage.getPath(), AGECantoDorado.class,
					false);
			int cantoDoradoIndex = cantoDorados.size() + 1;
			String cantoDoradoNumber = AGECantoDoradoNumberGenerator.getCantoDoradoNumber(cantoDorados.size() == 0 ? cantoDorados.size() : cantoDoradoIndex);
			cantoDoradoContext.useIndex(cantoDoradoIndex);
			cantoDoradoContext.useActionMessageMap(actionMsgMap);
			cantoDoradoContext.useCantoDoradoNumber(cantoDoradoNumber);
			cantoDoradoContext.useCollaborators(proposal.getCollaborators());
			AGECantoDorado cantoDorado = cantoDoradoContext.executeCreateCantoDorado();
			proposal = proposalService.addComponentRef(proposal, cantoDorado.getName(), LeosCategory.CANTODORADO);

			// updating the first annex number if not done already
			AGECantoDorado firstCantoDorado = getFirstIndexCantoDorado(cantoDorados);
			if (firstCantoDorado != null && CANTODORADO_TITLE_PREFIX.equals(firstCantoDorado.getMetadata().get().getNumber())) {
				int firstIndex = firstCantoDorado.getMetadata().get().getIndex();
				cantoDoradoContext.useCantoDoradoId(firstCantoDorado.getId());
				cantoDoradoContext.useIndex(firstIndex);
				cantoDoradoContext.useActionMessageMap(actionMsgMap);
				String firstCantoDoradoNumber = AGECantoDoradoNumberGenerator.getCantoDoradoNumber(firstIndex);
				cantoDoradoContext.useCantoDoradoNumber(firstCantoDoradoNumber);
				cantoDoradoContext.executeUpdateCantoDoradoIndex();
				HashMap<String, String> attachmentsElements = new HashMap<>();
				attachmentsElements.put(firstCantoDorado.getName(), firstCantoDoradoNumber);
			}

		}

        // TODO: To have other structure proposal
        if (cast(categoryTemplateMap.get(MEMORANDUM)) != null) {
            MemorandumContext memorandumContext = memorandumContextProvider.get();
            memorandumContext.usePackage(leosPackage);
            memorandumContext.useTemplate(cast(categoryTemplateMap.get(MEMORANDUM)));
            memorandumContext.usePurpose(purpose);
            memorandumContext.useActionMessageMap(actionMsgMap);
            memorandumContext.useType(metadata.getType());
            memorandumContext.usePackageTemplate(metadata.getTemplate());
            Memorandum memorandum = memorandumContext.executeCreateMemorandum();
            proposal = proposalService.addComponentRef(proposal, memorandum.getName(), LeosCategory.MEMORANDUM);
        }

        if (cast(categoryTemplateMap.get(MAIN)) != null) {
            AGEMainContext mainContext = mainContextProvider.get();
            mainContext.usePackage(leosPackage);
            mainContext.useTemplate(cast(categoryTemplateMap.get(MAIN)));
            mainContext.usePurpose(purpose);
            mainContext.useActionMessageMap(actionMsgMap);
            mainContext.useType(metadata.getType());
            mainContext.usePackageTemplate(metadata.getTemplate());
            AGEMain main = mainContext.executeCreateMain();
            proposal = proposalService.addComponentRef(proposal, main.getName(), LeosCategory.MAIN);
        }
        
        BillContext billContext = billContextProvider.get();
        billContext.usePackage(leosPackage);
        billContext.useTemplate(cast(categoryTemplateMap.get(BILL)));
        billContext.usePurpose(purpose);
        billContext.useActionMessageMap(actionMsgMap);
        Bill bill = billContext.executeCreateBill();
        proposalService.addComponentRef(proposal, bill.getName(), LeosCategory.BILL);
        proposalService.createVersion(proposal.getId(), VersionType.INTERMEDIATE, actionMsgMap.get(ContextAction.DOCUMENT_CREATED));
    }
    
    public void executeUpdateProposal() {
        LOG.trace("Executing 'Update Proposal' use case...");

        Validate.notNull(proposal, "Proposal is required!");
        Validate.notNull(proposalComment, "Proposal comment is required!");

        Option<ProposalMetadata> metadataOption = proposal.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "Proposal metadata is required!");

        Validate.notNull(purpose, "Proposal purpose is required!");
        ProposalMetadata metadata = metadataOption.get().withPurpose(purpose);

        proposal = proposalService.updateProposal(proposal, metadata, VersionType.MINOR, proposalComment);

        LeosPackage leosPackage = packageService.findPackageByDocumentId(proposal.getId());

        MemorandumContext memorandumContext = memorandumContextProvider.get();
        memorandumContext.usePackage(leosPackage);
        memorandumContext.usePurpose(purpose);
        memorandumContext.useActionMessageMap(actionMsgMap);
        memorandumContext.executeUpdateMemorandum();
        
        //Reports
		List<AGEReport> reports = packageService.findDocumentsByPackagePath(leosPackage.getPath(), AGEReport.class,
				false);
		reports.forEach(report -> {
			AGEReportContext reportContext = reportContextProvider.get();
			reportContext.usePurpose(purpose);
			reportContext.useReportId(report.getId());
			reportContext.useActionMessageMap(actionMsgMap);
			reportContext.executeUpdateReportMetadata();
		});
		
		List<AGECantoDorado> cantoDorados = packageService.findDocumentsByPackagePath(leosPackage.getPath(), AGECantoDorado.class,
				false);
		cantoDorados.forEach(cantoDorado -> {
			AGECantoDoradoContext cantoDoradoContext = cantoDoradoContextProvider.get();
			cantoDoradoContext.usePurpose(purpose);
			cantoDoradoContext.useCantoDoradoId(cantoDorado.getId());
			cantoDoradoContext.useActionMessageMap(actionMsgMap);
			cantoDoradoContext.executeUpdateCantoDoradoMetadata();
		});
		
		//MAIN
		
        AGEMainContext mainContext = mainContextProvider.get();
        mainContext.usePackage(leosPackage);
        mainContext.usePurpose(purpose);
        mainContext.useActionMessageMap(actionMsgMap);
        mainContext.executeUpdateMain();

        BillContext billContext = billContextProvider.get();
        billContext.usePackage(leosPackage);
        billContext.usePurpose(purpose);
        billContext.useActionMessageMap(actionMsgMap);
        billContext.executeUpdateBill();
    }
    
    public void executeDeleteProposal() {
        LOG.trace("Executing 'Delete Proposal' use case...");

        LeosPackage leosPackage = packageService.findPackageByDocumentId(proposal.getId());

        packageService.deletePackage(leosPackage);
    }
    
    public void executeUpdateProposalAsync() {
        Validate.notNull(propChildDocument, "Proposal child document is required!");
        proposalService.updateProposalAsync(propChildDocument, proposalComment);
    }
    
    /**
     * Given a proposalId, fetch all documents related from CMIS and update them to the next major version.
     * The document version is not updated if it is already a major version.
     */
    public void executeCreateMilestone() {
        LOG.info("Creating major versions for all documents of [proposal={}", proposal.getId());

        // 1. Proposal
        List<String> milestoneComments = proposal.getMilestoneComments();
        milestoneComments.add(milestoneComment);
        if (proposal.getVersionType().equals(VersionType.MAJOR)) {
            proposal = proposalService.updateProposalWithMilestoneComments(proposal.getId(), milestoneComments);
            LOG.info("Major version {} already present. Updated only milestoneComment for [proposal={}]", proposal.getVersionLabel(), proposal.getId());
        } else {
            proposal = proposalService.updateProposalWithMilestoneComments(proposal, milestoneComments, VersionType.MAJOR, versionComment);
            LOG.info("Created major version {} for [proposal={}]", proposal.getVersionLabel(), proposal.getId());
        }

        // Update the last structure
        final LeosPackage leosPackage = packageService.findPackageByDocumentId(proposal.getId());

        // 2. Memorandum
        final MemorandumContext memorandumContext = memorandumContextProvider.get();
        memorandumContext.usePackage(leosPackage);
        memorandumContext.useVersionComment(versionComment);
        memorandumContext.useMilestoneComment(milestoneComment);
        memorandumContext.executeCreateMilestone();
        
		// Reports
		final List<AGEReport> reports = packageService.findDocumentsByPackagePath(leosPackage.getPath(), AGEReport.class,
				false);
		reports.forEach(report -> {
			AGEReportContext reportContext = reportContextProvider.get();
			reportContext.useReportId(report.getId());
			reportContext.useVersionComment(versionComment);
			reportContext.useMilestoneComment(milestoneComment);
			reportContext.executeCreateMilestone();
		});

        // 2. Bill + Annexes
        final BillContext billContext = billContextProvider.get();
        billContext.usePackage(leosPackage);
        billContext.useVersionComment(versionComment);
        billContext.useMilestoneComment(milestoneComment);
        billContext.executeCreateMilestone();
    }
    
    public String getUpdatedProposalId() {
        return proposal.getId();
    }
    
	@SuppressWarnings("unchecked")
	public static <T> T cast(Object obj) {
		return (T) obj;
	}

	// Reports methods
	public void useReport(String reportId) {
		Validate.notNull(reportId, "Bill 'reportId' is required!");
		LOG.trace("Using Bill 'move direction'... [reportId={}]", reportId);
		this.reportId = reportId;
	}
	
	public void useCantoDorado(String cantoDoradoId) {
		Validate.notNull(cantoDoradoId, "Bill 'cantoDoradoId' is required!");
		LOG.trace("Using Bill 'move direction'... [reportId={}]", reportId);
		this.cantoDoradoId = cantoDoradoId;
	}

	public void useReportDocument(DocumentVO document) {
		Validate.notNull(document, "Report document is required!");
		reportDocument = document;
	}

	public void useReportTemplate(String templateName) {
		Validate.notNull(templateName, "Report template is required!");
		reportTemplate = templateName;
	}

	public void useMainTemplate(String templateName) {
		Validate.notNull(templateName, "Main template is required!");
		mainTemplate = templateName;
	}	
	
	public void executeRemoveProposalReport() {
		LOG.trace("Executing 'Remove Proposal Report' use case...");
		Validate.notNull(leosPackage, "Proposal package is required!");
		Proposal proposal = proposalService.findProposalByPackagePath(leosPackage.getPath());

		AGEReport deletedReport = reportService.findReport(reportId);
		int currentIndex = deletedReport.getMetadata().get().getIndex();

		reportService.deleteReport(deletedReport);

		String href = deletedReport.getName();
		proposalService.removeComponentRef(proposal, href, LeosCategory.REPORT);		

		// Renumber remaining reports
		List<AGEReport> reports = packageService.findDocumentsByPackagePath(leosPackage.getPath(), AGEReport.class, false);
		HashMap<String, String> attachments = new HashMap<>();
		reports.forEach(report -> {
			int index = report.getMetadata().get().getIndex();
			if (index > currentIndex || reports.size() == 1) {
				AGEReportContext affectedReportContext = reportContextProvider.get();
				affectedReportContext.useReportId(report.getId());
				affectedReportContext.useIndex(index == 1 ? index : index - 1);
				affectedReportContext.useActionMessageMap(actionMsgMap);
				String affectedReportNumber = AGEReportNumberGenerator
						.getReportNumber(reports.size() == 1 ? 0 : index - 1);
				affectedReportContext.useReportNumber(affectedReportNumber);
				affectedReportContext.executeUpdateReportIndex();
				attachments.put(report.getName(), affectedReportNumber);
			}
		});

		// HAY QUE ACTUALIZAR LOS REPORTS EN EL PROPOSAL, AHORA SOLO ELIMINA PERO NO ACTUALIZA LOS QUE QUEDEN		
		 proposalService.updateComponentRef(proposal, attachments,LeosCategory.REPORT);
	}
	
	public void executeRemoveProposalCantoDorado() {
		LOG.trace("Executing 'Remove Proposal Canto Dorado' use case...");
		Validate.notNull(leosPackage, "Proposal package is required!");
		Proposal proposal = proposalService.findProposalByPackagePath(leosPackage.getPath());

		AGECantoDorado deletedCantoDorado = cantoDoradoService.findCantoDorado(cantoDoradoId);
		int currentIndex = deletedCantoDorado.getMetadata().get().getIndex();

		cantoDoradoService.deleteCantoDorado(deletedCantoDorado);

		String href = deletedCantoDorado.getName();
		proposalService.removeComponentRef(proposal, href, LeosCategory.CANTODORADO);		

		// Renumber remaining reports
		List<AGECantoDorado> cantoDorados = packageService.findDocumentsByPackagePath(leosPackage.getPath(), AGECantoDorado.class, false);
		HashMap<String, String> attachments = new HashMap<>();
		cantoDorados.forEach(cantoDorado -> {
			int index = cantoDorado.getMetadata().get().getIndex();
			if (index > currentIndex || cantoDorados.size() == 1) {
				AGECantoDoradoContext affectedCantoDoradoContext = cantoDoradoContextProvider.get();
				affectedCantoDoradoContext.useCantoDoradoId(cantoDorado.getId());
				affectedCantoDoradoContext.useIndex(index == 1 ? index : index - 1);
				affectedCantoDoradoContext.useActionMessageMap(actionMsgMap);
				String affectedCantoDoradoNumber = AGECantoDoradoNumberGenerator
						.getCantoDoradoNumber(cantoDorados.size() == 1 ? 0 : index - 1);
				affectedCantoDoradoContext.useCantoDoradoNumber(affectedCantoDoradoNumber);
				affectedCantoDoradoContext.executeUpdateCantoDoradoIndex();
				attachments.put(cantoDorado.getName(), affectedCantoDoradoNumber);
			}
		});	
		 proposalService.updateComponentRef(proposal, attachments,LeosCategory.CANTODORADO);
	}

	public void executeImportProposalReport() {
		final LeosPackage leosPackage = packageService.findPackageByDocumentId(proposal.getId());
		AGEReportContext reportContext = reportContextProvider.get();
		reportContext.usePackage(leosPackage);

		Proposal proposal = proposalService.findProposalByPackagePath(leosPackage.getPath());

		Option<ProposalMetadata> metadataOption = proposal.getMetadata();
		Validate.isTrue(metadataOption.isDefined(), "Proposal metadata is required!");
		ProposalMetadata metadata = metadataOption.get();
		reportContext.usePurpose(metadata.getPurpose());
		reportContext.useType(metadata.getType());
		reportContext.usePackageTemplate(metadata.getTemplate());

		MetadataVO reportMeta = reportDocument.getMetadata();
		AGEReport report = reportService.findReport(reportId);
		reportContext.useTemplate(reportMeta.getDocTemplate());
		int reportIndex;
		if (reportMeta.getIndex() != null) {
			reportIndex = Integer.parseInt(reportMeta.getIndex());
		} else {
			// We dont need to fetch the content here, the executeUpdateAnnexMetadata gets
			// the latest version of the annex by id
			List<AGEReport> reports = packageService.findDocumentsByPackagePath(leosPackage.getPath(), AGEReport.class,
					false);
			reportIndex = reports.size() + 1;
		}
		reportContext.useIndex(reportIndex);
		reportContext.useCollaborators(proposal.getCollaborators());
		reportContext.useDocument(reportDocument);
		reportContext.useActionMessageMap(actionMsgMap);
		reportContext.useReportNumber(reportMeta.getNumber());
		AGEReport report1 = reportContext.executeImportReport();

		String href = report1.getName();
		String showAs = reportMeta.getNumber(); // createdReport.getMetadata().get().getNumber(); //ShowAs attribute is
												// not used so it is kept as blank as of now.
			
		proposalService.addComponentRef(proposal, href, LeosCategory.REPORT);
		// billService.addAttachment(proposal, href, showAs,
		// actionMsgMap.get(AGEContextAction.REPORT_ADDED));
	}

	public void executeMoveReport() {	
		LOG.trace("Executing 'Update Proposal Move Report' use case...");
		Validate.notNull(moveDirection, "Proposal moveDirection is required");
		Proposal proposal = proposalService.findProposalByPackagePath(leosPackage.getPath());
		AGEReport operatedReport = reportService.findReport(reportId);
		int currentIndex = operatedReport.getMetadata().get().getIndex();
		AGEReport affectedReport = findAffectedReport(moveDirection.equalsIgnoreCase("UP"), currentIndex);

		AGEReportContext operatedReportContext = reportContextProvider.get();
		operatedReportContext.useReportId(operatedReport.getId());
		operatedReportContext.useIndex(affectedReport.getMetadata().get().getIndex());
		operatedReportContext.useActionMessageMap(actionMsgMap);
		String operatedReportNumber = AGEReportNumberGenerator
				.getReportNumber(affectedReport.getMetadata().get().getIndex());
		operatedReportContext.useReportNumber(operatedReportNumber);
		operatedReportContext.executeUpdateReportIndex();

		AGEReportContext affectedReportContext = reportContextProvider.get();
		affectedReportContext.useReportId(affectedReport.getId());
		affectedReportContext.useIndex(currentIndex);
		affectedReportContext.useActionMessageMap(actionMsgMap);
		String affectedReportNumber = AGEReportNumberGenerator.getReportNumber(currentIndex);
		affectedReportContext.useReportNumber(affectedReportNumber);
		affectedReportContext.executeUpdateReportIndex();

		// Update proposal xml FALTA ACTUALIZAR PROPOSAL, METODO EQUIVALENTE A UPDATEATTACHMENTS
		 HashMap<String, String> attachments = new HashMap<>();
		 attachments.put(operatedReport.getName(), operatedReportNumber);
		 attachments.put(affectedReport.getName(), affectedReportNumber);
		
		 proposalService.updateComponentRef(proposal, attachments,LeosCategory.REPORT);
	}

	private AGEReport findAffectedReport(boolean before, int index) {		
		// We dont need to fetch the content here, the executeUpdateReportMetadata gets
		// the latest version of the annex by id
		List<AGEReport> reports = packageService.findDocumentsByPackagePath(leosPackage.getPath(), AGEReport.class, false);
		int targetIndex = index + (before ? -1 : 1); // index start with 0
		if (targetIndex < 0 || targetIndex > reports.size()) {
			throw new UnsupportedOperationException("Invalid index requested");
		}

		for (AGEReport report : reports) {// assuming unsorted report list
			report = reportService.findReport(report.getId());
			if (report.getMetadata().get().getIndex() == targetIndex) {
				return report;
			}
		}
		throw new UnsupportedOperationException("Invalid index for report");
	}

	public void executeCreateProposalReport() {
		LOG.trace("Executing 'Create Proposal Report' use case...");

		Validate.notNull(leosPackage, "Proposal package is required!");
		Validate.notNull(proposal, "Proposal is required!");
		Validate.notNull(purpose, "Purpose is required!");
		AGEReportContext reportContext = reportContextProvider.get();
		reportContext.usePackage(leosPackage);
		reportContext.usePurpose(purpose);
		reportContext.useTemplate(reportTemplate);
		// we are using the same template for the annexes for sj-23 and sj19, the only
		// change is this type. that's why we get it form the bill.
		Option<ProposalMetadata> metadataOption = proposal.getMetadata();
		Validate.isTrue(metadataOption.isDefined(), "Proposal metadata is required!");
		ProposalMetadata metadata = metadataOption.get();
		reportContext.useType(metadata.getType());
		reportContext.usePackageTemplate(metadata.getTemplate());
		// We dont need to fetch the content here, the executeUpdateAnnexMetadata gets
		// the latest version of the annex by id
		List<AGEReport> reports = packageService.findDocumentsByPackagePath(leosPackage.getPath(), AGEReport.class, false);
		int reportIndex = reports.size() + 1;
		String reportNumber = AGEReportNumberGenerator.getReportNumber(reports.size() == 0 ? reports.size() : reportIndex);
		reportContext.useIndex(reportIndex);
		reportContext.useCollaborators(proposal.getCollaborators());
		reportContext.useActionMessageMap(actionMsgMap);
		reportContext.useReportNumber(reportNumber);
		AGEReport report = reportContext.executeCreateReport();
		this.reportId = report.getId();		

		String href = report.getName();
		String showAs = reportNumber; // createdReport.getMetadata().get().getNumber(); //ShowAs attribute is not used
										// so it is kept as blank as of now.
		proposal = proposalService.addComponentRef(proposal, href, LeosCategory.REPORT);

		// updating the first annex number if not done already
		AGEReport firstReport = getFirstIndexReport(reports);
		if (firstReport != null && REPORT_TITLE_PREFIX.equals(firstReport.getMetadata().get().getNumber())) {
			int firstIndex = firstReport.getMetadata().get().getIndex();
			reportContext.useReportId(firstReport.getId());
			reportContext.useIndex(firstIndex);
			reportContext.useCollaborators(proposal.getCollaborators());
			reportContext.useActionMessageMap(actionMsgMap);
			String firstReportNumber = AGEReportNumberGenerator.getReportNumber(firstIndex);
			reportContext.useReportNumber(firstReportNumber);
			reportContext.executeUpdateReportIndex();
			HashMap<String, String> attachmentsElements = new HashMap<>();
			attachmentsElements.put(firstReport.getName(), firstReportNumber);
			// FALTA ACTUALIZAR EL PROPOSAL
			proposalService.updateComponentRef(proposal, attachmentsElements,LeosCategory.REPORT);
		}
	}
	
	public void executeCreateProposalCantoDorado() {
		LOG.trace("Executing 'Create Proposal Canto Dorado' use case...");

		Validate.notNull(leosPackage, "Proposal package is required!");
		Validate.notNull(proposal, "Proposal is required!");
		Validate.notNull(purpose, "Purpose is required!");
		AGECantoDoradoContext cantoDoradoContext = cantoDoradoContextProvider.get();
		cantoDoradoContext.usePackage(leosPackage);
		cantoDoradoContext.usePurpose(purpose);
		cantoDoradoContext.useTemplate(cantoDoradoTemplate);
		// we are using the same template for the annexes for sj-23 and sj19, the only
		// change is this type. that's why we get it form the bill.
		Option<ProposalMetadata> metadataOption = proposal.getMetadata();
		Validate.isTrue(metadataOption.isDefined(), "Proposal metadata is required!");
		ProposalMetadata metadata = metadataOption.get();
		cantoDoradoContext.useType(metadata.getType());
		cantoDoradoContext.usePackageTemplate(metadata.getTemplate());
		// We dont need to fetch the content here, the executeUpdateAnnexMetadata gets
		// the latest version of the annex by id
		List<AGECantoDorado> cantoDorados = packageService.findDocumentsByPackagePath(leosPackage.getPath(), AGECantoDorado.class, false);
		int cantoDoradotIndex = cantoDorados.size() + 1;
		String cantoDoradoNumber = AGECantoDoradoNumberGenerator.getCantoDoradoNumber(!cantoDorados.isEmpty() ? cantoDorados.size() : cantoDoradotIndex);
		cantoDoradoContext.useIndex(cantoDoradotIndex);
		cantoDoradoContext.useCollaborators(proposal.getCollaborators());
		cantoDoradoContext.useActionMessageMap(actionMsgMap);
		cantoDoradoContext.useCantoDoradoNumber(cantoDoradoNumber);
		
		AGECantoDorado cantoDorado = cantoDoradoContext.executeCreateCantoDorado();
		this.cantoDoradoId = cantoDorado.getId();		

		String href = cantoDorado.getName();
		//String showAs = cantoDoradoNumber; 
		proposal = proposalService.addComponentRef(proposal, href, LeosCategory.CANTODORADO);

		// updating the first canto dorado number if not done already
		AGECantoDorado firstCantoDorado = getFirstIndexCantoDorado(cantoDorados);
		if (firstCantoDorado != null && CANTODORADO_TITLE_PREFIX.equals(firstCantoDorado.getMetadata().get().getNumber())) {
			int firstIndex = firstCantoDorado.getMetadata().get().getIndex();
			cantoDoradoContext.useCantoDoradoId(firstCantoDorado.getId());
			cantoDoradoContext.useIndex(firstIndex);
			cantoDoradoContext.useCollaborators(proposal.getCollaborators());
			cantoDoradoContext.useActionMessageMap(actionMsgMap);
			String firstCantoDoradoNumber = AGECantoDoradoNumberGenerator.getCantoDoradoNumber(firstIndex);
			cantoDoradoContext.useCantoDoradoNumber(firstCantoDoradoNumber);
			cantoDoradoContext.executeUpdateCantoDoradoIndex();
			HashMap<String, String> attachmentsElements = new HashMap<>();
			attachmentsElements.put(firstCantoDorado.getName(), firstCantoDoradoNumber);
			// FALTA ACTUALIZAR EL PROPOSAL
			proposalService.updateComponentRef(proposal, attachmentsElements,LeosCategory.CANTODORADO);
		}
	}
	
	/**
	 * @param list
	 *            of CantoDorado currently added
	 * @return result if first CantoDorado is numbered or not
	 */
	private AGECantoDorado getFirstIndexCantoDorado(List<AGECantoDorado> cantoDorados) {
		AGECantoDorado firstCantoDorado= null;
		for (AGECantoDorado cantoDorado : cantoDorados) {
			if (cantoDorado.getMetadata().get().getIndex() == 1) {
				firstCantoDorado = cantoDorado;
			}
		}
		return firstCantoDorado;
	}
	
	public void useCantoDoradoTemplate(String templateName) {
		Validate.notNull(templateName, "Canto Dorado template is required!");
		cantoDoradoTemplate = templateName;
	}

}
