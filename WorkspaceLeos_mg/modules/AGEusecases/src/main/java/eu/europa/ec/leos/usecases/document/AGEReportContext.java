/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.usecases.document;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import eu.europa.ec.leos.domain.cmis.Content;
import eu.europa.ec.leos.domain.cmis.LeosPackage;
import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.AGEReport;
import eu.europa.ec.leos.domain.cmis.document.Annex;
import eu.europa.ec.leos.domain.cmis.metadata.AGEReportMetadata;
import eu.europa.ec.leos.domain.cmis.metadata.AnnexMetadata;
import eu.europa.ec.leos.domain.vo.DocumentVO;
import eu.europa.ec.leos.domain.vo.MetadataVO;
import eu.europa.ec.leos.services.document.AGEReportService;
import eu.europa.ec.leos.services.document.SecurityService;
import eu.europa.ec.leos.services.store.TemplateService;
import io.atlassian.fugue.Option;

@Component
@Scope("prototype")
public class AGEReportContext {

    private static final Logger LOG = LoggerFactory.getLogger(AGEReportContext.class);

    private final AGEReportService reportService;
    private final SecurityService securityService;
    private final TemplateService  templateService;

    private LeosPackage leosPackage;
    private AGEReport report = null;
    private int index;
    private String purpose = null;
    private String type = null;
    private String template = null;
    private String reportId = null;
    private Map<String, String> collaborators = null;

    private DocumentVO reportDocument;
    private final Map<ContextAction, String> actionMsgMap;
    private String reportNumber;
    private String versionComment;
    private String milestoneComment;

    public AGEReportContext(
            AGEReportService reportService,
            SecurityService securityService,
            TemplateService templateService) {
        this.reportService = reportService;
        this.securityService = securityService;
        this.templateService = templateService;
        this.actionMsgMap = new HashMap<>();
    }

    public void useTemplate(String name) {
        Validate.notNull(name, "Template name is required!");
        
        this.report = (AGEReport) templateService.getTemplate(name);
        Validate.notNull(report, "Template not found! [name=%s]", name);

        LOG.trace("Using {} template... [id={}, name={}]", report.getCategory(), report.getId(), report.getName());
    }
    
//    public void useTemplate(Report report) {
//        Validate.notNull(report, "Report template is required!");
//        LOG.trace("Using Report template... [id={}, name={}]", report.getId(), report.getName());
//        this.report = report;
//    }

    public void useActionMessageMap(Map<ContextAction, String> messages) {
        Validate.notNull(messages, "Action message map is required!");

        actionMsgMap.putAll(messages);
    }

    public void usePackage(LeosPackage leosPackage) {
        Validate.notNull(leosPackage, "Report package is required!");
        LOG.trace("Using Report package... [id={}, path={}]", leosPackage.getId(), leosPackage.getPath());
        this.leosPackage = leosPackage;
    }

    public void usePurpose(String purpose) {
        Validate.notNull(purpose, "Report purpose is required!");
        LOG.trace("Using Report purpose... [purpose={}]", purpose);
        this.purpose = purpose;
    }
    
    public void useType(String type) {
        Validate.notNull(type, "Report type is required!");
        LOG.trace("Using Report type... [type={}]", type);
        this.type = type;
    }

    public void usePackageTemplate(String template) {
        Validate.notNull(template, "template is required!");
        LOG.trace("Using template... [template={}]", template);
        this.template = template;
    }
    
    public void useIndex(int index) {
        Validate.notNull(index, "Report index is required!");
        LOG.trace("Using Report index... [index={}]", index);
        this.index = index;
    }

    public void useReportId(String reportId) {
        Validate.notNull(reportId, "Report 'reportId' is required!");
        LOG.trace("Using ReportId'... [reportId={}]", reportId);
        this.reportId = reportId;
    }

    public void useDocument(DocumentVO document) {
        Validate.notNull(document, "Report document is required!");
        reportDocument = document;
    }

    public void useCollaborators(Map<String, String> collaborators) {
        Validate.notNull(collaborators, "Report 'collaborators' are required!");
        LOG.trace("Using collaborators'... [collaborators={}]", collaborators);
        this.collaborators = Collections.unmodifiableMap(collaborators);
    }

    public void useReportNumber(String reportNumber) {
        Validate.notNull(reportNumber, "Report Number is required!");
        this.reportNumber = reportNumber;
    }
    
    public void useVersionComment(String comment) {
        Validate.notNull(comment, "Version comment is required!");
        this.versionComment = comment;
    }

    public void useMilestoneComment(String milestoneComment) {
        Validate.notNull(milestoneComment, "milestoneComment is required!");
        this.milestoneComment = milestoneComment;
    }


    public AGEReport executeCreateReport() {
        LOG.trace("Executing 'Create Report' use case...");

        Validate.notNull(leosPackage, "Report package is required!");
        Validate.notNull(report, "Report template is required!");
        Validate.notNull(collaborators, "Report collaborators are required!");
        Validate.notNull(reportNumber, "Report number is required");

        Option<AGEReportMetadata> metadataOption = report.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "Report metadata is required!");
        
        
        Validate.notNull(purpose, "Report purpose is required!");
        Validate.notNull(type, "Report type is required!");
        AGEReportMetadata metadata = metadataOption.get().withPurpose(purpose).withIndex(index).withNumber(reportNumber).withType(type).withTemplate(template);
        report = reportService.createReport(report.getId(), leosPackage.getPath(), metadata, actionMsgMap.get(ContextAction.REPORT_METADATA_UPDATED), null);
        report = securityService.updateCollaborators(report.getId(), collaborators, AGEReport.class);

        return report;
    }
    
    public void useActionMessage(ContextAction action, String actionMsg) {
        Validate.notNull(actionMsg, "Action message is required!");
        Validate.notNull(action, "Context Action not found! [name=%s]", action);

        LOG.trace("Using action message... [action={}, name={}]", action, actionMsg);
        actionMsgMap.put(action, actionMsg);
    }
    

    public void executeUpdateReportStructure() {
        byte[] xmlContent = getContent(report); //Use the content from template
        report = reportService.findReport(reportId); //Get the existing annex document
        
        Option<AGEReportMetadata> metadataOption = report.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "Report metadata is required!");
        AGEReportMetadata metadata = metadataOption.get();
        AGEReportMetadata reportMetadata = metadata.withPurpose(metadata.getPurpose()).
                    withType(metadata.getType()).withTitle(metadata.getTitle()).withTemplate(template).
                    withDocVersion(metadata.getDocVersion()).withDocTemplate(template);
        
        report = reportService.updateReportWithMetadata(report, xmlContent, reportMetadata, VersionType.INTERMEDIATE, actionMsgMap.get(ContextAction.REPORT_STRUCTURE_UPDATED));
    }
    
    private byte[] getContent(AGEReport report) {
        final Content content = report.getContent().getOrError(() -> "Annex content is required!");
        return content.getSource().getBytes();
    }
    
    public void executeUpdateReport() {
        LOG.trace("Executing 'Update Report' use case...");

        Validate.notNull(leosPackage, "Report package is required!");
        AGEReport report = reportService.findReportByPackagePath(leosPackage.getPath());

        Option<AGEReportMetadata> metadataOption = report.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "Report metadata is required!");

        Validate.notNull(purpose, "Report purpose is required!");
        AGEReportMetadata metadata = metadataOption.get().withPurpose(purpose);

        reportService.updateReport(report, metadata, VersionType.INTERMEDIATE, actionMsgMap.get(ContextAction.METADATA_UPDATED));
    }

    public AGEReport executeImportReport() {
        LOG.trace("Executing 'Import Report' use case...");

        Validate.notNull(leosPackage, "Report package is required!");
        Validate.notNull(report, "Report template is required!");
        Validate.notNull(collaborators, "Report collaborators are required!");
        Validate.notNull(reportNumber, "Report number is required");

        Option<AGEReportMetadata> metadataOption = report.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "Report metadata is required!");

        MetadataVO reportMeta = reportDocument.getMetadata();

        Validate.notNull(purpose, "Report purpose is required!");
        Validate.notNull(type, "Report type is required!");
        AGEReportMetadata metadata = metadataOption.get().withPurpose(purpose).withIndex(index).withNumber(reportNumber).withTitle(reportMeta.getTitle()).withType(type).withTemplate(template);

        report = reportService.createReportFromContent(leosPackage.getPath(), metadata, actionMsgMap.get(ContextAction.REPORT_BLOCK_UPDATED),
                reportDocument.getSource());
        report = securityService.updateCollaborators(report.getId(), collaborators, AGEReport.class);

        return report;
    }

    public void executeUpdateReportMetadata() {
        LOG.trace("Executing 'Update report metadata' use case...");
        Validate.notNull(purpose, "Report purpose is required!");
        Validate.notNull(reportId, "Report id is required!");

        report = reportService.findReport(reportId);
        Option<AGEReportMetadata> metadataOption = report.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "Report metadata is required!");

        // Updating only purpose at this time. other metadata needs to be set, if needed
        AGEReportMetadata reportMetadata = metadataOption.get().withPurpose(purpose);
        reportService.updateReport(report, reportMetadata, VersionType.INTERMEDIATE, actionMsgMap.get(ContextAction.METADATA_UPDATED));
    }

    public void executeUpdateReportIndex() {
        LOG.trace("Executing 'Update report index' use case...");
        Validate.notNull(reportId, "Report id is required!");
        Validate.notNull(index, "Report index is required!");
        Validate.notNull(reportNumber, "Report number is required");
        Validate.notNull(actionMsgMap, "Action Map is required");
        report = reportService.findReport(reportId);
        Option<AGEReportMetadata> metadataOption = report.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "Report metadata is required!");
        AGEReportMetadata metadata = metadataOption.get();
        AGEReportMetadata reportMetadata = metadata.withIndex(index).withNumber(reportNumber);
        report = reportService.updateReport(report, reportMetadata, VersionType.INTERMEDIATE, actionMsgMap.get(ContextAction.REPORT_METADATA_UPDATED));
    }
    


    public void executeCreateMilestone() {
    	report = reportService.findReport(reportId);
    	if (report != null) {
    		List<String> milestoneComments = report.getMilestoneComments();
    		milestoneComments.add(milestoneComment);
    		if (report.getVersionType().equals(VersionType.MAJOR)) {
    			report = reportService.updateReportWithMilestoneComments(report.getId(), milestoneComments);
    			LOG.info("Major version {} already present. Updated only milestoneComment for [report={}]", report.getVersionLabel(), report.getId());
    		} else {
    			report = reportService.updateReportWithMilestoneComments(report, milestoneComments, VersionType.MAJOR, versionComment);
    			LOG.info("Created major version {} for [report={}]", report.getVersionLabel(), report.getId());
    		}	
    	}

    }

    public String getUpdatedReportId() {
        return report.getId();
    }
   
}
