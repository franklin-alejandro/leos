/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.usecases.document;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import eu.europa.ec.leos.domain.cmis.Content;
import eu.europa.ec.leos.domain.cmis.LeosPackage;
import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.AGECantoDorado;
import eu.europa.ec.leos.domain.cmis.metadata.AGECantoDoradoMetadata;
import eu.europa.ec.leos.domain.vo.DocumentVO;
import eu.europa.ec.leos.domain.vo.MetadataVO;
import eu.europa.ec.leos.services.document.AGECantoDoradoService;
import eu.europa.ec.leos.services.document.SecurityService;
import eu.europa.ec.leos.services.store.TemplateService;
import io.atlassian.fugue.Option;

@Component
@Scope("prototype")
public class AGECantoDoradoContext {

    private static final Logger LOG = LoggerFactory.getLogger(AGECantoDoradoContext.class);

    private final AGECantoDoradoService cantoDoradoService;
    private final SecurityService securityService;
    private final TemplateService  templateService;

    private LeosPackage leosPackage;
    private AGECantoDorado cantoDorado = null;
    private int index;
    private String purpose = null;
    private String type = null;
    private String template = null;
    private String cantoDoradoId = null;
    private Map<String, String> collaborators = null;

    private DocumentVO cantoDoradoDocument;
    private final Map<ContextAction, String> actionMsgMap;
    private String cantoDoradoNumber;
    private String versionComment;
    private String milestoneComment;

    public AGECantoDoradoContext(
            AGECantoDoradoService cantoDoradoService,
            SecurityService securityService,
            TemplateService templateService) {
        this.cantoDoradoService = cantoDoradoService;
        this.securityService = securityService;
        this.templateService = templateService;
        this.actionMsgMap = new HashMap<>();
    }

    public void useTemplate(String name) {
        Validate.notNull(name, "Template name is required!");
        
        this.cantoDorado = (AGECantoDorado) templateService.getTemplate(name);
        Validate.notNull(cantoDorado, "Template not found! [name=%s]", name);

        LOG.trace("Using {} template... [id={}, name={}]", cantoDorado.getCategory(), cantoDorado.getId(), cantoDorado.getName());
    }
    
//    public void useTemplate(Report report) {
//        Validate.notNull(report, "Report template is required!");
//        LOG.trace("Using Report template... [id={}, name={}]", report.getId(), report.getName());
//        this.report = report;
//    }

    public void useActionMessageMap(Map<ContextAction, String> messages) {
        Validate.notNull(messages, "Action message map is required!");

        actionMsgMap.putAll(messages);
    }

    public void usePackage(LeosPackage leosPackage) {
        Validate.notNull(leosPackage, "cantoDorado package is required!");
        LOG.trace("Using cantoDorado package... [id={}, path={}]", leosPackage.getId(), leosPackage.getPath());
        this.leosPackage = leosPackage;
    }

    public void usePurpose(String purpose) {
        Validate.notNull(purpose, "cantoDorado purpose is required!");
        LOG.trace("Using cantoDorado purpose... [purpose={}]", purpose);
        this.purpose = purpose;
    }
    
    public void useType(String type) {
        Validate.notNull(type, "cantoDorado type is required!");
        LOG.trace("Using cantoDorado type... [type={}]", type);
        this.type = type;
    }

    public void usePackageTemplate(String template) {
        Validate.notNull(template, "template is required!");
        LOG.trace("Using template... [template={}]", template);
        this.template = template;
    }
    
    public void useIndex(int index) {
        Validate.notNull(index, "cantoDorado index is required!");
        LOG.trace("Using cantoDorado index... [index={}]", index);
        this.index = index;
    }

    public void useCantoDoradoId(String cantoDoradoId) {
        Validate.notNull(cantoDoradoId, "cantoDorado 'cantoDoradoId' is required!");
        LOG.trace("Using cantoDorado'... [cantoDoradoId={}]", cantoDoradoId);
        this.cantoDoradoId = cantoDoradoId;
    }

    public void useDocument(DocumentVO document) {
        Validate.notNull(document, "cantoDorado document is required!");
        cantoDoradoDocument = document;
    }

    public void useCollaborators(Map<String, String> collaborators) {
        Validate.notNull(collaborators, "cantoDorado 'collaborators' are required!");
        LOG.trace("Using collaborators'... [collaborators={}]", collaborators);
        this.collaborators = Collections.unmodifiableMap(collaborators);
    }

    public void useCantoDoradoNumber(String cantoDoradoNumber) {
        Validate.notNull(cantoDoradoNumber, "cantoDorado Number is required!");
        this.cantoDoradoNumber = cantoDoradoNumber;
    }
    
    public void useVersionComment(String comment) {
        Validate.notNull(comment, "Version comment is required!");
        this.versionComment = comment;
    }

    public void useMilestoneComment(String milestoneComment) {
        Validate.notNull(milestoneComment, "milestoneComment is required!");
        this.milestoneComment = milestoneComment;
    }


    public AGECantoDorado executeCreateCantoDorado() {
        LOG.trace("Executing 'Create CantoDorado' use case...");

        Validate.notNull(leosPackage, "cantoDorado package is required!");
        Validate.notNull(cantoDorado, "cantoDorado template is required!");
        Validate.notNull(collaborators, "cantoDorado collaborators are required!");
        Validate.notNull(cantoDoradoNumber, "cantoDorado number is required");

        Option<AGECantoDoradoMetadata> metadataOption = cantoDorado.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "CantoDorado metadata is required!");
        
        
        Validate.notNull(purpose, "cantoDorado purpose is required!");
        Validate.notNull(type, "cantoDorado type is required!");
        AGECantoDoradoMetadata metadata = metadataOption.get().withPurpose(purpose).withIndex(index).withNumber(cantoDoradoNumber).withType(type).withTemplate(template);
        cantoDorado = cantoDoradoService.createCantoDorado(cantoDorado.getId(), leosPackage.getPath(), metadata, actionMsgMap.get(ContextAction.CANTODORADO_METADATA_UPDATED), null);
        cantoDorado = securityService.updateCollaborators(cantoDorado.getId(), collaborators, AGECantoDorado.class);

        return cantoDorado;
    }
    
    public void useActionMessage(ContextAction action, String actionMsg) {
        Validate.notNull(actionMsg, "Action message is required!");
        Validate.notNull(action, "Context Action not found! [name=%s]", action);

        LOG.trace("Using action message... [action={}, name={}]", action, actionMsg);
        actionMsgMap.put(action, actionMsg);
    }
    

    public void executeUpdateCantoDoradoStructure() {
        byte[] xmlContent = getContent(cantoDorado); //Use the content from template
        cantoDorado = cantoDoradoService.findCantoDorado(cantoDoradoId); //Get the existing annex document
        
        Option<AGECantoDoradoMetadata> metadataOption = cantoDorado.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "cantoDorado metadata is required!");
        AGECantoDoradoMetadata metadata = metadataOption.get();
        AGECantoDoradoMetadata cantoDoradoMetadata = metadata.withPurpose(metadata.getPurpose()).
                    withType(metadata.getType()).withTitle(metadata.getTitle()).withTemplate(template).
                    withDocVersion(metadata.getDocVersion()).withDocTemplate(template);
        
        cantoDorado = cantoDoradoService.updateCantoDoradotWithMetadata(cantoDorado, xmlContent, cantoDoradoMetadata, VersionType.INTERMEDIATE, actionMsgMap.get(ContextAction.CANTODORADO_STRUCTURE_UPDATED));
    }
    
    private byte[] getContent(AGECantoDorado cantoDorado) {
        final Content content = cantoDorado.getContent().getOrError(() -> "cantoDorado content is required!");
        return content.getSource().getBytes();
    }
    
    public void executeUpdateCantoDorado() {
        LOG.trace("Executing 'Update cantoDorado' use case...");

        Validate.notNull(leosPackage, "cantoDorado package is required!");
        AGECantoDorado cantoDorado = cantoDoradoService.findCantoDoradoByPackagePath(leosPackage.getPath());

        Option<AGECantoDoradoMetadata> metadataOption = cantoDorado.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "CantoDorado metadata is required!");

        Validate.notNull(purpose, "CantoDorado purpose is required!");
        AGECantoDoradoMetadata metadata = metadataOption.get().withPurpose(purpose);

        cantoDoradoService.updateCantoDorado(cantoDorado, metadata, VersionType.INTERMEDIATE, actionMsgMap.get(ContextAction.METADATA_UPDATED));
    }

    public AGECantoDorado executeImporCantoDorado() {
        LOG.trace("Executing 'Import CantoDorado' use case...");

        Validate.notNull(leosPackage, "CantoDorado package is required!");
        Validate.notNull(cantoDorado, "CantoDorado template is required!");
        Validate.notNull(collaborators, "CantoDorado collaborators are required!");
        Validate.notNull(cantoDoradoNumber, "CantoDorado number is required");

        Option<AGECantoDoradoMetadata> metadataOption = cantoDorado.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "CantoDorado metadata is required!");

        MetadataVO cantoDoradoMeta = cantoDoradoDocument.getMetadata();

        Validate.notNull(purpose, "CantoDorado purpose is required!");
        Validate.notNull(type, "CantoDorado type is required!");
        AGECantoDoradoMetadata metadata = metadataOption.get().withPurpose(purpose).withIndex(index).withNumber(cantoDoradoNumber).withTitle(cantoDoradoMeta.getTitle()).withType(type).withTemplate(template);

        cantoDorado = cantoDoradoService.createCantoDoradoFromContent(leosPackage.getPath(), metadata, actionMsgMap.get(ContextAction.CANTODORADO_BLOCK_UPDATED),
        		cantoDoradoDocument.getSource());
        cantoDorado = securityService.updateCollaborators(cantoDorado.getId(), collaborators, AGECantoDorado.class);

        return cantoDorado;
    }

    public void executeUpdateCantoDoradoMetadata() {
        LOG.trace("Executing 'Update cantoDorado metadata' use case...");
        Validate.notNull(purpose, "cantoDorado purpose is required!");
        Validate.notNull(cantoDoradoId, "cantoDorado id is required!");

        cantoDorado = cantoDoradoService.findCantoDorado(cantoDoradoId);
        Option<AGECantoDoradoMetadata> metadataOption = cantoDorado.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "cantoDoradoMeta metadata is required!");

        // Updating only purpose at this time. other metadata needs to be set, if needed
        AGECantoDoradoMetadata cantoDoradoMetadata = metadataOption.get().withPurpose(purpose);
        cantoDoradoService.updateCantoDorado(cantoDorado, cantoDoradoMetadata, VersionType.INTERMEDIATE, actionMsgMap.get(ContextAction.METADATA_UPDATED));
    }

    public void executeUpdateCantoDoradoIndex() {
        LOG.trace("Executing 'Update cantoDorado index' use case...");
        Validate.notNull(cantoDoradoId, "cantoDorado id is required!");
        Validate.notNull(index, "cantoDorado index is required!");
        Validate.notNull(cantoDoradoNumber, "cantoDorado number is required");
        Validate.notNull(actionMsgMap, "Action Map is required");
        cantoDorado = cantoDoradoService.findCantoDorado(cantoDoradoId);
        Option<AGECantoDoradoMetadata> metadataOption = cantoDorado.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "cantoDoradoMeta metadata is required!");
        AGECantoDoradoMetadata metadata = metadataOption.get();
        AGECantoDoradoMetadata cantoDoradoMetadata = metadata.withIndex(index).withNumber(cantoDoradoNumber);
        cantoDorado = cantoDoradoService.updateCantoDorado(cantoDorado, cantoDoradoMetadata, VersionType.INTERMEDIATE, actionMsgMap.get(ContextAction.CANTODORADO_METADATA_UPDATED));
    }
    


    public void executeCreateMilestone() {
    	cantoDorado = cantoDoradoService.findCantoDorado(cantoDoradoId);
    	if (cantoDorado != null) {
    		List<String> milestoneComments = cantoDorado.getMilestoneComments();
    		milestoneComments.add(milestoneComment);
    		if (cantoDorado.getVersionType().equals(VersionType.MAJOR)) {
    			cantoDorado = cantoDoradoService.updateCantoDoradotWithMilestoneComments(cantoDorado.getId(), milestoneComments);
    			LOG.info("Major version {} already present. Updated only milestoneComment for [cantoDorado={}]", cantoDorado.getVersionLabel(), cantoDorado.getId());
    		} else {
    			cantoDorado = cantoDoradoService.updateCantoDoradoWithMilestoneComments(cantoDorado, milestoneComments, VersionType.MAJOR, versionComment);
    			LOG.info("Created major version {} for [cantoDorado={}]", cantoDorado.getVersionLabel(), cantoDorado.getId());
    		}	
    	}

    }

    public String getUpdatedCantoDoradoId() {
        return cantoDorado.getId();
    }
   
}
