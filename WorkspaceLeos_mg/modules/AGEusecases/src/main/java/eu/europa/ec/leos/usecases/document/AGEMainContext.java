/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.usecases.document;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import eu.europa.ec.leos.domain.cmis.Content;
import eu.europa.ec.leos.domain.cmis.LeosPackage;
import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.AGEMain;
import eu.europa.ec.leos.domain.cmis.metadata.AGEMainMetadata;
import eu.europa.ec.leos.domain.vo.DocumentVO;
import eu.europa.ec.leos.domain.vo.MetadataVO;
import eu.europa.ec.leos.services.document.AGEMainService;
import eu.europa.ec.leos.services.document.SecurityService;
import eu.europa.ec.leos.services.store.TemplateService;
import io.atlassian.fugue.Option;

@Component
@Scope("prototype")
public class AGEMainContext {

    private static final Logger LOG = LoggerFactory.getLogger(AGEMainContext.class);

    private final AGEMainService MainService;
    private final SecurityService securityService;
    private final TemplateService  templateService;

    private LeosPackage leosPackage;
    private AGEMain Main = null;
    private int index;
    private String purpose = null;
    private String type = null;
    private String template = null;
    private String MainId = null;
    private Map<String, String> collaborators = null;

    private DocumentVO MainDocument;
    private final Map<ContextAction, String> actionMsgMap;
    private String MainNumber;
    private String versionComment;
    private String milestoneComment;

    public AGEMainContext(
            AGEMainService MainService,
            SecurityService securityService,
            TemplateService templateService) {
        this.MainService = MainService;
        this.securityService = securityService;
        this.templateService = templateService;
        this.actionMsgMap = new HashMap<>();
    }

    public void useTemplate(String name) {
        Validate.notNull(name, "Template name is required!");
        
        this.Main = (AGEMain) templateService.getTemplate(name);
        Validate.notNull(Main, "Template not found! [name=%s]", name);

        LOG.trace("Using {} template... [id={}, name={}]", Main.getCategory(), Main.getId(), Main.getName());
    }
    
//    public void useTemplate(Main Main) {
//        Validate.notNull(Main, "Main template is required!");
//        LOG.trace("Using Main template... [id={}, name={}]", Main.getId(), Main.getName());
//        this.Main = Main;
//    }

    public void useActionMessageMap(Map<ContextAction, String> messages) {
        Validate.notNull(messages, "Action message map is required!");

        actionMsgMap.putAll(messages);
    }

    public void usePackage(LeosPackage leosPackage) {
        Validate.notNull(leosPackage, "Main package is required!");
        LOG.trace("Using Main package... [id={}, path={}]", leosPackage.getId(), leosPackage.getPath());
        this.leosPackage = leosPackage;
    }

    public void usePurpose(String purpose) {
        Validate.notNull(purpose, "Main purpose is required!");
        LOG.trace("Using Main purpose... [purpose={}]", purpose);
        this.purpose = purpose;
    }
    
    public void useType(String type) {
        Validate.notNull(type, "Main type is required!");
        LOG.trace("Using Main type... [type={}]", type);
        this.type = type;
    }

    public void usePackageTemplate(String template) {
        Validate.notNull(template, "template is required!");
        LOG.trace("Using template... [template={}]", template);
        this.template = template;
    }
    
    public void useIndex(int index) {
        Validate.notNull(index, "Main index is required!");
        LOG.trace("Using Main index... [index={}]", index);
        this.index = index;
    }

    public void useMainId(String MainId) {
        Validate.notNull(MainId, "Main 'MainId' is required!");
        LOG.trace("Using MainId'... [MainId={}]", MainId);
        this.MainId = MainId;
    }

    public void useDocument(DocumentVO document) {
        Validate.notNull(document, "Main document is required!");
        MainDocument = document;
    }

    public void useCollaborators(Map<String, String> collaborators) {
        Validate.notNull(collaborators, "Main 'collaborators' are required!");
        LOG.trace("Using collaborators'... [collaborators={}]", collaborators);
        this.collaborators = Collections.unmodifiableMap(collaborators);
    }

    public void useMainNumber(String MainNumber) {
        Validate.notNull(MainNumber, "Main Number is required!");
        this.MainNumber = MainNumber;
    }
    
    public void useVersionComment(String comment) {
        Validate.notNull(comment, "Version comment is required!");
        this.versionComment = comment;
    }

    public void useMilestoneComment(String milestoneComment) {
        Validate.notNull(milestoneComment, "milestoneComment is required!");
        this.milestoneComment = milestoneComment;
    }


    public AGEMain executeCreateMain() {
        LOG.trace("Executing 'Create Main' use case...");

        Validate.notNull(leosPackage, "Main package is required!");
        Validate.notNull(Main, "Main template is required!");
        Validate.notNull(collaborators, "Main collaborators are required!");
        Validate.notNull(MainNumber, "Main number is required");

        Option<AGEMainMetadata> metadataOption = Main.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "Main metadata is required!");
        
        
        Validate.notNull(purpose, "Main purpose is required!");
        Validate.notNull(type, "Main type is required!");
        AGEMainMetadata metadata = metadataOption.get().withPurpose(purpose).withIndex(index).withNumber(MainNumber).withType(type).withTemplate(template);
        Main = MainService.createMain(Main.getId(), leosPackage.getPath(), metadata, actionMsgMap.get(ContextAction.MAIN_METADATA_UPDATED), null);
        Main = securityService.updateCollaborators(Main.getId(), collaborators, AGEMain.class);

        return Main;
    }
    
    public void useActionMessage(ContextAction action, String actionMsg) {
        Validate.notNull(actionMsg, "Action message is required!");
        Validate.notNull(action, "Context Action not found! [name=%s]", action);

        LOG.trace("Using action message... [action={}, name={}]", action, actionMsg);
        actionMsgMap.put(action, actionMsg);
    }
    

    public void executeUpdateMainStructure() {
        byte[] xmlContent = getContent(Main); //Use the content from template
        Main = MainService.findMain(MainId); //Get the existing annex document
        
        Option<AGEMainMetadata> metadataOption = Main.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "Main metadata is required!");
        AGEMainMetadata metadata = metadataOption.get();
        AGEMainMetadata MainMetadata = metadata.withPurpose(metadata.getPurpose()).
                    withType(metadata.getType()).withTitle(metadata.getTitle()).withTemplate(template).
                    withDocVersion(metadata.getDocVersion()).withDocTemplate(template);
        
        Main = MainService.updateMainWithMetadata(Main, xmlContent, MainMetadata, VersionType.INTERMEDIATE, actionMsgMap.get(ContextAction.MAIN_STRUCTURE_UPDATED));
    }
    
    private byte[] getContent(AGEMain Main) {
        final Content content = Main.getContent().getOrError(() -> "Annex content is required!");
        return content.getSource().getBytes();
    }
    
    public void executeUpdateMain() {
        LOG.trace("Executing 'Update Main' use case...");

        Validate.notNull(leosPackage, "Main package is required!");
        AGEMain Main = MainService.findMainByPackagePath(leosPackage.getPath());

        Option<AGEMainMetadata> metadataOption = Main.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "Main metadata is required!");

        Validate.notNull(purpose, "Main purpose is required!");
        AGEMainMetadata metadata = metadataOption.get().withPurpose(purpose);

        MainService.updateMain(Main, metadata, VersionType.INTERMEDIATE, actionMsgMap.get(ContextAction.METADATA_UPDATED));
    }

    public AGEMain executeImportMain() {
        LOG.trace("Executing 'Import Main' use case...");

        Validate.notNull(leosPackage, "Main package is required!");
        Validate.notNull(Main, "Main template is required!");
        Validate.notNull(collaborators, "Main collaborators are required!");
        Validate.notNull(MainNumber, "Main number is required");

        Option<AGEMainMetadata> metadataOption = Main.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "Main metadata is required!");

        MetadataVO MainMeta = MainDocument.getMetadata();

        Validate.notNull(purpose, "Main purpose is required!");
        Validate.notNull(type, "Main type is required!");
        AGEMainMetadata metadata = metadataOption.get().withPurpose(purpose).withIndex(index).withNumber(MainNumber).withTitle(MainMeta.getTitle()).withType(type).withTemplate(template);

        Main = MainService.createMainFromContent(leosPackage.getPath(), metadata, actionMsgMap.get(ContextAction.MAIN_BLOCK_UPDATED),
                MainDocument.getSource());
        Main = securityService.updateCollaborators(Main.getId(), collaborators, AGEMain.class);

        return Main;
    }

    public void executeUpdateMainMetadata() {
        LOG.trace("Executing 'Update Main metadata' use case...");
        Validate.notNull(purpose, "Main purpose is required!");
        Validate.notNull(MainId, "Main id is required!");

        Main = MainService.findMain(MainId);
        Option<AGEMainMetadata> metadataOption = Main.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "Main metadata is required!");

        // Updating only purpose at this time. other metadata needs to be set, if needed
        AGEMainMetadata MainMetadata = metadataOption.get().withPurpose(purpose);
        MainService.updateMain(Main, MainMetadata, VersionType.INTERMEDIATE, actionMsgMap.get(ContextAction.METADATA_UPDATED));
    }

    public void executeUpdateMainIndex() {
        LOG.trace("Executing 'Update Main index' use case...");
        Validate.notNull(MainId, "Main id is required!");
        Validate.notNull(index, "Main index is required!");
        Validate.notNull(MainNumber, "Main number is required");
        Validate.notNull(actionMsgMap, "Action Map is required");
        Main = MainService.findMain(MainId);
        Option<AGEMainMetadata> metadataOption = Main.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "Main metadata is required!");
        AGEMainMetadata metadata = metadataOption.get();
        AGEMainMetadata MainMetadata = metadata.withIndex(index).withNumber(MainNumber);
        Main = MainService.updateMain(Main, MainMetadata, VersionType.INTERMEDIATE, actionMsgMap.get(ContextAction.MAIN_METADATA_UPDATED));
    }
    


    public void executeCreateMilestone() {
    	Main = MainService.findMain(MainId);
    	if (Main != null) {
    		List<String> milestoneComments = Main.getMilestoneComments();
    		milestoneComments.add(milestoneComment);
    		if (Main.getVersionType().equals(VersionType.MAJOR)) {
    			Main = MainService.updateMainWithMilestoneComments(Main.getId(), milestoneComments);
    			LOG.info("Major version {} already present. Updated only milestoneComment for [Main={}]", Main.getVersionLabel(), Main.getId());
    		} else {
    			Main = MainService.updateMainWithMilestoneComments(Main, milestoneComments, VersionType.MAJOR, versionComment);
    			LOG.info("Created major version {} for [Main={}]", Main.getVersionLabel(), Main.getId());
    		}	
    	}

    }

    public String getUpdatedMainId() {
        return Main.getId();
    }
   
}
