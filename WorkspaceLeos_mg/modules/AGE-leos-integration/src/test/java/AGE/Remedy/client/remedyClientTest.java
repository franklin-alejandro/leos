package AGE.Remedy.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import HPD_IncidentInterface_Create_WS_AdditionalHeaders.AuthenticationInfo;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.CreateInputMap;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.CreateRequestType;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.HPDIncidentInterfaceCreateWSPortTypePortType;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.HPDIncidentInterfaceCreateWSService;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.ObjectFactory;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.ReportedSourceType;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.ServiceTypeType;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.StatusReasonType;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.StatusType;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.WorkInfoSourceType;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.WorkInfoTypeType;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.WorkInfoViewAccessType;
import util.HTTPUtil;

public class remedyClientTest {
	HPDIncidentInterfaceCreateWSService service = new HPDIncidentInterfaceCreateWSService();
	HPDIncidentInterfaceCreateWSPortTypePortType incidentCreateServiceWSPort = service
			.getHPDIncidentInterfaceCreateWSPortTypeSoap();
	private String urlEndpoint = "https://mprremrssopro.dom.mpr.es/MidTier/WSDL/public/mprremrssopro.dom.mpr.es/HPD_IncidentInterface_Create_WS?wsdl";

	@Test(expected = Exception.class)
	public void test_SOAPFaultExceptionExpected() {
		System.out.println(incidentCreateServiceWSPort.helpDeskSubmitService(null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
				null, null, null, null, null));
	}

	@Test
	public void test_helpDeskSubmitService_setAuthenticationInfo() {
		ObjectFactory objectFactory = new ObjectFactory();
		AuthenticationInfo aInfo = new AuthenticationInfo();

		aInfo.setAuthentication(null);
		aInfo.setLocale(null);
		aInfo.setUserName("usuariows");
		aInfo.setPassword("usuariows");
		aInfo.setUserName(null);

		objectFactory.createAuthenticationInfo(aInfo);
	}

	@Test
	public void test_helpDeskSubmitService_HeaderInjection() {
		BindingProvider prov = (BindingProvider) incidentCreateServiceWSPort;
//		prov.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "https://mprremrssopro.dom.mpr.es/MidTier/services/ARService?server=mprremrssopro.dom.mpr.es%26webService=HPD_IncidentInterface_Create_WS");
		prov.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				"https://mprremrssopro.dom.mpr.es/MidTier/services/ARService?server=mprremrssopro.dom.mpr.es&webService=HPD_IncidentInterface_Create_WS");
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		prov.getRequestContext().put("userName", "myusername");
		prov.getRequestContext().put("password", "mypassword");
		prov.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
	}

	@Test(expected = Exception.class)
	// SOAPFaultException: ARERR [149] A user name must be supplied in the control record
	public void test_helpDeskSubmitService_CallServiceMethod() throws DatatypeConfigurationException {
		final GregorianCalendar now = new GregorianCalendar();
		AuthenticationInfo parameters = new AuthenticationInfo();
//		aInfo.setAuthentication("");
//		aInfo.setLocale("");
//		aInfo.setTimeZone("");
		parameters.setUserName("usuariows");
		parameters.setPassword("usuariows");

		incidentCreateServiceWSPort.helpDeskSubmitService(
				"APLICACIONES",//String assignedGroup,
				"",// String assignedGroupShiftName,
				"MINISTERIO DE LA PRESIDENCIA",// String assignedSupportCompany,
				"Soporte TI",// String assignedSupportOrganization,
				"Soporte LEOS Incidencias",//String assignee,
				"Aplicaciones",//String categorizationTier1,
				"Incidencia en Aplicación",//String categorizationTier2,
				"LEOS",//String categorizationTier3,
				"",//  String ciName,
				"",//String closureManufacturer,
				"SERVICIO",//String closureProductCategoryTier1,
				"Final",//String closureProductCategoryTier2,
				"",// String closureProductCategoryTier3,
				"",//String closureProductModelVersion,
				"",//String closureProductName,
				"",//String department,
				"ext_remedy01",//String firstName,
				"3-Moderate",// String impact, <<-- Necesita un objeto propio?
				"ext_remedy01",//String lastName,
				"",//String lookupKeyword,
				"",// String manufacturer,
				"SERVICIO",//String productCategorizationTier1,
				"Final",//String productCategorizationTier2,
				"LEOS",//String productCategorizationTier3,
				"",//String productModelVersion,
				"",//String productName,
				ReportedSourceType.EXTERNAL_ESCALATION,//eportedSourceType reportedSource,
				"",//String resolution,
				"",//String resolutionCategoryTier1,
				"",//String resolutionCategoryTier2,
				"",//String resolutionCategoryTier3,
				ServiceTypeType.USER_SERVICE_REQUEST,//ServiceTypeType serviceType,
				StatusType.NEW,//StatusType status,
				"CREATE",//String action,
				CreateRequestType.YES,//CreateRequestType createRequest,
				"PRUEBAS INTEGRACIÓN LEOS",// String summary,
				"PRUEBAS INTEGRACIÓN LEOS (ADJUNTOS). Prueba de adjuntos nº 1. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In efficitur dapibus diam sed mattis. Curabitur sed nisl interdum, facilisis erat nec, viverra risus. Sed non facilisis nibh. Etiam porttitor libero et est blandit, sed consequat nisl venenatis. Duis lectus est, malesuada eu laoreet scelerisque, luctus eu magna. Etiam non ipsum mollis, gravida nisi non, mollis dolor. Donec in sodales erat, non porta metus. Etiam euismod enim lectus. Vivamus metus justo, laoreet sit amet quam non, luctus euismod velit. Vestibulum tellus ante, pharetra id lobortis at, efficitur vitae enim. Donec aliquet mollis porta. Aenean mattis rutrum felis eu consequat.",// String notes,
				"3-Medium",//String urgency,
				"LEOS",//String workInfoSummary,
				"Adjunto",//String workInfoNotes,
				WorkInfoTypeType.INCIDENT_TASK_ACTION,// WorkInfoTypeType workInfoType, GENERAL <--
				DatatypeFactory.newInstance().newXMLGregorianCalendar(now),//XMLGregorianCalendar workInfoDate,
				WorkInfoSourceType.WEB,//WorkInfoSourceType workInfoSource, <--
				CreateRequestType.YES,//CreateRequestType workInfoLocked, <--
				WorkInfoViewAccessType.INTERNAL,//WorkInfoViewAccessType workInfoViewAccess,
				"",// String middleInitial,
				StatusReasonType.REQUEST,//StatusReasonType statusReason,
				"",//String directContactFirstName,
				"",//String directContactMiddleInitial,
				"",//String directContactLastName,
				"",//String templateID,
				"LEOS",//String serviceCI,
				"",//String serviceCIReconID,
				"",//String hpdCI,
				"",//String hpdCIReconID,
				"",// String hpdCIFormName,
				"",//String workInfoAttachment1Name,
				null,//byte[] workInfoAttachment1Data,
				null,//Integer workInfoAttachment1OrigSize,
				"usuariows",//String loginID,
				"MINISTERIO DE LA PRESIDENCIA",//String customerCompany,
				"",//String corporateID
				parameters//AuthenticationInfo authenticantionInfo
				);
	}
	
	@Test
	public void test_helpDeskSubmitService_CallWebService() throws DatatypeConfigurationException, MalformedURLException {
		final GregorianCalendar now = new GregorianCalendar();
		AuthenticationInfo parameters = new AuthenticationInfo();
//		aInfo.setAuthentication("");
//		aInfo.setLocale("");
//		aInfo.setTimeZone("");
		parameters.setUserName("usuariows");
		parameters.setPassword("usuariows");
		
		Map<String, Object> ctx = ((BindingProvider) incidentCreateServiceWSPort).getRequestContext();
		URL url = new URL("https://mprremrssopro.dom.mpr.es/MidTier/WSDL/public/mprremrssopro.dom.mpr.es/HPD_IncidentInterface_Create_WS?wsdl");
        String proto = url.getProtocol();
        String oldproto = (proto.equals("http"))?"https://":"http://";
        proto = proto+"://";
        String endpoint = (String) ctx.get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
        ctx.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint.replace(oldproto, proto));

        String xmlRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:HPD_IncidentInterface_WS\">\n" +
                "   <soapenv:Header>\n" +
                "      <urn:AuthenticationInfo>\n" +
                "         <urn:userName>" + parameters.getUserName() + "</urn:userName>\n" +
                "         <urn:password>" + parameters.getPassword() + "</urn:password>\n" +
                "      </urn:AuthenticationInfo>\n" +
                "   </soapenv:Header>\n" +
                "<soapenv:Body>\n"
                + "      <urn:HelpDesk_Submit_Service>\n"
                + "         <urn:Assigned_Group>APLICACIONES</urn:Assigned_Group>\n"
                + "         <urn:Assigned_Group_Shift_Name></urn:Assigned_Group_Shift_Name>\n"
                + "         <urn:Assigned_Support_Company>MINISTERIO DE LA PRESIDENCIA</urn:Assigned_Support_Company>\n"
                + "         <urn:Assigned_Support_Organization>Soporte TI</urn:Assigned_Support_Organization>\n"
                + "         <urn:Assignee>Soporte LEOS Incidencias</urn:Assignee>\n"
                + "         <urn:Categorization_Tier_1>Aplicaciones</urn:Categorization_Tier_1>\n"
                + "         <urn:Categorization_Tier_2>Incidencia en Aplicación</urn:Categorization_Tier_2>\n"
                + "         <urn:Categorization_Tier_3>LEOS</urn:Categorization_Tier_3>\n"
                + "         <urn:CI_Name></urn:CI_Name>\n"
                + "         <urn:Closure_Manufacturer></urn:Closure_Manufacturer>\n"
                + "         <urn:Closure_Product_Category_Tier1>SERVICIO</urn:Closure_Product_Category_Tier1>\n"
                + "         <urn:Closure_Product_Category_Tier2>Final</urn:Closure_Product_Category_Tier2>\n"
                + "         <urn:Closure_Product_Category_Tier3></urn:Closure_Product_Category_Tier3>\n"
                + "         <urn:Closure_Product_Model_Version></urn:Closure_Product_Model_Version>\n"
                + "         <urn:Closure_Product_Name></urn:Closure_Product_Name>\n"
                + "         <urn:Department></urn:Department>\n"
                + "         <urn:First_Name>ext_remedy01</urn:First_Name>\n"
                + "         <urn:Impact>3-Moderate/Limited</urn:Impact>\n"
                + "         <urn:Last_Name>ext_remedy01</urn:Last_Name>\n"
                + "         <urn:Lookup_Keyword></urn:Lookup_Keyword>\n"
                + "         <urn:Manufacturer></urn:Manufacturer>\n"
                + "         <urn:Product_Categorization_Tier_1>SERVICIO</urn:Product_Categorization_Tier_1>\n"
                + "         <urn:Product_Categorization_Tier_2>Final</urn:Product_Categorization_Tier_2>\n"
                + "         <urn:Product_Categorization_Tier_3>LEOS</urn:Product_Categorization_Tier_3>\n"
                + "         <urn:Product_Model_Version></urn:Product_Model_Version>\n"
                + "         <urn:Product_Name></urn:Product_Name>\n"
                + "         <urn:Reported_Source>External Escalation</urn:Reported_Source>\n"
                + "         <urn:Resolution></urn:Resolution>\n"
                + "         <urn:Resolution_Category_Tier_1></urn:Resolution_Category_Tier_1>\n"
                + "         <urn:Resolution_Category_Tier_2></urn:Resolution_Category_Tier_2>\n"
                + "         <urn:Resolution_Category_Tier_3></urn:Resolution_Category_Tier_3>\n"
                + "         <urn:Service_Type>User Service Restoration</urn:Service_Type>\n"
                + "         <urn:Status>New</urn:Status>\n"
                + "         <urn:Action>CREATE</urn:Action>\n"
                + "         <urn:Create_Request></urn:Create_Request>\n"
                + "         <urn:Summary>PRUEBAS INTEGRACIÓN LEOS</urn:Summary>\n"
                + "         <urn:Notes>PRUEBAS INTEGRACIÓN LEOS (ADJUNTOS). Prueba de adjuntos nº 1. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In efficitur dapibus diam sed mattis. Curabitur sed nisl interdum, facilisis erat nec, viverra risus. Sed non facilisis nibh. Etiam porttitor libero et est blandit, sed consequat nisl venenatis. Duis lectus est, malesuada eu laoreet scelerisque, luctus eu magna. Etiam non ipsum mollis, gravida nisi non, mollis dolor. Donec in sodales erat, non porta metus. Etiam euismod enim lectus. Vivamus metus justo, laoreet sit amet quam non, luctus euismod velit. Vestibulum tellus ante, pharetra id lobortis at, efficitur vitae enim. Donec aliquet mollis porta. Aenean mattis rutrum felis eu consequat.</urn:Notes>\n"
                + "         <urn:Urgency>3-Medium</urn:Urgency>\n"
                + "         <urn:Work_Info_Summary>LEOS</urn:Work_Info_Summary>\n"
                + "         <urn:Work_Info_Notes>Adjunto</urn:Work_Info_Notes>\n"
                + "         <urn:Work_Info_Type></urn:Work_Info_Type>\n"
                + "         <urn:Work_Info_Date></urn:Work_Info_Date>\n"
                + "         <urn:Work_Info_Source></urn:Work_Info_Source>\n"
                + "         <urn:Work_Info_Locked>No</urn:Work_Info_Locked>\n"
                + "         <urn:Work_Info_View_Access></urn:Work_Info_View_Access>\n"
                + "         <urn:Middle_Initial></urn:Middle_Initial>\n"
                + "         <urn:Status_Reason></urn:Status_Reason>\n"
                + "         <urn:Direct_Contact_First_Name></urn:Direct_Contact_First_Name>\n"
                + "         <urn:Direct_Contact_Middle_Initial></urn:Direct_Contact_Middle_Initial>\n"
                + "         <urn:Direct_Contact_Last_Name></urn:Direct_Contact_Last_Name>\n"
                + "         <urn:TemplateID></urn:TemplateID>\n"
                + "         <urn:ServiceCI>LEOS</urn:ServiceCI>\n"
                + "         <urn:ServiceCI_ReconID></urn:ServiceCI_ReconID>\n"
                + "         <urn:HPD_CI></urn:HPD_CI>\n"
                + "         <urn:HPD_CI_ReconID></urn:HPD_CI_ReconID>\n"
                + "         <urn:HPD_CI_FormName></urn:HPD_CI_FormName>\n"
                + "         <urn:WorkInfoAttachment1Name>autobus.gif</urn:WorkInfoAttachment1Name>\n"
                + "         <urn:WorkInfoAttachment1Data>R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==</urn:WorkInfoAttachment1Data>\n"
                + "         <urn:WorkInfoAttachment1OrigSize>1951</urn:WorkInfoAttachment1OrigSize>     \n"
                + "         <urn:Login_ID>ext_remedy01</urn:Login_ID>\n"
                + "         <urn:Customer_Company></urn:Customer_Company>\n"
                + "         <urn:Corporate_ID></urn:Corporate_ID>\n"
                + "      </urn:HelpDesk_Submit_Service>\n"
                + "   </soapenv:Body>" +
                "</soapenv:Envelope>";

        System.out.println("end " + endpoint);

        try {
            HttpClient httpclient = HTTPUtil.getHttpClient();
            HttpPost httppost = new HttpPost(endpoint.replace(oldproto, proto));
            httppost.setHeader("SOAPAction", "urn:HPD_IncidentInterface_Create_WS");
            httppost.setEntity(new StringEntity(xmlRequest));
            HttpResponse response = httpclient.execute(httppost);
            System.out.println(response.toString() +"\n" + EntityUtils.toString(response.getEntity(),"UTF-8"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

	}
	
	@Test
	public void test_helpDeskSubmitService_CallClientMethod() throws DatatypeConfigurationException, MalformedURLException {
		RemedyClient remedyClient = new RemedyClient();
		
		AuthenticationInfo parameters = new AuthenticationInfo();
//		aInfo.setAuthentication("");
//		aInfo.setLocale("");
//		aInfo.setTimeZone("");
		parameters.setUserName("usuariows");
		parameters.setPassword("usuariows");
		
		CreateInputMap inputMap = new CreateInputMap();
		inputMap.setAction("CREATE");
		inputMap.setAssignedGroup("APLICACIONES");
		inputMap.setAssignedGroupShiftName(null);
		inputMap.setAssignedSupportCompany("MINISTERIO DE LA PRESIDENCIA");
		inputMap.setAssignedSupportOrganization("Soporte TI");
		inputMap.setAssignee("Soporte LEOS Incidencias");
		inputMap.setCategorizationTier1("Aplicaciones");
		inputMap.setCategorizationTier2("Incidencia en Aplicación");
		inputMap.setCategorizationTier3("LEOS");
		inputMap.setCIName(null);
		inputMap.setClosureManufacturer(null);
		inputMap.setClosureProductCategoryTier1("SERVICIO");
		inputMap.setClosureProductCategoryTier2("Final");
		inputMap.setClosureProductCategoryTier3(null);
		inputMap.setClosureProductModelVersion(null);
		inputMap.setClosureProductName(null);
		inputMap.setCorporateID(null);
		inputMap.setCreateRequest(null); //Obligatorio dejar vacio en validación
		inputMap.setCustomerCompany(null);
		inputMap.setDepartment(null);
		inputMap.setDirectContactFirstName(null);
		inputMap.setDirectContactLastName(null);
		inputMap.setDirectContactMiddleInitial(null);
		inputMap.setFirstName("ext_remedy01");
		inputMap.setHPDCI(null);
		inputMap.setHPDCIFormName(null);
		inputMap.setHPDCIReconID(null);
		inputMap.setImpact("3-Moderate/Limited");
		inputMap.setLastName("ext_remedy01");
		inputMap.setLoginID("ext_remedy01");
		inputMap.setLookupKeyword(null);
		inputMap.setMiddleInitial(null);
		inputMap.setNotes("PRUEBAS INTEGRACIÓN LEOS (ADJUNTOS). Prueba de adjuntos nº 1. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In efficitur dapibus diam sed mattis. Curabitur sed nisl interdum, facilisis erat nec, viverra risus. Sed non facilisis nibh. Etiam porttitor libero et est blandit, sed consequat nisl venenatis. Duis lectus est, malesuada eu laoreet scelerisque, luctus eu magna. Etiam non ipsum mollis, gravida nisi non, mollis dolor. Donec in sodales erat, non porta metus. Etiam euismod enim lectus. Vivamus metus justo, laoreet sit amet quam non, luctus euismod velit. Vestibulum tellus ante, pharetra id lobortis at, efficitur vitae enim. Donec aliquet mollis porta. Aenean mattis rutrum felis eu consequat.");
		inputMap.setProductCategorizationTier1("SERVICIO");
		inputMap.setProductCategorizationTier2("Final");
		inputMap.setProductCategorizationTier3("LEOS");
		inputMap.setProductModelVersion(null);
		inputMap.setProductName(null);
		inputMap.setReportedSource(ReportedSourceType.EXTERNAL_ESCALATION);
		inputMap.setResolution(null);
		inputMap.setResolutionCategoryTier1(null);
		inputMap.setResolutionCategoryTier2(null);
		inputMap.setResolutionCategoryTier3(null);
		inputMap.setServiceCI("LEOS");
		inputMap.setServiceCIReconID(null);
		inputMap.setServiceType(ServiceTypeType.USER_SERVICE_RESTORATION);
		inputMap.setStatus(StatusType.NEW);
		inputMap.setStatusReason(null);
		inputMap.setSummary("PRUEBAS INTEGRACIÓN LEOS");
		inputMap.setTemplateID(null);
		inputMap.setUrgency("3-Medium");
		inputMap.setWorkInfoAttachment1Data("R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==".getBytes());
		inputMap.setWorkInfoAttachment1Name("autobus.gif");
		inputMap.setWorkInfoAttachment1OrigSize(1951);
		inputMap.setWorkInfoDate(null);
		inputMap.setWorkInfoLocked(CreateRequestType.NO);
		inputMap.setWorkInfoNotes("Adjunto");
		inputMap.setWorkInfoSource(null);
		inputMap.setWorkInfoSummary("LEOS");
		inputMap.setWorkInfoType(null);
		inputMap.setWorkInfoViewAccess(null);
		
		System.out.println(remedyClient.helpDeskSubmitService(inputMap, parameters, urlEndpoint));
	}
	
	@Test
	public void test_helpDeskSubmitService_CallClientValidationMethod() throws DatatypeConfigurationException, MalformedURLException {
		RemedyClient remedyClient = new RemedyClient();
		
		AuthenticationInfo parameters = new AuthenticationInfo();
//		aInfo.setAuthentication("");
//		aInfo.setLocale("");
//		aInfo.setTimeZone("");
		parameters.setUserName("");
		parameters.setPassword("usuariows");
		
		CreateInputMap inputMap = new CreateInputMap();
		inputMap.setAction("CREATE");
		inputMap.setAssignedGroup("APLICACIONES");
		inputMap.setAssignedGroupShiftName(null);
		inputMap.setAssignedSupportCompany("MINISTERIO DE LA PRESIDENCIA");
		inputMap.setAssignedSupportOrganization("Soporte TI");
		inputMap.setAssignee("Soporte LEOS Incidencias");
		inputMap.setCategorizationTier1("Aplicaciones");
		inputMap.setCategorizationTier2("Incidencia en Aplicación");
		inputMap.setCategorizationTier3("LEOS");
		inputMap.setCIName(null);
		inputMap.setClosureManufacturer(null);
		inputMap.setClosureProductCategoryTier1("SERVICIO");
		inputMap.setClosureProductCategoryTier2("Final");
		inputMap.setClosureProductCategoryTier3(null);
		inputMap.setClosureProductModelVersion(null);
		inputMap.setClosureProductName(null);
		inputMap.setCorporateID(null);
		inputMap.setCreateRequest(null);
		inputMap.setCustomerCompany(null);
		inputMap.setDepartment(null);
		inputMap.setDirectContactFirstName(null);
		inputMap.setDirectContactLastName(null);
		inputMap.setDirectContactMiddleInitial(null);
		inputMap.setFirstName("ext_remedy01");
		inputMap.setHPDCI(null);
		inputMap.setHPDCIFormName(null);
		inputMap.setHPDCIReconID(null);
		inputMap.setImpact("3-Moderate/Limited");
		inputMap.setLastName("ext_remedy01");
		inputMap.setLoginID("ext_remedy01");
		inputMap.setLookupKeyword(null);
		inputMap.setMiddleInitial(null);
		inputMap.setNotes("PRUEBAS INTEGRACIÓN LEOS (ADJUNTOS). Prueba de adjuntos nº 1. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In efficitur dapibus diam sed mattis. Curabitur sed nisl interdum, facilisis erat nec, viverra risus. Sed non facilisis nibh. Etiam porttitor libero et est blandit, sed consequat nisl venenatis. Duis lectus est, malesuada eu laoreet scelerisque, luctus eu magna. Etiam non ipsum mollis, gravida nisi non, mollis dolor. Donec in sodales erat, non porta metus. Etiam euismod enim lectus. Vivamus metus justo, laoreet sit amet quam non, luctus euismod velit. Vestibulum tellus ante, pharetra id lobortis at, efficitur vitae enim. Donec aliquet mollis porta. Aenean mattis rutrum felis eu consequat.");
		inputMap.setProductCategorizationTier1("SERVICIO");
		inputMap.setProductCategorizationTier2("Final");
		inputMap.setProductCategorizationTier3("LEOS");
		inputMap.setProductModelVersion(null);
		inputMap.setProductName(null);
		inputMap.setReportedSource(ReportedSourceType.EXTERNAL_ESCALATION);
		inputMap.setResolution(null);
		inputMap.setResolutionCategoryTier1(null);
		inputMap.setResolutionCategoryTier2(null);
		inputMap.setResolutionCategoryTier3(null);
		inputMap.setServiceCI("LEOS");
		inputMap.setServiceCIReconID(null);
		inputMap.setServiceType(ServiceTypeType.USER_SERVICE_RESTORATION);
		inputMap.setStatus(StatusType.NEW);
		inputMap.setStatusReason(null);
		inputMap.setSummary("PRUEBAS INTEGRACIÓN LEOS");
		inputMap.setTemplateID(null);
		inputMap.setUrgency("3-Medium");
		inputMap.setWorkInfoAttachment1Data("R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==".getBytes());
		inputMap.setWorkInfoAttachment1Name("autobus.gif");
		inputMap.setWorkInfoAttachment1OrigSize(1951);
		inputMap.setWorkInfoDate(null);
		inputMap.setWorkInfoLocked(CreateRequestType.NO);
		inputMap.setWorkInfoNotes("Adjunto");
		inputMap.setWorkInfoSource(null);
		inputMap.setWorkInfoSummary("LEOS");
		inputMap.setWorkInfoType(null);
		inputMap.setWorkInfoViewAccess(null);
		
		remedyClient.helpDeskSubmitService(inputMap, parameters, urlEndpoint);
	}

}