package AGE.Remedy.client;

import java.io.ByteArrayInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.ws.BindingProvider;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.common.base.Strings;

import HPD_IncidentInterface_Create_WS_AdditionalHeaders.AuthenticationInfo;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.CreateInputMap;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.HPDIncidentInterfaceCreateWSPortTypePortType;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.HPDIncidentInterfaceCreateWSService;
import util.HTTPUtil;

public class RemedyClient {
	
	private static final Logger LOG = LoggerFactory.getLogger(RemedyClient.class);
	
	@Autowired
	HPDIncidentInterfaceCreateWSService service = new HPDIncidentInterfaceCreateWSService();
	
	@Autowired
	HPDIncidentInterfaceCreateWSPortTypePortType incidentCreateServiceWSPort = service.getHPDIncidentInterfaceCreateWSPortTypeSoap();

	/**
	 * Método que llama al servicio HPD_IncidentInterface_Create_WS. 
	 * Debe recibir un objeto CreateInputMap y un objeto AuthenticationInfo.
	 * AuthenticationInfo como minimo debe tener seteados los parametros: userName y password
	 * CreateInputMap debe tener seteados los parámetros: Impact, Urgency, Summary, Notes, Action,
	 * First Name, Last Name, Service Type, ReportedSource y Status.
	 * 
	 * @param inputMap
	 * @param parameters
	 * @return List<String>
	 * @throws MalformedURLException
	 */
	public String helpDeskSubmitService(CreateInputMap inputMap, AuthenticationInfo parameters, String urlEndpoint) throws MalformedURLException {
		
		if(Boolean.TRUE.equals(validateInputMapAndParameters(inputMap, parameters))) {
			URL url = new URL(urlEndpoint);	
			Map<String, Object> ctx = ((BindingProvider) incidentCreateServiceWSPort).getRequestContext();
	        String proto = url.getProtocol();
	        String oldproto = (proto.equals("http"))?"https://":"http://";
	        proto = proto+"://";
	        String endpoint = (String) ctx.get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
	        ctx.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint.replace(oldproto, proto));
	        
	        StringBuilder xmlRequest = new StringBuilder();
	        xmlRequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:HPD_IncidentInterface_WS\">\n");
	        xmlRequest.append("   <soapenv:Header>\n" );
	        xmlRequest.append("      <urn:AuthenticationInfo>\n" );
	        xmlRequest.append("         <urn:userName>" + parameters.getUserName() + "</urn:userName>\n");
	        xmlRequest.append("         <urn:password>" + parameters.getPassword() + "</urn:password>\n");
	        xmlRequest.append("      </urn:AuthenticationInfo>\n");
	        xmlRequest.append("   </soapenv:Header>\n" );
	        xmlRequest.append("   <soapenv:Body>\n");
	        xmlRequest.append("      <urn:HelpDesk_Submit_Service>\n");
	        xmlRequest.append("         <urn:Assigned_Group>" + (inputMap.getAssignedGroup() == null ? "" : inputMap.getAssignedGroup()) + "</urn:Assigned_Group>\n");
	        xmlRequest.append("         <urn:Assigned_Group_Shift_Name>" + (inputMap.getAssignedGroupShiftName() == null ? "" : inputMap.getAssignedGroupShiftName()) + "</urn:Assigned_Group_Shift_Name>\n");
	        xmlRequest.append("         <urn:Assigned_Support_Company>" + (inputMap.getAssignedSupportCompany() == null ? "" : inputMap.getAssignedSupportCompany()) + "</urn:Assigned_Support_Company>\n");
	        xmlRequest.append("         <urn:Assigned_Support_Organization>" + (inputMap.getAssignedSupportOrganization() == null ? "" : inputMap.getAssignedSupportOrganization())  + "</urn:Assigned_Support_Organization>\n");
	        xmlRequest.append("         <urn:Assignee>" + (inputMap.getAssignee() == null ? "" : inputMap.getAssignee()) + "</urn:Assignee>\n");
	        xmlRequest.append("         <urn:Categorization_Tier_1>" + (inputMap.getCategorizationTier1() == null ? "" : inputMap.getCategorizationTier1()) + "</urn:Categorization_Tier_1>\n");
	        xmlRequest.append("         <urn:Categorization_Tier_2>" + (inputMap.getCategorizationTier2() == null ? "" : inputMap.getCategorizationTier2()) + "</urn:Categorization_Tier_2>\n");
	        xmlRequest.append("         <urn:Categorization_Tier_3>" + (inputMap.getCategorizationTier3() == null ? "" : inputMap.getCategorizationTier3()) + "</urn:Categorization_Tier_3>\n");
	        xmlRequest.append("         <urn:CI_Name>" + (inputMap.getCIName() == null ? "" : inputMap.getCIName()) + "</urn:CI_Name>\n");
	        xmlRequest.append("         <urn:Closure_Manufacturer>" + (inputMap.getClosureManufacturer() == null ? "" : inputMap.getClosureManufacturer()) + "</urn:Closure_Manufacturer>\n");
	        xmlRequest.append("         <urn:Closure_Product_Category_Tier1>" + (inputMap.getClosureProductCategoryTier1() == null ? "" : inputMap.getClosureProductCategoryTier1())  + "</urn:Closure_Product_Category_Tier1>\n");
	        xmlRequest.append("         <urn:Closure_Product_Category_Tier2>" + (inputMap.getClosureProductCategoryTier2() == null ? "" : inputMap.getClosureProductCategoryTier2())  + "</urn:Closure_Product_Category_Tier2>\n");
	        xmlRequest.append("         <urn:Closure_Product_Category_Tier3>" + (inputMap.getClosureProductCategoryTier3() == null ? "" : inputMap.getClosureProductCategoryTier3())  + "</urn:Closure_Product_Category_Tier3>\n");
	        xmlRequest.append("         <urn:Closure_Product_Model_Version>" + (inputMap.getClosureProductModelVersion() == null ? "" : inputMap.getClosureProductModelVersion())  + "</urn:Closure_Product_Model_Version>\n");
	        xmlRequest.append("         <urn:Closure_Product_Name>" + (inputMap.getClosureProductName() == null ? "" : inputMap.getClosureProductName())  + "</urn:Closure_Product_Name>\n");
	        xmlRequest.append("         <urn:Department>" + (inputMap.getDepartment() == null ? "" : inputMap.getDepartment())  + "</urn:Department>\n");
	        xmlRequest.append("         <urn:First_Name>" + (inputMap.getFirstName() == null ? "" : inputMap.getFirstName())  + "</urn:First_Name>\n");
	        xmlRequest.append("         <urn:Impact>" + (inputMap.getImpact() == null ? "" : inputMap.getImpact())  + "</urn:Impact>\n");
	        xmlRequest.append("         <urn:Last_Name>" + (inputMap.getLastName() == null ? "" : inputMap.getLastName()) + "</urn:Last_Name>\n");
	        xmlRequest.append("         <urn:Lookup_Keyword>" + (inputMap.getLookupKeyword() == null ? "" : inputMap.getLookupKeyword())  + "</urn:Lookup_Keyword>\n");
	        xmlRequest.append("         <urn:Manufacturer>" + (inputMap.getManufacturer() == null ? "" : inputMap.getManufacturer())  + "</urn:Manufacturer>\n");
	        xmlRequest.append("         <urn:Product_Categorization_Tier_1>" + (inputMap.getProductCategorizationTier1() == null ? "" : inputMap.getProductCategorizationTier1())  + "</urn:Product_Categorization_Tier_1>\n");
	        xmlRequest.append("         <urn:Product_Categorization_Tier_2>" + (inputMap.getProductCategorizationTier2() == null ? "" : inputMap.getProductCategorizationTier2())  + "</urn:Product_Categorization_Tier_2>\n");
	        xmlRequest.append("         <urn:Product_Categorization_Tier_3>" + (inputMap.getProductCategorizationTier3() == null ? "" : inputMap.getProductCategorizationTier3())  + "</urn:Product_Categorization_Tier_3>\n");
	        xmlRequest.append("         <urn:Product_Model_Version>" + (inputMap.getProductModelVersion() == null ? "" : inputMap.getProductModelVersion())  + "</urn:Product_Model_Version>\n");
	        xmlRequest.append("         <urn:Product_Name>" + (inputMap.getProductName() == null ? "" : inputMap.getProductName())  + "</urn:Product_Name>\n");
	        xmlRequest.append("         <urn:Reported_Source>" + (inputMap.getReportedSource() == null ? "" : inputMap.getReportedSource().value())  + "</urn:Reported_Source>\n");
	        xmlRequest.append("         <urn:Resolution>" + (inputMap.getResolution() == null ? "" : inputMap.getResolution())  + "</urn:Resolution>\n");
	        xmlRequest.append("         <urn:Resolution_Category_Tier_1>" + (inputMap.getResolutionCategoryTier1() == null ? "" : inputMap.getResolutionCategoryTier1())  + "</urn:Resolution_Category_Tier_1>\n");
	        xmlRequest.append("         <urn:Resolution_Category_Tier_2>" + (inputMap.getResolutionCategoryTier2() == null ? "" : inputMap.getResolutionCategoryTier2())  + "</urn:Resolution_Category_Tier_2>\n");
	        xmlRequest.append("         <urn:Resolution_Category_Tier_3>" + (inputMap.getResolutionCategoryTier3() == null ? "" : inputMap.getResolutionCategoryTier3())  + "</urn:Resolution_Category_Tier_3>\n");
	        xmlRequest.append("         <urn:Service_Type>" + (inputMap.getServiceType() == null ? "" : inputMap.getServiceType().value())  + "</urn:Service_Type>\n");
	        xmlRequest.append("         <urn:Status>" + (inputMap.getStatus() == null ? "" : inputMap.getStatus().value()) + "</urn:Status>\n");
	        xmlRequest.append("         <urn:Action>" + (inputMap.getAction() == null ? "" : inputMap.getAction()) + "</urn:Action>\n");
	        xmlRequest.append("         <urn:Create_Request>" + (inputMap.getCreateRequest() == null ? "" : inputMap.getCreateRequest().value())  + "</urn:Create_Request>\n");
	        xmlRequest.append("         <urn:Summary>" + (inputMap.getSummary() == null ? "" : inputMap.getSummary())  + "</urn:Summary>\n");
	        xmlRequest.append("         <urn:Notes>" + (inputMap.getNotes() == null ? "" : inputMap.getNotes())  + "</urn:Notes>\n");
	        xmlRequest.append("         <urn:Urgency>" + (inputMap.getUrgency() == null ? "" : inputMap.getUrgency())  + "</urn:Urgency>\n");
	        xmlRequest.append("         <urn:Work_Info_Summary>" + (inputMap.getWorkInfoSummary() == null ? "" : inputMap.getWorkInfoSummary())  + "</urn:Work_Info_Summary>\n");
	        xmlRequest.append("         <urn:Work_Info_Notes>" + (inputMap.getWorkInfoNotes() == null ? "" : inputMap.getWorkInfoNotes())  + "</urn:Work_Info_Notes>\n");
	        xmlRequest.append("         <urn:Work_Info_Type>" + (inputMap.getWorkInfoType() == null ? "" : inputMap.getWorkInfoType().value()) + "</urn:Work_Info_Type>\n");
	        xmlRequest.append("         <urn:Work_Info_Date>" + (inputMap.getWorkInfoDate() == null ? "" : inputMap.getAssignedGroupShiftName())  + "</urn:Work_Info_Date>\n");
	        xmlRequest.append("         <urn:Work_Info_Source>" + (inputMap.getWorkInfoSource() == null ? "" : inputMap.getWorkInfoSource().value()) + "</urn:Work_Info_Source>\n");
	        xmlRequest.append("         <urn:Work_Info_Locked>" + (inputMap.getWorkInfoLocked() == null ? "" : inputMap.getWorkInfoLocked().value()) + "</urn:Work_Info_Locked>\n");
	        xmlRequest.append("         <urn:Work_Info_View_Access>" + (inputMap.getWorkInfoViewAccess() == null ? "" : inputMap.getResolutionCategoryTier3()) + "</urn:Work_Info_View_Access>\n");
	        xmlRequest.append("         <urn:Middle_Initial>" + (inputMap.getMiddleInitial()) + "</urn:Middle_Initial>\n");
	        xmlRequest.append("         <urn:Status_Reason>" + (inputMap.getStatusReason() == null ? "" : inputMap.getStatusReason().value()) + "</urn:Status_Reason>\n");
	        xmlRequest.append("         <urn:Direct_Contact_First_Name>" + (inputMap.getDirectContactFirstName() == null ? "" : inputMap.getDirectContactFirstName())  + "</urn:Direct_Contact_First_Name>\n");
	        xmlRequest.append("         <urn:Direct_Contact_Middle_Initial>" + (inputMap.getDirectContactMiddleInitial() == null ? "" : inputMap.getDirectContactMiddleInitial())  + "</urn:Direct_Contact_Middle_Initial>\n");
	        xmlRequest.append("         <urn:Direct_Contact_Last_Name>" + (inputMap.getDirectContactLastName() == null ? "" : inputMap.getDirectContactLastName())  + "</urn:Direct_Contact_Last_Name>\n");
	        xmlRequest.append("         <urn:TemplateID>" + (inputMap.getTemplateID() == null ? "" : inputMap.getTemplateID())  + "</urn:TemplateID>\n");
	        xmlRequest.append("         <urn:ServiceCI>" + (inputMap.getServiceCI() == null ? "" : inputMap.getServiceCI())  + "</urn:ServiceCI>\n");
	        xmlRequest.append("         <urn:ServiceCI_ReconID>" + (inputMap.getServiceCIReconID() == null ? "" : inputMap.getServiceCIReconID())  + "</urn:ServiceCI_ReconID>\n");
	        xmlRequest.append("         <urn:HPD_CI>" + (inputMap.getHPDCI() == null ? "" : inputMap.getHPDCI())  + "</urn:HPD_CI>\n");
	        xmlRequest.append("         <urn:HPD_CI_ReconID>" + (inputMap.getHPDCIReconID() == null ? "" : inputMap.getHPDCIReconID()) + "</urn:HPD_CI_ReconID>\n");
	        xmlRequest.append("         <urn:HPD_CI_FormName>" + (inputMap.getHPDCIFormName() == null ? "" : inputMap.getHPDCIFormName())  + "</urn:HPD_CI_FormName>\n");
	        xmlRequest.append("         <urn:WorkInfoAttachment1Name>" + (inputMap.getWorkInfoAttachment1Name() == null ? "" : inputMap.getWorkInfoAttachment1Name())  + "</urn:WorkInfoAttachment1Name>\n");
	        xmlRequest.append("         <urn:WorkInfoAttachment1Data>" + (inputMap.getWorkInfoAttachment1Data() == null ? "" : new String(inputMap.getWorkInfoAttachment1Data()) )  + "</urn:WorkInfoAttachment1Data>\n");
	        xmlRequest.append("         <urn:WorkInfoAttachment1OrigSize>" + (inputMap.getWorkInfoAttachment1OrigSize() == null ? "" : inputMap.getWorkInfoAttachment1OrigSize())  + "</urn:WorkInfoAttachment1OrigSize>\n");
	        xmlRequest.append("         <urn:Login_ID>" + (inputMap.getLoginID() == null ? "" : inputMap.getLoginID())  + "</urn:Login_ID>\n");
	        xmlRequest.append("         <urn:Customer_Company>" + (inputMap.getCustomerCompany() == null ? "" : inputMap.getCustomerCompany())  + "</urn:Customer_Company>\n");
	        xmlRequest.append("         <urn:Corporate_ID>" + (inputMap.getCorporateID() == null ? "" : inputMap.getCorporateID())  + "</urn:Corporate_ID>\n");
	        xmlRequest.append("      </urn:HelpDesk_Submit_Service>\n");
	        xmlRequest.append("   </soapenv:Body>");
	        xmlRequest.append("</soapenv:Envelope>");
	        
	        try {
	        	LOG.info("method helpDeskSubmitService in class RemedyClient calling incident_interface_create service");
	            HttpClient httpclient = HTTPUtil.getHttpClient();
	            HttpPost httppost = new HttpPost(endpoint.replace(oldproto, proto));
	            httppost.setHeader("SOAPAction", "urn:HPD_IncidentInterface_Create_WS");
	            httppost.setEntity(new StringEntity(xmlRequest.toString()));
	            HttpResponse response = httpclient.execute(httppost);
	            String responseBody = EntityUtils.toString(response.getEntity(),"UTF-8");
	            return response.getStatusLine().getStatusCode() == 200 ? parseRemedyResult(responseBody) : null;
	        } catch (Exception ex) {
	            ex.printStackTrace();
	            return null;
	        }
		} else {
			LOG.error("method helpDeskSubmitService in class RemedyClient has not correct params");
			return null;
		}

	}
	
	private Boolean validateInputMapAndParameters(CreateInputMap inputMap ,AuthenticationInfo parameters) {
		return (!Strings.isNullOrEmpty(parameters.getUserName()) &&
				!Strings.isNullOrEmpty(parameters.getPassword()) &&
				!Strings.isNullOrEmpty(inputMap.getImpact()) &&
				!Strings.isNullOrEmpty(inputMap.getUrgency()) &&
				!Strings.isNullOrEmpty(inputMap.getSummary()) &&
				!Strings.isNullOrEmpty(inputMap.getNotes()) &&
				!Strings.isNullOrEmpty(inputMap.getAction()) &&
				!Strings.isNullOrEmpty(inputMap.getFirstName()) &&
				!Strings.isNullOrEmpty(inputMap.getLastName()) &&
				inputMap.getServiceType() != null &&
				inputMap.getReportedSource() != null &&
				inputMap.getStatus()!= null ) 
				? Boolean.TRUE : Boolean.FALSE;
	}
	
	
	private static String parseRemedyResult(String resultEntity) {
        String result = "";
        if (resultEntity != null) {
	        try {
	            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	            dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
	            DocumentBuilder db = dbf.newDocumentBuilder();
				Document dom = db.parse(new ByteArrayInputStream(resultEntity.getBytes()));
				dom.getDocumentElement().normalize();
				NodeList nodeList = dom.getElementsByTagName("ns0:HelpDesk_Submit_ServiceResponse");
				Node node = nodeList.item(0);
				Element element = (Element) node;
				return element.getElementsByTagName("ns0:Incident_Number").item(0).getTextContent();
	        } catch (Exception pce) {
	            return result;
	        }
        }
        return result;
    }
	
}
