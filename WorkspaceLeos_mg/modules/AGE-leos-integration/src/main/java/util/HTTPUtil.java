package util;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class HTTPUtil {
	HTTPUtil (){}
	public static HttpClient getHttpClient() {
        CloseableHttpClient client = null;
        
		System.out.println("DEBUG: Using direct connection!");
		client = HttpClients.createDefault();     	        	

        return client;
    }
}
