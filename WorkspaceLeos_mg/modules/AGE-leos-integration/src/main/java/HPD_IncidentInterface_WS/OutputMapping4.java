
package HPD_IncidentInterface_WS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para OutputMapping4 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OutputMapping4">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ListWOQuery" type="{urn:HPD_IncidentInterface_WS}ListWOObjectQuery"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutputMapping4", propOrder = {
    "listWOQuery"
})
public class OutputMapping4 {

    @XmlElement(name = "ListWOQuery", required = true)
    protected ListWOObjectQuery listWOQuery;

    /**
     * Obtiene el valor de la propiedad listWOQuery.
     * 
     * @return
     *     possible object is
     *     {@link ListWOObjectQuery }
     *     
     */
    public ListWOObjectQuery getListWOQuery() {
        return listWOQuery;
    }

    /**
     * Define el valor de la propiedad listWOQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link ListWOObjectQuery }
     *     
     */
    public void setListWOQuery(ListWOObjectQuery value) {
        this.listWOQuery = value;
    }

}
