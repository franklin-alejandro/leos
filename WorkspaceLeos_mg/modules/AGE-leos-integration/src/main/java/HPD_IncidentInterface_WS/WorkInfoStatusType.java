
package HPD_IncidentInterface_WS;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para WorkInfoStatusType.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="WorkInfoStatusType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Proposed"/>
 *     &lt;enumeration value="Enabled"/>
 *     &lt;enumeration value="Offline"/>
 *     &lt;enumeration value="Obsolete"/>
 *     &lt;enumeration value="Archive"/>
 *     &lt;enumeration value="Delete"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "WorkInfoStatusType")
@XmlEnum
public enum WorkInfoStatusType {

    @XmlEnumValue("Proposed")
    PROPOSED("Proposed"),
    @XmlEnumValue("Enabled")
    ENABLED("Enabled"),
    @XmlEnumValue("Offline")
    OFFLINE("Offline"),
    @XmlEnumValue("Obsolete")
    OBSOLETE("Obsolete"),
    @XmlEnumValue("Archive")
    ARCHIVE("Archive"),
    @XmlEnumValue("Delete")
    DELETE("Delete");
    private final String value;

    WorkInfoStatusType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static WorkInfoStatusType fromValue(String v) {
        for (WorkInfoStatusType c: WorkInfoStatusType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
