
package HPD_IncidentInterface_WS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ListQueryWorkInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ListQueryWorkInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://tempuri.org}ListWOQuery"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListQueryWorkInfo", namespace = "http://tempuri.org", propOrder = {
    "listWOQuery"
})
public class ListQueryWorkInfo {

    @XmlElement(name = "ListWOQuery", required = true)
    protected ListWOObjectQuery2 listWOQuery;

    /**
     * Obtiene el valor de la propiedad listWOQuery.
     * 
     * @return
     *     possible object is
     *     {@link ListWOObjectQuery2 }
     *     
     */
    public ListWOObjectQuery2 getListWOQuery() {
        return listWOQuery;
    }

    /**
     * Define el valor de la propiedad listWOQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link ListWOObjectQuery2 }
     *     
     */
    public void setListWOQuery(ListWOObjectQuery2 value) {
        this.listWOQuery = value;
    }

}
