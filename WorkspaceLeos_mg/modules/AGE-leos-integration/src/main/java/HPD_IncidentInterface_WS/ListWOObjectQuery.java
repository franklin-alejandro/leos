
package HPD_IncidentInterface_WS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para ListWOObjectQuery complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ListWOObjectQuery">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getListValues" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="WorkInfoInstanceID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="WorkInfoStatus" type="{urn:HPD_IncidentInterface_WS}WorkInfoStatusType"/>
 *                   &lt;element name="WorkInfoType" type="{urn:HPD_IncidentInterface_WS}Work_Info_TypeType"/>
 *                   &lt;element name="WorkInfoCommSource" type="{urn:HPD_IncidentInterface_WS}Work_Info_SourceType"/>
 *                   &lt;element name="WorkInfoSummary" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="WorkInfoNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ParentID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="WorkInfoSecureLog" type="{urn:HPD_IncidentInterface_WS}VIPType"/>
 *                   &lt;element name="WorkInfoSubmitDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="WorkInfoAttachment1Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="WorkInfoAttachment1Data" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                   &lt;element name="WorkInfoAttachment1OrigSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="WorkInfoAttachment2Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="WorkInfoAttachment2Data" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                   &lt;element name="WorkInfoAttachment2OrigSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="WorkInfoAttachment3Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="WorkInfoAttachment3Data" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                   &lt;element name="WorkInfoAttachment3OrigSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListWOObjectQuery", propOrder = {
    "getListValues"
})
public class ListWOObjectQuery {

    protected List<ListWOObjectQuery.GetListValues> getListValues;

    /**
     * Gets the value of the getListValues property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getListValues property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetListValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ListWOObjectQuery.GetListValues }
     * 
     * 
     */
    public List<ListWOObjectQuery.GetListValues> getGetListValues() {
        if (getListValues == null) {
            getListValues = new ArrayList<ListWOObjectQuery.GetListValues>();
        }
        return this.getListValues;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="WorkInfoInstanceID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="WorkInfoStatus" type="{urn:HPD_IncidentInterface_WS}WorkInfoStatusType"/>
     *         &lt;element name="WorkInfoType" type="{urn:HPD_IncidentInterface_WS}Work_Info_TypeType"/>
     *         &lt;element name="WorkInfoCommSource" type="{urn:HPD_IncidentInterface_WS}Work_Info_SourceType"/>
     *         &lt;element name="WorkInfoSummary" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="WorkInfoNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ParentID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="WorkInfoSecureLog" type="{urn:HPD_IncidentInterface_WS}VIPType"/>
     *         &lt;element name="WorkInfoSubmitDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="WorkInfoAttachment1Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="WorkInfoAttachment1Data" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *         &lt;element name="WorkInfoAttachment1OrigSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="WorkInfoAttachment2Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="WorkInfoAttachment2Data" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *         &lt;element name="WorkInfoAttachment2OrigSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="WorkInfoAttachment3Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="WorkInfoAttachment3Data" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *         &lt;element name="WorkInfoAttachment3OrigSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "workInfoInstanceID",
        "workInfoStatus",
        "workInfoType",
        "workInfoCommSource",
        "workInfoSummary",
        "workInfoNotes",
        "parentID",
        "workInfoSecureLog",
        "workInfoSubmitDate",
        "workInfoAttachment1Name",
        "workInfoAttachment1Data",
        "workInfoAttachment1OrigSize",
        "workInfoAttachment2Name",
        "workInfoAttachment2Data",
        "workInfoAttachment2OrigSize",
        "workInfoAttachment3Name",
        "workInfoAttachment3Data",
        "workInfoAttachment3OrigSize"
    })
    public static class GetListValues {

        @XmlElement(name = "WorkInfoInstanceID", required = true)
        protected String workInfoInstanceID;
        @XmlElement(name = "WorkInfoStatus", required = true)
        @XmlSchemaType(name = "string")
        protected WorkInfoStatusType workInfoStatus;
        @XmlElement(name = "WorkInfoType", required = true, nillable = true)
        @XmlSchemaType(name = "string")
        protected WorkInfoTypeType workInfoType;
        @XmlElement(name = "WorkInfoCommSource", required = true, nillable = true)
        @XmlSchemaType(name = "string")
        protected WorkInfoSourceType workInfoCommSource;
        @XmlElement(name = "WorkInfoSummary", required = true)
        protected String workInfoSummary;
        @XmlElement(name = "WorkInfoNotes", required = true)
        protected String workInfoNotes;
        @XmlElement(name = "ParentID", required = true)
        protected String parentID;
        @XmlElement(name = "WorkInfoSecureLog", required = true, nillable = true)
        @XmlSchemaType(name = "string")
        protected VIPType workInfoSecureLog;
        @XmlElement(name = "WorkInfoSubmitDate")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar workInfoSubmitDate;
        @XmlElement(name = "WorkInfoAttachment1Name")
        protected String workInfoAttachment1Name;
        @XmlElement(name = "WorkInfoAttachment1Data", required = true)
        protected byte[] workInfoAttachment1Data;
        @XmlElement(name = "WorkInfoAttachment1OrigSize")
        protected int workInfoAttachment1OrigSize;
        @XmlElement(name = "WorkInfoAttachment2Name")
        protected String workInfoAttachment2Name;
        @XmlElement(name = "WorkInfoAttachment2Data", required = true)
        protected byte[] workInfoAttachment2Data;
        @XmlElement(name = "WorkInfoAttachment2OrigSize")
        protected int workInfoAttachment2OrigSize;
        @XmlElement(name = "WorkInfoAttachment3Name")
        protected String workInfoAttachment3Name;
        @XmlElement(name = "WorkInfoAttachment3Data", required = true)
        protected byte[] workInfoAttachment3Data;
        @XmlElement(name = "WorkInfoAttachment3OrigSize")
        protected int workInfoAttachment3OrigSize;

        /**
         * Obtiene el valor de la propiedad workInfoInstanceID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWorkInfoInstanceID() {
            return workInfoInstanceID;
        }

        /**
         * Define el valor de la propiedad workInfoInstanceID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWorkInfoInstanceID(String value) {
            this.workInfoInstanceID = value;
        }

        /**
         * Obtiene el valor de la propiedad workInfoStatus.
         * 
         * @return
         *     possible object is
         *     {@link WorkInfoStatusType }
         *     
         */
        public WorkInfoStatusType getWorkInfoStatus() {
            return workInfoStatus;
        }

        /**
         * Define el valor de la propiedad workInfoStatus.
         * 
         * @param value
         *     allowed object is
         *     {@link WorkInfoStatusType }
         *     
         */
        public void setWorkInfoStatus(WorkInfoStatusType value) {
            this.workInfoStatus = value;
        }

        /**
         * Obtiene el valor de la propiedad workInfoType.
         * 
         * @return
         *     possible object is
         *     {@link WorkInfoTypeType }
         *     
         */
        public WorkInfoTypeType getWorkInfoType() {
            return workInfoType;
        }

        /**
         * Define el valor de la propiedad workInfoType.
         * 
         * @param value
         *     allowed object is
         *     {@link WorkInfoTypeType }
         *     
         */
        public void setWorkInfoType(WorkInfoTypeType value) {
            this.workInfoType = value;
        }

        /**
         * Obtiene el valor de la propiedad workInfoCommSource.
         * 
         * @return
         *     possible object is
         *     {@link WorkInfoSourceType }
         *     
         */
        public WorkInfoSourceType getWorkInfoCommSource() {
            return workInfoCommSource;
        }

        /**
         * Define el valor de la propiedad workInfoCommSource.
         * 
         * @param value
         *     allowed object is
         *     {@link WorkInfoSourceType }
         *     
         */
        public void setWorkInfoCommSource(WorkInfoSourceType value) {
            this.workInfoCommSource = value;
        }

        /**
         * Obtiene el valor de la propiedad workInfoSummary.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWorkInfoSummary() {
            return workInfoSummary;
        }

        /**
         * Define el valor de la propiedad workInfoSummary.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWorkInfoSummary(String value) {
            this.workInfoSummary = value;
        }

        /**
         * Obtiene el valor de la propiedad workInfoNotes.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWorkInfoNotes() {
            return workInfoNotes;
        }

        /**
         * Define el valor de la propiedad workInfoNotes.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWorkInfoNotes(String value) {
            this.workInfoNotes = value;
        }

        /**
         * Obtiene el valor de la propiedad parentID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getParentID() {
            return parentID;
        }

        /**
         * Define el valor de la propiedad parentID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setParentID(String value) {
            this.parentID = value;
        }

        /**
         * Obtiene el valor de la propiedad workInfoSecureLog.
         * 
         * @return
         *     possible object is
         *     {@link VIPType }
         *     
         */
        public VIPType getWorkInfoSecureLog() {
            return workInfoSecureLog;
        }

        /**
         * Define el valor de la propiedad workInfoSecureLog.
         * 
         * @param value
         *     allowed object is
         *     {@link VIPType }
         *     
         */
        public void setWorkInfoSecureLog(VIPType value) {
            this.workInfoSecureLog = value;
        }

        /**
         * Obtiene el valor de la propiedad workInfoSubmitDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getWorkInfoSubmitDate() {
            return workInfoSubmitDate;
        }

        /**
         * Define el valor de la propiedad workInfoSubmitDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setWorkInfoSubmitDate(XMLGregorianCalendar value) {
            this.workInfoSubmitDate = value;
        }

        /**
         * Obtiene el valor de la propiedad workInfoAttachment1Name.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWorkInfoAttachment1Name() {
            return workInfoAttachment1Name;
        }

        /**
         * Define el valor de la propiedad workInfoAttachment1Name.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWorkInfoAttachment1Name(String value) {
            this.workInfoAttachment1Name = value;
        }

        /**
         * Obtiene el valor de la propiedad workInfoAttachment1Data.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getWorkInfoAttachment1Data() {
            return workInfoAttachment1Data;
        }

        /**
         * Define el valor de la propiedad workInfoAttachment1Data.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setWorkInfoAttachment1Data(byte[] value) {
            this.workInfoAttachment1Data = value;
        }

        /**
         * Obtiene el valor de la propiedad workInfoAttachment1OrigSize.
         * 
         */
        public int getWorkInfoAttachment1OrigSize() {
            return workInfoAttachment1OrigSize;
        }

        /**
         * Define el valor de la propiedad workInfoAttachment1OrigSize.
         * 
         */
        public void setWorkInfoAttachment1OrigSize(int value) {
            this.workInfoAttachment1OrigSize = value;
        }

        /**
         * Obtiene el valor de la propiedad workInfoAttachment2Name.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWorkInfoAttachment2Name() {
            return workInfoAttachment2Name;
        }

        /**
         * Define el valor de la propiedad workInfoAttachment2Name.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWorkInfoAttachment2Name(String value) {
            this.workInfoAttachment2Name = value;
        }

        /**
         * Obtiene el valor de la propiedad workInfoAttachment2Data.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getWorkInfoAttachment2Data() {
            return workInfoAttachment2Data;
        }

        /**
         * Define el valor de la propiedad workInfoAttachment2Data.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setWorkInfoAttachment2Data(byte[] value) {
            this.workInfoAttachment2Data = value;
        }

        /**
         * Obtiene el valor de la propiedad workInfoAttachment2OrigSize.
         * 
         */
        public int getWorkInfoAttachment2OrigSize() {
            return workInfoAttachment2OrigSize;
        }

        /**
         * Define el valor de la propiedad workInfoAttachment2OrigSize.
         * 
         */
        public void setWorkInfoAttachment2OrigSize(int value) {
            this.workInfoAttachment2OrigSize = value;
        }

        /**
         * Obtiene el valor de la propiedad workInfoAttachment3Name.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWorkInfoAttachment3Name() {
            return workInfoAttachment3Name;
        }

        /**
         * Define el valor de la propiedad workInfoAttachment3Name.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWorkInfoAttachment3Name(String value) {
            this.workInfoAttachment3Name = value;
        }

        /**
         * Obtiene el valor de la propiedad workInfoAttachment3Data.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getWorkInfoAttachment3Data() {
            return workInfoAttachment3Data;
        }

        /**
         * Define el valor de la propiedad workInfoAttachment3Data.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setWorkInfoAttachment3Data(byte[] value) {
            this.workInfoAttachment3Data = value;
        }

        /**
         * Obtiene el valor de la propiedad workInfoAttachment3OrigSize.
         * 
         */
        public int getWorkInfoAttachment3OrigSize() {
            return workInfoAttachment3OrigSize;
        }

        /**
         * Define el valor de la propiedad workInfoAttachment3OrigSize.
         * 
         */
        public void setWorkInfoAttachment3OrigSize(int value) {
            this.workInfoAttachment3OrigSize = value;
        }

    }

}
