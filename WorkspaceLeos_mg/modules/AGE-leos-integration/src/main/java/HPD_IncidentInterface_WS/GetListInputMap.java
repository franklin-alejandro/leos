
package HPD_IncidentInterface_WS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para GetListInputMap complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="GetListInputMap">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Qualification" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="startRecord" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="maxLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetListInputMap", propOrder = {
    "qualification",
    "startRecord",
    "maxLimit"
})
public class GetListInputMap {

    @XmlElement(name = "Qualification", required = true)
    protected String qualification;
    @XmlElement(required = true)
    protected String startRecord;
    @XmlElement(required = true)
    protected String maxLimit;

    /**
     * Obtiene el valor de la propiedad qualification.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQualification() {
        return qualification;
    }

    /**
     * Define el valor de la propiedad qualification.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQualification(String value) {
        this.qualification = value;
    }

    /**
     * Obtiene el valor de la propiedad startRecord.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartRecord() {
        return startRecord;
    }

    /**
     * Define el valor de la propiedad startRecord.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartRecord(String value) {
        this.startRecord = value;
    }

    /**
     * Obtiene el valor de la propiedad maxLimit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxLimit() {
        return maxLimit;
    }

    /**
     * Define el valor de la propiedad maxLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxLimit(String value) {
        this.maxLimit = value;
    }

}
