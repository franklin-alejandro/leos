
package HPD_IncidentInterface_WS;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para SetInputMap complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SetInputMap">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Categorization_Tier_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Categorization_Tier_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Categorization_Tier_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Closure_Manufacturer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Closure_Product_Category_Tier1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Closure_Product_Category_Tier2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Closure_Product_Category_Tier3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Closure_Product_Model_Version" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Closure_Product_Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Summary" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Notes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Impact" type="{urn:HPD_IncidentInterface_WS}ImpactType"/>
 *         &lt;element name="Manufacturer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Product_Categorization_Tier_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Product_Categorization_Tier_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Product_Categorization_Tier_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Product_Model_Version" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Product_Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Reported_Source" type="{urn:HPD_IncidentInterface_WS}Reported_SourceType"/>
 *         &lt;element name="Resolution" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Resolution_Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Resolution_Category_Tier_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Resolution_Category_Tier_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Resolution_Method" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Service_Type" type="{urn:HPD_IncidentInterface_WS}Service_TypeType"/>
 *         &lt;element name="Status" type="{urn:HPD_IncidentInterface_WS}StatusType"/>
 *         &lt;element name="Urgency" type="{urn:HPD_IncidentInterface_WS}UrgencyType"/>
 *         &lt;element name="Action" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Work_Info_Summary" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Work_Info_Notes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Work_Info_Type" type="{urn:HPD_IncidentInterface_WS}Work_Info_TypeType"/>
 *         &lt;element name="Work_Info_Date" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Work_Info_Source" type="{urn:HPD_IncidentInterface_WS}Work_Info_SourceType"/>
 *         &lt;element name="Work_Info_Locked" type="{urn:HPD_IncidentInterface_WS}VIPType"/>
 *         &lt;element name="Work_Info_View_Access" type="{urn:HPD_IncidentInterface_WS}Work_Info_View_AccessType"/>
 *         &lt;element name="Incident_Number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ServiceCI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ServiceCI_ReconID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HPD_CI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HPD_CI_ReconID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HPD_CI_FormName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="z1D_CI_FormName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WorkInfoAttachment1Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WorkInfoAttachment1Data" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="WorkInfoAttachment1OrigSize" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Status_Reason" type="{urn:HPD_IncidentInterface_WS}Status_ReasonType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SetInputMap", propOrder = {
    "categorizationTier1",
    "categorizationTier2",
    "categorizationTier3",
    "closureManufacturer",
    "closureProductCategoryTier1",
    "closureProductCategoryTier2",
    "closureProductCategoryTier3",
    "closureProductModelVersion",
    "closureProductName",
    "company",
    "summary",
    "notes",
    "impact",
    "manufacturer",
    "productCategorizationTier1",
    "productCategorizationTier2",
    "productCategorizationTier3",
    "productModelVersion",
    "productName",
    "reportedSource",
    "resolution",
    "resolutionCategory",
    "resolutionCategoryTier2",
    "resolutionCategoryTier3",
    "resolutionMethod",
    "serviceType",
    "status",
    "urgency",
    "action",
    "workInfoSummary",
    "workInfoNotes",
    "workInfoType",
    "workInfoDate",
    "workInfoSource",
    "workInfoLocked",
    "workInfoViewAccess",
    "incidentNumber",
    "serviceCI",
    "serviceCIReconID",
    "hpdci",
    "hpdciReconID",
    "hpdciFormName",
    "z1DCIFormName",
    "workInfoAttachment1Name",
    "workInfoAttachment1Data",
    "workInfoAttachment1OrigSize",
    "statusReason"
})
public class SetInputMap {

    @XmlElement(name = "Categorization_Tier_1", required = true)
    protected String categorizationTier1;
    @XmlElement(name = "Categorization_Tier_2", required = true)
    protected String categorizationTier2;
    @XmlElement(name = "Categorization_Tier_3", required = true)
    protected String categorizationTier3;
    @XmlElement(name = "Closure_Manufacturer", required = true)
    protected String closureManufacturer;
    @XmlElement(name = "Closure_Product_Category_Tier1", required = true)
    protected String closureProductCategoryTier1;
    @XmlElement(name = "Closure_Product_Category_Tier2", required = true)
    protected String closureProductCategoryTier2;
    @XmlElement(name = "Closure_Product_Category_Tier3", required = true)
    protected String closureProductCategoryTier3;
    @XmlElement(name = "Closure_Product_Model_Version", required = true)
    protected String closureProductModelVersion;
    @XmlElement(name = "Closure_Product_Name", required = true)
    protected String closureProductName;
    @XmlElement(name = "Company", required = true)
    protected String company;
    @XmlElement(name = "Summary", required = true)
    protected String summary;
    @XmlElement(name = "Notes", required = true)
    protected String notes;
    @XmlElement(name = "Impact", required = true, nillable = true)
    protected String impact;
    @XmlElement(name = "Manufacturer", required = true)
    protected String manufacturer;
    @XmlElement(name = "Product_Categorization_Tier_1")
    protected String productCategorizationTier1;
    @XmlElement(name = "Product_Categorization_Tier_2", required = true)
    protected String productCategorizationTier2;
    @XmlElement(name = "Product_Categorization_Tier_3", required = true)
    protected String productCategorizationTier3;
    @XmlElement(name = "Product_Model_Version", required = true)
    protected String productModelVersion;
    @XmlElement(name = "Product_Name", required = true)
    protected String productName;
    @XmlElement(name = "Reported_Source", required = true, nillable = true)
    @XmlSchemaType(name = "string")
    protected ReportedSourceType reportedSource;
    @XmlElement(name = "Resolution", required = true)
    protected String resolution;
    @XmlElement(name = "Resolution_Category", required = true)
    protected String resolutionCategory;
    @XmlElement(name = "Resolution_Category_Tier_2", required = true)
    protected String resolutionCategoryTier2;
    @XmlElement(name = "Resolution_Category_Tier_3", required = true)
    protected String resolutionCategoryTier3;
    @XmlElement(name = "Resolution_Method", required = true)
    protected String resolutionMethod;
    @XmlElement(name = "Service_Type", required = true, nillable = true)
    @XmlSchemaType(name = "string")
    protected ServiceTypeType serviceType;
    @XmlElement(name = "Status", required = true)
    @XmlSchemaType(name = "string")
    protected StatusType status;
    @XmlElement(name = "Urgency", required = true, nillable = true)
    protected String urgency;
    @XmlElement(name = "Action", required = true)
    protected String action;
    @XmlElement(name = "Work_Info_Summary", required = true)
    protected String workInfoSummary;
    @XmlElement(name = "Work_Info_Notes", required = true)
    protected String workInfoNotes;
    @XmlElement(name = "Work_Info_Type", required = true, nillable = true)
    @XmlSchemaType(name = "string")
    protected WorkInfoTypeType workInfoType;
    @XmlElement(name = "Work_Info_Date", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar workInfoDate;
    @XmlElement(name = "Work_Info_Source", required = true, nillable = true)
    @XmlSchemaType(name = "string")
    protected WorkInfoSourceType workInfoSource;
    @XmlElement(name = "Work_Info_Locked", required = true, nillable = true)
    @XmlSchemaType(name = "string")
    protected VIPType workInfoLocked;
    @XmlElement(name = "Work_Info_View_Access", required = true, nillable = true)
    @XmlSchemaType(name = "string")
    protected WorkInfoViewAccessType workInfoViewAccess;
    @XmlElement(name = "Incident_Number", required = true)
    protected String incidentNumber;
    @XmlElement(name = "ServiceCI", required = true)
    protected String serviceCI;
    @XmlElement(name = "ServiceCI_ReconID", required = true)
    protected String serviceCIReconID;
    @XmlElement(name = "HPD_CI", required = true)
    protected String hpdci;
    @XmlElement(name = "HPD_CI_ReconID", required = true)
    protected String hpdciReconID;
    @XmlElement(name = "HPD_CI_FormName", required = true)
    protected String hpdciFormName;
    @XmlElement(name = "z1D_CI_FormName", required = true)
    protected String z1DCIFormName;
    @XmlElement(name = "WorkInfoAttachment1Name")
    protected String workInfoAttachment1Name;
    @XmlElement(name = "WorkInfoAttachment1Data")
    protected byte[] workInfoAttachment1Data;
    @XmlElement(name = "WorkInfoAttachment1OrigSize")
    protected Integer workInfoAttachment1OrigSize;
    @XmlElementRef(name = "Status_Reason", namespace = "urn:HPD_IncidentInterface_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<StatusReasonType> statusReason;

    /**
     * Obtiene el valor de la propiedad categorizationTier1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategorizationTier1() {
        return categorizationTier1;
    }

    /**
     * Define el valor de la propiedad categorizationTier1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategorizationTier1(String value) {
        this.categorizationTier1 = value;
    }

    /**
     * Obtiene el valor de la propiedad categorizationTier2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategorizationTier2() {
        return categorizationTier2;
    }

    /**
     * Define el valor de la propiedad categorizationTier2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategorizationTier2(String value) {
        this.categorizationTier2 = value;
    }

    /**
     * Obtiene el valor de la propiedad categorizationTier3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategorizationTier3() {
        return categorizationTier3;
    }

    /**
     * Define el valor de la propiedad categorizationTier3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategorizationTier3(String value) {
        this.categorizationTier3 = value;
    }

    /**
     * Obtiene el valor de la propiedad closureManufacturer.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosureManufacturer() {
        return closureManufacturer;
    }

    /**
     * Define el valor de la propiedad closureManufacturer.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosureManufacturer(String value) {
        this.closureManufacturer = value;
    }

    /**
     * Obtiene el valor de la propiedad closureProductCategoryTier1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosureProductCategoryTier1() {
        return closureProductCategoryTier1;
    }

    /**
     * Define el valor de la propiedad closureProductCategoryTier1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosureProductCategoryTier1(String value) {
        this.closureProductCategoryTier1 = value;
    }

    /**
     * Obtiene el valor de la propiedad closureProductCategoryTier2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosureProductCategoryTier2() {
        return closureProductCategoryTier2;
    }

    /**
     * Define el valor de la propiedad closureProductCategoryTier2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosureProductCategoryTier2(String value) {
        this.closureProductCategoryTier2 = value;
    }

    /**
     * Obtiene el valor de la propiedad closureProductCategoryTier3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosureProductCategoryTier3() {
        return closureProductCategoryTier3;
    }

    /**
     * Define el valor de la propiedad closureProductCategoryTier3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosureProductCategoryTier3(String value) {
        this.closureProductCategoryTier3 = value;
    }

    /**
     * Obtiene el valor de la propiedad closureProductModelVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosureProductModelVersion() {
        return closureProductModelVersion;
    }

    /**
     * Define el valor de la propiedad closureProductModelVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosureProductModelVersion(String value) {
        this.closureProductModelVersion = value;
    }

    /**
     * Obtiene el valor de la propiedad closureProductName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosureProductName() {
        return closureProductName;
    }

    /**
     * Define el valor de la propiedad closureProductName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosureProductName(String value) {
        this.closureProductName = value;
    }

    /**
     * Obtiene el valor de la propiedad company.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompany() {
        return company;
    }

    /**
     * Define el valor de la propiedad company.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompany(String value) {
        this.company = value;
    }

    /**
     * Obtiene el valor de la propiedad summary.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummary() {
        return summary;
    }

    /**
     * Define el valor de la propiedad summary.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummary(String value) {
        this.summary = value;
    }

    /**
     * Obtiene el valor de la propiedad notes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Define el valor de la propiedad notes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

    /**
     * Obtiene el valor de la propiedad impact.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImpact() {
        return impact;
    }

    /**
     * Define el valor de la propiedad impact.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImpact(String value) {
        this.impact = value;
    }

    /**
     * Obtiene el valor de la propiedad manufacturer.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * Define el valor de la propiedad manufacturer.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturer(String value) {
        this.manufacturer = value;
    }

    /**
     * Obtiene el valor de la propiedad productCategorizationTier1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCategorizationTier1() {
        return productCategorizationTier1;
    }

    /**
     * Define el valor de la propiedad productCategorizationTier1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCategorizationTier1(String value) {
        this.productCategorizationTier1 = value;
    }

    /**
     * Obtiene el valor de la propiedad productCategorizationTier2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCategorizationTier2() {
        return productCategorizationTier2;
    }

    /**
     * Define el valor de la propiedad productCategorizationTier2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCategorizationTier2(String value) {
        this.productCategorizationTier2 = value;
    }

    /**
     * Obtiene el valor de la propiedad productCategorizationTier3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCategorizationTier3() {
        return productCategorizationTier3;
    }

    /**
     * Define el valor de la propiedad productCategorizationTier3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCategorizationTier3(String value) {
        this.productCategorizationTier3 = value;
    }

    /**
     * Obtiene el valor de la propiedad productModelVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductModelVersion() {
        return productModelVersion;
    }

    /**
     * Define el valor de la propiedad productModelVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductModelVersion(String value) {
        this.productModelVersion = value;
    }

    /**
     * Obtiene el valor de la propiedad productName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Define el valor de la propiedad productName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductName(String value) {
        this.productName = value;
    }

    /**
     * Obtiene el valor de la propiedad reportedSource.
     * 
     * @return
     *     possible object is
     *     {@link ReportedSourceType }
     *     
     */
    public ReportedSourceType getReportedSource() {
        return reportedSource;
    }

    /**
     * Define el valor de la propiedad reportedSource.
     * 
     * @param value
     *     allowed object is
     *     {@link ReportedSourceType }
     *     
     */
    public void setReportedSource(ReportedSourceType value) {
        this.reportedSource = value;
    }

    /**
     * Obtiene el valor de la propiedad resolution.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResolution() {
        return resolution;
    }

    /**
     * Define el valor de la propiedad resolution.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResolution(String value) {
        this.resolution = value;
    }

    /**
     * Obtiene el valor de la propiedad resolutionCategory.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResolutionCategory() {
        return resolutionCategory;
    }

    /**
     * Define el valor de la propiedad resolutionCategory.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResolutionCategory(String value) {
        this.resolutionCategory = value;
    }

    /**
     * Obtiene el valor de la propiedad resolutionCategoryTier2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResolutionCategoryTier2() {
        return resolutionCategoryTier2;
    }

    /**
     * Define el valor de la propiedad resolutionCategoryTier2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResolutionCategoryTier2(String value) {
        this.resolutionCategoryTier2 = value;
    }

    /**
     * Obtiene el valor de la propiedad resolutionCategoryTier3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResolutionCategoryTier3() {
        return resolutionCategoryTier3;
    }

    /**
     * Define el valor de la propiedad resolutionCategoryTier3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResolutionCategoryTier3(String value) {
        this.resolutionCategoryTier3 = value;
    }

    /**
     * Obtiene el valor de la propiedad resolutionMethod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResolutionMethod() {
        return resolutionMethod;
    }

    /**
     * Define el valor de la propiedad resolutionMethod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResolutionMethod(String value) {
        this.resolutionMethod = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceType.
     * 
     * @return
     *     possible object is
     *     {@link ServiceTypeType }
     *     
     */
    public ServiceTypeType getServiceType() {
        return serviceType;
    }

    /**
     * Define el valor de la propiedad serviceType.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceTypeType }
     *     
     */
    public void setServiceType(ServiceTypeType value) {
        this.serviceType = value;
    }

    /**
     * Obtiene el valor de la propiedad status.
     * 
     * @return
     *     possible object is
     *     {@link StatusType }
     *     
     */
    public StatusType getStatus() {
        return status;
    }

    /**
     * Define el valor de la propiedad status.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusType }
     *     
     */
    public void setStatus(StatusType value) {
        this.status = value;
    }

    /**
     * Obtiene el valor de la propiedad urgency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrgency() {
        return urgency;
    }

    /**
     * Define el valor de la propiedad urgency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrgency(String value) {
        this.urgency = value;
    }

    /**
     * Obtiene el valor de la propiedad action.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAction() {
        return action;
    }

    /**
     * Define el valor de la propiedad action.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAction(String value) {
        this.action = value;
    }

    /**
     * Obtiene el valor de la propiedad workInfoSummary.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkInfoSummary() {
        return workInfoSummary;
    }

    /**
     * Define el valor de la propiedad workInfoSummary.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkInfoSummary(String value) {
        this.workInfoSummary = value;
    }

    /**
     * Obtiene el valor de la propiedad workInfoNotes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkInfoNotes() {
        return workInfoNotes;
    }

    /**
     * Define el valor de la propiedad workInfoNotes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkInfoNotes(String value) {
        this.workInfoNotes = value;
    }

    /**
     * Obtiene el valor de la propiedad workInfoType.
     * 
     * @return
     *     possible object is
     *     {@link WorkInfoTypeType }
     *     
     */
    public WorkInfoTypeType getWorkInfoType() {
        return workInfoType;
    }

    /**
     * Define el valor de la propiedad workInfoType.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkInfoTypeType }
     *     
     */
    public void setWorkInfoType(WorkInfoTypeType value) {
        this.workInfoType = value;
    }

    /**
     * Obtiene el valor de la propiedad workInfoDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWorkInfoDate() {
        return workInfoDate;
    }

    /**
     * Define el valor de la propiedad workInfoDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWorkInfoDate(XMLGregorianCalendar value) {
        this.workInfoDate = value;
    }

    /**
     * Obtiene el valor de la propiedad workInfoSource.
     * 
     * @return
     *     possible object is
     *     {@link WorkInfoSourceType }
     *     
     */
    public WorkInfoSourceType getWorkInfoSource() {
        return workInfoSource;
    }

    /**
     * Define el valor de la propiedad workInfoSource.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkInfoSourceType }
     *     
     */
    public void setWorkInfoSource(WorkInfoSourceType value) {
        this.workInfoSource = value;
    }

    /**
     * Obtiene el valor de la propiedad workInfoLocked.
     * 
     * @return
     *     possible object is
     *     {@link VIPType }
     *     
     */
    public VIPType getWorkInfoLocked() {
        return workInfoLocked;
    }

    /**
     * Define el valor de la propiedad workInfoLocked.
     * 
     * @param value
     *     allowed object is
     *     {@link VIPType }
     *     
     */
    public void setWorkInfoLocked(VIPType value) {
        this.workInfoLocked = value;
    }

    /**
     * Obtiene el valor de la propiedad workInfoViewAccess.
     * 
     * @return
     *     possible object is
     *     {@link WorkInfoViewAccessType }
     *     
     */
    public WorkInfoViewAccessType getWorkInfoViewAccess() {
        return workInfoViewAccess;
    }

    /**
     * Define el valor de la propiedad workInfoViewAccess.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkInfoViewAccessType }
     *     
     */
    public void setWorkInfoViewAccess(WorkInfoViewAccessType value) {
        this.workInfoViewAccess = value;
    }

    /**
     * Obtiene el valor de la propiedad incidentNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncidentNumber() {
        return incidentNumber;
    }

    /**
     * Define el valor de la propiedad incidentNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncidentNumber(String value) {
        this.incidentNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceCI.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceCI() {
        return serviceCI;
    }

    /**
     * Define el valor de la propiedad serviceCI.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceCI(String value) {
        this.serviceCI = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceCIReconID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceCIReconID() {
        return serviceCIReconID;
    }

    /**
     * Define el valor de la propiedad serviceCIReconID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceCIReconID(String value) {
        this.serviceCIReconID = value;
    }

    /**
     * Obtiene el valor de la propiedad hpdci.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHPDCI() {
        return hpdci;
    }

    /**
     * Define el valor de la propiedad hpdci.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHPDCI(String value) {
        this.hpdci = value;
    }

    /**
     * Obtiene el valor de la propiedad hpdciReconID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHPDCIReconID() {
        return hpdciReconID;
    }

    /**
     * Define el valor de la propiedad hpdciReconID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHPDCIReconID(String value) {
        this.hpdciReconID = value;
    }

    /**
     * Obtiene el valor de la propiedad hpdciFormName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHPDCIFormName() {
        return hpdciFormName;
    }

    /**
     * Define el valor de la propiedad hpdciFormName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHPDCIFormName(String value) {
        this.hpdciFormName = value;
    }

    /**
     * Obtiene el valor de la propiedad z1DCIFormName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZ1DCIFormName() {
        return z1DCIFormName;
    }

    /**
     * Define el valor de la propiedad z1DCIFormName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZ1DCIFormName(String value) {
        this.z1DCIFormName = value;
    }

    /**
     * Obtiene el valor de la propiedad workInfoAttachment1Name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkInfoAttachment1Name() {
        return workInfoAttachment1Name;
    }

    /**
     * Define el valor de la propiedad workInfoAttachment1Name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkInfoAttachment1Name(String value) {
        this.workInfoAttachment1Name = value;
    }

    /**
     * Obtiene el valor de la propiedad workInfoAttachment1Data.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getWorkInfoAttachment1Data() {
        return workInfoAttachment1Data;
    }

    /**
     * Define el valor de la propiedad workInfoAttachment1Data.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setWorkInfoAttachment1Data(byte[] value) {
        this.workInfoAttachment1Data = value;
    }

    /**
     * Obtiene el valor de la propiedad workInfoAttachment1OrigSize.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWorkInfoAttachment1OrigSize() {
        return workInfoAttachment1OrigSize;
    }

    /**
     * Define el valor de la propiedad workInfoAttachment1OrigSize.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWorkInfoAttachment1OrigSize(Integer value) {
        this.workInfoAttachment1OrigSize = value;
    }

    /**
     * Obtiene el valor de la propiedad statusReason.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link StatusReasonType }{@code >}
     *     
     */
    public JAXBElement<StatusReasonType> getStatusReason() {
        return statusReason;
    }

    /**
     * Define el valor de la propiedad statusReason.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link StatusReasonType }{@code >}
     *     
     */
    public void setStatusReason(JAXBElement<StatusReasonType> value) {
        this.statusReason = value;
    }

}
