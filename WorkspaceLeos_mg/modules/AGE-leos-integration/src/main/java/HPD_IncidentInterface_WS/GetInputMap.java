
package HPD_IncidentInterface_WS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para GetInputMap complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="GetInputMap">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Incident_Number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetInputMap", propOrder = {
    "incidentNumber"
})
public class GetInputMap {

    @XmlElement(name = "Incident_Number", required = true)
    protected String incidentNumber;

    /**
     * Obtiene el valor de la propiedad incidentNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncidentNumber() {
        return incidentNumber;
    }

    /**
     * Define el valor de la propiedad incidentNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncidentNumber(String value) {
        this.incidentNumber = value;
    }

}
