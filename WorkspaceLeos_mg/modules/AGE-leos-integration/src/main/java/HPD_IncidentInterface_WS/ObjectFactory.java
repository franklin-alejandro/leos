
package HPD_IncidentInterface_WS;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the HPD_IncidentInterface_WS package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _HelpDeskQueryListServiceResponse_QNAME = new QName("urn:HPD_IncidentInterface_WS", "HelpDesk_QueryList_ServiceResponse");
    private final static QName _HelpDeskQueryServiceResponse_QNAME = new QName("urn:HPD_IncidentInterface_WS", "HelpDesk_Query_ServiceResponse");
    private final static QName _HelpDeskGetWorkInfoList_QNAME = new QName("urn:HPD_IncidentInterface_WS", "HelpDesk_GetWorkInfoList");
    private final static QName _HelpDeskQueryService_QNAME = new QName("urn:HPD_IncidentInterface_WS", "HelpDesk_Query_Service");
    private final static QName _HelpDeskQueryListService_QNAME = new QName("urn:HPD_IncidentInterface_WS", "HelpDesk_QueryList_Service");
    private final static QName _HelpDeskGetWorkInfoListResponse_QNAME = new QName("urn:HPD_IncidentInterface_WS", "HelpDesk_GetWorkInfoListResponse");
    private final static QName _AuthenticationInfo_QNAME = new QName("urn:HPD_IncidentInterface_WS", "AuthenticationInfo");
    private final static QName _HelpDeskModifyService_QNAME = new QName("urn:HPD_IncidentInterface_WS", "HelpDesk_Modify_Service");
    private final static QName _ListWOQuery_QNAME = new QName("http://tempuri.org", "ListWOQuery");
    private final static QName _HelpDeskModifyServiceResponse_QNAME = new QName("urn:HPD_IncidentInterface_WS", "HelpDesk_Modify_ServiceResponse");
    private final static QName _GetListOutputMapGetListValuesRequiredResolutionDate_QNAME = new QName("urn:HPD_IncidentInterface_WS", "Required_Resolution_Date");
    private final static QName _GetListOutputMapGetListValuesSubmitDate_QNAME = new QName("urn:HPD_IncidentInterface_WS", "Submit_Date");
    private final static QName _GetListOutputMapGetListValuesStatusReason_QNAME = new QName("urn:HPD_IncidentInterface_WS", "Status_Reason");
    private final static QName _GetListOutputMapGetListValuesTargetDate_QNAME = new QName("urn:HPD_IncidentInterface_WS", "Target_Date");
    private final static QName _GetListOutputMapGetListValuesClosedDate_QNAME = new QName("urn:HPD_IncidentInterface_WS", "Closed_Date");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: HPD_IncidentInterface_WS
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListWOObjectQuery2 }
     * 
     */
    public ListWOObjectQuery2 createListWOObjectQuery2() {
        return new ListWOObjectQuery2();
    }

    /**
     * Create an instance of {@link ListWOObjectQuery }
     * 
     */
    public ListWOObjectQuery createListWOObjectQuery() {
        return new ListWOObjectQuery();
    }

    /**
     * Create an instance of {@link GetListOutputMap }
     * 
     */
    public GetListOutputMap createGetListOutputMap() {
        return new GetListOutputMap();
    }

    /**
     * Create an instance of {@link GetListInputMap }
     * 
     */
    public GetListInputMap createGetListInputMap() {
        return new GetListInputMap();
    }

    /**
     * Create an instance of {@link OutputMapping4 }
     * 
     */
    public OutputMapping4 createOutputMapping4() {
        return new OutputMapping4();
    }

    /**
     * Create an instance of {@link GetOutputMap }
     * 
     */
    public GetOutputMap createGetOutputMap() {
        return new GetOutputMap();
    }

    /**
     * Create an instance of {@link InputMapping4 }
     * 
     */
    public InputMapping4 createInputMapping4() {
        return new InputMapping4();
    }

    /**
     * Create an instance of {@link GetInputMap }
     * 
     */
    public GetInputMap createGetInputMap() {
        return new GetInputMap();
    }

    /**
     * Create an instance of {@link SetInputMap }
     * 
     */
    public SetInputMap createSetInputMap() {
        return new SetInputMap();
    }

    /**
     * Create an instance of {@link SetOutputMap }
     * 
     */
    public SetOutputMap createSetOutputMap() {
        return new SetOutputMap();
    }

    /**
     * Create an instance of {@link AuthenticationInfo }
     * 
     */
    public AuthenticationInfo createAuthenticationInfo() {
        return new AuthenticationInfo();
    }

    /**
     * Create an instance of {@link ListQueryWorkInfo }
     * 
     */
    public ListQueryWorkInfo createListQueryWorkInfo() {
        return new ListQueryWorkInfo();
    }

    /**
     * Create an instance of {@link ListWOObjectQuery2 .GetListValues }
     * 
     */
    public ListWOObjectQuery2 .GetListValues createListWOObjectQuery2GetListValues() {
        return new ListWOObjectQuery2 .GetListValues();
    }

    /**
     * Create an instance of {@link ListWOObjectQuery.GetListValues }
     * 
     */
    public ListWOObjectQuery.GetListValues createListWOObjectQueryGetListValues() {
        return new ListWOObjectQuery.GetListValues();
    }

    /**
     * Create an instance of {@link GetListOutputMap.GetListValues }
     * 
     */
    public GetListOutputMap.GetListValues createGetListOutputMapGetListValues() {
        return new GetListOutputMap.GetListValues();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListOutputMap }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:HPD_IncidentInterface_WS", name = "HelpDesk_QueryList_ServiceResponse")
    public JAXBElement<GetListOutputMap> createHelpDeskQueryListServiceResponse(GetListOutputMap value) {
        return new JAXBElement<GetListOutputMap>(_HelpDeskQueryListServiceResponse_QNAME, GetListOutputMap.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOutputMap }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:HPD_IncidentInterface_WS", name = "HelpDesk_Query_ServiceResponse")
    public JAXBElement<GetOutputMap> createHelpDeskQueryServiceResponse(GetOutputMap value) {
        return new JAXBElement<GetOutputMap>(_HelpDeskQueryServiceResponse_QNAME, GetOutputMap.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InputMapping4 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:HPD_IncidentInterface_WS", name = "HelpDesk_GetWorkInfoList")
    public JAXBElement<InputMapping4> createHelpDeskGetWorkInfoList(InputMapping4 value) {
        return new JAXBElement<InputMapping4>(_HelpDeskGetWorkInfoList_QNAME, InputMapping4 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetInputMap }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:HPD_IncidentInterface_WS", name = "HelpDesk_Query_Service")
    public JAXBElement<GetInputMap> createHelpDeskQueryService(GetInputMap value) {
        return new JAXBElement<GetInputMap>(_HelpDeskQueryService_QNAME, GetInputMap.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListInputMap }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:HPD_IncidentInterface_WS", name = "HelpDesk_QueryList_Service")
    public JAXBElement<GetListInputMap> createHelpDeskQueryListService(GetListInputMap value) {
        return new JAXBElement<GetListInputMap>(_HelpDeskQueryListService_QNAME, GetListInputMap.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutputMapping4 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:HPD_IncidentInterface_WS", name = "HelpDesk_GetWorkInfoListResponse")
    public JAXBElement<OutputMapping4> createHelpDeskGetWorkInfoListResponse(OutputMapping4 value) {
        return new JAXBElement<OutputMapping4>(_HelpDeskGetWorkInfoListResponse_QNAME, OutputMapping4 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthenticationInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:HPD_IncidentInterface_WS", name = "AuthenticationInfo")
    public JAXBElement<AuthenticationInfo> createAuthenticationInfo(AuthenticationInfo value) {
        return new JAXBElement<AuthenticationInfo>(_AuthenticationInfo_QNAME, AuthenticationInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetInputMap }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:HPD_IncidentInterface_WS", name = "HelpDesk_Modify_Service")
    public JAXBElement<SetInputMap> createHelpDeskModifyService(SetInputMap value) {
        return new JAXBElement<SetInputMap>(_HelpDeskModifyService_QNAME, SetInputMap.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListWOObjectQuery2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org", name = "ListWOQuery")
    public JAXBElement<ListWOObjectQuery2> createListWOQuery(ListWOObjectQuery2 value) {
        return new JAXBElement<ListWOObjectQuery2>(_ListWOQuery_QNAME, ListWOObjectQuery2 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetOutputMap }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:HPD_IncidentInterface_WS", name = "HelpDesk_Modify_ServiceResponse")
    public JAXBElement<SetOutputMap> createHelpDeskModifyServiceResponse(SetOutputMap value) {
        return new JAXBElement<SetOutputMap>(_HelpDeskModifyServiceResponse_QNAME, SetOutputMap.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:HPD_IncidentInterface_WS", name = "Required_Resolution_Date", scope = GetListOutputMap.GetListValues.class)
    public JAXBElement<XMLGregorianCalendar> createGetListOutputMapGetListValuesRequiredResolutionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetListOutputMapGetListValuesRequiredResolutionDate_QNAME, XMLGregorianCalendar.class, GetListOutputMap.GetListValues.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:HPD_IncidentInterface_WS", name = "Submit_Date", scope = GetListOutputMap.GetListValues.class)
    public JAXBElement<XMLGregorianCalendar> createGetListOutputMapGetListValuesSubmitDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetListOutputMapGetListValuesSubmitDate_QNAME, XMLGregorianCalendar.class, GetListOutputMap.GetListValues.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusReasonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:HPD_IncidentInterface_WS", name = "Status_Reason", scope = GetListOutputMap.GetListValues.class)
    public JAXBElement<StatusReasonType> createGetListOutputMapGetListValuesStatusReason(StatusReasonType value) {
        return new JAXBElement<StatusReasonType>(_GetListOutputMapGetListValuesStatusReason_QNAME, StatusReasonType.class, GetListOutputMap.GetListValues.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:HPD_IncidentInterface_WS", name = "Target_Date", scope = GetListOutputMap.GetListValues.class)
    public JAXBElement<XMLGregorianCalendar> createGetListOutputMapGetListValuesTargetDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetListOutputMapGetListValuesTargetDate_QNAME, XMLGregorianCalendar.class, GetListOutputMap.GetListValues.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:HPD_IncidentInterface_WS", name = "Closed_Date", scope = GetListOutputMap.GetListValues.class)
    public JAXBElement<XMLGregorianCalendar> createGetListOutputMapGetListValuesClosedDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetListOutputMapGetListValuesClosedDate_QNAME, XMLGregorianCalendar.class, GetListOutputMap.GetListValues.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusReasonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:HPD_IncidentInterface_WS", name = "Status_Reason", scope = GetOutputMap.class)
    public JAXBElement<StatusReasonType> createGetOutputMapStatusReason(StatusReasonType value) {
        return new JAXBElement<StatusReasonType>(_GetListOutputMapGetListValuesStatusReason_QNAME, StatusReasonType.class, GetOutputMap.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusReasonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:HPD_IncidentInterface_WS", name = "Status_Reason", scope = SetInputMap.class)
    public JAXBElement<StatusReasonType> createSetInputMapStatusReason(StatusReasonType value) {
        return new JAXBElement<StatusReasonType>(_GetListOutputMapGetListValuesStatusReason_QNAME, StatusReasonType.class, SetInputMap.class, value);
    }

}
