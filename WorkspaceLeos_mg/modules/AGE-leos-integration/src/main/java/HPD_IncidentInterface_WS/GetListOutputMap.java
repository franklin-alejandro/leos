
package HPD_IncidentInterface_WS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para GetListOutputMap complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="GetListOutputMap">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getListValues" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Assigned_Group" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Assigned_Group_Shift_Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Assigned_Support_Company" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Assigned_Support_Organization" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Assignee" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Categorization_Tier_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Categorization_Tier_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Categorization_Tier_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Closure_Manufacturer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Closure_Product_Category_Tier1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Closure_Product_Category_Tier2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Closure_Product_Category_Tier3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Closure_Product_Model_Version" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Closure_Product_Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Contact_Company" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Contact_Sensitivity" type="{urn:HPD_IncidentInterface_WS}Contact_SensitivityType"/>
 *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Department" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Summary" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Notes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="First_Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Impact" type="{urn:HPD_IncidentInterface_WS}ImpactType"/>
 *                   &lt;element name="Incident_Number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Internet_E-mail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Last_Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Manufacturer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Middle_Initial" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Organization" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Phone_Number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Priority" type="{urn:HPD_IncidentInterface_WS}PriorityType"/>
 *                   &lt;element name="Priority_Weight" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="Product_Categorization_Tier_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Product_Categorization_Tier_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Product_Categorization_Tier_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Product_Model_Version" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Product_Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Region" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Reported_Source" type="{urn:HPD_IncidentInterface_WS}Reported_SourceType"/>
 *                   &lt;element name="Resolution" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Resolution_Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Resolution_Category_Tier_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Resolution_Category_Tier_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Service_Type" type="{urn:HPD_IncidentInterface_WS}Service_TypeType"/>
 *                   &lt;element name="Site" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Site_Group" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Status" type="{urn:HPD_IncidentInterface_WS}StatusType"/>
 *                   &lt;element name="Urgency" type="{urn:HPD_IncidentInterface_WS}UrgencyType"/>
 *                   &lt;element name="VIP" type="{urn:HPD_IncidentInterface_WS}VIPType"/>
 *                   &lt;element name="ServiceCI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ServiceCI_ReconID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="HPD_CI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="HPD_CI_ReconID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="HPD_CI_FormName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="z1D_CI_FormName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Reported_Date" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="Target_Date" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="Submit_Date" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="Closed_Date" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="Required_Resolution_Date" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="Status_Reason" type="{urn:HPD_IncidentInterface_WS}Status_ReasonType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetListOutputMap", propOrder = {
    "getListValues"
})
public class GetListOutputMap {

    @XmlElement(required = true)
    protected List<GetListOutputMap.GetListValues> getListValues;

    /**
     * Gets the value of the getListValues property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getListValues property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetListValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetListOutputMap.GetListValues }
     * 
     * 
     */
    public List<GetListOutputMap.GetListValues> getGetListValues() {
        if (getListValues == null) {
            getListValues = new ArrayList<GetListOutputMap.GetListValues>();
        }
        return this.getListValues;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Assigned_Group" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Assigned_Group_Shift_Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Assigned_Support_Company" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Assigned_Support_Organization" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Assignee" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Categorization_Tier_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Categorization_Tier_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Categorization_Tier_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Closure_Manufacturer" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Closure_Product_Category_Tier1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Closure_Product_Category_Tier2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Closure_Product_Category_Tier3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Closure_Product_Model_Version" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Closure_Product_Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Contact_Company" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Contact_Sensitivity" type="{urn:HPD_IncidentInterface_WS}Contact_SensitivityType"/>
     *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Department" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Summary" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Notes" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="First_Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Impact" type="{urn:HPD_IncidentInterface_WS}ImpactType"/>
     *         &lt;element name="Incident_Number" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Internet_E-mail" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Last_Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Manufacturer" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Middle_Initial" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Organization" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Phone_Number" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Priority" type="{urn:HPD_IncidentInterface_WS}PriorityType"/>
     *         &lt;element name="Priority_Weight" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="Product_Categorization_Tier_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Product_Categorization_Tier_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Product_Categorization_Tier_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Product_Model_Version" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Product_Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Region" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Reported_Source" type="{urn:HPD_IncidentInterface_WS}Reported_SourceType"/>
     *         &lt;element name="Resolution" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Resolution_Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Resolution_Category_Tier_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Resolution_Category_Tier_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Service_Type" type="{urn:HPD_IncidentInterface_WS}Service_TypeType"/>
     *         &lt;element name="Site" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Site_Group" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Status" type="{urn:HPD_IncidentInterface_WS}StatusType"/>
     *         &lt;element name="Urgency" type="{urn:HPD_IncidentInterface_WS}UrgencyType"/>
     *         &lt;element name="VIP" type="{urn:HPD_IncidentInterface_WS}VIPType"/>
     *         &lt;element name="ServiceCI" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ServiceCI_ReconID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="HPD_CI" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="HPD_CI_ReconID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="HPD_CI_FormName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="z1D_CI_FormName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Reported_Date" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="Target_Date" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="Submit_Date" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="Closed_Date" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="Required_Resolution_Date" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="Status_Reason" type="{urn:HPD_IncidentInterface_WS}Status_ReasonType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "assignedGroup",
        "assignedGroupShiftName",
        "assignedSupportCompany",
        "assignedSupportOrganization",
        "assignee",
        "categorizationTier1",
        "categorizationTier2",
        "categorizationTier3",
        "city",
        "closureManufacturer",
        "closureProductCategoryTier1",
        "closureProductCategoryTier2",
        "closureProductCategoryTier3",
        "closureProductModelVersion",
        "closureProductName",
        "company",
        "contactCompany",
        "contactSensitivity",
        "country",
        "department",
        "summary",
        "notes",
        "firstName",
        "impact",
        "incidentNumber",
        "internetEMail",
        "lastName",
        "manufacturer",
        "middleInitial",
        "organization",
        "phoneNumber",
        "priority",
        "priorityWeight",
        "productCategorizationTier1",
        "productCategorizationTier2",
        "productCategorizationTier3",
        "productModelVersion",
        "productName",
        "region",
        "reportedSource",
        "resolution",
        "resolutionCategory",
        "resolutionCategoryTier2",
        "resolutionCategoryTier3",
        "serviceType",
        "site",
        "siteGroup",
        "status",
        "urgency",
        "vip",
        "serviceCI",
        "serviceCIReconID",
        "hpdci",
        "hpdciReconID",
        "hpdciFormName",
        "z1DCIFormName",
        "reportedDate",
        "targetDate",
        "submitDate",
        "closedDate",
        "requiredResolutionDate",
        "statusReason"
    })
    public static class GetListValues {

        @XmlElement(name = "Assigned_Group", required = true)
        protected String assignedGroup;
        @XmlElement(name = "Assigned_Group_Shift_Name")
        protected String assignedGroupShiftName;
        @XmlElement(name = "Assigned_Support_Company", required = true)
        protected String assignedSupportCompany;
        @XmlElement(name = "Assigned_Support_Organization", required = true)
        protected String assignedSupportOrganization;
        @XmlElement(name = "Assignee", required = true)
        protected String assignee;
        @XmlElement(name = "Categorization_Tier_1", required = true)
        protected String categorizationTier1;
        @XmlElement(name = "Categorization_Tier_2", required = true)
        protected String categorizationTier2;
        @XmlElement(name = "Categorization_Tier_3", required = true)
        protected String categorizationTier3;
        @XmlElement(name = "City", required = true)
        protected String city;
        @XmlElement(name = "Closure_Manufacturer", required = true)
        protected String closureManufacturer;
        @XmlElement(name = "Closure_Product_Category_Tier1", required = true)
        protected String closureProductCategoryTier1;
        @XmlElement(name = "Closure_Product_Category_Tier2", required = true)
        protected String closureProductCategoryTier2;
        @XmlElement(name = "Closure_Product_Category_Tier3", required = true)
        protected String closureProductCategoryTier3;
        @XmlElement(name = "Closure_Product_Model_Version", required = true)
        protected String closureProductModelVersion;
        @XmlElement(name = "Closure_Product_Name", required = true)
        protected String closureProductName;
        @XmlElement(name = "Company", required = true)
        protected String company;
        @XmlElement(name = "Contact_Company", required = true)
        protected String contactCompany;
        @XmlElement(name = "Contact_Sensitivity", required = true, nillable = true)
        @XmlSchemaType(name = "string")
        protected ContactSensitivityType contactSensitivity;
        @XmlElement(name = "Country", required = true)
        protected String country;
        @XmlElement(name = "Department", required = true)
        protected String department;
        @XmlElement(name = "Summary", required = true)
        protected String summary;
        @XmlElement(name = "Notes", required = true)
        protected String notes;
        @XmlElement(name = "First_Name", required = true)
        protected String firstName;
        @XmlElement(name = "Impact", required = true, nillable = true)
        protected String impact;
        @XmlElement(name = "Incident_Number", required = true)
        protected String incidentNumber;
        @XmlElement(name = "Internet_E-mail", required = true)
        protected String internetEMail;
        @XmlElement(name = "Last_Name", required = true)
        protected String lastName;
        @XmlElement(name = "Manufacturer", required = true)
        protected String manufacturer;
        @XmlElement(name = "Middle_Initial", required = true)
        protected String middleInitial;
        @XmlElement(name = "Organization", required = true)
        protected String organization;
        @XmlElement(name = "Phone_Number", required = true)
        protected String phoneNumber;
        @XmlElement(name = "Priority", required = true, nillable = true)
        @XmlSchemaType(name = "string")
        protected PriorityType priority;
        @XmlElement(name = "Priority_Weight", required = true, type = Integer.class, nillable = true)
        protected Integer priorityWeight;
        @XmlElement(name = "Product_Categorization_Tier_1", required = true)
        protected String productCategorizationTier1;
        @XmlElement(name = "Product_Categorization_Tier_2")
        protected String productCategorizationTier2;
        @XmlElement(name = "Product_Categorization_Tier_3", required = true)
        protected String productCategorizationTier3;
        @XmlElement(name = "Product_Model_Version", required = true)
        protected String productModelVersion;
        @XmlElement(name = "Product_Name", required = true)
        protected String productName;
        @XmlElement(name = "Region", required = true)
        protected String region;
        @XmlElement(name = "Reported_Source", required = true, nillable = true)
        @XmlSchemaType(name = "string")
        protected ReportedSourceType reportedSource;
        @XmlElement(name = "Resolution", required = true)
        protected String resolution;
        @XmlElement(name = "Resolution_Category", required = true)
        protected String resolutionCategory;
        @XmlElement(name = "Resolution_Category_Tier_2", required = true)
        protected String resolutionCategoryTier2;
        @XmlElement(name = "Resolution_Category_Tier_3", required = true)
        protected String resolutionCategoryTier3;
        @XmlElement(name = "Service_Type", required = true, nillable = true)
        @XmlSchemaType(name = "string")
        protected ServiceTypeType serviceType;
        @XmlElement(name = "Site", required = true)
        protected String site;
        @XmlElement(name = "Site_Group", required = true)
        protected String siteGroup;
        @XmlElement(name = "Status", required = true)
        @XmlSchemaType(name = "string")
        protected StatusType status;
        @XmlElement(name = "Urgency", required = true, nillable = true)
        protected String urgency;
        @XmlElement(name = "VIP", required = true, nillable = true)
        @XmlSchemaType(name = "string")
        protected VIPType vip;
        @XmlElement(name = "ServiceCI", required = true)
        protected String serviceCI;
        @XmlElement(name = "ServiceCI_ReconID", required = true)
        protected String serviceCIReconID;
        @XmlElement(name = "HPD_CI", required = true)
        protected String hpdci;
        @XmlElement(name = "HPD_CI_ReconID", required = true)
        protected String hpdciReconID;
        @XmlElement(name = "HPD_CI_FormName", required = true)
        protected String hpdciFormName;
        @XmlElement(name = "z1D_CI_FormName", required = true)
        protected String z1DCIFormName;
        @XmlElement(name = "Reported_Date")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar reportedDate;
        @XmlElementRef(name = "Target_Date", namespace = "urn:HPD_IncidentInterface_WS", type = JAXBElement.class, required = false)
        protected JAXBElement<XMLGregorianCalendar> targetDate;
        @XmlElementRef(name = "Submit_Date", namespace = "urn:HPD_IncidentInterface_WS", type = JAXBElement.class, required = false)
        protected JAXBElement<XMLGregorianCalendar> submitDate;
        @XmlElementRef(name = "Closed_Date", namespace = "urn:HPD_IncidentInterface_WS", type = JAXBElement.class, required = false)
        protected JAXBElement<XMLGregorianCalendar> closedDate;
        @XmlElementRef(name = "Required_Resolution_Date", namespace = "urn:HPD_IncidentInterface_WS", type = JAXBElement.class, required = false)
        protected JAXBElement<XMLGregorianCalendar> requiredResolutionDate;
        @XmlElementRef(name = "Status_Reason", namespace = "urn:HPD_IncidentInterface_WS", type = JAXBElement.class, required = false)
        protected JAXBElement<StatusReasonType> statusReason;

        /**
         * Obtiene el valor de la propiedad assignedGroup.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAssignedGroup() {
            return assignedGroup;
        }

        /**
         * Define el valor de la propiedad assignedGroup.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAssignedGroup(String value) {
            this.assignedGroup = value;
        }

        /**
         * Obtiene el valor de la propiedad assignedGroupShiftName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAssignedGroupShiftName() {
            return assignedGroupShiftName;
        }

        /**
         * Define el valor de la propiedad assignedGroupShiftName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAssignedGroupShiftName(String value) {
            this.assignedGroupShiftName = value;
        }

        /**
         * Obtiene el valor de la propiedad assignedSupportCompany.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAssignedSupportCompany() {
            return assignedSupportCompany;
        }

        /**
         * Define el valor de la propiedad assignedSupportCompany.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAssignedSupportCompany(String value) {
            this.assignedSupportCompany = value;
        }

        /**
         * Obtiene el valor de la propiedad assignedSupportOrganization.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAssignedSupportOrganization() {
            return assignedSupportOrganization;
        }

        /**
         * Define el valor de la propiedad assignedSupportOrganization.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAssignedSupportOrganization(String value) {
            this.assignedSupportOrganization = value;
        }

        /**
         * Obtiene el valor de la propiedad assignee.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAssignee() {
            return assignee;
        }

        /**
         * Define el valor de la propiedad assignee.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAssignee(String value) {
            this.assignee = value;
        }

        /**
         * Obtiene el valor de la propiedad categorizationTier1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCategorizationTier1() {
            return categorizationTier1;
        }

        /**
         * Define el valor de la propiedad categorizationTier1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCategorizationTier1(String value) {
            this.categorizationTier1 = value;
        }

        /**
         * Obtiene el valor de la propiedad categorizationTier2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCategorizationTier2() {
            return categorizationTier2;
        }

        /**
         * Define el valor de la propiedad categorizationTier2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCategorizationTier2(String value) {
            this.categorizationTier2 = value;
        }

        /**
         * Obtiene el valor de la propiedad categorizationTier3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCategorizationTier3() {
            return categorizationTier3;
        }

        /**
         * Define el valor de la propiedad categorizationTier3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCategorizationTier3(String value) {
            this.categorizationTier3 = value;
        }

        /**
         * Obtiene el valor de la propiedad city.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCity() {
            return city;
        }

        /**
         * Define el valor de la propiedad city.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCity(String value) {
            this.city = value;
        }

        /**
         * Obtiene el valor de la propiedad closureManufacturer.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClosureManufacturer() {
            return closureManufacturer;
        }

        /**
         * Define el valor de la propiedad closureManufacturer.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClosureManufacturer(String value) {
            this.closureManufacturer = value;
        }

        /**
         * Obtiene el valor de la propiedad closureProductCategoryTier1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClosureProductCategoryTier1() {
            return closureProductCategoryTier1;
        }

        /**
         * Define el valor de la propiedad closureProductCategoryTier1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClosureProductCategoryTier1(String value) {
            this.closureProductCategoryTier1 = value;
        }

        /**
         * Obtiene el valor de la propiedad closureProductCategoryTier2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClosureProductCategoryTier2() {
            return closureProductCategoryTier2;
        }

        /**
         * Define el valor de la propiedad closureProductCategoryTier2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClosureProductCategoryTier2(String value) {
            this.closureProductCategoryTier2 = value;
        }

        /**
         * Obtiene el valor de la propiedad closureProductCategoryTier3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClosureProductCategoryTier3() {
            return closureProductCategoryTier3;
        }

        /**
         * Define el valor de la propiedad closureProductCategoryTier3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClosureProductCategoryTier3(String value) {
            this.closureProductCategoryTier3 = value;
        }

        /**
         * Obtiene el valor de la propiedad closureProductModelVersion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClosureProductModelVersion() {
            return closureProductModelVersion;
        }

        /**
         * Define el valor de la propiedad closureProductModelVersion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClosureProductModelVersion(String value) {
            this.closureProductModelVersion = value;
        }

        /**
         * Obtiene el valor de la propiedad closureProductName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClosureProductName() {
            return closureProductName;
        }

        /**
         * Define el valor de la propiedad closureProductName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClosureProductName(String value) {
            this.closureProductName = value;
        }

        /**
         * Obtiene el valor de la propiedad company.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCompany() {
            return company;
        }

        /**
         * Define el valor de la propiedad company.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCompany(String value) {
            this.company = value;
        }

        /**
         * Obtiene el valor de la propiedad contactCompany.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContactCompany() {
            return contactCompany;
        }

        /**
         * Define el valor de la propiedad contactCompany.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContactCompany(String value) {
            this.contactCompany = value;
        }

        /**
         * Obtiene el valor de la propiedad contactSensitivity.
         * 
         * @return
         *     possible object is
         *     {@link ContactSensitivityType }
         *     
         */
        public ContactSensitivityType getContactSensitivity() {
            return contactSensitivity;
        }

        /**
         * Define el valor de la propiedad contactSensitivity.
         * 
         * @param value
         *     allowed object is
         *     {@link ContactSensitivityType }
         *     
         */
        public void setContactSensitivity(ContactSensitivityType value) {
            this.contactSensitivity = value;
        }

        /**
         * Obtiene el valor de la propiedad country.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountry() {
            return country;
        }

        /**
         * Define el valor de la propiedad country.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountry(String value) {
            this.country = value;
        }

        /**
         * Obtiene el valor de la propiedad department.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDepartment() {
            return department;
        }

        /**
         * Define el valor de la propiedad department.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDepartment(String value) {
            this.department = value;
        }

        /**
         * Obtiene el valor de la propiedad summary.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSummary() {
            return summary;
        }

        /**
         * Define el valor de la propiedad summary.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSummary(String value) {
            this.summary = value;
        }

        /**
         * Obtiene el valor de la propiedad notes.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNotes() {
            return notes;
        }

        /**
         * Define el valor de la propiedad notes.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNotes(String value) {
            this.notes = value;
        }

        /**
         * Obtiene el valor de la propiedad firstName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFirstName() {
            return firstName;
        }

        /**
         * Define el valor de la propiedad firstName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFirstName(String value) {
            this.firstName = value;
        }

        /**
         * Obtiene el valor de la propiedad impact.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getImpact() {
            return impact;
        }

        /**
         * Define el valor de la propiedad impact.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setImpact(String value) {
            this.impact = value;
        }

        /**
         * Obtiene el valor de la propiedad incidentNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIncidentNumber() {
            return incidentNumber;
        }

        /**
         * Define el valor de la propiedad incidentNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIncidentNumber(String value) {
            this.incidentNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad internetEMail.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInternetEMail() {
            return internetEMail;
        }

        /**
         * Define el valor de la propiedad internetEMail.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInternetEMail(String value) {
            this.internetEMail = value;
        }

        /**
         * Obtiene el valor de la propiedad lastName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * Define el valor de la propiedad lastName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLastName(String value) {
            this.lastName = value;
        }

        /**
         * Obtiene el valor de la propiedad manufacturer.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getManufacturer() {
            return manufacturer;
        }

        /**
         * Define el valor de la propiedad manufacturer.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setManufacturer(String value) {
            this.manufacturer = value;
        }

        /**
         * Obtiene el valor de la propiedad middleInitial.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMiddleInitial() {
            return middleInitial;
        }

        /**
         * Define el valor de la propiedad middleInitial.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMiddleInitial(String value) {
            this.middleInitial = value;
        }

        /**
         * Obtiene el valor de la propiedad organization.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOrganization() {
            return organization;
        }

        /**
         * Define el valor de la propiedad organization.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOrganization(String value) {
            this.organization = value;
        }

        /**
         * Obtiene el valor de la propiedad phoneNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPhoneNumber() {
            return phoneNumber;
        }

        /**
         * Define el valor de la propiedad phoneNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPhoneNumber(String value) {
            this.phoneNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad priority.
         * 
         * @return
         *     possible object is
         *     {@link PriorityType }
         *     
         */
        public PriorityType getPriority() {
            return priority;
        }

        /**
         * Define el valor de la propiedad priority.
         * 
         * @param value
         *     allowed object is
         *     {@link PriorityType }
         *     
         */
        public void setPriority(PriorityType value) {
            this.priority = value;
        }

        /**
         * Obtiene el valor de la propiedad priorityWeight.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getPriorityWeight() {
            return priorityWeight;
        }

        /**
         * Define el valor de la propiedad priorityWeight.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setPriorityWeight(Integer value) {
            this.priorityWeight = value;
        }

        /**
         * Obtiene el valor de la propiedad productCategorizationTier1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProductCategorizationTier1() {
            return productCategorizationTier1;
        }

        /**
         * Define el valor de la propiedad productCategorizationTier1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProductCategorizationTier1(String value) {
            this.productCategorizationTier1 = value;
        }

        /**
         * Obtiene el valor de la propiedad productCategorizationTier2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProductCategorizationTier2() {
            return productCategorizationTier2;
        }

        /**
         * Define el valor de la propiedad productCategorizationTier2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProductCategorizationTier2(String value) {
            this.productCategorizationTier2 = value;
        }

        /**
         * Obtiene el valor de la propiedad productCategorizationTier3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProductCategorizationTier3() {
            return productCategorizationTier3;
        }

        /**
         * Define el valor de la propiedad productCategorizationTier3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProductCategorizationTier3(String value) {
            this.productCategorizationTier3 = value;
        }

        /**
         * Obtiene el valor de la propiedad productModelVersion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProductModelVersion() {
            return productModelVersion;
        }

        /**
         * Define el valor de la propiedad productModelVersion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProductModelVersion(String value) {
            this.productModelVersion = value;
        }

        /**
         * Obtiene el valor de la propiedad productName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProductName() {
            return productName;
        }

        /**
         * Define el valor de la propiedad productName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProductName(String value) {
            this.productName = value;
        }

        /**
         * Obtiene el valor de la propiedad region.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegion() {
            return region;
        }

        /**
         * Define el valor de la propiedad region.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegion(String value) {
            this.region = value;
        }

        /**
         * Obtiene el valor de la propiedad reportedSource.
         * 
         * @return
         *     possible object is
         *     {@link ReportedSourceType }
         *     
         */
        public ReportedSourceType getReportedSource() {
            return reportedSource;
        }

        /**
         * Define el valor de la propiedad reportedSource.
         * 
         * @param value
         *     allowed object is
         *     {@link ReportedSourceType }
         *     
         */
        public void setReportedSource(ReportedSourceType value) {
            this.reportedSource = value;
        }

        /**
         * Obtiene el valor de la propiedad resolution.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResolution() {
            return resolution;
        }

        /**
         * Define el valor de la propiedad resolution.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResolution(String value) {
            this.resolution = value;
        }

        /**
         * Obtiene el valor de la propiedad resolutionCategory.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResolutionCategory() {
            return resolutionCategory;
        }

        /**
         * Define el valor de la propiedad resolutionCategory.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResolutionCategory(String value) {
            this.resolutionCategory = value;
        }

        /**
         * Obtiene el valor de la propiedad resolutionCategoryTier2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResolutionCategoryTier2() {
            return resolutionCategoryTier2;
        }

        /**
         * Define el valor de la propiedad resolutionCategoryTier2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResolutionCategoryTier2(String value) {
            this.resolutionCategoryTier2 = value;
        }

        /**
         * Obtiene el valor de la propiedad resolutionCategoryTier3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResolutionCategoryTier3() {
            return resolutionCategoryTier3;
        }

        /**
         * Define el valor de la propiedad resolutionCategoryTier3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResolutionCategoryTier3(String value) {
            this.resolutionCategoryTier3 = value;
        }

        /**
         * Obtiene el valor de la propiedad serviceType.
         * 
         * @return
         *     possible object is
         *     {@link ServiceTypeType }
         *     
         */
        public ServiceTypeType getServiceType() {
            return serviceType;
        }

        /**
         * Define el valor de la propiedad serviceType.
         * 
         * @param value
         *     allowed object is
         *     {@link ServiceTypeType }
         *     
         */
        public void setServiceType(ServiceTypeType value) {
            this.serviceType = value;
        }

        /**
         * Obtiene el valor de la propiedad site.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSite() {
            return site;
        }

        /**
         * Define el valor de la propiedad site.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSite(String value) {
            this.site = value;
        }

        /**
         * Obtiene el valor de la propiedad siteGroup.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSiteGroup() {
            return siteGroup;
        }

        /**
         * Define el valor de la propiedad siteGroup.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSiteGroup(String value) {
            this.siteGroup = value;
        }

        /**
         * Obtiene el valor de la propiedad status.
         * 
         * @return
         *     possible object is
         *     {@link StatusType }
         *     
         */
        public StatusType getStatus() {
            return status;
        }

        /**
         * Define el valor de la propiedad status.
         * 
         * @param value
         *     allowed object is
         *     {@link StatusType }
         *     
         */
        public void setStatus(StatusType value) {
            this.status = value;
        }

        /**
         * Obtiene el valor de la propiedad urgency.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUrgency() {
            return urgency;
        }

        /**
         * Define el valor de la propiedad urgency.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUrgency(String value) {
            this.urgency = value;
        }

        /**
         * Obtiene el valor de la propiedad vip.
         * 
         * @return
         *     possible object is
         *     {@link VIPType }
         *     
         */
        public VIPType getVIP() {
            return vip;
        }

        /**
         * Define el valor de la propiedad vip.
         * 
         * @param value
         *     allowed object is
         *     {@link VIPType }
         *     
         */
        public void setVIP(VIPType value) {
            this.vip = value;
        }

        /**
         * Obtiene el valor de la propiedad serviceCI.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServiceCI() {
            return serviceCI;
        }

        /**
         * Define el valor de la propiedad serviceCI.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServiceCI(String value) {
            this.serviceCI = value;
        }

        /**
         * Obtiene el valor de la propiedad serviceCIReconID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServiceCIReconID() {
            return serviceCIReconID;
        }

        /**
         * Define el valor de la propiedad serviceCIReconID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServiceCIReconID(String value) {
            this.serviceCIReconID = value;
        }

        /**
         * Obtiene el valor de la propiedad hpdci.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHPDCI() {
            return hpdci;
        }

        /**
         * Define el valor de la propiedad hpdci.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHPDCI(String value) {
            this.hpdci = value;
        }

        /**
         * Obtiene el valor de la propiedad hpdciReconID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHPDCIReconID() {
            return hpdciReconID;
        }

        /**
         * Define el valor de la propiedad hpdciReconID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHPDCIReconID(String value) {
            this.hpdciReconID = value;
        }

        /**
         * Obtiene el valor de la propiedad hpdciFormName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHPDCIFormName() {
            return hpdciFormName;
        }

        /**
         * Define el valor de la propiedad hpdciFormName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHPDCIFormName(String value) {
            this.hpdciFormName = value;
        }

        /**
         * Obtiene el valor de la propiedad z1DCIFormName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZ1DCIFormName() {
            return z1DCIFormName;
        }

        /**
         * Define el valor de la propiedad z1DCIFormName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZ1DCIFormName(String value) {
            this.z1DCIFormName = value;
        }

        /**
         * Obtiene el valor de la propiedad reportedDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getReportedDate() {
            return reportedDate;
        }

        /**
         * Define el valor de la propiedad reportedDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setReportedDate(XMLGregorianCalendar value) {
            this.reportedDate = value;
        }

        /**
         * Obtiene el valor de la propiedad targetDate.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public JAXBElement<XMLGregorianCalendar> getTargetDate() {
            return targetDate;
        }

        /**
         * Define el valor de la propiedad targetDate.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public void setTargetDate(JAXBElement<XMLGregorianCalendar> value) {
            this.targetDate = value;
        }

        /**
         * Obtiene el valor de la propiedad submitDate.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public JAXBElement<XMLGregorianCalendar> getSubmitDate() {
            return submitDate;
        }

        /**
         * Define el valor de la propiedad submitDate.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public void setSubmitDate(JAXBElement<XMLGregorianCalendar> value) {
            this.submitDate = value;
        }

        /**
         * Obtiene el valor de la propiedad closedDate.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public JAXBElement<XMLGregorianCalendar> getClosedDate() {
            return closedDate;
        }

        /**
         * Define el valor de la propiedad closedDate.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public void setClosedDate(JAXBElement<XMLGregorianCalendar> value) {
            this.closedDate = value;
        }

        /**
         * Obtiene el valor de la propiedad requiredResolutionDate.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public JAXBElement<XMLGregorianCalendar> getRequiredResolutionDate() {
            return requiredResolutionDate;
        }

        /**
         * Define el valor de la propiedad requiredResolutionDate.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public void setRequiredResolutionDate(JAXBElement<XMLGregorianCalendar> value) {
            this.requiredResolutionDate = value;
        }

        /**
         * Obtiene el valor de la propiedad statusReason.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link StatusReasonType }{@code >}
         *     
         */
        public JAXBElement<StatusReasonType> getStatusReason() {
            return statusReason;
        }

        /**
         * Define el valor de la propiedad statusReason.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link StatusReasonType }{@code >}
         *     
         */
        public void setStatusReason(JAXBElement<StatusReasonType> value) {
            this.statusReason = value;
        }

    }

}
