package eu.europa.ec.leos.ui.event.amendment;

public class AmendmentModeExtensionEvent {

    private String articleId;
    private boolean openLayout;

    public AmendmentModeExtensionEvent(boolean openLayout, String articleId) {
        this.openLayout = openLayout;
        this.articleId = articleId;
    }

    public boolean isOpenLayout() {
        return openLayout;
    }

    public String getArticleId() {
        return articleId;
    }

}
