package eu.europa.ec.leos.web.model;

import eu.europa.ec.leos.model.cantodorado.AGECantoDoradoOrganisms;

public class AGESearchCriteriaCantoDoradoVO {

	private AGECantoDoradoOrganisms organism;
	private String year;
	private String month;
	private String day;
	private String number;
	
	public AGESearchCriteriaCantoDoradoVO(AGECantoDoradoOrganisms organism, String year, String month, String day,
			String number) {
		super();
		this.organism = organism;
		this.year = year;
		this.month = month;
		this.day = day;
		this.number = number;
	}

	public AGECantoDoradoOrganisms getOrganism() {
		return organism;
	}

	public void setOrganism(AGECantoDoradoOrganisms organism) {
		this.organism = organism;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
}
