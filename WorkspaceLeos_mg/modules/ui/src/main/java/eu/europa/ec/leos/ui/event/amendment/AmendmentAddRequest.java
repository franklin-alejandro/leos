package eu.europa.ec.leos.ui.event.amendment;

public class AmendmentAddRequest {
	
	private String elementId;
	private String element;
	private String eli;
	private String nameNorm;
	private String actionType;
	private String elementType;
	private boolean isEditing;
	
	
	public AmendmentAddRequest(String elementId, String element, String eli, String nameNorm, String actionType, String elementType, boolean isEditing) {
		super();
		this.elementId = elementId;
		this.element = element;
		this.eli = eli;
		this.nameNorm = nameNorm;
		this.actionType = actionType;
		this.elementType = elementType;
		this.isEditing = isEditing;
	}
	
	public boolean isEditing() {
		return isEditing;
	}

	public void setEditing(boolean isEditing) {
		this.isEditing = isEditing;
	}

	public String getElementType() {
		return elementType;
	}

	public void setElementType(String elementType) {
		this.elementType = elementType;
	}



	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getEli() {
		return eli;
	}

	public void setEli(String eli) {
		this.eli = eli;
	}

	public String getNameNorm() {
		return nameNorm;
	}

	public void setNameNorm(String nameNorm) {
		this.nameNorm = nameNorm;
	}

	public void setElementId(String elementId) {
		this.elementId = elementId;
	}

	public String getElementId() {
		return elementId;
	}
	public void setIdElement(String idElement) {
		this.elementId = idElement;
	}
	public String getElement() {
		return element;
	}
	public void setElement(String element) {
		this.element = element;
	}

}
