package eu.europa.ec.leos.web.event.view.document;

import eu.europa.ec.leos.web.model.SearchCriteriaBOEVO;
//AGE-CORE-3.0.0
public class SearchActBOERequestEvent {
	
    private SearchCriteriaBOEVO searchCriteria;

    public SearchActBOERequestEvent(SearchCriteriaBOEVO searchCriteria) {
        super();
        this.searchCriteria = searchCriteria;
    }

    public SearchCriteriaBOEVO getSearchCriteria() {
        return searchCriteria;
    }
}


