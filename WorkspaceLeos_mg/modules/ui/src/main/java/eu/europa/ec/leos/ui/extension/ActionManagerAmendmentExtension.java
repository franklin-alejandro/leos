package eu.europa.ec.leos.ui.extension;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.vaadin.annotations.JavaScript;
import com.vaadin.ui.AbstractComponent;
import elemental.json.JsonObject;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.ui.event.amendment.AmendmentModeExtensionEvent;
import eu.europa.ec.leos.ui.event.amendment.AmendmentViewRequestEvent;
import eu.europa.ec.leos.web.support.LeosCacheToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@JavaScript({"vaadin://../js/ui/extension/actionManagerAmendmentConnector.js" + LeosCacheToken.TOKEN})
public class ActionManagerAmendmentExtension<T extends AbstractComponent> extends LeosJavaScriptExtension {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(ImportAmendingActExtension.class);
    private EventBus eventBus;

    public ActionManagerAmendmentExtension(T target, EventBus eventBus, MessageHelper messageHelper) {
        super();
        this.eventBus = eventBus;
        extend(target);
        registerServerSideAPI();
        getState().underModificationLabel = messageHelper.getMessage("document.amendment.underModification");
        getState().openAmendmentViewLabel = messageHelper.getMessage("document.amendment.openAmendmentView");
    }

    @Override
    public void attach() {
        super.attach();
        eventBus.register(this);
    }

    @Override
    public void detach() {
        eventBus.unregister(this);
        super.detach();
    }

    @Override
    protected ActionManagerAmendmentState getState() {
        return (ActionManagerAmendmentState)super.getState();
    }

    @Override
    protected ActionManagerAmendmentState getState(boolean markAsDirty) {
        return (ActionManagerAmendmentState)super.getState(markAsDirty);
    }

    @Subscribe
    public void openCloseAmendmentView(AmendmentModeExtensionEvent event) {
        getState().openLayout = event.isOpenLayout();
        getState().articleId = event.getArticleId();
        callFunction("openCloseAmendmentView");
    }

    private void registerServerSideAPI() {
        addFunction("editAmendment", arguments -> {
            JsonObject data = arguments.get(0);
            String elementId = data.getString("elementId");
            String type = data.getString("type");
            eventBus.post(new AmendmentViewRequestEvent(elementId, "", type));
        });
    }
}
