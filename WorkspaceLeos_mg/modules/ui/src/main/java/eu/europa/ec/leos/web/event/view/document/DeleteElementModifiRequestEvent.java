package eu.europa.ec.leos.web.event.view.document;

public class DeleteElementModifiRequestEvent {
	String quotedId;
	String elementId;
	String modId;
	String granParentId;
	String granParentType;

	public DeleteElementModifiRequestEvent(String quotedId, String elementId, String modId, String granParentId, String granParentType) {
		super();
		this.quotedId = quotedId;
		this.elementId = elementId;
		this.modId = modId;
		this.granParentId = granParentId;
		this.granParentType = granParentType;
	}

	public String getQuotedId() {
		return quotedId;
	}

	public void setQuotedId(String quotedId) {
		this.quotedId = quotedId;
	}

	public String getElementId() {
		return elementId;
	}

	public void setElementId(String elementId) {
		this.elementId = elementId;
	}

	public String getModId() {
		return modId;
	}

	public void setModId(String modId) {
		this.modId = modId;
	}

	public String getGranParentId() {
		return granParentId;
	}

	public void setGranParentId(String granParentId) {
		this.granParentId = granParentId;
	}

	public String getGranParentType() {
		return granParentType;
	}

	public void setGranParentType(String granParentType) {
		this.granParentType = granParentType;
	}
	
	
}
