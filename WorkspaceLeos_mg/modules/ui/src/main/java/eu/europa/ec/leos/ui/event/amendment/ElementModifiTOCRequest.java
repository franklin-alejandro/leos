package eu.europa.ec.leos.ui.event.amendment;

import java.util.ArrayList;

public class ElementModifiTOCRequest {
	
	ArrayList<String> elementId;

	public ElementModifiTOCRequest(ArrayList<String> elementId) {
		super();
		this.elementId = elementId;
	}

	public ArrayList<String> getElementId() {
		return elementId;
	}

	public void setElementId(ArrayList<String> elementId) {
		this.elementId = elementId;
	}
	
	
}
