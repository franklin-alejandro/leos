package eu.europa.ec.leos.ui.event.amendment;

import java.util.List;

import eu.europa.ec.leos.amend.AmendAction;


public class AmendmentElementModificationRequest {


	private AmendAction action;
	private List<String> elementAmendmentSelect;
	private String articleId;
	private String title;
	private String eli;
	private String numArticleCk;
	private String normType;
	private String typeElement;
	private boolean isEditing;
	

	public AmendmentElementModificationRequest(AmendAction action, List<String> elementAmendmentSelect, String articleId, String title, String eli,String numArticleCk, String normType, String typeElement, boolean isEditing ) {
		super();
		this.action = action;
		this.elementAmendmentSelect = elementAmendmentSelect;
		this.articleId = articleId;
		this.title = title;
		this.eli = eli;
		this.numArticleCk = numArticleCk;
		this.normType = normType;
		this.typeElement = typeElement;
		this.isEditing = isEditing;
	}
	
	public boolean isEditing() {
		return isEditing;
	}

	public void setEditing(boolean isEditing) {
		this.isEditing = isEditing;
	}

	public String getNumArticleCk() {
		return numArticleCk;
	}

	public String getTypeElement() {
		return typeElement;
	}

	public void setTypeElement(String typeElement) {
		this.typeElement = typeElement;
	}

	public void setNumArticleCk(String numArticleCk) {
		this.numArticleCk = numArticleCk;
	}

	public String getNormType() {
		return normType;
	}

	public void setNormType(String normType) {
		this.normType = normType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEli() {
		return eli;
	}

	public void setEli(String eli) {
		this.eli = eli;
	}

	public String getArticleId() {
		return articleId;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}

	public AmendAction getAction() {
		return action;
	}

	public void setAction(AmendAction action) {
		this.action = action;
	}

	public List<String> getElementAmendmentSelect() {
		return elementAmendmentSelect;
	}

	public void setElementAmendmentSelect(List<String> elementAmendmentSelect) {
		this.elementAmendmentSelect = elementAmendmentSelect;
	}

}
