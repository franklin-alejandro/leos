package eu.europa.ec.leos.ui.event.view.collection;

import eu.europa.ec.leos.web.model.AGESearchCriteriaCantoDoradoVO;

public class AGECreateCantoDoradoRequest {
	
	private AGESearchCriteriaCantoDoradoVO searchCriteria;

	public AGECreateCantoDoradoRequest(AGESearchCriteriaCantoDoradoVO searchCriteria) {
		super();
		this.searchCriteria = searchCriteria;
	}

	public AGESearchCriteriaCantoDoradoVO getSearchCriteria() {
		return searchCriteria;
	}

}
