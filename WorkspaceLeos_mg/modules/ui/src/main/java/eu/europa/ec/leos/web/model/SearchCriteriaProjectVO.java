package eu.europa.ec.leos.web.model;

public class SearchCriteriaProjectVO {
	
	private String title;

	public SearchCriteriaProjectVO(String title) {
		super();
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
