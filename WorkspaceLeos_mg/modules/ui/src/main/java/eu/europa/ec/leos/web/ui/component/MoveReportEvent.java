package eu.europa.ec.leos.web.ui.component;

import eu.europa.ec.leos.domain.vo.DocumentVO;

public class MoveReportEvent {
    public enum Direction {
        UP,
        DOWN
    }

    private Direction direction;
    private DocumentVO reportVo;

    public MoveReportEvent(DocumentVO reportVo, Direction direction) {
        this.direction = direction;
        this.reportVo = reportVo;
    }

    public Direction getDirection() {
        return direction;
    }

    public DocumentVO getReportVo() {
        return reportVo;
    }
}
