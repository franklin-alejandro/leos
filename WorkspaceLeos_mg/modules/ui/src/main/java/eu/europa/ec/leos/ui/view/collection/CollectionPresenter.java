/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.ui.view.collection;

import static eu.europa.ec.leos.domain.cmis.LeosCategory.BILL;
import static eu.europa.ec.leos.domain.cmis.LeosCategory.PROPOSAL;
import static eu.europa.ec.leos.security.LeosPermission.CAN_ADD_REMOVE_COLLABORATOR;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.locks.StampedLock;
import java.util.stream.Collectors;

import javax.inject.Provider;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import  com.vaadin.server.*;
import com.vaadin.ui.UI;

import eu.europa.ec.leos.domain.cmis.Content;
import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.cmis.LeosPackage;
import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.usecases.document.AGECollectionContext;
import eu.europa.ec.leos.usecases.document.AGEBillContext;
import eu.europa.ec.leos.domain.cmis.document.AGEReport;
import eu.europa.ec.leos.domain.cmis.document.AGECantoDorado;
import eu.europa.ec.leos.domain.cmis.document.Annex;
import eu.europa.ec.leos.domain.cmis.document.Bill;
import eu.europa.ec.leos.domain.cmis.document.LegDocument;
import eu.europa.ec.leos.domain.cmis.document.LeosDocument;
import eu.europa.ec.leos.domain.cmis.document.Memorandum;
import eu.europa.ec.leos.domain.cmis.document.Proposal;
import eu.europa.ec.leos.domain.cmis.document.XmlDocument;
import eu.europa.ec.leos.domain.cmis.metadata.AGEReportMetadata;
import eu.europa.ec.leos.domain.cmis.metadata.AGECantoDoradoMetadata;
import eu.europa.ec.leos.domain.cmis.metadata.AnnexMetadata;
import eu.europa.ec.leos.domain.cmis.metadata.BillMetadata;
import eu.europa.ec.leos.domain.cmis.metadata.ProposalMetadata;
import eu.europa.ec.leos.domain.vo.DocumentVO;
import eu.europa.ec.leos.domain.vo.MetadataVO;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.event.MilestoneCreatedEvent;
import eu.europa.ec.leos.model.event.MilestoneUpdatedEvent;
import eu.europa.ec.leos.model.event.UpdateUserInfoEvent;
import eu.europa.ec.leos.model.messaging.UpdateInternalReferencesMessage;
import eu.europa.ec.leos.model.notification.collaborators.AddCollaborator;
import eu.europa.ec.leos.model.notification.collaborators.EditCollaborator;
import eu.europa.ec.leos.model.notification.collaborators.RemoveCollaborator;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.permissions.Role;
import eu.europa.ec.leos.security.LeosPermissionAuthorityMapHelper;
import eu.europa.ec.leos.security.SecurityContext;
import eu.europa.ec.leos.services.content.processor.TransformationService;
import eu.europa.ec.leos.services.document.AGEReportService;
import eu.europa.ec.leos.services.document.AGECantoDoradoService;
import eu.europa.ec.leos.services.document.AnnexService;
import eu.europa.ec.leos.services.document.BillService;
import eu.europa.ec.leos.services.document.MemorandumService;
import eu.europa.ec.leos.services.document.ProposalService;
import eu.europa.ec.leos.services.document.SecurityService;
import eu.europa.ec.leos.services.export.ExportOptions;
import eu.europa.ec.leos.services.messaging.UpdateInternalReferencesProducer;
import eu.europa.ec.leos.services.milestone.MilestoneService;
import eu.europa.ec.leos.services.notification.NotificationService;
import eu.europa.ec.leos.services.store.PackageService;
import eu.europa.ec.leos.services.store.TemplateService;
import eu.europa.ec.leos.services.store.WorkspaceService;
import eu.europa.ec.leos.ui.event.CloseScreenRequestEvent;
import eu.europa.ec.leos.ui.event.CreateMilestoneEvent;
import eu.europa.ec.leos.ui.event.CreateMilestoneRequestEvent;
import eu.europa.ec.leos.ui.event.FetchMilestoneEvent;
import eu.europa.ec.leos.ui.event.view.collection.CopyCollectionRequest;
import eu.europa.ec.leos.ui.event.view.collection.CreateReportRequest;
import eu.europa.ec.leos.ui.event.view.collection.DeletePdfEvent;
import eu.europa.ec.leos.ui.event.view.collection.DeleteReportEvent;
import eu.europa.ec.leos.ui.event.view.collection.DeleteReportRequest;
import eu.europa.ec.leos.ui.event.view.collection.SaveReportMetaDataRequest;
import eu.europa.ec.leos.ui.event.view.AGECantoDoradoStructureChangeEvent;
import eu.europa.ec.leos.ui.event.view.collection.AGECreateCantoDoradoRequest;
import eu.europa.ec.leos.ui.event.view.collection.AddCollaboratorRequest;
import eu.europa.ec.leos.ui.event.view.collection.CreateAnnexRequest;
import eu.europa.ec.leos.ui.event.view.collection.DeleteAnnexEvent;
import eu.europa.ec.leos.ui.event.view.collection.DeleteAnnexRequest;
import eu.europa.ec.leos.ui.event.view.collection.DeleteCantoDoradoEvent;
import eu.europa.ec.leos.ui.event.view.collection.DeleteCantoDoradoRequest;
import eu.europa.ec.leos.ui.event.view.collection.DeleteCollectionEvent;
import eu.europa.ec.leos.ui.event.view.collection.DeleteCollectionRequest;
import eu.europa.ec.leos.ui.event.view.collection.DownloadMandateEvent;
import eu.europa.ec.leos.ui.event.view.collection.DownloadProposalEvent;
import eu.europa.ec.leos.ui.event.view.collection.EditCollaboratorRequest;
import eu.europa.ec.leos.ui.event.view.collection.ExportMandateEvent;
import eu.europa.ec.leos.ui.event.view.collection.ExportProposalEvent;
import eu.europa.ec.leos.ui.event.view.collection.RemoveCollaboratorRequest;
import eu.europa.ec.leos.ui.event.view.collection.SaveAnnexMetaDataRequest;
import eu.europa.ec.leos.ui.event.view.collection.SearchUserRequest;
import eu.europa.ec.leos.ui.model.MilestonesVO;
import eu.europa.ec.leos.ui.support.CoEditionHelper;
import eu.europa.ec.leos.ui.view.AbstractLeosPresenter;
import eu.europa.ec.leos.ui.window.milestone.MilestoneExplorer;
import eu.europa.ec.leos.ui.wizard.document.AGECreateCantoDoradoWindow;
import eu.europa.ec.leos.usecases.document.BillContext;
import eu.europa.ec.leos.usecases.document.CollectionContext;
import eu.europa.ec.leos.usecases.document.ContextAction;
import eu.europa.ec.leos.usecases.document.MemorandumContext;
import eu.europa.ec.leos.vo.catalog.CatalogItem;
import eu.europa.ec.leos.web.event.NavigationRequestEvent;
import eu.europa.ec.leos.web.event.NotificationEvent;
import eu.europa.ec.leos.web.event.component.WindowClosedEvent;
import eu.europa.ec.leos.web.event.view.annex.OpenAnnexEvent;
import eu.europa.ec.leos.web.event.view.cantodorado.OpenCantoDoradoEvent;
import eu.europa.ec.leos.web.event.view.document.CollaboratorsUpdatedEvent;
import eu.europa.ec.leos.web.event.view.document.CollaboratorsUpdatedEvent.Operation;
import eu.europa.ec.leos.web.event.view.document.DocumentUpdatedEvent;
import eu.europa.ec.leos.web.event.view.document.OpenLegalTextEvent;
import eu.europa.ec.leos.web.event.view.memorandum.OpenMemorandumEvent;
import eu.europa.ec.leos.web.event.view.report.OpenReportEvent;
import eu.europa.ec.leos.web.event.window.SaveMetaDataRequestEvent;
import eu.europa.ec.leos.web.model.UserVO;
import eu.europa.ec.leos.web.support.SessionAttribute;
import eu.europa.ec.leos.web.support.UrlBuilder;
import eu.europa.ec.leos.web.support.UuidHelper;
import eu.europa.ec.leos.web.support.user.UserHelper;
import eu.europa.ec.leos.web.support.xml.DownloadStreamResource;
import eu.europa.ec.leos.web.ui.component.MoveAnnexEvent;
import eu.europa.ec.leos.web.ui.component.MoveReportEvent;
import eu.europa.ec.leos.web.ui.navigation.Target;
import eu.europa.ec.leos.services.export.ExportService;
import eu.europa.ec.leos.services.export.AGEExportService;
import io.atlassian.fugue.Option;

@Component
@Scope("prototype")
//AGE-CORE-3.0.0
class CollectionPresenter extends AbstractLeosPresenter {

    private static final Logger LOG = LoggerFactory.getLogger(CollectionPresenter.class);

    private static final String EXPORT_PDF_CLEAN= "C:\\Program Files (x86)\\apache-tomcat-9.0.19";

    private final CollectionScreen collectionScreen;
    private final Provider<CollectionContext> proposalContextProvider;
    private final Provider<BillContext> billContextProvider;
    private final Provider<MemorandumContext> memorandumContextProvider;
    private final AnnexService annexService;
    private final AGEReportService reportService;
    private final AGECantoDoradoService cantoDoradoService;
    private final MemorandumService memorandumService;
    private final BillService billService;
    private final PackageService packageService;
    private final UserHelper userHelper;
    private final AGEExportService exportService;
    private final MilestoneService milestoneService;
    private final SecurityService securityService;
    private final NotificationService notificationService;
    private final TemplateService templateService;
    private final MessageHelper messageHelper;
    private final CoEditionHelper coEditionHelper;
    private final LeosPermissionAuthorityMapHelper authorityMapHelper;
    private final ProposalService proposalService;
    private final UpdateInternalReferencesProducer updateInternalReferencesProducer;

    private String proposalId;
    private String proposalVersionSeriesId;
    private Set<MilestonesVO> milestonesVOs = new TreeSet<>(Comparator.comparing(MilestonesVO::getUpdatedDate).reversed());
    private final StampedLock milestonesVOsLock = new StampedLock();
    private Set<String> docVersionSeriesIds;
    AGECantoDorado cantoDorado;
    
    @Value("${leos.export.word}")
    private String pathWords;
    
    @Value("${leos.export.powershell}")
    private String pathWordsPowershell;
    
    private static final String DOWNLOAD_PREFIX = "download";

    @Autowired CollectionPresenter(SecurityContext securityContext, HttpSession httpSession, EventBus eventBus,
                                   EventBus leosApplicationEventBus,
                                   CollectionScreen collectionScreen,
                                   Provider<CollectionContext> proposalContextProvider,
                                   Provider<BillContext> billContextProvider,
                                   Provider<MemorandumContext> memoContextProvider,
                                   MemorandumService memorandumService,
                                   AnnexService annexService,
                                   AGEReportService reportService,
                                   AGECantoDoradoService cantoDoradoService,
                                   BillService billService,
                                   PackageService packageService,
                                   MilestoneService milestoneService,
                                   UserHelper userHelper,
                                   AGEExportService exportService,
                                   SecurityService securityService,
                                   NotificationService notificationService, MessageHelper messageHelper,
                                   TemplateService templateService, CoEditionHelper coEditionHelper,
                                   LeosPermissionAuthorityMapHelper authorityMapHelper,
                                   UuidHelper uuidHelper,
                                   ProposalService proposalService,
                                   WorkspaceService workspaceService,
                                   UpdateInternalReferencesProducer updateInternalReferencesProducer,
                                   TransformationService transformationService) {
        
        super(securityContext, httpSession, eventBus, leosApplicationEventBus, uuidHelper, packageService, workspaceService);
        
        this.collectionScreen = collectionScreen;
        this.proposalContextProvider = proposalContextProvider;
        this.billContextProvider = billContextProvider;
        this.annexService = annexService;
        this.billService = billService;
        this.packageService = packageService;
        this.milestoneService = milestoneService;
        this.userHelper = userHelper;
        this.exportService = exportService;
        this.securityService = securityService;
        this.notificationService = notificationService;
        this.messageHelper = messageHelper;
        this.templateService = templateService;
        this.coEditionHelper = coEditionHelper;
        this.authorityMapHelper = authorityMapHelper;
        this.proposalService = proposalService;
        this.updateInternalReferencesProducer = updateInternalReferencesProducer;
        this.memorandumContextProvider =memoContextProvider;
        this.memorandumService = memorandumService;
        this.reportService = reportService;
        this.cantoDoradoService = cantoDoradoService;
}
    
    @Override
    public void enter() {
        super.enter();
        populateData();
    }
    
    private void populateData() {
        proposalId = getProposalRef() != null ? proposalService.findProposalByRef(getProposalRef()).getId() : proposalId;
        LeosPackage leosPackage = packageService.findPackageByDocumentId(proposalId);
        List<XmlDocument> documents = packageService.findDocumentsByPackagePath(leosPackage.getPath(), XmlDocument.class, false);
        List<LegDocument> legDocuments = packageService.findDocumentsByPackageId(leosPackage.getId(), LegDocument.class, false, false);
        legDocuments.sort(Comparator.comparing(LegDocument::getLastModificationInstant).reversed());
        
        DocumentVO proposalVO = createViewObject(documents);
        collectionScreen.populateData(proposalVO);

        long stamp = milestonesVOsLock.writeLock();
        try {
            milestonesVOs.clear();
            legDocuments.forEach(document -> milestonesVOs.add(getMilestonesVO(document)));
            collectionScreen.populateMilestones(milestonesVOs);
        } finally {
            milestonesVOsLock.unlockWrite(stamp);
        }

        collectionScreen.updateUserCoEditionInfo(coEditionHelper.getAllEditInfo(), user);
    }

    private DocumentVO createViewObject(List<XmlDocument> documents) {
        DocumentVO proposalVO = new DocumentVO(LeosCategory.PROPOSAL);
        List<DocumentVO> annexVOList = new ArrayList<>();
        List<DocumentVO> reportVOList = new ArrayList<>();
        List<DocumentVO> cantoDoradoVOList = new ArrayList<>();
        docVersionSeriesIds = new HashSet<>();
        //We have the latest version of the document, no need to search for them again
        for (XmlDocument document : documents) {
            switch (document.getCategory()) {
                case PROPOSAL: {
                    Proposal proposal = (Proposal) document;
                    MetadataVO metadataVO = createMetadataVO(proposal);
                    proposalVO.setMetaData(metadataVO);
                    proposalVO.addCollaborators(proposal.getCollaborators());
                    proposalVersionSeriesId = proposal.getVersionSeriesId();
                    break;
                }
                case MEMORANDUM: {
                    Memorandum memorandum = (Memorandum) document;
                    DocumentVO memorandumVO = getMemorandumVO(memorandum);
                    proposalVO.addChildDocument(memorandumVO);
                    memorandumVO.addCollaborators(memorandum.getCollaborators());
                    memorandumVO.getMetadata().setInternalRef(memorandum.getMetadata().getOrError(() -> "Memorandum metadata is not available!").getRef());
                    memorandumVO.setVersionSeriesId(memorandum.getVersionSeriesId());
                    docVersionSeriesIds.add(memorandum.getVersionSeriesId());
                    break;
                }
                case BILL: {
                    Bill bill = (Bill) document;
                    DocumentVO billVO = getLegalTextVO(bill);
                    proposalVO.addChildDocument(billVO);
                    billVO.addCollaborators(bill.getCollaborators());
                    billVO.getMetadata().setInternalRef(bill.getMetadata().getOrError(() -> "Legal text metadata is not available!").getRef());
                    billVO.setVersionSeriesId(bill.getVersionSeriesId());
                    docVersionSeriesIds.add(bill.getVersionSeriesId());
                    break;
                }
                case ANNEX: {
                    Annex annex = (Annex) document;
                    DocumentVO annexVO = createAnnexVO(annex);
                    annexVO.addCollaborators(annex.getCollaborators());
                    annexVO.getMetadata().setInternalRef(annex.getMetadata().getOrError(() -> "Annex metadata is not available!").getRef());
                    annexVOList.add(annexVO);
                    annexVO.setVersionSeriesId(annex.getVersionSeriesId());
                    docVersionSeriesIds.add(annex.getVersionSeriesId());
                    break;
                }
                case REPORT: {
                    AGEReport report = (AGEReport) document;
                    DocumentVO reportVO = createReportVO(report);
                    reportVO.addCollaborators(report.getCollaborators());
                    reportVO.getMetadata().setInternalRef(report.getMetadata().getOrError(() -> "Report metadata is not available!").getRef());
                    reportVOList.add(reportVO);
                    reportVO.setVersionSeriesId(report.getVersionSeriesId());
                    docVersionSeriesIds.add(report.getVersionSeriesId());
                    break;
                }
                case CANTODORADO: {
                    AGECantoDorado cantoDorado = (AGECantoDorado) document;
                    DocumentVO cantoDoradoVO = createCantoDoradoVO(cantoDorado);
                    cantoDoradoVO.addCollaborators(cantoDorado.getCollaborators());
                    cantoDoradoVO.getMetadata().setInternalRef(cantoDorado.getMetadata().getOrError(() -> "CantoDorado metadata is not available!").getRef());
                    cantoDoradoVOList.add(cantoDoradoVO);
                    cantoDoradoVO.setVersionSeriesId(cantoDorado.getVersionSeriesId());
                    docVersionSeriesIds.add(cantoDorado.getVersionSeriesId());
                    break;
                }
                default:
                    LOG.debug("Do nothing for rest of the categories like MEDIA, CONFIG & LEG");
                    break;
            }
        }

        annexVOList.sort(Comparator.comparingInt(DocumentVO::getDocNumber));
        reportVOList.sort(Comparator.comparingInt(DocumentVO::getDocNumber));
        cantoDoradoVOList.sort(Comparator.comparingInt(DocumentVO::getDocNumber));
        DocumentVO legalText = (DocumentVO) proposalVO.getChildDocument(LeosCategory.BILL);
        if (legalText != null) {
            for (DocumentVO annexVO : annexVOList) {
                legalText.addChildDocument(annexVO);
            }
        }
        for (DocumentVO reportVO : reportVOList) {
            proposalVO.addChildDocument(reportVO);
        }
        
        for (DocumentVO cantoDoradoVO : cantoDoradoVOList) {
            proposalVO.addChildDocument(cantoDoradoVO);
        }

        return proposalVO;
    }
    
    private MilestonesVO getMilestonesVO(LegDocument legDocument) {
        return new MilestonesVO(legDocument.getMilestoneComments(),
                         Date.from(legDocument.getCreationInstant()),
                         Date.from(legDocument.getLastModificationInstant()),
                         messageHelper.getMessage( "milestones.column.status.value." + legDocument.getStatus().name()),
                         legDocument.getName());
    }

    // FIXME refine
    private DocumentVO getMemorandumVO(Memorandum memorandum) {
        return new DocumentVO(memorandum.getId(),
                         memorandum.getMetadata().exists(m -> m.getLanguage() != null) ? memorandum.getMetadata().get().getLanguage() : "EN",
                         LeosCategory.MEMORANDUM,
                         memorandum.getLastModifiedBy(),
                         Date.from(memorandum.getLastModificationInstant()));
    }

    // FIXME refine
    private DocumentVO getLegalTextVO(Bill bill) {
        return new DocumentVO(bill.getId(),
                         bill.getMetadata().exists(m -> m.getLanguage() != null) ? bill.getMetadata().get().getLanguage() : "EN",
                         LeosCategory.BILL,
                         bill.getLastModifiedBy(),
                         Date.from(bill.getLastModificationInstant()));
    }

    // FIXME refine
    private DocumentVO createAnnexVO(Annex annex) {
        DocumentVO annexVO =
                new DocumentVO(annex.getId(),
                        annex.getMetadata().exists(m -> m.getLanguage() != null) ? annex.getMetadata().get().getLanguage() : "EN",
                          LeosCategory.ANNEX,
                          annex.getLastModifiedBy(),
                          Date.from(annex.getLastModificationInstant()));

        if (annex.getMetadata().isDefined()) {
            AnnexMetadata metadata = annex.getMetadata().get();
            annexVO.setDocNumber(metadata.getIndex());
            annexVO.setTitle(metadata.getTitle());
        }

        return annexVO;
    }
    
    // FIXME refine
    private DocumentVO createReportVO(AGEReport report) {
        DocumentVO reportVO =
                new DocumentVO(report.getId(),
                        report.getMetadata().exists(m -> m.getLanguage() != null) ? report.getMetadata().get().getLanguage() : "EN",
                          LeosCategory.REPORT,
                          report.getLastModifiedBy(),
                          Date.from(report.getLastModificationInstant()));

        if (report.getMetadata().isDefined()) {
            AGEReportMetadata metadata = report.getMetadata().get();
            reportVO.setDocNumber(metadata.getIndex());
            reportVO.setTitle(metadata.getTitle());
        }

        return reportVO;
    }
    
    private DocumentVO createCantoDoradoVO(AGECantoDorado cantoDorado) {
        DocumentVO cantoDordoVO =
                new DocumentVO(cantoDorado.getId(),
                        cantoDorado.getMetadata().exists(m -> m.getLanguage() != null) ? cantoDorado.getMetadata().get().getLanguage() : "EN",
                          LeosCategory.CANTODORADO,
                          cantoDorado.getLastModifiedBy(),
                          Date.from(cantoDorado.getLastModificationInstant()));

        if (cantoDorado.getMetadata().isDefined()) {
            AGECantoDoradoMetadata metadata = cantoDorado.getMetadata().get();
            cantoDordoVO.setDocNumber(metadata.getIndex());
            cantoDordoVO.setTitle(metadata.getTitle());
        }

        return cantoDordoVO;
    }

    private MetadataVO createMetadataVO(Proposal proposal) {
        ProposalMetadata metadata = proposal.getMetadata().getOrError(() -> "Proposal metadata is not available!");
        return new MetadataVO(metadata.getStage(), metadata.getType(), metadata.getPurpose(), metadata.getTemplate(), metadata.getLanguage());
    }

    @Subscribe
    void saveMetaData(SaveMetaDataRequestEvent event) {
        LOG.trace("Saving proposal metadata...");
        CollectionContext context = proposalContextProvider.get();
        context.useProposal(proposalId);
        context.usePurpose(event.getMetaDataVO().getDocPurpose());
        String comment = messageHelper.getMessage("operation.metadata.updated");
        context.useActionMessage(ContextAction.METADATA_UPDATED, comment);
        context.useActionComment(comment);
        context.executeUpdateProposal();
        eventBus.post(new NotificationEvent(NotificationEvent.Type.INFO, "metadata.edit.saved"));
        // TODO optimize refresh of data on the screen
        // populateData();
    }

    @Subscribe
    void saveAnnexMetaData(SaveAnnexMetaDataRequest event) {
        // 1. get Annex
        Annex annex = annexService.findAnnex(event.getAnnex().getId());
        AnnexMetadata metadata = annex.getMetadata().getOrError(()-> "Annex metadata not found!");
        AnnexMetadata updatedMetadata = metadata.withTitle(event.getAnnex().getTitle());

        // 2. save metadata
        annexService.updateAnnex(annex, updatedMetadata, VersionType.MINOR, messageHelper.getMessage("collection.block.annex.metadata.updated"));
        eventBus.post(new DocumentUpdatedEvent());
        // 3.update ui
        eventBus.post(new NotificationEvent(NotificationEvent.Type.INFO, "collection.block.annex.metadata.updated"));
        populateData();
    }
    
    @Subscribe
    void saveReportMetaData(SaveReportMetaDataRequest event) {
        // 1. get Annex
        AGEReport report = reportService.findReport(event.getReport().getId());
        AGEReportMetadata metadata = report.getMetadata().getOrError(()-> "Report metadata not found!");
        AGEReportMetadata updatedMetadata = metadata.withTitle(event.getReport().getTitle());

        // 2. save metadata
        reportService.updateReport(report, updatedMetadata, VersionType.INTERMEDIATE, messageHelper.getMessage("collection.block.report.metadata.updated"));
        eventBus.post(new DocumentUpdatedEvent());
        // 3.update ui
        eventBus.post(new NotificationEvent(NotificationEvent.Type.INFO, "collection.block.report.metadata.updated"));
        populateData();
    }

    @Subscribe
    void createAnnex(CreateAnnexRequest event) {
        // KLUGE temporary workaround
        try {
            LeosPackage leosPackage = packageService.findPackageByDocumentId(proposalId);
            Bill bill = billService.findBillByPackagePath(leosPackage.getPath());
            BillMetadata metadata = bill.getMetadata().getOrError(() -> "Bill metadata is required!");
            BillContext billContext = billContextProvider.get();
            billContext.usePackage(leosPackage);
            billContext.useTemplate(bill);
            billContext.usePurpose(metadata.getPurpose());
            billContext.useActionMessage(ContextAction.ANNEX_METADATA_UPDATED, messageHelper.getMessage("collection.block.annex.metadata.updated"));
            billContext.useActionMessage(ContextAction.ANNEX_ADDED, messageHelper.getMessage("collection.block.annex.added"));
            billContext.useActionMessage(ContextAction.DOCUMENT_CREATED, messageHelper.getMessage("operation.document.created"));
            // KLUGE temporary workaround
            // We will have to change the ui to allow the user to select the annex child template that s/he wants.
            CatalogItem templateItem = templateService.getTemplateItem(metadata.getDocTemplate());
            String annexTemplate = templateItem.getItems().get(0).getId();
            billContext.useAnnexTemplate(annexTemplate);
            billContext.executeCreateBillAnnex();
            eventBus.post(new DocumentUpdatedEvent());
            populateData();
        } catch (Exception e) {
            LOG.error("Unexpected error occurred while creating the annex", e);
            eventBus.post(new NotificationEvent(NotificationEvent.Type.ERROR, "collection.block.annex.add.error", e.getMessage()));
        }
    }
    
    @Subscribe
    void createReport(CreateReportRequest event) {
        // KLUGE temporary workaround
        try {
            LeosPackage leosPackage = packageService.findPackageByDocumentId(proposalId);   
            Proposal proposal = proposalService.findProposalByPackagePath(leosPackage.getPath());
            ProposalMetadata metadata = proposal.getMetadata().getOrError(() -> "Proposal metadata is required!");
            AGECollectionContext proposalContext = (AGECollectionContext)proposalContextProvider.get();
            proposalContext.usePackage(leosPackage);
            proposalContext.useTemplate(proposal);
            proposalContext.usePurpose(metadata.getPurpose());            
            proposalContext.useActionMessage(ContextAction.REPORT_METADATA_UPDATED, messageHelper.getMessage("collection.block.report.metadata.updated"));
            proposalContext.useActionMessage(ContextAction.REPORT_ADDED, messageHelper.getMessage("collection.block.report.added"));
            String docTemplate = metadata.getDocTemplate();
            CatalogItem templateItem = templateService.getTemplateItem(docTemplate);
            String reportTemplate = chooseReportTemplate(templateItem);                 
            proposalContext.useReportTemplate(reportTemplate);            
            proposalContext.executeCreateProposalReport();
            eventBus.post(new DocumentUpdatedEvent());
            populateData();
        } catch (Exception e) {
            LOG.error("Unexpected error occured while creating the report: {}", e.getMessage());
            eventBus.post(new NotificationEvent(NotificationEvent.Type.ERROR, "collection.block.report.add.error", e.getMessage()));
        }
    }   

    @Subscribe
    void createCantoDorado(AGECreateCantoDoradoRequest event) {
        String organism = event.getSearchCriteria().getOrganism().getType().toUpperCase();
        String number = event.getSearchCriteria().getNumber();
        String year = event.getSearchCriteria().getYear();
        String month = event.getSearchCriteria().getMonth();
        String day = event.getSearchCriteria().getDay();
        byte[] cantoDoradoXmlContent;
        byte[] cantoDoradoNewBytes;

        try {
            LeosPackage leosPackage = packageService.findPackageByDocumentId(proposalId);   
            byte[] billXmlContent =getContent(billService.findBillByPackagePath(leosPackage.getPath()));
            
            cantoDorado = cantoDoradoService.findCantoDoradoByPackagePath(leosPackage.getPath());

            if (cantoDorado == null) {
                Proposal proposal = proposalService.findProposalByPackagePath(leosPackage.getPath());
                ProposalMetadata metadata = proposal.getMetadata().getOrError(() -> "Proposal metadata is required!");
                AGECollectionContext proposalContext = (AGECollectionContext)proposalContextProvider.get();
                proposalContext.usePackage(leosPackage);
                proposalContext.useTemplate(proposal);
                proposalContext.usePurpose(metadata.getPurpose());            
                proposalContext.useActionMessage(ContextAction.CANTODORADO_METADATA_UPDATED, messageHelper.getMessage("collection.block.cantodorado.metadata.updated"));
                proposalContext.useActionMessage(ContextAction.CANTODORADO_ADDED, messageHelper.getMessage("collection.block.cantodorado.added"));
                String docTemplate = metadata.getDocTemplate();
                CatalogItem templateItem = templateService.getTemplateItem(docTemplate);
                String cantoDoradoTemplate = chooseCantoDoradoTemplate(templateItem);                 
                proposalContext.useCantoDoradoTemplate(cantoDoradoTemplate);            
                proposalContext.executeCreateProposalCantoDorado();
                cantoDorado = cantoDoradoService.findCantoDoradoByPackagePath(leosPackage.getPath());
                cantoDoradoXmlContent =getContent(cantoDorado);
                cantoDoradoNewBytes = cantoDoradoService.updateCantoDoradoFromBill(billXmlContent, cantoDoradoXmlContent, number, year, month, day, organism, true);
            } else {
                cantoDoradoXmlContent =getContent(cantoDorado);
                cantoDoradoNewBytes = cantoDoradoService.updateCantoDoradoFromBill(billXmlContent, cantoDoradoXmlContent, number, year, month, day, organism, false);
            }       
            cantoDoradoService.updateCantoDorado(cantoDorado, cantoDoradoNewBytes, VersionType.MAJOR, "Actualizado");
            downloadPdf();
            eventBus.post(new DocumentUpdatedEvent());
            populateData();
        } catch (Exception e) {
            LOG.error("Unexpected error occured while creating the canto dorado: {}", e.getMessage());
            eventBus.post(new NotificationEvent(NotificationEvent.Type.ERROR, "collection.block.cantodorado.add.error", e.getMessage()));
        }
    }
    
    private void downloadPdf () {
        File downloadFile = null;
        downloadFile = exportService.exportToPdfCantoDorado(proposalId);
        FileResource res = new FileResource(downloadFile);
        Page.getCurrent().open(res, "_blank", true);
    }
    
    @Subscribe
    void CopyCollection(CopyCollectionRequest event) {

        String proposalIdOld = proposalId;
        LeosPackage leosPackageOld = packageService.findPackageByDocumentId(proposalIdOld);
        Proposal proposalOld = proposalService.findProposalByPackagePath(leosPackageOld.getPath());
        ProposalMetadata metadataOld = proposalOld.getMetadata().getOrError(() -> "Proposal metadata is required!");

        List<XmlDocument> documentsOld = packageService.findDocumentsByPackagePath(leosPackageOld.getPath(), XmlDocument.class, false);

        AGECollectionContext context = (AGECollectionContext)proposalContextProvider.get();//AGE-CORE-3.0.0
        
        Bill billOld = billService.findBillByPackagePath(leosPackageOld.getPath());
        BillMetadata metadataBillOld =billOld.getMetadata().getOrError(() -> "Bill metadata is required!");
        
        String template = metadataOld.getDocTemplate();
        template += ";"+ metadataBillOld.getDocTemplate();
        String[] templates = (template != null) ? template.split(";") : new String[0];

        for (String name : templates) {
            context.useTemplate(name);
        }
        context.usePurpose(metadataOld.getPurpose() + "- Copia");
        context.useActionMessage(ContextAction.METADATA_UPDATED, messageHelper.getMessage("operation.metadata.updated"));
        LOG.trace("Executing 'Create Proposal' use case...");

        LeosPackage leosPackageNew = packageService.createPackage();

        Proposal proposalTemplate = context.cast(context.getCategoryTemplateMap().get(PROPOSAL));
        Validate.notNull(proposalTemplate, "Proposal template is required!");

        Option<ProposalMetadata> metadataOptionProp = proposalTemplate.getMetadata();
        Validate.isTrue(metadataOptionProp.isDefined(), "Proposal metadata is required!");

        ProposalMetadata metadataNew = metadataOptionProp.get().withPurpose(metadataOld.getPurpose() + "- Copia");

        Proposal proposalNew = proposalService.createProposal(proposalTemplate.getId(), leosPackageNew.getPath(), metadataNew,null);

        AGEBillContext billContext = (AGEBillContext) billContextProvider.get();
        billContext.usePackage(leosPackageNew);
        billContext.useTemplate(context.cast(context.getCategoryTemplateMap().get(BILL)));
        billContext.usePurpose(metadataOld.getPurpose() + "- Copia");
        billContext.useActionMessageMap(billContext.getActionMsgMap());
        Bill billNew = billContext.executeCreateBill();

        proposalService.addComponentRef(proposalNew, billNew.getName(), LeosCategory.BILL);

        List<String> annexIds = new ArrayList<>();
        List<String> reportsIds = new ArrayList<>();
        List<String> memoIds = new ArrayList<>();
        
        docVersionSeriesIds = new HashSet<>();
        for (XmlDocument document : documentsOld) {
            switch (document.getCategory()) { 
            case MEMORANDUM: {
                Memorandum memorandum = (Memorandum) document;
                MemorandumContext memorandumContext = memorandumContextProvider.get();
                memorandumContext.usePackage(leosPackageNew);
                memorandumContext.useTemplate(memorandum);
                memorandumContext.usePurpose(metadataOld.getPurpose() + "- Copia");
                DocumentVO memorandumVO = getMemorandumVO(memorandum);
                memorandumContext.useDocument(memorandumVO);
                memorandumContext.useType(metadataOld.getType());
                memorandumContext.executeCreateMemorandum();
                memoIds.add(document.getId());
                break;
            }
            case BILL: {
                byte[] newXmlContent =getContent(billService.findBillByPackagePath(leosPackageOld.getPath()));
                //BillMetadata metadataBill = metadataOptionBill.get().withPurpose(metadataOld.getPurpose() + "- Copia");
                billService.updateBill(billNew, newXmlContent, "copy");
                break;
            }
            case ANNEX: {
                try {
                    billContext.useActionMessage(ContextAction.ANNEX_METADATA_UPDATED, messageHelper.getMessage("collection.block.annex.metadata.updated"));
                    billContext.useActionMessage(ContextAction.ANNEX_ADDED, messageHelper.getMessage("collection.block.annex.added"));
                    BillMetadata metadata = billNew.getMetadata().getOrError(() -> "Bill metadata is required!");
                    CatalogItem templateItem = templateService.getTemplateItem(metadata.getDocTemplate());
                    String annexTemplate = templateItem.getItems().get(0).getId();
                    billContext.useAnnexTemplate(annexTemplate);
                    billContext.executeCreateBillAnnex();
                    annexIds.add(document.getId());
                } catch (Exception e) {
                    LOG.error("Unexpected error occured while creating the annex: {}", e.getMessage());
                    eventBus.post(new NotificationEvent(NotificationEvent.Type.ERROR, "collection.block.annex.add.error", e.getMessage()));
                }
                break;
            }
            case REPORT: {
                try {   
                    context.usePackage(leosPackageNew);
                    context.useTemplate(proposalNew);
                    context.usePurpose(metadataNew.getPurpose()+ " - Copia");            
                    context.useActionMessage(ContextAction.REPORT_METADATA_UPDATED, messageHelper.getMessage("collection.block.report.metadata.updated"));
                    context.useActionMessage(ContextAction.REPORT_ADDED, messageHelper.getMessage("collection.block.report.added"));
                    String docTemplate = metadataNew.getDocTemplate();
                    CatalogItem templateItem = templateService.getTemplateItem(docTemplate);
                    String reportTemplate = chooseReportTemplate(templateItem);                 
                    context.useReportTemplate(reportTemplate);  
                    context.executeCreateProposalReport();
                    reportsIds.add(document.getId());
                } catch (Exception e) {
                    LOG.error("Unexpected error occured while creating the report: {}", e.getMessage());
                    eventBus.post(new NotificationEvent(NotificationEvent.Type.ERROR, "collection.block.report.add.error", e.getMessage()));
                }
            }
            }
        }
        List<Annex> annexes = packageService.findDocumentsByPackagePath(leosPackageNew.getPath(), Annex.class, false);
        for (int cont = 0;cont < annexes.size(); cont ++ ) {
            Annex annexOld = annexService.findAnnex(annexIds.get(cont));
            byte[] newXmlContentAnnex =getContent(annexOld);
            annexService.updateAnnex(annexes.get(cont), newXmlContentAnnex, VersionType.MINOR, "copy");
        }
        List<AGEReport> reports = packageService.findDocumentsByPackagePath(leosPackageNew.getPath(), AGEReport.class, false);      
        for (int cont = 0;cont < reports.size(); cont ++ ) {
            AGEReport reportOld = reportService.findReport(reportsIds.get(cont));
            byte[] newXmlContentReport =getContent(reportOld);
            reportService.updateReport(reports.get(cont), newXmlContentReport, false, "copy");
        }
        List<Memorandum> memos = packageService.findDocumentsByPackagePath(leosPackageNew.getPath(), Memorandum.class, false);      
        for (int cont = 0;cont < memos.size(); cont ++ ) {
            Memorandum memoOld = memorandumService.findMemorandum(memoIds.get(cont));
            byte[] newXmlContentMemo =getContent(memoOld);
            memorandumService.updateMemorandum(memos.get(cont), newXmlContentMemo, VersionType.MINOR, "copy");
        }
        eventBus.post(new NotificationEvent(NotificationEvent.Type.INFO, "collection.info.button.copy", metadataOld.getStage() + " " + metadataOld.getType() 
                                            + " " + metadataOld.getPurpose()  + "- Copia"));
    }
    
 
    
    private String chooseReportTemplate(CatalogItem catalog) {
        String[] templates = catalog.getId().split(";");
        for(String template : templates) {
            if(template.contains("RE-"))
                return template;
        }
        return "RE-01";
    }
    
    //CUARENTENA
    private String chooseCantoDoradoTemplate(CatalogItem catalog) {
        String[] templates = catalog.getId().split(";");
        for(String template : templates) {
            if(template.contains("CD-"))
                return template;
        }
        return "CD-01";
    }

    @Subscribe
    void deleteAnnexRequest(DeleteAnnexRequest event) {
        collectionScreen.confirmAnnexDeletion(event.getAnnex());
    }
    
    @Subscribe
    void deleteReportRequest(DeleteReportRequest event) {
        collectionScreen.confirmReportDeletion(event.getReport());
    }
    
    @Subscribe
    void deleteCantoDoradoRequest(DeleteCantoDoradoRequest event) {
        collectionScreen.confirmCantoDoradoDeletion(event.getCantoDorado());
    }

    @Subscribe
    void deleteAnnex(DeleteAnnexEvent event) {
        // FIXME To be implemented, this part is commented: add a confirmation dialog?
        // 1. delete Annex
        LeosPackage leosPackage = packageService.findPackageByDocumentId(proposalId);

        BillContext billContext = billContextProvider.get();
        billContext.useAnnex(event.getAnnex().getId());
        billContext.usePackage(leosPackage);
        billContext.useActionMessage(ContextAction.ANNEX_METADATA_UPDATED, messageHelper.getMessage("collection.block.annex.metadata.updated"));
        billContext.useActionMessage(ContextAction.ANNEX_DELETED, messageHelper.getMessage("collection.block.annex.removed"));
        billContext.executeRemoveBillAnnex();
        updateInternalReferencesProducer.send(new UpdateInternalReferencesMessage(proposalId, event.getAnnex().getMetadata().getInternalRef(), id));
        eventBus.post(new DocumentUpdatedEvent());
        // 2. update ui
        populateData();
        eventBus.post(new NotificationEvent(NotificationEvent.Type.INFO, "annex.deleted"));
    }
    
    @Subscribe
    void deleteReport(DeleteReportEvent event) {
        // FIXME To be implemented, this part is commented: add a confirmation dialog?
        // 1. delete Report       
        LeosPackage leosPackage = packageService.findPackageByDocumentId(proposalId);
        AGECollectionContext proposalContext = (AGECollectionContext) proposalContextProvider.get();
        proposalContext.usePackage(leosPackage);
        proposalContext.useReport(event.getReport().getId());
        proposalContext.useActionMessage(ContextAction. REPORT_METADATA_UPDATED, messageHelper.getMessage("collection.block.report.metadata.updated"));
        proposalContext.useActionMessage(ContextAction.REPORT_DELETED, messageHelper.getMessage("collection.block.report.removed"));
        proposalContext.executeRemoveProposalReport();
        eventBus.post(new DocumentUpdatedEvent());
        // 2. update ui
        populateData();
        eventBus.post(new NotificationEvent(NotificationEvent.Type.INFO, "report.deleted"));
    }

    @Subscribe
    void deleteCantoDorado (DeleteCantoDoradoEvent event) {
 
        LeosPackage leosPackage = packageService.findPackageByDocumentId(proposalId);
        AGECollectionContext proposalContext = (AGECollectionContext) proposalContextProvider.get();
        proposalContext.usePackage(leosPackage);
        proposalContext.useCantoDorado(event.getCantoDorado().getId());
        proposalContext.useActionMessage(ContextAction. CANTODORADO_METADATA_UPDATED, messageHelper.getMessage("collection.block.report.cantodorado.updated"));
        proposalContext.useActionMessage(ContextAction.CANTODORADO_DELETED, messageHelper.getMessage("collection.block.cantodorado.removed"));
        proposalContext.executeRemoveProposalCantoDorado();
        eventBus.post(new DocumentUpdatedEvent());
        // 2. update ui
        populateData();
        eventBus.post(new NotificationEvent(NotificationEvent.Type.INFO, "cantodorado.deleted"));
    }
    
    @Subscribe
    void moveAnnex(MoveAnnexEvent event) {
        //1.call Service to reorder and update annexes
        LeosPackage leosPackage = packageService.findPackageByDocumentId(proposalId);

        BillContext billContext = billContextProvider.get();
        billContext.usePackage(leosPackage);
        billContext.useMoveDirection(event.getDirection().toString());
        billContext.useAnnex(event.getAnnexVo().getId());
        billContext.useActionMessage(ContextAction.ANNEX_METADATA_UPDATED, messageHelper.getMessage("collection.block.annex.metadata.updated"));
        billContext.executeMoveAnnex();
        eventBus.post(new DocumentUpdatedEvent());
        //2. update screen to show update
        populateData();
    }
    
    @Subscribe
    void moveReport(MoveReportEvent event) {
        //1.call Service to reorder and update reports
        LeosPackage leosPackage = packageService.findPackageByDocumentId(proposalId);
        Proposal proposal = proposalService.findProposalByPackagePath(leosPackage.getPath());
        AGECollectionContext collectionContext = (AGECollectionContext) proposalContextProvider.get();
        collectionContext.usePackage(leosPackage);
        collectionContext.useMoveDirection(event.getDirection().toString());
        collectionContext.useReport(event.getReportVo().getId());
        collectionContext.useTemplate(proposal);
        collectionContext.useActionMessage(ContextAction.REPORT_METADATA_UPDATED, messageHelper.getMessage("collection.block.report.metadata.updated"));
        collectionContext.executeMoveReport();
        eventBus.post(new DocumentUpdatedEvent());
        //2. update screen to show update
        populateData();
    }

    private String getProposalRef() {
        return (String) httpSession.getAttribute(id + "." + SessionAttribute.PROPOSAL_REF.name());
    }

    @Subscribe
    void openMemorandum(OpenMemorandumEvent event) {
        eventBus.post(new NavigationRequestEvent(Target.MEMORANDUM, event.getMemorandum().getMetadata().getInternalRef()));
    }

    @Subscribe
    void openLegalText(OpenLegalTextEvent event) {
        eventBus.post(new NavigationRequestEvent(Target.LEGALTEXT, event.getLegalText().getMetadata().getInternalRef()));
    }

    @Subscribe
    void openAnnex(OpenAnnexEvent event) {
        eventBus.post(new NavigationRequestEvent(Target.ANNEX, event.getAnnex().getMetadata().getInternalRef()));
    }
    
    @Subscribe
    void openReport(OpenReportEvent event) {
        eventBus.post(new NavigationRequestEvent(Target.REPORT, event.getReport().getMetadata().getInternalRef()));
    }
    
    @Subscribe
    void openCantoDorado(OpenCantoDoradoEvent event) {
        eventBus.post(new NavigationRequestEvent(Target.CANTODORADO, event.getCantoDorado().getMetadata().getInternalRef()));
    }

    @Subscribe
    void deleteCollectionRequest(DeleteCollectionRequest event) {
        collectionScreen.confirmCollectionDeletion();
    }

    @Subscribe
    void createMilestoneRequest(CreateMilestoneRequestEvent event){
        collectionScreen.openCreateMilestoneWindow();
    }

    @Subscribe
    void createMilestone(CreateMilestoneEvent event) {
        final String milestoneComment = event.getMilestoneComment();
        final String versionComment = messageHelper.getMessage("milestone.versionComment");
        final CollectionContext context = proposalContextProvider.get();
        createMajorVersions(proposalId, milestoneComment, versionComment, context);
        String milestoneProposalId = context.getUpdatedProposalId();
        saveLegFileInCMIS(milestoneProposalId, milestoneComment);
        populateData();
        eventBus.post(new NavigationRequestEvent(Target.PROPOSAL, getProposalRef()));
    }

    @Subscribe
    public void addNewMilestone(MilestoneCreatedEvent milestoneCreatedEvent){
        if(milestoneCreatedEvent.getProposalVersionSeriesId().equals(proposalVersionSeriesId)){
            MilestonesVO newMilestonesVO = getMilestonesVO(milestoneCreatedEvent.getLegDocument());
            long stamp = milestonesVOsLock.writeLock();
            try {
                if(milestonesVOs.add(newMilestonesVO)){
                    collectionScreen.populateMilestones(milestonesVOs);
                    eventBus.post(new NotificationEvent(leosUI, "milestone.caption", "milestone.creation", NotificationEvent.Type.TRAY, StringEscapeUtils.escapeHtml4(newMilestonesVO.getTitle())));
                }
            } finally {
                milestonesVOsLock.unlockWrite(stamp);
            }
        }
    }

    @Subscribe
    public void updateMilestones(MilestoneUpdatedEvent milestoneUpdatedEvent) {
        MilestonesVO eventMilestone = getMilestonesVO(milestoneUpdatedEvent.getLegDocument());
        long stamp = milestonesVOsLock.writeLock();
        try{
            MilestonesVO milestoneToBeUpdated = milestonesVOs.stream().filter(milestonesVO -> milestonesVO.getLegDocumentName().equals(eventMilestone.getLegDocumentName()))
                                                             .findAny().orElse(null);
            if(milestoneToBeUpdated != null && !milestoneToBeUpdated.equals(eventMilestone)){
                milestoneToBeUpdated.setStatus(eventMilestone.getStatus());
                milestoneToBeUpdated.setUpdatedDate(eventMilestone.getUpdatedDate());
                collectionScreen.populateMilestones(milestonesVOs);
                eventBus.post(new NotificationEvent(leosUI, "milestone.caption", "milestone.updated", NotificationEvent.Type.TRAY, StringEscapeUtils.escapeHtml4(milestoneToBeUpdated.getTitle())));
            }
        } finally {
             milestonesVOsLock.unlockWrite(stamp);
        }
    }

    /**
     * Create a major version in CMIS for all documents of this proposal: bill, memorandum, annexes, etc.
     * If already a major version, do not create it.
     * @param proposalId the proposal Id
     * @param milestoneComment the milestone title
     * @param versionComment the version comment
     * @param context the proposal context
     */
    private void createMajorVersions(String proposalId, String milestoneComment, String versionComment, CollectionContext context) {
        context.useProposal(proposalId);
        context.useMilestoneComment(milestoneComment);
        context.useVersionComment(versionComment);
        context.executeCreateMilestone();
    }

    private void saveLegFileInCMIS(String proposalId, String milestoneComment) {
        try{
            LegDocument newLegDocument = milestoneService.createMilestone(proposalId, milestoneComment);
            eventBus.post(new NotificationEvent(NotificationEvent.Type.INFO, "document.create.milestone.created"));
            leosApplicationEventBus.post(new MilestoneCreatedEvent(proposalVersionSeriesId, newLegDocument));
            LOG.trace("Milestone creation successfully requested leg");
        } catch(WebServiceException wse) {
            LOG.error("External system not available due to WebServiceException", wse);
            eventBus.post(new NotificationEvent(NotificationEvent.Type.ERROR, "document.create.milestone.url.error", wse.getMessage()));
        } catch (Exception e){
            LOG.error("Milestone creation request failed.", e);
            eventBus.post(new NotificationEvent(NotificationEvent.Type.ERROR, "document.create.milestone.error", e.getMessage()));
        }
    }

    @Subscribe
    void deleteCollectionEvent(DeleteCollectionEvent event) {
        CollectionContext context = proposalContextProvider.get();
        context.useProposal(proposalId);
        context.executeDeleteProposal();
        eventBus.post(new NotificationEvent(NotificationEvent.Type.INFO, "collection.deleted"));
        eventBus.post(new NavigationRequestEvent(Target.WORKSPACE));
    }
    
    @Subscribe
    void exportProposal(ExportProposalEvent event) {
        
        
        File downloadFile = null;
        try {
            String formatExport;
            switch (event.getExportOptions()) {
                case TO_PDF:                    
                    downloadFile = exportService.exportToPdf(proposalId); 
                    formatExport = "Pdf";
                    DownloadStreamResource downloadStreamResource = new DownloadStreamResource(
                            getDownloadFileName(formatExport), new FileInputStream(downloadFile));
                    Page.getCurrent().open(downloadStreamResource, "_blank", true);
                   // collectionScreen.setExportPdfStreamResource(downloadStreamResource);
                    eventBus.post(new NotificationEvent("menu.download.caption", "collection.downloaded", NotificationEvent.Type.TRAY));                   
                    break;
                case TO_PDF_LW:
                case TO_WORD_LW:
                    // Esto es lo que había originalmente para exportar a word
                    /*String jobId = exportService.exportToToolboxCoDe(proposalId, event.getExportOptions());
                    formatExport = event.getExportOptions().equals(ExportOptions.TO_PDF_LW) ? "Pdf" : "Legiswrite";    
                    eventBus.post(new NotificationEvent("collection.caption.menuitem.export", "collection.exported", NotificationEvent.Type.TRAY, formatExport, user.getEmail(), jobId));
                    */
                    
                    // Para la medición de tiempos
                    long start = System.currentTimeMillis();
                
                    // Primero exporta a pdf
                    downloadFile = exportService.exportToPdf(proposalId);                    
                    String entrada = downloadFile.getAbsolutePath();
                    String directorioWords = pathWords;
                    LOG.debug("Directorio words: "+pathWords);
                    
                    String directorioPowershell = pathWordsPowershell;
                    LOG.debug("Directorio del script powershell: "+pathWordsPowershell);
                    
                    // Los directorios deben ser absolutos
                    if (directorioWords.startsWith(".") || directorioWords.isEmpty()) {
                        LOG.debug("Usando el directorio de words por defecto");
                        directorioWords = System.getProperty("user.dir")+"\\"+pathWords;
                    }
                    if (directorioPowershell.startsWith(".") || directorioPowershell.isEmpty()) {
                        LOG.debug("Usando el directorio de powershell por defecto");
                        directorioPowershell = System.getProperty("user.dir")+"\\"+pathWords;
                    }
                    //directorioWords.replaceAll("/", "\\");
                    Date salida_ts = new Date();
                    
                    // Se añade un timestamp al nombre del archivo para reducir la posibilidad problemas con descargas concurrentes
                    String salida = directorioWords+"\\"+proposalId+"_"+salida_ts.getTime()+".docx"; //"pdf_leos_convertido_params.docx";
                    String command = "powershell.exe -file \""+directorioPowershell+"\\convertirPdfDocx.ps1\" "+"\""+entrada+"\" \""+salida+"\"";
                    command= command.replaceAll("\\\\\\\\", "\\\\");
                    
                    File archivoEntrada = new File(entrada);
                    if (archivoEntrada.exists()) {
                        LOG.debug("Existe el archivo de entrada: "+entrada);                        
                    }
                    else {
                        LOG.debug("NO existe el archivo de entrada: "+entrada);
                        LOG.error("Se esperaba el archivo: "+entrada);
                    }
                    try {
                        
                        // Borra los words antiguos del directorio para que no se acumulen
                        File path = new File(directorioWords);
                        File [] files = path.listFiles();
                        LOG.debug("Limpieza del directorio de .DOCXs: "+pathWords);                     
                        for (int i = 0; i < files.length; i++){
                            if (
                                    files[i].isFile() && 
                                    files[i].getName().contains(".docx")
                                ){
                                long diff = new Date().getTime() - files[i].lastModified();
                                // Lo borra si ha sido generado hace más de 5 minutos
                                if (diff > 5* 60 * 1000) {
                                    files[i].delete();
                                    LOG.debug("Borrando: "+files[i]);
                                }                               
                                else {
                                    LOG.debug("Aún no se borra: "+files[i]);
                                }
                            }
                        }                       
                        
                        // Borra el archivo a exportar si ya existe, sea cual sea su fecha
                        File archivoDocx = new File(salida);
                        if (archivoDocx.exists()) {
                            LOG.debug("Borrando archivo previo: "+salida);
                            archivoDocx.delete();
                        }
                                            
                        
                        // Ejecuta el comando de conversión de pdf a docx
                        Process powerShellProcess = Runtime.getRuntime().exec(command);

                        // Salida del comando
                        LOG.debug("Comando: "+command);      
                        BufferedReader stdInput = new BufferedReader(new InputStreamReader(powerShellProcess.getErrorStream()));
                        String line;
                        LOG.debug("Salida de errores del comando:");
                        while ((line = stdInput.readLine()) != null) {
                        	LOG.debug("Salida de error: "+line);
                        }         
                        LOG.debug("Fin de la salida de errores");
                        
                        int intentos = 0;
                        // Espera a que se genere el archivo
                        while(!archivoDocx.exists() && intentos < 30) {
                            try {
                                LOG.debug(intentos+" - Esperando DOCX...");
                                intentos++;
                                if (archivoEntrada.exists()) {
                                    LOG.debug("Existe el archivo de entrada: "+entrada);                        
                                }
                                else {
                                    LOG.debug("NO existe el archivo de entrada: "+entrada);
                                    LOG.error("Se esperaba el archivo: "+entrada);
                                }
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block                              
                                e.printStackTrace();
                            }
                        }
						
						powerShellProcess.getOutputStream().close();
                        
                        // ¿Ha funcionado?
                        if (archivoDocx.exists()) {                     
                            LOG.debug("Documento exportado: "+salida);
                            // Intenta la descarga
                            formatExport = "DOCX";
                            LOG.debug("Archivo a descargar: "+archivoDocx.getAbsolutePath());
                            if (!archivoDocx.exists()) {
                                LOG.debug("El archivo no existe");
                            }
                            else {
                                LOG.debug("Intentando descargar...");
                                DownloadStreamResource downloadStreamResourceDocx = new DownloadStreamResource(
                                        getDownloadFileName(formatExport), new FileInputStream(archivoDocx));
                                collectionScreen.setExportPdfStreamResource(downloadStreamResourceDocx);
                                eventBus.post(new NotificationEvent("menu.download.caption", "collection.downloaded", NotificationEvent.Type.TRAY));                            
                            }
                        }
                        else {
                            LOG.error("Documento NO exportado");
                        }                                               
                    }
                    catch (IOException e) {
                        LOG.error("Error en la conversión a .DOCX: "+e.getMessage());
                    }
                    LOG.debug("Generación de .DOCX finalizada");
                    long finish = System.currentTimeMillis();
                    long timeElapsed = finish - start;
                    LOG.info("Tiempo en exportar .DOCX: "+(timeElapsed/1000)+" segundos");
                    
                    break;
                default:
                    throw new Exception("Bad export format option");
            }
        } catch(WebServiceException wse) {
            LOG.error("External system not available due to WebServiceException: {}", wse.getMessage());
            eventBus.post(new NotificationEvent(NotificationEvent.Type.ERROR, "collection.export.url.error", wse.getMessage()));
        } catch (Exception e) {
            LOG.error("Unexpected error occured while sending job to ToolBox: {}", e.getMessage());
            eventBus.post(new NotificationEvent(NotificationEvent.Type.ERROR, "collection.export.error", e.getMessage()));
        }finally {
            if (downloadFile != null && downloadFile.exists()) {
                downloadFile.delete();
            }
        }
    }
    
    //Evolutivo #2543
    @Subscribe
    void deletePdfEvent(DeletePdfEvent event) {
        try {
            String pathTomcat = System.getProperty("catalina.base");
            File carpeta;
            File[] files;
            if (pathTomcat != null) {
                //DES PRE PRO
                carpeta = new File(EXPORT_PDF_CLEAN);
                files =carpeta.listFiles();
            } else {
                //LOCAL
                 carpeta = new File(System.getProperty("user.dir"));
                 files =carpeta.listFiles();
            }
            if (files != null) {
                for(File f : files) {
                    if(f.exists() && f.getName().endsWith(".pdf"))
                        f.delete();
                }   
            }
        }catch (Exception ex) {
             LOG.error("Unexpected error occured while delete the pdf: {}", ex.getMessage());
             eventBus.post(new NotificationEvent(NotificationEvent.Type.ERROR, "collection.export.error", ex.getMessage()));
        }
        
    }

    private String getDownloadFileName(String extension) throws Exception {
        return String.format("%s_%s_%s.%s", DOWNLOAD_PREFIX, proposalId, String.valueOf((new Date()).getTime()), extension.toLowerCase());
    }

    @Subscribe
    void exportMandate(ExportMandateEvent event) {
        try {
            LeosPackage leosPackage = packageService.findPackageByDocumentId(proposalId);
            BillContext context = billContextProvider.get();
            context.usePackage(leosPackage);
            if (proposalId != null) {
                File resultOutputFile = exportService.createDocuWritePackage("Proposal_" + proposalId + "_" + System.currentTimeMillis()+".zip", proposalId, event.getExportOptions(), Optional.empty());
                DownloadStreamResource downloadStreamResource = new DownloadStreamResource(resultOutputFile.getName(), new FileInputStream(resultOutputFile));
                collectionScreen.setDownloadStreamResource(downloadStreamResource);
            }
        } catch (Exception e) {
            LOG.error("Unexpected error occurred while using ExportService", e);
            eventBus.post(new NotificationEvent(NotificationEvent.Type.ERROR, "export.docuwrite.error.message", e.getMessage()));
        }
    }

    private String getJobFileName() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("Proposal_");
        strBuilder.append(proposalId);
        strBuilder.append(".zip");
        return strBuilder.toString();
    }

    private void prepareDownloadPackage(File packageFile) throws FileNotFoundException {
        if (packageFile != null) {
            DownloadStreamResource downloadStreamResource = new DownloadStreamResource(packageFile.getName(), new FileInputStream(packageFile));
            collectionScreen.setDownloadStreamResource(downloadStreamResource);
            eventBus.post(new NotificationEvent("menu.download.caption", "collection.downloaded", NotificationEvent.Type.TRAY));
            LOG.trace("Successfully prepared proposal for download");
        }
    }

    @Subscribe
    void prepareProposalDownloadPackage(DownloadProposalEvent event) {
        String jobFileName = getJobFileName();
        File packageFile = null;
        try {
            packageFile = exportService.createCollectionPackage(jobFileName, proposalId, ExportOptions.TO_WORD_LW);
            prepareDownloadPackage(packageFile);
        } catch (Exception e) {
            LOG.error("Error while creating download proposal package", e);
            eventBus.post(new NotificationEvent(NotificationEvent.Type.ERROR, "collection.downloaded.error", e.getMessage()));
        } finally {
            if (packageFile != null && packageFile.exists()) {
                packageFile.delete();
            }
        }
    }

    @Subscribe
    void prepareMandateDownloadPackage(DownloadMandateEvent event) {
        String jobFileName = getJobFileName();
        File packageFile = null;
        try {
            packageFile = exportService.createCollectionPackage(jobFileName, proposalId, ExportOptions.TO_WORD_DW);
            prepareDownloadPackage(packageFile);
        } catch (Exception e) {
            LOG.error("Error while creating download proposal package", e);
            eventBus.post(new NotificationEvent(NotificationEvent.Type.ERROR, "collection.downloaded.error", e.getMessage()));
        } finally {
            if (packageFile != null && packageFile.exists()) {
                packageFile.delete();
            }
        }
    }

    @Subscribe
    void closeProposalView(CloseScreenRequestEvent event) {
        eventBus.post(new NavigationRequestEvent(Target.PREVIOUS));
    }

    @Subscribe
    void searchUser(SearchUserRequest event) {
        List<User> users = userHelper.searchUsersByKey(event.getSearchKey());
        collectionScreen.proposeUsers(users.stream().map(UserVO::new).collect(Collectors.toList()));
    }

    @Subscribe
    void addCollaborator(AddCollaboratorRequest event) {
        User user = event.getCollaborator().getUser();
        Role role = event.getCollaborator().getRole();
        LOG.trace("Adding collaborator...{}, with authority {}", user.getLogin(), role);
        getXmlDocuments().forEach(doc -> updateCollaborators(user, role, doc, false));

        notificationService.sendNotification(new AddCollaborator(user, role.getName(), proposalId, event.getProposalURL()));
        eventBus.post(new NotificationEvent(NotificationEvent.Type.INFO, "collaborator.message.added", user.getName(),
                messageHelper.getMessage(role.getMessageKey())));
        leosApplicationEventBus.post(new CollaboratorsUpdatedEvent(id, proposalVersionSeriesId, Operation.ADDED));
    }

    @Subscribe
    void removeCollaborator(RemoveCollaboratorRequest event) {
        User user = event.getCollaborator().getUser();
        Role role = event.getCollaborator().getRole();
        List<XmlDocument> documents = getXmlDocuments();
        LOG.trace("Removing collaborator...{}, with authority {}", user.getLogin(), role);
        if (checkCollaboratorIsNotLastOwner(documents, role)) {
            documents.forEach(doc -> updateCollaborators(user, role, doc, true));
            notificationService.sendNotification(new RemoveCollaborator(user, role.getName(), proposalId, event.getProposalURL()));
            eventBus.post(new NotificationEvent(NotificationEvent.Type.INFO, "collaborator.message.removed", user.getName()));
        } else {
            eventBus.post(new NotificationEvent(NotificationEvent.Type.ERROR, "collaborator.message.last.owner.removed",
                    messageHelper.getMessage(role.getMessageKey())));
        }
        leosApplicationEventBus.post(new CollaboratorsUpdatedEvent(id, proposalVersionSeriesId, Operation.REMOVED));
    }

    @Subscribe
    void editCollaborator(EditCollaboratorRequest event) {
        User user = event.getCollaborator().getUser();
        Role role = event.getCollaborator().getRole();
        List<XmlDocument> documents = getXmlDocuments();
        Role oldRole = authorityMapHelper.getRoleFromListOfRoles(documents.get(0).getCollaborators().get(user.getLogin()));
        LOG.trace("Updating collaborator...{}, old authority {}, with new authority {}", user.getLogin(), oldRole, role);
        if (checkCollaboratorIsNotLastOwner(documents, oldRole)) {
            documents.forEach(doc -> updateCollaborators(user, role, doc, false));

            notificationService.sendNotification(new EditCollaborator(user, role.getName(), proposalId, event.getProposalURL()));
            eventBus.post(new NotificationEvent(NotificationEvent.Type.INFO, "collaborator.message.edited", user.getName(),
                    messageHelper.getMessage(oldRole.getMessageKey()),
                    messageHelper.getMessage(role.getMessageKey())));
        } else {
            eventBus.post(new NotificationEvent(NotificationEvent.Type.ERROR, "collaborator.message.last.owner.edited",
                    messageHelper.getMessage(oldRole.getMessageKey())));
        }
        leosApplicationEventBus.post(new CollaboratorsUpdatedEvent(id, proposalVersionSeriesId, Operation.EDITED));
    }

    //Update document based on action(add/edit/remove)
    private void updateCollaborators(User user, Role role, XmlDocument doc, boolean isRemoveAction) {
        Map<String, String> collaborators = doc.getCollaborators();
        if (isRemoveAction) {
            collaborators.remove(user.getLogin());
        } else {
            collaborators.put(user.getLogin(), role.getName());
        }
        securityService.updateCollaborators(doc.getId(), collaborators, doc.getClass());
    }

    //Check if in collaborators there is only one author
    private boolean checkCollaboratorIsNotLastOwner(List<XmlDocument> documents, Role role) {
        boolean isLastOwner = false;
        boolean canAddRemoveCollaborators = false;
        for(String permission : role.getPermissions().getPermissions()){
            if(CAN_ADD_REMOVE_COLLABORATOR.name().equals(permission)){
                canAddRemoveCollaborators = true;
            }
        }
        if (role.isCollaborator() && canAddRemoveCollaborators) {
            Map<String, String> collaborators = documents.get(0).getCollaborators();
            if (Collections.frequency(collaborators.values(), role.getName()) == 1) {
                isLastOwner = true;
            }
        }
        return !isLastOwner;
    }

    private List<XmlDocument> getXmlDocuments() {
        LeosPackage leosPackage = packageService.findPackageByDocumentId(proposalId);
        return packageService.findDocumentsByPackagePath(leosPackage.getPath(), XmlDocument.class, false);
    }

    @Subscribe
    private void onCollaboratorsUpdate(CollaboratorsUpdatedEvent event) {
        if (event.getDocumentId().equals(proposalVersionSeriesId) &&
                (event.getOperation().equals(Operation.EDITED) || event.getOperation().equals(Operation.REMOVED) || 
                        (event.getOperation().equals(Operation.ADDED) && !event.getPresenterId().equals(id)))) {
            populateData();
        }
    }

    @Subscribe
    void updateProposalMetadata(DocumentUpdatedEvent event) {
        if (event.isModified()) {
            CollectionContext context = proposalContextProvider.get();
            context.useChildDocument(proposalId);
            context.useActionComment(messageHelper.getMessage("operation.metadata.updated"));
            context.executeUpdateProposalAsync();
        }
    }

    @Subscribe
    public void onInfoUpdate(UpdateUserInfoEvent updateUserInfoEvent) {
        if(isCurrentInfoId(updateUserInfoEvent.getActionInfo().getInfo().getDocumentId())) {
            if (!user.getLogin().equals(updateUserInfoEvent.getActionInfo().getInfo().getUserLoginName())) {
                eventBus.post(new NotificationEvent(leosUI, "coedition.caption", "coedition.operation." + updateUserInfoEvent.getActionInfo().getOperation().getValue(),
                        NotificationEvent.Type.TRAY, updateUserInfoEvent.getActionInfo().getInfo().getUserName()));
            }
            LOG.debug("Proposal Presenter updated the edit info -" + updateUserInfoEvent.getActionInfo().getOperation().name());
            collectionScreen.updateUserCoEditionInfo(coEditionHelper.getAllEditInfo(), user);
        }
    }
    
    @Subscribe
    public void fetchMilestone(FetchMilestoneEvent event) {
        LeosPackage leosPackage = packageService.findPackageByDocumentId(proposalId);
        LegDocument legDocument = packageService.findDocumentByPackagePathAndName(leosPackage.getPath(), event.getLegFileName(), LegDocument.class);
        collectionScreen.showMilestoneExplorer(legDocument, event.getMilestoneTitle());
    }
    
    private boolean isCurrentInfoId(String versionSeriesId) {
        return docVersionSeriesIds.contains(versionSeriesId);
    }

    @Subscribe
    public void afterClosedWindow(WindowClosedEvent<MilestoneExplorer> windowClosedEvent) {
        LOG.debug("Caught windowClosedEvent for MilestoneExplorer");
    }
    
    private byte[] getContent(LeosDocument doc) {
        final Content content = doc.getContent().getOrError(() -> "Document content is required!");
        return content.getSource().getBytes();
    }

}
