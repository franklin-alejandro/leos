package eu.europa.ec.leos.web.ui.window;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.oj.DocTypeSpain;
import eu.europa.ec.leos.ui.extension.ImportElementExtension;
import eu.europa.ec.leos.web.event.NotificationEvent;
import eu.europa.ec.leos.web.event.NotificationEvent.Type;
import eu.europa.ec.leos.web.event.view.document.DisableAllButtonEvent;
import eu.europa.ec.leos.web.event.view.document.ImportElementBOERequestEvent;
import eu.europa.ec.leos.web.event.view.document.ImportSelectionChangeEvent;
import eu.europa.ec.leos.web.event.view.document.SearchActBOERequestEvent;
import eu.europa.ec.leos.web.event.view.document.SearchAmendingActResponseEvent;
import eu.europa.ec.leos.web.event.view.document.SelectAllElementRequestEvent;
import eu.europa.ec.leos.web.event.view.document.SelectedElementRequestEvent;
import eu.europa.ec.leos.web.event.view.document.SelectedElementResponseEvent;
import eu.europa.ec.leos.web.model.SearchCriteriaBOEVO;
import eu.europa.ec.leos.web.model.SearchCriteriaBOEVO.Accion;
//AGE-CORE-3.0.0
public class ImportBOEWindow extends AbstractWindow {

    private static final long serialVersionUID = -7318940290911926353L;
    private EventBus eventBus;
    public static final String ARTICLE = "article";
    public static final String RECITAL = "recital";
    public static final String PROVISO = "proviso";
    
    private Label content;
    private Button importBtn;
    private Label importInfo;
    private Button selectAllArticles;
    private Button selectAllRecitals;
    private Button selectAllprovisos;
    private CheckBox checkboxIdentificador;
    private Binder<SearchCriteriaBOEVO> searchCriteriaBinder;
    SearchCriteriaBOEVO searchCriteriaBean;
    NativeSelect<DocTypeSpain> type;
    ComboBox<String> year;
    NativeSelect<String> month;
    NativeSelect<String> day;
    TextField number;
    TextField reference;

    public ImportBOEWindow(MessageHelper messageHelper, EventBus eventBus) {
        super(messageHelper, eventBus);
        this.eventBus = eventBus;
        setCaption(messageHelper.getMessage("document.importer.boe.window.title"));
        prepareWindow();
    }

    @Override
    public void attach() {
        super.attach();
        eventBus.register(this);
    }

    @Override
    public void detach() {
        super.detach();
        eventBus.unregister(this);
    }

    public void prepareWindow() {
       // setWidth("27.5cm");
    	setWidth("30.5cm");
        setHeight(95, Unit.PERCENTAGE);
        
        VerticalLayout windowLayout = new VerticalLayout();
        windowLayout.setSizeFull();
        windowLayout.setMargin(new MarginInfo(false, false, false, true));
        setBodyComponent(windowLayout);
        
        windowLayout.addComponent(buildSearchLayout());
        windowLayout.addComponent(buildReferenceLayout());
        
    	Button searchButton = new Button(messageHelper.getMessage("document.importer.button.search"));
    	searchButton.addStyleName("primary");
    	searchButton.setDisableOnClick(true);

    	searchButton.addClickListener(event -> {
    		if (searchCriteriaBinder.validate().isOk()) {
    			try {
    				searchCriteriaBean = new SearchCriteriaBOEVO(type.getValue(), year.getValue(), month.getValue(), day.getValue(), number.getValue(), reference.getValue(),Accion.IMPORT);
    		    	searchCriteriaBinder.readBean(searchCriteriaBean);
    				if (checkboxIdentificador.isEmpty() && number.isEmpty()) {
    					number.focus();
    					eventBus.post(new NotificationEvent(Type.INFO, "document.importer.project.format.error"));
    				} else if (checkboxIdentificador.isEmpty() && !searchCriteriaBean.getNumber().isEmpty()) {
    					String regex = "^(\\d+$|\\(d+\\))";
    					Pattern p = Pattern.compile(regex);
    					Matcher m = p.matcher(searchCriteriaBean.getNumber());
    					if (m.find()) {
    						searchCriteriaBinder.writeBean(searchCriteriaBean);
    						eventBus.post(new SearchActBOERequestEvent(searchCriteriaBean));
    						importBtn.setData(searchCriteriaBean);
    					} else {
    						reference.focus();
    						eventBus.post(new NotificationEvent(Type.INFO, "document.importer.project.format.error")); 
    					}
    				}else if (!checkboxIdentificador.isEmpty() && searchCriteriaBean.getReference().isEmpty()) {
    					reference.focus();
    					eventBus.post(new NotificationEvent(Type.INFO, "document.importer.project.format.error"));
    				} else if  (!checkboxIdentificador.isEmpty() && !searchCriteriaBean.getReference().isEmpty()) { 
    					String regex = "BOE-A-\\d{4}-\\d{1,5}";
    					Pattern p = Pattern.compile(regex);
    					Matcher m = p.matcher(searchCriteriaBean.getReference().toUpperCase());
    					if (m.find()) {
    						searchCriteriaBinder.writeBean(searchCriteriaBean);
        					eventBus.post(new SearchActBOERequestEvent(searchCriteriaBean));
        					importBtn.setData(searchCriteriaBean);
    					} else {
    						reference.focus();
    						eventBus.post(new NotificationEvent(Type.INFO, "document.importer.boe.window.reference.error")); 
    					}
    				}
    				event.getButton().setEnabled(true);
    				reset();	
    			} catch (ValidationException e) {
    				e.printStackTrace();
    			}
    		} else {
    			event.getButton().setEnabled(true);
    		}
    	});
    	windowLayout.addComponent(searchButton);
    	windowLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_CENTER);

        final Component textContent = buildContentLayout();
        windowLayout.addComponent(textContent);
        windowLayout.setExpandRatio(textContent, 1.0f);

        //add components to window layout
        selectAllRecitals = buildSelectAllButton(RECITAL);
        addComponentOnLeft(selectAllRecitals);
        selectAllRecitals.setCaption(messageHelper.getMessage("document.import.selectAll.recitals"));
        
        selectAllArticles = buildSelectAllButton(ARTICLE);
        addComponentOnLeft(selectAllArticles);
        selectAllArticles.setCaption(messageHelper.getMessage("document.import.selectAll.articles"));
        
        selectAllprovisos = buildSelectAllButton(PROVISO);
		addComponentOnLeft(selectAllprovisos);
		selectAllprovisos.setCaption(messageHelper.getMessage("document.import.selectAll.provisos"));
        
        buildImportInfoLabel();
        buildImportButton();
        
    }
    
    private Button buildSelectAllButton(String type) {
        Button selectAll = new Button();
        selectAll.setEnabled(false);
        selectAll.addStyleName("leos-import-selectAll");
        selectAll.setData(Boolean.FALSE);
        selectAll.addClickListener(event -> {
            if((boolean) selectAll.getData()) {
                selectAll.removeStyleName("leos-import-selectAll-active");
                selectAll.setData(Boolean.FALSE);
            } else {
                selectAll.addStyleName("leos-import-selectAll-active");
                selectAll.setData(Boolean.TRUE);
            }
            eventBus.post(new SelectAllElementRequestEvent((boolean) selectAll.getData(), type));
        });
        return selectAll;
    }

    private void buildImportInfoLabel() {
        importInfo = new Label();
        importInfo.setContentMode(ContentMode.HTML);
        importInfo.setValue("&nbsp;");
        addComponentAtPosition(importInfo, 1);
    }
    
    private void buildImportButton() {
        importBtn = new Button();
        importBtn.addStyleName("primary");
        importBtn.setCaption(messageHelper.getMessage("document.importer.button.import"));
        importBtn.setStyleName("primary");
        importBtn.setCaptionAsHtml(true);
        importBtn.setEnabled(false);
        addComponentAtPosition(importBtn, 2);
        
        importBtn.addClickListener(event -> {
            eventBus.post(new SelectedElementRequestEvent());
        });
    }
    
    private HorizontalLayout buildReferenceLayout () {
    	HorizontalLayout referenceLayout = new HorizontalLayout();
    	referenceLayout.setWidth("50%");
    	referenceLayout.setMargin(new MarginInfo(false, true, true, false));
    	checkboxIdentificador = new CheckBox(messageHelper.getMessage("document.importer.boe.window.checkbox"));
    	
    	checkboxIdentificador.setValue(false);

		checkboxIdentificador.addValueChangeListener(event -> // Java 8
		activateCheck(checkboxIdentificador.getValue())
    	);
    	
    	reference = new TextField();
    	reference.setPlaceholder("BOE-A-XXXX-XXXXX");
    	searchCriteriaBinder.forField(reference)
    	.bind(SearchCriteriaBOEVO::getReference, SearchCriteriaBOEVO::setReference);
    	reference.setEnabled(false);
    	
    	referenceLayout.addComponent(checkboxIdentificador);
    	referenceLayout.addComponent(reference);

    	return referenceLayout;
    }

    private HorizontalLayout buildSearchLayout() {
    	HorizontalLayout searchLayout = new HorizontalLayout();
    	searchLayout.setWidth("100%");
    	searchLayout.setMargin(new MarginInfo(false, true, true, false));

    	searchCriteriaBinder = new Binder<>();

    	type = new NativeSelect<>(messageHelper.getMessage("document.importer.type.caption"), Arrays.asList(DocTypeSpain.values()));
    	type.setEmptySelectionAllowed(false);
    	type.setRequiredIndicatorVisible(true);
    	type.setSelectedItem(DocTypeSpain.LEY); //default

    	searchCriteriaBinder.forField(type)
    	.asRequired(messageHelper.getMessage("document.importer.type.required.error"))
    	.bind(SearchCriteriaBOEVO::getType, SearchCriteriaBOEVO::setType);

    	List<String> yearsList = new ArrayList<>();
    	for (int years = Calendar.getInstance().get(Calendar.YEAR); years >= 1862; years--) {
    		yearsList.add(String.valueOf(years));
    	}
    	
    	year = new ComboBox<>(messageHelper.getMessage("document.importer.year.caption"), yearsList);
    	year.setEmptySelectionAllowed(false);
    	year.setRequiredIndicatorVisible(true);
    	year.setSelectedItem(yearsList.get(0)); //default
    
    	searchCriteriaBinder.forField(year)
    	.asRequired(messageHelper.getMessage("document.importer.year.required.error"))
    	.bind(SearchCriteriaBOEVO::getYear, SearchCriteriaBOEVO::setYear);

    	List<String> monthList = new ArrayList<>();
    	for (int months = 1; months <= 12; months++) {
    		monthList.add(String.valueOf(months));
    	}

    	month = new NativeSelect<>(messageHelper.getMessage("document.importer.month.caption"), monthList);
    	month.setEmptySelectionAllowed(false);
    	month.setRequiredIndicatorVisible(true);
    	month.setSelectedItem(monthList.get(0)); //default

    	searchCriteriaBinder.forField(month)
    	.asRequired(messageHelper.getMessage("document.importer.year.required.error"))
    	.bind(SearchCriteriaBOEVO::getMonth, SearchCriteriaBOEVO::setMonth);

    	List<String> dayList = new ArrayList<>();
    	for (int days = 1; days <= 31; days++) {
    		dayList.add(String.valueOf(days));
    	}

    	day = new NativeSelect<>(messageHelper.getMessage("document.importer.day.caption"), dayList);
    	day.setEmptySelectionAllowed(false);
    	day.setRequiredIndicatorVisible(true);
    	day.setSelectedItem(dayList.get(0)); //default

    	searchCriteriaBinder.forField(day)
    	.asRequired(messageHelper.getMessage("document.importer.year.required.error"))
    	.bind(SearchCriteriaBOEVO::getDay, SearchCriteriaBOEVO::setDay);

    	number = new TextField(messageHelper.getMessage("document.importer.number.caption"));
    	searchCriteriaBinder.forField(number)
    	.bind(SearchCriteriaBOEVO::getNumber, SearchCriteriaBOEVO::setNumber);

    	searchLayout.addStyleName("");
    	searchLayout.addComponent(type);
    	searchLayout.addComponent(year);
    	searchLayout.addComponent(month);
    	searchLayout.addComponent(day);
    	searchLayout.addComponent(number);

    	return searchLayout;
    }

    private Component buildContentLayout() {
        // create placeholder to display imported content
        content = new Label();
        content.setContentMode(ContentMode.HTML);
        content.setSizeFull();
        content.setStyleName("leos-import-content");
        
        new ImportElementExtension(eventBus, content);//content extension
        
        return content;
    }

    @Subscribe
    public void getSearchActResponse(SearchAmendingActResponseEvent event) {
        String document = event.getDocument();
        Boolean isEnabled = document == null ? false : true;
        content.setValue(document);
        content.addStyleName(LeosCategory.BILL.name().toLowerCase());
        content.addStyleName(LeosCategory.ANNEX.name().toLowerCase());
        selectAllArticles.setEnabled(isEnabled);
        selectAllRecitals.setEnabled(isEnabled);
        selectAllprovisos.setEnabled(isEnabled);
    }
    
    @Subscribe
    public void receiveSelectedElement(SelectedElementResponseEvent event) {
    	SearchCriteriaBOEVO searchBean = (SearchCriteriaBOEVO) importBtn.getData();
        eventBus.post(new ImportElementBOERequestEvent(searchBean, event.getElementIds()));
    }
    
    @Subscribe
    public void enableImportButton(ImportSelectionChangeEvent event) {
        int count = event.getCount();
        if(count > 0) {
            importBtn.setEnabled(true);
            importInfo.setValue(messageHelper.getMessage("document.import.selected.element", count));
        } else {
            importBtn.setEnabled(false);
            importInfo.setValue(null);
        }
    }
    
    @Subscribe
    public void disableAllButton(DisableAllButtonEvent event) {
        selectAllArticles.setEnabled(Boolean.FALSE);
        selectAllRecitals.setEnabled(Boolean.FALSE);
        selectAllprovisos.setEnabled(Boolean.FALSE);
        importBtn.setEnabled(Boolean.FALSE);
        importInfo.setValue(null);
    }
    
    private void reset() {
        importBtn.setEnabled(false);
        importInfo.setValue(null);
        selectAllArticles.removeStyleName("leos-import-selectAll-active");
        selectAllRecitals.removeStyleName("leos-import-selectAll-active");
        selectAllprovisos.removeStyleName("leos-import-selectAll-active");
        selectAllArticles.setData(Boolean.FALSE);
        selectAllRecitals.setData(Boolean.FALSE);
        selectAllprovisos.setData(Boolean.FALSE);
    }
    
    private void activateCheck (boolean marcado) {
    	if (marcado) {
    		type.setEnabled(false);
    		year.setEnabled(false);
    		month.setEnabled(false);
    		day.setEnabled(false);
    		number.setEnabled(false);
    		number.setValue("");
    		reference.setEnabled(true);
    		reference.focus();
    	} else {
    		type.setEnabled(true);
    		year.setEnabled(true);
    		month.setEnabled(true);
    		day.setEnabled(true);
    		number.setEnabled(true);
    		reference.setEnabled(false);
    		reference.setValue("");
    	}
    }
}
