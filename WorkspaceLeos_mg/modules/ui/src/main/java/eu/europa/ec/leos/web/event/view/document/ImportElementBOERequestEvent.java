package eu.europa.ec.leos.web.event.view.document;

import java.util.List;

import eu.europa.ec.leos.web.model.SearchCriteriaBOEVO;
//AGE-CORE-3.0.0
public class ImportElementBOERequestEvent {
	
	SearchCriteriaBOEVO searchCriteria;
	
	private List<String> elementIds;

    public ImportElementBOERequestEvent(SearchCriteriaBOEVO searchCriteria, List<String> elementIdList) {
        this.searchCriteria = searchCriteria;
        this.elementIds = elementIdList;
    }

    public SearchCriteriaBOEVO getSearchCriteria() {
        return searchCriteria;
    }

    public List<String> getElementIds() {
        return elementIds;
    }

}
