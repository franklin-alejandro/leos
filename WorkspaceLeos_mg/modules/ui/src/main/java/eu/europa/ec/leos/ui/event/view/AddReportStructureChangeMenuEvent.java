package eu.europa.ec.leos.ui.event.view;

import eu.europa.ec.leos.model.report.AGEReportStructureType;

public class AddReportStructureChangeMenuEvent {
	 private AGEReportStructureType structureType;

	    public AddReportStructureChangeMenuEvent(AGEReportStructureType structureType) {
	        super();
	        this.structureType = structureType;
	    }

	    public AGEReportStructureType getStructureType() {
	        return structureType;
	    }
	}
