package eu.europa.ec.leos.web.event.view.document;

import java.util.List;

import eu.europa.ec.leos.web.model.SearchCriteriaVO;
import eu.europa.ec.leos.web.model.SearchIdProjectVO;

public class ImportElementProjectRequestEvent {
	
	SearchIdProjectVO searchCriteria;
	
	private List<String> elementIds;

    public ImportElementProjectRequestEvent(SearchIdProjectVO searchCriteria, List<String> elementIdList) {
        this.searchCriteria = searchCriteria;
        this.elementIds = elementIdList;
    }

    public SearchIdProjectVO getSearchCriteria() {
        return searchCriteria;
    }

    public List<String> getElementIds() {
        return elementIds;
    }

}
