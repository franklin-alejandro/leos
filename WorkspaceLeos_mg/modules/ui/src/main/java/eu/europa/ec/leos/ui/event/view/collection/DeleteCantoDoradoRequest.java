package eu.europa.ec.leos.ui.event.view.collection;

import eu.europa.ec.leos.domain.vo.DocumentVO;

public class DeleteCantoDoradoRequest {
	
    private DocumentVO cantoDorado;

    public DeleteCantoDoradoRequest(DocumentVO cantoDorado) {
        this.cantoDorado = cantoDorado;
    }

    public DocumentVO getCantoDorado(){
        return cantoDorado;
    }

}
