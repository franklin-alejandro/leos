package eu.europa.ec.leos.ui.event;

public class ScrollToEvent {
    
    private String elementId;
    
    public ScrollToEvent(String elementId) {
        this.elementId = elementId;
    }
    
    public String getElementId() {
        return elementId;
    }
    
}