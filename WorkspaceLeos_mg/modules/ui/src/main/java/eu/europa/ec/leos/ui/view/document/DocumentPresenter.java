/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.ui.view.document;

import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.PERSON;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.ROLE;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.SIGNATURE;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.RULE;
import static eu.europa.ec.leos.services.support.xml.AGEXmlHelper.QUOTEDSTRUCTURE;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.CLAUSE;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.INDENT;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.PARAGRAPH;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.POINT;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.SUBPARAGRAPH;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.SUBPOINT;
//AGE-CORE-3.0.0
import eu.europa.ec.leos.services.support.xml.AGEExtRefUpdateOutput;
import eu.europa.ec.leos.services.support.IdGenerator;
//AGE-CORE-3.0.0

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Provider;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.vaadin.server.VaadinServletService;

import eu.europa.ec.leos.amend.AmendActDetails;
import eu.europa.ec.leos.domain.cmis.Content;
import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.cmis.LeosPackage;
import eu.europa.ec.leos.domain.cmis.document.Bill;
import eu.europa.ec.leos.domain.cmis.document.LegDocument;
import eu.europa.ec.leos.domain.cmis.document.XmlDocument;
import eu.europa.ec.leos.domain.cmis.metadata.BillMetadata;
import eu.europa.ec.leos.domain.common.Result;
import eu.europa.ec.leos.domain.common.TocMode;
import eu.europa.ec.leos.domain.vo.DocumentVO;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.action.ActionType;
import eu.europa.ec.leos.model.action.CheckinCommentVO;
import eu.europa.ec.leos.model.action.CheckinElement;
import eu.europa.ec.leos.model.action.VersionVO;
import eu.europa.ec.leos.model.event.DocumentUpdatedByCoEditorEvent;
import eu.europa.ec.leos.model.event.UpdateUserInfoEvent;
import eu.europa.ec.leos.model.messaging.UpdateInternalReferencesMessage;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.model.xml.Element;
import eu.europa.ec.leos.security.LeosPermission;
import eu.europa.ec.leos.security.SecurityContext;
import eu.europa.ec.leos.services.content.ReferenceLabelService;
import eu.europa.ec.leos.services.amendment.AGEAmendService;
import eu.europa.ec.leos.services.amendment.AGEAmendPatternLabelService;
import eu.europa.ec.leos.services.content.TemplateConfigurationService;
import eu.europa.ec.leos.services.content.processor.DocumentContentService;
import eu.europa.ec.leos.services.content.processor.ElementProcessor;
import eu.europa.ec.leos.services.document.BillService;
import eu.europa.ec.leos.services.document.util.CheckinCommentUtil;
import eu.europa.ec.leos.services.export.ExportOptions;
import eu.europa.ec.leos.services.export.ExportService;
import eu.europa.ec.leos.services.messaging.UpdateInternalReferencesProducer;
import eu.europa.ec.leos.services.store.LegService;
import eu.europa.ec.leos.services.store.PackageService;
import eu.europa.ec.leos.services.store.WorkspaceService;
import eu.europa.ec.leos.services.toc.StructureContext;
import eu.europa.ec.leos.ui.component.ComparisonComponent;
import eu.europa.ec.leos.ui.event.CloseBrowserRequestEvent;
import eu.europa.ec.leos.ui.event.CloseScreenRequestEvent;
import eu.europa.ec.leos.ui.event.EnableSyncScrollRequestEvent;
import eu.europa.ec.leos.ui.event.FetchMilestoneByVersionedReferenceEvent;
import eu.europa.ec.leos.ui.event.MergeElementRequestEvent;
import eu.europa.ec.leos.ui.event.ScrollToEvent;
import eu.europa.ec.leos.ui.event.amendment.AmendmentAddRequest;
import eu.europa.ec.leos.ui.event.amendment.AmendmentElementModificationRequest;
import eu.europa.ec.leos.ui.event.amendment.AmendmentElementsPreviewRequest;
import eu.europa.ec.leos.ui.event.amendment.AmendmentModeCloseEvent;
import eu.europa.ec.leos.ui.event.amendment.AmendmentModeExtensionEvent;
import eu.europa.ec.leos.ui.event.amendment.AmendmentViewRequestEvent;
import eu.europa.ec.leos.ui.event.doubleCompare.DocuWriteExportRequestEvent;
import eu.europa.ec.leos.ui.event.doubleCompare.DoubleCompareRequestEvent;
import eu.europa.ec.leos.ui.event.metadata.DocumentMetadataRequest;
import eu.europa.ec.leos.ui.event.metadata.DocumentMetadataResponse;
import eu.europa.ec.leos.ui.event.metadata.SearchMetadataRequest;
import eu.europa.ec.leos.services.content.processor.AGEBillProcessor;
import eu.europa.ec.leos.ui.event.toc.CloseEditTocEvent;
import eu.europa.ec.leos.ui.event.toc.InlineTocCloseRequestEvent;
import eu.europa.ec.leos.ui.event.toc.InlineTocEditRequestEvent;
import eu.europa.ec.leos.ui.event.toc.RefreshTocEvent;
import eu.europa.ec.leos.ui.event.toc.SaveTocRequestEvent;
import eu.europa.ec.leos.ui.model.AnnotateMetadata;
import eu.europa.ec.leos.ui.support.CoEditionHelper;
import eu.europa.ec.leos.ui.view.AbstractLeosPresenter;
import eu.europa.ec.leos.ui.view.CommonDelegate;
import eu.europa.ec.leos.ui.view.ComparisonDelegate;
import eu.europa.ec.leos.ui.view.ComparisonDisplayMode;
import eu.europa.ec.leos.ui.window.milestone.MilestoneExplorer;
import eu.europa.ec.leos.usecases.document.BillContext;
import eu.europa.ec.leos.usecases.document.CollectionContext;
import eu.europa.ec.leos.vo.coedition.InfoType;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;
import eu.europa.ec.leos.web.event.NavigationRequestEvent;
import eu.europa.ec.leos.web.event.NotificationEvent;
import eu.europa.ec.leos.web.event.NotificationEvent.Type;
import eu.europa.ec.leos.web.event.component.CleanComparedContentEvent;
import eu.europa.ec.leos.web.event.component.CompareRequestEvent;
import eu.europa.ec.leos.web.event.component.CompareTimeLineRequestEvent;
import eu.europa.ec.leos.web.event.component.LayoutChangeRequestEvent;
import eu.europa.ec.leos.web.event.component.RestoreVersionRequestEvent;
import eu.europa.ec.leos.web.event.component.ShowVersionRequestEvent;
import eu.europa.ec.leos.web.event.component.VersionListRequestEvent;
import eu.europa.ec.leos.web.event.component.VersionListResponseEvent;
import eu.europa.ec.leos.web.event.component.WindowClosedEvent;
import eu.europa.ec.leos.web.event.view.document.CheckElementCoEditionEvent;
import eu.europa.ec.leos.web.event.view.document.CheckElementCoEditionEvent.Action;
import eu.europa.ec.leos.web.event.view.document.CloseDocumentEvent;
import eu.europa.ec.leos.web.event.view.document.CloseElementEvent;
import eu.europa.ec.leos.web.event.view.document.ComparisonEvent;
import eu.europa.ec.leos.web.event.view.document.DeleteElementModifiRequestEvent;
import eu.europa.ec.leos.web.event.view.document.DeleteElementRequestEvent;
import eu.europa.ec.leos.web.event.view.document.DisableAllButtonEvent;
import eu.europa.ec.leos.web.event.view.document.DocumentUpdatedEvent;
import eu.europa.ec.leos.web.event.view.document.EditElementRequestEvent;
import eu.europa.ec.leos.web.event.view.document.FetchCrossRefTocRequestEvent;
import eu.europa.ec.leos.web.event.view.document.FetchElementRequestEvent;
import eu.europa.ec.leos.web.event.view.document.FetchUserGuidanceRequest;
import eu.europa.ec.leos.web.event.view.document.FetchUserPermissionsRequest;
import eu.europa.ec.leos.web.event.view.document.ImportElementBOERequestEvent;
import eu.europa.ec.leos.web.event.view.document.ImportElementProjectRequestEvent;
import eu.europa.ec.leos.web.event.view.document.ImportElementRequestEvent;
import eu.europa.ec.leos.web.event.view.document.InsertElementRequestEvent;
import eu.europa.ec.leos.web.event.view.document.MergeSuggestionRequest;
import eu.europa.ec.leos.web.event.view.document.ReferenceLabelRequestEvent;
import eu.europa.ec.leos.web.event.view.document.RefreshDocumentEvent;
import eu.europa.ec.leos.web.event.view.document.SaveElementRequestEvent;
import eu.europa.ec.leos.web.event.view.document.SaveIntermediateVersionEvent;
import eu.europa.ec.leos.web.event.view.document.SearchActBOERequestEvent;
import eu.europa.ec.leos.web.event.view.document.SearchActImportIdRequestEvent;
import eu.europa.ec.leos.web.event.view.document.SearchActImportRequestEvent;
import eu.europa.ec.leos.web.event.view.document.SearchActRequestEvent;
import eu.europa.ec.leos.web.event.view.document.SearchAndReplaceTextEvent;
import eu.europa.ec.leos.web.event.view.document.ShowImportBOEWindowEvent;
import eu.europa.ec.leos.web.event.view.document.ShowImportProjectWindowEvent;
import eu.europa.ec.leos.web.event.view.document.ShowImportWindowEvent;
import eu.europa.ec.leos.web.event.view.document.ShowIntermediateVersionWindowEvent;
import eu.europa.ec.leos.web.event.window.CloseElementEditorEvent;
import eu.europa.ec.leos.web.event.window.ShowTimeLineWindowEvent;
import eu.europa.ec.leos.web.model.SearchCriteriaBOEVO;
import eu.europa.ec.leos.web.model.SearchCriteriaBOEVO.Accion;
import eu.europa.ec.leos.web.model.SearchCriteriaProjectVO;
import eu.europa.ec.leos.web.model.SearchCriteriaVO;
import eu.europa.ec.leos.web.model.SearchIdProjectVO;
import eu.europa.ec.leos.web.model.VersionInfoVO;
import eu.europa.ec.leos.web.support.SessionAttribute;
import eu.europa.ec.leos.web.support.UrlBuilder;
import eu.europa.ec.leos.web.support.UuidHelper;
import eu.europa.ec.leos.web.support.user.UserHelper;
import eu.europa.ec.leos.web.support.xml.DownloadStreamResource;
import eu.europa.ec.leos.web.ui.navigation.Target;
import eu.europa.ec.leos.web.ui.screen.document.ColumnPosition;
import eu.europa.ec.leos.services.content.processor.AGETransformationService;
import eu.europa.ec.leos.services.importoj.AGEImportService;

@Component
@Scope("prototype")
//AGE-CORE-3.0.0
class DocumentPresenter extends AbstractLeosPresenter {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentPresenter.class);

    private final DocumentScreen documentScreen;
    private final BillService billService;
    private final AGEBillProcessor billProcessor;
    private final ElementProcessor<Bill> elementProcessor;
    private final ReferenceLabelService referenceLabelService;
    private final AGEAmendPatternLabelService amendPatternLabelService;
    private final AGETransformationService transformationService;//CUARENTENA  AGE-CORE-3.0.0
    private final UrlBuilder urlBuilder;
    private final ComparisonDelegate<Bill> comparisonDelegate;
    private final DocumentContentService documentContentService;
    private final PackageService packageService;
    private final ExportService exportService;
    private final CommonDelegate<Bill> commonDelegate;
    private final TemplateConfigurationService templateConfigurationService;
    private final AGEImportService importService;
    private final UserHelper userHelper;
    private final MessageHelper messageHelper;
    private final Provider<CollectionContext> proposalContextProvider;
    private final Provider<BillContext> billContextProvider;
    private final CoEditionHelper coEditionHelper;
    private final Provider<StructureContext> structureContextProvider;
    private final LegService legService;
    private final UpdateInternalReferencesProducer updateInternalReferencesProducer;
    private final AGEAmendService amendService;

    private String strDocumentVersionSeriesId;
    private String documentId;
    private String documentRef;
    private Element elementToEditAfterClose;
    private boolean comparisonMode;
    

    private final static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    @Autowired
    DocumentPresenter(SecurityContext securityContext, HttpSession httpSession, EventBus eventBus,
                      DocumentScreen documentScreen, BillService billService,
                      CommonDelegate<Bill> commonDelegate,
                      ComparisonDelegate<Bill> comparisonDelegate,
                      DocumentContentService documentContentService, PackageService packageService, ExportService exportService,
                      AGEBillProcessor billProcessor, ReferenceLabelService referenceLabelService,
                      AGEAmendPatternLabelService amendPatternLabelService,
                      ElementProcessor<Bill> elementProcessor,AGETransformationService transformationService,
                      //TocRulesService tocRulesService,
                      UrlBuilder urlBuilder, TemplateConfigurationService templateConfigurationService,
                      AGEImportService importService, UserHelper userHelper, MessageHelper messageHelper,
                      Provider<CollectionContext> proposalContextProvider, Provider<BillContext> billContextProvider, CoEditionHelper coEditionHelper,
                      EventBus leosApplicationEventBus, UuidHelper uuidHelper, Provider<StructureContext> structureContextProvider,
                      WorkspaceService workspaceService, LegService legService, UpdateInternalReferencesProducer updateInternalReferencesProducer, AGEAmendService amendService) {

        super(securityContext, httpSession, eventBus, leosApplicationEventBus, uuidHelper, packageService, workspaceService);
        LOG.trace("Initializing document presenter...");
        this.documentScreen = documentScreen;
        this.billService = billService;
        this.comparisonDelegate = comparisonDelegate;
        this.documentContentService = documentContentService;
        this.packageService = packageService;
        this.exportService = exportService;
        this.commonDelegate = commonDelegate;
        this.billProcessor = billProcessor;
        this.elementProcessor = elementProcessor;
        this.referenceLabelService = referenceLabelService;
        this.amendPatternLabelService = amendPatternLabelService;
        this.transformationService = transformationService;
        this.urlBuilder = urlBuilder;
        this.templateConfigurationService = templateConfigurationService;
        this.importService = importService;
        this.userHelper = userHelper;
        this.messageHelper = messageHelper;
        this.proposalContextProvider = proposalContextProvider;
        this.billContextProvider = billContextProvider;
        this.coEditionHelper = coEditionHelper;
        this.workspaceService = workspaceService;
        this.structureContextProvider = structureContextProvider;
        this.legService = legService;
        this.updateInternalReferencesProducer = updateInternalReferencesProducer;
        this.amendService = amendService;
    }

    private byte[] getContent(Bill bill) {
        final Content content = bill.getContent().getOrError(() -> "Document content is required!");
        return content.getSource().getBytes();
    }

    @Override
    public void enter() {
        super.enter();
        init();
    }

    private void init() {
        try {
            populateViewWithDocumentDetails(TocMode.SIMPLIFIED);
            final List<VersionVO> allVersions = billService.getAllVersions(documentId, documentRef);
            documentScreen.setDataFunctions(
                    allVersions,
                    this::majorVersionsFn, this::countMajorVersionsFn,
                    this::minorVersionsFn, this::countMinorVersionsFn,
                    this::recentChangesFn, this::countRecentChangesFn);
        } catch (Exception exception) {
            LOG.error("Exception occurred in init(): ", exception);
            eventBus.post(new NotificationEvent(Type.INFO, "unknown.error.message"));
        }
    }
    
    @Subscribe
    public void updateVersionsTab(DocumentUpdatedEvent event) {
        final List<VersionVO> allVersions = billService.getAllVersions(documentId, documentRef);
        documentScreen.refreshVersions(allVersions, comparisonMode);
    }
    
    private Integer countMinorVersionsFn(String currIntVersion) {
        return billService.findAllMinorsCountForIntermediate(documentRef, currIntVersion);
    }
    
    private List<Bill> minorVersionsFn(String currIntVersion, int startIndex, int maxResults) {
        return billService.findAllMinorsForIntermediate(documentRef, currIntVersion, startIndex, maxResults);
    }
    
    private Integer countMajorVersionsFn() {
        return billService.findAllMajorsCount(documentRef);
    }
    
    private List<Bill> majorVersionsFn(int startIndex, int maxResults) {
        return billService.findAllMajors(documentRef, startIndex, maxResults);
    }
    
    private Integer countRecentChangesFn() {
        return billService.findRecentMinorVersionsCount(documentId, documentRef);
    }
    
    private List<Bill> recentChangesFn(int startIndex, int maxResults) {
        return billService.findRecentMinorVersions(documentId, documentRef, startIndex, maxResults);
    }
    
    @Override
    public void detach() {
        super.detach();
        coEditionHelper.removeUserEditInfo(id, strDocumentVersionSeriesId, null, InfoType.DOCUMENT_INFO);
    }

    @Subscribe
    void closeDocument(CloseDocumentEvent event) {
        coEditionHelper.removeUserEditInfo(id, strDocumentVersionSeriesId, null, InfoType.DOCUMENT_INFO);
        eventBus.post(new NavigationRequestEvent(Target.PREVIOUS));
    }

    @Subscribe
    void closeDocument(CloseBrowserRequestEvent event) {
        coEditionHelper.removeUserEditInfo(id, strDocumentVersionSeriesId, null, InfoType.DOCUMENT_INFO);
    }

    @Subscribe
    void closeScreen(CloseScreenRequestEvent event) {
        if (documentScreen.isTocEnabled()) {
            eventBus.post(new CloseEditTocEvent());
        } else {
            eventBus.post(new CloseDocumentEvent());
        }
    }

    @Subscribe
    void refreshDocument(RefreshDocumentEvent event) {
        populateViewWithDocumentDetails(event.getTocMode());
    }
    
    @Subscribe
    void refreshToc(RefreshTocEvent event) {
        Bill bill = getDocument();
        if (bill != null) {
            documentScreen.setToc(getListOfTableOfContent(bill, event.getTocMode()));
        }
    }

    @Subscribe
    void checkElementCoEdition(CheckElementCoEditionEvent event) {
        if (event.getAction().equals(Action.MERGE)) {
            Bill bill = getDocument();
            Element mergeOnElement = billProcessor.getMergeOnElement(bill, event.getElementContent(), event.getElementTagName(), event.getElementId());
            if (mergeOnElement != null) {
                documentScreen.checkElementCoEdition(coEditionHelper.getCurrentEditInfo(strDocumentVersionSeriesId), user,
                        mergeOnElement.getElementId(), mergeOnElement.getElementTagName(), event.getAction(), event.getActionEvent());
            } else {
                documentScreen.showAlertDialog("operation.element.not.performed");
            }
        } else {
            documentScreen.checkElementCoEdition(coEditionHelper.getCurrentEditInfo(strDocumentVersionSeriesId), user,
                    event.getElementId(), event.getElementTagName(), event.getAction(), event.getActionEvent());
        }
    }

    @Subscribe
    void editElement(EditElementRequestEvent event) {
        String elementId = event.getElementId();
        String elementTagName = event.getElementTagName();
        elementToEditAfterClose = null;

        Bill bill = getDocument();
        String jsonAlternatives = "";

        if(CLAUSE.equals(elementTagName)){
            jsonAlternatives = templateConfigurationService.getTemplateConfiguration(bill.getMetadata().get().getDocTemplate(),"alternatives");
        }
        String element = elementProcessor.getElement(bill, elementTagName, elementId);
        coEditionHelper.storeUserEditInfo(httpSession.getId(), id, user, strDocumentVersionSeriesId, elementId, InfoType.ELEMENT_INFO);
        documentScreen.showElementEditor(event.getElementId(), elementTagName, element, jsonAlternatives);
    }

    @Subscribe
    void saveElement(SaveElementRequestEvent event) {
        final String elementId = event.getElementId();
        final String elementTagName = event.getElementTagName();
        elementToEditAfterClose = null;
        //AGE-CORE-3.0.0
        final List<List<String>> refs = event.getReferences();

        Bill bill = getDocument();       
        byte[] newXmlContent = elementProcessor.updateElement(bill, billProcessor.addRef2linkReferences(event.getElementContent(),refs), elementTagName, elementId);
        if (newXmlContent == null) {
            documentScreen.showAlertDialog("operation.element.not.performed");
            return;
        }
        
        AGEExtRefUpdateOutput updateOutput = billProcessor.updatedExternalMetaRefs(newXmlContent);
        newXmlContent = updateOutput.getDoc();
        List<String> updateComments = updateOutput.getUpdates();
        if (!updateComments.isEmpty()) {
            for (int i = 0; i < updateComments.size(); i++) {
            	eventBus.post(new NotificationEvent("Publicación BOE:",updateComments.get(i).replace(":", "<br/>").replace("-", "<br/>"),Type.AGETRAY));	
            }        	
        }
        //AGE-CORE-3.0.0

        final String title = messageHelper.getMessage("operation.element.updated", StringUtils.capitalize(elementTagName));
        final String description = messageHelper.getMessage("operation.checkin.minor");
        final String elementLabel = generateLabel(elementId, bill);
        final CheckinCommentVO checkinComment = new CheckinCommentVO(title, description, new CheckinElement(ActionType.UPDATED, elementId, elementTagName, elementLabel));
        final String checkinCommentJson = CheckinCommentUtil.getJsonObject(checkinComment);
        
        bill = billService.updateBill(bill, newXmlContent, checkinCommentJson);
        
        if (bill != null) {
            if (!event.isSaveAndClose() && checkIfCloseElementEditor(elementTagName, event.getElementContent())) {
                elementToEditAfterClose = billProcessor.getSplittedElement(bill, elementTagName, elementId);
                eventBus.post(new CloseElementEvent());
            } else {
                String elementContent = elementProcessor.getElement(bill, elementTagName, elementId);
                documentScreen.refreshElementEditor(elementId, elementTagName, elementContent);
            }
            //AGE-CORE-3.0.0
            eventBus.post(new NotificationEvent(Type.WARNING, "document.content.updated"));
            //AGE-CORE-3.0.0
            eventBus.post(new DocumentUpdatedEvent());
            documentScreen.scrollToMarkedChange(elementId);
            leosApplicationEventBus.post(new DocumentUpdatedByCoEditorEvent(user, strDocumentVersionSeriesId, id));
        }
    }

    boolean checkIfCloseElementEditor(String elementTagName, String elementContent) {
        switch (elementTagName) {
            case SUBPARAGRAPH:
            case SUBPOINT:
            case RULE:
                return elementContent.contains("<" + elementTagName + ">");                
            case PARAGRAPH:
                return elementContent.contains("<subparagraph>");
            case POINT:
                return elementContent.contains("<alinea>");
            case INDENT:
                return elementContent.contains("<alinea>");
            default:
                return false;
        }
    }

    @Subscribe
    void closeElementEditor(CloseElementEditorEvent event) {
        String elementId = event.getElementId();
        coEditionHelper.removeUserEditInfo(id, strDocumentVersionSeriesId, elementId, InfoType.ELEMENT_INFO);
        LOG.debug("User edit information removed");
        eventBus.post(new RefreshDocumentEvent());
        if (elementToEditAfterClose != null) {
            documentScreen.scrollTo(elementToEditAfterClose.getElementId());
            eventBus.post(new EditElementRequestEvent(elementToEditAfterClose.getElementId(), elementToEditAfterClose.getElementTagName()));
        }
    }

    @Subscribe
    void deleteElement(DeleteElementRequestEvent event) {
        String tagName = event.getElementTagName();//AGE-CORE-3.0.0
        //Evolutivo #2546
        tagName = (tagName.equalsIgnoreCase(ROLE) || tagName.equalsIgnoreCase(PERSON))? SIGNATURE : tagName;
        final Bill bill = getDocument();
        final byte[] newXmlContent = billProcessor.deleteElement(bill, event.getElementId(), tagName, user);

        final String updatedLabel = generateLabel(event.getElementId(), bill);
        final String comment =  messageHelper.getMessage("operation.element.deleted", updatedLabel);
        
        updateBillContent(bill, newXmlContent, comment, "document." + tagName + ".deleted");
        updateInternalReferencesProducer.send(new UpdateInternalReferencesMessage(bill.getId(), bill.getMetadata().get().getRef(), id));
    }
    
    @Subscribe
    void deleteElementModifi(DeleteElementModifiRequestEvent event) {
    	String quotedId = event.getQuotedId();
    	String elementId = event.getElementId();
    	String modId = event.getModId();
    	String granParentId = event.getGranParentId();
    	String granParentType = event.getGranParentType();
        final Bill bill = getDocument();
        final byte[] newXmlContent = amendPatternLabelService.deleteElementModifi(bill, elementId, quotedId, modId, granParentId, granParentType);
        
        
        final String title = messageHelper.getMessage("operation.element.updated", StringUtils.capitalize(QUOTEDSTRUCTURE));
        final String description = messageHelper.getMessage("document.amendment.created.checkin");
        final String elementLabel = generateLabel(quotedId, bill);
        final CheckinCommentVO checkinComment = new CheckinCommentVO(title, description, new CheckinElement(ActionType.DELETED, quotedId, QUOTEDSTRUCTURE, elementLabel));
        final String checkinCommentJson = CheckinCommentUtil.getJsonObject(checkinComment);
        billService.updateBill(bill, newXmlContent,checkinCommentJson);
        
        eventBus.post(new DocumentUpdatedEvent());
        eventBus.post(new RefreshDocumentEvent());
        leosApplicationEventBus.post(new DocumentUpdatedByCoEditorEvent(user, strDocumentVersionSeriesId, id));
        eventBus.post(new NotificationEvent(Type.INFO, "document.content.updated"));
        documentScreen.scrollTo(quotedId);
    }

    public String generateLabel(String reference, XmlDocument sourceDocument) {
        final byte[] sourceXmlContent = sourceDocument.getContent().get().getSource().getBytes();
        Result<String> updatedLabel = referenceLabelService.generateLabelStringRef(Arrays.asList(reference),  sourceDocument.getMetadata().get().getRef(), sourceXmlContent);
        return updatedLabel.get();
    }

    @Subscribe
    void insertElement(InsertElementRequestEvent event) {
        String tagName = event.getElementTagName();
        String numberType = event.getNumberType();
        //Evolutivo #2546
        tagName = (tagName.equalsIgnoreCase(ROLE) || tagName.equalsIgnoreCase(PERSON))? SIGNATURE : tagName;
        final Bill bill = getDocument();
        final boolean before = InsertElementRequestEvent.POSITION.BEFORE.equals(event.getPosition());
        
        final byte[] newXmlContent = billProcessor.insertNewElement(bill, event.getElementId(), before, tagName, numberType);
        
        final String title = messageHelper.getMessage("operation.element.inserted", StringUtils.capitalize(tagName));
        final String description = messageHelper.getMessage("operation.checkin.minor");
        final String elementLabel = "";
        final CheckinCommentVO checkinComment = new CheckinCommentVO(title, description, new CheckinElement(ActionType.INSERTED, event.getElementId(), tagName, elementLabel));
        final String checkinCommentJson = CheckinCommentUtil.getJsonObject(checkinComment);
        
        updateBillContent(bill, newXmlContent, checkinCommentJson,"document." + tagName + ".inserted");
        updateInternalReferencesProducer.send(new UpdateInternalReferencesMessage(bill.getId(), bill.getMetadata().get().getRef(), id));
    }

    @Subscribe
    void mergeElement(MergeElementRequestEvent event) {
        String elementId = event.getElementId();
        String tagName = event.getElementTagName();
        String elementContent = event.getElementContent();

        Bill bill = getDocument();
        Element mergeOnElement = billProcessor.getMergeOnElement(bill, elementContent, tagName, elementId);
        if (mergeOnElement != null) {
            byte[] newXmlContent = billProcessor.mergeElement(bill, elementContent, tagName, elementId);
            bill = billService.updateBill(bill, newXmlContent, messageHelper.getMessage("operation.element.updated", StringUtils.capitalize(tagName)));
            if (bill != null) {
                elementToEditAfterClose = mergeOnElement;
                eventBus.post(new CloseElementEvent());
                eventBus.post(new NotificationEvent(Type.INFO, "document.content.updated"));
                eventBus.post(new DocumentUpdatedEvent());
                leosApplicationEventBus.post(new DocumentUpdatedByCoEditorEvent(user, strDocumentVersionSeriesId, id));
            }
        } else {
            documentScreen.showAlertDialog("operation.element.not.performed");
        }
    }

    @Subscribe
    void editInlineToc(InlineTocEditRequestEvent event) {
        Bill bill = getDocument();
        coEditionHelper.storeUserEditInfo(httpSession.getId(), id, user, strDocumentVersionSeriesId, null, InfoType.TOC_INFO);
        documentScreen.enableTocEdition(getListOfTableOfContent(bill, TocMode.NOT_SIMPLIFIED));
    }

    @Subscribe
    void closeInlineToc(InlineTocCloseRequestEvent event) {
        coEditionHelper.removeUserEditInfo(id, strDocumentVersionSeriesId, null, InfoType.TOC_INFO);
        LOG.debug("User edit information removed");
    }
    
    @Subscribe
    void saveToc(SaveTocRequestEvent event) {
        Bill bill = getDocument();
        byte [] newXmlContent = null;
        List<String> idsMod;
        final String title = messageHelper.getMessage("operation.toc.updated");
        final String description = messageHelper.getMessage("operation.checkin.minor");
        Set<CheckinElement> updatedElements = event.getSaveElements();
        updatedElements.forEach(item -> {
            final String label = generateLabel(item.getElementId(),bill);
            item.setElementLabel(label);
        });
        idsMod = amendPatternLabelService.extracIdsMod(updatedElements, bill);

        final CheckinCommentVO checkinComment = new CheckinCommentVO(title, description, new CheckinElement(ActionType.STRUCTURAL, updatedElements));
        final String checkinCommentJson = CheckinCommentUtil.getJsonObject(checkinComment);
        
        Bill billAux = billService.saveTableOfContent(bill, event.getTableOfContentItemVOs(), checkinCommentJson, user);
        newXmlContent = amendPatternLabelService.checkModifAndDelete(idsMod, billAux);
        updateBillContent(bill, newXmlContent, checkinCommentJson);
        
        eventBus.post(new NotificationEvent(Type.INFO, "toc.edit.saved"));
        eventBus.post(new DocumentUpdatedEvent());
        leosApplicationEventBus.post(new DocumentUpdatedByCoEditorEvent(user, strDocumentVersionSeriesId, id));
        updateInternalReferencesProducer.send(new UpdateInternalReferencesMessage(bill.getId(), bill.getMetadata().get().getRef(), id));
    }
    

    @Subscribe
    void fetchTocAndAncestors(FetchCrossRefTocRequestEvent event) {
        Bill bill = getDocument();
        List<String> elementAncestorsIds = null;
        if (event.getElementIds() != null && event.getElementIds().size() > 0) {
            try {
                elementAncestorsIds = billService.getAncestorsIdsForElementId(bill, event.getElementIds());
            } catch (Exception e) {
                LOG.warn("Could not get ancestors Ids", e);
            }
        }
        // we are combining two operations (get toc + get selected element ancestors)
        documentScreen.setTocAndAncestors(packageService.getTableOfContent(bill.getId(), TocMode.SIMPLIFIED_CLEAN), elementAncestorsIds);
    }

    @Subscribe
    void fetchElement(FetchElementRequestEvent event) {
        XmlDocument document = workspaceService.findDocumentByRef(event.getDocumentRef(), XmlDocument.class);
        String contentForType = elementProcessor.getElement(document, event.getElementTagName(), event.getElementId());
        String wrappedContentXml = wrapXmlFragment(contentForType != null ? contentForType : "");
        InputStream contentStream = new ByteArrayInputStream(wrappedContentXml.getBytes(StandardCharsets.UTF_8));
        contentForType = transformationService.toXmlFragmentWrapper(contentStream, urlBuilder.getWebAppPath(VaadinServletService.getCurrentServletRequest()),
                securityContext.getPermissions(document));

        documentScreen.setElement(event.getElementId(), event.getElementTagName(), contentForType, event.getDocumentRef());
    }

    @Subscribe
    void fetchReferenceLabel(ReferenceLabelRequestEvent event) {
        // Validate
        if (event.getReferences().size() < 1) {
            eventBus.post(new NotificationEvent(Type.ERROR, "unknown.error.message"));
            LOG.error("No reference found in the request from client");
            return;
        }
        final byte[] sourceXmlContent = getDocument().getContent().get().getSource().getBytes();
        Result<String> updatedLabel = referenceLabelService.generateLabelStringRef(event.getReferences(), getDocumentRef(), event.getCurrentElementID(), sourceXmlContent, event.getDocumentRef(), true);
        documentScreen.setReferenceLabel(updatedLabel.get(), event.getDocumentRef());
    }

    @Subscribe
    void mergeSuggestion(MergeSuggestionRequest event) {
        Bill bill = getDocument();
        commonDelegate.mergeSuggestion(bill, event, elementProcessor, billService::updateBill);
    }

    @Subscribe
    public void fetchMetadata(DocumentMetadataRequest event) {
        AnnotateMetadata metadata = new AnnotateMetadata();
        Bill bill = getDocument();
        metadata.setVersion(bill.getVersionLabel());
        metadata.setId(bill.getId());
        metadata.setTitle(bill.getTitle());
        eventBus.post(new DocumentMetadataResponse(metadata));
    }

    @Subscribe
    public void fetchSearchMetadata(SearchMetadataRequest event) {
        // TODO solve issue when calling MilestoneExplorer within DocumentPresenter. Commenting this for now:
        // eventBus.post(new SearchMetadataResponse(Collections.emptyList()));
    }

    private String getEditableXml(Bill document) {
        return documentContentService.toEditableContent(document,
                urlBuilder.getWebAppPath(VaadinServletService.getCurrentServletRequest()), securityContext);
    }

    private String getImportXml(String content) {
        return transformationService.toImportXml(
                new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8)),
                urlBuilder.getWebAppPath(VaadinServletService.getCurrentServletRequest()), securityContext.getPermissions(content));
    }
    
    private String getImportProjectXml(String content) {
    	return transformationService.toImportProjectXml(
    			new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8)),
    			urlBuilder.getWebAppPath(VaadinServletService.getCurrentServletRequest()), securityContext.getPermissions(content));
    }

    private Bill getDocument() {
        documentRef = getDocumentRef();
        Bill bill = null;
        try {
            if (documentRef != null) {
                bill = billService.findBillByRef(documentRef);
                strDocumentVersionSeriesId = bill.getVersionSeriesId();
                documentId = bill.getId();
                structureContextProvider.get().useDocumentTemplate(bill.getMetadata().getOrError(() -> "Bill metadata is required!").getDocTemplate());
            }
        } catch (IllegalArgumentException iae) {
            LOG.debug("Rejecting view. Document " + documentRef + "cannot be retrieved due to exception", iae);
            // rejectView(RepositoryView.VIEW_ID, "document.not.found", documentId); // FIXME uncomment or replace
        }

        return bill;
    }
    

    private String getDocumentRef() {
        return (String) httpSession.getAttribute(id + "." + SessionAttribute.BILL_REF.name());
    }
    
    private List<TableOfContentItemVO> getListOfTableOfContent(Bill bill, TocMode mode) {
        return billService.getTableOfContent(bill, mode);
    }

    private void populateViewWithDocumentDetails(TocMode mode) {
        Bill bill = getDocument();
        if (bill != null) {
            documentScreen.setDocumentTitle(bill.getTitle());
            documentScreen.setDocumentVersionInfo(getVersionInfo(bill));
            documentScreen.refreshContent(getEditableXml(bill));
            documentScreen.setToc(getListOfTableOfContent(bill, mode));
            DocumentVO billVO = createLegalTextVO(bill);
            documentScreen.updateUserCoEditionInfo(coEditionHelper.getCurrentEditInfo(bill.getVersionSeriesId()), id);
            documentScreen.setPermissions(billVO);
        }
    }
    
    @Subscribe
    void showTimeLineWindow(ShowTimeLineWindowEvent event) {
        List<Bill> documentVersions = billService.findVersions(documentId);
        documentScreen.showTimeLineWindow(documentVersions);
    }

    @Subscribe
    void showIntermediateVersionWindow(ShowIntermediateVersionWindowEvent event) {
        documentScreen.showIntermediateVersionWindow();
    }

    @Subscribe
    void showImportWindow(ShowImportWindowEvent event) {
        documentScreen.showImportWindow();
    }
    
    @Subscribe
    void showImportProjectWindow(ShowImportProjectWindowEvent event) {
        documentScreen.showImportProjectWindow();
    }
    
    @Subscribe
    void showImportBOEWindow(ShowImportBOEWindowEvent event) {
        documentScreen.showImportBOEWindow();
    }

    @Subscribe
    void saveIntermediateVersion(SaveIntermediateVersionEvent event) {
        final String checkinCommentJson = event.getCheckinComment();
        final Bill bill = billService.createVersion(documentId, event.getVersionType(), checkinCommentJson);
        eventBus.post(new NotificationEvent(NotificationEvent.Type.INFO, "document.major.version.saved"));
        eventBus.post(new DocumentUpdatedEvent());
        leosApplicationEventBus.post(new DocumentUpdatedByCoEditorEvent(user, bill.getVersionSeriesId(), id));
        populateViewWithDocumentDetails(TocMode.SIMPLIFIED);
    }

    @Subscribe
    void importElements(ImportElementRequestEvent event) {
        try {
            SearchCriteriaVO searchCriteria = event.getSearchCriteria();
            List<String> elementIds = event.getElementIds();
            String aknDocument = importService.getAknDocument(searchCriteria.getType().getValue(), Integer.parseInt(searchCriteria.getYear()),
                    Integer.parseInt(searchCriteria.getNumber()));
            if (aknDocument != null) {
                Bill bill = getDocument();
                BillMetadata metadata = bill.getMetadata().getOrError(() -> "Bill metadata is required");
                byte[] newXmlContent = importService.insertSelectedElements(bill, aknDocument.getBytes(StandardCharsets.UTF_8), elementIds,
                        metadata.getLanguage());
                String notificationMsg = "document.import.element.inserted" + (elementIds.stream().anyMatch((s) -> s.startsWith("rec_")) ? ".recitals" : "") +
                        (elementIds.stream().anyMatch((s) -> s.startsWith("art_")) ? ".articles" : "");
                updateBillContent(bill, newXmlContent, messageHelper.getMessage("operation.import.element.inserted"), notificationMsg);
                documentScreen.closeImportWindow();
            } else {
                eventBus.post(new NotificationEvent(Type.INFO, "document.import.no.result"));
            }
        } catch (Exception e) {
            LOG.error("Unable to perform the importElements operation", e);
            eventBus.post(new NotificationEvent(Type.INFO, "document.import.failed"));
        }
    }
    
    @Subscribe
    void importElementsProject(ImportElementProjectRequestEvent event) {
        try {
        	SearchIdProjectVO serachCriteria = event.getSearchCriteria();
            List<String> elementIds = event.getElementIds();
            Bill billAux = billService.findBill(serachCriteria.getId());
            Bill bill = getDocument();
            if (bill != null) {
            	BillMetadata metadata = bill.getMetadata().getOrError(() -> "Bill metadata is required");
    			byte[] newXmlContent = importService.insertSelectedProjectElements(getContent(bill), getContent(billAux), elementIds,metadata.getLanguage());
    			String notificationMsg = "document.import.element.inserted" + (elementIds.stream().anyMatch((s) -> s.startsWith("rec_")) ? ".recitals" : "") +
    			        (elementIds.stream().anyMatch((s) -> s.startsWith("art_")) ? ".articles" : "");
    			updateBillContent(bill, newXmlContent, messageHelper.getMessage("operation.import.element.inserted"), notificationMsg);
    			documentScreen.closeImportWindow();
            }
        } catch (Exception e) {
        	LOG.error("ERROR in importElementsProject: " + e);
            eventBus.post(new NotificationEvent(Type.INFO, "document.import.failed"));
        }
    }

    private void updateBillContent(Bill bill, byte[] xmlContent, String operationMsg, String notificationMsg) {
        bill = billService.updateBill(bill, xmlContent, operationMsg);
        if (bill != null) {
            eventBus.post(new NotificationEvent(Type.INFO, notificationMsg));
            eventBus.post(new RefreshDocumentEvent());
            eventBus.post(new DocumentUpdatedEvent());
            leosApplicationEventBus.post(new DocumentUpdatedByCoEditorEvent(user, strDocumentVersionSeriesId, id));
        }
    }
    
    private void updateBillContent(Bill bill, byte[] xmlContent, String operationMsg) {
        bill = billService.updateBill(bill, xmlContent, operationMsg);
        if (bill != null) {
            eventBus.post(new RefreshDocumentEvent());
            eventBus.post(new DocumentUpdatedEvent());
            leosApplicationEventBus.post(new DocumentUpdatedByCoEditorEvent(user, strDocumentVersionSeriesId, id));
        }
    }


    @Subscribe
    void searchAct(SearchActRequestEvent event) {
        try {
            SearchCriteriaVO searchCriteria = event.getSearchCriteria();
            String aknDocument = importService.getAknDocument(searchCriteria.getType().getValue(), Integer.parseInt(searchCriteria.getYear()),
                    Integer.parseInt(searchCriteria.getNumber()));
            if (aknDocument != null) {
                String transformedAknDocument = getImportXml(aknDocument);
                documentScreen.displaySearchedContent(transformedAknDocument);
            } else {
                documentScreen.displaySearchedContent(null);
                eventBus.post(new NotificationEvent(Type.INFO, "document.import.no.result"));
            }
        } catch (Exception e) {
            LOG.error("Unable to perform searchAct operation", e);
            documentScreen.displaySearchedContent(null);
            eventBus.post(new NotificationEvent(Type.INFO, "document.import.failed"));
        }
    }
    
    @Subscribe
    void searchImportProjectAct(SearchActImportIdRequestEvent event) {
        try {
        	SearchIdProjectVO serachCriteria = event.getSearchCriteria();
        	Bill bill = billService.findBill(serachCriteria.getId());
            final Content content = bill.getContent().getOrError(() -> "Bill content is required!");
            final byte[] contentBytes = content.getSource().getBytes();
            String aknDocument = new String (contentBytes,Charset.forName("UTF-8"));
            String transformedAknDocument = getImportProjectXml(aknDocument);
			documentScreen.displaySearchedProjectContent(transformedAknDocument);
        } catch (Exception e) {
            documentScreen.displaySearchedContent(null);
            eventBus.post(new NotificationEvent(Type.INFO, "document.import.failed"));
        }
    }
    
    @Subscribe
    void searchActImportProject(SearchActImportRequestEvent event) {
    	List<DocumentVO> documentVOs;
        try {
        	SearchCriteriaProjectVO searchCriteria = event.getSearchCriteria();
        	String title = searchCriteria.getTitle();
        	
        	Bill billAux =getDocument();
        	String idAux=billAux.getId();
           
            List<Bill> bills = workspaceService.browseWorkspace(Bill.class, false);
                  documentVOs = bills
                          .stream()
                          .map(this::mapBillToViewObject)
                          .filter(bill -> bill.getTitle().toLowerCase().contains(title.toLowerCase()))
                          .filter(bill -> !bill.getId().equals(idAux))
                          .collect(Collectors.toList());
                  documentScreen.displaySearchedDocuments(documentVOs);
        } catch (Exception e) {
            documentScreen.displaySearchedContent(null);
            eventBus.post(new NotificationEvent(Type.INFO, "document.import.failed"));
        }
    }
    
    @Subscribe
    void searchActImportBOE(SearchActBOERequestEvent event) {
    	String aknDocument = null;
    	String transformedAknDocument = "";
    	String title = "";
    	 try {
             SearchCriteriaBOEVO searchCriteria = event.getSearchCriteria();
             if (!searchCriteria.getReference().isEmpty()) {
                 if (searchCriteria.getReference().trim().equals("BOE-A-2077-0000")) {
                	 searchActImportBOEloop();
                 }
                 else {
                	 if(importService.getBOEDocumentoDerogado(searchCriteria.getReference().trim())) {
                		 eventBus.post(new NotificationEvent(Type.INFO, "document.import.derogative"));
                	 }
                	 aknDocument = importService.getBOEDocument(searchCriteria.getReference().trim(),searchCriteria.getAccion().toString());
                 }
             } else if (!searchCriteria.getNumber().isEmpty()) {
            	 if(importService.getBOEDocumentoDerogado(searchCriteria.getType().getValue(), Integer.parseInt(searchCriteria.getYear()),
            			 Integer.parseInt(searchCriteria.getMonth()), Integer.parseInt(searchCriteria.getDay()), 
            			 Integer.parseInt(searchCriteria.getNumber().trim()))){
            		 eventBus.post(new NotificationEvent(Type.INFO, "document.import.derogative"));
            	 }
            	  aknDocument = importService.getBOEDocument(searchCriteria.getType().getValue(), Integer.parseInt(searchCriteria.getYear()),
                  		 Integer.parseInt(searchCriteria.getMonth()), Integer.parseInt(searchCriteria.getDay()), Integer.parseInt(searchCriteria.getNumber().trim()), searchCriteria.getAccion().toString());
             }
             if (aknDocument != null) {  	 
            	 if (searchCriteria.getAccion().equals(Accion.AMENDMENT)) {
            		// transformedAknDocument =  getAmendmentXml(aknDocument);
            		 String[] arrayDocument = aknDocument.split(":::");
            		 if (arrayDocument[2].contains("No soportado----")) {
            			 String [] tipoNorma = arrayDocument[2].split("----");
            			 transformedAknDocument = null;
                         eventBus.post(new NotificationEvent(Type.INFO, "document.import.amendment.notsupported", tipoNorma[1] ));
            		 } else {
               	        InputStream contentStream = new ByteArrayInputStream(arrayDocument[1].getBytes(StandardCharsets.UTF_8));
             	        transformedAknDocument = transformationService.toXmlFragmentWrapper(contentStream, urlBuilder.getWebAppPath(VaadinServletService.getCurrentServletRequest()),
             	        securityContext.getPermissions(aknDocument));
             	        transformedAknDocument = arrayDocument[0] + ":::" + transformedAknDocument + ":::" +  arrayDocument[2] + ":::" + arrayDocument[3];
            		 }

            	 } else {
            		transformedAknDocument = getImportBOEXml(aknDocument);
            	 }
                 documentScreen.displaySearchedContent(transformedAknDocument);
             } else {
                 documentScreen.displaySearchedContent(null);
                 eventBus.post(new NotificationEvent(Type.INFO, "document.import.no.result"));
             }
         } catch (Exception e) {
             documentScreen.displaySearchedContent(null);
             eventBus.post(new NotificationEvent(Type.INFO, "document.import.failed"));
         }
    }
    
    @Subscribe
    public void changeAmendmentMode(AmendmentModeCloseEvent event) {
        eventBus.post(new LayoutChangeRequestEvent(ColumnPosition.OFF, ComparisonComponent.class));
    }
 
    void searchActImportBOEloop() {
    	String aknDocument = null;
    	//Method for testing purposes
    	//Comment importElementsBOE and uncommment this to text list of BOE References
	    int numOKs = 0;
	    int numFailures = 0;
	    int noResults = 0;
	    List<String> listBOECodesFailure = new ArrayList<>();
    	/*
	    List<String> listBOECodes = Arrays.asList("BOE-A-1995-25444", "BOE-A-2015-10565", "BOE-A-2020-3580", "BOE-A-2015-3443","BOE-A-2000-323","BOE-A-2019-2364",
	    		"BOE-A-2004-21831","BOE-A-2020-1651","BOE-A-2020-1651","BOE-A-2004-4214","BOE-A-2015-11724","BOE-A-2018-17989","BOE-A-2019-5330","BOE-A-2020-1938",
	    		"BOE-A-2019-860","BOE-A-2004-4214","BOE-A-1978-31229","BOE-A-1862-4073","BOE-A-1889-4763","BOE-A-1946-6220","BOE-A-1947-3843","BOE-A-1954-15431",
	    		"BOE-A-1981-12774","BOE-A-2015-10197","BOE-A-2000-1546","BOE-A-1948-40366","BOE-A-1949-12565","BOE-A-1882-6036","BOE-A-1946-2453","BOE-A-2020-3782"
	    		,"BOE-A-2020-3692");
    	*/
    	List<String> listBOECodes = Arrays.asList("BOE-A-2003-811","BOE-A-1985-10249","BOE-A-2014-6438","BOE-A-2008-10206","BOE-A-2012-3549","BOE-A-1980-16128","BOE-A-1981-7560","BOE-A-2011-20036","BOE-A-2016-5530","BOE-A-2014-9903","BOE-A-2011-2709","BOE-A-1967-5590","BOE-A-2012-4772","BOE-A-1998-20055","BOE-A-2010-19487","BOE-A-1994-28968","BOE-A-1986-10638","BOE-A-2017-5859","BOE-A-2000-1654","BOE-A-2012-2011","BOE-A-1985-6730","BOE-A-1999-652","BOE-A-2011-1384","BOE-A-2019-18362","BOE-A-2016-1404","BOE-A-2010-14562","BOE-A-1997-25350","BOE-A-1976-17384","BOE-A-2016-8518","BOE-A-1990-24962","BOE-A-2019-17255","BOE-A-1975-26549","BOE-A-1977-27024","BOE-A-1980-27586","BOE-A-1979-13898","BOE-A-1979-8004","BOE-A-1980-1566","BOE-A-1970-1255","BOE-A-1981-12786","BOE-A-1982-17678","BOE-A-1984-13785","BOE-A-1984-8269","BOE-A-1989-8296","BOE-A-1983-49971","BOE-A-1983-49964","BOE-A-1983-50045","BOE-A-1970-494","BOE-A-1964-7522","BOE-A-1971-459","BOE-A-1985-873","BOE-A-2006-19503","BOE-A-2004-21142","BOE-A-1993-10255","BOE-A-1999-6371","BOE-A-1976-8989","BOE-A-2019-11398","BOE-A-1976-7526","BOE-A-1976-1812","BOE-A-2019-14422","BOE-A-1976-751","BOE-A-2019-5569","BOE-A-1851-4969","BOE-A-2006-16665","BOE-A-2019-5243","BOE-A-1997-21768","BOE-A-1995-4814","BOE-A-1995-10370","BOE-A-2019-4357","BOE-A-1999-4643","BOE-A-2003-3708","BOE-A-1985-26588","BOE-A-1996-6337","BOE-A-2003-10624","BOE-A-2003-13183","BOE-A-1962-6702","BOE-A-2008-17630","BOE-A-2006-3668","BOE-A-2013-9212","BOE-A-1964-10834","BOE-A-1983-33966","BOE-A-1985-20800","BOE-A-2011-18913","BOE-A-1994-2529","BOE-A-1983-33970","BOE-A-1984-17218","BOE-A-1983-5917","BOE-A-1984-26009","BOE-A-1982-25454","BOE-A-1982-4569","BOE-A-1994-5706","BOE-A-1985-26315","BOE-A-1994-17118","BOE-A-1986-30898","BOE-A-1995-22237","BOE-A-1995-20060","BOE-A-1995-3747","BOE-A-1983-31435","BOE-A-1983-32649","BOE-A-1984-12252","BOE-A-1987-10509","BOE-A-1984-11369","BOE-A-1984-5566","BOE-A-1984-8337","BOE-A-1985-22560","BOE-A-1993-24376","BOE-A-1993-19892","BOE-A-1985-3571","BOE-A-1989-9618","BOE-A-1985-10274","BOE-A-1993-31000","BOE-A-1989-10380","BOE-A-1983-18036","BOE-A-1975-24975","BOE-A-1978-23781","BOE-A-1984-5078","BOE-A-1978-11100","BOE-A-1977-25573","BOE-A-1973-1718","BOE-A-1986-9317","BOE-A-1971-782","BOE-A-1972-751","BOE-A-1972-306","BOE-A-1972-1194","BOE-A-1968-878","BOE-A-1982-19505","BOE-A-1969-1417","BOE-A-1971-431","BOE-A-1987-15291","BOE-A-1986-14369","BOE-A-1987-12161","BOE-A-1965-1821","BOE-A-1967-1407","BOE-A-2007-20783","BOE-A-2019-6344","BOE-A-2019-1793","BOE-A-2019-2712","BOE-A-2018-18003","BOE-A-2018-18005","BOE-A-2018-9463","BOE-A-2018-17221","BOE-A-2018-8155","BOE-A-2018-16538","BOE-A-2018-7154","BOE-A-2018-16432","BOE-A-2018-6891","BOE-A-2018-6182","BOE-A-2018-3433","BOE-A-2017-6015","BOE-A-2018-928","BOE-A-2016-4475","BOE-A-2016-2177","BOE-A-2015-4682","BOE-A-2015-10141","BOE-A-2015-9677","BOE-A-2015-10201","BOE-A-2015-6876","BOE-A-2013-4187","BOE-A-2014-5739","BOE-A-2014-8446","BOE-A-2014-1219","BOE-A-2013-6876","BOE-A-2013-8555","BOE-A-2014-9467","BOE-A-2013-4461","BOE-A-2013-5454","BOE-A-2014-6857","BOE-A-2013-7385","BOE-A-2014-10871","BOE-A-1960-30015","BOE-A-2011-20276","BOE-A-2011-11179","BOE-A-2012-871","BOE-A-2011-19439","BOE-A-2011-19599","BOE-A-2011-16283","BOE-A-2012-5932","BOE-A-2012-1542","BOE-A-2011-10781","BOE-A-2011-8344","BOE-A-2011-9865","BOE-A-2011-13387","BOE-A-2011-6874","BOE-A-2011-16758","BOE-A-1962-13436","BOE-A-1962-4439","BOE-A-1963-13860","BOE-A-1964-16662","BOE-A-1963-22624","BOE-A-2011-4038","BOE-A-2010-19647","BOE-A-2010-14629","BOE-A-2010-11156","BOE-A-2010-4766","BOE-A-2009-18003","BOE-A-2010-731","BOE-A-2000-4696","BOE-A-2008-3310","BOE-A-2009-12401","BOE-A-2003-10463","BOE-A-2009-12934","BOE-A-2009-14504","BOE-A-2008-14721","BOE-A-2006-21407","BOE-A-2007-8351","BOE-A-2007-7419","BOE-A-2007-11593","BOE-A-2007-10559","BOE-A-2007-7791","BOE-A-2008-4480","BOE-A-2007-14486","BOE-A-2006-18694","BOE-A-2006-18968","BOE-A-2007-18534","BOE-A-2008-519","BOE-A-2007-20639","BOE-A-2005-5791","BOE-A-2005-8783","BOE-A-2005-19250","BOE-A-2005-11970","BOE-A-2005-10899","BOE-A-2005-19990","BOE-A-2005-2537","BOE-A-2005-1256","BOE-A-2006-6246","BOE-A-2006-2782","BOE-A-2006-2783","BOE-A-2006-8346","BOE-A-2006-9298","BOE-A-2006-8044","BOE-A-2006-7028","BOE-A-2004-19123","BOE-A-2004-17003","BOE-A-2004-15261","BOE-A-1998-8427","BOE-A-1993-30621","BOE-A-1983-34090","BOE-A-1989-18835","BOE-A-1997-14057","BOE-A-1995-10376","BOE-A-1990-8114","BOE-A-1968-436","BOE-A-1970-733","BOE-A-1968-1452","BOE-A-1968-898","BOE-A-1972-10","BOE-A-1971-1440","BOE-A-1988-17065","BOE-A-1973-1239","BOE-A-1969-786","BOE-A-1977-1648","BOE-A-1979-11241","BOE-A-1977-29585","BOE-A-1975-24190","BOE-A-1974-1951","BOE-A-1999-11203","BOE-A-1994-17121","BOE-A-2001-23923","BOE-A-2002-14079","BOE-A-1999-23476","BOE-A-2002-2521","BOE-A-1999-18290","BOE-A-2001-6823","BOE-A-2002-3137","BOE-A-2002-2517","BOE-A-2002-8266","BOE-A-2003-706","BOE-A-2002-10791","BOE-A-2001-24749","BOE-A-1999-5941","BOE-A-1998-21126","BOE-A-1999-24787","BOE-A-2002-9157","BOE-A-1998-2298","BOE-A-1999-647","BOE-A-1989-25841","BOE-A-1999-2938","BOE-A-2001-14769","BOE-A-2001-22357","BOE-A-1999-20940","BOE-A-2001-8794","BOE-A-2000-24364","BOE-A-1998-26351","BOE-A-1993-7949","BOE-A-1989-13352","BOE-A-1989-13485","BOE-A-1993-14575","BOE-A-1998-20643","BOE-A-1993-4678","BOE-A-1989-27948","BOE-A-1993-14101","BOE-A-1989-25109","BOE-A-1989-2071","BOE-A-1998-19580","BOE-A-1998-27866","BOE-A-1997-584","BOE-A-1985-26514","BOE-A-1989-11296","BOE-A-1985-19176","BOE-A-1997-5412","BOE-A-1985-12313","BOE-A-1988-7326","BOE-A-2002-6701","BOE-A-1988-29447","BOE-A-1985-26680","BOE-A-1984-2421","BOE-A-1997-15809","BOE-A-1992-20896","BOE-A-1988-16138","BOE-A-1988-27017","BOE-A-1988-1154","BOE-A-1985-20370","BOE-A-1996-8671","BOE-A-1992-21158","BOE-A-1992-19985","BOE-A-1996-18461","BOE-A-1987-26787","BOE-A-1987-13249","BOE-A-1984-14426","BOE-A-1987-22092","BOE-A-1996-1769","BOE-A-1984-14281","BOE-A-2000-1556","BOE-A-1996-15078","BOE-A-1987-16196","BOE-A-1991-14237","BOE-A-1986-25968","BOE-A-1992-9361","BOE-A-1995-13303","BOE-A-1986-6200","BOE-A-1991-6082","BOE-A-1983-16949","BOE-A-1983-8189","BOE-A-1983-17040","BOE-A-1994-630","BOE-A-1981-26082","BOE-A-1994-28965","BOE-A-1981-1811","BOE-A-1994-28509","BOE-A-1994-9311","BOE-A-1994-7653","BOE-A-1994-23283","BOE-A-1981-17508","BOE-A-1982-10983","BOE-A-1979-25390","BOE-A-1979-20609","BOE-A-2003-18333","BOE-A-1979-24915","BOE-A-1979-7046","BOE-A-2000-21432","BOE-A-1991-30905","BOE-A-1999-15798","BOE-A-1979-25083","BOE-A-1980-2456","BOE-A-1989-28112","BOE-A-1988-29233","BOE-A-1993-29581","BOE-A-1993-30230","BOE-A-1989-7364","BOE-A-1995-63","BOE-A-1991-28263","BOE-A-2002-19805","BOE-A-2004-12135","BOE-A-1997-8069","BOE-A-1996-4524","BOE-A-1999-14066","BOE-A-1989-14138","BOE-A-2003-12703","BOE-A-1999-5258","BOE-A-1999-17418","BOE-A-2000-23503","BOE-A-1975-16967","BOE-A-2001-22768","BOE-A-2003-22723","BOE-A-2004-3632","BOE-A-1987-2492","BOE-A-1991-7557","BOE-A-2004-3952","BOE-A-2003-20838","BOE-A-2004-6324","BOE-A-2004-4781","BOE-A-2004-510","BOE-A-2003-23814","BOE-A-2004-3717","BOE-A-2004-6760","BOE-A-1997-17582","BOE-A-1989-25944","BOE-A-1979-6663","BOE-A-1964-19393","BOE-A-1961-12556","BOE-A-1967-7596","BOE-A-2011-8629","BOE-A-2017-15820","BOE-A-1977-10171","BOE-A-1977-1197","BOE-A-1977-23048","BOE-A-1977-31066","BOE-A-1977-909","BOE-A-1988-8677","BOE-A-1969-1111","BOE-A-1954-17807","BOE-A-1954-8506","BOE-A-1958-11342","BOE-A-1964-19148","BOE-A-1975-23293","BOE-A-2006-20849","BOE-A-2012-1543","BOE-A-2012-4965","BOE-A-2012-11652","BOE-A-2014-10745","BOE-A-2015-4390","BOE-A-2001-11051","BOE-A-2004-21561","BOE-A-1999-19391","BOE-A-1998-21930","BOE-A-1995-10633","BOE-A-1994-21214","BOE-A-1995-3396","BOE-A-1995-4340","BOE-A-1995-20840","BOE-A-1995-20697","BOE-A-1995-23030","BOE-A-1996-6643","BOE-A-1984-11619","BOE-A-1979-19544","BOE-A-1979-12987","BOE-A-1980-5628","BOE-A-1982-22966","BOE-A-1986-2902","BOE-A-1986-11263","BOE-A-1983-24284","BOE-A-1983-25238","BOE-A-1983-25944","BOE-A-1984-12186","BOE-A-1984-3278","BOE-A-1988-5966","BOE-A-1984-499","BOE-A-1987-25860","BOE-A-1985-25332","BOE-A-1985-13764","BOE-A-1985-9989","BOE-A-1984-4019","BOE-A-1974-1520","BOE-A-1979-7661","BOE-A-1984-26052","BOE-A-1981-17891","BOE-A-1985-25639","BOE-A-1982-32247","BOE-A-1976-10594","BOE-A-1961-187","BOE-A-1978-15900","BOE-A-1973-1215","BOE-A-1980-11706","BOE-A-1973-1179","BOE-A-1978-1857","BOE-A-1973-478","BOE-A-1974-1577","BOE-A-1974-1890","BOE-A-1974-720","BOE-A-1974-924","BOE-A-1973-1189","BOE-A-1970-502","BOE-A-1969-965","BOE-A-1968-1332","BOE-A-1960-14667","BOE-A-1968-974","BOE-A-1960-19427","BOE-A-1963-22702","BOE-A-1960-19532","BOE-A-1960-19409","BOE-A-1960-19483","BOE-A-1961-23872","BOE-A-1963-617","BOE-A-1962-24391","BOE-A-1965-6164","BOE-A-1967-2910","BOE-A-1964-21391","BOE-A-1967-5586","BOE-A-1965-21377","BOE-A-1962-5068","BOE-A-1961-23943","BOE-A-1965-6116","BOE-A-1963-14042","BOE-A-1965-7058","BOE-A-1962-24373","BOE-A-1966-9003","BOE-A-1966-8998","BOE-A-1961-23937","BOE-A-1964-21457","BOE-A-1964-62","BOE-A-1961-6201","BOE-A-1962-3741","BOE-A-1971-20017","BOE-A-1972-416","BOE-A-1971-584","BOE-A-1968-385","BOE-A-1975-4184","BOE-A-1974-2042","BOE-A-1975-7497","BOE-A-1975-8169","BOE-A-1962-10765","BOE-A-1963-12567","BOE-A-1972-21","BOE-A-1964-19071","BOE-A-1962-22133","BOE-A-1963-24119","BOE-A-1966-13709","BOE-A-1964-23635","BOE-A-1965-14452","BOE-A-1960-18149","BOE-A-1963-1997","BOE-A-1978-25660","BOE-A-1978-27242","BOE-A-1978-25686","BOE-A-1971-1129","BOE-A-1973-1231","BOE-A-1974-46","BOE-A-1975-11384","BOE-A-1975-13157","BOE-A-1974-2127","BOE-A-1974-1472","BOE-A-1973-1297","BOE-A-1974-1541","BOE-A-1974-1159","BOE-A-1972-1516","BOE-A-1972-66","BOE-A-1971-947","BOE-A-1970-1055","BOE-A-1970-10540","BOE-A-1971-845","BOE-A-1968-1002","BOE-A-1969-1226","BOE-A-1964-20");
	    for (String BOEid : listBOECodes) {
	    	try {
	            aknDocument = importService.getBOEDocument(BOEid,"");
	            if (aknDocument != null) {  	 
	                 String transformedAknDocument = getImportBOEXml(aknDocument);
	                 documentScreen.displaySearchedContent(transformedAknDocument);
	                 numOKs++;
	             } else {
	                 documentScreen.displaySearchedContent(null);
	                 eventBus.post(new NotificationEvent(Type.INFO, "document.import.no.result"));
	                 noResults++;
	             }
	         } catch (Exception e) {
	             documentScreen.displaySearchedContent(null);
	             listBOECodesFailure.add(BOEid);
	             numFailures++;
	         }
	    }
	    LOG.info("OK: " + numOKs +"Fallos: " + numFailures + "Sin Resultado: " + noResults);
	    eventBus.post("OK: " + numOKs +"Fallos: " + numFailures + "Sin Resultado: " + noResults);
    }    
    
    @Subscribe
    void importElementsBOE(ImportElementBOERequestEvent event) {
    	String aknDocument = null;
    	try {
    		SearchCriteriaBOEVO serachCriteria = event.getSearchCriteria();
    		List<String> elementIds = event.getElementIds();
    		if (!serachCriteria.getReference().isEmpty()) {
    			if (serachCriteria.getReference().trim() == "BOE-A-2077-0000") {
    				importElementsBOEloop(event.getElementIds());
    			}
    			else {
    				aknDocument = importService.getBOEDocument(serachCriteria.getReference().trim(),"");
    			}
    		} else if (!serachCriteria.getNumber().isEmpty()) {
    			aknDocument = importService.getBOEDocument(serachCriteria.getType().getValue(), Integer.parseInt(serachCriteria.getYear()),
    					Integer.parseInt(serachCriteria.getMonth()), Integer.parseInt(serachCriteria.getDay()), Integer.parseInt(serachCriteria.getNumber().trim()),"");
    		}
    		if (aknDocument != null) {
    			Bill bill = getDocument();
    			BillMetadata metadata = bill.getMetadata().getOrError(() -> "Bill metadata is required");
    			byte[] newXmlContent = importService.insertSelectedElementsBOE(getContent(bill),aknDocument.getBytes(StandardCharsets.UTF_8) /*StringUtils.normalizeSpace(aknDocument).getBytes(UTF_8)*/, elementIds,
    					metadata.getLanguage());
    			ArrayList<String> listIdModified = importService.getNumberBOEModified(aknDocument.getBytes(StandardCharsets.UTF_8), elementIds, metadata.getLanguage());
    			boolean isDerogadElement = importService.getElementRepealed(aknDocument.getBytes(StandardCharsets.UTF_8), elementIds);
    			boolean isReferencedElement = importService.getBOEElementReferenced(aknDocument.getBytes(StandardCharsets.UTF_8), elementIds);
    			String notificationMsg = "document.import.element.inserted" + (elementIds.stream().anyMatch((s) -> s.startsWith("rec_")) ? ".recitals" : "") +
    					(elementIds.stream().anyMatch((s) -> s.startsWith("art_")) ? ".articles" : "");
    			String message;
    			boolean isListModified = false;
    			if (!listIdModified.isEmpty()) {
    				isListModified = true;
    				String contentModified = messageHelper.getMessage("document.import.modified.reference", serachCriteria.getReference().trim());
    				for(String aux: listIdModified) {
    					String[] auxArray = aux.split(":");
    					contentModified +=  "<p>" + auxArray[1] + "</p>";
    				}
    				updateBillContent(bill, newXmlContent, messageHelper.getMessage("operation.import.element.inserted"));         	  
    				eventBus.post(new DisableAllButtonEvent());
    				documentScreen.displaySearchedContent(contentModified);
    				message = makeMessageAlert(isListModified, isDerogadElement, isReferencedElement, messageHelper);
    				eventBus.post(new NotificationEvent(Type.INFO,message));
    			} else if (isDerogadElement || isReferencedElement) {
    				updateBillContent(bill, newXmlContent, messageHelper.getMessage("operation.import.element.inserted"));
    				message = makeMessageAlert(isListModified, isDerogadElement, isReferencedElement, messageHelper);
    				eventBus.post(new NotificationEvent(Type.INFO,message));
    				documentScreen.closeImportWindow();
    			} else {
    				updateBillContent(bill, newXmlContent, messageHelper.getMessage("operation.import.element.inserted"), notificationMsg);
    				documentScreen.closeImportWindow();
    			}

    		} else {
    			eventBus.post(new NotificationEvent(Type.INFO, "document.import.no.result"));
    		}
    	} catch (Exception e) {
    		documentScreen.displaySearchedContent(null);
    		eventBus.post(new NotificationEvent(Type.INFO, "document.import.failed"));
    	}
    }
    
    private String makeMessageAlert(boolean listIdModified, boolean isDerogadElement, boolean isReferencedElement, MessageHelper messageHelper) {
    	String message = "";
    	if (listIdModified) {
    		message = messageHelper.getMessage("document.import.modified.result") + " ";
    	}
    	if (isDerogadElement) {
    		message += messageHelper.getMessage("document.import.derogative.element")  + " ";
    	}
    	if (isReferencedElement) {
    		message += messageHelper.getMessage("document.import.reference.modified");
    	}
    	return message;
    }
 
    void importElementsBOEloop(List<String> elementIds) {
    	//Method for testing purposes
    	//Comment importElementsBOE and uncommment this to text list of BOE References
	    String aknDocument = null;
	    int numOKs = 0;
	    int numFailures = 0;
	    int noResults = 0;
	    List<String> listBOECodesFailure = new ArrayList<>();
	    List<String> listBOECodes = Arrays.asList("BOE-A-1995-25444", "BOE-A-2015-10565", "BOE-A-2020-3580", "BOE-A-2015-3443","BOE-A-2000-323","BOE-A-2019-2364",
	    		"BOE-A-2004-21831","BOE-A-2020-1651","BOE-A-2020-1651","BOE-A-2004-4214","BOE-A-2015-11724","BOE-A-2018-17989","BOE-A-2019-5330","BOE-A-2020-1938",
	    		"BOE-A-2019-860","BOE-A-2004-4214","BOE-A-1978-31229","BOE-A-1862-4073","BOE-A-1889-4763","BOE-A-1946-6220","BOE-A-1947-3843","BOE-A-1954-15431",
	    		"BOE-A-1981-12774","BOE-A-2015-10197","BOE-A-2000-1546","BOE-A-1948-40366","BOE-A-1949-12565","BOE-A-1882-6036","BOE-A-1946-2453","BOE-A-2020-3782"
	    		,"BOE-A-2020-3692");
	    for (String BOEid : listBOECodes) {
		    try {          
		           	aknDocument = importService.getBOEDocument(BOEid,"");
		           	if (aknDocument != null) {
		              Bill bill = getDocument();
		              BillMetadata metadata = bill.getMetadata().getOrError(() -> "Bill metadata is required");
		              byte[] newXmlContent = importService.insertSelectedElementsBOE(getContent(bill),aknDocument.getBytes(StandardCharsets.UTF_8) /*StringUtils.normalizeSpace(aknDocument).getBytes(UTF_8)*/, elementIds,
		                      metadata.getLanguage());
		              String notificationMsg = "document.import.element.inserted" + (elementIds.stream().anyMatch((s) -> s.startsWith("rec_")) ? ".recitals" : "") +
		                      (elementIds.stream().anyMatch((s) -> s.startsWith("art_")) ? ".articles" : "");
		              updateBillContent(bill, newXmlContent, messageHelper.getMessage("operation.import.element.inserted"), notificationMsg);
		              documentScreen.closeImportWindow();
		              numOKs++;
		          } else {
		        	  noResults++;
		          }
		   	 } catch (Exception e) {
		        	numFailures++;
		        	listBOECodesFailure.add(BOEid);
		        }
	    }
	    eventBus.post(new NotificationEvent(Type.INFO, "OK: " + numOKs +"Fallos: " + numFailures + "Sin Resultado: " + noResults));
    }    
    
    private String getImportBOEXml(String content) {
        return transformationService.toImportBOEXml(
        new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8)),
        urlBuilder.getWebAppPath(VaadinServletService.getCurrentServletRequest()), securityContext.getPermissions(content));
    }
    
    //Evolutivo #2678
    @Subscribe    
    void amendmentElementsPreviewRequest(AmendmentElementsPreviewRequest event) {  
    	List<String> elementAmendmentSelect = event.getElementAmendmentSelect();
    	String parentElement = event.getParentElement();
    	String elementPreview = amendPatternLabelService.generatePreview(elementAmendmentSelect, parentElement);
    	documentScreen.showAmendmentViewLabelPreview(elementPreview);
    }
    
    
    @Subscribe
    void scrollTo(ScrollToEvent event) {
        documentScreen.scrollTo(event.getElementId());
    }
    
    @Subscribe    
    void amendmentElementModificationRequest(AmendmentElementModificationRequest event) {  
    	String elementPreview = amendPatternLabelService.generateLabel(event.getAction(), event.getElementAmendmentSelect(),event.getTitle(), event.getEli(), event.getNumArticleCk(), event.getNormType(), event.getTypeElement(), event.getArticleId(), event.isEditing());
    	documentScreen.showAmendmentViewLabelPreviewWithAction(elementPreview);
    }

    
    private DocumentVO mapBillToViewObject(Bill bill) {
        DocumentVO documentVO = new DocumentVO(bill);
        //documentVO.setProcedureType(ProcedureType.ORDINARY_LEGISLATIVE_PROC);//AGE-CORE-3.0.0 CUARENTENA
        documentVO.setProcedureType(null);
        documentVO.setCreatedBy(bill.getCreatedBy());
        documentVO.setCreatedOn(Date.from(bill.getCreationInstant()));
        return documentVO;
    }

    @Subscribe
    void searchAndReplaceText(SearchAndReplaceTextEvent event) {
        Bill bill = getDocument();
        byte[] updatedContent = billService.searchAndReplaceText(getContent(bill), event.getSearchText(), event.getReplaceText());
        if (updatedContent != null) {
            // save document into repository
            bill = billService.updateBill(bill, updatedContent, messageHelper.getMessage("operation.search.replace.updated"));
            if (bill != null) {
                eventBus.post(new RefreshDocumentEvent());
                eventBus.post(new DocumentUpdatedEvent());
                leosApplicationEventBus.post(new DocumentUpdatedByCoEditorEvent(user, strDocumentVersionSeriesId, id));
                eventBus.post(new NotificationEvent(Type.INFO, "document.popup.replace.success"));
            }
        } else {
            eventBus.post(new NotificationEvent(Type.INFO, "document.popup.replace.failed"));
        }
    }

    @Subscribe
    void cleanComparedContent(CleanComparedContentEvent event) {
        documentScreen.cleanComparedContent();
    }

    @Subscribe
    void showVersion(ShowVersionRequestEvent event) {
        final Bill version = billService.findBillVersion(event.getVersionId());
        final String versionContent = comparisonDelegate.getDocumentAsHtml(version);
        final String versionInfo = getVersionInfoAsString(version);
        documentScreen.showVersion(versionContent, versionInfo);
        eventBus.post(new EnableSyncScrollRequestEvent(true));
    }

    @Subscribe
    void compareUpdateDocumentView(CompareRequestEvent event) {
        final Bill oldVersion = billService.findBillVersion(event.getOldVersionId());
        final Bill newVersion = billService.findBillVersion(event.getNewVersionId());
        final String comparedContent = comparisonDelegate.getMarkedContent(oldVersion, newVersion);
        final String comparedInfo = messageHelper.getMessage("version.compare.simple", oldVersion.getVersionLabel(), newVersion.getVersionLabel());
        documentScreen.populateMarkedContent(comparedContent, comparedInfo);
        eventBus.post(new EnableSyncScrollRequestEvent(true));
    }

    @Subscribe
    void comparePopulateTimeLine(CompareTimeLineRequestEvent event) {
        final Bill oldVersion = billService.findBillVersion(event.getOldVersion());
        final Bill newVersion = billService.findBillVersion(event.getNewVersion());
        final ComparisonDisplayMode displayMode = event.getDisplayMode();
        HashMap<ComparisonDisplayMode, Object> result = comparisonDelegate.versionCompare(oldVersion, newVersion, displayMode);
        documentScreen.displayComparison(result);
    }

    @Subscribe
    void doubleCompare(DoubleCompareRequestEvent event) {
        final Bill original = billService.findBillVersion(event.getOriginalProposalId());
        final Bill intermediate = billService.findBillVersion(event.getIntermediateMajorId());
        final Bill current = billService.findBillVersion(event.getCurrentId());
        final boolean isEnabled = event.isEnabled();
        final String comparedContent = comparisonDelegate.doubleCompareHtmlContents(original, intermediate, current, isEnabled);
        final String comparedInfo = messageHelper.getMessage("version.compare.double", original.getVersionLabel(), intermediate.getVersionLabel(), current.getVersionLabel());
        documentScreen.populateDoubleComparisonContent(comparedContent, comparedInfo);
        eventBus.post(new EnableSyncScrollRequestEvent(true));
    }

    private String getVersionInfoAsString(XmlDocument document) {
        final VersionInfoVO versionInfo = getVersionInfo(document);
        final String versionInfoString = messageHelper.getMessage(
                "document.version.caption",
                versionInfo.getDocumentVersion(),
                versionInfo.getLastModifiedBy(),
                versionInfo.getEntity(),
                versionInfo.getLastModificationInstant()
        );
        return versionInfoString;
    }

    @Subscribe
    void getDocumentVersionsList(VersionListRequestEvent<Bill> event) {
        List<Bill> billVersions = billService.findVersions(documentId);
        eventBus.post(new VersionListResponseEvent<>(new ArrayList<>(billVersions)));
    }

    @Subscribe
    void exportToDocuWrite(DocuWriteExportRequestEvent<Bill> event) {
        ExportOptions exportOptions = event.getExportOptions();
        try {
            LeosPackage leosPackage = packageService.findPackageByDocumentId(documentId);
            BillContext context = billContextProvider.get();
            context.usePackage(leosPackage);
            String proposalId = context.getProposalIdFromBill();
            if (proposalId != null) {
                Optional<XmlDocument> versionToCompare = Optional.of(event.getVersion());
                File resultOutputFile = exportService.createDocuWritePackage("Proposal_" + proposalId + "_" + System.currentTimeMillis()+".zip", proposalId, exportOptions,
                        versionToCompare);
                DownloadStreamResource downloadStreamResource = new DownloadStreamResource(resultOutputFile.getName(), new FileInputStream(resultOutputFile));
                documentScreen.setDownloadStreamResource(downloadStreamResource);
            }
        } catch (Exception e) {
            LOG.error("Unexpected error occurred while using ExportService", e);
            eventBus.post(new NotificationEvent(Type.ERROR, "export.docuwrite.error.message", e.getMessage()));
        }
    }

    @Subscribe
    void versionRestore(RestoreVersionRequestEvent event) {
        String versionId = event.getVersionId();
        Bill version = billService.findBillVersion(versionId);
        byte[] resultXmlContent = getContent(version);
        billService.updateBill(getDocument(), resultXmlContent, messageHelper.getMessage("operation.restore.version", version.getVersionLabel()));

        List<Bill> documentVersions = billService.findVersions(documentId);
        documentScreen.updateTimeLineWindow(documentVersions);
        eventBus.post(new RefreshDocumentEvent());
        eventBus.post(new DocumentUpdatedEvent()); // Document might be updated.
        leosApplicationEventBus.post(new DocumentUpdatedByCoEditorEvent(user, strDocumentVersionSeriesId, id));
    }

    @Subscribe
    public void getUserGuidance(FetchUserGuidanceRequest event) {
        // KLUGE temporary hack for compatibility with new domain model
        Bill bill = billService.findBill(documentId);
        String jsonGuidance = templateConfigurationService.getTemplateConfiguration(bill.getMetadata().get().getDocTemplate(),"guidance");
        documentScreen.setUserGuidance(jsonGuidance);
    }

    @Subscribe
    public void getUserPermissions(FetchUserPermissionsRequest event) {
        Bill bill = getDocument();
        List<LeosPermission> userPermissions = securityContext.getPermissions(bill);
        documentScreen.sendUserPermissions(userPermissions);
    }

    private String getUserEntity(String userId) {
        User user = userHelper.getUser(userId);
        return user.getDefaultEntity() != null ? user.getDefaultEntity().getOrganizationName() : "";
    }

    private VersionInfoVO getVersionInfo(XmlDocument document) {
        String userId = document.getLastModifiedBy();
        User user = userHelper.getUser(userId);

        return new VersionInfoVO(
                document.getVersionLabel(),
                user.getName(), user.getDefaultEntity() != null ? user.getDefaultEntity().getOrganizationName() : "",
                dateFormatter.format(Date.from(document.getLastModificationInstant())),
                document.getVersionType());
    }

    private DocumentVO createLegalTextVO(Bill bill) {
        DocumentVO billVO = new DocumentVO(bill.getId(),
                bill.getMetadata().exists(m -> m.getLanguage() != null) ? bill.getMetadata().get().getLanguage() : "EN",
                LeosCategory.BILL,
                bill.getLastModifiedBy(),
                Date.from(bill.getLastModificationInstant()));
        if (bill.getMetadata().isDefined()) {
            BillMetadata metadata = bill.getMetadata().get();
            billVO.getMetadata().setInternalRef(metadata.getRef());
        }
        if (!bill.getCollaborators().isEmpty()) {
            billVO.addCollaborators(bill.getCollaborators());
        }
        return billVO;
    }

    @Subscribe
    void updateProposalMetadata(DocumentUpdatedEvent event) {
        if (event.isModified()) {
            CollectionContext context = proposalContextProvider.get();
            context.useChildDocument(documentId);
            context.useActionComment(messageHelper.getMessage("operation.metadata.updated"));
            context.executeUpdateProposalAsync();
        }
    }

    @Subscribe
    public void onInfoUpdate(UpdateUserInfoEvent updateUserInfoEvent) {
        if (isCurrentInfoId(updateUserInfoEvent.getActionInfo().getInfo().getDocumentId())) {
            if (!id.equals(updateUserInfoEvent.getActionInfo().getInfo().getPresenterId())) {
                eventBus.post(new NotificationEvent(leosUI, "coedition.caption",
                        "coedition.operation." + updateUserInfoEvent.getActionInfo().getOperation().getValue(),
                        NotificationEvent.Type.TRAY, updateUserInfoEvent.getActionInfo().getInfo().getUserName()));
            }
            LOG.debug("Document Presenter updated the edit info -" + updateUserInfoEvent.getActionInfo().getOperation().name());
            documentScreen.updateUserCoEditionInfo(updateUserInfoEvent.getActionInfo().getCoEditionVos(), id);
        }
    }

    private boolean isCurrentInfoId(String versionSeriesId) {
        return versionSeriesId.equals(strDocumentVersionSeriesId);
    }

    @Subscribe
    private void documentUpdatedByCoEditor(DocumentUpdatedByCoEditorEvent documentUpdatedByCoEditorEvent) {
        if (isCurrentInfoId(documentUpdatedByCoEditorEvent.getDocumentId()) &&
                !id.equals(documentUpdatedByCoEditorEvent.getPresenterId())) {
            eventBus.post(new NotificationEvent(leosUI, "coedition.caption", "coedition.operation.update", NotificationEvent.Type.TRAY,
                    documentUpdatedByCoEditorEvent.getUser().getName()));
            documentScreen.displayDocumentUpdatedByCoEditorWarning();
        }
    }

    @Subscribe
    public void fetchMilestoneByVersionedReference(FetchMilestoneByVersionedReferenceEvent event) {
        LeosPackage leosPackage = packageService.findPackageByDocumentId(documentId);
        LegDocument legDocument = legService.findLastLegByVersionedReference(leosPackage.getPath(), event.getVersionedReference());
        documentScreen.showMilestoneExplorer(legDocument, String.join(",", legDocument.getMilestoneComments()));
    }
    
    @Subscribe
    public void changeComparisionMode(ComparisonEvent event) {
        comparisonMode = event.isComparsionMode();
        if (comparisonMode) {
            documentScreen.cleanComparedContent();
            if (!documentScreen.isComparisonComponentVisible()) {
                LayoutChangeRequestEvent layoutEvent = new LayoutChangeRequestEvent(ColumnPosition.DEFAULT, ComparisonComponent.class);
                eventBus.post(layoutEvent);
            }
        } else {
            LayoutChangeRequestEvent layoutEvent = new LayoutChangeRequestEvent(ColumnPosition.OFF, ComparisonComponent.class);
            eventBus.post(layoutEvent);
        }
        updateVersionsTab(new DocumentUpdatedEvent());
    }
    
    @Subscribe
    void amendingActViewRequestEvent(AmendmentViewRequestEvent event) {
        try {
            final Bill bill = getDocument();
            final String articleId = event.getElementId();
            final String type = event.getType();

            AmendActDetails amendActDetails = amendService.openAmendmentEditor(bill, articleId, type);

            if(amendActDetails == null) {
                eventBus.post(new CloseElementEvent());
                eventBus.post(new DocumentUpdatedEvent());
            }
            eventBus.post(new AmendmentModeExtensionEvent(true, articleId));
            documentScreen.showAmendmentView(amendActDetails, event.getArticleNum(), articleId, type);
            documentScreen.scrollTo(articleId);
        } catch (Exception e) {
            LOG.error("Unexpected error occurred while opening Amendment View", e);
            eventBus.post(new NotificationEvent(Type.ERROR, "generic.error.message", e.getMessage()));
        }
    }  
    
    @Subscribe
    public void amendmentAddRequest (AmendmentAddRequest event) {
    	 String elementId = event.getElementId();
    	 String element = event.getElement();
    	 String eli = event.getEli();
    	 String nameNorm = event.getNameNorm();
    	 String actionType = event.getActionType();
    	 String elementType = event.getElementType();
    	 boolean isEditing = event.isEditing();
         Bill bill = getDocument();
         String idModNew;
         
         try {
        	 if (!isEditing) {
            	 idModNew = amendPatternLabelService.extractIdModAmendment(element);      
                 final byte[] newXmlContentReference = amendPatternLabelService.insertNewMetadaElementActiveRef(bill, eli);
                 if (newXmlContentReference != null) {
                     final String title = messageHelper.getMessage("operation.element.updated", StringUtils.capitalize(elementType));
                     final String description = messageHelper.getMessage("document.amendment.created.checkin");
                     final String elementLabel = generateLabel(elementId, bill);
                     final CheckinCommentVO checkinComment = new CheckinCommentVO(title, description, new CheckinElement(ActionType.UPDATED, elementId, elementType, elementLabel));
                     final String checkinCommentJson = CheckinCommentUtil.getJsonObject(checkinComment);
                     bill = billService.updateBill(bill, newXmlContentReference, checkinCommentJson);

                     final byte[] newXmlContent = elementProcessor.updateElement(bill, element, elementType, elementId);                
                     bill = billService.updateBill(bill, newXmlContent, checkinCommentJson);
                     
                     final byte[] newXmlContentAnalysis = amendPatternLabelService.insertNewMetadaElementAnalisis(bill, idModNew, eli, actionType);
                     if (newXmlContentAnalysis != null) {
                    	 billService.updateBill(bill, newXmlContentAnalysis, checkinCommentJson);
                     }                    
                     changeAmendmentMode(new AmendmentModeCloseEvent());
                     eventBus.post(new DocumentUpdatedEvent());
                     eventBus.post(new RefreshDocumentEvent());
                     leosApplicationEventBus.post(new DocumentUpdatedByCoEditorEvent(user, strDocumentVersionSeriesId, id));
                     documentScreen.scrollTo(elementId);
                 } else {
                	 System.out.println("No se puede convertir este artículo en esta ley, ya que ya se ha modificado en el...");
                	 eventBus.post(new NotificationEvent(Type.INFO,  messageHelper.getMessage("document.importer.amendment.samelaw")));
                 } 
             } else {// EDICION MODIFICACIÓN
            	 idModNew = amendPatternLabelService.extractIdModAmendment(element);     
            	 String quotedStructureId = IdGenerator.generateId("akn_quoted_amend",9);
            	 byte[] newXmlContent = amendPatternLabelService.editingMetadataElementAnalisis(bill, idModNew, quotedStructureId, eli, actionType);
            	 if (newXmlContent != null) { 
                     final String title = messageHelper.getMessage("operation.element.updated", StringUtils.capitalize(elementType));
                     final String description = messageHelper.getMessage("document.amendment.created.edit");
                     final String elementLabel = generateLabel(elementId, bill);
                     final CheckinCommentVO checkinComment = new CheckinCommentVO(title, description, new CheckinElement(ActionType.UPDATED, elementId, elementType, elementLabel));
                     final String checkinCommentJson = CheckinCommentUtil.getJsonObject(checkinComment);
            		 newXmlContent = amendPatternLabelService.elementAmendmentSeveral(newXmlContent, elementId, idModNew, quotedStructureId, elementType, actionType);
            		 billService.updateBill(bill, newXmlContent,checkinCommentJson);
                     changeAmendmentMode(new AmendmentModeCloseEvent());
                     eventBus.post(new DocumentUpdatedEvent());
                     eventBus.post(new RefreshDocumentEvent());
                     leosApplicationEventBus.post(new DocumentUpdatedByCoEditorEvent(user, strDocumentVersionSeriesId, id));
                     documentScreen.scrollTo(elementId);
            	 } else {
            		 eventBus.post(new NotificationEvent(Type.INFO,  messageHelper.getMessage("document.importer.amendment.sameelement")));
            	 }
            	 
             }
         } catch (Exception e) {
             LOG.error("UnexpectedgenerateLabel error occurred while using Add Amendment", e);
             eventBus.post(new NotificationEvent(Type.ERROR, "generic.error.message", e.getMessage()));
		}
         
    }
      
    /**
     * Reloads the document after the embedded milestone explorer is closed
     * Solves LEOS-3923
     * @param windowClosedEvent not used
     */
    @Subscribe
    public void afterClosedWindow(WindowClosedEvent<MilestoneExplorer> windowClosedEvent) {
        eventBus.post(new NavigationRequestEvent(Target.LEGALTEXT, getDocumentRef()));
    }
}