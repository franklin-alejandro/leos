package eu.europa.ec.leos.ui.wizard.document;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.EventBus;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.cantodorado.AGECantoDoradoOrganisms;
import eu.europa.ec.leos.services.support.LeosUtil;
import eu.europa.ec.leos.ui.event.view.collection.AGECreateCantoDoradoRequest;
import eu.europa.ec.leos.web.event.NotificationEvent;
import eu.europa.ec.leos.web.event.NotificationEvent.Type;
import eu.europa.ec.leos.web.model.AGESearchCriteriaCantoDoradoVO;
import eu.europa.ec.leos.web.ui.window.AbstractWindow;

public class AGECreateCantoDoradoWindow extends AbstractWindow {

	private static final long serialVersionUID = -7318940290911926353L;
	private static final Logger LOG = LoggerFactory.getLogger(AGECreateCantoDoradoWindow.class);
	private EventBus eventBus;
	
	private Binder<AGESearchCriteriaCantoDoradoVO> searchCriteriaBinder;
	AGESearchCriteriaCantoDoradoVO searchCriteriaBean;
	NativeSelect<AGECantoDoradoOrganisms> organisms;
	NativeSelect<String> year;
    NativeSelect<String> month;
    NativeSelect<String> day;
    TextField number;
    private Button createBtn;

	public AGECreateCantoDoradoWindow(MessageHelper messageHelper, EventBus eventBus) {
		super(messageHelper, eventBus);
		this.eventBus = eventBus;
		setCaption(messageHelper.getMessage("document.cantodorado.title"));
		prepareWindow();
	}

	public void prepareWindow() {
		setWidth("12.5cm");
		setHeight(45, Unit.PERCENTAGE);
        VerticalLayout windowLayout = new VerticalLayout();
        windowLayout.setSizeFull();
        windowLayout.setMargin(new MarginInfo(false, false, false, true));
        setBodyComponent(windowLayout);
        
        searchCriteriaBinder = new Binder<>();
        
        windowLayout.addComponent(buildLayoutOrganisms());
        windowLayout.addComponent(buildDateLayout());
        windowLayout.addComponent(buildDNumberNormLayout());
        buildCreateButton();

	}
	
	private HorizontalLayout buildLayoutOrganisms () {
		HorizontalLayout organismsLayout = new HorizontalLayout();
		organismsLayout.setWidth("100%");
		organismsLayout.setMargin(new MarginInfo(false, true, true, false));
		
        organisms = new NativeSelect<>(messageHelper.getMessage("document.cantodorado.organism"), Arrays.asList(AGECantoDoradoOrganisms.values()));
        organisms.setEmptySelectionAllowed(false);
        organisms.setRequiredIndicatorVisible(true);
        organisms.setSelectedItem(AGECantoDoradoOrganisms.PRESIDENCIA); //default
        
    	searchCriteriaBinder.forField(organisms)
    	.asRequired(messageHelper.getMessage("document.importer.type.required.error"))
    	.bind(AGESearchCriteriaCantoDoradoVO::getOrganism, AGESearchCriteriaCantoDoradoVO::setOrganism);
    	
    	organismsLayout.addComponent(organisms);
    	
        return organismsLayout;
	}
	
	private HorizontalLayout buildDateLayout() {
    	HorizontalLayout dateLayout = new HorizontalLayout();
    	dateLayout.setWidth("90%");
    	dateLayout.setMargin(new MarginInfo(false, true, true, false));
    	
    	year = new NativeSelect<>(messageHelper.getMessage("document.cantodorado.date"));
    	year.setItems(LeosUtil.getYearsList());
    	year.setWidth("95%");
    	year.setEmptySelectionAllowed(false);
    	year.setRequiredIndicatorVisible(true);
    	year.setSelectedItem(LeosUtil.getYearsList().get(0));
    
    	searchCriteriaBinder.forField(year)
    	.asRequired(messageHelper.getMessage("document.importer.year.required.error"))
    	.bind(AGESearchCriteriaCantoDoradoVO::getYear, AGESearchCriteriaCantoDoradoVO::setYear);

    	month = new NativeSelect<>(messageHelper.getMessage("document.cantodorado.date.month"));
    	month.setItems(LeosUtil.getMonthList());
    	month.setWidth("95%");
    	month.setEmptySelectionAllowed(false);
    	month.setRequiredIndicatorVisible(true);
    	month.setSelectedItem(LeosUtil.getMonthList().get(0));

    	searchCriteriaBinder.forField(month)
    	.asRequired(messageHelper.getMessage("document.importer.year.required.error"))
    	.bind(AGESearchCriteriaCantoDoradoVO::getMonth, AGESearchCriteriaCantoDoradoVO::setMonth);

    	day = new NativeSelect<>(messageHelper.getMessage("document.cantodorado.date.day"));
    	day.setItems(LeosUtil.getDayList());
    	day.setWidth("95%");
    	day.setEmptySelectionAllowed(false);
    	day.setRequiredIndicatorVisible(true);
    	day.setSelectedItem(LeosUtil.getDayList().get(0));

    	searchCriteriaBinder.forField(day)
    	.asRequired(messageHelper.getMessage("document.importer.year.required.error"))
    	.bind(AGESearchCriteriaCantoDoradoVO::getDay, AGESearchCriteriaCantoDoradoVO::setDay);
    	
    	dateLayout.addStyleName("");
    	dateLayout.addComponent(year);
    	dateLayout.addComponent(month);
    	dateLayout.addComponent(day);
    	
    	return dateLayout;
	}
	
	
	private HorizontalLayout buildDNumberNormLayout () {
		HorizontalLayout organismsLayout = new HorizontalLayout();
		organismsLayout.setWidth("100%");
		organismsLayout.setMargin(new MarginInfo(false, true, true, false));
		
    	number = new TextField(messageHelper.getMessage("document.cantodorado.norm"));
    	searchCriteriaBinder.forField(number)
    	.bind(AGESearchCriteriaCantoDoradoVO::getNumber, AGESearchCriteriaCantoDoradoVO::setNumber);
    	
    	organismsLayout.addComponent(number);
    	
    	return organismsLayout;
	}
	
    private void buildCreateButton() {
        createBtn = new Button();
        createBtn.addStyleName("primary");
        createBtn.setCaption(messageHelper.getMessage("document.cantodorado.create"));
        createBtn.setStyleName("primary");
        createBtn.setCaptionAsHtml(true);
        createBtn.setEnabled(true);
        createBtn.setDisableOnClick(true);


        createBtn.addClickListener(event -> {
        	if (searchCriteriaBinder.validate().isOk()) {
        		//String monthS = LeosUtil.getMonthLetter(Integer.parseInt(month.getValue()));
        		String monthS = month.getValue();
        		searchCriteriaBean = new AGESearchCriteriaCantoDoradoVO(organisms.getValue(), year.getValue(), monthS, day.getValue(), number.getValue());
        		searchCriteriaBinder.readBean(searchCriteriaBean);
        		if(!number.isEmpty()) {
        			String regex = "^(\\d+$|\\(d+\\))";
        			Pattern p = Pattern.compile(regex);
        			Matcher m = p.matcher(searchCriteriaBean.getNumber());
        			if (m.find()) {
        				if (searchCriteriaBean.getNumber().length() < 6) {
            				try {
            					searchCriteriaBinder.writeBean(searchCriteriaBean);
            					eventBus.post(new AGECreateCantoDoradoRequest(searchCriteriaBean));
            					createBtn.setData(searchCriteriaBean);
            					close();
            				} catch (ValidationException e) {
            					// TODO Auto-generated catch block
            					e.printStackTrace();
            				}
        				} else {
        					number.focus();
        					event.getButton().setEnabled(true);
            				eventBus.post(new NotificationEvent(Type.INFO, "document.cantodorado.norm.error.length")); 
        				}
        			} else {
        				number.focus();
        				event.getButton().setEnabled(true);
        				eventBus.post(new NotificationEvent(Type.INFO, "document.cantodorado.norm.error.format")); 
        			}
        		} else {
        			number.focus();
        			event.getButton().setEnabled(true);
        			eventBus.post(new NotificationEvent(Type.INFO, "document.cantodorado.norm.error.empty"));
        		}
        	} else {
        		event.getButton().setEnabled(true);
				number.focus();
				eventBus.post(new NotificationEvent(Type.INFO, "document.importer.project.format.error"));
    		}
    	});
        addComponentAtPosition(createBtn, 1);
    }

	@Override
	public void attach() {
		super.attach();
		eventBus.register(this);
	}

	@Override
	public void detach() {
		super.detach();
		eventBus.unregister(this);
	}

}
