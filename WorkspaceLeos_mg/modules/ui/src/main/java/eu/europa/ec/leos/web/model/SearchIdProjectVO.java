package eu.europa.ec.leos.web.model;

public class SearchIdProjectVO {
	
	String  id;

	public SearchIdProjectVO(String id) {
		super();
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
