package eu.europa.ec.leos.ui.view.reportincidence;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.EventBus;
import com.vaadin.data.Binder;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;

import AGE.Remedy.client.RemedyClient;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.AuthenticationInfo;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.CreateInputMap;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.CreateRequestType;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.ReportedSourceType;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.ServiceTypeType;
import HPD_IncidentInterface_Create_WS_AdditionalHeaders.StatusType;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.ui.wizard.WizardStep;
import eu.europa.ec.leos.web.event.NotificationEvent;
import eu.europa.ec.leos.web.event.NotificationEvent.Type;
import eu.europa.ec.leos.web.model.AGEReportIncidenceVO;
import eu.europa.ec.leos.web.ui.window.AbstractWindow;



public class AGECreateReportIncidenceWindow extends AbstractWindow implements WizardStep, Upload.StartedListener,
Upload.FailedListener, Upload.SucceededListener,
Upload.FinishedListener, Upload.ChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = LoggerFactory.getLogger(AGECreateReportIncidenceWindow.class);
	private EventBus eventBus;
    private String user;      
    private String password;
    private String endpoint;	
	private VerticalLayout mainLayout = new VerticalLayout();
	private Binder<AGEReportIncidenceVO> searchCriteriaBinder;
	AGEReportIncidenceVO searchCriteriaBean;
    NativeSelect<String> impact;
    NativeSelect<String> urgency;
    TextArea summary;
    TextArea notes;
    Button createBtn;
    Boolean send = true; 
    private Upload upload;
    private File file;
    // mime types accepted separated by ","
    private static final String MIME_TYPES_ALLOWED = "image/jpeg, image/png";
    // extension types accepted separated by ,
    private static final String EXT_TYPES_ALLOWED = ".jpg, .jpeg, .png";

	public AGECreateReportIncidenceWindow(MessageHelper messageHelper, EventBus eventBus, String user, String password, String endpoint) {
		super(messageHelper, eventBus);
		this.eventBus = eventBus;
		this.user = user;
		this.password = password;
		this.endpoint = endpoint;
		setCaption(messageHelper.getMessage("leos.ui.header.reportIncidence"));

		prepareWindow();
		}

	public void prepareWindow() {
		setWidth("12.5cm");
		setHeight("12.5cm");
		setResponsive(true);
		mainLayout.setSizeFull();
		mainLayout.setResponsive(true);
		mainLayout.setMargin(new MarginInfo(false, false, false, true));
        setBodyComponent(mainLayout);
        
        searchCriteriaBinder = new Binder<>();
        
        mainLayout.addComponent(buildReportIncidenceLayout());
        mainLayout.addComponent(buildReportSummary());
        mainLayout.addComponent(buildReportNotes());
        mainLayout.addComponent(buildLabelNotes());
        mainLayout.addComponent(buildUploadContent());

        buildCreateButton();

	}

	
	private HorizontalLayout buildReportIncidenceLayout () {
		HorizontalLayout impactLayout = new HorizontalLayout();
		impactLayout.setWidth("100%");
		impactLayout.setMargin(new MarginInfo(false, true, false, false));
		impactLayout.setResponsive(true);
		
		ArrayList<String> impacts = new ArrayList<String>();
		impacts.add(messageHelper.getMessage("leos.ui.view.reportIncidence.impact.extensive"));
		impacts.add(messageHelper.getMessage("leos.ui.view.reportIncidence.impact.significant"));
		impacts.add(messageHelper.getMessage("leos.ui.view.reportIncidence.impact.moderate"));
		impacts.add(messageHelper.getMessage("leos.ui.view.reportIncidence.impact.minor"));
		
		ArrayList<String> urgencys = new ArrayList<String>();
		urgencys.add(messageHelper.getMessage("leos.ui.view.reportIncidence.urgency.critical"));
		urgencys.add(messageHelper.getMessage("leos.ui.view.reportIncidence.urgency.hignt"));
		urgencys.add(messageHelper.getMessage("leos.ui.view.reportIncidence.urgency.medium"));
		urgencys.add(messageHelper.getMessage("leos.ui.view.reportIncidence.urgency.low"));
		
    	impact = new NativeSelect<>(messageHelper.getMessage("leos.ui.view.reportIncidence.impact"));
    	impact.setItems(impacts);
    	impact.setResponsive(true);
    	impact.setWidth("95%");
    	impact.setEmptySelectionAllowed(false);
    	impact.setRequiredIndicatorVisible(true);
    	impact.setSelectedItem(impacts.get(3));
    	searchCriteriaBinder.forField(impact)
    	.bind(AGEReportIncidenceVO::getImpact, AGEReportIncidenceVO::setImpact);
    	
       	urgency = new NativeSelect<>(messageHelper.getMessage("leos.ui.view.reportIncidence.urgency"));
       	urgency.setItems(urgencys);
       	urgency.setWidth("95%");
       	urgency.setResponsive(true);
       	urgency.setEmptySelectionAllowed(false);
       	urgency.setRequiredIndicatorVisible(true);
       	urgency.setSelectedItem(urgencys.get(3));
    	searchCriteriaBinder.forField(urgency)
    	.bind(AGEReportIncidenceVO::getUrgency, AGEReportIncidenceVO::setUrgency);
     	
    	impactLayout.addComponent(impact);
    	impactLayout.addComponent(urgency);

    	return impactLayout;
	}
	
	private HorizontalLayout buildReportSummary() {
		HorizontalLayout impactLayout = new HorizontalLayout();
		impactLayout.setResponsive(true);
		impactLayout.setWidth("100%");
		impactLayout.setMargin(new MarginInfo(false, true, false, false));
		
    	summary = new TextArea(messageHelper.getMessage("leos.ui.view.reportIncidence.summary"));
    	summary.setRows(2);
    	summary.setMaxLength(100);
    	summary.setWidth("100%");
    	summary.setRequiredIndicatorVisible(true);
    	summary.setValue("PRUEBAS INTEGRACION LEOS");
    	summary.setResponsive(true);
    	searchCriteriaBinder.forField(summary)
    	.bind(AGEReportIncidenceVO::getSummary, AGEReportIncidenceVO::setSummary);   	
    
    	impactLayout.addComponent(summary);

    	return impactLayout;
	}
	private HorizontalLayout buildReportNotes() {
		HorizontalLayout impactLayout = new HorizontalLayout();
		impactLayout.setWidth("100%");
		impactLayout.setMargin(new MarginInfo(false, true, true, false));
		impactLayout.setResponsive(true);
    	notes = new TextArea(messageHelper.getMessage("leos.ui.view.reportIncidence.notes"));
    	notes.setWidth("100%");
    	notes.setMaxLength(500);
    	notes.setRows(4);
    	notes.setRequiredIndicatorVisible(true);
    	notes.setResponsive(true);
    	searchCriteriaBinder.forField(notes)
    	.bind(AGEReportIncidenceVO::getNotes, AGEReportIncidenceVO::setNotes);
    	   	
    	impactLayout.addComponent(notes);
	
    	return impactLayout;
	}
	
	private HorizontalLayout buildLabelNotes() {
		HorizontalLayout impactLayout = new HorizontalLayout();
		impactLayout.setWidth("100%");
		impactLayout.setMargin(new MarginInfo(true, true, false, false));
		impactLayout.setResponsive(true);
	    Label fileSelect = new Label();
	    fileSelect.setCaption(messageHelper.getMessage("leos.ui.view.reportIncidence.upload.caption"));
	    fileSelect.setCaptionAsHtml(true);
	    fileSelect.setResponsive(true);   	
    	impactLayout.addComponent(fileSelect);
 	
    	return impactLayout;
	}
	
    private void buildCreateButton() {
        createBtn = new Button();
        createBtn.addStyleName("primary");
        createBtn.setCaption(messageHelper.getMessage("leos.ui.view.reportIncidence.send"));
        createBtn.setCaptionAsHtml(true);
        createBtn.setResponsive(true);   	
        createBtn.setEnabled(true);
        createBtn.addClickListener(event -> {
        	upload.submitUpload();
        });

        addComponentAtPosition(createBtn, 1);       
    }
    

    private VerticalLayout buildUploadContent() {
    	VerticalLayout uploadLayout = new VerticalLayout();
        uploadLayout.setMargin(false);
        uploadLayout.setResponsive(true);   	
        upload = new Upload();
        upload.setButtonCaption(null);
        upload.setResponsive(true);   	
        upload.setReceiver(
                new Upload.Receiver() {

					private static final long serialVersionUID = 1L;

					@Override
                    public OutputStream receiveUpload(String filename,
                            String mimeType) {
                        OutputStream outputFile;
                        try {
                        	if("".equals(filename)) {
                        		filename = "temp";
                        	}
                            file = File.createTempFile(filename,
                                    filename.contains(".") ? filename.substring(filename.lastIndexOf("."), filename.length()) : "");
                            if (!file.exists()) {
                                file.createNewFile();
                            }
                            outputFile = new FileOutputStream(file);
                        } catch (Exception e) {
                            LOG.error("Error uploading file", e);
                            return null;
                        }
                        return outputFile;
                    }
                }

        );
        
        upload.addChangeListener(this);
        upload.addStartedListener(this);
        upload.addFailedListener(this);
        upload.addFinishedListener(this);
        upload.addSucceededListener(this);
        upload.setImmediateMode(false);
        JavaScript.getCurrent().execute("document.querySelectorAll('.upload-wizard .gwt-FileUpload')[0].setAttribute('accept', '" + EXT_TYPES_ALLOWED + "')");
       
        uploadLayout.addComponent(upload);

		return uploadLayout;
    }
    
	@Override
	public void attach() {
		super.attach();
		eventBus.register(this);
	}

	@Override
	public void detach() {
		super.detach();
		eventBus.unregister(this);
	}


    @Override
    public boolean canFinish() {
        return false;
    }

    @Override
    public void filenameChanged(Upload.ChangeEvent event) {
    }

    @Override
    public void uploadStarted(final Upload.StartedEvent event) {
        String contentType = event.getMIMEType();
        String extensionType = event.getFilename().contains(".")
                ? event.getFilename().substring(event.getFilename().lastIndexOf("."), event.getFilename().length()).toLowerCase()
                : "";

    	if (searchCriteriaBinder.validate().isOk()) {
    		searchCriteriaBean = new AGEReportIncidenceVO(impact.getValue(), urgency.getValue(), summary.getValue(), notes.getValue());
    		searchCriteriaBinder.readBean(searchCriteriaBean);
    		if(summary.getValue().isEmpty()) {
    			send = false;
    			summary.focus();
    			eventBus.post(new NotificationEvent(Type.INFO, "leos.ui.view.reportIncidence.summary.error.empty"));        		
    		}else if(summary.getValue().length()<10) {
    			send = false;
    			summary.focus();
    			eventBus.post(new NotificationEvent(Type.INFO, "leos.ui.view.reportIncidence.summary.error.length"));        		
    		}else if(notes.getValue().isEmpty()) {
    			send = false;
    			notes.focus();
    			eventBus.post(new NotificationEvent(Type.INFO, "leos.ui.view.reportIncidence.notes.error.empty"));        		
    		}else if(notes.getValue().length()<10) {
    			send = false;
    			notes.focus();
    			eventBus.post(new NotificationEvent(Type.INFO, "leos.ui.view.reportIncidence.notes.error.length"));        		
    		}else if ((!MIME_TYPES_ALLOWED.contains(contentType) ||
                    !EXT_TYPES_ALLOWED.contains(extensionType)) && !extensionType.equals("")) {
            	send = false;
    			eventBus.post(new NotificationEvent(Type.ERROR, "leos.ui.view.reportIncidence.upload.error.type", EXT_TYPES_ALLOWED));        		
            }else {
    			send = true;
    		}
    	}
    }

    @Override
    public void uploadSucceeded(final Upload.SucceededEvent event) {
    		try {
    
	    		if(send) {
		    		byte[] fileContent;
		    	 
					fileContent = Base64.getEncoder().encode(Files.readAllBytes(file.toPath()));
					Long longitud = event.getLength();
					Integer longInt = longitud.intValue();
			
					RemedyClient remedyClient = new RemedyClient();
					AuthenticationInfo parameters = new AuthenticationInfo();
					parameters.setUserName(user);
					parameters.setPassword(password);
					
					CreateInputMap inputMap = new CreateInputMap();
					inputMap.setAction("CREATE");
					inputMap.setAssignedGroup("APLICACIONES");
					inputMap.setAssignedGroupShiftName(null);
					inputMap.setAssignedSupportCompany("MINISTERIO DE LA PRESIDENCIA");
					inputMap.setAssignedSupportOrganization("Soporte TI");
					inputMap.setAssignee("Soporte LEOS Incidencias");
					inputMap.setCategorizationTier1("Aplicaciones");
					inputMap.setCategorizationTier2("Incidencia en Aplicación");
					inputMap.setCategorizationTier3("LEOS");
					inputMap.setCIName(null);
					inputMap.setClosureManufacturer(null);
					inputMap.setClosureProductCategoryTier1("SERVICIO");
					inputMap.setClosureProductCategoryTier2("Final");
					inputMap.setClosureProductCategoryTier3(null);
					inputMap.setClosureProductModelVersion(null);
					inputMap.setClosureProductName(null);
					inputMap.setCorporateID(null);
					inputMap.setCreateRequest(null);
					inputMap.setCustomerCompany(null);
					inputMap.setDepartment(null);
					inputMap.setDirectContactFirstName(null);
					inputMap.setDirectContactLastName(null);
					inputMap.setDirectContactMiddleInitial(null);
					inputMap.setFirstName("ext_remedy01");
					inputMap.setHPDCI(null);
					inputMap.setHPDCIFormName(null);
					inputMap.setHPDCIReconID(null);
					inputMap.setImpact(translateImpact(impact.getValue()));
					inputMap.setLastName("ext_remedy01");
					inputMap.setLoginID("ext_remedy01");
					inputMap.setLookupKeyword(null);
					inputMap.setMiddleInitial(null);
					inputMap.setNotes(notes.getValue());
					inputMap.setProductCategorizationTier1("SERVICIO");
					inputMap.setProductCategorizationTier2("Final");
					inputMap.setProductCategorizationTier3("LEOS");
					inputMap.setProductModelVersion(null);
					inputMap.setProductName(null);
					inputMap.setReportedSource(ReportedSourceType.EXTERNAL_ESCALATION);
					inputMap.setResolution(null);
					inputMap.setResolutionCategoryTier1(null);
					inputMap.setResolutionCategoryTier2(null);
					inputMap.setResolutionCategoryTier3(null);
					inputMap.setServiceCI("LEOS");
					inputMap.setServiceCIReconID(null);
					inputMap.setServiceType(ServiceTypeType.USER_SERVICE_RESTORATION);
					inputMap.setStatus(StatusType.NEW);
					inputMap.setStatusReason(null);
					inputMap.setSummary(summary.getValue());
					inputMap.setTemplateID(null);
					inputMap.setUrgency(translateUrgency(urgency.getValue()));
					inputMap.setWorkInfoAttachment1Data(fileContent);
					inputMap.setWorkInfoAttachment1Name(event.getFilename());
					inputMap.setWorkInfoAttachment1OrigSize(longInt);
					inputMap.setWorkInfoDate(null);
					inputMap.setWorkInfoLocked(CreateRequestType.NO);
					inputMap.setWorkInfoNotes("Adjunto");
					inputMap.setWorkInfoSource(null);
					inputMap.setWorkInfoSummary("LEOS");
					inputMap.setWorkInfoType(null);
					inputMap.setWorkInfoViewAccess(null);
				
					String respuesta = remedyClient.helpDeskSubmitService(inputMap, parameters, endpoint);
					if(respuesta!=null) {
						LOG.info("Se ha enviado la incidencia a Remedy correctamente: "+ respuesta);
			  			eventBus.post(new NotificationEvent(Type.INFO, "leos.ui.view.reportIncidence.ok", respuesta));   	
			  			close();
					}else {
						  LOG.error("Error sending incidence at Remedy");
						  eventBus.post(new NotificationEvent(Type.ERROR, "leos.ui.view.reportIncidence.error")); 
					}
				
				}
			
			} catch (Exception e) {
				  LOG.error("Error sending incidence at Remedy", e);
				  eventBus.post(new NotificationEvent(Type.ERROR, "leos.ui.view.reportIncidence.error")); 
			}
		
    }

    @Override
    public void uploadFailed(final Upload.FailedEvent event) {
    	
    	LOG.error("Error uploading file", event.getReason());
    	eventBus.post(new NotificationEvent(Type.ERROR, "leos.ui.view.reportIncidence.error"));    
    	
    }

    @Override
    public void uploadFinished(final Upload.FinishedEvent event) {
    }


    @Override
    public boolean validateState() {
        return true;
    }


	@Override
	public String getStepTitle() {
	    return messageHelper.getMessage("wizard.document.upload.template.title");
	}
	
	@Override
	public String getStepDescription() {
	    return messageHelper.getMessage("wizard.document.upload.template.desc");
	}

	@Override
	public Component getComponent() {
		return null;
	}
	
	private String translateUrgency(String impact) {
		String result = "";
		
		if(impact.equals("Low") || (impact.equals("Baja"))){
			result = "4-Low";
		}else 	if(impact.equals("Medium") || (impact.equals("Media"))){
			result = "3-Medium";
		}else 	if(impact.equals("High") || (impact.equals("Alta"))){
			result = "2-High";
		}else 	if(impact.equals("Critical") || (impact.equals("Crítica"))){
			result = "1-Critical";	
		}
		
		return result;
	}
	
	private String translateImpact(String impact) {
		String result = "";
		
		if(impact.equals("Extensive/Widespread") || (impact.equals("Extenso/Amplio"))){
			result = "1-Extensive/Widespread";
		}else 	if(impact.equals("Significant/Large") || (impact.equals("Significativo/Grande"))){
			result = "2-Significant/Large";
		}else 	if(impact.equals("Moderate/Limited") || (impact.equals("Moderado/Limitado"))){
			result = "3-Moderate/Limited";
		}else 	if(impact.equals("Minor/Localized") || (impact.equals("Menor/Localizado"))){
			result = "4-Minor/Localized";	
		}
		
		
		return result;
	}
  	
}
