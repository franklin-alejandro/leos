package eu.europa.ec.leos.ui.event.view.collection;

import eu.europa.ec.leos.domain.vo.DocumentVO;

public class DeleteReportRequest {
	
    private DocumentVO report;

    public DeleteReportRequest(DocumentVO report) {
        this.report = report;
    }

    public DocumentVO getReport(){
        return report;
    }

}
