package eu.europa.ec.leos.ui.event.view;

import eu.europa.ec.leos.model.cantodorado.AGECantoDoradoStructureType;

public class AGECantoDoradoStructureChangeEvent {

	private AGECantoDoradoStructureType structureType;

    public AGECantoDoradoStructureChangeEvent(AGECantoDoradoStructureType structureType) {
        super();
        this.structureType = structureType;
    }

    public AGECantoDoradoStructureType getStructureType() {
        return structureType;
    }
}

