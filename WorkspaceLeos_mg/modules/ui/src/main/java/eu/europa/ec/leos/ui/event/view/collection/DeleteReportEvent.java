package eu.europa.ec.leos.ui.event.view.collection;

import eu.europa.ec.leos.domain.vo.DocumentVO;

public class DeleteReportEvent {
    private DocumentVO report;

    public DeleteReportEvent(DocumentVO report) {
        this.report = report;
    }

    public DocumentVO getReport(){
        return report;
    }
}
