//Evolutivo #2678
package eu.europa.ec.leos.ui.component.amendment;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

import eu.europa.ec.leos.amend.AmendActDetails;
import eu.europa.ec.leos.amend.AmendAction;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.services.support.LeosUtil;
import eu.europa.ec.leos.ui.event.ScrollToEvent;
import eu.europa.ec.leos.ui.event.amendment.AmendmentAddRequest;
import eu.europa.ec.leos.ui.event.amendment.AmendmentDoSearchEvent;
import eu.europa.ec.leos.ui.event.amendment.AmendmentElementModificationRequest;
import eu.europa.ec.leos.ui.event.amendment.AmendmentElementsPreviewRequest;
import eu.europa.ec.leos.ui.event.amendment.AmendmentModeCloseEvent;
import eu.europa.ec.leos.ui.event.amendment.AmendmentModeExtensionEvent;
import eu.europa.ec.leos.ui.extension.ImportAmendingActExtension;
import eu.europa.ec.leos.ui.view.LayoutModeType;
import eu.europa.ec.leos.web.event.NotificationEvent;
import eu.europa.ec.leos.web.event.NotificationEvent.Type;
import eu.europa.ec.leos.web.event.component.LayoutModeEvent;
import eu.europa.ec.leos.web.event.view.document.HandleAmendingActResponseEvent;
import eu.europa.ec.leos.web.event.view.document.SearchActBOERequestEvent;
import eu.europa.ec.leos.web.event.view.document.SearchAmendingActResponseEvent;
import eu.europa.ec.leos.web.event.view.document.SelectedElementAmendmentResponseEvent;
import eu.europa.ec.leos.model.oj.DocTypeSpain;
import eu.europa.ec.leos.web.model.SearchCriteriaBOEVO;
import eu.europa.ec.leos.web.model.SearchCriteriaBOEVO.Accion;
import eu.europa.ec.leos.web.ui.component.ContentPane;

@ViewScope
@SpringComponent
@DesignRoot("AmendActDesign.html")
public class AmendmentEditorComponent extends VerticalLayout implements ContentPane {
	private static final long serialVersionUID = 1L;
    
    private static final String ARTICLE = "article";
    private static final String PROVISO = "proviso";
    private static final String HEADING = "heading";
    private static final String PARAGRAPH = "paragraph";
    private static final String POINT = "point";
    private static final String LIST = "list";
    
    private EventBus eventBus;
    private MessageHelper messageHelper;
    
    private HorizontalLayout firstLayout;
    private HorizontalLayout searchType;
    private HorizontalLayout searchReference;
    private HorizontalLayout labelSearchResultLayout;
    private HorizontalLayout searchResultLayout;
    private HorizontalLayout buttonActionsLayout;
    private HorizontalLayout previewLayout;
    private HorizontalLayout labelActionsButtonsLayout;
    private HorizontalLayout labelPreviewButtonLayout;
    private HorizontalLayout searchCheckBox;
    
    private Label labelFirst;
    private Label labelTypeInput;
    private Label labelYearInput;
    private Label labelMonthInput;
    private Label labelDayInput;
    private Label labelNrInput;
    //private Label labelInfoSearch;
    private Label labelResultContent;
    private Label labelActionsButtons;
    private Label labelPreviewButton;
    private Label labelPreview;
    
    private NativeSelect<DocTypeSpain>  typeInput;
    private NativeSelect<String> yearInput;
    private NativeSelect<String> monthInput;
    private NativeSelect<String> dayInput;
    private TextField nrInput; 
    private TextField reference;
    private Button searchButton;

    private Label searchResultContent;    
    private Binder<SearchCriteriaBOEVO> searchCriteriaBinder;
    SearchCriteriaBOEVO searchCriteriaBeanBO;
    private Button insertUpButton; 
    private Button insertDownButton;  
    private Button replaceButton;   
    private Button deleteButton;    
    private Label previewModificationElement;  
    private Button addAmendmentButton;   
    private Button cancelButton;      
    private String elementId;   
    private String articleNum; 
    private String typeElement;
    private AmendActDetails amendActDetails;
	private String regex="";
	private List<String> elementAmendmentSelect;
	private String parentElement;
	private String title="";
	private String eli="";
    private String normType = "";
    private String actionType = "";
    private boolean isEditing = false;
    
    @Autowired
    public AmendmentEditorComponent (final EventBus eventBus, final MessageHelper messageHelper) {
    	this.eventBus = eventBus;
    	this.messageHelper = messageHelper;

    	Design.read(this);
    	initView();
    	initExtensions();
    	
    }
    
    private void initExtensions() {
    	new ImportAmendingActExtension(eventBus, searchResultContent);
    }
    
    private void initView() {
    	setCaption(messageHelper.getMessage("document.importer.boe.window.title"));
    	enabledInputFields(true);
    	enabledActonsButtons(false);
    	enablePreviewButtons();
    	
    	buildSearch();
    	buildResultContent();
    	buildActionButtons();
    	buildPreview();
    }
    
    @Subscribe
    public void initSearch(AmendmentDoSearchEvent event) {
		if(!searchCriteriaBeanBO.isEmptySearchCriteria() && amendActDetails != null) {
			isEditing = true;
            doSearch(null);
            searchButton.setEnabled(false);
        } else {
        	isEditing = false;
            searchButton.setEnabled(true);
        }
    }

    
    private void buildPreview () {
    	labelPreview.setCaption(messageHelper.getMessage("document.importer.amendment.previewlabel.caption"));
    	labelPreviewButton.setCaption(messageHelper.getMessage("document.importer.amendment.labelpreviewbutton.caption"));
    	addAmendmentButton.setCaption(messageHelper.getMessage("document.importer.amendment.addamendmentbutton.caption"));
    	cancelButton.setCaption(messageHelper.getMessage("document.importer.amendment.cancelbutton.caption"));

    	previewModificationElement.setContentMode(ContentMode.HTML);
    	previewModificationElement.setStyleName("leos-import-content-preview-amendment");
    	addAmendmentButton.addClickListener(event -> addAmendment()); 
    	cancelButton.addClickListener(event -> close()); 
    }
    
    private void buildActionButtons () {
    	labelActionsButtons.setCaption(messageHelper.getMessage("document.importer.amendment.labelactionsbuttons.caption"));
    	insertUpButton.setCaption(messageHelper.getMessage("document.importer.amendment.insertupbutton.caption"));
    	//insertUpButton.setIcon(new ThemeResource("icons/user.png"));
    	insertUpButton.setIcon(FontAwesome.ARROW_CIRCLE_UP);
    	insertDownButton.setCaption(messageHelper.getMessage("document.importer.amendment.insertdownbutton.caption"));
    	insertDownButton.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);
    	replaceButton.setCaption(messageHelper.getMessage("document.importer.amendment.replacebutton.caption"));
    	deleteButton.setCaption(messageHelper.getMessage("document.importer.amendment.deletebutton.caption"));
  	     
    	insertUpButton.addClickListener(event -> amendmentElementModification(AmendAction.INSERT_TOP, elementAmendmentSelect));
    	insertDownButton.addClickListener(event -> amendmentElementModification(AmendAction.INSERT_BOTTOM, elementAmendmentSelect));
    	replaceButton.addClickListener(event ->  amendmentElementModification (AmendAction.REPLACE, elementAmendmentSelect));
    	deleteButton.addClickListener(event -> amendmentElementModification(AmendAction.DELETE, elementAmendmentSelect));
    }

	private void amendmentElementModification(AmendAction action, List<String> elementAmendmentSelect) {
		actionType = action.name();
		eventBus.post(new  AmendmentElementModificationRequest(action, elementAmendmentSelect, elementId, title, eli, articleNum, normType, typeElement, isEditing));
	}

	private void buildResultContent () {
    	labelResultContent.setValue(messageHelper.getMessage("document.importer.amendment.labelloadcontent.caption"));
    	searchResultContent.setContentMode(ContentMode.HTML);
    	searchResultContent.setSizeFull();
    	searchResultContent.setStyleName("leos-import-content-amendment");
    }
    
    private void buildSearch () {
    	labelFirst.setValue(messageHelper.getMessage("document.importer.amendment.labelfirst.caption"));
    	labelTypeInput.setValue(messageHelper.getMessage("document.importer.amendment.labeltypeinput.caption"));
    	typeInput.setItems(Arrays.asList(DocTypeSpain.values()));
    	typeInput.setEmptySelectionAllowed(false);
    	typeInput.setRequiredIndicatorVisible(true);
    	typeInput.setSelectedItem(SearchCriteriaBOEVO.DEFAULT_DOCTYPE); //default
    	typeInput.setStyleName("leos-select-amendment");

    	labelYearInput.setValue(messageHelper.getMessage("document.importer.amendment.labelyearinput.caption"));
    	yearInput.setItems(LeosUtil.getYearsList());
        yearInput.setEmptySelectionAllowed(false);
        yearInput.setRequiredIndicatorVisible(true);
        yearInput.setSelectedItem(LeosUtil.getYearsList().get(0));

    	labelMonthInput.setValue(messageHelper.getMessage("document.importer.amendment.labelmonthinput.caption"));
    	monthInput.setItems(LeosUtil.getMonthNumber());
    	monthInput.setEmptySelectionAllowed(false);
    	monthInput.setRequiredIndicatorVisible(true);
    	monthInput.setSelectedItem(LeosUtil.getMonthNumber().get(0)); //default

    	labelDayInput.setValue(messageHelper.getMessage("document.importer.amendment.labeldayinput.caption"));
    	dayInput.setItems(LeosUtil.getDayList());
    	dayInput.setEmptySelectionAllowed(false);
    	dayInput.setRequiredIndicatorVisible(true);
    	dayInput.setSelectedItem(LeosUtil.getDayList().get(0)); //default

        labelNrInput.setValue(messageHelper.getMessage("document.importer.amendment.labelnrinput.caption"));
        
        CheckBox checkboxIdentificador;
        checkboxIdentificador = new CheckBox(messageHelper.getMessage("document.importer.amendment.checkbox.caption"));
    	checkboxIdentificador.setValue(false);
    	checkboxIdentificador.setId("checkIdentificador");

		checkboxIdentificador.addValueChangeListener(event -> // Java 8
		activateCheck(checkboxIdentificador.getValue())
    	);
				
		searchCheckBox.addComponent(checkboxIdentificador);
		searchCheckBox.setComponentAlignment(checkboxIdentificador, Alignment.MIDDLE_LEFT);

    	reference.setPlaceholder("BOE-A-XXXX-XXXXX");
        
        searchCriteriaBeanBO = new SearchCriteriaBOEVO(typeInput.getValue(), yearInput.getValue(), monthInput.getValue(), dayInput.getValue(), nrInput.getValue(), reference.getValue().toUpperCase(), Accion.AMENDMENT);
        createSearchBinder();

        searchButton.setCaption(messageHelper.getMessage("document.importer.amendment.searchbutton.caption"));
        searchButton.setDisableOnClick(true);

        searchButton.addClickListener(event ->  doSearch(checkboxIdentificador));
    }
    
    
    private void createSearchBinder() {
    	searchCriteriaBinder = new Binder<>();
        searchCriteriaBinder.forField(typeInput)
        .asRequired(messageHelper.getMessage("document.importer.type.required.error"))
        .bind(SearchCriteriaBOEVO::getType, SearchCriteriaBOEVO::setType);
        searchCriteriaBinder.forField(yearInput)
        .asRequired(messageHelper.getMessage("document.importer.year.required.error"))
        .bind(SearchCriteriaBOEVO::getYear, SearchCriteriaBOEVO::setYear);
    	searchCriteriaBinder.forField(monthInput)
    	.asRequired(messageHelper.getMessage("document.importer.year.required.error"))
    	.bind(SearchCriteriaBOEVO::getMonth, SearchCriteriaBOEVO::setMonth);
    	searchCriteriaBinder.forField(dayInput)
    	.asRequired(messageHelper.getMessage("document.importer.year.required.error"))
    	.bind(SearchCriteriaBOEVO::getDay, SearchCriteriaBOEVO::setDay);
        searchCriteriaBinder.forField(nrInput)
        .bind(SearchCriteriaBOEVO::getNumber, SearchCriteriaBOEVO::setNumber);
    	searchCriteriaBinder.forField(reference)
    	.bind(SearchCriteriaBOEVO::getReference, SearchCriteriaBOEVO::setReference);
    	reference.setEnabled(false);
    }
    
    private void doSearch(CheckBox checkboxIdentificador) {
    	if (searchCriteriaBinder.validate().isOk()) {
			try {
				searchCriteriaBeanBO = new SearchCriteriaBOEVO(typeInput.getValue(), yearInput.getValue(), monthInput.getValue(), dayInput.getValue(), nrInput.getValue(), reference.getValue().toUpperCase(), Accion.AMENDMENT);
		    	searchCriteriaBinder.readBean(searchCriteriaBeanBO);
				if (checkboxIdentificador!= null && checkboxIdentificador.isEmpty() && nrInput.isEmpty()) {
					nrInput.focus();
					eventBus.post(new NotificationEvent(Type.INFO, "document.importer.project.format.error"));
				} else if (checkboxIdentificador == null || checkboxIdentificador.isEmpty() && !searchCriteriaBeanBO.getNumber().isEmpty()) {
					regex = "^(\\d+$|\\(d+\\))";
					Pattern p = Pattern.compile(regex);
					Matcher m = p.matcher(searchCriteriaBeanBO.getNumber());
					if (m.find()) {
	                    insertUpButton.setData(searchCriteriaBeanBO);
	                    insertDownButton.setData(searchCriteriaBeanBO); 
	                    replaceButton.setData(searchCriteriaBeanBO); 
	                    deleteButton.setData(searchCriteriaBeanBO);
						searchCriteriaBinder.writeBean(searchCriteriaBeanBO);
						eventBus.post(new SearchActBOERequestEvent(searchCriteriaBeanBO));

					} else {
						reference.focus();
						eventBus.post(new NotificationEvent(Type.INFO, "document.importer.project.format.error")); 
					}
				}else if (!checkboxIdentificador.isEmpty() && searchCriteriaBeanBO.getReference().isEmpty()) {
					reference.focus();
					eventBus.post(new NotificationEvent(Type.INFO, "document.importer.project.format.error"));
				} else if  (!checkboxIdentificador.isEmpty() && !searchCriteriaBeanBO.getReference().isEmpty()) { 
					regex = "BOE-A-\\d{4}-\\d{1,5}";
					Pattern p = Pattern.compile(regex);
					Matcher m = p.matcher(searchCriteriaBeanBO.getReference().toUpperCase());
					if (m.find()) {
						searchCriteriaBinder.writeBean(searchCriteriaBeanBO);
	                    insertUpButton.setData(searchCriteriaBeanBO);
	                    insertDownButton.setData(searchCriteriaBeanBO); 
	                    replaceButton.setData(searchCriteriaBeanBO); 
	                    deleteButton.setData(searchCriteriaBeanBO);
	                    eventBus.post(new SearchActBOERequestEvent(searchCriteriaBeanBO));
					} else {
						reference.focus();
						eventBus.post(new NotificationEvent(Type.INFO, "document.importer.boe.window.reference.error")); 
					}
				}
				searchButton.setEnabled(true);
				reset();	
			} catch (ValidationException e) {
				e.printStackTrace();
			}
		} else {
			searchButton.setEnabled(true);
		}
	}
    
    
    public void populateAmendmentView(AmendActDetails amendActDetails, String articleNum, String articleId, String typeElement) {
    	this.elementId = articleId;
    	this.articleNum = articleNum;
    	this.typeElement = typeElement;
    	this.amendActDetails = amendActDetails;
    	if (amendActDetails != null) { //edit mode
    		searchCriteriaBeanBO.setType(amendActDetails.getType());
    		searchCriteriaBeanBO.setDay(String.valueOf(amendActDetails.getDay()));
    		searchCriteriaBeanBO.setMonth(String.valueOf(amendActDetails.getMonth()));
    		searchCriteriaBeanBO.setYear(String.valueOf(amendActDetails.getYear()));
    		searchCriteriaBeanBO.setNumber(String.valueOf(amendActDetails.getNumber()));
    		
    		typeInput.setValue(searchCriteriaBeanBO.getType());
    		yearInput.setValue(searchCriteriaBeanBO.getYear());
    		monthInput.setValue(searchCriteriaBeanBO.getMonth());
    		dayInput.setValue(searchCriteriaBeanBO.getDay());
    		nrInput.setValue(searchCriteriaBeanBO.getNumber());
    		disableInputs();	
    	}
    }
    
    private void activateCheck (boolean marcado) {
    	if (marcado) {
    		typeInput.setEnabled(false);
    		yearInput.setEnabled(false);
    		monthInput.setEnabled(false);
    		dayInput.setEnabled(false);
    		nrInput.setEnabled(false);
    		nrInput.setValue("");
    		reference.setEnabled(true);
    		reference.focus();
    	} else {
    		typeInput.setEnabled(true);
    		yearInput.setEnabled(true);
    		monthInput.setEnabled(true);
    		dayInput.setEnabled(true);
    		nrInput.setEnabled(true);
    		reference.setEnabled(false);
    		reference.setValue("");
    		nrInput.focus();
    	}
    }
    
    private void disableInputs () {
    	searchType.setEnabled(false);
    	searchReference.setEnabled(false);
    }
	
    private void enabledInputFields(boolean enable) { 
    	typeInput.setEnabled(enable);    
    	yearInput.setEnabled(enable);    
    	nrInput.setEnabled(enable);    
    }
    
    private void enabledActonsButtons(boolean enable) {
    	insertUpButton.setEnabled(enable);
    	insertDownButton.setEnabled(enable);
    	replaceButton.setEnabled(enable);
    	deleteButton.setEnabled(enable);  	
    }
    
    private void enablePreviewButtons() {
    	addAmendmentButton.setEnabled(false);
    	cancelButton.setEnabled(true);
    }

    @Subscribe
    public void searchActResponse(SearchAmendingActResponseEvent event) { 
    	String document = event.getDocument();
    	if (document != null) {
        	String[] aux= document.split(":::");
        	title = aux[0];
        	eli = aux[2];
        	normType = aux[3];
        	searchResultContent.setValue(aux[1]);    
    	} else {
    		searchResultContent.setValue("");   
    	}
    	eventBus.post(new HandleAmendingActResponseEvent());
    }
  
    @Subscribe    
    public void handleSelectionChange(SelectedElementAmendmentResponseEvent event) {
    	elementAmendmentSelect = event.getElementSelect() ;
    	parentElement = event.getParentElement();
    	if (elementAmendmentSelect.isEmpty()) {
    		previewModificationElement.setValue( "");
    		enableActionButtons(!elementAmendmentSelect.isEmpty());
    	} else if (elementAmendmentSelect.size() == 1) {
        	String element = elementAmendmentSelect.get(0);
        	String [] elements = element.split("--");
        	String type = elements[0];
    		activateActionsOneSelection(type);
    		eventBus.post(new AmendmentElementsPreviewRequest(elementAmendmentSelect, parentElement));
    	} else if (elementAmendmentSelect.size() > 1) {
    		
    		eventBus.post(new AmendmentElementsPreviewRequest(elementAmendmentSelect, parentElement));
    	}
    }
    
    public void setAmendModificationElement (byte[] modificationElement) {
    	String modification = new String(modificationElement, StandardCharsets.UTF_8);
    	previewModificationElement.setValue(modification);	
    }
    
    private void activateActionsOneSelection (String type) {
		switch (type) {
		case ARTICLE:
		case PROVISO:
		case PARAGRAPH:
		case LIST:
		case POINT:
			enableActionButtons(true);
			break;
		case HEADING:
			insertUpButton.setEnabled(false);
			insertDownButton.setEnabled(false);
			replaceButton.setEnabled(true);
			deleteButton.setEnabled(false);
			break;
		default:
			break;
		}
    }
  
    private void addAmendment() {
    	String elementAmendment = previewModificationElement.getValue();
    	elementAmendment = elementAmendment.replace("&nbsp;", " ");
    	//nameNorm, quitar el campo no usado
    	eventBus.post(new AmendmentAddRequest(elementId, elementAmendment, eli, title, actionType, typeElement, isEditing));
    }     
    
    private void enableActionButtons(boolean enable) { 
    	insertUpButton.setEnabled(enable);        
    	insertDownButton.setEnabled(enable);        
    	replaceButton.setEnabled(enable);        
    	deleteButton.setEnabled(enable);   
    }
    private void reset () {   
    	enableActionButtons(false); 
    	previewModificationElement.setValue("");       
    	addAmendmentButton.setEnabled(false);    
    }
    
   
    public void close () {
        eventBus.post(new AmendmentModeCloseEvent());
        eventBus.post(new AmendmentModeExtensionEvent(false, elementId));
        eventBus.post(new ScrollToEvent(elementId));
    	
    }

    public void showAmendmentViewLabelPreview (String content) {
    	addAmendmentButton.setEnabled(false);
    	previewModificationElement.setValue(content);
    }
    
    public void activatedAddAmendement (String content) {
    	addAmendmentButton.setEnabled(true);
    	previewModificationElement.setValue(content);
    }

	@Override
	public void attach() {
        super.attach();
        eventBus.register(this);	
	}
	
    @Override
    public void detach() {
        eventBus.unregister(this);
        super.detach();
    }

	@Override
	public Class<?> getChildClass() {
		// TODO Auto-generated method stub
		return null;
	}
}
