package eu.europa.ec.leos.web.ui.component;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;

import com.google.common.eventbus.EventBus;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Binder;
import com.vaadin.data.ReadOnlyHasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

import eu.europa.ec.leos.domain.vo.DocumentVO;
import eu.europa.ec.leos.i18n.LanguageHelper;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.security.LeosPermission;
import eu.europa.ec.leos.security.SecurityContext;
import eu.europa.ec.leos.ui.event.view.collection.DeleteReportRequest;
import eu.europa.ec.leos.ui.event.view.collection.SaveReportMetaDataRequest;
import eu.europa.ec.leos.vo.coedition.CoEditionVO;
import eu.europa.ec.leos.vo.coedition.InfoType;
import eu.europa.ec.leos.web.event.view.report.OpenReportEvent;
import eu.europa.ec.leos.web.support.user.UserHelper;
import eu.europa.ec.leos.web.ui.converter.LangCodeToDescriptionV8Converter;
//import eu.europa.ec.leos.web.ui.converter.UserLoginDisplayConverter;
import eu.europa.ec.leos.web.ui.converter.UserLoginDisplayConverter;

@SpringComponent
@Scope("prototype")
@DesignRoot("ReportBlockDesign.html")
public class ReportBlockComponent extends VerticalLayout {
    public static SimpleDateFormat dataFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    private static final long serialVersionUID = 1133841334809202933L;
    private static final int TITLE_MAX_LEGTH  = 2000;

    protected HeadingComponent heading;
    protected Label titleCaption;
    protected EditBoxComponent title;
    protected Label reportUserCoEdition;
    protected Button openButton;
    protected Label language;
    protected Label lastUpdated;
    protected Button moveUpButton;
    protected Button moveDownButton;

    private MessageHelper messageHelper;
    private EventBus eventBus;
    private UserHelper userHelper;
    private SecurityContext securityContext;
    private LangCodeToDescriptionV8Converter langConverter;

    private boolean enableSave;
    private Binder<DocumentVO> reportBinder;

    @Value("${leos.coedition.sip.enabled}")
    private boolean coEditionSipEnabled;

    @Value("${leos.coedition.sip.domain}")
    private String coEditionSipDomain;

    @Autowired
    public ReportBlockComponent(LanguageHelper languageHelper, MessageHelper messageHelper, EventBus eventBus, UserHelper userHelper,
            SecurityContext securityContext) {
        this.messageHelper = messageHelper;
        this.eventBus = eventBus;
        this.userHelper = userHelper;
        this.securityContext = securityContext;
        this.langConverter = new LangCodeToDescriptionV8Converter(languageHelper);
        Design.read(this);
    }

    @PostConstruct
    private void init() {
        addStyleName("report-block");

        reportBinder = new Binder<>();
        reportBinder.forField(new ReadOnlyHasValue<>(language::setValue))
                .withConverter(langConverter)
                .bind(DocumentVO::getLanguage, DocumentVO::setLanguage);
        reportBinder.forField(title).bind(DocumentVO::getTitle, DocumentVO::setTitle);

        titleCaption.setCaption(messageHelper.getMessage("collection.block.caption.report.title"));
        title.setPlaceholder(messageHelper.getMessage("collection.block.report.title.prompt"));
        openButton.setCaption(messageHelper.getMessage("leos.button.open"));// using same caption as of card
        language.setCaption(messageHelper.getMessage("collection.caption.language"));
        heading.setCaption(messageHelper.getMessage("collection.block.caption.report"));

        heading.addRightButton(createDeleteReportButton());
        openButton.addClickListener(event -> openReport());
        title.addValueChangeListener(event -> saveData());
       

        moveUpButton.setDisableOnClick(true);
        moveUpButton.setDescription(messageHelper.getMessage("collection.block.report.move.up"));
        moveUpButton.addClickListener(event -> eventBus.post(new MoveReportEvent((DocumentVO) this.getData(), MoveReportEvent.Direction.UP)));

        moveDownButton.setDisableOnClick(true);
        moveDownButton.setDescription(messageHelper.getMessage("collection.block.report.move.down"));
        moveDownButton.addClickListener(event -> eventBus.post(new MoveReportEvent((DocumentVO) this.getData(), MoveReportEvent.Direction.DOWN)));
    }

    private Button createDeleteReportButton() {
        Button button = new Button();
        button.setIcon(VaadinIcons.MINUS_CIRCLE);
        button.setDescription(messageHelper.getMessage("collection.description.button.delete.report"));
        button.addStyleName("delete-button");
        button.addClickListener(listener -> deleteReport());
        return button;
    }

    public void populateData(DocumentVO report) {
    	enableSave = false; // To avoid triggering save on load of data
        resetBasedOnPermissions(report);
        this.setData(report);
        reportBinder.setBean(report);
        heading.setCaption(messageHelper.getMessage("collection.block.caption.report", report.getDocNumber())); // update
        setLastUpdated(report.getUpdatedBy(), report.getUpdatedOn());
        enableSave = true;
        title.setTitleMaxSize(TITLE_MAX_LEGTH);
    }

    private void resetBasedOnPermissions(DocumentVO reportVO) {
        boolean enableUpdate = securityContext.hasPermission(reportVO, LeosPermission.CAN_UPDATE);
        heading.getRightButton().setVisible(enableUpdate);
        moveUpButton.setVisible(enableUpdate);
        moveDownButton.setVisible(enableUpdate);
        title.setEnabled(enableUpdate);
    }

    private void openReport() {
        eventBus.post(new OpenReportEvent((DocumentVO) this.getData()));
    }

    private void deleteReport() {
        // TODO Confirm
        eventBus.post(new DeleteReportRequest((DocumentVO) this.getData()));
    }

    private void saveData() {
    	if (enableSave) {
	        // get original vo and update with latest value and fire save
	        DocumentVO report = ((DocumentVO) this.getData());
	        report.setTitle(title.getValue());
	        eventBus.post(new SaveReportMetaDataRequest(report));
    	}
    }

    public void setLastUpdated(String lastUpdatedBy, Date lastUpdatedOn) {
        lastUpdated.setValue(messageHelper.getMessage("collection.caption.document.lastupdated", dataFormat.format(lastUpdatedOn),
                new UserLoginDisplayConverter(userHelper).convertToPresentation(lastUpdatedBy, null, null)));
    }

    public void updateUserCoEditionInfo(List<CoEditionVO> coEditionVos, User user) {
        // Update report user CoEdition information
        reportUserCoEdition.setIcon(null);
        reportUserCoEdition.setDescription("");
        reportUserCoEdition.removeStyleName("leos-user-coedition-self-user");
        DocumentVO reportVo = (DocumentVO) this.getData();
        coEditionVos.stream()
                .filter((x) -> (InfoType.ELEMENT_INFO.equals(x.getInfoType()) || InfoType.TOC_INFO.equals(x.getInfoType())) && x.getDocumentId().equals(reportVo.getVersionSeriesId()))
                .sorted(Comparator.comparing(CoEditionVO::getUserName).thenComparingLong(CoEditionVO::getEditionTime))
                .forEach(x -> {
                    StringBuilder userDescription = new StringBuilder();
                    if (!x.getUserLoginName().equals(user.getLogin())) {
                        userDescription.append("<a class=\"leos-user-coedition-lync\" href=\"")
                            .append(StringUtils.isEmpty(x.getUserEmail()) ? "" : (coEditionSipEnabled ? new StringBuilder("sip:").append(x.getUserEmail().replaceFirst("@.*", "@" + coEditionSipDomain)).toString()
                                    : new StringBuilder("mailto:").append(x.getUserEmail()).toString()))
                            .append("\">").append(x.getUserName()).append(" (").append(StringUtils.isEmpty(x.getEntity()) ? "-" : x.getEntity())
                            .append(")</a>");
                    } else {
                        userDescription.append(x.getUserName()).append(" (").append(StringUtils.isEmpty(x.getEntity()) ? "-" : x.getEntity()).append(")");
                    }
                    reportUserCoEdition.setDescription(
                            reportUserCoEdition.getDescription() +
                                    messageHelper.getMessage("coedition.tooltip.message", userDescription, dataFormat.format(new Date(x.getEditionTime()))) +
                                    "<br>",
                            ContentMode.HTML);
                });
        if (!reportUserCoEdition.getDescription().isEmpty()) {
            reportUserCoEdition.setIcon(VaadinIcons.USER);
            if (!reportUserCoEdition.getDescription().contains("href=\"")) {
                reportUserCoEdition.addStyleName("leos-user-coedition-self-user");
            }
        }
    }
}