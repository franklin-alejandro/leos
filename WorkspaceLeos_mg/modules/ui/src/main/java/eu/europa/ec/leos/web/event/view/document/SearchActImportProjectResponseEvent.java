package eu.europa.ec.leos.web.event.view.document;

public class SearchActImportProjectResponseEvent {
	 private String document;

	public SearchActImportProjectResponseEvent(String document) {
		super();
		this.document = document;
	}

	public String getDocument() {
		return document;
	}

}
