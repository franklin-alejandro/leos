package eu.europa.ec.leos.web.model;

public class AGEReportIncidenceVO {


	private String impact;
	private String urgency;
	private String summary;
	private String notes;


	
	public AGEReportIncidenceVO(String impact,
			String urgency, String summary, String notes) {
		super();


		this.impact = impact;
		this.urgency = urgency;
		this.summary = summary;
		this.notes = notes;
	}



	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public String getUrgency() {
		return urgency;
	}

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	
	
}
