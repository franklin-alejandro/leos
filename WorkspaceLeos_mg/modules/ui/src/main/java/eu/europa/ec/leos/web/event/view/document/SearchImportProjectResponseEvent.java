package eu.europa.ec.leos.web.event.view.document;

import java.util.List;

import eu.europa.ec.leos.domain.vo.DocumentVO;

public class SearchImportProjectResponseEvent {
	
	List<DocumentVO> documentVOs;

	public SearchImportProjectResponseEvent(List<DocumentVO> documentVOs) {
		super();
		this.documentVOs = documentVOs;
	}

	public List<DocumentVO> getDocumentVOs() {
		return documentVOs;
	}
}
