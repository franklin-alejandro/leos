package eu.europa.ec.leos.ui.extension;

import eu.europa.ec.leos.ui.shared.js.LeosJavaScriptExtensionState;

public class ActionManagerAmendmentState extends LeosJavaScriptExtensionState {

    public boolean openLayout;
    public String articleId;
    public String underModificationLabel;
    public String openAmendmentViewLabel;

}
