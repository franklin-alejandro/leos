//Evolutivo #2678
package eu.europa.ec.leos.ui.view;

public enum LayoutModeType {
    COMPARISON_ON,
    COMPARISON_OFF,
    AMENDING_ON,
    AMENDING_OFF
}
