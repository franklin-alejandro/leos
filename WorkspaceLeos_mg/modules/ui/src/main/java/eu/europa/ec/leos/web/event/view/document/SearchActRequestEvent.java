package eu.europa.ec.leos.web.event.view.document;

import eu.europa.ec.leos.web.model.SearchCriteriaVO;

public class SearchActRequestEvent {

    private SearchCriteriaVO searchCriteria;

    public SearchActRequestEvent(SearchCriteriaVO searchCriteria) {
        super();
        this.searchCriteria = searchCriteria;
    }

    public SearchCriteriaVO getSearchCriteria() {
        return searchCriteria;
    }
    
	public void setSearchCriteria(SearchCriteriaVO searchCriteria) {
		this.searchCriteria = searchCriteria;
	}
}
