package eu.europa.ec.leos.web.model;

import eu.europa.ec.leos.model.oj.DocTypeSpain;

public class SearchCriteriaBOEVO {
	public static DocTypeSpain DEFAULT_DOCTYPE = DocTypeSpain.LEY;
    public static String DEFAULT_YEAR = null;
    public static String DEFAULT_NUMBER = "";
	
	private DocTypeSpain type;
	private String year;
	private String month;
	private String day;
	private String number;
	private String reference;
    private Accion accion;
	
	public enum Accion {
    	IMPORT,
    	AMENDMENT
    }
    	
//	public SearchCriteriaBOEVO(DocTypeSpain type, String year, String month, String day, String number, String reference) {
//		super();
//		this.type = type;
//		this.year = year;
//		this.month = month;
//		this.day = day;
//		this.number = number;
//		this.reference = reference;
//	}
	
	public SearchCriteriaBOEVO(DocTypeSpain type, String year, String month, String day, String number,
			String reference, Accion accion) {
		super();
		this.type = type;
		this.year = year;
		this.month = month;
		this.day = day;
		this.number = number;
		this.reference = reference;
		this.accion = accion;
	}

    public boolean isEmptySearchCriteria() {
        return type == DEFAULT_DOCTYPE && year == DEFAULT_YEAR && number == DEFAULT_NUMBER;
    }

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}


	public DocTypeSpain getType() {
		return type;
	}

	public void setType(DocTypeSpain type) {
		this.type = type;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Accion getAccion() {
		return accion;
	}

	public void setAccion(Accion accion) {
		this.accion = accion;
	}
	
	
}
