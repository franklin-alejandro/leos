package eu.europa.ec.leos.ui.event.view;

import eu.europa.ec.leos.model.report.AGEReportStructureType;

public class AGEReportStructureChangeEvent {

	private AGEReportStructureType structureType;

    public AGEReportStructureChangeEvent(AGEReportStructureType structureType) {
        super();
        this.structureType = structureType;
    }

    public AGEReportStructureType getStructureType() {
        return structureType;
    }
}

