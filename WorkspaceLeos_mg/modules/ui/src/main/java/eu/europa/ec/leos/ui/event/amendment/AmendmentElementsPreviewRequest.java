package eu.europa.ec.leos.ui.event.amendment;

import java.util.List;

public class AmendmentElementsPreviewRequest {

	private List<String> elementAmendmentSelect;
	private String parentElement;
	
	public AmendmentElementsPreviewRequest(List<String> elementAmendmentSelect, String parentElement) {
		super();
		this.elementAmendmentSelect = elementAmendmentSelect;
		this.parentElement = parentElement;
	}
	
	public String getParentElement() {
		return parentElement;
	}

	public void setParentElement(String parentElement) {
		this.parentElement = parentElement;
	}

	public List<String> getElementAmendmentSelect() {
		return elementAmendmentSelect;
	}

	public void setElementAmendmentSelect(List<String> elementAmendmentSelect) {
		this.elementAmendmentSelect = elementAmendmentSelect;
	}
}
