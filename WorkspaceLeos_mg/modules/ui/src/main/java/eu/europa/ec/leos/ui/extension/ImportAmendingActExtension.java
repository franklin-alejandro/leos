/*
 * Copyright 2020 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.ui.extension;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.vaadin.annotations.JavaScript;
import com.vaadin.ui.JavaScriptFunction;
import com.vaadin.ui.Label;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import eu.europa.ec.leos.ui.event.amendment.AmendmentDoSearchEvent;
import eu.europa.ec.leos.web.event.view.document.*;
import eu.europa.ec.leos.web.support.LeosCacheToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@JavaScript({"vaadin://../js/ui/extension/importAmendingActConnector.js"+ LeosCacheToken.TOKEN})
public class ImportAmendingActExtension extends LeosJavaScriptExtension {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(ImportAmendingActExtension.class);
    private EventBus eventBus;

    public ImportAmendingActExtension(EventBus eventBus, Label target) {
        super();
        this.eventBus = eventBus;
        extend(target);
        registerServerSideAPI();
    }

    @Override
    public void attach() {
        super.attach();
        eventBus.register(this);
    }
    
    @Override
    public void detach() {
        eventBus.unregister(this);
        super.detach();
    }

    @Subscribe
    public void handleAmendingActEvents(HandleAmendingActResponseEvent event) {
        callFunction("handleAmendingActEvents");
    }

    private void registerServerSideAPI() {
        addFunction("responseSelectedElementsAmend", new JavaScriptFunction() {
            @Override
            public void call(JsonArray arguments) {
                JsonObject data = arguments.get(0);
                JsonArray selectedElementsJson =  data.hasKey("selectedElements") ? data.getArray("selectedElements"): null;
                String parentElement =  data.hasKey("parentElements") ? data.getString("parentElements"): null;
                List<String> selectedElements = new ArrayList<>();
                for (int index = 0; selectedElementsJson != null && index < selectedElementsJson.length(); index++) {
                    selectedElements.add(selectedElementsJson.getString(index));
                }

                eventBus.post(new SelectedElementAmendmentResponseEvent(selectedElements, parentElement));
            }
        });
        addFunction("doSearch", new JavaScriptFunction() {
            @Override
            public void call(JsonArray arguments) {
                eventBus.post(new AmendmentDoSearchEvent());
            }
        });
    }
}