/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.ui.view.annex;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.UI;

import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.cmis.document.Annex;
import eu.europa.ec.leos.domain.common.InstanceType;
import eu.europa.ec.leos.domain.vo.DocumentVO;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.instance.Instance;
import eu.europa.ec.leos.model.annex.AnnexStructureType;
import eu.europa.ec.leos.security.SecurityContext;
import eu.europa.ec.leos.services.store.PackageService;
import eu.europa.ec.leos.services.toc.StructureContext;
import eu.europa.ec.leos.ui.component.ComparisonComponent;
import eu.europa.ec.leos.ui.component.markedText.MarkedTextComponent;
import eu.europa.ec.leos.ui.component.toc.TableOfContentItemConverter;
import eu.europa.ec.leos.ui.component.versions.VersionsTab;
import eu.europa.ec.leos.ui.event.view.AddStructureChangeMenuEvent;
import eu.europa.ec.leos.ui.window.toc.TocEditor;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;
import eu.europa.ec.leos.web.event.component.LayoutChangeRequestEvent;
import eu.europa.ec.leos.web.event.view.document.InstanceTypeResolver;
import eu.europa.ec.leos.web.event.view.document.SearchActImportProjectResponseEvent;
import eu.europa.ec.leos.web.event.view.document.SearchAmendingActResponseEvent;
import eu.europa.ec.leos.web.event.view.document.SearchImportProjectResponseEvent;
import eu.europa.ec.leos.web.support.cfg.ConfigurationHelper;
import eu.europa.ec.leos.web.support.user.UserHelper;
import eu.europa.ec.leos.web.ui.screen.document.ColumnPosition;
import eu.europa.ec.leos.web.ui.window.ImportBOEWindow;
import eu.europa.ec.leos.web.ui.window.ImportProjectWindow;
import eu.europa.ec.leos.web.ui.window.ImportWindow;

import javax.inject.Provider;
import java.util.List;

@ViewScope
@SpringComponent
@Instance(instances = {InstanceType.COMMISSION, InstanceType.OS})
public class ProposalAnnexScreenImpl extends AnnexScreenImpl {
	private static final long serialVersionUID = -6719257516608653344L;

	private MarkedTextComponent markedTextComponent;

    private ImportWindow importWindow;
    private ImportProjectWindow importProjectWindow;
    private ImportBOEWindow importBOEWindow;//AGE-CORE-3.0.0

	ProposalAnnexScreenImpl(MessageHelper messageHelper, EventBus eventBus, SecurityContext securityContext, UserHelper userHelper,
			ConfigurationHelper cfgHelper, TocEditor numberEditor, InstanceTypeResolver instanceTypeResolver,
			VersionsTab<Annex> versionsTab, Provider<StructureContext> structureContextProvider, PackageService packageService,
			MarkedTextComponent markedTextComponent) {
		super(messageHelper, eventBus, securityContext, userHelper, cfgHelper, numberEditor, instanceTypeResolver, versionsTab, structureContextProvider, packageService);
		this.markedTextComponent = markedTextComponent;
	}

	@Override
	public void init() {
		super.init();
		actionsMenuBar.setChildComponentClass(MarkedTextComponent.class);
		screenLayoutHelper.addPane(comparisonComponent, 2, false);
		screenLayoutHelper.layoutComponents();
	}

	@Subscribe
	void changePosition(LayoutChangeRequestEvent event) {
		changeLayout(event, markedTextComponent);
	}

	@Override
	public void cleanComparedContent() {
		final String versionInfo = messageHelper.getMessage("document.compare.version.caption.simple");
		markedTextComponent.populateMarkedContent("", LeosCategory.BILL, versionInfo);
	}

	@Override
	public void showVersion(String content, String versionInfo) {
		changePosition(new LayoutChangeRequestEvent(ColumnPosition.DEFAULT, ComparisonComponent.class));

		markedTextComponent.populateMarkedContent(content.replaceAll("(?i) id=\"", " id=\"marked-"), LeosCategory.BILL, versionInfo);
		markedTextComponent.hideCompareButtons();
	}

	@Override
	public void populateComparisonContent(String comparedContent, String comparedInfo) {
		markedTextComponent.populateMarkedContent(comparedContent, LeosCategory.BILL, comparedInfo);
		markedTextComponent.showCompareButtons();
	}

	@Override
	public void populateDoubleComparisonContent(String comparedContent, String versionInfo) {
		throw new IllegalArgumentException("Operation not valid");
	}

	@Override
	public void scrollToMarkedChange(String elementId) {
		markedTextComponent.scrollToMarkedChange(elementId);
	}

	@Override
	public void enableTocEdition(List<TableOfContentItemVO> tableOfContent) {
		tableOfContentComponent.handleEditTocRequest(tocEditor);
		tableOfContentComponent.setTableOfContent(TableOfContentItemConverter.buildTocData(tableOfContent));
	}

	@Override
	public void setStructureChangeMenuItem() {
		AnnexStructureType structureType = getStructureType();
		eventBus.post(new AddStructureChangeMenuEvent(structureType));
	}

	@Override
	public boolean isComparisonComponentVisible() {
		return comparisonComponent != null && comparisonComponent.getParent() != null;
	}

	@Override
	public void showImportWindow() {
		importWindow = new ImportWindow(messageHelper, eventBus);
		UI.getCurrent().addWindow(importWindow);
		importWindow.center();
		importWindow.focus();
	}

	@Override
	public void showImportProjectWindow() {       
		importProjectWindow = new ImportProjectWindow(messageHelper,  eventBus);

		UI.getCurrent().addWindow(importProjectWindow);
		importProjectWindow.center();
		importProjectWindow.focus();
	}

	@Override
	public void showImportBOEWindow() {       
		importBOEWindow = new ImportBOEWindow(messageHelper,  eventBus);
		UI.getCurrent().addWindow(importBOEWindow);
		importBOEWindow.center();
		importBOEWindow.focus();
	}

	@Override
	public void displaySearchedContent(String content) {
		eventBus.post(new SearchAmendingActResponseEvent(content));
	}

	@Override
	public void displaySearchedProjectContent(String content) {
		eventBus.post(new SearchActImportProjectResponseEvent(content));
	}

	@Override
	public void displaySearchedDocuments(List<DocumentVO> documentVOs) {
		eventBus.post(new SearchImportProjectResponseEvent(documentVOs));
	}


    @Override
    public void closeImportWindow() {
        if (importWindow != null) {
            importWindow.close();
        } 
        if (importProjectWindow != null) {
        	importProjectWindow.close();
        } 
        if (importBOEWindow != null) {//AGE-CORE-3.0.0
        	importBOEWindow.close();
        } 
    }

}
