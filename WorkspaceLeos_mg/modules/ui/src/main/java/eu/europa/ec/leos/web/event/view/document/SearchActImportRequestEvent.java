package eu.europa.ec.leos.web.event.view.document;

import eu.europa.ec.leos.web.model.SearchCriteriaProjectVO;
import eu.europa.ec.leos.web.model.SearchCriteriaVO;

public class SearchActImportRequestEvent {

	SearchCriteriaProjectVO searchCriteria;

	public SearchActImportRequestEvent(SearchCriteriaProjectVO searchCriteriaBean) {
		super();
		this.searchCriteria = searchCriteriaBean;
	}

	public SearchCriteriaProjectVO getSearchCriteria() {
		return searchCriteria;
	}
}
