//Evolutivo #2678
package eu.europa.ec.leos.web.event.component;

import eu.europa.ec.leos.ui.view.LayoutModeType;

public class LayoutModeEvent {
	
	private LayoutModeType layoutModeType;

	public LayoutModeEvent(LayoutModeType layoutModeType) {
		this.layoutModeType = layoutModeType;
	}

	public LayoutModeType getLayoutModeType() {
		return layoutModeType;
	}
	
}
