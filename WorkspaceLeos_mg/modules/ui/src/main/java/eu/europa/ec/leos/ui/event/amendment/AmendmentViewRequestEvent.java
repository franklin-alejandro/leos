//Evolutivo #2678
package eu.europa.ec.leos.ui.event.amendment;

public class AmendmentViewRequestEvent {

	private String elementId;    
	private String num;
	private String type;
    
    public AmendmentViewRequestEvent(String elementId, String num, String type) {
		this.elementId = elementId;
		this.num = num;
		this.type = type;
	}
          
    public AmendmentViewRequestEvent(String elementId) {
		super();
		this.elementId = elementId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public String getElementId() {
		return elementId;
	}

	public void setElementId(String elementId) {
		this.elementId = elementId;
	}

	public String getArticleNum() {
		return num;
	}

	public void setArticleNum(String num) {
		this.num = num;
	}

	
}
