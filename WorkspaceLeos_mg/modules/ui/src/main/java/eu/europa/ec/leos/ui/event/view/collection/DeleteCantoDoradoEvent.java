package eu.europa.ec.leos.ui.event.view.collection;

import eu.europa.ec.leos.domain.vo.DocumentVO;

public class DeleteCantoDoradoEvent {
    private DocumentVO cantoDorado;

    public DeleteCantoDoradoEvent(DocumentVO cantoDorado) {
        this.cantoDorado = cantoDorado;
    }

    public DocumentVO getCantoDorado(){
        return cantoDorado;
    }
}
