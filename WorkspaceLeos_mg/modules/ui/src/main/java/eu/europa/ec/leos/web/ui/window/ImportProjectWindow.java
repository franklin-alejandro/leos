package eu.europa.ec.leos.web.ui.window;

import java.util.List;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.v7.ui.Table;

import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.vo.DocumentVO;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.ui.extension.ImportElementExtension;
import eu.europa.ec.leos.web.event.view.document.ImportElementProjectRequestEvent;
import eu.europa.ec.leos.web.event.view.document.ImportSelectionChangeEvent;
import eu.europa.ec.leos.web.event.view.document.SearchActImportIdRequestEvent;
import eu.europa.ec.leos.web.event.view.document.SearchActImportProjectResponseEvent;
import eu.europa.ec.leos.web.event.view.document.SearchActImportRequestEvent;
import eu.europa.ec.leos.web.event.view.document.SearchImportProjectResponseEvent;
import eu.europa.ec.leos.web.event.view.document.SelectAllElementRequestEvent;
import eu.europa.ec.leos.web.event.view.document.SelectedElementRequestEvent;
import eu.europa.ec.leos.web.event.view.document.SelectedElementResponseEvent;
import eu.europa.ec.leos.web.model.SearchCriteriaProjectVO;
import eu.europa.ec.leos.web.model.SearchIdProjectVO;


@SuppressWarnings("deprecation")
public class ImportProjectWindow extends AbstractWindow {

	private static final long serialVersionUID = 1L;

	private MessageHelper messageHelper;
	private EventBus eventBus;

	public static final String ARTICLE = "article";
	public static final String RECITAL = "recital";
	public static final String PROVISO = "proviso";

	VerticalLayout windowLayout;
	private Table tableDocument;
	private Label content;
	private Label counter;
	private Label info;
	private Label infoLabel;
	private Button importBtn;
	private Label importInfo;
	private TextField titleTxf;
	private Button selectAllArticles;
	private Button selectAllRecitals;
	private Button selectAllprovisos;
	private Binder<SearchCriteriaProjectVO> searchCriteriaTitleBinder;

	public ImportProjectWindow(MessageHelper messageHelper, EventBus eventBus) {

		super(messageHelper, eventBus);
		this.messageHelper = messageHelper;
		this.eventBus = eventBus;
		setCaption(messageHelper.getMessage("document.importer.project.window.title"));
		prepareWindow();
	}

	@Override
	public void attach() {
		super.attach();
		eventBus.register(this);
	}

	@Override
	public void detach() {
		super.detach();
		eventBus.unregister(this);
	}

	public void prepareWindow() {
		setWidth("27.5cm");
		setHeight(95, Unit.PERCENTAGE);

		windowLayout = new VerticalLayout();
		windowLayout.setSizeFull();
		windowLayout.setMargin(new MarginInfo(false, false, false, true));
		setBodyComponent(windowLayout);

		windowLayout.addComponent(buildSearchLayout());

		infoLabel = new Label(messageHelper.getMessage("document.importer.project.label.info"), ContentMode.HTML);
		windowLayout.addComponent(infoLabel);

		final Component textContent = buildContentTableLayout();
		windowLayout.addComponent(textContent);
		windowLayout.setExpandRatio(textContent, 1.0f);

		//add components to window layout
		selectAllRecitals = buildSelectAllButton(RECITAL);
		addComponentOnLeft(selectAllRecitals);
		selectAllRecitals.setCaption(messageHelper.getMessage("document.import.selectAll.recitals"));

		selectAllArticles = buildSelectAllButton(ARTICLE);
		addComponentOnLeft(selectAllArticles);
		selectAllArticles.setCaption(messageHelper.getMessage("document.import.selectAll.articles"));

		selectAllprovisos = buildSelectAllButton(PROVISO);
		addComponentOnLeft(selectAllprovisos);
		selectAllprovisos.setCaption(messageHelper.getMessage("document.import.selectAll.provisos"));
		
		buildImportInfoLabel();
		buildImportButton();
		searchButton();
	}

	private Button buildSelectAllButton(String type) {
		Button selectAll = new Button();
		selectAll.setEnabled(false);
		selectAll.addStyleName("leos-import-selectAll");
		selectAll.setData(Boolean.FALSE);
		selectAll.addClickListener(event -> {
			if((boolean) selectAll.getData()) {
				selectAll.removeStyleName("leos-import-selectAll-active");
				selectAll.setData(Boolean.FALSE);
			} else {
				selectAll.addStyleName("leos-import-selectAll-active");
				selectAll.setData(Boolean.TRUE);
			}
			eventBus.post(new SelectAllElementRequestEvent((boolean) selectAll.getData(), type));
		});
		return selectAll;
	}

	private void buildImportInfoLabel() {
		importInfo = new Label();
		importInfo.setContentMode(ContentMode.HTML);
		importInfo.setValue("&nbsp;");
		addComponentAtPosition(importInfo, 1);
	}

	private void buildImportButton() {
		importBtn = new Button();
		importBtn.addStyleName("primary");
		importBtn.setCaption(messageHelper.getMessage("document.importer.button.import"));
		importBtn.setStyleName("primary");
		importBtn.setCaptionAsHtml(true);
		importBtn.setEnabled(false);
		addComponentAtPosition(importBtn, 2);

		importBtn.addClickListener(event -> {
			eventBus.post(new SelectedElementRequestEvent());
		});
	}

	private HorizontalLayout buildSearchLayout() {
		HorizontalLayout searchLayout = new HorizontalLayout();
		searchLayout.setWidth("100%");
		searchLayout.setMargin(new MarginInfo(false, true, true, false));

		searchCriteriaTitleBinder = new Binder<>();

		String regex = ".*"; // regex to look for digits and white space
		titleTxf = new TextField(messageHelper.getMessage("repository.filter.search.prompt"));
		titleTxf.setWidth(900, Unit.PIXELS);
		titleTxf.setRequiredIndicatorVisible(true);
		titleTxf.setMaxLength(300);

		// Counter for input length
		counter = new Label();
		counter.setValue(titleTxf.getValue().length() +
				" de " + titleTxf.getMaxLength());

		// Display the current length interactively in the counter
		titleTxf.addValueChangeListener(event -> {
			int len = event.getValue().length();
			counter.setValue(len + " of " + titleTxf.getMaxLength());
			if (len>0) {
				searchButton();
			}
		});

		titleTxf.setValueChangeMode(ValueChangeMode.EAGER);

		searchCriteriaTitleBinder.forField(titleTxf)
		.asRequired(messageHelper.getMessage("document.importer.project.format.error"))
		.withValidator(new RegexpValidator(messageHelper.getMessage("document.importer.project.format.error"), regex))
		.bind(SearchCriteriaProjectVO::getTitle, SearchCriteriaProjectVO::setTitle);

		searchLayout.addComponent(titleTxf);
		searchLayout.addComponent(counter);

		info = new Label(VaadinIcons.INFO_CIRCLE.getHtml(), ContentMode.HTML);
		info.setDescription(messageHelper.getMessage("document.importer.project.search.info"), ContentMode.HTML);
		info.addStyleName("leos-import-search-info-icon");
		searchLayout.addComponent(info);
		searchLayout.setComponentAlignment(info, Alignment.BOTTOM_RIGHT);

		return searchLayout;
	}

	private void searchButton () {
		SearchCriteriaProjectVO searchCriteriaBean = new SearchCriteriaProjectVO(titleTxf.getValue());
		searchCriteriaTitleBinder.readBean(searchCriteriaBean);
		try {
			searchCriteriaTitleBinder.writeBean(searchCriteriaBean);
		} catch (ValidationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		eventBus.post(new SearchActImportRequestEvent(searchCriteriaBean));
	}

	private Component buildContentTableLayout() {
		// create placeholder to display imported content
		tableDocument = new Table(messageHelper.getMessage("document.importer.project.all"));

		tableDocument.addContainerProperty(messageHelper.getMessage("toc.item.type.title"), String.class, null);
		tableDocument.addContainerProperty(messageHelper.getMessage("repository.caption.created"),  String.class, null);
		tableDocument.addContainerProperty(messageHelper.getMessage("repository.caption.updated"),  String.class, null);
		tableDocument.addContainerProperty(messageHelper.getMessage("document.importer.project.table.action"),  Button.class, null);
		tableDocument.setColumnWidth(messageHelper.getMessage("toc.item.type.title"), 600);
		//align center column
		tableDocument.setColumnAlignment("Acción", Table.ALIGN_CENTER);

		tableDocument.setEnabled(true);
		tableDocument.setSizeFull();

		return tableDocument;
	}	

	@Subscribe
	public void getSearchActProjectResponse(SearchImportProjectResponseEvent event) {
		List<DocumentVO> documents = event.getDocumentVOs();
		Boolean isEnabled = documents == null ? false : true;
		int index = 1;
		reset();
		if (isEnabled && documents.size() > 0) {
			for (DocumentVO document : documents) {
				Button detailsField = new Button(messageHelper.getMessage("leos.button.open"));
				detailsField.setData(document);
				detailsField.addClickListener(event1 -> {
					DocumentVO dVO=(DocumentVO)event1.getButton().getData();				
					SearchIdProjectVO searchCriteriaBean = new SearchIdProjectVO(dVO.getId());
					importBtn.setData(searchCriteriaBean);
					eventBus.post(new SearchActImportIdRequestEvent(searchCriteriaBean));
					activateImport();

				});
				detailsField.addStyleName("primary");

				tableDocument.addItem(new Object[]{document.getTitle(), document.getCreatedBy(),document.getUpdatedBy(),detailsField},++index);
			}
		} 
		tableDocument.setPageLength(1);
	}
	
	 @Subscribe
	 public void getSearchActImportProjectResponse(SearchActImportProjectResponseEvent event) {
		 String document = event.getDocument();
		 Boolean isEnabled = document == null ? false : true;
		 
		 content = new Label();
	     content.setContentMode(ContentMode.HTML);
	     content.setSizeFull();
	     content.setStyleName("leos-import-content");
		 windowLayout.removeComponent(tableDocument);
		 content.setValue(document);
	     content.addStyleName(LeosCategory.BILL.name().toLowerCase());
		 windowLayout.addComponent(content);
		 windowLayout.setExpandRatio(content, 1.0f);
		 
		 new ImportElementExtension(eventBus, content);
		 
		 selectAllArticles.setEnabled(isEnabled);
		 selectAllRecitals.setEnabled(isEnabled);
		 selectAllprovisos.setEnabled(isEnabled);
	 }

    @Subscribe
    public void receiveSelectedElement(SelectedElementResponseEvent event) {
    	SearchIdProjectVO searchBean = (SearchIdProjectVO) importBtn.getData();
        eventBus.post(new ImportElementProjectRequestEvent(searchBean, event.getElementIds()));
    }

	@Subscribe
	public void enableImportButton(ImportSelectionChangeEvent event) {
		int count = event.getCount();
		if(count > 0) {
			importBtn.setEnabled(true);
			importInfo.setValue(messageHelper.getMessage("document.import.selected.element", count));
		} else {
			importBtn.setEnabled(false);
			importInfo.setValue(null);
		}
	}

	private void reset() {
		tableDocument.getContainerDataSource().removeAllItems();
		importBtn.setVisible(false);
		importInfo.setValue(null);
		selectAllArticles.removeStyleName("leos-import-selectAll-active");
		selectAllRecitals.removeStyleName("leos-import-selectAll-active");
		selectAllprovisos.removeStyleName("leos-import-selectAll-active");
		selectAllArticles.setData(Boolean.FALSE);
		selectAllRecitals.setData(Boolean.FALSE);
		selectAllprovisos.setData(Boolean.FALSE);
		selectAllArticles.setVisible(false);
		selectAllRecitals.setVisible(false);
		selectAllprovisos.setVisible(false);
	}
	
	private void activateImport() {
		titleTxf.setVisible(false);
		counter.setVisible(false);
		info.setVisible(false);
		infoLabel.setValue(messageHelper.getMessage("document.importer.project.label.info.bis"));
		importBtn.setVisible(true);
		importInfo.setValue(null);
		selectAllArticles.removeStyleName("leos-import-selectAll-active");
		selectAllRecitals.removeStyleName("leos-import-selectAll-active");
		selectAllprovisos.removeStyleName("leos-import-selectAll-active");
		selectAllArticles.setData(Boolean.FALSE);
		selectAllRecitals.setData(Boolean.FALSE);
		selectAllprovisos.setData(Boolean.FALSE);
		selectAllArticles.setVisible(true);
		selectAllRecitals.setVisible(true);
		selectAllprovisos.setVisible(true);
	}

}
