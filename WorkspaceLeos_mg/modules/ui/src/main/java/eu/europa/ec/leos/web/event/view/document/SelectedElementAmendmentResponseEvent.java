package eu.europa.ec.leos.web.event.view.document;

import java.util.List;

public class SelectedElementAmendmentResponseEvent {
	
	private List<String> elementSelect;
	private String parentElement;

	public SelectedElementAmendmentResponseEvent(List<String> elementSelect, String parentElement) {
		super();
		this.elementSelect = elementSelect;
		this.parentElement = parentElement;
	}
	
	public String getParentElement() {
		return parentElement;
	}

	public void setParentElement(String parentElement) {
		this.parentElement = parentElement;
	}

	public List<String> getElementSelect() {
		return elementSelect;
	}

	public void setElementSelect(List<String> elementSelect) {
		this.elementSelect = elementSelect;
	}

	
	
	

}
