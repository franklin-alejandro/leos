package eu.europa.ec.leos.ui.event.view;

import eu.europa.ec.leos.model.main.AGEMainStructureType;

public class AGEMainStructureChangeEvent {

	private AGEMainStructureType structureType;

    public AGEMainStructureChangeEvent(AGEMainStructureType structureType) {
        super();
        this.structureType = structureType;
    }

    public AGEMainStructureType getStructureType() {
        return structureType;
    }
}

