package eu.europa.ec.leos.web.event.view.document;

import eu.europa.ec.leos.web.model.SearchIdProjectVO;
import eu.europa.ec.leos.web.model.SearchCriteriaVO;

public class SearchActImportIdRequestEvent {
	
	SearchIdProjectVO searchCriteria;

	public SearchActImportIdRequestEvent(SearchIdProjectVO searchCriteriaBean) {
		super();
		this.searchCriteria = searchCriteriaBean;
	}

	public SearchIdProjectVO getSearchCriteria() {
		return searchCriteria;
	}

}
