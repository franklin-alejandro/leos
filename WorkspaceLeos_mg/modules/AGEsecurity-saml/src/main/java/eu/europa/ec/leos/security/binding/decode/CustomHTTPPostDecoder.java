package eu.europa.ec.leos.security.binding.decode;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.opensaml.common.binding.SAMLMessageContext;
import org.opensaml.saml2.binding.decoding.HTTPPostDecoder;
import org.opensaml.ws.message.decoder.MessageDecodingException;
import org.opensaml.ws.transport.http.HTTPInTransport;
import org.opensaml.xml.parse.ParserPool;
import org.opensaml.xml.util.Base64;
import org.opensaml.xml.util.DatatypeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomHTTPPostDecoder extends HTTPPostDecoder {
	
	/** Class logger. */
    private final Logger log = LoggerFactory.getLogger(CustomHTTPPostDecoder.class);
	
    /** Constructor. */
    public CustomHTTPPostDecoder() {
        super();
    }

    /**
     * Constructor.
     * 
     * @param pool parser pool used to deserialize messages
     */
    public CustomHTTPPostDecoder(ParserPool pool) {
        super(pool);
    }
    
	@Override
	@SuppressWarnings("rawtypes")
    protected boolean isIntendedDestinationEndpointURIRequired(SAMLMessageContext samlMsgCtx) {
        return false;
    }
    
    @Override
    protected InputStream getBase64DecodedMessage(HTTPInTransport transport) throws MessageDecodingException {
        log.debug("Getting Base64 encoded message from request");
        String encodedMessage = transport.getParameterValue("SAML_REQUEST");
        if (DatatypeHelper.isEmpty(encodedMessage)) {
            encodedMessage = transport.getParameterValue("SAML_RESPONSE");
        }

        if (DatatypeHelper.isEmpty(encodedMessage)) {
            log.error("Request did not contain either a SAMLRequest or "
                    + "SAMLResponse paramter.  Invalid request for SAML 2 HTTP POST binding.");
            throw new MessageDecodingException("No SAML message present in request");
        }

        log.trace("Base64 decoding SAML message:\n{}", encodedMessage);
        byte[] decodedBytes = Base64.decode(encodedMessage);
        if(decodedBytes == null){
            log.error("Unable to Base64 decode SAML message");
            throw new MessageDecodingException("Unable to Base64 decode SAML message");
        }

        log.trace("Decoded SAML message:\n{}", new String(decodedBytes));
        return new ByteArrayInputStream(decodedBytes);
    }

}
