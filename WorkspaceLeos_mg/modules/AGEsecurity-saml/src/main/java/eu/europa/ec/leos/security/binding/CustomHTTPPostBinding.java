package eu.europa.ec.leos.security.binding;

import java.util.List;

import org.apache.velocity.app.VelocityEngine;
import org.opensaml.ws.security.SecurityPolicyRule;
import org.opensaml.ws.transport.InTransport;
import org.opensaml.ws.transport.http.HTTPInTransport;
import org.opensaml.ws.transport.http.HTTPTransport;
import org.opensaml.xml.parse.ParserPool;
import org.springframework.security.saml.context.SAMLMessageContext;
import org.springframework.security.saml.processor.HTTPPostBinding;

import eu.europa.ec.leos.security.binding.decode.CustomHTTPPostDecoder;
import eu.europa.ec.leos.security.binding.encode.CustomHTTPPostEncoder;

public class CustomHTTPPostBinding extends HTTPPostBinding {
	
	public CustomHTTPPostBinding(ParserPool parserPool, VelocityEngine velocityEngine) {
		super(parserPool, new CustomHTTPPostDecoder(parserPool), new CustomHTTPPostEncoder(velocityEngine, "/templates/saml2-post-binding.vm"));
	}

	@Override
	public boolean supports(InTransport transport) {
        if (transport instanceof HTTPInTransport) {
            HTTPTransport t = (HTTPTransport) transport;
            return "POST".equalsIgnoreCase(t.getHTTPMethod()) && (t.getParameterValue("SAML_REQUEST") != null || t.getParameterValue("SAML_RESPONSE") != null);
        } else {
            return false;
        }
    }
	
	@Override
    public void getSecurityPolicy(List<SecurityPolicyRule> securityPolicy, SAMLMessageContext samlContext) {

		samlContext.setInboundSAMLMessageAuthenticated(true);
		
//        SignatureTrustEngine engine = samlContext.getLocalTrustEngine();
//        securityPolicy.add(new SAML2HTTPPostSimpleSignRule(engine, parserPool, engine.getKeyInfoResolver()));
//        securityPolicy.add(new SAMLProtocolMessageXMLSignatureSecurityPolicyRule(engine));

    }

}
