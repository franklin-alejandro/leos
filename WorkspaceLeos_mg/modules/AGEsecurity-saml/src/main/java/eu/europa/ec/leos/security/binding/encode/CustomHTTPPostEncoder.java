package eu.europa.ec.leos.security.binding.encode;

import java.io.UnsupportedEncodingException;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.opensaml.common.binding.SAMLMessageContext;
import org.opensaml.saml2.binding.encoding.HTTPPostEncoder;
import org.opensaml.saml2.core.RequestAbstractType;
import org.opensaml.saml2.core.StatusResponseType;
import org.opensaml.ws.message.encoder.MessageEncodingException;
import org.opensaml.xml.util.Base64;
import org.opensaml.xml.util.XMLHelper;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Encoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomHTTPPostEncoder extends HTTPPostEncoder {
	
	/** Class logger. */
    private final Logger log = LoggerFactory.getLogger(HTTPPostEncoder.class);
    
    /**
     * Constructor.
     * 
     * @param engine Velocity engine instance used to create POST body
     * @param templateId ID of the template used to create POST body
     */
    public CustomHTTPPostEncoder(VelocityEngine engine, String templateId) {
        super(engine, templateId);
    }
    
    /**
     * Populate the Velocity context instance which will be used to render the POST body.
     * 
     * @param velocityContext the Velocity context instance to populate with data
     * @param messageContext the SAML message context source of data
     * @param endpointURL endpoint URL to which to encode message
     * @throws MessageEncodingException thrown if there is a problem encoding the message
     */
    @Override
    protected void populateVelocityContext(VelocityContext velocityContext, SAMLMessageContext messageContext,
            String endpointURL) throws MessageEncodingException {
        
        Encoder esapiEncoder = ESAPI.encoder();

        String encodedEndpointURL = esapiEncoder.encodeForHTMLAttribute(endpointURL);
        log.debug("Encoding action url of '{}' with encoded value '{}'", endpointURL, encodedEndpointURL);
        velocityContext.put("action", encodedEndpointURL);
        velocityContext.put("binding", getBindingURI());

        log.debug("Marshalling and Base64 encoding SAML message");
        if (messageContext.getOutboundSAMLMessage().getDOM() == null) {
            marshallMessage(messageContext.getOutboundSAMLMessage());
        }
        try {
            String messageXML = XMLHelper.nodeToString(messageContext.getOutboundSAMLMessage().getDOM());
            String encodedMessage = Base64.encodeBytes(messageXML.getBytes("UTF-8"), Base64.DONT_BREAK_LINES);
            if (messageContext.getOutboundSAMLMessage() instanceof RequestAbstractType) {
                velocityContext.put("SAML_REQUEST", encodedMessage);
            } else if (messageContext.getOutboundSAMLMessage() instanceof StatusResponseType) {
                velocityContext.put("SAML_RESPONSE", encodedMessage);
            } else {
                throw new MessageEncodingException(
                        "SAML message is neither a SAML RequestAbstractType or StatusResponseType");
            }
        } catch (UnsupportedEncodingException e) {
            log.error("UTF-8 encoding is not supported, this VM is not Java compliant.");
            throw new MessageEncodingException("Unable to encode message, UTF-8 encoding is not supported");
        }

        String relayState = messageContext.getRelayState();
        if (checkRelayState(relayState)) {
            String encodedRelayState = esapiEncoder.encodeForHTMLAttribute(relayState);
            log.debug("Setting RelayState parameter to: '{}', encoded as '{}'", relayState, encodedRelayState);
            velocityContext.put("RelayState", encodedRelayState);
        }
    }

}
