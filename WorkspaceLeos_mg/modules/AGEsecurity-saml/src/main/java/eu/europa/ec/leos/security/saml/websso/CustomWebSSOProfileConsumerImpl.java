package eu.europa.ec.leos.security.saml.websso;

import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.QName;

import org.opensaml.common.SAMLException;
import org.opensaml.saml2.core.AudienceRestriction;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.Condition;
import org.opensaml.saml2.core.Conditions;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.OneTimeUse;
import org.opensaml.saml2.core.ProxyRestriction;
import org.opensaml.saml2.core.Subject;
import org.opensaml.saml2.core.SubjectConfirmation;
import org.opensaml.xml.encryption.DecryptionException;
import org.springframework.security.saml.context.SAMLMessageContext;
import org.springframework.security.saml.websso.WebSSOProfileConsumerImpl;
import org.springframework.util.Assert;

public class CustomWebSSOProfileConsumerImpl extends WebSSOProfileConsumerImpl {

	@Override
    protected void verifySubject(Subject subject, AuthnRequest request, SAMLMessageContext context) throws SAMLException, DecryptionException {

        for (SubjectConfirmation confirmation : subject.getSubjectConfirmations()) {

            if (SubjectConfirmation.METHOD_BEARER.equals(confirmation.getMethod())) {

                NameID nameID;
                if (subject.getEncryptedID() != null) {
                    Assert.notNull(context.getLocalDecrypter(), "Can't decrypt NameID, no decrypter is set in the context");
                    nameID = (NameID) context.getLocalDecrypter().decrypt(subject.getEncryptedID());
                } else {
                    nameID = subject.getNameID();
                }
                context.setSubjectNameIdentifier(nameID);
                return;

            }

        }

        throw new SAMLException("Assertion invalidated by subject confirmation - can't be confirmed by the bearer method");

    }
	
	@Override
	protected void verifyAssertionConditions(Conditions conditions, SAMLMessageContext context, boolean audienceRequired) throws SAMLException {

        // Verify that audience is present when required
        if (audienceRequired && (conditions == null || conditions.getAudienceRestrictions().size() == 0)) {
            throw new SAMLException("Assertion invalidated by missing Audience Restriction");
        }

        // If no conditions are implied, storage is deemed valid
        if (conditions == null) {
            return;
        }

        if (conditions.getNotBefore() != null) {
            if (conditions.getNotBefore().minusSeconds(getResponseSkew()).isAfterNow()) {
                throw new SAMLException("Assertion is not yet valid, invalidated by condition notBefore " + conditions.getNotBefore());
            }
        }
        if (conditions.getNotOnOrAfter() != null) {
            if (conditions.getNotOnOrAfter().plusSeconds(getResponseSkew()).isBeforeNow()) {
                throw new SAMLException("Assertion is no longer valid, invalidated by condition notOnOrAfter " + conditions.getNotOnOrAfter());
            }
        }

        List<Condition> notUnderstoodConditions = new LinkedList<Condition>();

        for (Condition condition : conditions.getConditions()) {

            QName conditionQName = condition.getElementQName();

            if (conditionQName.equals(AudienceRestriction.DEFAULT_ELEMENT_NAME)) {

                //verifyAudience(context, conditions.getAudienceRestrictions());

            } else if (conditionQName.equals(OneTimeUse.DEFAULT_ELEMENT_NAME)) {

                throw new SAMLException("System cannot honor OneTimeUse condition of the Assertion for WebSSO");

            } else if (conditionQName.equals(ProxyRestriction.DEFAULT_ELEMENT_NAME)) {

                ProxyRestriction restriction = (ProxyRestriction) condition;
                log.debug("Honoring ProxyRestriction with count {}, system does not issue assertions to 3rd parties", restriction.getProxyCount());

            } else {

                log.debug("Condition {} is not understood", condition);
                notUnderstoodConditions.add(condition);

            }

        }

        // Check not understood conditions
        verifyConditions(context, notUnderstoodConditions);

    }
	
}
