(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof _dereq_&&_dereq_;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof _dereq_&&_dereq_,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(_dereq_,module,exports){
'use strict';

function injectStylesheet(doc, href) {
  var link = doc.createElement('link');
  link.rel = 'stylesheet';
  link.type = 'text/css';
  link.href = href;
  doc.head.appendChild(link);
}

function injectScript(doc, src) {
  var script = doc.createElement('script');
  script.type = 'text/javascript';
  script.src = src;

  // Set 'async' to false to maintain execution order of scripts.
  // See https://developer.mozilla.org/en-US/docs/Web/HTML/Element/script
  script.async = false;
  doc.head.appendChild(script);
}

function injectAssets(doc, config, assets) {
  assets.forEach(function (path) {
    var url = config.assetRoot + '/' + config.manifest[path]; //LEOS changes
    if (url.match(/\.css/)) {
      injectStylesheet(doc, url);
    } else {
      injectScript(doc, url);
    }
  });
}

/**
 * Bootstrap the Hypothesis client.
 *
 * This triggers loading of the necessary resources for the client
 */
function bootHypothesisClient(doc, config) {
  // Detect presence of Hypothesis in the page
  var appLinkEl = doc.querySelector('link[type="application/annotator+html"]');
  if (appLinkEl) {
    return;
  }

  // Register the URL of the sidebar app which the Hypothesis client should load.
  // The <link> tag is also used by browser extensions etc. to detect the
  // presence of the Hypothesis client on the page.
  var sidebarUrl = doc.createElement('link');
  sidebarUrl.rel = 'sidebar';
  sidebarUrl.href = config.sidebarAppUrl;
  sidebarUrl.type = 'application/annotator+html';
  doc.head.appendChild(sidebarUrl);

  // Register the URL of the annotation client which is currently being used to drive
  // annotation interactions.
  var clientUrl = doc.createElement('link');
  clientUrl.rel = 'hypothesis-client';
  clientUrl.href = config.assetRoot + '/boot.js';
  clientUrl.type = 'application/annotator+javascript';
  doc.head.appendChild(clientUrl);

  injectAssets(doc, config, [
  // Vendor code and polyfills
  'scripts/polyfills.bundle.js', 'scripts/jquery.bundle.js',

  // Main entry point for the client
  'scripts/annotator.bundle.js', 'styles/icomoon.css', 'styles/annotator.css', 'styles/pdfjs-overrides.css']);
}

/**
 * Bootstrap the sidebar application which displays annotations.
 */
function bootSidebarApp(doc, config) {
  injectAssets(doc, config, [
  // Vendor code and polyfills required by app.bundle.js
  'scripts/raven.bundle.js', 'scripts/angular.bundle.js', 'scripts/katex.bundle.js', 'scripts/showdown.bundle.js', 'scripts/polyfills.bundle.js', 'scripts/unorm.bundle.js',

  // The sidebar app
  'scripts/sidebar.bundle.js', 'styles/angular-csp.css', 'styles/angular-toastr.css', 'styles/icomoon.css', 'styles/katex.min.css', 'styles/sidebar.css']);
}

function boot(document_, config) {
  if (document_.querySelector('hypothesis-app')) {
    bootSidebarApp(document_, config);
  } else {
    bootHypothesisClient(document_, config);
  }
}

module.exports = boot;

},{}],2:[function(_dereq_,module,exports){
'use strict';

// This is the main entry point for the Hypothesis client in the host page
// and the sidebar application.
//
// The same boot script is used for both entry points so that the browser
// already has it cached when it encounters the reference in the sidebar
// application.

// Variables replaced by the build script

/* global __MANIFEST__ */

var boot = _dereq_('./boot');
var settings = _dereq_('../shared/settings').jsonConfigsFrom(document);

boot(document, {
  assetRoot: settings.assetRoot || '',
  manifest: __MANIFEST__,
  sidebarAppUrl: settings.sidebarAppUrl || ''
});

},{"../shared/settings":3,"./boot":1}],3:[function(_dereq_,module,exports){
'use strict';

// `Object.assign()`-like helper. Used because this script needs to work
// in IE 10/11 without polyfills.

function assign(dest, src) {
  for (var k in src) {
    if (src.hasOwnProperty(k)) {
      dest[k] = src[k];
    }
  }
  return dest;
}

/**
 * Return a parsed `js-hypothesis-config` object from the document, or `{}`.
 *
 * Find all `<script class="js-hypothesis-config">` tags in the given document,
 * parse them as JSON, and return the parsed object.
 *
 * If there are no `js-hypothesis-config` tags in the document then return
 * `{}`.
 *
 * If there are multiple `js-hypothesis-config` tags in the document then merge
 * them into a single returned object (when multiple scripts contain the same
 * setting names, scripts further down in the document override those further
 * up).
 *
 * @param {Document|Element} document - The root element to search.
 */
function jsonConfigsFrom(document) {
  var config = {};
  var settingsElements = document.querySelectorAll('script.js-hypothesis-config');

  if (settingsElements.length) {
    for (var i = 0; i < settingsElements.length; i++) {
      var settings;
      try {
        settings = JSON.parse(settingsElements[i].textContent);
      } catch (err) {
        console.warn('Could not parse settings from js-hypothesis-config tags', err);
        settings = {};
      }
      assign(config, settings);
    }
  } else {
    // LEOS: For sidebar application the document is the iframe and then script.js-hypothesis-config
    // is not on it. In this case configuration is taken from documentURI.
    var url = new URL(document.documentURI);
    var searchParams = new URLSearchParams(url.search);
    if (searchParams.get('config') != null) {
      config = JSON.parse(searchParams.get('config'));
      assign(config, { 'sidebarAppUrl': document.baseURI + 'app.html' });
    }
  }

  return config;
}

module.exports = {
  jsonConfigsFrom: jsonConfigsFrom
};

},{}]},{},[2])
//# sourceMappingURL=boot.bundle.js.map
