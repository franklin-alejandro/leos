/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.cmis.search;

import eu.europa.ec.leos.cmis.mapping.AGECmisProperties;
import eu.europa.ec.leos.model.filter.QueryFilter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class AGEQueryFilter extends QueryFilter {

    private final List<Filter> filters = new ArrayList<>();
    private final List<SortOrder> sortOrders = new ArrayList<>();

    public enum FilterType {
        Root("root"),//NO CMIS COLUMN
        actType(AGECmisProperties.METADATA_ACTTYPE.getId()),
        procedureType(AGECmisProperties.METADATA_PROCEDURETYPE.getId()),
        docType(AGECmisProperties.METADATA_TYPE.getId()),
        ref(AGECmisProperties.METADATA_REF.getId()),
        template(AGECmisProperties.DOCUMENT_TEMPLATE.getId()),
        docTemplate(AGECmisProperties.LEOS_DOCTEMPLATE.getId()),

        category(AGECmisProperties.DOCUMENT_CATEGORY.getId()),
        language(AGECmisProperties.DOCUMENT_LANGUAGE.getId()),
        role(AGECmisProperties.COLLABORATORS.getId(), "ANY"),
        title(AGECmisProperties.DOCUMENT_TITLE.getId()),
        versionLabel(AGECmisProperties.VERSION_LABEL.getId()),
        versionType(AGECmisProperties.VERSION_TYPE.getId()),
        containedDocuments(AGECmisProperties.CONTAINED_DOCUMENTS.getId(), "ANY"),
        
        cmisVersionLabel("cmis:versionLabel"),
        creationDate("cmis:creationDate"),
        lastModificationDate("cmis:lastModificationDate");

        private String cmisColumnName;
        private String multiColumnType;

        FilterType(String columnName) {
            this.cmisColumnName = columnName;
        }

        FilterType(String columnName, String multiColumnType) {
            this.cmisColumnName = columnName;
            this.multiColumnType = multiColumnType;
        }

        public static String getColumnType(String uiName) {
            for (FilterType column : values()) {
                if (column.name().equals(uiName)) {
                    return column.multiColumnType;
                }
            }
            throw new IllegalArgumentException("No Column Name found for the UI Name : " + uiName);
        }

        public static String getColumnName(String uiName) {
            for (FilterType column : values()) {
                if (column.name().equals(uiName)) {
                    return column.cmisColumnName;
                }
            }
            throw new IllegalArgumentException("No Column Name found for the UI Name : " + uiName);
        }
        
        static String getVersionsWithoutVersionLabelQueryString(String docRef) {
            StringBuilder queryBuilder =  new StringBuilder(
            		AGECmisProperties.METADATA_REF.getId())
            		.append(" = '")
            		.append(docRef)
                    .append("' ")
                    .append(" AND ").append(AGECmisProperties.VERSION_LABEL.getId()).append(" IS NULL")
                    .append(" order by cmis:creationDate DESC");
            return queryBuilder.toString();
        }
    }

}
