/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.cmis.mapping;

public enum AGECmisProperties {
	
    DOCUMENT_CATEGORY("ls:leos_category"),
    DOCUMENT_TITLE("ls:leos_title"),               // FIXME maybe move title property to metadata or remove it entirely
    DOCUMENT_TEMPLATE("ls:leos_template"),
    DOCUMENT_LANGUAGE("ls:leos_language"),
    METADATA_REF("ls:metadata_ref"),
    MILESTONE_COMMENTS("ls:leos_milestonecomments"),
    INITIAL_CREATED_BY("ls:leos_initialcreatedby"),
    INITIAL_CREATION_DATE("ls:leos_initialcreationdate"),
    JOB_ID("ls:leos_jobid"),
    JOB_DATE("ls:leos_jobdate"),
    STATUS("ls:leos_status"),
    METADATA_STAGE("ls:metadata_docstage"),
    METADATA_TYPE("ls:metadata_doctype"),
    METADATA_PURPOSE("ls:metadata_docpurpose"),
    METADATA_DOCTEMPLATE("ls:metadata_doctemplate"),
    ANNEX_INDEX("ls:annex_docindex"),
    ANNEX_NUMBER("ls:annex_docnumber"),
    ANNEX_TITLE("ls:annex_doctitle"),
    REPORT_INDEX("ls:report_docindex"),
    REPORT_NUMBER("ls:report_docnumber"),
    REPORT_TITLE("ls:report_doctitle"),
    CANTODORADO_INDEX("ls:annex_docindex"),
    CANTODORADO_NUMBER("ls:annex_docnumber"),
    CANTODORADO_TITLE("ls:annex_doctitle"),
    ACL_NAME("ls:acl_name"),
    ACL_DOMAIN("ls:acl_domain"),
    GROUP_NAME("ls:group_name"),
    GROUP_PERMIT("ls:group_permit"),
    COLLABORATORS("ls:leos_collaborators"),
    VERSION_LABEL("ls:leos_versionlabel"),
    VERSION_TYPE("ls:leos_versiontype"),
	CONTAINED_DOCUMENTS("ls:leos_contained_documents"),
	LASTMODIFIER("ls:leos_lastmodifiedby"),
	CREATOR("ls:leos_createdby_user"),
	METADATA_ACTTYPE("ls:metadata_acttype"),
	METADATA_DOCPURPOSE("ls:metadata_docpurpose"),
	METADATA_DOCSTAGE("ls:metadata_docstage"),
	METADATA_DOCTEMPLATEE("ls:metadata_doctemplate"),
	METADATA_DOCTYPE("ls:metadata_doctype"),
	METADATA_PROCEDURETYPE("ls:metadata_proceduretype"),
	LEOS_DOCTEMPLATE("ls:leos_doctemplate"),
	LEOS_CHECKINCOMMENT("ls:leos_checkincomment"),
	LEOS_CHECKINCOMMENT_EXT("ls:leos_checkincomment_ext");
	         
    private String id;

    AGECmisProperties(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}

