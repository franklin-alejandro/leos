/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.cmis.extensions;

import eu.europa.ec.leos.cmis.mapping.AGECmisProperties;
import eu.europa.ec.leos.domain.cmis.metadata.*;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static eu.europa.ec.leos.cmis.mapping.AGECmisProperties.*;
import static eu.europa.ec.leos.cmis.mapping.AGECmisProperties.DOCUMENT_LANGUAGE;
import static eu.europa.ec.leos.cmis.mapping.AGECmisProperties.DOCUMENT_TEMPLATE;
import static eu.europa.ec.leos.cmis.mapping.AGECmisProperties.DOCUMENT_TITLE;
import static eu.europa.ec.leos.cmis.mapping.AGECmisProperties.METADATA_DOCTEMPLATE;
import static eu.europa.ec.leos.cmis.mapping.AGECmisProperties.METADATA_PURPOSE;
import static eu.europa.ec.leos.cmis.mapping.AGECmisProperties.METADATA_REF;
import static eu.europa.ec.leos.cmis.mapping.AGECmisProperties.METADATA_STAGE;
import static eu.europa.ec.leos.cmis.mapping.AGECmisProperties.METADATA_TYPE;
import static java.util.stream.Collectors.toMap;
import static org.springframework.util.StringUtils.isEmpty;

public class AGELeosMetadataExtensions extends LeosMetadataExtensions {

    //todo: fix this generics..
    public static Map<String, ? extends Object> toCmisProperties(LeosMetadata leosMetadata) {

        Map<String, ? extends Object> cmisProperties;
        if (leosMetadata instanceof ProposalMetadata) {
            cmisProperties = toCmisProperties((ProposalMetadata) leosMetadata);
        } else if (leosMetadata instanceof MemorandumMetadata) {
            cmisProperties = toCmisProperties((MemorandumMetadata) leosMetadata);
        } else if (leosMetadata instanceof BillMetadata) {
            cmisProperties = toCmisProperties((BillMetadata) leosMetadata);
        } else if (leosMetadata instanceof AnnexMetadata) {
            cmisProperties = toCmisProperties((AnnexMetadata) leosMetadata);
        } else if (leosMetadata instanceof AGEReportMetadata) {
            cmisProperties = toCmisProperties((AGEReportMetadata) leosMetadata);
        } else if (leosMetadata instanceof AGECantoDoradoMetadata) {
            cmisProperties = toCmisProperties((AGECantoDoradoMetadata) leosMetadata);
        } else {
            throw new IllegalStateException("Unknown LEOS Metadata! [type=" + leosMetadata.getClass().getSimpleName() + ']');
        }

        return cmisProperties.entrySet()
                .stream()
                .filter(mapEntry -> !isEmpty(mapEntry.getValue()))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private static Map<String, String> toCmisProperties(ProposalMetadata proposalMetadata) {

        String title = Stream.of(proposalMetadata.getStage(), proposalMetadata.getType(), proposalMetadata.getPurpose())
                .filter(s -> s != null && !s.isEmpty())
                .collect(Collectors.joining(" "));

        return buildCommonProperties(proposalMetadata, title);
    }
    
    private static Map<String, String> toCmisProperties(MemorandumMetadata memorandumMetadata) {
        String title = memorandumMetadata.getType();

        return buildCommonProperties(memorandumMetadata, title);
    }

    private static Map<String, String> toCmisProperties(BillMetadata billMetadata) {
        String title = Stream.of(billMetadata.getStage(), billMetadata.getType(), billMetadata.getPurpose())
                .filter(s -> s != null && !s.isEmpty())
                .collect(Collectors.joining(" "));

        return buildCommonProperties(billMetadata, title);
    }   
    
    private static Map<String, ?> toCmisProperties(AnnexMetadata annexMetadata) {
        String title = annexMetadata.getType();

        Map<String, Object> cmisProperties = new HashMap<>();

        cmisProperties.putAll(buildCommonProperties(annexMetadata, title));

        cmisProperties.put(AGECmisProperties.ANNEX_INDEX.getId(), annexMetadata.getIndex());
        cmisProperties.put(AGECmisProperties.ANNEX_NUMBER.getId(), annexMetadata.getNumber());
        cmisProperties.put(AGECmisProperties.ANNEX_TITLE.getId(), annexMetadata.getTitle());

        return cmisProperties;
    }

    private static Map<String, ?> toCmisProperties(AGEReportMetadata reportMetadata) {
        String title = reportMetadata.getType();

        Map<String, Object> cmisProperties = new HashMap<>();

        cmisProperties.putAll(buildCommonProperties(reportMetadata, title));

        cmisProperties.put(AGECmisProperties.REPORT_INDEX.getId(), reportMetadata.getIndex());
        cmisProperties.put(AGECmisProperties.REPORT_NUMBER.getId(), reportMetadata.getNumber());
        cmisProperties.put(AGECmisProperties.REPORT_TITLE.getId(), reportMetadata.getTitle());

        return cmisProperties;
    }    
    
    //MGM En justicia no se va a utilizar el canto dorado, comento el método 
//    private static Map<String, ?> toCmisProperties(AGECantoDoradoMetadata cantoDoradoMetadata) {
//        String title = cantoDoradoMetadata.getType();
//
//        Map<String, Object> cmisProperties = new HashMap<>();
//
//        cmisProperties.putAll(buildCommonProperties(cantoDoradoMetadata, title));
//
//        cmisProperties.put(AGECmisProperties.CANTODORADO_INDEX.getId(), cantoDoradoMetadata.getIndex());
//        cmisProperties.put(AGECmisProperties.CANTODORADO_NUMBER.getId(), cantoDoradoMetadata.getNumber());
//        cmisProperties.put(AGECmisProperties.CANTODORADO_TITLE.getId(), cantoDoradoMetadata.getTitle());
//
//        return cmisProperties;
//    }  

    private static Map<String, String> buildCommonProperties(LeosMetadata leosMetadata, String title) {
        Map<String, String> cmisProperties = new HashMap<>();
        cmisProperties.put(AGECmisProperties.METADATA_STAGE.getId(), leosMetadata.getStage());
        cmisProperties.put(AGECmisProperties.METADATA_TYPE.getId(), leosMetadata.getType());
        cmisProperties.put(AGECmisProperties.METADATA_PURPOSE.getId(), leosMetadata.getPurpose());
        cmisProperties.put(AGECmisProperties.DOCUMENT_TEMPLATE.getId(), leosMetadata.getTemplate());
        cmisProperties.put(AGECmisProperties.DOCUMENT_LANGUAGE.getId(), leosMetadata.getLanguage());
        cmisProperties.put(AGECmisProperties.METADATA_DOCTEMPLATE.getId(), leosMetadata.getDocTemplate());

        String ref = leosMetadata.getRef();
        cmisProperties.put(AGECmisProperties.METADATA_REF.getId(), ref != null ? ref : "");

        cmisProperties.put(AGECmisProperties.DOCUMENT_TITLE.getId(), title);

        return cmisProperties;
    }

}
