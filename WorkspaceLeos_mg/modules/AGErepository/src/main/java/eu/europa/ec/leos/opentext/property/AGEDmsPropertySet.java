package eu.europa.ec.leos.opentext.property;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;

/**
 * Esta clase representa una colección de propiedades de carpeta o documento. 
 * La clase no permite propiedades duplicadas (propiedades con el mismo nombre). 
 * 
 * @author BosArthur
 */
public class AGEDmsPropertySet {

	private HashMap<String, AGEDmsProperty> propertyMap;
	private List<String> properties;

	/**
	 * Initializa una colección vacia
	 */
	public AGEDmsPropertySet() {
		propertyMap = new HashMap<>();
		properties = new ArrayList<>();
	}

	/**
	 * Añade una propiedad de tipo <code>DmsBooleanProperty</code> a la colección.
	 * Si la colección ya contiene una propiedad con el mismo nombre se
	 * reemplazara la propiedad en la colección.
	 * 
	 * @param 	name
	 * 			el nombre de la propiedad
	 * 
	 * @param 	value
	 * 			el valor de la propiedad
	 * 
	 * @return	un <code>DmsBooleanProperty</code>
	 * 
	 * @throws 	AGEDmsPropertyException
	 * 			si algo va mal en la llamada del método
	 */
	public AGEDmsProperty add(String name, boolean value) throws Exception {
		return add(name, Boolean.valueOf(value));
	}

	/**
	 * Añade una propiedad de tipo <code>DmsNumberProperty</code> a la colección.
	 * Si la colección ya contiene una propiedad con el mismo nombre se
	 * reemplazara la propiedad en la colección.
	 * 
	 * @param 	name
	 * 			el nombre de la propiedad
	 * 
	 * @param 	value
	 * 			el valor de la propiedad
	 * 
	 * @return	un <code>DmsNumberProperty</code>
	 * 
	 * @throws 	DmsPropertyException
	 * 			si algo va mal en la llamada del método
	 */
	public AGEDmsProperty add(String name, byte value) throws Exception {
		return add(name, Byte.valueOf(value));
	}

	/**
	 * Añade una propiedad de tipo <code>DmsNumberProperty</code> a la colección.
	 * Si la colección ya contiene una propiedad con el mismo nombre se
	 * reemplazara la propiedad en la colección.
	 * 
	 * @param 	name
	 * 			el nombre de la propiedad
	 * 
	 * @param 	value
	 * 			el valor de la propiedad
	 * 
	 * @return	un <code>DmsNumberProperty</code>
	 * 
	 * @throws 	DmsPropertyException
	 * 			si algo va mal en la llamada del método
	 */
	public AGEDmsProperty add(String name, short value) throws Exception {
		return add(name, Short.valueOf(value));
	}

	/**
	 * Añade una propiedad de tipo <code>DmsNumberProperty</code> a la colección.
	 * Si la colección ya contiene una propiedad con el mismo nombre se
	 * reemplazara la propiedad en la colección.
	 * 
	 * @param 	name
	 * 			el nombre de la propiedad
	 * 
	 * @param 	value
	 * 			el valor de la propiedad
	 * 
	 * @return	un <code>DmsNumberProperty</code>
	 * 
	 * @throws 	DmsPropertyException
	 * 			si algo va mal en la llamada del método
	 */
	public AGEDmsProperty add(String name, int value) throws Exception {
		return add(name, Integer.valueOf(value));
	}

	/**
	 * Añade una propiedad de tipo <code>DmsNumberProperty</code> a la colección.
	 * Si la colección ya contiene una propiedad con el mismo nombre se
	 * reemplazara la propiedad en la colección.
	 * 
	 * @param 	name
	 * 			el nombre de la propiedad
	 * 
	 * @param 	value
	 * 			el valor de la propiedad
	 * 
	 * @return	un <code>DmsNumberProperty</code>
	 * 
	 * @throws 	DmsPropertyException
	 * 			si algo va mal en la llamada del método
	 */
	public AGEDmsProperty add(String name, long value) throws Exception {
		return add(name, Long.valueOf(value));
	}

	/**
	 * Añade una propiedad de tipo <code>DmsNumberProperty</code> a la colección.
	 * Si la colección ya contiene una propiedad con el mismo nombre se
	 * reemplazara la propiedad en la colección.
	 * 
	 * @param 	name
	 * 			el nombre de la propiedad
	 * 
	 * @param 	value
	 * 			el valor de la propiedad
	 * 
	 * @return	un <code>DmsNumberProperty</code>
	 * 
	 * @throws 	DmsPropertyException
	 * 			si algo va mal en la llamada del método
	 */
	public AGEDmsProperty add(String name, float value) throws Exception {
		return add(name, Float.valueOf(value));
	}

	/**
	 * Añade una propiedad de tipo <code>DmsNumberProperty</code> a la colección.
	 * Si la colección ya contiene una propiedad con el mismo nombre se
	 * reemplazara la propiedad en la colección.
	 * 
	 * @param 	name
	 * 			el nombre de la propiedad
	 * 
	 * @param 	value
	 * 			el valor de la propiedad
	 * 
	 * @return	un <code>DmsNumberProperty</code>
	 * 
	 * @throws 	DmsPropertyException
	 * 			si algo va mal en la llamada del método
	 */
	public AGEDmsProperty add(String name, double value) throws Exception {
		return add(name, Double.valueOf(value));
	}

	/**
	 * Añade una propiedad a la colección usando Java Generics. Si la colección 
	 * ya contiene una propiedad con el mismo nombre se reemplazara la propiedad 
	 * en la colección.
	 * 
	 * @param 	name
	 * 			el nombre de la propiedad
	 * 
	 * @param 	value
	 * 			el valor generico de la propiedad
	 * 
	 * @return	<code>DmsBooleanProperty</code>, <code>DmsBooleanArrayProperty</code>,
	 * 			<code>DmsDateProperty</code>, <code>DmsDateArrayProperty</code>
	 * 			<code>DmsNumberProperty</code>, <code>DmsNumberArrayProperty</code>
	 * 			<code>DmsStringProperty</code> o <code>DmsStringArrayProperty</code>.
	 * 
	 * @throws 	DmsPropertyException
	 * 			si algo va mal en la llamada del método
	 */
	public <T> AGEDmsProperty add(String name, T value) throws Exception {
		if (properties.contains(name)) {
			properties.remove(name);
			propertyMap.remove(name);
		}
		AGEDmsProperty property = createProperty(name, value);
		this.propertyMap.put(name, property);
		this.properties.add(name);
		return property;
	}

	/**
	 * Elimina una propiedad de la colección
	 * 
	 * @param 	name
	 * 			el nombre de la propiedad para eliminar
	 * 
	 * @throws 	DmsPropertyException
	 * 			si algo va mal en la llamada del método
	 */
	public void remove(String name) throws Exception {
		if (properties.contains(name)) {
			properties.remove(name);
			propertyMap.remove(name);
		}
	}

	/**
	 * Añade una propiedad de tipo <code>AGEDmsProperty</code> a la colección. Si la 
	 * colección ya contiene una propiedad con el mismo nombre se reemplazara 
	 * la propiedad en la colección.
	 * 
	 * @param 	name
	 * 			la propiedad
	 * 
	 * @throws 	DmsPropertyException
	 * 			si algo va mal en la llamada del método
	 */
	public void add(AGEDmsProperty property) throws Exception {
		if (properties.contains(property.getName())) {
			properties.remove(property.getName());
			propertyMap.remove(property.getName());
		}
		this.propertyMap.put(property.getName(), property);
		this.properties.add(property.getName());
	}

	/**
	 * Este método estático crea una propiedad (sin añadirla a la colección) 
	 * mediante Java Generics.
	 * 
	 * @param 	name
	 * 			el nombre de la propiedad
	 * 
	 * @param 	value
	 * 			el valor generico de la propiedad
	 * 
	 * @return	<code>DmsBooleanProperty</code>, <code>DmsBooleanArrayProperty</code>,
	 * 			<code>DmsDateProperty</code>, <code>DmsDateArrayProperty</code>
	 * 			<code>DmsNumberProperty</code>, <code>DmsNumberArrayProperty</code>
	 * 			<code>DmsStringProperty</code> o <code>DmsStringArrayProperty</code>.
	 * 
	 * @throws 	DmsPropertyException
	 * 			si algo va mal en la llamada del método
	 */
	public static <T> AGEDmsProperty createProperty(String name, T value) throws Exception {
		if (name == null)
			throw new Exception("Name is mandatory.");
		if (value == null)
			throw new Exception("Value is mandatory.");

		AGEDmsProperty property = null;
		if (value instanceof ArrayList) {
			ArrayList valueArray = (ArrayList) value;
			if (valueArray.get(0) instanceof String) {
				String[] valueStringArray = (String[]) valueArray.toArray(new String[valueArray.size()]);
				property = new AGEDmsStringArrayProperty(name, (String[]) valueStringArray);
			}
		}
		else if (value instanceof String)
			property = new AGEDmsStringProperty(name, (String) value);
		else if (value instanceof String[])
			property = new AGEDmsStringArrayProperty(name, (String[]) value);
		else if (value instanceof Boolean)
			property = new AGEDmsBooleanProperty(name, ((Boolean) value).booleanValue());
		else if (value instanceof Boolean[])
			property = new AGEDmsBooleanArrayProperty(name, (Boolean[]) value);
		else if (value instanceof boolean[])
			property = new AGEDmsBooleanArrayProperty(name, ArrayUtils.toObject((boolean[]) value));
		else if (value instanceof Byte)
			property = new AGEDmsNumberProperty(name, (Byte) value);
		else if (value instanceof Byte[])
			property = new AGEDmsNumberArrayProperty(name, (Byte[]) value);
		else if (value instanceof byte[])
			property = new AGEDmsNumberArrayProperty(name, ArrayUtils.toObject((byte[]) value));
		else if (value instanceof Short)
			property = new AGEDmsNumberProperty(name, (Short) value);
		else if (value instanceof Short[])
			property = new AGEDmsNumberArrayProperty(name, (Short[]) value);
		else if (value instanceof short[])
			property = new AGEDmsNumberArrayProperty(name, ArrayUtils.toObject((short[]) value));
		else if (value instanceof Integer)
			property = new AGEDmsNumberProperty(name, (Integer) value);
		else if (value instanceof Integer[])
			property = new AGEDmsNumberArrayProperty(name, (Integer[]) value);
		else if (value instanceof BigInteger)
			property = new AGEDmsNumberProperty(name, (BigInteger) value);
		else if (value instanceof BigInteger[])
			property = new AGEDmsNumberArrayProperty(name, (BigInteger[]) value);
		else if (value instanceof int[])
			property = new AGEDmsNumberArrayProperty(name, ArrayUtils.toObject((int[]) value));
		else if (value instanceof Long)
			property = new AGEDmsNumberProperty(name, (Long) value);
		else if (value instanceof Long[])
			property = new AGEDmsNumberArrayProperty(name, (Long[]) value);
		else if (value instanceof long[])
			property = new AGEDmsNumberArrayProperty(name, ArrayUtils.toObject((long[]) value));
		else if (value instanceof Float)
			property = new AGEDmsNumberProperty(name, (Float) value);
		else if (value instanceof Float[])
			property = new AGEDmsNumberArrayProperty(name, (Float[]) value);
		else if (value instanceof float[])
			property = new AGEDmsNumberArrayProperty(name, ArrayUtils.toObject((float[]) value));
		else if (value instanceof Double)
			property = new AGEDmsNumberProperty(name, (Double) value);
		else if (value instanceof Double[])
			property = new AGEDmsNumberArrayProperty(name, (Double[]) value);
		else if (value instanceof double[])
			property = new AGEDmsNumberArrayProperty(name, ArrayUtils.toObject((double[]) value));
		else if (value instanceof Number)
			property = new AGEDmsNumberProperty(name, (Number) value);
		else if (value instanceof Number[])
			property = new AGEDmsNumberArrayProperty(name, (Number[]) value);
		else if (value instanceof Date)
			property = new AGEDmsDateProperty(name, (Date) value);
		else if (value instanceof Date[])
			property = new AGEDmsDateArrayProperty(name, (Date[]) value);
		else if (value instanceof GregorianCalendar)
			property = new AGEDmsDateProperty(name, ((GregorianCalendar) value).getTime());
		else if (value instanceof GregorianCalendar[]) {
			ArrayList<GregorianCalendar> lista = new ArrayList<>(Arrays.asList((GregorianCalendar[])value));
			ArrayList<Date> listDate = new ArrayList<Date>();
			for (GregorianCalendar gregDate:lista) {
				listDate.add(gregDate.getTime());
			}
			property = new AGEDmsDateArrayProperty(name, (Date[]) listDate.toArray());
		}
		else
			throw new Exception("Class " + value.getClass().getCanonicalName() + " not supported.");

		return property;
	}

	/**
	 * Devuelve el número de propiedades en la colección
	 * 
	 * @return	número de propiedades
	 */
	public int size() {
		return properties.size();
	}

	/**
	 * Devuelve la propiedad de la posición especificada
	 * 
	 * @param 	index
	 * 			posición de la propiedad a devolver
	 * 
	 * @return	la propiedad de la posición especificada
	 */
	public AGEDmsProperty get(int index) {
		String name = properties.get(index);
		return propertyMap.get(name);
	}

	/**
	 * Devuelve la propiedad del nombre especificado
	 * 
	 * @param 	name
	 * 			nombre de la propiedad a devolver
	 * 
	 * @return	la propiedad del nombre especificado
	 */
	public AGEDmsProperty get(String name) {
		return propertyMap.get(name);
	}

	/**
	 * Devuelve los nombre y los valores de las propiedades de la colección 
	 * como una cadena String
	 * 
	 * @return	las propiedades (nombre y valor) concatenados
	 */
	public String getValueAsString() {
		StringBuilder buffer = new StringBuilder();
		Iterator iterator = properties.iterator();
		while (iterator.hasNext())
		{
			AGEDmsProperty property = (AGEDmsProperty)iterator.next();
			buffer.append(property.getName()).append("=").append(property.getValueAsString());
			if (!iterator.hasNext())
				continue;
			buffer.append("|");
		}

		return buffer.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((properties == null) ? 0 : properties.hashCode());
		result = prime * result + ((propertyMap == null) ? 0 : propertyMap.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AGEDmsPropertySet other = (AGEDmsPropertySet) obj;
		if (properties == null) {
			if (other.properties != null)
				return false;
		} else if (!properties.equals(other.properties))
			return false;
		if (propertyMap == null) {
			if (other.propertyMap != null)
				return false;
		} else if (!propertyMap.equals(other.propertyMap))
			return false;
		return true;
	}

	
	/**
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DmsPropertySet other = (DmsPropertySet) obj;
		if (properties == null) {
			if (other.properties != null)
				return false;
		} else if (!properties.equals(other.properties))
			return false;
		if (propertyMap == null) {
			if (other.propertyMap != null)
				return false;
		} else if (!propertyMap.equals(other.propertyMap))
			return false;
		return true;
	}*/

}
