package eu.europa.ec.leos.opentext.property;

public enum AGEDmsSearchCondition {

	EQUAL, 
	NOT_EQUAL,
	GREATER_THAN, 
	LESS_THAN,
	GREATER_EQUAL, 
	LESS_EQUAL, 
	BEGINS_WITH, 
	ENDS_WITH, 
	CONTAINS, 
	DOES_NOT_CONTAIN, 
	IN, 
	NOT_IN,
	IS_NULL, 
	IS_NOT_NULL;

}
