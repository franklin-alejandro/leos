package eu.europa.ec.leos.opentext;

import com.documentum.fc.client.IDfSession;

public interface AGEDfcSessionFactory {
	
	public IDfSession openDocbaseSession();
	
	public void closeDocbaseSession();
	
}
