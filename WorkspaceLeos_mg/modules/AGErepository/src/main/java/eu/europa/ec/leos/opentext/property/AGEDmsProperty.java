package eu.europa.ec.leos.opentext.property;

public abstract class AGEDmsProperty {
	
	private String name = "";
	private AGEDmsSearchCondition searchCondition = AGEDmsSearchCondition.EQUAL;

	public AGEDmsProperty(String name) {
		this.name = name;
		setSearchCondition(AGEDmsSearchCondition.EQUAL);
	}

	public AGEDmsProperty(String name, AGEDmsSearchCondition searchCondition) {
		this.name = name;
		this.searchCondition = searchCondition;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AGEDmsSearchCondition getSearchCondition() {
		return searchCondition;
	}

	public void setSearchCondition(AGEDmsSearchCondition searchCondition) {
		this.searchCondition = searchCondition;
	}

	public abstract String getValueAsString();

	public abstract Object getValueAsObject();

	public abstract void clearValue();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((searchCondition == null) ? 0 : searchCondition.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AGEDmsProperty other = (AGEDmsProperty) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (searchCondition != other.searchCondition)
			return false;
		return true;
	}

	
	/**
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DmsProperty other = (DmsProperty) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (searchCondition != other.searchCondition)
			return false;
		return true;
	}
	*/
}
