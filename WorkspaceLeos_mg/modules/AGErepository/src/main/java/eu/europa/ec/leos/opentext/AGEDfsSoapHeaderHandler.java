package eu.europa.ec.leos.opentext;

import java.security.SecureRandom;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AGEDfsSoapHeaderHandler implements SOAPHandler<SOAPMessageContext> {

	protected static final Logger logger = LoggerFactory.getLogger(AGEDfsSoapHeaderHandler.class.getName());
	
	private static final String NS = "http://context.core.datamodel.fs.documentum.emc.com/";
	
	private String repositoryName;
	private String userName;
	private String password;
	private SecureRandom secureRandom;
	
	public AGEDfsSoapHeaderHandler(String repositoryName, String userName, String password, SecureRandom secureRandom) {
		this.repositoryName = repositoryName;
		this.userName = userName;
		this.password = password;
		this.secureRandom = secureRandom;
	}
	
    public Set<QName> getHeaders() {
        return null;
    }

    public boolean handleMessage(SOAPMessageContext smc) {
        Boolean outboundMessage = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if (outboundMessage) {
            try {
                SOAPMessage message = smc.getMessage();
                SOAPPart part = message.getSOAPPart();
                SOAPEnvelope envelope = part.getEnvelope();
                SOAPHeader soapHeader = envelope.getHeader();
                if (soapHeader == null) {
                	soapHeader = envelope.addHeader();
                }

                SOAPElement sc = soapHeader.addChildElement("ServiceContext", "", NS);
                sc.addNamespaceDeclaration("", NS);
                sc.addNamespaceDeclaration("ns2", "http://properties.core.datamodel.fs.documentum.emc.com/");
                sc.addNamespaceDeclaration("ns3", "http://profiles.core.datamodel.fs.documentum.emc.com/");

                String random = String.valueOf(secureRandom.nextLong());
            	
                sc.addAttribute(new QName("token"), (new StringBuilder("temporary/127.0.0.1-")).append(String.valueOf(System.currentTimeMillis())).append("-").append(random).toString());

                SOAPElement identity = sc.addChildElement("Identities", "", NS);
                identity.addAttribute(new QName("xmlns:xsi"), "http://www.w3.org/2001/XMLSchema-instance");
                identity.addAttribute(new QName("password"), password);
                identity.addAttribute(new QName("repositoryName"), repositoryName);
                identity.addAttribute(new QName("userName"), userName);
                identity.addAttribute(new QName("xsi:type"), "RepositoryIdentity");

                SOAPElement profiles = sc.addChildElement("Profiles", "", NS);
                profiles.addAttribute(new QName("xmlns:xsi"), "http://www.w3.org/2001/XMLSchema-instance");
                profiles.addAttribute(new QName("allowAsyncContentTransfer"), "false");
                profiles.addAttribute(new QName("allowCachedContentTransfer"), "false");
                profiles.addAttribute(new QName("isProcessOLELinks"), "false");
                profiles.addAttribute(new QName("transferMode"), "BASE64");
                profiles.addAttribute(new QName("xsi:type"), "ns3:ContentTransferProfile");

            } catch (Exception e) {
        		throw new RuntimeException("handleFault: cannot handle SOAPMessage", e);
            }
        }
        return true;
    }

    public boolean handleFault(SOAPMessageContext smc) {
        return true;
    }

    public void close(MessageContext messageContext) {
    	// Empty on purpose
    }
}