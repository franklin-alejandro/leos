package eu.europa.ec.leos.opentext.property;

/**
 * Esta clase representa una propiedad de carpeta o documento de tipo 
 * <code>java.lang.String</code>
 * 
 * @author BosArthur
 */
public class AGEDmsStringProperty extends AGEDmsProperty {
	
	private String value;

	public AGEDmsStringProperty(String name) {
		super(name);
		this.value = null;
	}

	public AGEDmsStringProperty(String name, String value) {
		super(name);
		this.value = value;
	}

	public AGEDmsStringProperty(String name, AGEDmsSearchCondition searchCondition, String value) {
		super(name);
		setSearchCondition(searchCondition);
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void clearValue() {
		this.value = null;
	}

	public Object getValueAsObject() {
		return this.value;
	}

	public String getValueAsString() {
		return String.valueOf(this.value);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AGEDmsStringProperty other = (AGEDmsStringProperty) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	/**
	public boolean equals(Object o) {
		if (!super.equals(o)) {
			return false;
		}
		DmsStringProperty that = (DmsStringProperty) o;

		return this.value != null ? this.value.equals(that.value) : that.value == null;
	}*/

}