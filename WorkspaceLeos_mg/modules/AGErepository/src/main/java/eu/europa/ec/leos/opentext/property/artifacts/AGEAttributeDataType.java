package eu.europa.ec.leos.opentext.property.artifacts;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AttributeDataType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AttributeDataType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="E00_BOOLEAN"/>
 *     &lt;enumeration value="E01_INTEGER"/>
 *     &lt;enumeration value="E02_STRING"/>
 *     &lt;enumeration value="E03_ID"/>
 *     &lt;enumeration value="E04_TIME"/>
 *     &lt;enumeration value="E05_DOUBLE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AGEAttributeDataType")
@XmlEnum
public enum AGEAttributeDataType {

    @XmlEnumValue("E00_BOOLEAN")
    E_00_BOOLEAN("E00_BOOLEAN"),
    @XmlEnumValue("E01_INTEGER")
    E_01_INTEGER("E01_INTEGER"),
    @XmlEnumValue("E02_STRING")
    E_02_STRING("E02_STRING"),
    @XmlEnumValue("E03_ID")
    E_03_ID("E03_ID"),
    @XmlEnumValue("E04_TIME")
    E_04_TIME("E04_TIME"),
    @XmlEnumValue("E05_DOUBLE")
    E_05_DOUBLE("E05_DOUBLE");
    private final String value;

    AGEAttributeDataType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AGEAttributeDataType fromValue(String v) {
        for (AGEAttributeDataType c: AGEAttributeDataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
