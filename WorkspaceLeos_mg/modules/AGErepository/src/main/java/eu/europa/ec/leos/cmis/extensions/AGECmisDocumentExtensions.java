/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.cmis.extensions;

import eu.europa.ec.leos.cmis.domain.ContentImpl;
import eu.europa.ec.leos.cmis.domain.SourceImpl;
import eu.europa.ec.leos.cmis.mapping.AGECmisProperties;
import eu.europa.ec.leos.domain.cmis.Content;
import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.cmis.LeosLegStatus;
import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.*;
import eu.europa.ec.leos.domain.cmis.metadata.AGEReportMetadata;
import io.atlassian.fugue.Option;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Property;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.time.Instant;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.UnknownFormatConversionException;

import static eu.europa.ec.leos.cmis.extensions.AGECmisMetadataExtensions.*;

public class AGECmisDocumentExtensions extends CmisDocumentExtensions {

    private static final Logger logger = LoggerFactory.getLogger(CmisDocumentExtensions.class);

    @SuppressWarnings("unchecked")
    public static <T extends LeosDocument> T toLeosDocument(Document document, Class<? extends T> type, boolean fetchContent, Map<String, String> oldVersions) {
    	//MGM busca la categoría del documento con el que se está trabajando (en este caso cuando el catalog es recuperado, mira su tipo)
        T leosDocument;
        LeosCategory category = getCategory(document); //MGM Catalog es tipo config el otro que se adquiere es proposal.
        switch (category) {
            case PROPOSAL:
                if (type.isAssignableFrom(Proposal.class)) {
                    leosDocument = (T) toLeosProposal(document, fetchContent);
                } else {
                    throw new IllegalStateException("Incompatible types! [category=" + category + ", mappedType=" + Proposal.class.getSimpleName() + ", wantedType=" + type.getSimpleName() + ']');
                }
                break;
            case MEMORANDUM:
                if (type.isAssignableFrom(Memorandum.class)) {
                    leosDocument = (T) toLeosMemorandum(document, fetchContent, oldVersions);
                } else {
                    throw new IllegalStateException("Incompatible types! [category=" + category + ", mappedType=" + Memorandum.class.getSimpleName() + ", wantedType=" + type.getSimpleName() + ']');
                }
                break;
            case BILL:
                if (type.isAssignableFrom(Bill.class)) {
                    leosDocument = (T) toLeosBill(document, fetchContent, oldVersions);
                } else {
                    throw new IllegalStateException("Incompatible types! [category=" + category + ", mappedType=" + Bill.class.getSimpleName() + ", wantedType=" + type.getSimpleName() + ']');
                }
                break;
            case ANNEX:
                if (type.isAssignableFrom(Annex.class)) {
                    leosDocument = (T) toLeosAnnex(document, fetchContent, oldVersions);
                } else {
                    throw new IllegalStateException("Incompatible types! [category=" + category + ", mappedType=" + Annex.class.getSimpleName() + ", wantedType=" + type.getSimpleName() + ']');
                }
                break;
            case MEDIA:
                if (type.isAssignableFrom(MediaDocument.class)) {
                    leosDocument = (T) toLeosMediaDocument(document, fetchContent);
                } else {
                    throw new IllegalStateException("Incompatible types! [category=" + category + ", mappedType=" + MediaDocument.class.getSimpleName() + ", wantedType=" + type.getSimpleName() + ']');
                }
                break;
            case CONFIG:
                if (type.isAssignableFrom(ConfigDocument.class)) {
                    leosDocument = (T) toLeosConfigDocument(document, fetchContent);
                } else {
                    throw new IllegalStateException("Incompatible types! [category=" + category + ", mappedType=" + ConfigDocument.class.getSimpleName() + ", wantedType=" + type.getSimpleName() + ']');
                }
                break;
            case LEG:
                if (type.isAssignableFrom(LegDocument.class)) {
                    leosDocument = (T) toLeosLegDocument(document, fetchContent);
                } else {
                    throw new IllegalStateException("Incompatible types! [category=" + category + ", mappedType=" + LegDocument.class.getSimpleName() + ", wantedType=" + type.getSimpleName() + ']');
                }
                break;
            case STRUCTURE:
                if (type.isAssignableFrom(Structure.class)) {
                    leosDocument = (T) toLeosStructureDocument(document, fetchContent);
                } else {
                    throw new IllegalStateException("Incompatible types! [category=" + category + ", mappedType=" + Structure.class.getSimpleName() + ", wantedType=" + type.getSimpleName() + ']');
                }
                break;
            case REPORT:
                if (type.isAssignableFrom(AGEReport.class)) {
                    leosDocument = (T) toLeosReport(document, fetchContent);
                } else {
                    throw new IllegalStateException("Incompatible types! [category=" + category + ", mappedType=" + AGEReport.class.getSimpleName() + ", wantedType=" + type.getSimpleName() + ']');
                }
                break;
            case CANTODORADO:
                if (type.isAssignableFrom(AGECantoDorado.class)) {
                    leosDocument = (T) toLeosCantoDorado(document, fetchContent);
                } else {
                    throw new IllegalStateException("Incompatible types! [category=" + category + ", mappedType=" + AGECantoDorado.class.getSimpleName() + ", wantedType=" + type.getSimpleName() + ']');
                }
                break;
            default:
                throw new IllegalStateException("Unknown category:" + category);
        }

        return leosDocument;
    }

    protected static Proposal toLeosProposal(Document d, boolean fetchContent) {
        return new Proposal(d.getId(), d.getName(), 
        		getLeosCreator(d),
                getCreationInstant(d),
                getLeosLastModifier(d),
                getLastModificationInstant(d),
                d.getVersionSeriesId(), d.getVersionLabel(), getLeosVersionLabel(d), 
                getLeosCheckinComment(d), 
                getVersionType(d), 
                d.isLatestVersion(),
                getTitle(d),
                getCollaborators(d),
                getMilestoneComments(d),
                getInitialCreatedBy(d),
                getInitialCreationInstant(d),
                contentOption(d, fetchContent),
                getProposalMetadataOption(d));
    }

    protected static Memorandum toLeosMemorandum(Document d, boolean fetchContent, Map<String, String> oldVersions) {
        return new Memorandum(d.getId(), d.getName(), 
        		getLeosCreator(d),
                getCreationInstant(d),
                getLeosLastModifier(d),
                getLastModificationInstant(d),
                d.getVersionSeriesId(), d.getVersionLabel(), getLeosVersionLabel(d, oldVersions), 
                getLeosCheckinComment(d), getVersionType(d), d.isLatestVersion(),
                getTitle(d),
                getCollaborators(d),
                getMilestoneComments(d),
                contentOption(d, fetchContent),
                getMemorandumMetadataOption(d));
    }

    protected static Bill toLeosBill(Document d, boolean fetchContent, Map<String, String> oldVersions) {
        return new Bill(d.getId(), d.getName(), getLeosCreator(d),
                getCreationInstant(d),
                getLeosLastModifier(d),
                getLastModificationInstant(d),
                d.getVersionSeriesId(), d.getVersionLabel(), getLeosVersionLabel(d, oldVersions), getLeosCheckinComment(d), getVersionType(d), d.isLatestVersion(),
                getTitle(d),
                getCollaborators(d),
                getMilestoneComments(d),
                contentOption(d, fetchContent),
                getBillMetadataOption(d));
    }

    protected static Annex toLeosAnnex(Document d, boolean fetchContent, Map<String, String> oldVersions) {
        return new Annex(d.getId(), d.getName(), getLeosCreator(d),
                getCreationInstant(d),
                getLeosLastModifier(d),
                getLastModificationInstant(d),
                d.getVersionSeriesId(), d.getVersionLabel(), getLeosVersionLabel(d, oldVersions), getLeosCheckinComment(d), getVersionType(d), d.isLatestVersion(),
                getTitle(d),
                getCollaborators(d),
                getMilestoneComments(d),
                contentOption(d, fetchContent),
                getAnnexMetadataOption(d));
    }

    protected static AGEReport toLeosReport(Document d, boolean fetchContent) {
        return new AGEReport(d.getId(), d.getName(), getLeosCreator(d),
                getCreationInstant(d),
                getLeosLastModifier(d),
                getLastModificationInstant(d),
                d.getVersionSeriesId(), d.getVersionLabel(), getLeosVersionLabel(d), getLeosCheckinComment(d), getVersionType(d), d.isLatestVersion(),
                getTitle(d),
                getCollaborators(d),
                getMilestoneComments(d),
                contentOption(d, fetchContent),
                getReportMetadataOption(d));
    }
    
    protected static AGECantoDorado toLeosCantoDorado(Document d, boolean fetchContent) {
        return new AGECantoDorado(d.getId(), d.getName(), getLeosCreator(d),
                getCreationInstant(d),
                getLeosLastModifier(d),
                getLastModificationInstant(d),
                d.getVersionSeriesId(), d.getVersionLabel(), getLeosVersionLabel(d), getLeosCheckinComment(d), getVersionType(d), d.isLatestVersion(),
                getTitle(d),
                getCollaborators(d),
                getMilestoneComments(d),
                contentOption(d, fetchContent),
                getCantoDoradoMetadataOption(d));
    }

	protected static MediaDocument toLeosMediaDocument(Document d, boolean fetchContent) {
        return new MediaDocument(d.getId(), d.getName(), getLeosCreator(d),
                getCreationInstant(d),
                getLeosLastModifier(d),
                getLastModificationInstant(d),
                d.getVersionSeriesId(), d.getVersionLabel(), getLeosVersionLabel(d), getLeosCheckinComment(d), getVersionType(d), d.isLatestVersion(),
                contentOption(d, fetchContent));
    }

    protected static ConfigDocument toLeosConfigDocument(Document d, boolean fetchContent) {
        return new ConfigDocument(d.getId(), d.getName(), getLeosCreator(d),
                getCreationInstant(d),
                getLeosLastModifier(d),
                getLastModificationInstant(d),
                d.getVersionSeriesId(), d.getVersionLabel(), getLeosVersionLabel(d), getLeosCheckinComment(d), getVersionType(d), d.isLatestVersion(),
                contentOption(d, fetchContent));
    }

    protected static Structure toLeosStructureDocument(Document d, boolean fetchContent) {
        return new Structure(d.getId(), d.getName(), getLeosCreator(d),
                getCreationInstant(d),
                getLeosLastModifier(d),
                getLastModificationInstant(d),
                d.getVersionSeriesId(), d.getVersionLabel(), getLeosVersionLabel(d), getLeosCheckinComment(d), getVersionType(d), d.isLatestVersion(),
                contentOption(d, fetchContent),
                getStructureMetadataOption(d));
    }

    protected static LegDocument toLeosLegDocument(Document d, boolean fetchContent) {
        return new LegDocument(d.getId(), d.getName(), getLeosCreator(d),
                getCreationInstant(d),
                getLeosLastModifier(d),
                getLastModificationInstant(d),
                d.getVersionSeriesId(), d.getVersionLabel(), getLeosVersionLabel(d), getLeosCheckinComment(d), getVersionType(d), d.isLatestVersion(),
                getMilestoneComments(d),
                contentOption(d, fetchContent),
                getInitialCreatedBy(d),
                getInitialCreationInstant(d),
                getJobId(d),
                getJobDate(d),
                getStatus(d),
                getContainedDocuments(d));
    }

    protected static LeosCategory getCategory(Document document) {
        // FIXME add check for leos:document primary type???
        String cmisCategory = document.getPropertyValue(AGECmisProperties.DOCUMENT_CATEGORY.getId()); 
        return LeosCategory.valueOf(cmisCategory);
    }

    static Instant getCreationInstant(Document document) {
        GregorianCalendar creationDate = document.getCreationDate();
        return creationDate != null ? creationDate.toInstant() : Instant.MIN;
    }

    static Instant getLastModificationInstant(Document document) {
        GregorianCalendar lastModificationDate = document.getLastModificationDate();
        return lastModificationDate != null ? lastModificationDate.toInstant() : Instant.MIN;
    }

    protected static Option<Content> contentOption(Document document, boolean fetchContent) {
        Content content = null;
        if (fetchContent) {
            ContentStream contentStream = document.getContentStream();
            if (contentStream != null) {
                content = new ContentImpl(contentStream.getFileName(), contentStream.getMimeType(),
                        contentStream.getLength(), new SourceImpl(contentStream.getStream()));
            }
        }

        return Option.option(content);
    }

    static Map<String, String> getCollaborators(Document document) {

        Property<String> collaboratorsProperty = document.getProperty(AGECmisProperties.COLLABORATORS.getId());
        List<String> collaboratorsPropertyValues = collaboratorsProperty.getValues();

        Map<String, String> users = new HashMap<>();
        collaboratorsPropertyValues.forEach(value -> {
            try {
                String[] values = value.split("::");
                if (values.length != 2) {
                    throw new UnknownFormatConversionException("User record is in incorrect format, required format[login::Authority ], present value=" + value);
                }

                users.put(values[0], values[1]);
            } catch (Exception e) {
                logger.error("Failure in processing user record [value=" + value + "], continuing...", e);
            }
        });

        return users;
    }

    // FIXME maybe move title property to metadata or remove it entirely
    private static String getTitle(Document document) { // FIXME add check for leos:xml primary type
        return document.getPropertyValue(AGECmisProperties.DOCUMENT_TITLE.getId());
    }

    private static List<String> getMilestoneComments(Document document) {
        Property<String> milestoneComments = document.getProperty(AGECmisProperties.MILESTONE_COMMENTS.getId());
    	// INCIDENCIA REDMINE 953
    	List<String> milestoneComments_prev = milestoneComments.getValues();
        if (milestoneComments_prev.size() == 0){
        	return new ArrayList<>();
        }
        else {
        	return milestoneComments_prev;
        }
    }

    private static String getJobId(Document document) {
        return document.getPropertyValue(AGECmisProperties.JOB_ID.getId());
    }
    
    private static Instant getJobDate(Document document) {
        GregorianCalendar jobDate = document.getPropertyValue(AGECmisProperties.JOB_DATE.getId());
        return jobDate != null ? jobDate.toInstant() : Instant.MIN;
    }
    
    private static LeosLegStatus getStatus(Document document) {
        return LeosLegStatus.valueOf(document.getPropertyValue(AGECmisProperties.STATUS.getId()));
    }

    static String getInitialCreatedBy(Document document) {
        String initialCreatedBy = document.getPropertyValue(AGECmisProperties.INITIAL_CREATED_BY.getId());
        return initialCreatedBy != null ? initialCreatedBy : document.getCreatedBy();
    }

    static Instant getInitialCreationInstant(Document document) {
        GregorianCalendar initialCreationDate = document.getPropertyValue(AGECmisProperties.INITIAL_CREATION_DATE.getId());
        return initialCreationDate != null ? initialCreationDate.toInstant() : getCreationInstant(document);
    }


    public static String getLeosVersionLabel(Document document, Map<String, String>  oldVersions) {
        String versionLabel = document.getPropertyValue(AGECmisProperties.VERSION_LABEL.getId());
        if (StringUtils.isEmpty(versionLabel)) {
            versionLabel = oldVersions.get(document.getId());
        }
        return versionLabel;
    }
    
    public static String getLeosVersionLabel(Document document) {
        return document.getPropertyValue(AGECmisProperties.VERSION_LABEL.getId());
    }

    private static VersionType getVersionType(Document document) {
    	BigInteger versionType = document.getPropertyValue(AGECmisProperties.VERSION_TYPE.getId());
    	// Añadido caso versionType=0 para compatibilidad con objetos antiguos en Documentum
    	if ((versionType != null) && (versionType != BigInteger.valueOf(0))) {
            return VersionType.fromValue(versionType.intValueExact());
        } else if (!document.isMajorVersion()) { // For compatibility with documents with no populated leos:versionType property
            return VersionType.MINOR;
        } else if ((document.getProperty(AGECmisProperties.MILESTONE_COMMENTS.getId()) != null) &&
                (!document.getProperty(AGECmisProperties.MILESTONE_COMMENTS.getId()).getValues().isEmpty())) {
            return VersionType.MAJOR;
        } else {
            return VersionType.INTERMEDIATE;
        }
    }

    private static List<String> getContainedDocuments(Document document) {
        Property<String> containedDocuments = document.getProperty(AGECmisProperties.CONTAINED_DOCUMENTS.getId());
        return containedDocuments.getValues();
    }
    
    //REDMINE 1054 - propiedad lastModifiedBy
    protected static String getLeosLastModifier(Document document) {
    	return document.getPropertyValue(AGECmisProperties.LASTMODIFIER.getId());
	}   
    
    protected static String getLeosCreator(Document document) {
		return document.getPropertyValue(AGECmisProperties.CREATOR.getId());
	}
    
    //Paso a LEOS 3.0.0. - cmis:checkincomment admite solo 120 caracteres en Documentum
    protected static String getLeosCheckinComment(Document document) {
       	String commentOld = (String) document.getPropertyValue(AGECmisProperties.LEOS_CHECKINCOMMENT.getId());
       	String commentNew = "";
       	Property<String> commentExt = document.getProperty(AGECmisProperties.LEOS_CHECKINCOMMENT_EXT.getId());
    	if (commentExt != null) {
    		List<String> commentExtList = commentExt.getValues();
         	if (commentExtList.size() == 0){
            	return commentOld;
            }
         	else {
            	for (String i:commentExtList) {
            		commentNew = commentNew + i;
            	}
            	return commentNew;
         	}
        }
    	else return commentOld;
    }
}
