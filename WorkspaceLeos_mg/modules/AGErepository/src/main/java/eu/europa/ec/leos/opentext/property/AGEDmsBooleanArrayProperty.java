package eu.europa.ec.leos.opentext.property;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Esta clase representa una propiedad multivaluado de carpeta o documento de tipo <code>boolean</code>
 * 
 * @author BosArthur
 */
public class AGEDmsBooleanArrayProperty extends AGEDmsArrayProperty<Boolean> {

	private List<Boolean> values;

	public AGEDmsBooleanArrayProperty(String name) {
		super(name);
		setValues(null);
	}

	public AGEDmsBooleanArrayProperty(String name, Boolean[] values) {
		super(name);
		setValues(values);
	}

	public AGEDmsBooleanArrayProperty(String name, AGEDmsSearchCondition searchCondition, Boolean[] values) {
		super(name);
		setSearchCondition(searchCondition);
		setValues(values);
	}

	public Boolean[] getValues() {
		return this.values.toArray(new Boolean[0]);
	}

	public List<Boolean> getValuesAsList() {
		return this.values;
	}

	public void setValues(Boolean[] values) {
		if (values == null)
			this.values = new ArrayList<>((Collection<Boolean>) Arrays.asList(new Boolean[0]));
		else
			this.values = new ArrayList<>((Collection<Boolean>) Arrays.asList(values));
	}

	public void clearValue() {
		this.values = new ArrayList<>((Collection<Boolean>) Arrays.asList(new Boolean[0]));
	}

	public static AGEDmsBooleanArrayProperty makeProperty(String name, boolean[] values) {
		Boolean[] booleans = new Boolean[values.length];

		for (int i = 0; i < values.length; i++) {
			booleans[i] = Boolean.valueOf(values[i]);
		}
		return new AGEDmsBooleanArrayProperty(name, booleans);
	}

	public Object getValueAsObject() {
		return this.values;
	}

	public Iterator iterateValuesAsObjects() {
		return getValuesAsList().iterator();
	}

	public Iterator iterateValuesAsStrings() {
		ArrayList stringList = new ArrayList(getValuesAsList().size());

		Boolean[] arrayOfBoolean = getValues();
		int i = 0;
		for (int j = arrayOfBoolean.length; i < j;) {
			Boolean value = arrayOfBoolean[i];
			stringList.add(String.valueOf(value));
			i++;
		}
		return stringList.iterator();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AGEDmsBooleanArrayProperty other = (AGEDmsBooleanArrayProperty) obj;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		return true;
	}

	/**
	 * public boolean equals2(Object o) { if (!super.equals(o)) { return false; } DmsBooleanArrayProperty that = (DmsBooleanArrayProperty) o;
	 * 
	 * return this.values != null ? this.values.equals(that.values) : that.values == null; }
	 */
}
