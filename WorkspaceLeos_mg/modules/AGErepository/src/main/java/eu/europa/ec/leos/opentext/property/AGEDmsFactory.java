package eu.europa.ec.leos.opentext.property;

import java.util.List;
import java.util.Map;

import eu.europa.ec.leos.opentext.property.artifacts.AGEArtifact;
import eu.europa.ec.leos.opentext.property.artifacts.AGEAttribute;
import eu.europa.ec.leos.opentext.property.artifacts.AGEAttributeDataType;

/**
 * Esta interfaz define el <i>protocolo</i> de comunicación con un gestor
 * documental de la plataforma @Doc.
 * 
 * @author BosArthur
 */
public interface AGEDmsFactory {

	/**
	 * Este método crea un cabinet para una aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @return identificador del cabinet
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public String createCabinet(String idApplication) throws Exception;

	/**
	 * Este método comprueba si existe el cabinet de una aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @return <i>true</i> si existe, <i>false</i> si no existe
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public boolean existsCabinet(String idApplication) throws Exception;

	/**
	 * Este método devuelve el identificador de un path compuesto por "/"
	 * perteneciente a una aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param path
	 *            el path
	 * 
	 * @return identificador del path
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public String getPathId(String idApplication, String path) throws Exception;

	/**
	 * Este método devuelve el cabinet a la que pertenece la carpeta
	 * 
	 * @param idFolder
	 *            el identificador de la carpeta
	 * 
	 * @return identificador del cabinet
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public String getCabinetFromFolder(String idFolder) throws Exception;

	/**
	 * Este método devuelve el cabinet a la que pertenece el documento
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @return identificador del cabinet
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public String getCabinetFromDocument(String idDocument) throws Exception;

	/**
	 * Este método crea una carpeta raíz perteneciente a una aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param type
	 *            el tipo de carpeta
	 * 
	 * @param name
	 *            el nombre de la carpeta
	 * 
	 * @return identificador de la carpeta
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public String createFolder(String idApplication, String type, String name) throws Exception;

	/**
	 * Este método crea una carpeta debajo de una carpeta padre perteneciente a
	 * una aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param type
	 *            el tipo de carpeta
	 * 
	 * @param name
	 *            el nombre de la carpeta
	 * 
	 * @param idParentFolder
	 *            el identificador de la carpeta padre
	 * 
	 * @return identificador de la carpeta
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public String createFolder(String idApplication, String type, String name, String idParentFolder) throws Exception;

	/**
	 * Este método comprueba si existe una carpeta perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idFolder
	 *            el identificador de la carpeta
	 * 
	 * @return <i>true</i> si existe, <i>false</i> si no existe
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public boolean existsFolder(String idApplication, String idFolder) throws Exception;

	/**
	 * Este método bloquea una carpeta perteneciente a una aplicación cliente.
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idFolder
	 *            el identificador de la carpeta
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public void lockFolder(String idApplication, String idFolder) throws Exception;

	/**
	 * Este método desbloquea una carpeta perteneciente a una aplicación
	 * cliente.
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idFolder
	 *            el identificador de la carpeta
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public void unlockFolder(String idApplication, String idFolder) throws Exception;

	/**
	 * Este método comprueba si una carpeta esta bloqueado perteneciente a una
	 * aplicación cliente.
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idFolder
	 *            el identificador de la carpeta
	 * 
	 * @return <i>true</i> si esta bloqueado, <i>false</i> si no esta bloqueado
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public boolean isLockedFolder(String idApplication, String idFolder) throws Exception;

	/**
	 * Este método devuelve las propiedades especificadas en el parametro
	 * <i>properties</i> de una carpeta perteneciente a una aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idFolder
	 *            el identificador de la carpeta
	 * 
	 * @param properties
	 *            las propiedades que se deben devolver
	 * 
	 * @return propiedades de la carpeta
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public AGEDmsPropertySet getFolderProperties(String idApplication, String idFolder, AGEDmsPropertySet properties) throws Exception;

	/**
	 * Este método devuelve todas las propiedades de una carpeta perteneciente a
	 * una aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idFolder
	 *            el identificador de la carpeta
	 * 
	 * @return propiedades de la carpeta
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public AGEDmsPropertySet getFolderProperties(String idApplication, String idFolder) throws Exception;

	/**
	 * Este método guarda las propiedades de una carpeta perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idFolder
	 *            el identificador de la carpeta
	 * 
	 * @param properties
	 *            las propiedades que se deben guardar
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public void setFolderProperties(String idApplication, String idFolder, AGEDmsPropertySet properties) throws Exception;

	/**
	 * Este método devuelve una propiedad de una carpeta perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idFolder
	 *            el identificador de la carpeta
	 * 
	 * @param property
	 *            la propiedad que se debe devolver
	 * 
	 * @return propiedad de la carpeta
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public AGEDmsProperty getFolderProperty(String idApplication, String idFolder, AGEDmsProperty property) throws Exception;

	/**
	 * Este método guarda una propiedad de una carpeta perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idFolder
	 *            el identificador de la carpeta
	 * 
	 * @param property
	 *            la propiedad que se debe guardar
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public void setFolderProperty(String idApplication, String idFolder, AGEDmsProperty property) throws Exception;

	/**
	 * Este método devuelve el tipo de una carpeta perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idFolder
	 *            el identificador de la carpeta
	 * 
	 * @return tipo de la carpeta
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public String getFolderType(String idApplication, String idFolder) throws Exception;

	/**
	 * Este método elimina una carpeta y su contenido perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idFolder
	 *            el identificador de la carpeta
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public void deleteFolder(String idApplication, String idFolder) throws Exception;

	/**
	 * Este método renombra una carpeta perteneciente a una aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idFolder
	 *            el identificador de la carpeta
	 * 
	 * @param name
	 *            el nuevo nombre de la carpeta
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public void renameFolder(String idApplication, String idFolder, String name) throws Exception;

	/**
	 * Este método crea un documento sin contenido perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param type
	 *            el tipo de documento
	 * 
	 * @param name
	 *            el nombre del documento
	 * 
	 * @param idFolder
	 *            el identificador de la carpeta donde crear el documento
	 * 
	 * @param fileStore
	 *            el filestore donde se guarda el documento
	 * 
	 * @return identificador del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public String createDocument(String idApplication, String type, String name, String idFolder, String fileStore) throws Exception;

	/**
	 * Este método crea un documento con contenido perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param type
	 *            el tipo de documento
	 * 
	 * @param name
	 *            el nombre del documento
	 * 
	 * @param idFolder
	 *            el identificador de la carpeta donde crear el documento
	 * 
	 * @param extension
	 *            la extension del documento
	 * 
	 * @param content
	 *            el contenido del documento
	 * 
	 * @param fileStore
	 *            el filestore donde se guarda el documento
	 * 
	 * @return identificador del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public String createDocument(String idApplication, String type, String name, String idFolder, String extension, byte[] content, String fileStore) throws Exception;

	/**
	 * Este método establece el contenido de un documento perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @param extension
	 *            la extension del documento
	 * 
	 * @param content
	 *            el contenido del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public void setDocumentContent(String idApplication, String idDocument, String extension, byte[] content) throws Exception;

	/**
	 * Este método devuelve el identificador de la carpeta padre de un documento
	 * perteneciente a una aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public String getDocumentParentId(String idApplication, String idDocument) throws Exception;

	/**
	 * Este método devuelve el identificador de la versión de un documento
	 * perteneciente a una aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @param version
	 *            la versión del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public String getDocumentVersionId(String idApplication, String idDocument, String version) throws Exception;

	/**
	 * Este método devuelve el tipo de un documento perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @return tipo del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public String getDocumentType(String idApplication, String idDocument) throws Exception;

	/**
	 * Este método devuelve el contenido de un documento perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @return el contenido del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public byte[] getDocumentContent(String idApplication, String idDocument) throws Exception;

	/**
	 * Este método devuelve el formato de un documento perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @return el contenido del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public String getDocumentContentFormat(String idApplication, String idDocument) throws Exception;

	/**
	 * Este método devuelve el contenido de una versión de un documento
	 * perteneciente a una aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @param version
	 *            la versión del documento
	 * 
	 * @return el contenido de la versión del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public byte[] getDocumentContentByVersion(String idApplication, String idDocument, String version) throws Exception;

	/**
	 * Este método devuelve el formato de una versión de un documento
	 * perteneciente a una aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @param version
	 *            la versión del documento
	 * 
	 * @return el formato de la versión del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public String getDocumentContentFormatByVersion(String idApplication, String idDocument, String version) throws Exception;

	/**
	 * Este método hace un checkout de un documento perteneciente a una
	 * aplicación cliente. Para crear versiones de documentos debe usarse este
	 * método primero y luego el método <i>checkinDocument</i>.
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public void checkoutDocument(String idDocument) throws Exception;

	/**
	 * Este método hace un checkin de un documento perteneciente a una
	 * aplicación cliente. Para crear versiones de documentos debe usarse
	 * primero el método <i>checkoutDocument</i> y luego este método.
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @param extension
	 *            la extension del documento
	 * 
	 * @param content
	 *            el contenido del documento
	 * 
	 * @return el identificador de la versión del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	
	public String altCheckinDocument(String idDocument, String nextVersionLabel, Boolean isMajor, Map<String, Object> updatedProperties, String extension, byte[] content, String comment) throws Exception;
	
	
	public String checkinDocument(String idDocument, String extension, byte[] content) throws Exception;

	/**
	 * Este método cancela el checkout de un documento perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public void cancelCheckoutDocument(String idDocument) throws Exception;

	/**
	 * Este método devuelve las versiones de un documento perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @return las versiones del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public List<String> getDocumentVersions(String idApplication, String idDocument) throws Exception;

	/**
	 * Este método comprueba si existe un documento perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @return <i>true</i> si existe, <i>false</i> si no existe
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public boolean existsDocument(String idApplication, String idDocument) throws Exception;

	/**
	 * Este método devuelve las propiedades especificadas en el parametro
	 * <i>properties</i> de un documento perteneciente a una aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @param properties
	 *            las propiedades que se deben devolver
	 * 
	 * @return propiedades del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public AGEDmsPropertySet getDocumentProperties(String idApplication, String idDocument, AGEDmsPropertySet properties) throws Exception;

	/**
	 * Este método devuelve todas las propiedades de un documento perteneciente
	 * a una aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @return propiedades del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public AGEDmsPropertySet getDocumentProperties(String idApplication, String idDocument) throws Exception;

	/**
	 * Este método guarda las propiedades de un documento perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @param properties
	 *            las propiedades que se deben guardar
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public void setDocumentProperties(String idApplication, String idDocument, AGEDmsPropertySet properties) throws Exception;

	/**
	 * Este método devuelve una propiedad de un documento perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @param property
	 *            la propiedad que se debe devolver
	 * 
	 * @return propiedad del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public AGEDmsProperty getDocumentProperty(String idApplication, String idDocument, AGEDmsProperty property) throws Exception;

	/**
	 * Este método guarda una propiedad de un documento perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @param property
	 *            la propiedad que se debe guardar
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public void setDocumentProperty(String idApplication, String idDocument, AGEDmsProperty property) throws Exception;

	/**
	 * Este método devuelve el filestore de un documento perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @return filestore del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public String getDocumentFileStore(String idApplication, String idDocument) throws Exception;

	/**
	 * Este método establece el filestore de un documento perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @param fileStore
	 *            el filestore donde se guarda el documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public void setDocumentFileStore(String idApplication, String idDocument, String fileStore) throws Exception;

	/**
	 * Este método elimina un documento y sus versiones perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public void deleteDocument(String idApplication, String idDocument) throws Exception;

	/**
	 * Este método renombra un documento perteneciente a una aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @param name
	 *            el nuevo nombre del documento
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public void renameDocument(String idApplication, String idDocument, String name) throws Exception;

	/**
	 * Este método mueve un documento perteneciente a una aplicación cliente a otra carpeta
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idDocument
	 *            el identificador del documento
	 * 
	 * @param idFolderTo
	 *            el identificador de la carpeta destino
	 * 
	 * @throws Exception
	 *             si algo va mal en la llamada del método
	 */
	public void moveDocument(String idApplication, String idDocument, String idFolderTo) throws Exception;

	/**
	 * Este método devuelve los identificadores de los documentos en una
	 * determinada carpeta perteneciente a una aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param idFolder
	 *            el identificador de la carpeta
	 * 
	 * @return los identificadores de los documentos
	 * 
	 * @throws Exception
	 */
	public List<String> getDocumentsInFolder(String idApplication, String idFolder) throws Exception;

	/**
	 * Este método busca documentos que cumplen con los criterios de búsqueda y
	 * devuelve los identificadores de los documentos perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param type
	 *            el tipo de documento
	 * 
	 * @param searchProperties
	 *            los valores de las propiedades que deben tener los documentos
	 * 
	 * @return los identificadores de los documentos
	 * 
	 * @throws Exception
	 */
	public List<String> searchDocuments(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, AGEDmsProperty returnProperty) throws Exception;
	public List<String> searchDocuments(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, AGEDmsProperty returnProperty, int maxSearchResults) throws Exception;

	/**
	 * Este método busca documentos de forma páginada que cumplen con los criterios de búsqueda y
	 * devuelve los identificadores de los documentos perteneciente a una
	 * aplicación cliente. Tambien devuelve el número de documentos encontrados.
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param type
	 *            el tipo de documento
	 * 
	 * @param searchProperties
	 *            los valores de las propiedades que deben tener los documentos
	 *            
	 * @param returnProperties
	 *            las propiedades que se devuelven como resultado
	 *            
	 * @param orderProperty
	 *            la propiedad para ordenar los resultados
	 *            
	 * @param startIndex
	 *            el indice de inicio
	 * 
	 * @param pageSize
	 *            el número de documentos devueltos
	 * 
	 * @return los identificadores de los documentos, el hit count y si tiene más resultados
	 * 
	 * @throws Exception
	 */
	public AGEDmsSearchResult searchDocumentsPaged(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, List<AGEDmsProperty> returnProperties, AGEDmsProperty orderProperty, Boolean isAscending, int startIndex, int pageSize) throws Exception;
	public AGEDmsSearchResult searchDocumentsPaged(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, AGEDmsProperty returnProperty, AGEDmsProperty orderProperty, Boolean isAscending, int startIndex, int pageSize) throws Exception;

	/**
	 * Este método busca documentos que cumplen con el criterio de fulltext y
	 * devuelve los identificadores de los documentos perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param type
	 *            el tipo de documento
	 * 
	 * @param searchProperties
	 *            los valores de las propiedades que deben tener los documentos
	 *            
	 * @param searchText
	 *            el texto que deben contener los documentos
	 * 
	 * @param returnProperties
	 *            las propiedades que se devuelven como resultado
	 *            
	 * @param orderProperty
	 *            la propiedad para ordenar los resultados
	 *            
	 * @param maxResults
	 *            el número máximo de documentos devueltos
	 *            
	 * @return los identificadores de los documentos, el hit count y si tiene más resultados
	 * 
	 * @throws Exception
	 */
	public AGEDmsSearchResult searchDocumentsFullText(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, String searchText, List<AGEDmsProperty> returnProperties, AGEDmsProperty orderProperty, Boolean isAscending, int maxResults) throws Exception;
	public AGEDmsSearchResult searchDocumentsFullText(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, String searchText, AGEDmsProperty returnProperty, AGEDmsProperty orderProperty, Boolean isAscending, int maxResults) throws Exception;

	/**
	 * Este método busca carpetas que cumplen con los criterios de búsqueda y
	 * devuelve los identificadores de las carpetas perteneciente a una
	 * aplicación cliente
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param type
	 *            el tipo de carpeta
	 * 
	 * @param searchProperties
	 *            los valores de las propiedades que deben tener las carpetas
	 * 
	 * @return los identificadores de las carpetas
	 * 
	 * @throws Exception
	 */
	public List<String> searchFolders(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, AGEDmsProperty returnProperty) throws Exception;
	public List<String> searchFolders(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, AGEDmsProperty returnProperty, int maxSearchResults) throws Exception;

	/**
	 * Este método busca carpetas de forma paginada que cumplen con los criterios de búsqueda y
	 * devuelve los identificadores de las carpetas perteneciente a una
	 * aplicación cliente. Tambien devuelve el número de folders encontrados.
	 * 
	 * @param idApplication
	 *            id de aplicación
	 * 
	 * @param type
	 *            el tipo de carpeta
	 * 
	 * @param searchProperties
	 *            los valores de las propiedades que deben tener las carpetas
	 * 
	 * @param returnProperties
	 *            propiedades que se devuelven como resultado
	 *            
	 * @param orderProperty
	 *            la propiedad para ordenar los resultados
	 *            
	 * @param startIndex
	 *            el indice de inicio
	 * 
	 * @param pageSize
	 *            el número de carpetas devueltas
	 *            
	 * @return los identificadores de las carpetas y el hit count
	 * 
	 * @throws Exception
	 */
	public AGEDmsSearchResult searchFoldersPaged(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, List<AGEDmsProperty> returnProperties, AGEDmsProperty orderProperty, Boolean isAscending, int startIndex, int pageSize) throws Exception;
	public AGEDmsSearchResult searchFoldersPaged(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, AGEDmsProperty returnProperty, AGEDmsProperty orderProperty, Boolean isAscending, int startIndex, int pageSize) throws Exception;

	/**
	 * Este método devuelve los atributos de un artefacto
	 * 
	 * @param artifactTypeName
	 *            el nombre del artefacto
	 * 
	 * @return los atributos del artefacto
	 * 
	 * @throws Exception
	 */
	public List<AGEAttribute> getAttributes(String artifactTypeName) throws Exception;

	/**
	 * Este método devuelve el tipo de dato de un atributo de un artefacto
	 * 
	 * @param artifactTypeName
	 *            el nombre del artefacto
	 * 
	 * @param attributeName
	 *            el nombre del atributo
	 * 
	 * @return el tipo de dato del atributo
	 * 
	 * @throws Exception
	 */
	public AGEAttributeDataType getAttributeDataType(String artifactTypeName, String attributeName) throws Exception;

	/**
	 * Este método devuelve la longitud de un atributo de un artefacto
	 * 
	 * @param artifactTypeName
	 *            el nombre del artefacto
	 * 
	 * @param attributeName
	 *            el nombre del atributo
	 * 
	 * @return la longitud del atributo
	 * 
	 * @throws Exception
	 */
	public Integer getAttributeLength(String artifactTypeName, String attributeName) throws Exception;

	/**
	 * Este método devuelve si un atributo de un artefacto es multivaluado
	 * 
	 * @param artifactTypeName
	 *            el nombre del artefacto
	 * 
	 * @param attributeName
	 *            el nombre del atributo
	 * 
	 * @return <i>true</i>el atributo es multivaluado, <i>false</i> de lo
	 *         contrario
	 * 
	 * @throws Exception
	 */
	public boolean isAttributeRepeating(String artifactTypeName, String attributeName) throws Exception;

	/**
	 * Este método devuelve los valores por defecto de un atributo de un
	 * artefacto
	 * 
	 * @param artifactTypeName
	 *            el nombre del artefacto
	 * 
	 * @param attributeName
	 *            el nombre del atributo
	 * 
	 * @return los valores por defecto del atributo
	 * 
	 * @throws Exception
	 */
	public List<String> getAttributeDefaultValues(String artifactTypeName, String attributeName) throws Exception;

	/**
	 * Este método obliga a recargar la definición de los atributos
	 * 
	 * @throws Exception
	 */
	public void reloadAttributes() throws Exception;

	/**
	 * Este método ejecuta una query y devuelve el resultado en una lista
	 * 
	 * @param query
	 *            la query
	 * 
	 * @return el resultado
	 * 
	 * @throws Exception
	 */
	public List<AGEDmsPropertySet> executeQuery(String query) throws Exception;

	/**
	 * Este método crea artefactos
	 * 
	 * @param artifacts
	 *            los artefactos
	 * 
	 * @throws Exception
	 */
	public void deployArtifacts(List<AGEArtifact> artifacts) throws Exception;

}