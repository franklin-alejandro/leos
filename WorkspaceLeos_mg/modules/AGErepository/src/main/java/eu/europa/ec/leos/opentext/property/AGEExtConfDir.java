package eu.europa.ec.leos.opentext.property;

public class AGEExtConfDir {

	public static String getDir() {
		String extConfDir = System.getenv("/");
		//String extConfDir = System.getenv("EXT_CONF_DIR");
		//if (extConfDir == null || extConfDir.isEmpty()) extConfDir = System.getProperty("EXT_CONF_DIR");

		if (extConfDir == null || extConfDir.isEmpty())
			throw new RuntimeException("EXT_CONF_DIR not set as environment variable or system property.");

		extConfDir = extConfDir.replace('\\', '/');
		if (extConfDir.endsWith("/"))
			return extConfDir.substring(0, extConfDir.length() - 1);
		else
			return extConfDir;
	}
	
	public static String getFilePath(String file) {
		file = file.replace('\\', '/');
		String filePath;
		if (file.startsWith("/"))
			filePath = getDir().concat(file);
		else
			filePath = getDir().concat("/").concat(file);
		return filePath;
	}
	
	public static String getFilePathExpanded(String file) {
		return file.replaceFirst("\\$EXT_CONF_DIR", getDir());
	}
	
	public static void main(String [] args) {
		System.setProperty("EXT_CONF_DIR",  "C:\\test");
		System.out.println(getFilePath("test.txt"));
		System.out.println(getFilePath("\\test.txt"));
		System.setProperty("EXT_CONF_DIR",  "C:\\test\\");
		System.out.println(getFilePath("test.txt"));
		System.out.println(getFilePath("\\test.txt"));
		System.setProperty("EXT_CONF_DIR",  "/test");
		System.out.println(getFilePath("test.txt"));
		System.out.println(getFilePath("/test.txt"));
		System.setProperty("EXT_CONF_DIR",  "/test/");
		System.out.println(getFilePath("test.txt"));
		System.out.println(getFilePath("/test.txt"));
	}
}