package eu.europa.ec.leos.opentext.property;

import java.util.List;

public class AGEDmsSearchResult {

	private int hitCount;
	private List<AGEDmsPropertySet> result;
	private boolean moreResults;

	public int getHitCount() {
		return hitCount;
	}

	public void setHitCount(int hitCount) {
		this.hitCount = hitCount;
	}

	public List<AGEDmsPropertySet> getResult() {
		return result;
	}

	public void setResult(List<AGEDmsPropertySet> result) {
		this.result = result;
	}

	public boolean hasMoreResults() {
		return moreResults;
	}

	public void setMoreResults(boolean moreResults) {
		this.moreResults = moreResults;
	}

}
