/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.cmis;

import eu.europa.ec.leos.cmis.mapping.AGECmisMapper;
import eu.europa.ec.leos.domain.cmis.document.XmlDocument;
import org.apache.chemistry.opencmis.client.api.DocumentType;
import org.apache.chemistry.opencmis.client.api.FileableCmisObject;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.Repository;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.api.Tree;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.RepositoryInfo;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.List;

public class AGECmisSessionFactoryImpl extends CmisSessionFactoryImpl {

    @Autowired
    private SessionFactory sessionFactory;
    @Value("${leos.cmis.repository.id}")
    private String repositoryId;
    @Value("${leos.cmis.repository.binding}")
    private String repositoryBinding;
    @Value("${leos.cmis.ws.repository.url}")
    private String repositoryRepositoryServiceWsUrl;
    @Value("${leos.cmis.atom.repository.url}")
    private String repositoryRepositoryAtomUrl;
    @Value("${leos.cmis.browser.repository.url}")
    private String repositoryRepositoryBrowserUrl;
    @Value("${leos.cmis.ws.discovery.url}")
    private String repositoryDiscoveryServiceWsUrl;
    @Value("${leos.cmis.ws.multifiling.url}")
    private String repositoryMultiFilingServiceWsUrl;
    @Value("${leos.cmis.ws.navigation.url}")
    private String repositoryNavigationServiceWsUrl;
    @Value("${leos.cmis.ws.object.url}")
    private String repositoryObjectServiceWsUrl;
    @Value("${leos.cmis.ws.policy.url}")
    private String repositoryPolicyServiceWsUrl;
    @Value("${leos.cmis.ws.relationship.url}")
    private String repositoryRelationshipServiceWsUrl;
    @Value("${leos.cmis.ws.versioning.url}")
    private String repositoryVersioningServiceWsUrl;
    @Value("${leos.cmis.ws.acl.url}")
    private String repositoryAclServiceWsUrl;
    @Value("${leos.cmis.ws.authentication.provider.class}")
    private String repositoryAuthProviderClass;
    @Value("${leos.cmis.httpInvoker.class:}")
    private String httpInvokerClass;

	@Value("${leos.cmis.repository.username:}")
    private String username;
	@Value("${leos.cmis.repository.password:}")
    private String password;
    @Value("${leos.cmis.repository.auth.http.basic:}")
    private String authhttpbasic;
    @Value("${leos.cmis.repository.auth.http.oauth.bearer:}")
    private String httpoauthbearer;
    @Value("${leos.cmis.repository.auth.soap.usernametoken:}")
    private String usernametoken;
    @Value("${leos.cmis.repository.ReadTimeout:}")
    private String readtimeout;
    @Value("${leos.cmis.repository.connectTimeout:}")
    private String connecttimeout;
    @Value("${leos.cmis.repository.cookies:}")
    private String repositorycookies;
    
    private static final Logger LOG = LoggerFactory.getLogger(AGECmisSessionFactoryImpl.class);

    public Session createSession() {

        LOG.info("Creating a CMIS session...");
        Map<String, String> parameters = new HashMap<>();
//		  puesto que vamos a obtner el REPOSITORY_ID consultando la lista de repositorios mejor dejarlo a NULL
//        parameters.put(SessionParameter.REPOSITORY_ID, repositoryId);
        BindingType bindingType = BindingType.fromValue(repositoryBinding);
        Objects.requireNonNull(bindingType);
        parameters.put(SessionParameter.BINDING_TYPE, bindingType.value());
        if (StringUtils.isNotBlank(httpInvokerClass)) {
            LOG.info("Overriding default HTTP invoker with class: {}", httpInvokerClass);
            parameters.put(SessionParameter.HTTP_INVOKER_CLASS, httpInvokerClass);
        }
        parameters.put(SessionParameter.AUTHENTICATION_PROVIDER_CLASS, repositoryAuthProviderClass);

        parameters.put(SessionParameter.USER, username);
        parameters.put(SessionParameter.PASSWORD, password);
        parameters.put(SessionParameter.AUTH_HTTP_BASIC, authhttpbasic);
        parameters.put(SessionParameter.AUTH_OAUTH_BEARER, httpoauthbearer);
        parameters.put(SessionParameter.AUTH_SOAP_USERNAMETOKEN, usernametoken);
        parameters.put(SessionParameter.CONNECT_TIMEOUT, readtimeout);
        parameters.put(SessionParameter.READ_TIMEOUT, connecttimeout);
        parameters.put(SessionParameter.COOKIES, repositorycookies);
        
        switch (bindingType) {
            case WEBSERVICES:
                parameters.put(SessionParameter.WEBSERVICES_JAXWS_IMPL, "cxf");
                parameters.put(SessionParameter.WEBSERVICES_ACL_SERVICE, repositoryAclServiceWsUrl);
                parameters.put(SessionParameter.WEBSERVICES_DISCOVERY_SERVICE, repositoryDiscoveryServiceWsUrl);
                parameters.put(SessionParameter.WEBSERVICES_MULTIFILING_SERVICE, repositoryMultiFilingServiceWsUrl);
                parameters.put(SessionParameter.WEBSERVICES_NAVIGATION_SERVICE, repositoryNavigationServiceWsUrl);
                parameters.put(SessionParameter.WEBSERVICES_OBJECT_SERVICE, repositoryObjectServiceWsUrl);
                parameters.put(SessionParameter.WEBSERVICES_POLICY_SERVICE, repositoryPolicyServiceWsUrl);
                parameters.put(SessionParameter.WEBSERVICES_RELATIONSHIP_SERVICE, repositoryRelationshipServiceWsUrl);
                parameters.put(SessionParameter.WEBSERVICES_REPOSITORY_SERVICE, repositoryRepositoryServiceWsUrl);
                parameters.put(SessionParameter.WEBSERVICES_VERSIONING_SERVICE, repositoryVersioningServiceWsUrl);
                break;
            case ATOMPUB:
                parameters.put(SessionParameter.ATOMPUB_URL, repositoryRepositoryAtomUrl);
                break;
            case BROWSER:
                parameters.put(SessionParameter.BROWSER_URL, repositoryRepositoryBrowserUrl);
                break;
            default:
                throw new IllegalArgumentException("Repository binding of type '" + repositoryBinding + "' is not supported!");
        }

//        Session session = sessionFactory.createSession(parameters);

//		FORMA INDIRECTA DE CONEXION

		LOG.info("URL DE atomPub: {}", parameters.get(SessionParameter.ATOMPUB_URL ));
		LOG.info("Repositorio: {}", parameters.get(SessionParameter.REPOSITORY_ID ));
		LOG.info("SessionParameters: {}", parameters.toString() );
        
		List<Repository> sessionrepos = sessionFactory.getRepositories(parameters);
		LOG.info("Número repos: {}", sessionrepos.size() );
		
		int repo = 0;
		int i = 0;
		while ( i < sessionrepos.size() ) {
			if (sessionrepos.get(i).getName() == repositoryId) {
					repo=i;
			}
			i++;
		}
		Repository sessionRepository = sessionrepos.get(repo);
		Session session = sessionRepository.createSession();
	
		
        Objects.requireNonNull(session);
        // KLUGE LEOS-2398 completely disable client-side session cache by default
        session.getDefaultContext().setCacheEnabled(false);
        // KLUGE LEOS-2369 load all properties by default
        session.getDefaultContext().setFilterString("*");
        
        
        //MGM prueba get folder de leos de session 
//        Folder folder = (Folder) session.getObjectByPath("/");
//        otro interesante es el getRootFolder();8
//        List<Tree<FileableCmisObject>> folderStructure = folder.getFolderTree(2);
        session.getRootFolder();
        String objectId = null;
		String queryString = "select cmis:objectId from cmis:folder where cmis:name = '" + "leos" + "'";
    	ItemIterable<QueryResult> results = session.query(queryString, false);
    	for (QueryResult qResult : results) {
    		objectId = qResult.getPropertyValueByQueryName("cmis:objectId");
    	}
    	//Aqui si que encuentra el id de leos."guid": "5cfd9fb4-9ce2-4a5f-a3ff-feb483c9b031",
        
        // FIXME find a better place to log the repository info on demand
        logRepositoryInfo(session);
//        MGM comento para que salte la parte de bindear metadatos(probando)
//        checkMandatoryCapabilities(session);
        return session;
    }
    //this method checks for mandatory capabilities. if not then throw exception.
    private void checkMandatoryCapabilities(Session session) {
        DocumentType leosDocType = (DocumentType) session.getTypeDefinition(AGECmisMapper.cmisPrimaryType(XmlDocument.class));
        //1. if leos:Document type is available
        Objects.requireNonNull(leosDocType, "leos:document type is not defined");
        LOG.debug("Leos Document Type is defined");

        //2. check if the leos document type is versionable
        if (!leosDocType.isVersionable()) {
            throw new IllegalStateException("leos:document is not a versionable!");
        }
        LOG.debug("Leos Document Type is versionable: {}", leosDocType.isVersionable());
    }

    private void logRepositoryInfo(Session session) {

        if (LOG.isDebugEnabled()) {
            RepositoryInfo repositoryInfo = session.getRepositoryInfo();
            LOG.debug("Repository information...");
            LOG.debug("Repository vendor name: {}", repositoryInfo.getVendorName());
            LOG.debug("Repository product name: {}", repositoryInfo.getProductName());
            LOG.debug("Repository product version: {}", repositoryInfo.getProductVersion());
            LOG.debug("Repository id: {}", repositoryInfo.getId());
            LOG.debug("Repository name: {}", repositoryInfo.getName());
            LOG.debug("Repository description: {}", repositoryInfo.getDescription());
            LOG.debug("Repository CMIS version supported: {}", repositoryInfo.getCmisVersionSupported());
        }

    }
    
}
