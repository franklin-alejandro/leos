/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.repository.document;

import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.AGECantoDorado;
import eu.europa.ec.leos.domain.cmis.metadata.AGECantoDoradoMetadata;
import eu.europa.ec.leos.model.filter.QueryFilter;
import org.springframework.security.access.prepost.PostAuthorize;

import java.util.List;
import java.util.stream.Stream;

/**
 * cantoDorado Repository interface.
 * <p>
 * Represents collections of *cantoDorado* documents, with specific methods to persist and retrieve.
 * Allows CRUD operations based on strongly typed Business Entities: [cantoDorado] and [cantoDoradoMetadata].
 */
public interface AGECantoDoradoRepository {

    /**
     * Creates a [cantoDorado] document from a given template and with the specified characteristics.
     *
     * @param templateId the ID of the template for the cantoDorado.
     * @param path       the path where to create the cantoDorado.
     * @param name       the name of the cantoDorado.
     * @param metadata   the metadata of the cantoDorado.
     * @return the created Canto Dorado document.
     */
	AGECantoDorado createCantoDorado(String templateId, String path, String name, AGECantoDoradoMetadata metadata);

    /**
     * Creates a [cantoDorado] document from a given content and with the specified characteristics.
     *
     * @param path     the path where to create the cantoDorado.
     * @param name     the name of the cantoDorado.
     * @param metadata the metadata of the cantoDorado.
     * @param content  the content of the cantoDorado.
     * @return the created Canto Dorado document.
     */
	AGECantoDorado createCantoDoradoFromContent(String path, String name, AGECantoDoradoMetadata metadata, byte[] content);

    /**
     * Updates a [cantoDorado] document with the given metadata.
     *
     * @param id       the ID of the cantoDorado document to update.
     * @param metadata the metadata of the cantoDorado.
     * @return the updated cantoDorado document.
     */
	AGECantoDorado updateCantoDorado(String id, AGECantoDoradoMetadata metadata);

    /**
     * Updates a [cantoDorado] document with the given content.
     *
     * @param id      the ID of the cantoDorado document to update.
     * @param content the content of the cantoDorado.
     * @param versionType the version type to be created
     * @param comment the comment of the update, optional.
     * @return the updated cantoDorado document.
     */
	AGECantoDorado updateCantoDorado(String id, byte[] content, VersionType versionType, String comment);

    /**
     * Updates a [CantoDorado] document with the given metadata and content.
     *
     * @param id       the ID of the cantoDorado document to update.
     * @param metadata the metadata of the cantoDorado.
     * @param content  the content of the cantoDorado.
     * @param versionType  the version type to be created
     * @param comment  the comment of the update, optional.
     * @return the updated cantoDorado document.
     */
	AGECantoDorado updateCantoDorado(String id, AGECantoDoradoMetadata metadata, byte[] content, VersionType versionType, String comment);

	AGECantoDorado updateMilestoneComments(String id, List<String> milestoneComments, byte[] content, VersionType versionType, String comment);

	AGECantoDorado updateMilestoneComments(String id, List<String> milestoneComments);

    /**
     * Finds a [CantoDorado] document with the specified characteristics.
     *
     * @param id     the ID of the Canto Dorado document to retrieve.
     * @param latest retrieves the latest version of the proposal document, when *true*.
     * @return the found Canto Dorado document.
     */
	AGECantoDorado findCantoDoradoById(String id, boolean latest);

    /**
     * Finds all versions of a [Canto Dorado] document with the specified characteristics.
     *
     * @param id           the ID of the Canto Dorado document to retrieve.
     * @param fetchContent streams the content
     * @return the list of found Canto Dorado document versions or empty.
     */
    List<AGECantoDorado> findCantoDoradoVersions(String id, boolean fetchContent);

    /**
     * Finds a [Canto Dorado] document with the specified characteristics.
     *
     * @param ref the reference metadata of the Canto Dorado document to retrieve.
     * @return the found Canto Dorado document.
     */
    @PostAuthorize("hasPermission(returnObject, 'CAN_READ')")
    AGECantoDorado findCantoDoradoByRef(String ref);
 
    List<AGECantoDorado> findAllMinorsForIntermediate(String docRef, String currIntVersion, String prevIntVersion, int startIndex, int maxResults);
    
    int findAllMinorsCountForIntermediate(String docRef, String currIntVersion, String prevIntVersion);
    
    Integer findAllMajorsCount(String docRef);
        
    List<AGECantoDorado> findRecentMinorVersions(String documentId, String documentRef, int startIndex, int maxResults);
    
    Integer findRecentMinorVersionsCount(String documentId, String documentRef);

	void deleteCantoDorado(String id);

	List<AGECantoDorado> findAllMajors(String docRef, int startIndex, int maxResult);
}
