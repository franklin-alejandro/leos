/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.repository.document;

import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.AGEMain;
import eu.europa.ec.leos.domain.cmis.metadata.AGEMainMetadata;
import eu.europa.ec.leos.model.filter.QueryFilter;
import eu.europa.ec.leos.repository.LeosRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Stream;

/**
 * Main Repository implementation.
 *
 * @constructor Creates a specific Main Repository, injected with a generic LEOS Repository.
 */
@Repository
public class AGEMainRepositoryImpl implements AGEMainRepository {

    private static final Logger logger = LoggerFactory.getLogger(AGEMainRepositoryImpl.class);

    private final LeosRepository leosRepository;

    @Autowired
    public AGEMainRepositoryImpl(LeosRepository leosRepository) {
        this.leosRepository = leosRepository;
    }

    @Override
    public AGEMain createMain(String templateId, String path, String name, AGEMainMetadata metadata) {
        logger.debug("Creating Main... [template=" + templateId + ", path=" + path + ", name=" + name + "]");
        return leosRepository.createDocument(templateId, path, name, metadata, AGEMain.class);
    }

    @Override
    public AGEMain createMainFromContent(String path, String name, AGEMainMetadata metadata, byte[] content) {
        logger.debug("Creating Main From Content... [path=" + path + ", name=" + name + "]");
        return leosRepository.createDocumentFromContent(path, name, metadata, AGEMain.class, LeosCategory.MEMORANDUM.name(), content);
    }

    @Override
    public AGEMain updateMain(String id, AGEMainMetadata metadata) {
        logger.debug("Updating Main metadata... [id=" + id + "]");
        return leosRepository.updateDocument(id, metadata, AGEMain.class);
    }

    @Override
    public AGEMain updateMain(String id, byte[] content, VersionType versionType, String comment) {
        logger.debug("Updating Main content... [id=" + id + "]");
        return leosRepository.updateDocument(id, content, versionType, comment, AGEMain.class);
    }

    @Override
    public AGEMain updateMain(String id, AGEMainMetadata metadata, byte[] content, VersionType versionType, String comment) {
        logger.debug("Updating Main metadata and content... [id=" + id + "]");
        return leosRepository.updateDocument(id, metadata, content, versionType, comment, AGEMain.class);
    }

    @Override
    public AGEMain updateMilestoneComments(String id, List<String> milestoneComments, byte[] content, VersionType versionType, String comment) {
        logger.debug("Updating Main milestoneComments... [id=" + id + "]");
        return leosRepository.updateMilestoneComments(id, content, milestoneComments, versionType, comment, AGEMain.class);
    }

    @Override
    public AGEMain updateMilestoneComments(String id, List<String> milestoneComments) {
        logger.debug("Updating Main milestoneComments... [id=" + id + "]");
        return leosRepository.updateMilestoneComments(id, milestoneComments, AGEMain.class);
    }

    @Override
    public AGEMain findMainById(String id, boolean latest) {
        logger.debug("Finding Main by ID... [id=" + id + ", latest=" + latest + "]");
        return leosRepository.findDocumentById(id, AGEMain.class, latest);
    }

    @Override
    public List<AGEMain> findMainVersions(String id, boolean fetchContent) {
        logger.debug("Finding Main versions... [id=" + id + "]");
        return leosRepository.findDocumentVersionsById(id, AGEMain.class, fetchContent);
    }

    @Override
    public AGEMain findMainByRef(String ref) {
        logger.debug("Finding Main by ref... [ref=" + ref + "]");
        return leosRepository.findDocumentByRef(ref, AGEMain.class);
    }
    
    @Override
    public List<AGEMain> findAllMinorsForIntermediate(String docRef, String currIntVersion, String prevIntVersion, int startIndex, int maxResults) {
        logger.debug("Finding Main versions between intermediates...");
        return leosRepository.findAllMinorsForIntermediate(AGEMain.class, docRef, currIntVersion, prevIntVersion, startIndex, maxResults);
    }
    
    @Override
    public int findAllMinorsCountForIntermediate(String docRef, String currIntVersion, String prevIntVersion) {
    	logger.debug("Finding Main minor versions count between intermediates...");
        return leosRepository.findAllMinorsCountForIntermediate(AGEMain.class, docRef, currIntVersion, prevIntVersion);
    }
    
    @Override
    public Integer findAllMajorsCount(String docRef) {
        return leosRepository.findAllMajorsCount(AGEMain.class, docRef);
    }
    
    @Override
    public List<AGEMain> findAllMajors(String docRef, int startIndex, int maxResult) {
        return leosRepository.findAllMajors(AGEMain.class, docRef, startIndex, maxResult);
    }
    
    @Override
    public List<AGEMain> findRecentMinorVersions(String documentId, String documentRef, int startIndex, int maxResults) {
        final AGEMain report = leosRepository.findLatestMajorVersionById(AGEMain.class, documentId);
        return leosRepository.findRecentMinorVersions(AGEMain.class, documentRef, report.getCmisVersionLabel(), startIndex, maxResults);
    }
    
    @Override
    public Integer findRecentMinorVersionsCount(String documentId, String documentRef) {
        final AGEMain report = leosRepository.findLatestMajorVersionById(AGEMain.class, documentId);
        return leosRepository.findRecentMinorVersionsCount(AGEMain.class, documentRef, report.getCmisVersionLabel());
    }
    
    @Override
    public void deleteMain(String id) {
        logger.debug("Deleting Main... [id=" + id + "]");
        leosRepository.deleteDocumentById(id);
    }
    
}
