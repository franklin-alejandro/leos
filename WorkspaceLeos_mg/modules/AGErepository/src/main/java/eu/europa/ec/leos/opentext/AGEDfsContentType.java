package eu.europa.ec.leos.opentext;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "dm_format_s")
@XmlAccessorType(XmlAccessType.FIELD)
public class AGEDfsContentType {

	@XmlElement(name = "name")
	private String name;
	
	@XmlElement(name = "dos_extension")
	private String dosExtension;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDosExtension() {
		return dosExtension;
	}
	
	public void setDosExtension(String dosExtension) {
		this.dosExtension = dosExtension;
	}

	@Override
	public String toString() {
		return "DfsContentType [name=" + name + ", dosExtension="
				+ dosExtension + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dosExtension == null) ? 0 : dosExtension.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AGEDfsContentType other = (AGEDfsContentType) obj;
		if (dosExtension == null) {
			if (other.dosExtension != null)
				return false;
		} else if (!dosExtension.equals(other.dosExtension))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}