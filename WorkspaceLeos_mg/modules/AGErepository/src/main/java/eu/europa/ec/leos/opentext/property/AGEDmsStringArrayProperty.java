package eu.europa.ec.leos.opentext.property;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Esta clase representa una propiedad multivaluado de carpeta o documento 
 * de tipo <code>java.util.Date</code>
 * 
 * @author BosArthur
 */
public class AGEDmsStringArrayProperty extends AGEDmsArrayProperty<String> {
	
	private List<String> values;

	public AGEDmsStringArrayProperty(String name) {
		super(name);
		setValues(null);
	}

	public AGEDmsStringArrayProperty(String name, String[] values) {
		super(name);
		setValues(values);
	}

	public AGEDmsStringArrayProperty(String name, AGEDmsSearchCondition searchCondition, String[] values) {
		super(name);
		setSearchCondition(searchCondition);
		setValues(values);
	}

	public String[] getValues() {
		return this.values.toArray(new String[0]);
	}

	public List<String> getValuesAsList() {
		return this.values;
	}

	public void setValues(String[] values) {
		if (values == null)
			this.values = new ArrayList((Collection)Arrays.asList(new String[0]));
		else
			this.values = new ArrayList((Collection)Arrays.asList(values));
	}

	public void clearValue() {
		this.values = new ArrayList((Collection)Arrays.asList(new String[0]));
	}

	public Object getValueAsObject() {
		return getValues();
	}

	public Iterator iterateValuesAsObjects() {
		return getValuesAsList().iterator();
	}

	public Iterator iterateValuesAsStrings() {
		ArrayList stringList = new ArrayList(getValuesAsList().size());

		String[] arrayOfString = getValues();
		int i = 0;
		for (int j = arrayOfString.length; i < j; ) {
			String value = arrayOfString[i];
			stringList.add(value); 
			i++;
		}
		return stringList.iterator();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AGEDmsStringArrayProperty other = (AGEDmsStringArrayProperty) obj;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		return true;
	}

	/**
	public boolean equals(Object o) {
		if (!super.equals(o)) {
			return false;
		}
		DmsStringArrayProperty that = (DmsStringArrayProperty) o;

		return this.values != null ? this.values.equals(that.values) : that.values == null;
	}*/
}
