package eu.europa.ec.leos.opentext.property;

import java.util.Date;

import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang.ArrayUtils;

/**
 * Esta clase representa una colección de propiedades de carpeta o documento. La
 * clase permite propiedades duplicadas (propiedades con el mismo nombre) para
 * poder establecer diferentes criterios de búsqueda.
 * 
 * @author BosArthur
 */
public class AGEDmsSearchPropertySet {

	private MultiValueMap propertyMap;

	/**
	 * Initializa una colección vacia
	 */
	public AGEDmsSearchPropertySet() {
		propertyMap = new MultiValueMap();
	}

	/**
	 * Añade una propiedad de tipo <code>DmsBooleanProperty</code> a la
	 * colección. Si la colección ya contiene una propiedad con el mismo nombre
	 * se añadira la propiedad en la colección.
	 * 
	 * @param name
	 *            el nombre de la propiedad
	 * 
	 * @param searchCondition
	 *            la condición de búsqueda
	 * 
	 * @param value
	 *            el valor de la propiedad
	 * 
	 * @return un <code>DmsBooleanProperty</code>
	 * 
	 * @throws DmsPropertyException
	 *             si algo va mal en la llamada del método
	 */
	public AGEDmsProperty add(String name, AGEDmsSearchCondition searchCondition, boolean value) throws Exception {
		return add(name, searchCondition, Boolean.valueOf(value));
	}

	/**
	 * Añade una propiedad de tipo <code>DmsNumberProperty</code> a la
	 * colección. Si la colección ya contiene una propiedad con el mismo nombre
	 * se añadira la propiedad en la colección.
	 * 
	 * @param name
	 *            el nombre de la propiedad
	 * 
	 * @param searchCondition
	 *            la condición de búsqueda
	 * 
	 * @param value
	 *            el valor de la propiedad
	 * 
	 * @return un <code>DmsNumberProperty</code>
	 * 
	 * @throws DmsPropertyException
	 *             si algo va mal en la llamada del método
	 */
	public AGEDmsProperty add(String name, AGEDmsSearchCondition searchCondition, byte value) throws Exception {
		return add(name, searchCondition, Byte.valueOf(value));
	}

	/**
	 * Añade una propiedad de tipo <code>DmsNumberProperty</code> a la
	 * colección. Si la colección ya contiene una propiedad con el mismo nombre
	 * se añadira la propiedad en la colección.
	 * 
	 * @param name
	 *            el nombre de la propiedad
	 * 
	 * @param searchCondition
	 *            la condición de búsqueda
	 * 
	 * @param value
	 *            el valor de la propiedad
	 * 
	 * @return un <code>DmsNumberProperty</code>
	 * 
	 * @throws DmsPropertyException
	 *             si algo va mal en la llamada del método
	 */
	public AGEDmsProperty add(String name, AGEDmsSearchCondition searchCondition, short value) throws Exception {
		return add(name, searchCondition, Short.valueOf(value));
	}

	/**
	 * Añade una propiedad de tipo <code>DmsNumberProperty</code> a la
	 * colección. Si la colección ya contiene una propiedad con el mismo nombre
	 * se añadira la propiedad en la colección.
	 * 
	 * @param name
	 *            el nombre de la propiedad
	 * 
	 * @param searchCondition
	 *            la condición de búsqueda
	 * 
	 * @param value
	 *            el valor de la propiedad
	 * 
	 * @return un <code>DmsNumberProperty</code>
	 * 
	 * @throws DmsPropertyException
	 *             si algo va mal en la llamada del método
	 */
	public AGEDmsProperty add(String name, AGEDmsSearchCondition searchCondition, int value) throws Exception {
		return add(name, searchCondition, Integer.valueOf(value));
	}

	/**
	 * Añade una propiedad de tipo <code>DmsNumberProperty</code> a la
	 * colección. Si la colección ya contiene una propiedad con el mismo nombre
	 * se añadira la propiedad en la colección.
	 * 
	 * @param name
	 *            el nombre de la propiedad
	 * 
	 * @param searchCondition
	 *            la condición de búsqueda
	 * 
	 * @param value
	 *            el valor de la propiedad
	 * 
	 * @return un <code>DmsNumberProperty</code>
	 * 
	 * @throws DmsPropertyException
	 *             si algo va mal en la llamada del método
	 */
	public AGEDmsProperty add(String name, AGEDmsSearchCondition searchCondition, long value) throws Exception {
		return add(name, searchCondition, Long.valueOf(value));
	}

	/**
	 * Añade una propiedad de tipo <code>DmsNumberProperty</code> a la
	 * colección. Si la colección ya contiene una propiedad con el mismo nombre
	 * se añadira la propiedad en la colección.
	 * 
	 * @param name
	 *            el nombre de la propiedad
	 * 
	 * @param searchCondition
	 *            la condición de búsqueda
	 * 
	 * @param value
	 *            el valor de la propiedad
	 * 
	 * @return un <code>DmsNumberProperty</code>
	 * 
	 * @throws DmsPropertyException
	 *             si algo va mal en la llamada del método
	 */
	public AGEDmsProperty add(String name, AGEDmsSearchCondition searchCondition, float value) throws Exception {
		return add(name, searchCondition, Float.valueOf(value));
	}

	/**
	 * Añade una propiedad de tipo <code>DmsNumberProperty</code> a la
	 * colección. Si la colección ya contiene una propiedad con el mismo nombre
	 * se añadira la propiedad en la colección.
	 * 
	 * @param name
	 *            el nombre de la propiedad
	 * 
	 * @param searchCondition
	 *            la condición de búsqueda
	 * 
	 * @param value
	 *            el valor de la propiedad
	 * 
	 * @return un <code>DmsNumberProperty</code>
	 * 
	 * @throws DmsPropertyException
	 *             si algo va mal en la llamada del método
	 */
	public AGEDmsProperty add(String name, AGEDmsSearchCondition searchCondition, double value) throws Exception {
		return add(name, searchCondition, Double.valueOf(value));
	}

	/**
	 * Añade una propiedad a la colección usando Java Generics. Si la colección
	 * ya contiene una propiedad con el mismo nombre se añadira la propiedad en
	 * la colección.
	 * 
	 * @param name
	 *            el nombre de la propiedad
	 * 
	 * @param searchCondition
	 *            la condición de búsqueda
	 * 
	 * @param value
	 *            el valor generico de la propiedad
	 * 
	 * @return <code>DmsBooleanProperty</code>,
	 *         <code>DmsBooleanArrayProperty</code>,
	 *         <code>DmsDateProperty</code>, <code>DmsDateArrayProperty</code>
	 *         <code>DmsNumberProperty</code>,
	 *         <code>DmsNumberArrayProperty</code>
	 *         <code>DmsStringProperty</code> o
	 *         <code>DmsStringArrayProperty</code>.
	 * 
	 * @throws DmsPropertyException
	 *             si algo va mal en la llamada del método
	 */
	public <T> AGEDmsProperty add(String name, AGEDmsSearchCondition searchCondition, T value) throws Exception {
		AGEDmsProperty property = createProperty(name, searchCondition, value);
		propertyMap.put(name, property);
		return property;
	}

	/**
	 * Este método estático crea una propiedad (sin añadirla a la colección)
	 * mediante Java Generics.
	 * 
	 * @param name
	 *            el nombre de la propiedad
	 * 
	 * @param searchCondition
	 *            la condición de búsqueda
	 * 
	 * @param value
	 *            el valor generico de la propiedad
	 * 
	 * @return <code>DmsBooleanProperty</code>,
	 *         <code>DmsBooleanArrayProperty</code>,
	 *         <code>DmsDateProperty</code>, <code>DmsDateArrayProperty</code>
	 *         <code>DmsNumberProperty</code>,
	 *         <code>DmsNumberArrayProperty</code>
	 *         <code>DmsStringProperty</code> o
	 *         <code>DmsStringArrayProperty</code>.
	 * 
	 * @throws DmsPropertyException
	 *             si algo va mal en la llamada del método
	 */
	public static <T> AGEDmsProperty createProperty(String name, AGEDmsSearchCondition searchCondition, T value) throws Exception {
		if (name == null)
			throw new Exception("Name is mandatory.");
		if (searchCondition == null)
			throw new Exception("SearchCondition is mandatory.");

		AGEDmsProperty property = null;
		if (value == null)
			property = new AGEDmsStringProperty(name, searchCondition, null);
		else if (value instanceof String)
			property = new AGEDmsStringProperty(name, searchCondition, (String) value);
		else if (value instanceof String[])
			property = new AGEDmsStringArrayProperty(name, searchCondition, (String[]) value);
		else if (value instanceof Boolean)
			property = new AGEDmsBooleanProperty(name, searchCondition, ((Boolean) value).booleanValue());
		else if (value instanceof Boolean[])
			property = new AGEDmsBooleanArrayProperty(name, searchCondition, (Boolean[]) value);
		else if (value instanceof boolean[])
			property = new AGEDmsBooleanArrayProperty(name, searchCondition, ArrayUtils.toObject((boolean[]) value));
		else if (value instanceof Byte)
			property = new AGEDmsNumberProperty(name, searchCondition, (Byte) value);
		else if (value instanceof Byte[])
			property = new AGEDmsNumberArrayProperty(name, searchCondition, (Byte[]) value);
		else if (value instanceof byte[])
			property = new AGEDmsNumberArrayProperty(name, searchCondition, ArrayUtils.toObject((byte[]) value));
		else if (value instanceof Short)
			property = new AGEDmsNumberProperty(name, searchCondition, (Short) value);
		else if (value instanceof Short[])
			property = new AGEDmsNumberArrayProperty(name, searchCondition, (Short[]) value);
		else if (value instanceof short[])
			property = new AGEDmsNumberArrayProperty(name, searchCondition, ArrayUtils.toObject((short[]) value));
		else if (value instanceof Integer)
			property = new AGEDmsNumberProperty(name, searchCondition, (Integer) value);
		else if (value instanceof Integer[])
			property = new AGEDmsNumberArrayProperty(name, searchCondition, (Integer[]) value);
		else if (value instanceof int[])
			property = new AGEDmsNumberArrayProperty(name, searchCondition, ArrayUtils.toObject((int[]) value));
		else if (value instanceof Long)
			property = new AGEDmsNumberProperty(name, searchCondition, (Long) value);
		else if (value instanceof Long[])
			property = new AGEDmsNumberArrayProperty(name, searchCondition, (Long[]) value);
		else if (value instanceof long[])
			property = new AGEDmsNumberArrayProperty(name, searchCondition, ArrayUtils.toObject((long[]) value));
		else if (value instanceof Float)
			property = new AGEDmsNumberProperty(name, searchCondition, (Float) value);
		else if (value instanceof Float[])
			property = new AGEDmsNumberArrayProperty(name, searchCondition, (Float[]) value);
		else if (value instanceof float[])
			property = new AGEDmsNumberArrayProperty(name, searchCondition, ArrayUtils.toObject((float[]) value));
		else if (value instanceof Double)
			property = new AGEDmsNumberProperty(name, searchCondition, (Double) value);
		else if (value instanceof Double[])
			property = new AGEDmsNumberArrayProperty(name, searchCondition, (Double[]) value);
		else if (value instanceof double[])
			property = new AGEDmsNumberArrayProperty(name, searchCondition, ArrayUtils.toObject((double[]) value));
		else if (value instanceof Number)
			property = new AGEDmsNumberProperty(name, searchCondition, (Number) value);
		else if (value instanceof Number[])
			property = new AGEDmsNumberArrayProperty(name, searchCondition, (Number[]) value);
		else if (value instanceof Date)
			property = new AGEDmsDateProperty(name, searchCondition, (Date) value);
		else if (value instanceof Date[])
			property = new AGEDmsDateArrayProperty(name, searchCondition, (Date[]) value);
		else
			throw new Exception("Class " + value.getClass().getCanonicalName() + " not supported.");

		return property;
	}

	/**
	 * Añade una propiedad de tipo <code>DmsProperty</code> a la colección. Si
	 * la colección ya contiene una propiedad con el mismo nombre se reemplazara
	 * la propiedad en la colección.
	 * 
	 * @param property
	 *            la propiedad
	 * 
	 * @throws DmsPropertyException
	 *             si algo va mal en la llamada del método
	 */
	public void add(AGEDmsProperty property) throws Exception {
		if (propertyMap.keySet().contains(property.getName()))
			propertyMap.remove(property.getName());
		this.propertyMap.put(property.getName(), property);
	}

	/**
	 * Devuelve el número de propiedades en la colección
	 * 
	 * @return número de propiedades
	 */
	public int size() {
		return propertyMap.totalSize();
	}

	/**
	 * Devuelve la propiedad de la posición especificada
	 * 
	 * @param index
	 *            posición de la propiedad a devolver
	 * 
	 * @return la propiedad de la posición especificada
	 */
	public AGEDmsProperty get(int index) {
		Object[] properties = propertyMap.values().toArray(new Object[0]);
		return (AGEDmsProperty) properties[index];
	}

}