package eu.europa.ec.leos.opentext.logging;

import java.io.StringWriter;

import javax.xml.XMLConstants;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.cxf.common.util.StringUtils;
//import org.apache.cxf.helpers.XMLUtils;
import org.apache.cxf.io.CachedOutputStream;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A simple logging handler which outputs the bytes of the message to the Logger.
 */
public abstract class AGEAdocAbstractLoggingInterceptor extends AbstractPhaseInterceptor<Message> {

	protected static final Logger logger = LoggerFactory.getLogger(AGEAdocAbstractLoggingInterceptor.class.getName());

	protected int limit = 100 * 1024;
	protected boolean prettyLogging;
	protected String containPattern;
	protected String excludePattern;

	public AGEAdocAbstractLoggingInterceptor(String phase) {
		super(phase);
	}

	public void setLimit(int lim) {
		limit = lim;
	}

	public int getLimit() {
		return limit;
	}

	public void setPrettyLogging(boolean flag) {
		prettyLogging = flag;
	}

	public boolean isPrettyLogging() {
		return prettyLogging;
	}

	public void setContainPattern(String containPattern) {
		this.containPattern = containPattern;
	}

	public String getContainPattern() {
		return containPattern;
	}

	public void setExcludePattern(String excludePattern) {
		this.excludePattern = excludePattern;
	}

	public String getExcludePattern() {
		return excludePattern;
	}

	protected void writePayload(StringBuilder builder, CachedOutputStream cos, String encoding, String contentType) throws Exception {
		StringBuilder internal = new StringBuilder();
		if (isPrettyLogging() && (contentType != null && contentType.indexOf("xml") >= 0) && cos.size() > 0) {
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer serializer = tf.newTransformer();
			serializer.setOutputProperty(OutputKeys.INDENT, "yes");
			serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			StringWriter swriter = new StringWriter();
			// TODO Este try catch se debe eliminar controlando el error que se produce cuando se intenta imprimir la petición y/o respuesta de un mensaje MTOM.
			try {
				serializer.transform(new StreamSource(cos.getInputStream()), new StreamResult(swriter));
			} catch (Exception e) {
				logger.warn("No se ha podido escribir la traza: " + cos.getInputStream());
			}
			String result = swriter.toString();
			if (result.length() < limit || limit == -1) {
				internal.append(swriter.toString());
			} else {
				internal.append(swriter.toString().substring(0, limit));
			}
		} else {
			if (StringUtils.isEmpty(encoding)) {
				cos.writeCacheTo(internal, limit);
			} else {
				cos.writeCacheTo(internal, encoding, limit);
			}
		}
		builder.append(internal);
	}

	protected void log(String message) {
		logger.debug(message);
	}

}
