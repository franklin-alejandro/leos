package eu.europa.ec.leos.opentext;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import eu.europa.ec.leos.opentext.property.AGEDmsFactory;
import eu.europa.ec.leos.opentext.property.AGEDmsProperty;
import eu.europa.ec.leos.opentext.property.AGEDmsPropertySet;
import eu.europa.ec.leos.opentext.property.AGEDmsSearchPropertySet;
import eu.europa.ec.leos.opentext.property.AGEDmsSearchResult;
import eu.europa.ec.leos.opentext.property.artifacts.AGEArtifact;
import eu.europa.ec.leos.opentext.property.artifacts.AGEAttribute;
import eu.europa.ec.leos.opentext.property.artifacts.AGEAttributeDataType;

// Mario: comentado por que peta al meter la librería de documentum. No existe el paquete fs 
// en la dependencia 16.4 de documentum. Esto seguramente lo han actualizado en presidencia.


//import java.security.SecureRandom;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;
//
//import javax.annotation.PostConstruct;
//import javax.xml.ws.handler.Handler;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;

//import com.emc.documentum.fs.datamodel.core.CacheStrategyType;
//import com.emc.documentum.fs.datamodel.core.DataObject;
//import com.emc.documentum.fs.datamodel.core.DataPackage;
//import com.emc.documentum.fs.datamodel.core.ObjectIdentity;
//import com.emc.documentum.fs.datamodel.core.ObjectIdentitySet;
//import com.emc.documentum.fs.datamodel.core.ObjectLocation;
//import com.emc.documentum.fs.datamodel.core.ObjectPath;
//import com.emc.documentum.fs.datamodel.core.OperationOptions;
//import com.emc.documentum.fs.datamodel.core.Qualification;
//import com.emc.documentum.fs.datamodel.core.ReferenceRelationship;
//import com.emc.documentum.fs.datamodel.core.Relationship;
//import com.emc.documentum.fs.datamodel.core.VersionInfo;
//import com.emc.documentum.fs.datamodel.core.VersionStrategy;
//import com.emc.documentum.fs.datamodel.core.content.BinaryContent;
//import com.emc.documentum.fs.datamodel.core.content.ContentTransferMode;
//import com.emc.documentum.fs.datamodel.core.context.RepositoryIdentity;
//import com.emc.documentum.fs.datamodel.core.profiles.CheckinProfile;
//import com.emc.documentum.fs.datamodel.core.profiles.ContentProfile;
//import com.emc.documentum.fs.datamodel.core.profiles.ContentTransferProfile;
//import com.emc.documentum.fs.datamodel.core.profiles.DeleteProfile;
//import com.emc.documentum.fs.datamodel.core.profiles.FormatFilter;
//import com.emc.documentum.fs.datamodel.core.profiles.PropertyFilterMode;
//import com.emc.documentum.fs.datamodel.core.profiles.PropertyProfile;
//import com.emc.documentum.fs.datamodel.core.properties.BooleanArrayProperty;
//import com.emc.documentum.fs.datamodel.core.properties.BooleanProperty;
//import com.emc.documentum.fs.datamodel.core.properties.DateArrayProperty;
//import com.emc.documentum.fs.datamodel.core.properties.DateProperty;
//import com.emc.documentum.fs.datamodel.core.properties.NumberArrayProperty;
//import com.emc.documentum.fs.datamodel.core.properties.NumberProperty;
//import com.emc.documentum.fs.datamodel.core.properties.ObjectIdArrayProperty;
//import com.emc.documentum.fs.datamodel.core.properties.ObjectIdProperty;
//import com.emc.documentum.fs.datamodel.core.properties.Property;
//import com.emc.documentum.fs.datamodel.core.properties.PropertySet;
//import com.emc.documentum.fs.datamodel.core.properties.StringArrayProperty;
//import com.emc.documentum.fs.datamodel.core.properties.StringProperty;
//import com.emc.documentum.fs.datamodel.core.query.Condition;
//import com.emc.documentum.fs.datamodel.core.query.ExpressionSet;
//import com.emc.documentum.fs.datamodel.core.query.ExpressionSetOperator;
//import com.emc.documentum.fs.datamodel.core.query.FullTextExpression;
//import com.emc.documentum.fs.datamodel.core.query.OrderByClause;
//import com.emc.documentum.fs.datamodel.core.query.PassthroughQuery;
//import com.emc.documentum.fs.datamodel.core.query.PropertyExpression;
//import com.emc.documentum.fs.datamodel.core.query.QueryExecution;
//import com.emc.documentum.fs.datamodel.core.query.QueryResult;
//import com.emc.documentum.fs.datamodel.core.query.RepositoryEventSubId;
//import com.emc.documentum.fs.datamodel.core.query.RepositoryScope;
//import com.emc.documentum.fs.datamodel.core.query.SimpleValue;
//import com.emc.documentum.fs.datamodel.core.query.Status;
//import com.emc.documentum.fs.datamodel.core.query.StructuredQuery;
//import com.emc.documentum.fs.datamodel.core.query.ValueList;
//import com.emc.documentum.fs.rt.ServiceException;
//import com.emc.documentum.fs.rt.ServiceInvocationException;
//import com.emc.documentum.fs.rt.context.ContextFactory;
//import com.emc.documentum.fs.rt.context.IServiceContext;
//import com.emc.documentum.fs.rt.context.ServiceFactory;
//import com.emc.documentum.fs.services.core.CacheException;
//import com.emc.documentum.fs.services.core.CoreServiceException;
//import com.emc.documentum.fs.services.core.QueryValidationException;
//import com.emc.documentum.fs.services.core.client.IObjectService;
//import com.emc.documentum.fs.services.core.client.IQueryService;
//import com.emc.documentum.fs.services.core.client.IVersionControlService;
//import com.emc.documentum.fs.services.search.client.ISearchService;

//import eu.europa.ec.leos.opentext.property.AGEDmsProperty;
//import eu.europa.ec.leos.opentext.property.AGEDmsArrayProperty;
//import eu.europa.ec.leos.opentext.property.AGEDmsBooleanArrayProperty;
//import eu.europa.ec.leos.opentext.property.AGEDmsBooleanProperty;
//import eu.europa.ec.leos.opentext.property.AGEDmsDateProperty;
//import eu.europa.ec.leos.opentext.property.AGEDmsDateArrayProperty;
//import eu.europa.ec.leos.opentext.property.AGEDmsNumberProperty;
//import eu.europa.ec.leos.opentext.property.AGEDmsNumberArrayProperty;
//import eu.europa.ec.leos.opentext.property.AGEDmsPropertySet;
//import eu.europa.ec.leos.opentext.property.AGEDmsSearchCondition;
//import eu.europa.ec.leos.opentext.property.AGEDmsStringProperty;
//import eu.europa.ec.leos.opentext.property.AGEDmsStringArrayProperty;
//import eu.europa.ec.leos.opentext.property.AGEDmsSearchResult;
//import eu.europa.ec.leos.opentext.property.AGEDmsSearchPropertySet;
//import eu.europa.ec.leos.opentext.property.artifacts.AGEAttribute;
//import eu.europa.ec.leos.opentext.property.artifacts.AGEAttributeDataType;
//import eu.europa.ec.leos.opentext.property.artifacts.AGEArtifact;
//import eu.europa.ec.leos.opentext.property.AGEDmsFactory;

@Component
public class AGEDfsDmsFactory implements AGEDmsFactory {

	@Override
	public String createCabinet(String idApplication) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsCabinet(String idApplication) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getPathId(String idApplication, String path) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCabinetFromFolder(String idFolder) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCabinetFromDocument(String idDocument) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createFolder(String idApplication, String type, String name) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createFolder(String idApplication, String type, String name, String idParentFolder) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsFolder(String idApplication, String idFolder) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void lockFolder(String idApplication, String idFolder) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unlockFolder(String idApplication, String idFolder) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isLockedFolder(String idApplication, String idFolder) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public AGEDmsPropertySet getFolderProperties(String idApplication, String idFolder, AGEDmsPropertySet properties)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AGEDmsPropertySet getFolderProperties(String idApplication, String idFolder) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setFolderProperties(String idApplication, String idFolder, AGEDmsPropertySet properties)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public AGEDmsProperty getFolderProperty(String idApplication, String idFolder, AGEDmsProperty property)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setFolderProperty(String idApplication, String idFolder, AGEDmsProperty property) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getFolderType(String idApplication, String idFolder) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteFolder(String idApplication, String idFolder) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void renameFolder(String idApplication, String idFolder, String name) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String createDocument(String idApplication, String type, String name, String idFolder, String fileStore)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createDocument(String idApplication, String type, String name, String idFolder, String extension,
			byte[] content, String fileStore) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDocumentContent(String idApplication, String idDocument, String extension, byte[] content)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getDocumentParentId(String idApplication, String idDocument) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDocumentVersionId(String idApplication, String idDocument, String version) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDocumentType(String idApplication, String idDocument) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] getDocumentContent(String idApplication, String idDocument) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDocumentContentFormat(String idApplication, String idDocument) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] getDocumentContentByVersion(String idApplication, String idDocument, String version)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDocumentContentFormatByVersion(String idApplication, String idDocument, String version)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void checkoutDocument(String idDocument) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String altCheckinDocument(String idDocument, String nextVersionLabel, Boolean isMajor,
			Map<String, Object> updatedProperties, String extension, byte[] content, String comment) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String checkinDocument(String idDocument, String extension, byte[] content) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void cancelCheckoutDocument(String idDocument) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<String> getDocumentVersions(String idApplication, String idDocument) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsDocument(String idApplication, String idDocument) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public AGEDmsPropertySet getDocumentProperties(String idApplication, String idDocument,
			AGEDmsPropertySet properties) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AGEDmsPropertySet getDocumentProperties(String idApplication, String idDocument) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDocumentProperties(String idApplication, String idDocument, AGEDmsPropertySet properties)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public AGEDmsProperty getDocumentProperty(String idApplication, String idDocument, AGEDmsProperty property)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDocumentProperty(String idApplication, String idDocument, AGEDmsProperty property) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getDocumentFileStore(String idApplication, String idDocument) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDocumentFileStore(String idApplication, String idDocument, String fileStore) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteDocument(String idApplication, String idDocument) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void renameDocument(String idApplication, String idDocument, String name) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void moveDocument(String idApplication, String idDocument, String idFolderTo) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<String> getDocumentsInFolder(String idApplication, String idFolder) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> searchDocuments(String idApplication, String type, AGEDmsSearchPropertySet searchProperties,
			AGEDmsProperty returnProperty) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> searchDocuments(String idApplication, String type, AGEDmsSearchPropertySet searchProperties,
			AGEDmsProperty returnProperty, int maxSearchResults) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AGEDmsSearchResult searchDocumentsPaged(String idApplication, String type,
			AGEDmsSearchPropertySet searchProperties, List<AGEDmsProperty> returnProperties,
			AGEDmsProperty orderProperty, Boolean isAscending, int startIndex, int pageSize) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AGEDmsSearchResult searchDocumentsPaged(String idApplication, String type,
			AGEDmsSearchPropertySet searchProperties, AGEDmsProperty returnProperty, AGEDmsProperty orderProperty,
			Boolean isAscending, int startIndex, int pageSize) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AGEDmsSearchResult searchDocumentsFullText(String idApplication, String type,
			AGEDmsSearchPropertySet searchProperties, String searchText, List<AGEDmsProperty> returnProperties,
			AGEDmsProperty orderProperty, Boolean isAscending, int maxResults) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AGEDmsSearchResult searchDocumentsFullText(String idApplication, String type,
			AGEDmsSearchPropertySet searchProperties, String searchText, AGEDmsProperty returnProperty,
			AGEDmsProperty orderProperty, Boolean isAscending, int maxResults) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> searchFolders(String idApplication, String type, AGEDmsSearchPropertySet searchProperties,
			AGEDmsProperty returnProperty) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> searchFolders(String idApplication, String type, AGEDmsSearchPropertySet searchProperties,
			AGEDmsProperty returnProperty, int maxSearchResults) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AGEDmsSearchResult searchFoldersPaged(String idApplication, String type,
			AGEDmsSearchPropertySet searchProperties, List<AGEDmsProperty> returnProperties,
			AGEDmsProperty orderProperty, Boolean isAscending, int startIndex, int pageSize) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AGEDmsSearchResult searchFoldersPaged(String idApplication, String type,
			AGEDmsSearchPropertySet searchProperties, AGEDmsProperty returnProperty, AGEDmsProperty orderProperty,
			Boolean isAscending, int startIndex, int pageSize) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AGEAttribute> getAttributes(String artifactTypeName) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AGEAttributeDataType getAttributeDataType(String artifactTypeName, String attributeName) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getAttributeLength(String artifactTypeName, String attributeName) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAttributeRepeating(String artifactTypeName, String attributeName) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<String> getAttributeDefaultValues(String artifactTypeName, String attributeName) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void reloadAttributes() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<AGEDmsPropertySet> executeQuery(String query) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deployArtifacts(List<AGEArtifact> artifacts) throws Exception {
		// TODO Auto-generated method stub
		
	}
//
//	private static final Logger logger = LoggerFactory.getLogger(AGEDfsDmsFactory.class);
//
//	// DFS Date Format
//	private static final String DATE_FORMAT_DFS = "dd/MM/yyyy HH:mm:ss";
//
//	// ISO-8601 Date Format
//	private static final String DATE_FORMAT_ISO = "yyyy-MM-dd'T'HH:mm:ss";
//
//	private static final String FIELD_OBJECT_NAME = "object_name";
//	private static final String FIELD_LOCK = "locked";
//	private static final String FIELD_CONTENT_TYPE = "a_content_type";
//	private static final String FIELD_STORAGE_TYPE = "a_storage_type";
//	private static final String FIELD_OBJECT_TYPE = "r_object_type";
//	private static final String FIELD_FOLDER_ID = "i_folder_id";
//
//	private IObjectService objectService;
//	private IQueryService queryService;
//	private ISearchService searchService;
//	private IVersionControlService versionControlService;
//
//	@Value("${leos.opentext.repository.url}")
//	private String host;
//
//	@Value("${leos.cmis.repository.id}")
//	private String repositoryName;
//
//	@Value("${leos.cmis.repository.username:}")
//	private String userName;
//
//	@Value("${leos.cmis.repository.password:}")
//	private String password;
//
//	@Value("${leos.opentext.repository.maxSearchResultsConf}")
//	private int maxSearchResultsConf;
//
//	@Autowired
//	private AGEDfsContentTypeMapper dfsContentTypeMapper;
//
//	/*
//	@Autowired
//	private ContextFactory contextFactory;
//	
//	@Autowired
//	private ServiceFactory serviceFactory;
//	*/
//	
//	// This hashmap contains the artifact types and its attribute types
//	private HashMap<String, HashMap> artifactTypeMap = new HashMap<>();
//
//	private SimpleDateFormat dfsDateFormat = new SimpleDateFormat(DATE_FORMAT_DFS);
//	private SimpleDateFormat isoDateFormat = new SimpleDateFormat(DATE_FORMAT_ISO);
//
//	@PostConstruct
//	private void initContext() throws Exception {
//		logger.info("Initializing DfsDmsFactory...");
//		try {
//			logger.debug("Creating secure random generator.");
//			SecureRandom secureRandom = new SecureRandom();
//			int seedByteCount = 20;
//			byte[] seed = secureRandom.generateSeed(seedByteCount);
//			secureRandom.setSeed(seed);
//
//			logger.info("Creating DFS ContextFactory.");
//			ContextFactory contextFactory = ContextFactory.getInstance();
//
//			logger.info("Creating DFS ServiceContext.");
//			IServiceContext serviceContext = contextFactory.newContext();
//
//			logger.info("Setting credentials, repositoryName [" + repositoryName + "], username [" + userName + "].");
//			RepositoryIdentity repoId = new RepositoryIdentity();
//			repoId.setRepositoryName(repositoryName);
//			repoId.setUserName(userName);
//			repoId.setPassword(password);
//			serviceContext.addIdentity(repoId);
//
//			logger.info("Creating DFS ServiceFactory.");
//			ServiceFactory serviceFactory = ServiceFactory.getInstance();
//
//			/*
//			 * Create our own SOAPHandlers in order to tweak Documentum DFS with Apache CXF and pass them on to the 
//			 * ObjectService, QueryService, SearchService and VersionControlService. See each handler for more information.
//			 */
//			List<Handler> handlerList = new ArrayList<>();
//			handlerList.add(new AGEDfsSoapHeaderHandler(repositoryName, userName, password, secureRandom));
//			handlerList.add(new AGEDfsSoapFaultHandler());
//
//			logger.info("Using host [" + host + "].");
//
//			logger.info("Creating DFS ObjectService.");
//			objectService = serviceFactory.getRemoteService(IObjectService.class, serviceContext, "core", host, handlerList);
//
//			logger.info("Creating DFS QueryService.");
//			queryService = serviceFactory.getRemoteService(IQueryService.class, serviceContext, "core", host, handlerList);
//
//			logger.info("Creating DFS SearchService.");
//			searchService = serviceFactory.getRemoteService(ISearchService.class, serviceContext, "search", host, handlerList);
//
//			logger.info("Creating DFS VersionControlService.");
//			versionControlService = serviceFactory.getRemoteService(IVersionControlService.class, serviceContext, "core", host, handlerList);
//
//		} catch (ServiceInvocationException e) {
//			throw new Exception("Documentum DFS Exception: " + e);
//		}
//	}
//
//	private IObjectService getObjectService() {
//		return objectService;
//	}
//
//	private IQueryService getQueryService() {
//		return queryService;
//	}
//
//	private ISearchService getSearchService() {
//		return searchService;
//	}
//
//	private IVersionControlService getVersionControlService() {
//		return versionControlService;
//	}
//
//	/**
//	 * Este método devuelve la instancia <code>DfsContentTypeMapper</code>. El <code>DfsContentTypeMapper</code> tiene la responsabilidad de mantener el mapeo de tipos de contenido de Documentum
//	 * contra extensiones de documentos y vice versa.
//	 * 
//	 * @return DfsContentTypeMapper
//	 * 
//	 * @throws Exception
//	 *             si algo va mal en la llamada del método
//	 */
//	public AGEDfsContentTypeMapper getContentTypeMapper() throws Exception {
//		return dfsContentTypeMapper;
//	}
//
//	public List<DataObject> executeDQL(String queryString) throws Exception {
//		try {
//			PassthroughQuery query = new PassthroughQuery();
//			query.setQueryString(queryString);
//			query.addRepository(repositoryName);
//			QueryExecution queryEx = new QueryExecution(0, 50000, 50000);
//			queryEx.setCacheStrategyType(CacheStrategyType.DEFAULT_CACHE_STRATEGY);
//			QueryResult queryResult = getQueryService().execute(query, queryEx, null);
//			DataPackage dataPackage = queryResult.getDataPackage();
//			return dataPackage.getDataObjects();
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" + "Error executeDQL: " + e.getMessage(), e);
//		}
//	}
//
//	public String createCabinet(String idApplication) throws Exception {
//		ObjectIdentity objectIdentity = new ObjectIdentity(repositoryName);
//		DataObject dataObject = new DataObject(objectIdentity, "dm_cabinet");
//		PropertySet folderDataObjProperties = new PropertySet();
//		folderDataObjProperties.set(FIELD_OBJECT_NAME, idApplication);
//		dataObject.setProperties(folderDataObjProperties);
//
//		DataPackage dataPackage = new DataPackage();
//		dataPackage.addDataObject(dataObject);
//		try {
//			DataPackage ret = getObjectService().create(dataPackage, null);
//			return ret.getDataObjects().get(0).getIdentity().getValueAsString();
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" + "Error createCabinet: " + e.getMessage(), e);
//		}
//	}
//
//	public boolean existsCabinet(String idApplication) throws Exception {
//		ObjectIdentity objectIdentity = getApplicationCabinetIdentity(idApplication);
//		ObjectIdentitySet objectIdSet = new ObjectIdentitySet(objectIdentity);
//		try {
//			DataPackage dataPackage = getObjectService().get(objectIdSet, null);
//			return (dataPackage.getDataObjects().get(0) != null);
//		} catch (Exception e) {
//			return false;
//		}
//	}
//
//	public String getPathId(String idApplication, String path) throws Exception {
//		ObjectIdentity objectIdentity = getObjectIdentityByPath(idApplication, path);
//		ObjectIdentitySet objectIdSet = new ObjectIdentitySet(objectIdentity);
//		try {
//			DataPackage dataPackage = getObjectService().get(objectIdSet, null);
//			return dataPackage.getDataObjects().get(0).getIdentity().getValueAsString();
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" + "Error getPath: " + e.getMessage(), e);
//		}
//	}
//
//	public String getCabinetFromFolder(String idFolder) throws Exception {
//		try {
//			PassthroughQuery query = new PassthroughQuery();
//			query.setQueryString("select object_name from dm_folder where r_object_id = (select i_cabinet_id from dm_folder where r_object_id = '" + idFolder + "')");
//			query.addRepository(repositoryName);
//			QueryExecution queryEx = new QueryExecution();
//			queryEx.setCacheStrategyType(CacheStrategyType.DEFAULT_CACHE_STRATEGY);
//			QueryResult queryResult = getQueryService().execute(query, queryEx, null);
//			DataPackage dataPackage = queryResult.getDataPackage();
//			List<DataObject> dataObjects = dataPackage.getDataObjects();
//			Iterator iterator = dataObjects.iterator();
//			if (iterator.hasNext()) {
//				DataObject dataObject = (DataObject) iterator.next();
//				return dataObject.getProperties().get(FIELD_OBJECT_NAME).getValueAsString();
//			} else {
//				throw new Exception("Cabinet not found for folder [" + idFolder + "].");
//			}
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" + "Error getCabinetFromFolder: " + e.getMessage(), e);
//		}
//	}
//
//	public String getCabinetFromDocument(String idDocument) throws Exception {
//
//		try {
//			PassthroughQuery query = new PassthroughQuery();
//			query.setQueryString("select object_name from dm_folder where r_object_id = (select i_cabinet_id from dm_document where r_object_id = '" + idDocument + "')");
//			query.addRepository(repositoryName);
//			QueryExecution queryEx = new QueryExecution();
//			queryEx.setCacheStrategyType(CacheStrategyType.DEFAULT_CACHE_STRATEGY);
//			QueryResult queryResult = getQueryService().execute(query, queryEx, null);
//			DataPackage dataPackage = queryResult.getDataPackage();
//			List<DataObject> dataObjects = dataPackage.getDataObjects();
//			Iterator iterator = dataObjects.iterator();
//			if (iterator.hasNext()) {
//				DataObject dataObject = (DataObject) iterator.next();
//				return dataObject.getProperties().get(FIELD_OBJECT_NAME).getValueAsString();
//			} else {
//				throw new Exception("Documentum DFS Exception-" + "Cabinet not found for document [" + idDocument + "].");
//			}
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" + "Error getCabinetFromDocument: " + e.getMessage(), e);
//		}
//	}
//
//	public String createFolder(String idApplication, String type, String name) throws Exception {
//		return createFolder(idApplication, type, name, null);
//	}
//
//	public String createFolder(String idApplication, String type, String name, String idParentFolder) throws Exception {
//
//		ObjectIdentity objectIdentity = new ObjectIdentity(repositoryName);
//		DataObject dataObject = new DataObject(objectIdentity, type);
//		PropertySet folderDataObjProperties = new PropertySet();
//		folderDataObjProperties.set(FIELD_OBJECT_NAME, name);
//		dataObject.setProperties(folderDataObjProperties);
//
//		// If idParentFolder is null we use the Cabinet as parent
//		ObjectIdentity parentFolderIdentity;
//		if (idParentFolder != null && !idParentFolder.isEmpty()) {
//			parentFolderIdentity = getFolderIdentity(idParentFolder);
//		} else {
//			parentFolderIdentity = getApplicationCabinetIdentity(idApplication);
//		}
//		ReferenceRelationship parentFolderRelationship = new ReferenceRelationship();
//		parentFolderRelationship.setName(Relationship.RELATIONSHIP_FOLDER);
//		parentFolderRelationship.setTarget(parentFolderIdentity);
//		parentFolderRelationship.setTargetRole(Relationship.ROLE_PARENT);
//		dataObject.getRelationships().add(parentFolderRelationship);
//
//		DataPackage dataPackage = new DataPackage();
//		dataPackage.addDataObject(dataObject);
//		try {
//			DataPackage ret = getObjectService().create(dataPackage, null);
//			return ret.getDataObjects().get(0).getIdentity().getValueAsString();
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" + "Error createFolder: " + e.getMessage(), e);
//		}
//	}
//
//	public boolean existsFolder(String idApplication, String idFolder) throws Exception {
//		ObjectIdentity objectIdentity = getFolderIdentity(idFolder);
//		ObjectIdentitySet objectIdSet = new ObjectIdentitySet(objectIdentity);
//		try {
//			DataPackage dataPackage = getObjectService().get(objectIdSet, null);
//			return (dataPackage.getDataObjects().get(0) != null);
//		} catch (Exception e) {
//			return false;
//		}
//	}
//
//	public void lockFolder(String idApplication, String idFolder) throws Exception {
//		synchronized (this) {
//			if (isLockedFolder(idApplication, idFolder))
//				throw new Exception("Documentum DFS Exception-" + "Folder allready locked");
//			setFolderProperty(idApplication, idFolder, new AGEDmsBooleanProperty(FIELD_LOCK, true));
//		}
//	}
//
//	public void unlockFolder(String idApplication, String idFolder) throws Exception {
//		synchronized (this) {
//			setFolderProperty(idApplication, idFolder, new AGEDmsBooleanProperty(FIELD_LOCK, false));
//		}
//	}
//
//	public boolean isLockedFolder(String idApplication, String idFolder) throws Exception {
//		AGEDmsBooleanProperty lockProperty = (AGEDmsBooleanProperty) getFolderProperty(idApplication, idFolder, new AGEDmsBooleanProperty(FIELD_LOCK));
//		return lockProperty.getValue();
//	}
//
//	public String getFolderType(String idApplication, String idFolder) throws Exception {
//		AGEDmsStringProperty prop = (AGEDmsStringProperty) getFolderProperty(idApplication, idFolder, new AGEDmsStringProperty(FIELD_OBJECT_TYPE));
//		return prop.getValue();
//	}
//
//	public void deleteFolder(String idApplication, String idFolder) throws Exception {
//
//		ObjectIdentity objectIdentity = getFolderIdentity(idFolder);
//		ObjectIdentitySet objectIdSet = new ObjectIdentitySet(objectIdentity);
//
//		DeleteProfile deleteProfile = new DeleteProfile();
//		deleteProfile.setDeepDeleteFolders(true);
//		deleteProfile.setDeepDeleteChildrenInFolders(true);
//		OperationOptions operationOptions = new OperationOptions();
//		operationOptions.setDeleteProfile(deleteProfile);
//
//		try {
//			getObjectService().delete(objectIdSet, operationOptions);
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error deleteFolder: " + e.getMessage(), e);
//		}
//	}
//
//	public void renameFolder(String idApplication, String idFolder, String name) throws Exception {
//		AGEDmsStringProperty property = new AGEDmsStringProperty(FIELD_OBJECT_NAME, name);
//		setFolderProperty(idApplication, idFolder, property);
//	}
//
//	public AGEDmsPropertySet getFolderProperties(String idApplication, String idFolder) throws Exception {
//		return getFolderProperties(idApplication, idFolder, null);
//	}
//
//	public AGEDmsPropertySet getFolderProperties(String idApplication, String idFolder, AGEDmsPropertySet properties) throws Exception {
//
//		ObjectIdentity objectIdentity = getFolderIdentity(idFolder);
//		ObjectIdentitySet objectIdSet = new ObjectIdentitySet(objectIdentity);
//
//		// Consulta unicamente las propiedades demandadas si se pasa un
//		// AGEDmsPropertySet
//		PropertyProfile propertyProfile = new PropertyProfile();
//		if (properties != null) {
//			propertyProfile.setFilterMode(PropertyFilterMode.SPECIFIED_BY_INCLUDE);
//			ArrayList<String> includeProperties = new ArrayList<>();
//			for (int i = 0; i < properties.size(); i++) {
//				AGEDmsProperty property = properties.get(i);
//				includeProperties.add(property.getName());
//			}
//			propertyProfile.setIncludeProperties(includeProperties);
//		}
//
//		OperationOptions operationOptions = new OperationOptions();
//		operationOptions.setPropertyProfile(propertyProfile);
//
//		try {
//			DataPackage dataPackage = getObjectService().get(objectIdSet, operationOptions);
//			PropertySet dfsProperties = dataPackage.getDataObjects().get(0).getProperties();
//			return mapDocumentumPropertiesToADocProperties(dfsProperties);
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" + "Error getFolderProperties: " + e.getMessage(), e);
//		}
//	}
//
//	public void setFolderProperties(String idApplication, String idFolder, AGEDmsPropertySet properties) throws Exception {
//
//		ObjectIdentity objectIdentity = getFolderIdentity(idFolder);
//		DataObject dataObject = new DataObject(objectIdentity);
//
//		PropertyProfile propertyProfile = new PropertyProfile();
//		propertyProfile.setFilterMode(PropertyFilterMode.ALL);
//		OperationOptions operationOptions = new OperationOptions();
//		operationOptions.setProfile(propertyProfile);
//
//		String objectName = getFolderType(idApplication, idFolder);
//		PropertySet dfsProperties = mapAdocPropertiesToDocumentumProperties(properties, objectName);
//		dataObject.setProperties(dfsProperties);
//
//		try {
//			getObjectService().update(new DataPackage(dataObject), operationOptions);
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error setFolderProperties: " + e.getMessage(), e);
//		}
//	}
//
//	public AGEDmsProperty getFolderProperty(String idApplication, String idFolder, AGEDmsProperty property) throws Exception {
//
//		ObjectIdentity objectIdentity = getFolderIdentity(idFolder);
//		ObjectIdentitySet objectIdSet = new ObjectIdentitySet(objectIdentity);
//
//		// Consulta unicamente las propiedades demandadas
//		PropertyProfile propertyProfile = new PropertyProfile();
//		propertyProfile.setFilterMode(PropertyFilterMode.SPECIFIED_BY_INCLUDE);
//		ArrayList<String> includeProperties = new ArrayList<>();
//		includeProperties.add(property.getName());
//		propertyProfile.setIncludeProperties(includeProperties);
//
//		OperationOptions operationOptions = new OperationOptions();
//		operationOptions.setPropertyProfile(propertyProfile);
//
//		try {
//			DataPackage dataPackage = getObjectService().get(objectIdSet, operationOptions);
//			PropertySet dfsProperties = dataPackage.getDataObjects().get(0).getProperties();
//			if (dfsProperties.iterator().hasNext()) {
//				Property dfsProperty = dfsProperties.iterator().next();
//				return mapDocumentumPropertyToADocProperty(dfsProperty);
//			} else {
//				throw new Exception("Property with name " + property.getName() + " not found.");
//			}
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error getFolderProperty: " + e.getMessage(), e);
//		}
//	}
//
//	public void setFolderProperty(String idApplication, String idFolder, AGEDmsProperty property) throws Exception {
//
//		ObjectIdentity objectIdentity = getFolderIdentity(idFolder);
//		DataObject dataObject = new DataObject(objectIdentity);
//
//		PropertyProfile propertyProfile = new PropertyProfile();
//		propertyProfile.setFilterMode(PropertyFilterMode.ALL);
//		OperationOptions operationOptions = new OperationOptions();
//		operationOptions.setProfile(propertyProfile);
//
//		PropertySet dfsProperties = new PropertySet();
//		String objectName = getFolderType(idApplication, idFolder);
//		Property dfsProperty = mapAdocPropertyToDocumentumProperty(property, objectName);
//		dfsProperties.set(dfsProperty);
//		dataObject.setProperties(dfsProperties);
//
//		try {
//			getObjectService().update(new DataPackage(dataObject), operationOptions);
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error setFolderProperty: " + e.getMessage(), e);
//		}
//	}
//
//	public String createDocument(String idApplication, String type, String name, String idFolder, String fileStore) throws Exception {
//		return createDocument(idApplication, type, name, idFolder, null, null, fileStore);
//	}
//
//	public String createDocument(String idApplication, String type, String name, String idFolder, String extension, byte[] content, String fileStore) throws Exception {
//
//		if (idFolder == null)
//			throw new Exception("Documentum DFS Exception-" + "The parameter idFolder is mandatory.");
//
//		// Set Identity
//		ObjectIdentity objectIdentity = new ObjectIdentity(repositoryName);
//		DataObject dataObject = new DataObject(objectIdentity, type);
//		PropertySet documentDataObjProperties = new PropertySet();
//		documentDataObjProperties.set(FIELD_OBJECT_NAME, name);
//		documentDataObjProperties.set(FIELD_STORAGE_TYPE, fileStore == null ? "" : fileStore);
//		String documentumContentType = null;
//		if (extension == null)
//			documentumContentType = "unknown";
//		else {
//			// Lookup the Documentum content type for the extension, if we
//			// cannot find a mapping
//			// we suppose the extension is the Documentum content type.
//			documentumContentType = dfsContentTypeMapper.getDocumentumContentTypeByDosExtension(extension);
//			if (documentumContentType == null || documentumContentType.isEmpty())
//				documentumContentType = extension;
//		}
//		documentDataObjProperties.set(FIELD_CONTENT_TYPE, documentumContentType);
//		dataObject.setProperties(documentDataObjProperties);
//
//		// Set Relationship
//		ObjectIdentity parentFolderIdentity = getFolderIdentity(idFolder);
//		ReferenceRelationship parentFolderRelationship = new ReferenceRelationship();
//		parentFolderRelationship.setName(Relationship.RELATIONSHIP_FOLDER);
//		parentFolderRelationship.setTarget(parentFolderIdentity);
//		parentFolderRelationship.setTargetRole(Relationship.ROLE_PARENT);
//		dataObject.getRelationships().add(parentFolderRelationship);
//
//		// Set TransferProfile
//		ContentTransferProfile transferProfile = new ContentTransferProfile();
//		transferProfile.setTransferMode(ContentTransferMode.MTOM);
//		OperationOptions operationOptions = new OperationOptions();
//		operationOptions.setContentTransferProfile(transferProfile);
//
//		// Set Content
//		if (content != null)
//			dataObject.getContents().add(new BinaryContent(content, documentumContentType));
//
//		DataPackage dataPackage = new DataPackage();
//		dataPackage.addDataObject(dataObject);
//
//		try {
//			DataPackage ret = getObjectService().create(dataPackage, operationOptions);
//			return ret.getDataObjects().get(0).getIdentity().getValueAsString();
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" + "Error createDocument: " + e.getMessage(), e);
//		}
//	}
//
//	public void setDocumentContent(String idApplication, String idDocument, String extension, byte[] content) throws Exception {
//
//		ObjectIdentity objectIdentity = getDocumentIdentity(idDocument);
//		DataObject dataObject = new DataObject(objectIdentity);
//
//		PropertySet documentDataObjProperties = new PropertySet();
//
//		String documentumContentType = null;
//		if (extension == null)
//			documentumContentType = "unknown";
//		else {
//			// Lookup the Documentum content type for the extension, if we
//			// cannot find a mapping
//			// we suppose the extension is the Documentum content type.
//			documentumContentType = dfsContentTypeMapper.getDocumentumContentTypeByDosExtension(extension);
//			if (documentumContentType == null || documentumContentType.isEmpty())
//				documentumContentType = extension;
//		}
//		documentDataObjProperties.set(FIELD_CONTENT_TYPE, documentumContentType);
//		dataObject.setProperties(documentDataObjProperties);
//
//		// Set TransferProfile
//		ContentTransferProfile transferProfile = new ContentTransferProfile();
//		transferProfile.setTransferMode(ContentTransferMode.MTOM);
//		OperationOptions operationOptions = new OperationOptions();
//		operationOptions.setContentTransferProfile(transferProfile);
//
//		// Set Content
//		dataObject.getContents().add(new BinaryContent(content, documentumContentType));
//
//		DataPackage dataPackage = new DataPackage();
//		dataPackage.addDataObject(dataObject);
//
//		try {
//			getObjectService().update(dataPackage, operationOptions);
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error setDocumentContent: " + e.getMessage(), e);
//		}
//	}
//
//	public String getDocumentParentId(String idApplication, String idDocument) throws Exception {
//		AGEDmsStringArrayProperty prop = (AGEDmsStringArrayProperty) getDocumentProperty(idApplication, idDocument, new AGEDmsStringArrayProperty(FIELD_FOLDER_ID));
//		return prop.getValues()[0];
//	}
//
//	public String getDocumentType(String idApplication, String idDocument) throws Exception {
//		AGEDmsStringProperty prop = (AGEDmsStringProperty) getDocumentProperty(idApplication, idDocument, new AGEDmsStringProperty(FIELD_OBJECT_TYPE));
//		return prop.getValue();
//	}
//
//	public String getDocumentFileStore(String idApplication, String idDocument) throws Exception {
//		AGEDmsStringProperty prop = (AGEDmsStringProperty) getDocumentProperty(idApplication, idDocument, new AGEDmsStringProperty(FIELD_STORAGE_TYPE));
//		return prop.getValue();
//	}
//
//	public void setDocumentFileStore(String idApplication, String idDocument, String fileStore) throws Exception {
//		AGEDmsStringProperty property = new AGEDmsStringProperty(FIELD_STORAGE_TYPE, fileStore);
//		setDocumentProperty(idApplication, idDocument, property);
//	}
//
//	public byte[] getDocumentContent(String idApplication, String idDocument) throws Exception {
//
//		ObjectIdentity objectIdentity = getDocumentIdentity(idDocument);
//		ObjectIdentitySet objectIdSet = new ObjectIdentitySet(objectIdentity);
//
//		ContentProfile contentProfile = new ContentProfile();
//		contentProfile.setFormatFilter(FormatFilter.ANY);
//		OperationOptions operationOptions = new OperationOptions();
//		operationOptions.setContentProfile(contentProfile);
//		operationOptions.setProfile(contentProfile);
//
//		try {
//			DataPackage dataPackage = getObjectService().get(objectIdSet, operationOptions);
//			if (dataPackage.getDataObjects().get(0).getContents().isEmpty())
//				return (byte[]) null;
//			else {
//				return dataPackage.getDataObjects().get(0).getContents().get(0).getAsByteArray();
//			}
//		} catch (Exception e) {
//			throw new Exception("Error getDocument: " + e.getMessage(), e);
//		}
//	}
//
//	public byte[] getDocumentContentByVersion(String idApplication, String idDocument, String version) throws Exception {
//
//		try {
//			String idDocumentVersion = getDocumentVersionId(idApplication, idDocument, version);
//			return getDocumentContent(idApplication, idDocumentVersion);
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error getDocumentContentByVersion: " + e.getMessage(), e);
//		}
//	}
//
//	public String getDocumentContentFormat(String idApplication, String idDocument) throws Exception {
//
//		ObjectIdentity objectIdentity = getDocumentIdentity(idDocument);
//		ObjectIdentitySet objectIdSet = new ObjectIdentitySet(objectIdentity);
//
//		ContentProfile contentProfile = new ContentProfile();
//		contentProfile.setFormatFilter(FormatFilter.ANY);
//		OperationOptions operationOptions = new OperationOptions();
//		operationOptions.setContentProfile(contentProfile);
//		operationOptions.setProfile(contentProfile);
//
//		try {
//			DataPackage dataPackage = getObjectService().get(objectIdSet, operationOptions);
//			if (dataPackage.getDataObjects().get(0).getContents().isEmpty()) {
//				return null;
//			} else {
//				String documentumContentType = dataPackage.getDataObjects().get(0).getContents().get(0).getFormat();
//				String dosExtension = dfsContentTypeMapper.getDosExtensionByDocumentumContentType(documentumContentType);
//				if (dosExtension == null || dosExtension.isEmpty())
//					throw new Exception("Documentum DFS Exception-" +"Documentum content format [" + documentumContentType + "] not found");
//				return dosExtension;
//			}
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error getDocumentContentFormat: " + e.getMessage(), e);
//		}
//	}
//
//	public String getDocumentContentFormatByVersion(String idApplication, String idDocument, String version) throws Exception {
//
//		try {
//			String idDocumentVersion = getDocumentVersionId(idApplication, idDocument, version);
//			return getDocumentContentFormat(idApplication, idDocumentVersion);
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error getDocumentContentFormatByVersion: " + e.getMessage(), e);
//		}
//	}
//
//	public void checkoutDocument(String idDocument) throws Exception {
//
//		ObjectIdentity objectIdentity = getDocumentIdentity(idDocument);
//		ObjectIdentitySet objectIdSet = new ObjectIdentitySet(objectIdentity);
//
//		try {
//			getVersionControlService().checkout(objectIdSet, null);
//		} catch (Exception e) {
//			throw new Exception("Error checkoutDocument: " + e.getMessage(), e);
//		}
//	}
//
//	public String checkinDocument(String idDocument, String extension, byte[] content) throws Exception {
//
//		ObjectIdentity objectIdentity = getDocumentIdentity(idDocument);
//		DataObject dataObject = new DataObject(objectIdentity);
//
//		PropertySet documentDataObjProperties = new PropertySet();
//
//		String documentumContentType = null;
//		if (extension == null)
//			documentumContentType = "unknown";
//		else {
//			// Lookup the Documentum content type for the extension, if we
//			// cannot find a mapping
//			// we suppose the extension is the Documentum content type.
//			documentumContentType = dfsContentTypeMapper.getDocumentumContentTypeByDosExtension(extension);
//			if (documentumContentType == null || documentumContentType.isEmpty())
//				documentumContentType = extension;
//		}
//		documentDataObjProperties.set(FIELD_CONTENT_TYPE, documentumContentType);
//		dataObject.setProperties(documentDataObjProperties);
//
//		// Set TransferProfile
//		ContentTransferProfile transferProfile = new ContentTransferProfile();
//		transferProfile.setTransferMode(ContentTransferMode.MTOM);
//		OperationOptions operationOptions = new OperationOptions();
//		operationOptions.setContentTransferProfile(transferProfile);
//
//		// Set Content
//		dataObject.getContents().add(new BinaryContent(content, documentumContentType));
//
//		DataPackage dataPackage = new DataPackage();
//		dataPackage.addDataObject(dataObject);
//
//		boolean retainLock = false;
//		List<String> labels = new ArrayList<>();
//
//		try {
//			DataPackage ret = getVersionControlService().checkin(dataPackage, VersionStrategy.NEXT_MINOR, retainLock, labels, operationOptions);
//			return ret.getDataObjects().get(0).getIdentity().getValueAsString();
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error checkinDocument: " + e.getMessage(), e);
//		}
//	}
//
//	public Map<String, Object> altDocumentumPropertyMapper(Map<String, ?> olpProperties, String comment) {
//    	Map<String, Object> newProperties = new LinkedHashMap<>();
//    	newProperties.putAll(olpProperties);
//    	Map<String,String> propertyMap = new LinkedHashMap<>();
//    	
//    	if (newProperties.get("cmis:checkinComment") != null) {
//    		newProperties.put("cmis:checkinComment",comment);
//    	}
//    	
//    	propertyMap.put("cmis:name","object_name");
//    	propertyMap.put("cmis:objectId","r_object_id");
//    	propertyMap.put("cmis:baseTypeId","cmis:document");
//    	propertyMap.put("cmis:objectTypeId","r_object_type");
//    	propertyMap.put("cmis:createdBy","r_creator_name");
//    	propertyMap.put("cmis:creationDate","r_creation_date");
//    	propertyMap.put("cmis:lastModifiedBy","r_modifier");
//    	propertyMap.put("cmis:lastModificationDate","r_modification_date");
//    	propertyMap.put("cmis:changeToken","i_vstamp");
//    	propertyMap.put("cmis:isImmutable","r_immutable_flag");
//    	propertyMap.put("cmis:isLatestVersion","i_has_folder");
//    	propertyMap.put("cmis:versionLabel"," r_version_label");
//    	propertyMap.put("cmis:versionSeriesId","i_chronicle_id");
//    	propertyMap.put("cmis:versionSeriesCheckedOutBy","r_lock_owner");
//    	propertyMap.put("cmis:checkinComment","log_entry");
//    	propertyMap.put("cmis:contentStreamLength","r_full_content_size");
//    	propertyMap.put("cmis:contentStreamMimeType","a_content_type");
//    	propertyMap.put("cmis:contentStreamFileName","object_name Read-only");
//    	propertyMap.put("cmis:contentStreamId","i_contents_id");
//
//    	for (Map.Entry<String, String> prop : propertyMap.entrySet()) {
//    		if (newProperties.get(prop.getKey()) != null) {
//   				newProperties.put(prop.getValue(), newProperties.get(prop.getKey()));
//   				newProperties.remove(prop.getKey());
//    		}    		
//    	}
//    	
//    	return newProperties;
//    }
//	
//	public String altCheckinDocument(String idDocument, String nextVersionLabel, Boolean isMajor, Map<String, Object> attributes, String extension, byte[] content, String comment) throws Exception {
//
//		ObjectIdentity objectIdentity = getDocumentIdentity(idDocument);
//		DataObject dataObject = new DataObject(objectIdentity);
//		
//		attributes = altDocumentumPropertyMapper(attributes, comment);
//		AGEDmsPropertySet altAttributes = new AGEDmsPropertySet();
//		//AGEDmsPropertySet altAttributes = getDocumentProperties("",idDocument);
//
//		String documentumContentType = null;
//		if (extension == null)
//			documentumContentType = "unknown";
//		else {
//			// Lookup the Documentum content type for the extension, if we
//			// cannot find a mapping
//			// we suppose the extension is the Documentum content type.
//			documentumContentType = dfsContentTypeMapper.getDocumentumContentTypeByDosExtension(extension);
//			if (documentumContentType == null || documentumContentType.isEmpty())
//				documentumContentType = extension;
//		}		
//		
//		attributes.put(FIELD_CONTENT_TYPE, documentumContentType);
//		
//		for (String attrName : attributes.keySet())
//	 	{
//			Object attValue = attributes.get(attrName);
//			Boolean isArrayEmpty = false;
//			if (attValue instanceof ArrayList) isArrayEmpty = ((ArrayList) attValue).isEmpty();
//			if ((attValue != null) && (attValue != Collections.emptyList()) && (isArrayEmpty != true)) {
//				altAttributes.add(attrName, attValue);
//			}
//	 	}
//		
//		//Set properties
//		String objectName = getDocumentType("LEOS", idDocument);
//		dataObject.setProperties(mapAdocPropertiesToDocumentumProperties(altAttributes,objectName));
//		
//		// Set TransferProfile
//		ContentTransferProfile transferProfile = new ContentTransferProfile();
//		transferProfile.setTransferMode(ContentTransferMode.MTOM);
//		CheckinProfile checkinProfile = new CheckinProfile();
//		checkinProfile.setMakeCurrent(true);
//		OperationOptions operationOptions = new OperationOptions();
//		operationOptions.setContentTransferProfile(transferProfile);
//		operationOptions.setCheckinProfile(checkinProfile);
//
//		// Set Content
//		dataObject.getContents().add(new BinaryContent(content, documentumContentType));
//
//		DataPackage dataPackage = new DataPackage();
//		dataPackage.addDataObject(dataObject);
//
//		boolean retainLock = false;
//		List<String> labels = new ArrayList<>();
//
//		VersionStrategy version;
//		if (isMajor == true) {
//			version = VersionStrategy.NEXT_MAJOR;
//		}
//		else {
//			version = VersionStrategy.NEXT_MINOR;
//		}
//		
//		try {
//			DataPackage ret = getVersionControlService().checkin(dataPackage, version, retainLock, labels, operationOptions);
//			return ret.getDataObjects().get(0).getIdentity().getValueAsString();
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error checkinDocument: " + e.getMessage(), e);
//		}
//	}
//	
//	public void cancelCheckoutDocument(String idDocument) throws Exception {
//
//		ObjectIdentity objectIdentity = getDocumentIdentity(idDocument);
//		ObjectIdentitySet objectIdSet = new ObjectIdentitySet(objectIdentity);
//
//		try {
//			getVersionControlService().cancelCheckout(objectIdSet);
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error cancelCheckoutDocument: " + e.getMessage(), e);
//		}
//	}
//
//	public String getDocumentVersionId(String idApplication, String idDocument, String version) throws Exception {
//
//		try {
//			List<String> documentVersionIds = this.getDocumentVersionIds(idApplication, idDocument);
//			ObjectIdentitySet objectIdSet = new ObjectIdentitySet();
//			for (int i = 0; i < documentVersionIds.size(); i++) {
//				String objectId = documentVersionIds.get(i);
//				Qualification objectQualification = new Qualification("dm_document (all) where r_object_id = '" + objectId + "'");
//				ObjectIdentity objectIdentity = new ObjectIdentity(objectQualification, repositoryName);
//				objectIdSet.getIdentities().add(objectIdentity);
//			}
//			List<VersionInfo> versionInfoList = getVersionControlService().getVersionInfo(objectIdSet);
//			Iterator iterator = versionInfoList.iterator();
//			while (iterator.hasNext()) {
//				VersionInfo versionInfo = (VersionInfo) iterator.next();
//				if (versionInfo.getVersion().equals(version))
//					return versionInfo.getIdentity().getValueAsString();
//			}
//			return null;
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error getDocumentVersionId: " + e.getMessage(), e);
//		}
//	}
//
//	public List<String> getDocumentVersions(String idApplication, String idDocument) throws Exception {
//
//		try {
//			List<String> documentVersionIds = getDocumentVersionIds(idApplication, idDocument);
//			ObjectIdentitySet objectIdSet = new ObjectIdentitySet();
//			for (int i = 0; i < documentVersionIds.size(); i++) {
//				String objectId = documentVersionIds.get(i);
//				Qualification objectQualification = new Qualification("dm_document (all) where r_object_id = '" + objectId + "'");
//				ObjectIdentity objectIdentity = new ObjectIdentity(objectQualification, repositoryName);
//				objectIdSet.getIdentities().add(objectIdentity);
//			}
//			List<VersionInfo> versionInfoList = getVersionControlService().getVersionInfo(objectIdSet);
//			List<String> objectIdsRet = new ArrayList<>();
//			Iterator iterator = versionInfoList.iterator();
//			while (iterator.hasNext()) {
//				VersionInfo versionInfo = (VersionInfo) iterator.next();
//				objectIdsRet.add(versionInfo.getVersion());
//			}
//			return objectIdsRet;
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error getDocumentVersions: " + e.getMessage(), e);
//		}
//	}
//
//	private List<String> getDocumentVersionIds(String idApplication, String idDocument) throws ServiceException {
//		PassthroughQuery query = new PassthroughQuery();
//		query.setQueryString("select * from dm_document (all) where i_chronicle_id in (select i_chronicle_id from dm_document (all) where r_object_id = '" + idDocument + "')");
//		query.addRepository(repositoryName);
//		QueryExecution queryEx = new QueryExecution();
//		queryEx.setCacheStrategyType(CacheStrategyType.DEFAULT_CACHE_STRATEGY);
//		QueryResult queryResult = getQueryService().execute(query, queryEx, null);
//		DataPackage dataPackage = queryResult.getDataPackage();
//		List<DataObject> dataObjects = dataPackage.getDataObjects();
//		List<String> objectIdsRet = new ArrayList<>();
//		Iterator iterator = dataObjects.iterator();
//		while (iterator.hasNext()) {
//			DataObject dataObject = (DataObject) iterator.next();
//			String objectId = dataObject.getIdentity().getValueAsString();
//			objectIdsRet.add(objectId);
//		}
//		return objectIdsRet;
//	}
//
//	public boolean existsDocument(String idApplication, String idDocument) throws Exception {
//
//		ObjectIdentity objectIdentity = getDocumentIdentity(idDocument);
//		ObjectIdentitySet objectIdSet = new ObjectIdentitySet(objectIdentity);
//
//		try {
//			DataPackage dataPackage = getObjectService().get(objectIdSet, null);
//			return (dataPackage.getDataObjects().get(0) != null);
//		} catch (Exception e) {
//			return false;
//		}
//	}
//
//	public AGEDmsPropertySet getDocumentProperties(String idApplication, String idDocument) throws Exception {
//		return getDocumentProperties(idApplication, idDocument, null);
//	}
//
//	public AGEDmsPropertySet getDocumentProperties(String idApplication, String idDocument, AGEDmsPropertySet properties) throws Exception {
//
//		ObjectIdentity objectIdentity = getDocumentIdentity(idDocument);
//		ObjectIdentitySet objectIdSet = new ObjectIdentitySet(objectIdentity);
//
//		// Consulta unicamente las propiedades demandadas si se pasa un
//		// AGEDmsPropertySet
//		PropertyProfile propertyProfile = new PropertyProfile();
//		if (properties != null) {
//			propertyProfile.setFilterMode(PropertyFilterMode.SPECIFIED_BY_INCLUDE);
//			ArrayList<String> includeProperties = new ArrayList<>();
//			for (int i = 0; i < properties.size(); i++) {
//				AGEDmsProperty property = properties.get(i);
//				includeProperties.add(property.getName());
//			}
//			propertyProfile.setIncludeProperties(includeProperties);
//		}
//
//		OperationOptions operationOptions = new OperationOptions();
//		operationOptions.setPropertyProfile(propertyProfile);
//
//		try {
//			DataPackage dataPackage = getObjectService().get(objectIdSet, operationOptions);
//			PropertySet dfsProperties = dataPackage.getDataObjects().get(0).getProperties();
//			return mapDocumentumPropertiesToADocProperties(dfsProperties);
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error getDocumentProperties: " + e.getMessage(), e);
//		}
//	}
//
//	public void setDocumentProperties(String idApplication, String idDocument, AGEDmsPropertySet properties) throws Exception {
//
//		ObjectIdentity objectIdentity = getDocumentIdentity(idDocument);
//		DataObject dataObject = new DataObject(objectIdentity);
//
//		PropertyProfile propertyProfile = new PropertyProfile();
//		propertyProfile.setFilterMode(PropertyFilterMode.ALL);
//		OperationOptions operationOptions = new OperationOptions();
//		operationOptions.setProfile(propertyProfile);
//
//		String objectName = getDocumentType(idApplication, idDocument);
//		PropertySet dfsProperties = mapAdocPropertiesToDocumentumProperties(properties, objectName);
//		dataObject.setProperties(dfsProperties);
//
//		try {
//			getObjectService().update(new DataPackage(dataObject), operationOptions);
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error getDocumentProperties: " + e.getMessage(), e);
//		}
//	}
//
//	public AGEDmsProperty getDocumentProperty(String idApplication, String idDocument, AGEDmsProperty property) throws Exception {
//
//		ObjectIdentity objectIdentity = getDocumentIdentity(idDocument);
//		ObjectIdentitySet objectIdSet = new ObjectIdentitySet(objectIdentity);
//
//		// Consulta unicamente las propiedades demandadas
//		PropertyProfile propertyProfile = new PropertyProfile();
//		propertyProfile.setFilterMode(PropertyFilterMode.SPECIFIED_BY_INCLUDE);
//		ArrayList<String> includeProperties = new ArrayList<>();
//		includeProperties.add(property.getName());
//		propertyProfile.setIncludeProperties(includeProperties);
//
//		OperationOptions operationOptions = new OperationOptions();
//		operationOptions.setPropertyProfile(propertyProfile);
//
//		try {
//			DataPackage dataPackage = getObjectService().get(objectIdSet, operationOptions);
//			PropertySet dfsProperties = dataPackage.getDataObjects().get(0).getProperties();
//			if (dfsProperties.iterator().hasNext()) {
//				Property dfsProperty = dfsProperties.iterator().next();
//				return mapDocumentumPropertyToADocProperty(dfsProperty);
//			} else {
//				throw new Exception("Property with name " + property.getName() + " not found.");
//			}
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error getDocumentProperty: " + e.getMessage(), e);
//		}
//	}
//
//	public void setDocumentProperty(String idApplication, String idDocument, AGEDmsProperty property) throws Exception {
//
//		ObjectIdentity objectIdentity = getDocumentIdentity(idDocument);
//		DataObject dataObject = new DataObject(objectIdentity);
//
//		PropertyProfile propertyProfile = new PropertyProfile();
//		propertyProfile.setFilterMode(PropertyFilterMode.ALL);
//		OperationOptions operationOptions = new OperationOptions();
//		operationOptions.setProfile(propertyProfile);
//
//		PropertySet dfsProperties = new PropertySet();
//		String objectName = getDocumentType(idApplication, idDocument);
//		Property dfsProperty = mapAdocPropertyToDocumentumProperty(property, objectName);
//		dfsProperties.set(dfsProperty);
//		dataObject.setProperties(dfsProperties);
//
//		try {
//			getObjectService().update(new DataPackage(dataObject), operationOptions);
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error setDocumentProperty: " + e.getMessage(), e);
//		}
//	}
//
//	public void deleteDocument(String idApplication, String idDocument) throws Exception {
//
//		ObjectIdentity objectIdentity = getDocumentIdentity(idDocument);
//		ObjectIdentitySet objectIdSet = new ObjectIdentitySet(objectIdentity);
//
//		DeleteProfile deleteProfile = new DeleteProfile();
//		deleteProfile.setDeepDeleteFolders(true);
//		deleteProfile.setDeepDeleteChildrenInFolders(true);
//		OperationOptions operationOptions = new OperationOptions();
//		operationOptions.setDeleteProfile(deleteProfile);
//
//		try {
//			getObjectService().delete(objectIdSet, operationOptions);
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error deleteDocument: " + e.getMessage(), e);
//		}
//	}
//
//	public void renameDocument(String idApplication, String idDocument, String name) throws Exception {
//		AGEDmsStringProperty property = new AGEDmsStringProperty(FIELD_OBJECT_NAME, name);
//		setDocumentProperty(idApplication, idDocument, property);
//	}
//
//	public void moveDocument(String idApplication, String idDocument, String idFolderTo) throws Exception {
//
//		ObjectIdentity objectIdentityDocument = getDocumentIdentity(idDocument);
//		ObjectIdentitySet objectIdSetDocument = new ObjectIdentitySet(objectIdentityDocument);
//
//		String idFolderFrom = getDocumentParentId(idApplication, idDocument);
//		logger.debug("idFolderFrom = " + idFolderFrom);
//		logger.debug("idFolderTo = " + idFolderTo);
//		logger.debug("idDocument = " + idDocument);
//
//		ObjectIdentity objectIdentityFolderFrom = getFolderIdentity(idFolderFrom);
//		ObjectLocation locationFrom = new ObjectLocation(objectIdentityFolderFrom);
//
//		ObjectIdentity objectIdentityFolderTo = getFolderIdentity(idFolderTo);
//		ObjectLocation locationTo = new ObjectLocation(objectIdentityFolderTo);
//
//		try {
//			getObjectService().move(objectIdSetDocument, locationFrom, locationTo, new DataPackage(), null);
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error moveDocument: " + e.getMessage(), e);
//		}
//	}
//
//	public List<String> getDocumentsInFolder(String idApplication, String idFolder) throws Exception {
//		PassthroughQuery query = new PassthroughQuery();
//		query.setQueryString("select r_object_id from dm_document where folder(id('" + idFolder + "'))");
//		query.addRepository(repositoryName);
//		QueryExecution queryEx = new QueryExecution();
//		queryEx.setCacheStrategyType(CacheStrategyType.DEFAULT_CACHE_STRATEGY);
//
//		try {
//			QueryResult queryResult = getQueryService().execute(query, queryEx, null);
//			DataPackage dataPackage = queryResult.getDataPackage();
//			List<DataObject> dataObjects = dataPackage.getDataObjects();
//			List<String> objectIdsRet = new ArrayList<>();
//			Iterator iterator = dataObjects.iterator();
//			while (iterator.hasNext()) {
//				DataObject dataObject = (DataObject) iterator.next();
//				String objectId = dataObject.getIdentity().getValueAsString();
//				objectIdsRet.add(objectId);
//			}
//			return objectIdsRet;
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error getDocumentsFolder: " + e.getMessage(), e);
//		}
//	}
//
//	public List<String> searchDocuments(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, AGEDmsProperty returnProperty) throws Exception {
//		return searchDocuments(idApplication, type, searchProperties, returnProperty, maxSearchResultsConf);
//	}
//
//	public List<String> searchDocuments(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, AGEDmsProperty returnProperty, int maxSearchResults) throws Exception {
//
//		StructuredQuery query = new StructuredQuery();
//		query.setObjectType(type);
//		query.addRepository(repositoryName);
//		query.setIncludeAllVersions(false);
//		query.setIncludeHidden(false);
//		query.setDateFormat(DATE_FORMAT_DFS);
//		query.setDatabaseSearch(true); // Tiene que ser true, de lo contrario
//										// seria un FTR y tarda en indexar.
//
//		// Solo establecemos un scope si pasamos un idApplication
//		if (idApplication != null) {
//			RepositoryScope scope = new RepositoryScope();
//			scope.setLocationPath("/" + idApplication);
//			scope.setRepositoryName(repositoryName);
//			List<RepositoryScope> scopes = new ArrayList<>();
//			scopes.add(scope);
//			query.setScopes(scopes);
//		}
//
//		ExpressionSet expressionSet = new ExpressionSet();
//		// Por defecto concatenamos las expresiones mediante AND
//		expressionSet.setOperator(ExpressionSetOperator.AND);
//		query.setRootExpressionSet(expressionSet);
//
//		for (int i = 0; searchProperties != null && i < searchProperties.size(); i++) {
//			AGEDmsProperty property = searchProperties.get(i);
//			logger.debug(property.getName() + " " + property.getSearchCondition() + " " + property.getValueAsString());
//			AGEAttributeDataType dataType = getAttributeDataType(type, property.getName());
//			List<String> values = convertAdocPropertyToDocumentumStringFormat(property, dataType);
//			if (values == null) {
//				// Si no se pasa valores tampoco establecemos una expresión
//			} else if (values.size() == 1 && Condition.valueOf(property.getSearchCondition().name()) != Condition.IN && Condition.valueOf(property.getSearchCondition().name()) != Condition.NOT_IN) {
//				// Si se pasa un valor usamos el SimpleValue sin embargo si la condición de búsqueda es IN o NOT_IN debemos usar un ValueList.
//				PropertyExpression simpleValuePropertyExpression = new PropertyExpression();
//				simpleValuePropertyExpression.setProperty(property.getName());
//				simpleValuePropertyExpression.setCaseSensitive(false);
//				simpleValuePropertyExpression.setCondition(Condition.valueOf(property.getSearchCondition().name()));
//				SimpleValue simpleValue = new SimpleValue();
//				simpleValue.setValue(values.get(0));
//				simpleValuePropertyExpression.setValue(simpleValue);
//				expressionSet.addExpression(simpleValuePropertyExpression);
//			} else {
//				PropertyExpression valueListPropertyExpression = new PropertyExpression();
//				valueListPropertyExpression.setProperty(property.getName());
//				valueListPropertyExpression.setCaseSensitive(false);
//				valueListPropertyExpression.setCondition(Condition.valueOf(property.getSearchCondition().name()));
//				ValueList valueList = new ValueList();
//				valueList.setValues(values);
//				valueListPropertyExpression.setValue(valueList);
//				expressionSet.addExpression(valueListPropertyExpression);
//			}
//		}
//
//		// Execute Query
//		QueryExecution queryExec = new QueryExecution(0, maxSearchResults, maxSearchResults);
//		logger.debug("maxSearchResults [" + maxSearchResults + "]");
//
//		PropertyProfile propertyProfile = new PropertyProfile(PropertyFilterMode.SPECIFIED_BY_INCLUDE);
//		if (returnProperty == null)
//			propertyProfile.setIncludeProperties(Arrays.asList("r_object_id"));
//		else
//			propertyProfile.setIncludeProperties(Arrays.asList(returnProperty.getName()));
//		OperationOptions options = new OperationOptions();
//		options.setPropertyProfile(propertyProfile);
//
//		try {
//			QueryResult queryResult = getSearchService().execute(query, queryExec, options);
//			DataPackage dataPackage = queryResult.getDataPackage();
//			List<DataObject> dataObjects = dataPackage.getDataObjects();
//			logger.debug("Found " + dataObjects.size() + " document(s)");
//			if (queryResult.getQueryStatus().isHasMoreResults() && maxSearchResults == maxSearchResultsConf)
//				throw new Exception("The query has more results");
//			List<String> objectIdsRet = new ArrayList<>();
//			Iterator iterator = dataObjects.iterator();
//			while (iterator.hasNext()) {
//				DataObject dataObject = (DataObject) iterator.next();
//				PropertySet properties = dataObject.getProperties();
//				if (returnProperty == null)
//					objectIdsRet.add(properties.get("r_object_id").getValueAsString());
//				else {
//					if (properties.get(returnProperty.getName()) != null) {
//						String returnPropertyValue = properties.get(returnProperty.getName()).getValueAsString();
//						if (returnPropertyValue != null && !returnPropertyValue.isEmpty() && !objectIdsRet.contains(returnPropertyValue))
//							objectIdsRet.add(properties.get(returnProperty.getName()).getValueAsString());
//					}
//				}
//			}
//			return objectIdsRet;
//		} catch (Exception e) {
//			throw new Exception("Error searchDocuments: " + e.getMessage(), e);
//		}
//	}
//
//	public AGEDmsSearchResult searchDocumentsPaged(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, AGEDmsProperty returnProperty, AGEDmsProperty orderProperty, Boolean isAscending, int startIndex, int pageSize) throws Exception {
//		List<AGEDmsProperty> returnProperties = new ArrayList<>();
//		if (returnProperty != null)
//			returnProperties.add(returnProperty);
//		return searchDocumentsPaged(idApplication, type, searchProperties, returnProperties, orderProperty, isAscending, startIndex, pageSize);
//	}
//
//	public AGEDmsSearchResult searchDocumentsPaged(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, List<AGEDmsProperty> returnProperties, AGEDmsProperty orderProperty, Boolean isAscending, int startIndex, int pageSize) throws Exception {
//
//		// En este metodo resolvemos 2 problemas con la búsqueda mediante las
//		// DFS.
//		// 1. Las DFS no son capaces de devolver el número de registros totales
//		// de una búsqueda
//		// Si se búsca por FTR si es poosible, sin embargo hemos detectado
//		// inconsistencias en los
//		// resultados (por ejemplo r_object_ids que ya no existen).
//		// Ademas la búsqueda por FTR tiene como inconveniente que no se indexa
//		// al instante.
//		// 2. Buscar por la BB.DD y usando un "order by" no devuelve resultados
//		// si filtramos por
//		// metadatos multivaluados en combinación de multiples EQUAL.
//
//		if (startIndex < 0)
//			startIndex = 0;
//
//		if (pageSize < 1)
//			pageSize = maxSearchResultsConf;
//
//		try {
//
//			StructuredQuery query = new StructuredQuery();
//			query.setObjectType(type);
//			query.addRepository(repositoryName);
//			query.setIncludeAllVersions(false);
//			query.setIncludeHidden(false);
//			query.setDateFormat(DATE_FORMAT_DFS);
//			query.setDatabaseSearch(true);
//
//			// Solo establecemos un scope si pasamos un idApplication
//			if (idApplication != null) {
//				RepositoryScope scope = new RepositoryScope();
//				scope.setLocationPath("/" + idApplication);
//				scope.setRepositoryName(repositoryName);
//				List<RepositoryScope> scopes = new ArrayList<>();
//				scopes.add(scope);
//				query.setScopes(scopes);
//			}
//
//			ExpressionSet expressionSet = new ExpressionSet();
//			// Por defecto concatenamos las expresiones mediante AND
//			expressionSet.setOperator(ExpressionSetOperator.AND);
//			query.setRootExpressionSet(expressionSet);
//
//			for (int i = 0; searchProperties != null && i < searchProperties.size(); i++) {
//				AGEDmsProperty property = searchProperties.get(i);
//				logger.debug(property.getName() + " " + property.getSearchCondition() + " " + property.getValueAsString());
//				AGEAttributeDataType dataType = getAttributeDataType(type, property.getName());
//				List<String> values = convertAdocPropertyToDocumentumStringFormat(property, dataType);
//				if (values == null) {
//					// Si no se pasa valores tampoco establecemos una expresión
//				} else if (values.size() == 1 && Condition.valueOf(property.getSearchCondition().name()) != Condition.IN && Condition.valueOf(property.getSearchCondition().name()) != Condition.NOT_IN) {
//					// Si se pasa un valor usamos el SimpleValue sin embargo si la condición de búsqueda es IN o NOT_IN debemos usar un ValueList.
//					PropertyExpression simpleValuePropertyExpression = new PropertyExpression();
//					simpleValuePropertyExpression.setProperty(property.getName());
//					simpleValuePropertyExpression.setCaseSensitive(false);
//					simpleValuePropertyExpression.setCondition(Condition.valueOf(property.getSearchCondition().name()));
//					SimpleValue simpleValue = new SimpleValue();
//					simpleValue.setValue(values.get(0));
//					simpleValuePropertyExpression.setValue(simpleValue);
//					expressionSet.addExpression(simpleValuePropertyExpression);
//				} else {
//					PropertyExpression valueListPropertyExpression = new PropertyExpression();
//					valueListPropertyExpression.setProperty(property.getName());
//					valueListPropertyExpression.setCaseSensitive(false);
//					valueListPropertyExpression.setCondition(Condition.valueOf(property.getSearchCondition().name()));
//					ValueList valueList = new ValueList();
//					valueList.setValues(values);
//					valueListPropertyExpression.setValue(valueList);
//					expressionSet.addExpression(valueListPropertyExpression);
//				}
//			}
//
//			// Set order by
//			OrderByClause orderByClause = new OrderByClause();
//			if (orderProperty == null) {
//				orderByClause.setAttribute("r_object_id");
//				orderByClause.setAscending(true);
//			} else {
//				orderByClause.setAttribute(orderProperty.getName());
//				if (isAscending == null)
//					orderByClause.setAscending(true);
//				else
//					orderByClause.setAscending(isAscending);
//			}
//			List<OrderByClause> orderByClauses = new ArrayList<>();
//			orderByClauses.add(orderByClause);
//			query.setOrderByClauses(orderByClauses);
//
//			// Set return properties
//			PropertyProfile propertyProfile = new PropertyProfile(PropertyFilterMode.SPECIFIED_BY_INCLUDE);
//			List<String> includedProperties = new ArrayList<>();
//			for (AGEDmsProperty returnProperty: returnProperties)
//				includedProperties.add(returnProperty.getName());
//			if (includedProperties.isEmpty())
//				includedProperties.add("r_object_id");
//			propertyProfile.setIncludeProperties(includedProperties);
//			OperationOptions options = new OperationOptions();
//			options.setPropertyProfile(propertyProfile);
//
//			// Lanzamos un execute (ojo pedimos solo 1 resultado) para simplemente obtener la DQL, luego modificamos la DQL para solventar el problema del "order by" y poder obtener el número de resultados
//			QueryExecution queryExec = new QueryExecution(0, 1, 1);
//			QueryResult queryResult = getSearchService().execute(query, queryExec, options);
//
//			// Comprobamos que no hay problema con la query
//			if (queryResult.getQueryStatus().getRepositoryStatusInfos().get(0).getStatus() == Status.FAILURE)
//				throw new Exception(queryResult.getQueryStatus().getRepositoryStatusInfos().get(0).getErrorMessage());
//
//			// Obtenemos la DQL
//			String dqlQuery = null;
//			for (int i = 0; i < queryResult.getQueryStatus().getRepositoryStatusInfos().get(0).getRepositoryEvents().size(); i++) {
//				if (queryResult.getQueryStatus().getRepositoryStatusInfos().get(0).getRepositoryEvents().get(i).getEventSubId() == RepositoryEventSubId.NATIVEQUERY) {
//					dqlQuery = queryResult.getQueryStatus().getRepositoryStatusInfos().get(0).getRepositoryEvents().get(i).getMessage();
//					break;
//				}
//			}
//			if (dqlQuery == null)
//				throw new Exception("Documentum DFS Exception-" +"No DQL Query found.");
//
//			// El problema del "order by" reside en la parte ENABLE(...) de la DQL, asi que lo quitamos y lo lanzamos otra vez como PassthroughQuery.
//			logger.debug("Original DQL Query [" + dqlQuery + "]");
//			int idx = dqlQuery.indexOf(" ENABLE(");
//			if (idx != -1)
//				dqlQuery = dqlQuery.substring(0, idx);
//			logger.debug("Altered DQL Query [" + dqlQuery + "]");
//
//			PassthroughQuery ptQuery = new PassthroughQuery();
//			ptQuery.setQueryString(dqlQuery);
//			ptQuery.addRepository(repositoryName);
//
//			logger.debug("startIndex [" + startIndex + "]");
//			logger.debug("pageSize [" + pageSize + "]");
//			logger.debug("maxSearchResults [" + maxSearchResultsConf + "]");
//
//			// Ejecutar Passthrough Query
//			queryExec = new QueryExecution(startIndex, pageSize, maxSearchResultsConf);
//			queryResult = getSearchService().execute(ptQuery, queryExec, options);
//
//			DataPackage dataPackage = queryResult.getDataPackage();
//			List<DataObject> dataObjects = dataPackage.getDataObjects();
//			List<AGEDmsPropertySet> objectIdsRet = new ArrayList<>();
//			Iterator iterator = dataObjects.iterator();
//			while (iterator.hasNext()) {
//				DataObject dataObject = (DataObject) iterator.next();
//				PropertySet properties = dataObject.getProperties();
//				AGEDmsPropertySet dmsProperties = new AGEDmsPropertySet();
//				for (AGEDmsProperty returnProperty: returnProperties) {
//					Property returnPropertyValue = properties.get(returnProperty.getName());
//					if (returnPropertyValue != null && returnPropertyValue.getValueAsObject() != null) 
//						dmsProperties.add(returnProperty.getName(), returnPropertyValue.getValueAsObject());
//					else {
//						AGEDmsStringProperty dmsProperty = new AGEDmsStringProperty(returnProperty.getName());
//						dmsProperties.add(dmsProperty);
//					}
//				}
//				if (dmsProperties.size() == 0)
//					dmsProperties.add("r_object_id", properties.get("r_object_id").getValueAsString());
//				objectIdsRet.add(dmsProperties);
//			}
//			AGEDmsSearchResult searchResult = new AGEDmsSearchResult();
//			searchResult.setResult(objectIdsRet);
//
//			// Si obtenemos resultados determinamos el número total de registros
//			if (!objectIdsRet.isEmpty()) {
//				// Transformamos la DQL en un count(*)
//				idx = dqlQuery.indexOf("FROM");
//				dqlQuery = "SELECT count(*) as hitcount " + dqlQuery.substring(idx);
//				// Quitamos el ORDER BY
//				idx = dqlQuery.indexOf(" ORDER BY");
//				dqlQuery = dqlQuery.substring(0, idx);
//				logger.debug("Count DQL Query [" + dqlQuery + "]");
//				ptQuery = new PassthroughQuery();
//				ptQuery.setQueryString(dqlQuery);
//				ptQuery.addRepository(repositoryName);
//				queryResult = getQueryService().execute(ptQuery, null, null);
//				iterator = queryResult.getDataPackage().getDataObjects().iterator();
//				if (iterator.hasNext()) {
//					DataObject dataObject = (DataObject) iterator.next();
//					PropertySet properties = dataObject.getProperties();
//					searchResult.setHitCount(((NumberProperty) properties.get("hitcount")).getValue().intValue());
//				}
//			} else {
//				searchResult.setHitCount(0);
//			}
//			return searchResult;
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error searchDocumentsPaged: " + e.getMessage(), e);
//		}
//
//	}
//
//	public AGEDmsSearchResult searchDocumentsFullText(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, String searchText, AGEDmsProperty returnProperty, AGEDmsProperty orderProperty, Boolean isAscending, int maxResults) throws Exception {
//		List<AGEDmsProperty> returnProperties = new ArrayList<>();
//		if (returnProperty != null)
//			returnProperties.add(returnProperty);
//		return searchDocumentsFullText(idApplication, type, searchProperties, searchText, returnProperties, orderProperty, isAscending, maxResults);
//	}
//
//	public AGEDmsSearchResult searchDocumentsFullText(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, String searchText, List<AGEDmsProperty> returnProperties, AGEDmsProperty orderProperty, Boolean isAscending, int maxResults) throws Exception {
//
//		if (maxResults < 1)
//			maxResults = maxSearchResultsConf;
//
//		StructuredQuery query = new StructuredQuery();
//		query.setObjectType(type);
//		query.addRepository(repositoryName);
//		query.setIncludeAllVersions(false);
//		query.setIncludeHidden(false);
//		query.setDateFormat(DATE_FORMAT_DFS);
//		query.setDatabaseSearch(false);
//
//		// Solo establecemos un scope si pasamos un idApplication
//		if (idApplication != null) {
//			RepositoryScope scope = new RepositoryScope();
//			scope.setLocationPath("/" + idApplication);
//			scope.setRepositoryName(repositoryName);
//			List<RepositoryScope> scopes = new ArrayList<>();
//			scopes.add(scope);
//			query.setScopes(scopes);
//		}
//
//		ExpressionSet expressionSet = new ExpressionSet();
//		// Por defecto concatenamos las expresiones mediante AND
//		expressionSet.setOperator(ExpressionSetOperator.AND);
//		query.setRootExpressionSet(expressionSet);
//
//		for (int i = 0; searchProperties != null && i < searchProperties.size(); i++) {
//			AGEDmsProperty property = searchProperties.get(i);
//			logger.debug(property.getName() + " " + property.getSearchCondition() + " " + property.getValueAsString());
//			AGEAttributeDataType dataType = getAttributeDataType(type, property.getName());
//			List<String> values = convertAdocPropertyToDocumentumStringFormat(property, dataType);
//			if (values == null) {
//				// Si no se pasa valores tampoco establecemos una expresión
//			} else if (values.size() == 1 && Condition.valueOf(property.getSearchCondition().name()) != Condition.IN && Condition.valueOf(property.getSearchCondition().name()) != Condition.NOT_IN) {
//				// Si se pasa un valor usamos el SimpleValue sin embargo si la condición de búsqueda es IN o NOT_IN debemos usar un ValueList.
//				PropertyExpression simpleValuePropertyExpression = new PropertyExpression();
//				simpleValuePropertyExpression.setProperty(property.getName());
//				simpleValuePropertyExpression.setCaseSensitive(false);
//				simpleValuePropertyExpression.setCondition(Condition.valueOf(property.getSearchCondition().name()));
//				SimpleValue simpleValue = new SimpleValue();
//				simpleValue.setValue(values.get(0));
//				simpleValuePropertyExpression.setValue(simpleValue);
//				expressionSet.addExpression(simpleValuePropertyExpression);
//			} else {
//				PropertyExpression valueListPropertyExpression = new PropertyExpression();
//				valueListPropertyExpression.setProperty(property.getName());
//				valueListPropertyExpression.setCaseSensitive(false);
//				valueListPropertyExpression.setCondition(Condition.valueOf(property.getSearchCondition().name()));
//				ValueList valueList = new ValueList();
//				valueList.setValues(values);
//				valueListPropertyExpression.setValue(valueList);
//				expressionSet.addExpression(valueListPropertyExpression);
//			}
//		}
//
//		if (searchText != null && !searchText.isEmpty()) {
//			// Full-text search is case-insensitive
//			expressionSet.addExpression(new FullTextExpression("(" + searchText + ")"));
//		}
//
//		// Set order by
//		OrderByClause orderByClause = new OrderByClause();
//		if (orderProperty == null) {
//			orderByClause.setAttribute("r_object_id");
//			orderByClause.setAscending(true);
//		} else {
//			orderByClause.setAttribute(orderProperty.getName());
//			if (isAscending == null)
//				orderByClause.setAscending(true);
//			else
//				orderByClause.setAscending(isAscending);
//		}
//		List<OrderByClause> orderByClauses = new ArrayList<>();
//		orderByClauses.add(orderByClause);
//		query.setOrderByClauses(orderByClauses);
//
//		// Set return properties
//		PropertyProfile propertyProfile = new PropertyProfile(PropertyFilterMode.SPECIFIED_BY_INCLUDE);
//		List<String> includedProperties = new ArrayList<>();
//		for (AGEDmsProperty returnProperty: returnProperties)
//			includedProperties.add(returnProperty.getName());
//		if (includedProperties.isEmpty())
//			includedProperties.add("r_object_id");
//		propertyProfile.setIncludeProperties(includedProperties);
//		OperationOptions options = new OperationOptions();
//		options.setPropertyProfile(propertyProfile);
//
//		// Execute Query
//		QueryExecution queryExec = new QueryExecution(0, maxResults, maxResults);
//		logger.debug("maxResults [" + maxResults + "]");
//
//		try {
//			QueryResult queryResult = getSearchService().execute(query, queryExec, options);
//			DataPackage dataPackage = queryResult.getDataPackage();
//			List<DataObject> dataObjects = dataPackage.getDataObjects();
//			logger.debug("Found " + dataObjects.size() + " document(s)");
//			List<AGEDmsPropertySet> objectIdsRet = new ArrayList<>();
//			Iterator iterator = dataObjects.iterator();
//			while (iterator.hasNext()) {
//				DataObject dataObject = (DataObject) iterator.next();
//				PropertySet properties = dataObject.getProperties();
//				AGEDmsPropertySet dmsProperties = new AGEDmsPropertySet();
//				for (AGEDmsProperty returnProperty: returnProperties) {
//					Property returnPropertyValue = properties.get(returnProperty.getName());
//					if (returnPropertyValue != null && returnPropertyValue.getValueAsObject() != null) 
//						dmsProperties.add(returnProperty.getName(), returnPropertyValue.getValueAsObject());
//					else {
//						AGEDmsStringProperty dmsProperty = new AGEDmsStringProperty(returnProperty.getName());
//						dmsProperties.add(dmsProperty);
//					}
//				}
//				if (dmsProperties.size() == 0)
//					dmsProperties.add("r_object_id", properties.get("r_object_id").getValueAsString());
//				objectIdsRet.add(dmsProperties);
//			}
//			AGEDmsSearchResult searchResult = new AGEDmsSearchResult();
//			searchResult.setResult(objectIdsRet);
//			searchResult.setMoreResults(queryResult.getQueryStatus().isHasMoreResults());
//			searchResult.setHitCount(objectIdsRet.size());
//			return searchResult;
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error searchDocumentsFullText: " + e.getMessage(), e);
//		}
//
//	}
//
//	public List<String> searchFolders(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, AGEDmsProperty returnProperty) throws Exception {
//		return searchFolders(idApplication, type, searchProperties, returnProperty, maxSearchResultsConf);
//	}
//
//	public List<String> searchFolders(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, AGEDmsProperty returnProperty, int maxSearchResults) throws Exception {
//
//		StructuredQuery query = new StructuredQuery();
//		query.setObjectType(type);
//		query.addRepository(repositoryName);
//		query.setIncludeAllVersions(false);
//		query.setIncludeHidden(false);
//		query.setDateFormat(DATE_FORMAT_DFS);
//		query.setDatabaseSearch(true); // Tiene que ser true, de lo contrario
//										// seria un FTR y tarda en indexar.
//
//		// Solo establecemos un scope si pasamos un idApplication
//		if (idApplication != null) {
//			RepositoryScope scope = new RepositoryScope();
//			scope.setLocationPath("/" + idApplication);
//			scope.setRepositoryName(repositoryName);
//			List<RepositoryScope> scopes = new ArrayList<>();
//			scopes.add(scope);
//			query.setScopes(scopes);
//		}
//
//		ExpressionSet expressionSet = new ExpressionSet();
//		// Por defecto concatenamos las expresiones mediante AND
//		expressionSet.setOperator(ExpressionSetOperator.AND);
//		query.setRootExpressionSet(expressionSet);
//
//		for (int i = 0; searchProperties != null && i < searchProperties.size(); i++) {
//			AGEDmsProperty property = searchProperties.get(i);
//			logger.debug(property.getName() + " " + property.getSearchCondition() + " " + property.getValueAsString());
//			AGEAttributeDataType dataType = getAttributeDataType(type, property.getName());
//			List<String> values = convertAdocPropertyToDocumentumStringFormat(property, dataType);
//			if (values == null) {
//				// Si no se pasa valores tampoco establecemos una expresión
//			} else if (values.size() == 1 && Condition.valueOf(property.getSearchCondition().name()) != Condition.IN && Condition.valueOf(property.getSearchCondition().name()) != Condition.NOT_IN) {
//				// Si se pasa un valor usamos el SimpleValue sin embargo si la condición de búsqueda es IN o NOT_IN debemos usar un ValueList.
//				PropertyExpression simpleValuePropertyExpression = new PropertyExpression();
//				simpleValuePropertyExpression.setProperty(property.getName());
//				simpleValuePropertyExpression.setCaseSensitive(false);
//				simpleValuePropertyExpression.setCondition(Condition.valueOf(property.getSearchCondition().name()));
//				SimpleValue simpleValue = new SimpleValue();
//				simpleValue.setValue(values.get(0));
//				simpleValuePropertyExpression.setValue(simpleValue);
//				expressionSet.addExpression(simpleValuePropertyExpression);
//			} else {
//				PropertyExpression valueListPropertyExpression = new PropertyExpression();
//				valueListPropertyExpression.setProperty(property.getName());
//				valueListPropertyExpression.setCaseSensitive(false);
//				valueListPropertyExpression.setCondition(Condition.valueOf(property.getSearchCondition().name()));
//				ValueList valueList = new ValueList();
//				valueList.setValues(values);
//				valueListPropertyExpression.setValue(valueList);
//				expressionSet.addExpression(valueListPropertyExpression);
//			}
//		}
//
//		// Execute Query
//		QueryExecution queryExec = new QueryExecution(0, maxSearchResults, maxSearchResults);
//		logger.debug("maxSearchResults [" + maxSearchResults + "]");
//
//		PropertyProfile propertyProfile = new PropertyProfile(PropertyFilterMode.SPECIFIED_BY_INCLUDE);
//		if (returnProperty == null)
//			propertyProfile.setIncludeProperties(Arrays.asList("r_object_id"));
//		else
//			propertyProfile.setIncludeProperties(Arrays.asList(returnProperty.getName()));
//		OperationOptions options = new OperationOptions();
//		options.setPropertyProfile(propertyProfile);
//
//		try {
//			QueryResult queryResult = getSearchService().execute(query, queryExec, options);
//			if (queryResult.getQueryStatus().isHasMoreResults() && maxSearchResults == maxSearchResultsConf)
//				throw new Exception("Documentum DFS Exception-" +"The query has more results");
//			DataPackage dataPackage = queryResult.getDataPackage();
//			List<DataObject> dataObjects = dataPackage.getDataObjects();
//			logger.debug("Found " + dataObjects.size() + " folder(s)");
//			List<String> objectIdsRet = new ArrayList<>();
//			Iterator iterator = dataObjects.iterator();
//			while (iterator.hasNext()) {
//				DataObject dataObject = (DataObject) iterator.next();
//				PropertySet properties = dataObject.getProperties();
//				if (returnProperty == null)
//					objectIdsRet.add(properties.get("r_object_id").getValueAsString());
//				else {
//					if (properties.get(returnProperty.getName()) != null) {
//						String returnPropertyValue = properties.get(returnProperty.getName()).getValueAsString();
//						if (returnPropertyValue != null && !returnPropertyValue.isEmpty() && !objectIdsRet.contains(returnPropertyValue))
//							objectIdsRet.add(properties.get(returnProperty.getName()).getValueAsString());
//					}
//				}
//			}
//			return objectIdsRet;
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error searchFolders: " + e.getMessage(), e);
//		}
//	}
//
//	public AGEDmsSearchResult searchFoldersPaged(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, AGEDmsProperty returnProperty, AGEDmsProperty orderProperty, Boolean isAscending, int startIndex, int pageSize) throws Exception {
//		List<AGEDmsProperty> returnProperties = new ArrayList<>();
//		if (returnProperty != null)
//			returnProperties.add(returnProperty);
//		return searchFoldersPaged(idApplication, type, searchProperties, returnProperties, orderProperty, isAscending, startIndex, pageSize);
//	}
//
//	public AGEDmsSearchResult searchFoldersPaged(String idApplication, String type, AGEDmsSearchPropertySet searchProperties, List<AGEDmsProperty> returnProperties, AGEDmsProperty orderProperty, Boolean isAscending, int startIndex, int pageSize) throws Exception {
//
//		// En este metodo resolvemos 2 problemas con la búsqueda mediante las DFS.
//		// 1. Las DFS no son capaces de devolver el número de registros totales de una búsqueda
//		// Si se búsca por FTR si es poosible, sin embargo hemos detectado
//		// inconsistencias en los resultados (por ejemplo r_object_ids que ya no existen).
//		// Ademas la búsqueda por FTR tiene como inconveniente que no se indexa al instante.
//		// 2. Buscar por la BB.DD y usando un "order by" no devuelve resultados si filtramos por metadatos multivaluados en combinación de multiples EQUAL.
//
//		if (startIndex < 0)
//			startIndex = 0;
//
//		if (pageSize < 1)
//			pageSize = maxSearchResultsConf;
//
//		try {
//
//			StructuredQuery query = new StructuredQuery();
//			query.setObjectType(type);
//			query.addRepository(repositoryName);
//			query.setIncludeAllVersions(false);
//			query.setIncludeHidden(false);
//			query.setDateFormat(DATE_FORMAT_DFS);
//			query.setDatabaseSearch(true); // Hacemos siempre un database search
//
//			// Solo establecemos un scope si pasamos un idApplication
//			if (idApplication != null) {
//				RepositoryScope scope = new RepositoryScope();
//				scope.setLocationPath("/" + idApplication);
//				scope.setRepositoryName(repositoryName);
//				List<RepositoryScope> scopes = new ArrayList<>();
//				scopes.add(scope);
//				query.setScopes(scopes);
//			}
//
//			ExpressionSet expressionSet = new ExpressionSet();
//			// Por defecto concatenamos las expresiones mediante AND
//			expressionSet.setOperator(ExpressionSetOperator.AND);
//			query.setRootExpressionSet(expressionSet);
//
//			for (int i = 0; searchProperties != null && i < searchProperties.size(); i++) {
//				AGEDmsProperty property = searchProperties.get(i);
//				logger.debug(property.getName() + " " + property.getSearchCondition() + " " + property.getValueAsString());
//				AGEAttributeDataType dataType = getAttributeDataType(type, property.getName());
//				List<String> values = convertAdocPropertyToDocumentumStringFormat(property, dataType);
//				if (values == null) {
//					// Si no se pasa valores tampoco establecemos una expresión
//				} else if (values.size() == 1 && Condition.valueOf(property.getSearchCondition().name()) != Condition.IN && Condition.valueOf(property.getSearchCondition().name()) != Condition.NOT_IN) {
//					// Si se pasa un valor usamos el SimpleValue sin embargo si la condición de búsqueda es IN o NOT_IN debemos usar un ValueList.
//					PropertyExpression simpleValuePropertyExpression = new PropertyExpression();
//					simpleValuePropertyExpression.setProperty(property.getName());
//					simpleValuePropertyExpression.setCaseSensitive(false);
//					simpleValuePropertyExpression.setCondition(Condition.valueOf(property.getSearchCondition().name()));
//					SimpleValue simpleValue = new SimpleValue();
//					simpleValue.setValue(values.get(0));
//					simpleValuePropertyExpression.setValue(simpleValue);
//					expressionSet.addExpression(simpleValuePropertyExpression);
//				} else {
//					PropertyExpression valueListPropertyExpression = new PropertyExpression();
//					valueListPropertyExpression.setProperty(property.getName());
//					valueListPropertyExpression.setCaseSensitive(false);
//					valueListPropertyExpression.setCondition(Condition.valueOf(property.getSearchCondition().name()));
//					ValueList valueList = new ValueList();
//					valueList.setValues(values);
//					valueListPropertyExpression.setValue(valueList);
//					expressionSet.addExpression(valueListPropertyExpression);
//				}
//			}
//
//			// Set order by
//			OrderByClause orderByClause = new OrderByClause();
//			if (orderProperty == null) {
//				orderByClause.setAttribute("r_object_id");
//				orderByClause.setAscending(true);
//			} else {
//				orderByClause.setAttribute(orderProperty.getName());
//				if (isAscending == null)
//					orderByClause.setAscending(true);
//				else
//					orderByClause.setAscending(isAscending);
//			}
//			List<OrderByClause> orderByClauses = new ArrayList<>();
//			orderByClauses.add(orderByClause);
//			query.setOrderByClauses(orderByClauses);
//
//			// Set return properties
//			PropertyProfile propertyProfile = new PropertyProfile(PropertyFilterMode.SPECIFIED_BY_INCLUDE);
//			List<String> includedProperties = new ArrayList<>();
//			for (AGEDmsProperty returnProperty: returnProperties)
//				includedProperties.add(returnProperty.getName());
//			if (includedProperties.isEmpty())
//				includedProperties.add("r_object_id");
//			propertyProfile.setIncludeProperties(includedProperties);
//			OperationOptions options = new OperationOptions();
//			options.setPropertyProfile(propertyProfile);
//
//			// Lanzamos un execute (ojo pedimos solo 1 resultado) para simplemente obtener la DQL, luego modificamos la DQL para solventar el problema del "order by" y poder obtener el número de resultados
//			QueryExecution queryExec = new QueryExecution(0, 1, 1);
//			QueryResult queryResult = getSearchService().execute(query, queryExec, options);
//
//			// Comprobamos que no hay problema con la query
//			if (queryResult.getQueryStatus().getRepositoryStatusInfos().get(0).getStatus() == Status.FAILURE)
//				throw new Exception(queryResult.getQueryStatus().getRepositoryStatusInfos().get(0).getErrorMessage());
//
//			// Obtenemos la DQL
//			String dqlQuery = null;
//			for (int i = 0; i < queryResult.getQueryStatus().getRepositoryStatusInfos().get(0).getRepositoryEvents().size(); i++) {
//				if (queryResult.getQueryStatus().getRepositoryStatusInfos().get(0).getRepositoryEvents().get(i).getEventSubId() == RepositoryEventSubId.NATIVEQUERY) {
//					dqlQuery = queryResult.getQueryStatus().getRepositoryStatusInfos().get(0).getRepositoryEvents().get(i).getMessage();
//					break;
//				}
//			}
//			if (dqlQuery == null)
//				throw new Exception("Documentum DFS Exception-" +"No DQL Query found.");
//
//			// El problema del "order by" reside en la parte ENABLE(...) de la DQL, asi que lo quitamos y lo lanzamos otra vez como PassthroughQuery.
//			logger.debug("Original DQL Query [" + dqlQuery + "]");
//			int idx = dqlQuery.indexOf(" ENABLE(");
//			if (idx != -1)
//				dqlQuery = dqlQuery.substring(0, idx);
//			logger.debug("Altered DQL Query [" + dqlQuery + "]");
//
//			PassthroughQuery ptQuery = new PassthroughQuery();
//			ptQuery.setQueryString(dqlQuery);
//			ptQuery.addRepository(repositoryName);
//
//			logger.debug("startIndex [" + startIndex + "]");
//			logger.debug("pageSize [" + pageSize + "]");
//			logger.debug("maxSearchResults [" + maxSearchResultsConf + "]");
//
//			// Ejecutar Passthrough Query
//			queryExec = new QueryExecution(startIndex, pageSize, maxSearchResultsConf);
//			queryResult = getSearchService().execute(ptQuery, queryExec, options);
//
//			DataPackage dataPackage = queryResult.getDataPackage();
//			List<DataObject> dataObjects = dataPackage.getDataObjects();
//			List<AGEDmsPropertySet> objectIdsRet = new ArrayList<>();
//			Iterator iterator = dataObjects.iterator();
//			while (iterator.hasNext()) {
//				DataObject dataObject = (DataObject) iterator.next();
//				PropertySet properties = dataObject.getProperties();
//				AGEDmsPropertySet dmsProperties = new AGEDmsPropertySet();
//				for (AGEDmsProperty returnProperty: returnProperties) {
//					Property returnPropertyValue = properties.get(returnProperty.getName());
//					if (returnPropertyValue != null && returnPropertyValue.getValueAsObject() != null) 
//						dmsProperties.add(returnProperty.getName(), returnPropertyValue.getValueAsObject());
//					else {
//						AGEDmsStringProperty dmsProperty = new AGEDmsStringProperty(returnProperty.getName());
//						dmsProperties.add(dmsProperty);
//					}
//				}
//				if (dmsProperties.size() == 0)
//					dmsProperties.add("r_object_id", properties.get("r_object_id").getValueAsString());
//				objectIdsRet.add(dmsProperties);
//			}
//			AGEDmsSearchResult searchResult = new AGEDmsSearchResult();
//			searchResult.setResult(objectIdsRet);
//
//			// Si obtenemos resultados determinamos el número total de registros
//			if (!objectIdsRet.isEmpty()) {
//				// Transformamos la DQL en un count(*)
//				idx = dqlQuery.indexOf("FROM");
//				dqlQuery = "SELECT count(*) as hitcount " + dqlQuery.substring(idx);
//				// Quitamos el ORDER BY
//				idx = dqlQuery.indexOf(" ORDER BY");
//				dqlQuery = dqlQuery.substring(0, idx);
//				logger.debug("Count DQL Query [" + dqlQuery + "]");
//				ptQuery = new PassthroughQuery();
//				ptQuery.setQueryString(dqlQuery);
//				ptQuery.addRepository(repositoryName);
//				queryResult = getQueryService().execute(ptQuery, null, null);
//				iterator = queryResult.getDataPackage().getDataObjects().iterator();
//				if (iterator.hasNext()) {
//					DataObject dataObject = (DataObject) iterator.next();
//					PropertySet properties = dataObject.getProperties();
//					searchResult.setHitCount(((NumberProperty) properties.get("hitcount")).getValue().intValue());
//				}
//			} else {
//				searchResult.setHitCount(0);
//			}
//			return searchResult;
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Error searchFoldersPaged: " + e.getMessage(), e);
//		}
//	}
//
//	public List<AGEAttribute> getAttributes(String artifactTypeName) throws Exception {
//		logger.debug("artifactTypeName [" + artifactTypeName + "]");
//		try {
//			HashMap<String, AGEAttribute> attributeTypeMap = loadAttributes(artifactTypeName);
//			List<AGEAttribute> attributeTypeList = new ArrayList<>();
//			attributeTypeList.addAll(attributeTypeMap.values());
//			return attributeTypeList;
//		} catch (Exception e) {
//			logger.debug("Error getAttributes: " + e.getMessage());
//		}
//		return (List<AGEAttribute>) null;
//	}
//
//	public AGEAttributeDataType getAttributeDataType(String artifactTypeName, String attributeName) throws Exception {
//		logger.debug("artifactTypeName [" + artifactTypeName + "], attributeName [" + attributeName + "]");
//		AGEAttribute attribute = loadAttribute(artifactTypeName, attributeName);
//		return attribute.getDatatype();
//	}
//
//	public Integer getAttributeLength(String artifactTypeName, String attributeName) throws Exception {
//		logger.debug("artifactTypeName [" + artifactTypeName + "], attributeName [" + attributeName + "]");
//		AGEAttribute attribute = loadAttribute(artifactTypeName, attributeName);
//		return attribute.getLength();
//	}
//
//	public boolean isAttributeRepeating(String artifactTypeName, String attributeName) throws Exception {
//		logger.debug("artifactTypeName [" + artifactTypeName + "], attributeName [" + attributeName + "]");
//		AGEAttribute attribute = loadAttribute(artifactTypeName, attributeName);
//		return attribute.isRepeating();
//	}
//
//	public List<String> getAttributeDefaultValues(String artifactTypeName, String attributeName) throws Exception {
//		logger.debug("artifactTypeName [" + artifactTypeName + "], attributeName [" + attributeName + "]");
//		AGEAttribute attribute = loadAttribute(artifactTypeName, attributeName);
//		return attribute.getDefaultValues();
//	}
//
//	public void reloadAttributes() throws Exception {
//		logger.debug("Reloading attributes");
//		artifactTypeMap.clear();
//	}
//
//	public List<AGEDmsPropertySet> executeQuery(String query) throws Exception {
//		List<AGEDmsPropertySet> list = new ArrayList<>();
//		try {
//			logger.debug("Executing query [" + query + "]");
//			PassthroughQuery pasQuery = new PassthroughQuery();
//			pasQuery.setQueryString(query);
//			pasQuery.addRepository(repositoryName);
//			QueryExecution queryEx = new QueryExecution(0, 50000, 50000);
//			queryEx.setCacheStrategyType(CacheStrategyType.DEFAULT_CACHE_STRATEGY);
//			QueryResult queryResult = getQueryService().execute(pasQuery, queryEx, null);
//			Iterator iterator = queryResult.getDataPackage().getDataObjects().iterator();
//			while (iterator.hasNext()) {
//				DataObject dataObject = (DataObject) iterator.next();
//				list.add(mapDocumentumPropertiesToADocProperties(dataObject.getProperties()));
//			}
//		} catch (Exception e) {
//			logger.debug("Error executing query [" + query + "]: " + e.getMessage());
//		}
//		return list;
//	}
//
//	public void deployArtifacts(List<AGEArtifact> artifacts) throws Exception {
//		logger.debug("Deploying artifacts");
//		for (int i = 0; i < artifacts.size(); i++) {
//			// Para cada uno de los artefactos componemos una DQL
//			StringBuilder dql = new StringBuilder();
//
//			AGEArtifact artifact = artifacts.get(i);
//
//			dql.append("CREATE TYPE " + artifact.getTypeName());
//			String nextToken = " ";
//			for (int i2 = 0; artifact.getAttributes() != null && i2 < artifact.getAttributes().size(); i2++) {
//
//				if (i2 == 0)
//					nextToken = " (";
//
//				AGEAttribute attr = artifact.getAttributes().get(i2);
//
//				dql.append(nextToken + attr.getName());
//				switch (attr.getDatatype()) {
//				case E_00_BOOLEAN:
//					dql.append(" boolean");
//					break;
//				case E_01_INTEGER:
//					dql.append(" integer");
//					break;
//				case E_02_STRING:
//					if (attr.getLength() <= 0)
//						throw new Exception("Documentum DFS Exception-" +"Invalid length for artifact [" + artifact.getTypeName() + "] and attribute [" + attr.getName() + "]");
//					dql.append(" string(" + attr.getLength() + ")");
//					break;
//				case E_03_ID:
//					dql.append(" id");
//					break;
//				case E_04_TIME:
//					dql.append(" time");
//					break;
//				case E_05_DOUBLE:
//					dql.append(" double");
//					break;
//				}
//				if (attr.isRepeating())
//					dql.append(" repeating");
//				if ((i2 + 1) == artifact.getAttributes().size())
//					nextToken = ") ";
//				else
//					nextToken = ", ";
//			}
//			if (artifact.getSupertype() == null || artifact.getSupertype().isEmpty())
//				throw new Exception("Documentum DFS Exception-" +"Artifact [" + artifact.getTypeName() + "] should have a supertype");
//
//			dql.append(nextToken + "WITH SUPERTYPE " + artifact.getSupertype());
//			nextToken = " ";
//			dql.append(nextToken + "PUBLISH;");
//
//			logger.debug("Deploying artifact [" + dql + "]");
//			executeDQL(dql.toString());
//
//		}
//		logger.debug("Finished deploying artifacts");
//	}
//
//	private AGEDmsProperty mapDocumentumPropertyToADocProperty(Property property) throws Exception {
//		AGEDmsProperty propertyRet;
//		// Los campos TIME vacios de Documentum devuelven "null".
//		if (property instanceof DateProperty && property.getValueAsString().equalsIgnoreCase("null")) {
//			propertyRet = new AGEDmsDateProperty(property.getName(), null);
//		} else if (property instanceof BooleanArrayProperty) {
//			BooleanArrayProperty arrayProperty = (BooleanArrayProperty) property;
//			propertyRet = new AGEDmsBooleanArrayProperty(property.getName(), arrayProperty.getValues());
//		} else if (property instanceof ObjectIdProperty) {
//			propertyRet = new AGEDmsStringProperty(property.getName(), ((ObjectIdProperty) property).getValue().getId());
//		} else if (property instanceof ObjectIdArrayProperty) {
//			ObjectIdArrayProperty arrayProperty = (ObjectIdArrayProperty) property;
//			String[] objectIds = new String[arrayProperty.getValues().length];
//			for (int i = 0; i < arrayProperty.getValues().length; i++) {
//				objectIds[i] = arrayProperty.getValues()[i].getId();
//			}
//			propertyRet = new AGEDmsStringArrayProperty(property.getName(), objectIds);
//		} else {
//			propertyRet = AGEDmsPropertySet.createProperty(property.getName(), property.getValueAsObject());
//		}
//		return propertyRet;
//	}
//
//	private AGEDmsPropertySet mapDocumentumPropertiesToADocProperties(PropertySet properties) throws Exception {
//		AGEDmsPropertySet propertiesRet = new AGEDmsPropertySet();
//		for (Iterator<Property> dfsPropertySet = properties.iterator(); dfsPropertySet.hasNext();) {
//			Property dfsProperty = dfsPropertySet.next();
//			AGEDmsProperty property = mapDocumentumPropertyToADocProperty(dfsProperty);
//			propertiesRet.add(property);
//			logger.debug(property.getName() + " = [" + property.getValueAsString() + "], class = [" + property.getClass().getCanonicalName() + "]");
//		}
//		return propertiesRet;
//	}
//
//	private List<String> convertAdocPropertyToDocumentumStringFormat(AGEDmsProperty property, AGEAttributeDataType dataType) throws Exception {
//
//		ArrayList<String> values = new ArrayList<>();
//
//		if (property instanceof AGEDmsBooleanArrayProperty) {
//			Boolean[] booleans = ((AGEDmsBooleanArrayProperty) property).getValues();
//			for (int i = 0; booleans != null && i < booleans.length; i++)
//				values.add(booleans[i].toString());
//			return values;
//		}
//
//		if (property instanceof AGEDmsBooleanProperty) {
//			if (((AGEDmsBooleanProperty) property).getValueAsString() != null)
//				values.add(((AGEDmsBooleanProperty) property).getValueAsString());
//			return values;
//		}
//
//		if (property instanceof AGEDmsNumberArrayProperty) {
//			Number[] numbers = ((AGEDmsNumberArrayProperty) property).getValues();
//			for (int i = 0; numbers != null && i < numbers.length; i++)
//				values.add(numbers[i].toString());
//			return values;
//		}
//
//		if (property instanceof AGEDmsNumberProperty) {
//			if (((AGEDmsNumberProperty) property).getValueAsString() != null)
//				values.add(((AGEDmsNumberProperty) property).getValueAsString());
//			return values;
//		}
//
//		if (property instanceof AGEDmsDateArrayProperty) {
//			Date[] dates = ((AGEDmsDateArrayProperty) property).getValues();
//			for (int i = 0; dates != null && i < dates.length; i++)
//				values.add(dfsDateFormat.format(dates[i]));
//			return values;
//		}
//
//		if (property instanceof AGEDmsDateProperty) {
//			if (((AGEDmsDateProperty) property).getValueAsString() != null)
//				values.add(dfsDateFormat.format(((AGEDmsDateProperty) property).getValueAsObject()));
//			return values;
//		}
//
//		if (property instanceof AGEDmsStringArrayProperty) {
//			int numValues = ((AGEDmsStringArrayProperty) property).getValues().length;
//			for (int i = 0; i < numValues; i++) {
//				String value = ((AGEDmsStringArrayProperty) property).getValues()[i];
//				values.add(checkDfsValue(dataType, value));
//			}
//			return values;
//		}
//
//		if (property instanceof AGEDmsStringProperty) {
//			String value = ((AGEDmsStringProperty) property).getValue();
//			if (value != null)
//				values.add(checkDfsValue(dataType, value));
//			return values;
//		}
//
//		return (List<String>) null;
//	}
//
//	private Property mapAdocPropertyToDocumentumProperty(AGEDmsProperty property, String objectName) throws Exception {
//
//		if (property instanceof AGEDmsBooleanArrayProperty)
//			return new BooleanArrayProperty(property.getName(), ((AGEDmsBooleanArrayProperty) property).getValues());
//
//		if (property instanceof AGEDmsBooleanProperty)
//			return new BooleanProperty(property.getName(), ((AGEDmsBooleanProperty) property).getValue());
//
//		if (property instanceof AGEDmsNumberArrayProperty)
//			return new NumberArrayProperty(property.getName(), ((AGEDmsNumberArrayProperty) property).getValues());
//
//		if (property instanceof AGEDmsNumberProperty) {
//			// Aqui tenemos otro bug de documentum. Si pasamos null en un
//			// NumberProperty
//			// obtenemos un java.lang.NumberFormatException si la propiedad ya
//			// existe
//			// en el PropertySet.
//			if (((AGEDmsNumberProperty) property).getValue() == null)
//				return new StringProperty(property.getName(), null);
//			else
//				return new NumberProperty(property.getName(), ((AGEDmsNumberProperty) property).getValue());
//		}
//
//		if (property instanceof AGEDmsDateArrayProperty)
//			return new DateArrayProperty(property.getName(), ((AGEDmsDateArrayProperty) property).getValues());
//
//		if (property instanceof AGEDmsDateProperty)
//			return new DateProperty(property.getName(), ((AGEDmsDateProperty) property).getValue());
//
//		// En caso de pasar un string array comprobamos los tipos y sus valores.
//		// Si no comprobamos por ejemplo los integer y introducimos un real (por
//		// ejemplo 10.0), documentum se estrella debido a un bug.
//		if (property instanceof AGEDmsStringArrayProperty) {
//			int numValues = ((AGEDmsStringArrayProperty) property).getValues().length;
//			String[] values = new String[numValues];
//			AGEAttributeDataType dataType = getAttributeDataType(objectName, property.getName());
//			for (int i = 0; i < numValues; i++) {
//				String value = ((AGEDmsStringArrayProperty) property).getValues()[i];
//				values[i] = checkDfsValue(dataType, value);
//			}
//			StringArrayProperty dfsProperty = new StringArrayProperty();
//			dfsProperty.setName(property.getName());
//			dfsProperty.setValues(values);
//			return dfsProperty;
//		}
//
//		// En caso de pasar un string comprobamos los tipos y sus valores
//		if (property instanceof AGEDmsStringProperty) {
//			StringProperty dfsProperty = new StringProperty();
//			dfsProperty.setName(property.getName());
//			AGEAttributeDataType dataType = getAttributeDataType(objectName, property.getName());
//			String value = ((AGEDmsStringProperty) property).getValue();
//			dfsProperty.setValue(checkDfsValue(dataType, value));
//			return dfsProperty;
//		}
//
//		return null;
//	}
//
//	private PropertySet mapAdocPropertiesToDocumentumProperties(AGEDmsPropertySet properties, String objectName) throws Exception {
//		PropertySet propertiesRet = new PropertySet();
//		for (int i = 0; i < properties.size(); i++) {
//			AGEDmsProperty property = properties.get(i);
//			Property dfsProperty = mapAdocPropertyToDocumentumProperty(property, objectName);
//			propertiesRet.set(dfsProperty);
//			logger.debug(property.getName() + " = [" + (dfsProperty != null ? dfsProperty.getValueAsString() : "") + "], class = [" + property.getClass().getCanonicalName() + "]");
//		}
//		return propertiesRet;
//	}
//
//	private ObjectIdentity getFolderIdentity(String objectId) {
//		String dql = "dm_folder where r_object_id = '" + objectId + "'";
//		logger.debug("Qualifaction: " + dql);
//		Qualification objectQualification = new Qualification(dql);
//		return new ObjectIdentity(objectQualification, repositoryName);
//	}
//
//	private ObjectIdentity getDocumentIdentity(String objectId) {
//		String dql = "dm_document (all) where r_object_id = '" + objectId + "'";
//		logger.debug("Qualifaction: " + dql);
//		Qualification objectQualification = new Qualification(dql);
//		return new ObjectIdentity(objectQualification, repositoryName);
//	}
//
//	private ObjectIdentity getObjectIdentityByPath(String idApplication, String path) {
//		String dql = "/" + idApplication + "/" + path;
//		logger.debug("Qualifaction: " + dql);
//		ObjectPath objectPath = new ObjectPath(dql);
//		return new ObjectIdentity(objectPath, repositoryName);
//	}
//
//	private ObjectIdentity getApplicationCabinetIdentity(String idApplication) {
//		String dql = "/" + idApplication;
//		logger.debug("Qualifaction: " + dql);
//		ObjectPath objectPath = new ObjectPath(dql);
//		return new ObjectIdentity(objectPath, repositoryName);
//	}
//
//	private synchronized HashMap<String, AGEAttribute> loadAttributes(String artifactTypeName) throws Exception {
//		HashMap attributeTypeMap = null;
//		try {
//			// Lookup the attribute types in memory before consulting documentum
//			if (artifactTypeMap.containsKey(artifactTypeName)) {
//				logger.debug("Recycling attribute types for artifact type [" + artifactTypeName + "]");
//				attributeTypeMap = artifactTypeMap.get(artifactTypeName);
//			} else {
//				logger.debug("Loading attribute types for artifact type [" + artifactTypeName + "]");
//				attributeTypeMap = new HashMap<String, AGEAttribute>();
//				artifactTypeMap.put(artifactTypeName, attributeTypeMap);
//				PassthroughQuery query = new PassthroughQuery();
//
//				query.setQueryString(
//						"select distinct t1.name as name, t2.attr_name as attr_name, t2.attr_type as attr_type, t2.attr_repeating as attr_repeating, t2.attr_length as attr_length, t4.default_expr_value as default_value, t3.not_null as not_null, t3.is_required as is_required, t3.read_only as read_only from dm_type_sp t1, dm_type_rp t2, dmi_dd_attr_info_sp t3, dmi_dd_attr_info_rp t4 where t1.r_object_id = t2.r_object_id and t3.r_object_id = t4.r_object_id and t1.name = t3.type_name and t2.attr_name = t3.attr_name and t1.name = '"
//								+ artifactTypeName + "' order by t1.name, t2.attr_name");
//
//				query.addRepository(repositoryName);
//				QueryExecution queryEx = new QueryExecution();
//				queryEx.setCacheStrategyType(CacheStrategyType.DEFAULT_CACHE_STRATEGY);
//				queryEx.setMaxResultCount(1000); // Ojo
//				QueryResult queryResult = getQueryService().execute(query, queryEx, null);
//				Iterator iterator = queryResult.getDataPackage().getDataObjects().iterator();
//				while (iterator.hasNext()) {
//					DataObject dataObject = (DataObject) iterator.next();
//
//					String attrName = dataObject.getProperties().get("attr_name").getValueAsString();
//					String attrType = dataObject.getProperties().get("attr_type").getValueAsString();
//					int attrLength = Integer.parseInt(dataObject.getProperties().get("attr_length").getValueAsString());
//					boolean attrRepeating = !dataObject.getProperties().get("attr_repeating").getValueAsString().equalsIgnoreCase("0");
//					String defaultValue = dataObject.getProperties().get("default_value").getValueAsString();
//					boolean notNull = !dataObject.getProperties().get("not_null").getValueAsString().equalsIgnoreCase("0");
//					boolean isRequired = !dataObject.getProperties().get("is_required").getValueAsString().equalsIgnoreCase("0");
//					boolean readOnly = !dataObject.getProperties().get("read_only").getValueAsString().equalsIgnoreCase("0");
//
//					AGEAttribute attribute = null;
//					// If the attribute doesn't exist we add a new one,
//					// otherwise we add the default value to the existing
//					// attribute.
//					if (attributeTypeMap.get(attrName) == null) {
//						attribute = new AGEAttribute();
//						attribute.setName(attrName);
//						if (attrType.equals("0"))
//							attribute.setDatatype(AGEAttributeDataType.E_00_BOOLEAN);
//						else if (attrType.equals("1"))
//							attribute.setDatatype(AGEAttributeDataType.E_01_INTEGER);
//						else if (attrType.equals("2"))
//							attribute.setDatatype(AGEAttributeDataType.E_02_STRING);
//						else if (attrType.equals("3"))
//							attribute.setDatatype(AGEAttributeDataType.E_03_ID);
//						else if (attrType.equals("4"))
//							attribute.setDatatype(AGEAttributeDataType.E_04_TIME);
//						else if (attrType.equals("5"))
//							attribute.setDatatype(AGEAttributeDataType.E_05_DOUBLE);
//						else
//							throw new Exception("Documentum DFS Exception-" +"Unknown datatype encountered");
//						attribute.setLength(attrLength);
//						attribute.setRepeating(attrRepeating);
//						attribute.setNotNull(notNull);
//						attribute.setRequired(isRequired);
//						attribute.setReadOnly(readOnly);
//						attributeTypeMap.put(attribute.getName(), attribute);
//					} else {
//						attribute = (AGEAttribute) attributeTypeMap.get(attrName);
//						attribute.getDefaultValues().add(defaultValue);
//					}
//				}
//			}
//		} catch (Exception e) {
//			logger.debug("Error getting attribute types for artifact type [" + artifactTypeName + "]: " + e.getMessage());
//		}
//		return attributeTypeMap;
//	}
//
//	private synchronized AGEAttribute loadAttribute(String artifactTypeName, String attributeName) throws Exception {
//		AGEAttribute attribute = null;
//		try {
//			// Lookup the attribute type in memory before consulting documentum
//			if (artifactTypeMap.containsKey(artifactTypeName)) {
//				HashMap attributeTypeMap = artifactTypeMap.get(artifactTypeName);
//				if (attributeTypeMap.containsKey(attributeName)) {
//					logger.debug("Recycling attribute type [" + attributeName + "]");
//					attribute = (AGEAttribute) attributeTypeMap.get(attributeName);
//				}
//			} else {
//				loadAttributes(artifactTypeName);
//				HashMap attributeTypeMap = artifactTypeMap.get(artifactTypeName);
//				attribute = (AGEAttribute) attributeTypeMap.get(attributeName);
//			}
//		} catch (Exception e) {
//			logger.error("Error getting attribute type [" + attributeName + "] for artifact type [" + artifactTypeName + "]: " + e.getMessage());
//			throw new Exception("Documentum DFS Exception-" +"Error getting attribute type [" + attributeName + "] for artifact type [" + artifactTypeName + "]");
//		}
//
//		if (attribute == null) {
//			logger.error("AGEAttribute type [" + attributeName + "] doesn't exist for artifact type [" + artifactTypeName + "]");
//			throw new Exception("Documentum DFS Exception-" +"AGEAttribute type [" + attributeName + "] doesn't exist for artifact type [" + artifactTypeName + "]");
//		}
//
//		return attribute;
//	}
//
//	private String checkDfsValue(AGEAttributeDataType dataType, String value) throws Exception {
//		if (value == null || value.isEmpty())
//			return null;
//		switch (dataType) {
//		case E_00_BOOLEAN:
//			return checkDfsBoolean(value);
//		case E_01_INTEGER:
//			return checkDfsInteger(value);
//		case E_02_STRING:
//			return value;
//		case E_03_ID:
//			return value;
//		case E_04_TIME:
//			return checkDfsTime(value);
//		case E_05_DOUBLE:
//			return checkDfsDouble(value);
//		}
//		return null;
//	}
//
//	private String checkDfsTime(String dateValue) throws Exception {
//		if (dateValue == null || dateValue.isEmpty()) {
//			return null;
//		}
//		Date isoDate;
//		try {
//			isoDate = isoDateFormat.parse(dateValue);
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Invalid DFS time format.");
//		}
//
//		// parse will accept any date as long as it's in the format
//		// you defined, it simply rolls dates over, for example, december 32
//		// becomes jan 1 and december 0 becomes november 30
//		// This statement will make sure that once the string
//		// has been checked for proper formatting that the date is still the
//		// date that was entered, if it's not, we assume that the date is
//		// invalid
//		if (!isoDateFormat.format(isoDate).equals(dateValue))
//			throw new Exception("Documentum DFS Exception-" +"Invalid DFS time format.");
//
//		return dfsDateFormat.format(isoDate);
//	}
//
//	private String checkDfsInteger(String intValue) throws Exception {
//		if (intValue == null || intValue.isEmpty()) {
//			return null;
//		}
//		int parsedInteger;
//		try {
//			parsedInteger = Integer.parseInt(intValue);
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Invalid DFS integer format.");
//		}
//		return Integer.toString(parsedInteger);
//	}
//
//	private String checkDfsDouble(String doubleValue) throws Exception {
//		if (doubleValue == null || doubleValue.isEmpty()) {
//			return null;
//		}
//		double parsedDouble;
//		try {
//			parsedDouble = Double.parseDouble(doubleValue);
//		} catch (Exception e) {
//			throw new Exception("Documentum DFS Exception-" +"Invalid DFS double format.");
//		}
//		return Double.toString(parsedDouble);
//	}
//
//	private String checkDfsBoolean(String booleanValue) throws Exception {
//		if (booleanValue == null || booleanValue.isEmpty()) {
//			return null;
//		}
//		// Boolean.parseBoolean(booleanValue) doesn't throw an
//		// exception in case we pass for example "adaskdhsakh".
//		// So we will do this simple check.
//		if (booleanValue.equalsIgnoreCase("true"))
//			return "true";
//		if (booleanValue.equalsIgnoreCase("false"))
//			return "false";
//		throw new Exception("Documentum DFS Exception-" +"Invalid DFS boolean format.");
//	}
//	
//	public void testQueryServiceEndPoint() throws ServiceException {
//		getQueryService().execute(new PassthroughQuery(), new QueryExecution(), null);
//	}
//
//	public String getDfsDmsAddress() {
//		return host;
//	}	
//
}
