/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.cmis.repository;

import com.google.common.base.Stopwatch;
import eu.europa.ec.leos.cmis.extensions.AGECmisDocumentExtensions;
import eu.europa.ec.leos.cmis.mapping.AGECmisProperties;
import eu.europa.ec.leos.cmis.search.AGESearchStrategy;
import eu.europa.ec.leos.cmis.search.AGESearchStrategyProvider;
import eu.europa.ec.leos.cmis.support.AGEOperationContextProvider;
import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.cmis.LeosLegStatus;
import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.model.filter.QueryFilter;
import eu.europa.ec.leos.repository.AGERepositoryContext;
import org.apache.chemistry.opencmis.client.api.*;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.UnfileObject;
import org.apache.chemistry.opencmis.commons.enums.Updatability;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisBaseException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.documentum.fc.client.IDfSession;
import com.documentum.fc.common.DfException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import eu.europa.ec.leos.opentext.*;

import javax.inject.Provider;
import java.io.ByteArrayInputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;
import java.util.concurrent.TimeUnit;
import java.security.Principal;

import static eu.europa.ec.leos.cmis.support.AGEOperationContextProvider.getMinimalContext;
import static eu.europa.ec.leos.cmis.support.OperationContextProvider.getMinimalContext;

@Repository
@Primary
public class AGECmisRepository {

	@Value("${leos.cmis.repository.checkif}")
	private String checkInterface;

	private static final Logger logger = LoggerFactory.getLogger(AGECmisRepository.class);

	private final Session cmisSession;
	private final Provider<AGERepositoryContext> repositoryContextProvider;
	private static final Map<String, Long> synchronizedKeys = new ConcurrentHashMap<>();

	@Autowired
	protected AGEDfcSessionFactoryImpl sessionFactory;

	@Autowired
	protected AGEDfsDmsFactory dfsFactory;

	AGECmisRepository(Session cmisSession, Provider<AGERepositoryContext> repositoryContextProvider) {
		this.cmisSession = cmisSession;
		this.repositoryContextProvider = repositoryContextProvider;
	}

	private AGESearchStrategy getSearchStrategy() {
		return AGESearchStrategyProvider.getSearchStrategy(cmisSession);
	}

	Folder createFolder(final String path, final String name) {
		logger.trace("Creating folder... [path=" + path + ", name=" + name + "]");
		OperationContext context = getMinimalContext(cmisSession);
		Folder parentFolder = findFolderByPath(path, context);

		Map<String, String> properties = new HashMap<>();
		properties.put(PropertyIds.OBJECT_TYPE_ID, BaseTypeId.CMIS_FOLDER.value());
		properties.put(PropertyIds.NAME, name);

		return parentFolder.createFolder(properties, null, null, null, context);
	}

	void deleteFolder(final String path) {
		logger.trace("Deleting folder... [path=" + path + "]");
		OperationContext context = getMinimalContext(cmisSession);
		Folder folder = findFolderByPath(path, context);
		folder.deleteTree(true, UnfileObject.DELETE, true);
	}

	Document createDocumentFromContent(final String path, final String name, Map<String, Object> properties,
			final String mimeType, byte[] contentBytes) {
		logger.trace("Creating document... [path=" + path + ", name=" + name + ", mimeType=" + mimeType + "]");
		OperationContext context = getMinimalContext(cmisSession);

		// REDMINE 1054 - propiedad lastModifiedBy
		properties.put(AGECmisProperties.LASTMODIFIER.getId(), getUser());
		properties.put(AGECmisProperties.CREATOR.getId(), getUser());
		
		// Paso a LEOS 3.0.0. - cmis:checkincomment admite solo 120 caracteres en
		// Documentum
		String comment = (String) properties.get("cmis:checkinComment");
		if (comment != null) {
			properties.put(AGECmisProperties.LEOS_CHECKINCOMMENT.getId(), comment);
			properties.put("cmis:checkinComment", comment.substring(0, Math.min(comment.length(), 119)));
		}

		Folder targetFolder = findFolderByPath(path, context);
		ByteArrayInputStream byteStream = new ByteArrayInputStream(contentBytes);
		ContentStream contentStream = cmisSession.getObjectFactory().createContentStream(name,
				(long) contentBytes.length, mimeType, byteStream);

		Map<String, Object> updatedProperties = new LinkedHashMap<>();
		updatedProperties.putAll(properties);
		updatedProperties.put(AGECmisProperties.VERSION_TYPE.getId(), VersionType.MINOR.value());
		updatedProperties.put(AGECmisProperties.VERSION_LABEL.getId(), getNextVersionLabel(VersionType.MINOR, null));

		return targetFolder.createDocument(updatedProperties, contentStream, VersioningState.MINOR);
	}

	Document createDocumentFromSource(final String sourceId, String path, Map<String, Object> properties) {
		logger.trace("Creating document from source... [sourceId=" + sourceId + "]");
		OperationContext context = getMinimalContext(cmisSession);

		// REDMINE 1054 - propiedad lastModifiedBy
		properties.put(AGECmisProperties.LASTMODIFIER.getId(), getUser());
		properties.put(AGECmisProperties.CREATOR.getId(), getUser());
		// Paso a LEOS 3.0.0. - cmis:checkincomment admite solo 120 caracteres en
		// Documentum
		String comment = (String) properties.get("cmis:checkinComment");
		if (comment != null) {
			properties.put(AGECmisProperties.LEOS_CHECKINCOMMENT.getId(), comment);
			properties.put("cmis:checkinComment", comment.substring(0, Math.min(comment.length(), 119)));
		}

		Folder targetFolder = findFolderByPath(path, context);
		Document sourceDoc = findDocumentById(sourceId, false, context);

		Map<String, Object> updatedProperties = new LinkedHashMap<>();

		// Adaption to allow Documentum creating the new doc with default ACLs instead
		// of copying with inherited ACLs
		for (Property<?> prop : sourceDoc.getProperties()) {
			if (prop.getDefinition().getUpdatability() == Updatability.READWRITE
					|| prop.getDefinition().getUpdatability() == Updatability.ONCREATE) {
				updatedProperties.put(prop.getId(), prop.getValue());
			}
		}

		updatedProperties.putAll(properties);
		updatedProperties.put(AGECmisProperties.VERSION_TYPE.getId(), VersionType.MINOR.value());
		updatedProperties.put(AGECmisProperties.VERSION_LABEL.getId(), getNextVersionLabel(VersionType.MINOR, null));
//		// Deleting predefined values of Documentum properties used for ACL Definition
		updatedProperties.remove(AGECmisProperties.ACL_NAME.getId());
		updatedProperties.remove(AGECmisProperties.ACL_DOMAIN.getId());
		updatedProperties.remove(AGECmisProperties.GROUP_NAME.getId());
		updatedProperties.remove(AGECmisProperties.GROUP_PERMIT.getId());

		return targetFolder.createDocument(updatedProperties, sourceDoc.getContentStream(), VersioningState.MINOR);
	}

	void deleteDocumentById(final String id) {
		logger.trace("Deleting document... [id=" + id + "]");
		OperationContext context = getMinimalContext(cmisSession);
		CmisObject cmisObject = cmisSession.getObject(id, context);
		require(cmisObject instanceof Document, "CMIS object referenced by id [" + id + "] is not a Document!");
		cmisObject.delete(true);
	}

	Document updateDocument(final String id, Map<String, ?> properties) {
		logger.trace("Updating document properties... [id=" + id + "]");
		OperationContext context = getMinimalContext(cmisSession);
		Document document = findDocumentById(id, true, context);
		return (Document) document.updateProperties(properties);
	}

	public Document updateDocument(String id, Map<String, ?> properties, byte[] updatedDocumentBytes,
			VersionType versionType, String comment) {
		Stopwatch stopwatch = Stopwatch.createStarted();
		logger.trace("Updating document properties and content... [id={}]", id);
		try {
			synchronized (getSyncKey(id)) {
				OperationContext context = getMinimalContext(cmisSession);
				Document actualVersion = findDocumentById(id, true, context);

				String newId;
				if (actualVersion.isVersionSeriesCheckedOut()) {
					newId = actualVersion.getVersionSeriesCheckedOutId();
					logger.trace("Document already check out ... [id={} , newVersion id={}]", id,
							actualVersion.getVersionSeriesCheckedOutId());
				} else {
					// newId = actualVersion.checkOut().getId();
					newId = actualVersion.getId();
					checkOutWorkingCopy(newId);
				}
				Document newVersion = findDocumentById(newId, false, context);
				final Map<String, String> oldVersions = repositoryContextProvider.get()
						.getVersionsWithoutVersionLabel();
				final String actualLabelVersion = AGECmisDocumentExtensions.getLeosVersionLabel(actualVersion,
						oldVersions);
				final String nextLabelVersion = getNextVersionLabel(versionType, actualLabelVersion);
				Document updatedDocument = checkInWorkingCopy(nextLabelVersion, newVersion, properties,
						updatedDocumentBytes, versionType, comment);
				logger.trace("Updated document properties and content...");
				if (updatedDocument == null) {
					throw new IllegalStateException("Update not successful for document:" + id);
				} else {
					return updatedDocument;
				}
			}
		} finally {
			cleanSyncKey(id);
			logger.debug("Repository updateDocument {}, in {} milliseconds ({} sec)", id,
					stopwatch.elapsed(TimeUnit.MILLISECONDS), stopwatch.elapsed(TimeUnit.SECONDS));
		}
	}

	List<String> commentConversion(String comment) {
		List<String> commentString = new ArrayList<>();
		double nStrings = Math.floorDiv(comment.length(), 3999) + 1;
		for (int i = 0; i < nStrings; i++) {
			commentString.add(comment.substring(i * 3999, Math.min(comment.length(), (i + 1) * 3999)));
		}
		return commentString;
	}

	private Document checkInWorkingCopy(String nextVersionLabel, Document pwc, Map<String, ?> properties,
			byte[] updatedDocumentBytes, VersionType versionType, String comment) {
		Stopwatch stopwatch = Stopwatch.createStarted();
		logger.trace("Document checkInWorkingCopy [actual document id: {}, nextVersionLabel: {}]", pwc.getId(),
				nextVersionLabel);
		Map<String, Object> updatedProperties = new LinkedHashMap<>();
		// KLUGE LEOS-2408 workaround for issue related to reset properties values with
		// OpenCMIS In-Memory server
		// add input properties to existing properties map, eventually overriding old
		// properties values
		pwc.getProperties().forEach(property -> {
			if (Updatability.READWRITE == property.getDefinition().getUpdatability()) {
				updatedProperties.put(property.getId(), property.getValue());
			}
		});

		updatedProperties.putAll(properties);
		updatedProperties.put(AGECmisProperties.VERSION_TYPE.getId(), versionType.value());
		updatedProperties.put(AGECmisProperties.VERSION_LABEL.getId(), nextVersionLabel);
		final boolean isMajor = versionType.equals(VersionType.MAJOR) || versionType.equals(VersionType.INTERMEDIATE);

		// REDMINE 1054 - propiedad lastModifiedBy
		updatedProperties.put(AGECmisProperties.LASTMODIFIER.getId(), getUser());
		// Paso a LEOS 3.0.0. - cmis:checkincomment admite solo 120 caracteres en
		// Documentum
		updatedProperties.put(AGECmisProperties.LEOS_CHECKINCOMMENT.getId(),
				comment.substring(0, Math.min(comment.length(), 3999)));
		// MGM en presidencia tenian que extender el checkincomment por que en documentum solo admitía 120 chars
		// voy a ver si en alfresco no hace falta. Quizás sobre este metadato.
		// comento el siguiente update
		// updatedProperties.put(AGECmisProperties.LEOS_CHECKINCOMMENT_EXT.getId(), commentConversion(comment));

		try (ByteArrayInputStream byteStream = new ByteArrayInputStream(updatedDocumentBytes)) {
			OperationContext context = getMinimalContext(cmisSession);
			ObjectId updatedDocId;
			try {
				ContentStream pwcContentStream = pwc.getContentStream();
				ContentStream contentStream = cmisSession.getObjectFactory().createContentStream(
						pwcContentStream.getFileName(), updatedDocumentBytes.length, pwcContentStream.getMimeType(),
						byteStream);

				if (comment != null) {
					comment = comment.substring(0, Math.min(comment.length(), 119));
				}
				if (checkInterface.contentEquals("dfc")) {
					dfcCheckInWorkingCopy(pwc.getId(), nextVersionLabel, isMajor, updatedProperties,
							updatedDocumentBytes, comment);
				} else if (checkInterface.contentEquals("dfs")) {
					dfsCheckInWorkingCopy(pwc.getId(), nextVersionLabel, isMajor, updatedProperties,
							updatedDocumentBytes, comment);
				} else {
					updatedDocId = pwc.checkIn(isMajor, updatedProperties, contentStream, comment);
				}
				logger.trace("Document checked-in successfully...[updated document id:" + pwc.getId() + ']');
			} catch (Exception e) {
				logger.error("Document update failed, trying to cancel the checkout", e);
				if (checkInterface.contentEquals("dfc")) {
					dfcCancelCheckOutWorkingCopy(pwc.getId());
				} else if (checkInterface.contentEquals("dfs")) {
					dfsCancelCheckOutWorkingCopy(pwc.getId());
				} else {
					pwc.cancelCheckOut();
				}
				throw e;
			}

			return findDocumentById(pwc.getId(), true, context);
		} catch (Throwable e) {
			throw new IllegalStateException("unexpected exception", e);
		} finally {
			logger.debug("Repository checkInWorkingCopy version: '{}' in {} milliseconds ({} sec)", nextVersionLabel,
					stopwatch.elapsed(TimeUnit.MILLISECONDS), stopwatch.elapsed(TimeUnit.SECONDS));
		}
	}

	private Document checkOutWorkingCopy(String id) {

		String pwcId = id;

		OperationContext context = getMinimalContext(cmisSession);
		Document document = findDocumentById(id, true, context);

		if (checkInterface.contentEquals("dfc")) {
			dfcCheckOutWorkingCopy(id);
		} else if (checkInterface.contentEquals("dfs")) {
			try {
				dfsCheckOutWorkingCopy(id);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			if (document.isVersionSeriesCheckedOut()) {
				logger.trace("Document already check out ... [id=" + id + ", pwc id="
						+ document.getVersionSeriesCheckedOutId());
			} else {
				pwcId = document.checkOut().getId();
			}
		}
		Document pwc = findDocumentById(pwcId, false, context);
		logger.trace("Document checked-out... [id=" + id + ", pwc id=" + pwc.getId() + ']');
		return pwc;
	}

	private void dfcCheckOutWorkingCopy(String id) {

		IDfSession DfcSession = sessionFactory.openDocbaseSession();
		AGEDfcRepository DfcRepo = new AGEDfcRepository(DfcSession);

		try {
			DfcRepo.checkoutDocument(id);
		} catch (DfException e) {
			logger.trace("Algo fue mal al hacer Checkout con la IF de Documentum: {}", e.getMessage());
			e.printStackTrace();
		}

		sessionFactory.closeDocbaseSession();
		logger.trace("Document checked-out... [id=" + id + ']');

	}

	private void dfsCheckOutWorkingCopy(String id) throws Exception {

		try {
			dfsFactory.checkoutDocument(id);
		} catch (Exception e) {
			throw new Exception("Error checkoutDocument: Document doesn't exist.");
		}

		logger.trace("Document checked-out... [id=" + id + ']');

	}

	private void dfsCancelCheckOutWorkingCopy(String id) throws Exception {

		try {
			dfsFactory.cancelCheckoutDocument(id);
		} catch (Exception e) {
			throw new Exception("Error cancelling checkoutDocument: Document doesn't exist.");
		}

		logger.trace("cancelled Document checked-out... [id=" + id + ']');

	}

	private void dfcCancelCheckOutWorkingCopy(String id) {

		IDfSession DfcSession = sessionFactory.openDocbaseSession();
		AGEDfcRepository DfcRepo = new AGEDfcRepository(DfcSession);

		try {
			DfcRepo.cancelCheckoutDocument(id);
		} catch (DfException e) {
			logger.trace("Algo fue mal al hacer el Cancel Checkout con la IF de Documentum: {}", e.getMessage());
			e.printStackTrace();
		}

		sessionFactory.closeDocbaseSession();
		logger.trace("Document cancelled checked-out... [id=" + id + ']');

	}

	private void dfcCheckInWorkingCopy(String objectId, String nextVersionLabel, Boolean isMajor,
			Map<String, Object> updatedProperties, byte[] updatedDocumentBytes, String comment) {

		IDfSession DfcSession = sessionFactory.openDocbaseSession();
		AGEDfcRepository DfcRepo = new AGEDfcRepository(DfcSession);

		try {
			DfcRepo.checkinDocument(objectId, nextVersionLabel, isMajor, updatedDocumentBytes, updatedProperties,
					comment);
		} catch (DfException e) {
			logger.trace("Algo fue mal al hacer Checkin con la IF de Documentum: {}", e.getMessage());
			e.printStackTrace();
		}

		sessionFactory.closeDocbaseSession();
		logger.trace("Document checked-in... [id=" + objectId + ']');

	}

	private void dfsCheckInWorkingCopy(String objectId, String nextVersionLabel, Boolean isMajor,
			Map<String, Object> updatedProperties, byte[] updatedDocumentBytes, String comment) throws Exception {

		try {
			dfsFactory.altCheckinDocument(objectId, nextVersionLabel, isMajor, updatedProperties, null,
					updatedDocumentBytes, comment);
		} catch (Exception e) {
			throw new Exception("Error checking-in document:", e);
		}

		logger.trace("cancelled document check-in... [id=" + objectId + ']');

	}

	// FIXME replace primaryType string with some enum value
	List<Document> findDocumentsByParentPath(final String path, final String primaryType,
			final Set<LeosCategory> categories, final boolean descendants) {
		logger.trace("Finding documents by parent path... [path=" + path + ", primaryType=" + primaryType
				+ ", categories=" + categories + ", descendants=" + descendants + ']');
		OperationContext context = AGEOperationContextProvider.getOperationContext(cmisSession,
				"cmis:lastModificationDate DESC");
		Folder folder = findFolderByPath(path, context);
		List<Document> documents = getSearchStrategy().AGEfindDocuments(folder, primaryType, categories, descendants,
				false, context);
		logger.trace("Found " + documents.size() + " CMIS document(s).");
		return documents;
	}

	List<Document> findDocumentsByPackageId(final String id, final String primaryType,
			final Set<LeosCategory> categories, final boolean allVersion) {
		logger.trace("Finding documents by package Id... [pkgId=" + id + ", primaryType=" + primaryType
				+ ", categories=" + categories + ", allVersion=" + allVersion + ']');
		OperationContext context = getMinimalContext(cmisSession);
		Folder folder = findFolderById(id, context);
		Boolean isCmisRepoSearchable = cmisSession.getRepositoryInfo().getCapabilities()
				.isAllVersionsSearchableSupported();

		List<Document> documents = getSearchStrategy().AGEfindDocuments(folder, primaryType, categories, false,
				allVersion, context);
		if (allVersion && !isCmisRepoSearchable && !documents.isEmpty()) {
			documents = findAllVersions(documents.get(0).getId());
		}

		logger.trace("Found " + documents.size() + " CMIS document(s).");
		return documents;
	}

	Document findDocumentByParentPath(final String path, final String name) {
		logger.trace("Finding document by parent path... [path=" + path + ", name=" + name + ']');
		OperationContext context = getMinimalContext(cmisSession);
		Folder parentfolder = findFolderByPath(path, context);
        String whereClause = "cmis:name = '" + name +"' AND IN_TREE('" + parentfolder.getId() + "')";
		logger.trace("WHERE " + whereClause);
		
		//Comento que reciba un document por que el path este es de un folder
//		ItemIterable<CmisObject> ObjectsIter = cmisSession.queryObjects("cmis:document", whereClause, false, context);
		
		//MGM 13:00 pongo que reciba un folder
		// Cuando recibe el folder se enfada por java.lang.IllegalArgumentException: Latest version is not a document!
		// at org.apache.chemistry.opencmis.client.runtime.SessionImpl.getLatestDocumentVersion(SessionImpl.java:720)
//		ItemIterable<CmisObject> ObjectsIter = cmisSession.queryObjects("cmis:folder", whereClause, false, context);
		
		//MGM con cmis document no tira petes con repo local, pero falla al hacer la cuenta. 
		ItemIterable<CmisObject> ObjectsIter = cmisSession.queryObjects("cmis:document", whereClause, false, context);
		List<CmisObject> documents = new ArrayList<>();
		ObjectsIter.forEach(t -> documents.add(t));
		logger.info("Found " + documents.size() + " CMIS document(s).");
		require(documents.size() == 1,
				"CMIS object referenced by path [$path] is not a Document or there are more than 1 document with this name!");
		return cmisSession.getLatestDocumentVersion(documents.get(0).getId());
		
		
		// MGM: Comentario. A ver, esta puta mierda esta intentado sacar un unico documento de la carpeta catalog, 
		// no se que mierda espera encontrar ahí, pero ahí hay como 30 documentos. 
		// pero encima, es que pasa el path de una carpeta, entonces claro, si le dices cmis:document se intenta traer un fichero
		// QUE NO HAY, por que es una carpeta y ya si le dices que se traiga un cmis:folder lo que pasa es que se enfada por que no es un documento
		// vamos que no tiene ni puta idea de lo que quire. Hay que buscar que quiere en el workspace pressenter o en el searchstrategy.
		// Se enfada en el return al buscar la ultima version. 
	}

	Document findDocumentById(final String id, final boolean latest) {
		logger.trace("Finding document by id... [id=" + id + ", latest=" + latest + ']');
		OperationContext context = getMinimalContext(cmisSession);
		return findDocumentById(id, latest, context);
	}

	List<Document> findDocumentsByUserId(final String userId, String primaryType, String leosAuthority) {
		logger.trace("Finding document by user id... userId=" + userId);
		return findDocumentsForUser(userId, primaryType, leosAuthority);
	}

	List<Document> findDocumentsByStatus(LeosLegStatus status, String primaryType) {
		OperationContext context = getMinimalContext(cmisSession);
		return getSearchStrategy().findDocumentsByStatus(status, primaryType, context);
	}

	@Deprecated // shouldn't be used. Check time difference between document.getAllVersions() VS
				// queryFindAllVersions()
	List<Document> findAllVersions(final String id) {
		logger.trace("Finding all document versions... [id=" + id + ']');
		OperationContext context = getMinimalContext(cmisSession);
		Document document = findDocumentById(id, false, context);
		final List<Document> versions = document.getAllVersions();
		logger.trace("Found " + versions.size() + " CMIS version(s).");
		return versions;
	}

	public List<Document> findVersionsWithoutVersionLabel(String primaryType, String docRef) {
		OperationContext context = getMinimalContext(cmisSession);
		return getSearchStrategy().findVersionsWithoutVersionLabel(primaryType, docRef, context);
	}

	private Document findDocumentById(String id, boolean latest, OperationContext context) {
		CmisObject cmisObject = latest ? cmisSession.getLatestDocumentVersion(id, context)
				: cmisSession.getObject(id, context);
		require(cmisObject instanceof Document, "CMIS object referenced by id [" + id + "] is not a Document!");
		return (Document) cmisObject;
	}

	private List<Document> findDocumentsForUser(final String userId, String primaryType, String leosAuthority) {
		OperationContext context = AGEOperationContextProvider.getOperationContext(cmisSession,
				"cmis:lastModificationDate DESC");
		final List<Document> documents = getSearchStrategy().findDocumentsForUser(userId, primaryType, leosAuthority,
				context);
		logger.trace("Found " + documents.size() + " docuemnts for " + userId);
		return documents;
	}

	private Folder findFolderByPath(String path, OperationContext context) {
		boolean pathAvailable = cmisSession.existsPath(path);
		require(pathAvailable, "Path [" + path + "] is not available in CMIS repository!");
		CmisObject cmisObject = cmisSession.getObjectByPath(path, context);
		require(cmisObject instanceof Folder, "CMIS object referenced by path [" + path + "] is not a Folder!");
		return (Folder) cmisObject;
	}

	private Folder findFolderById(String id, OperationContext context) {
		boolean idAvailable = cmisSession.exists(id);
		require(idAvailable, "Id [" + id + "] is not available in CMIS repository!");
		CmisObject cmisObject = cmisSession.getObject(id, context);
		require(cmisObject instanceof Folder, "CMIS object referenced by id [" + id + "] is not a Folder!");
		return (Folder) cmisObject;
	}

	private void require(boolean requiredCondition, String message) {
		if (!requiredCondition) {
			throw new IllegalArgumentException(message);
		}
	}

	private static synchronized Long getSyncKey(String key) {
		if (!synchronizedKeys.containsKey(key)) {
			synchronizedKeys.put(key, new Long(System.currentTimeMillis()));
		}
		return synchronizedKeys.get(key);
	}

	private static void cleanSyncKey(String key) {
		synchronizedKeys.remove(key);
	}

	private String getNextVersionLabel(VersionType versionType, String oldVersion) {
		if (StringUtils.isEmpty(oldVersion)) {
			if (versionType.equals(VersionType.MAJOR)) {
				return "1.0.0";
			} else if (versionType.equals(VersionType.INTERMEDIATE)) {
				return "0.1.0";
			} else {
				return "0.0.1";
			}
		}

		String[] newVersion = oldVersion.split("\\.");
		if (versionType.equals(VersionType.MAJOR)) {
			newVersion[0] = Integer.parseInt(newVersion[0]) + 1 + "";
			newVersion[1] = "0";
			newVersion[2] = "0";
		} else if (versionType.equals(VersionType.INTERMEDIATE)) {
			newVersion[1] = Integer.parseInt(newVersion[1]) + 1 + "";
			newVersion[2] = "0";
		} else {
			newVersion[2] = Integer.parseInt(newVersion[2]) + 1 + "";
		}
		return newVersion[0] + "." + newVersion[1] + "." + newVersion[2];
	}

	// Pagination changes
	Stream<Document> findPagedDocumentsByParentPath(String path, String primaryType, Set<LeosCategory> categories,
			boolean descendants, int startIndex, int maxResults, QueryFilter workspaceFilter) {
		logger.trace(
				"Finding documents by parent path... [path=$path, primaryType=$primaryType, categories=$categories, descendants=$descendants]");
		OperationContext context = AGEOperationContextProvider.getOperationContext(cmisSession, maxResults);

		Folder folder = findFolderByPath(path, context);
		Stream<Document> documents = getSearchStrategy().AGEfindDocumentPage(folder, primaryType, categories,
				descendants, false, context, startIndex, workspaceFilter);

		logger.trace("Found ${documents.count()} CMIS document(s).");
		return documents;
	}

	int findDocumentCountByParentPath(String path, String primaryType, Set<LeosCategory> categories,
			boolean descendants, QueryFilter workspaceFilter) {
		logger.trace(
				"Finding documents by parent path... [path=$path, primaryType=$primaryType, categories=$categories, descendants=$descendants]");
		OperationContext context = AGEOperationContextProvider.getMinimalContext(cmisSession);

		Folder folder = findFolderByPath(path, context);
		int documentCount = getSearchStrategy().AGEfindDocumentCount(folder, primaryType, categories, descendants,
					false, context, workspaceFilter);
		logger.trace("Found ${documentCount} CMIS document(s).");
		return documentCount;
	}

	List<Document> findDocumentsByRef(String ref, String primaryType) {
		OperationContext context = getMinimalContext(cmisSession);
		List<Document> documents = getSearchStrategy().findDocumentsByRef(ref, primaryType, context);
		logger.trace("Found " + documents.size() + " documents for " + ref);
		return documents;
	}

	public Stream<Document> findAllMinorsForIntermediate(String primaryType, String docRef, String currIntVersion,
			String prevIntVersion, int startIndex, int maxResults) {
		OperationContext context = AGEOperationContextProvider.getOperationContext(cmisSession, maxResults);
		Stream<Document> documents = getSearchStrategy().findAllMinorsForIntermediate(primaryType, docRef,
				currIntVersion, prevIntVersion, startIndex, context);
		return documents;
	}

	public int findAllMinorsCountForIntermediate(String primaryType, String docRef, String currIntVersion,
			String prevIntVersion) {
		OperationContext context = getMinimalContext(cmisSession);
		return getSearchStrategy().findAllMinorsCountForIntermediate(primaryType, docRef, currIntVersion,
				prevIntVersion, context);
	}

	public Integer findAllMajorsCount(String primaryType, String docRef) {
		OperationContext context = getMinimalContext(cmisSession);
		return getSearchStrategy().findAllMajorsCount(primaryType, docRef, context);
	}

	public Stream<Document> findAllMajors(String primaryType, String docRef, int startIndex, int maxResult) {
		OperationContext context = AGEOperationContextProvider.getOperationContext(cmisSession, maxResult);
		return getSearchStrategy().findAllMajors(primaryType, docRef, startIndex, context);
	}

	public Stream<Document> findRecentMinorVersions(String primaryType, String documentRef, String lastMajorId,
			int startIndex, int maxResults) {
		OperationContext context = AGEOperationContextProvider.getOperationContext(cmisSession, maxResults);
		return getSearchStrategy().findRecentMinorVersions(primaryType, documentRef, lastMajorId, startIndex, context);
	}

	public Integer findRecentMinorVersionsCount(String primaryType, String documentRef, String versionLabel) {
		OperationContext context = getMinimalContext(cmisSession);
		return getSearchStrategy().findRecentMinorVersionsCount(primaryType, documentRef, versionLabel, context);
	}

	public Document findLatestMajorVersionById(String id) {
		OperationContext context = getMinimalContext(cmisSession);
		CmisObject cmisObject = cmisSession.getLatestDocumentVersion(id, true, context);
		require(cmisObject instanceof Document, "CMIS object referenced by id [" + id + "] is not a Document!");
		return (Document) cmisObject;
	}

	// REDMINE 1054 - propiedad lastModifiedBy
	protected Authentication getPrincipal() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	protected String getUser() {
		Principal principal = getPrincipal();
		String name = principal.getName();
		return name;
	}

}
