/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.repository.document;

import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.AGEReport;
import eu.europa.ec.leos.domain.cmis.metadata.AGEReportMetadata;
import eu.europa.ec.leos.model.filter.QueryFilter;
import eu.europa.ec.leos.repository.LeosRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Stream;

/**
 * Report Repository implementation.
 *
 * @constructor Creates a specific Report Repository, injected with a generic LEOS Repository.
 */
@Repository
public class AGEReportRepositoryImpl implements AGEReportRepository {

    private static final Logger logger = LoggerFactory.getLogger(AGEReportRepositoryImpl.class);

    private final LeosRepository leosRepository;

    @Autowired
    public AGEReportRepositoryImpl(LeosRepository leosRepository) {
        this.leosRepository = leosRepository;
    }

    @Override
    public AGEReport createReport(String templateId, String path, String name, AGEReportMetadata metadata) {
        logger.debug("Creating Report... [template=" + templateId + ", path=" + path + ", name=" + name + "]");
        return leosRepository.createDocument(templateId, path, name, metadata, AGEReport.class);
    }

    @Override
    public AGEReport createReportFromContent(String path, String name, AGEReportMetadata metadata, byte[] content) {
        logger.debug("Creating Report From Content... [path=" + path + ", name=" + name + "]");
        return leosRepository.createDocumentFromContent(path, name, metadata, AGEReport.class, LeosCategory.MEMORANDUM.name(), content);
    }

    @Override
    public AGEReport updateReport(String id, AGEReportMetadata metadata) {
        logger.debug("Updating Report metadata... [id=" + id + "]");
        return leosRepository.updateDocument(id, metadata, AGEReport.class);
    }

    @Override
    public AGEReport updateReport(String id, byte[] content, VersionType versionType, String comment) {
        logger.debug("Updating Report content... [id=" + id + "]");
        return leosRepository.updateDocument(id, content, versionType, comment, AGEReport.class);
    }

    @Override
    public AGEReport updateReport(String id, AGEReportMetadata metadata, byte[] content, VersionType versionType, String comment) {
        logger.debug("Updating Report metadata and content... [id=" + id + "]");
        return leosRepository.updateDocument(id, metadata, content, versionType, comment, AGEReport.class);
    }

    @Override
    public AGEReport updateMilestoneComments(String id, List<String> milestoneComments, byte[] content, VersionType versionType, String comment) {
        logger.debug("Updating Report milestoneComments... [id=" + id + "]");
        return leosRepository.updateMilestoneComments(id, content, milestoneComments, versionType, comment, AGEReport.class);
    }

    @Override
    public AGEReport updateMilestoneComments(String id, List<String> milestoneComments) {
        logger.debug("Updating Report milestoneComments... [id=" + id + "]");
        return leosRepository.updateMilestoneComments(id, milestoneComments, AGEReport.class);
    }

    @Override
    public AGEReport findReportById(String id, boolean latest) {
        logger.debug("Finding Report by ID... [id=" + id + ", latest=" + latest + "]");
        return leosRepository.findDocumentById(id, AGEReport.class, latest);
    }

    @Override
    public List<AGEReport> findReportVersions(String id, boolean fetchContent) {
        logger.debug("Finding Report versions... [id=" + id + "]");
        return leosRepository.findDocumentVersionsById(id, AGEReport.class, fetchContent);
    }

    @Override
    public AGEReport findReportByRef(String ref) {
        logger.debug("Finding Report by ref... [ref=" + ref + "]");
        return leosRepository.findDocumentByRef(ref, AGEReport.class);
    }
    
    @Override
    public List<AGEReport> findAllMinorsForIntermediate(String docRef, String currIntVersion, String prevIntVersion, int startIndex, int maxResults) {
        logger.debug("Finding Report versions between intermediates...");
        return leosRepository.findAllMinorsForIntermediate(AGEReport.class, docRef, currIntVersion, prevIntVersion, startIndex, maxResults);
    }
    
    @Override
    public int findAllMinorsCountForIntermediate(String docRef, String currIntVersion, String prevIntVersion) {
    	logger.debug("Finding Report minor versions count between intermediates...");
        return leosRepository.findAllMinorsCountForIntermediate(AGEReport.class, docRef, currIntVersion, prevIntVersion);
    }
    
    @Override
    public Integer findAllMajorsCount(String docRef) {
        return leosRepository.findAllMajorsCount(AGEReport.class, docRef);
    }
    
    @Override
    public List<AGEReport> findAllMajors(String docRef, int startIndex, int maxResult) {
        return leosRepository.findAllMajors(AGEReport.class, docRef, startIndex, maxResult);
    }
    
    @Override
    public List<AGEReport> findRecentMinorVersions(String documentId, String documentRef, int startIndex, int maxResults) {
        final AGEReport report = leosRepository.findLatestMajorVersionById(AGEReport.class, documentId);
        return leosRepository.findRecentMinorVersions(AGEReport.class, documentRef, report.getCmisVersionLabel(), startIndex, maxResults);
    }
    
    @Override
    public Integer findRecentMinorVersionsCount(String documentId, String documentRef) {
        final AGEReport report = leosRepository.findLatestMajorVersionById(AGEReport.class, documentId);
        return leosRepository.findRecentMinorVersionsCount(AGEReport.class, documentRef, report.getCmisVersionLabel());
    }
    
    @Override
    public void deleteReport(String id) {
        logger.debug("Deleting Report... [id=" + id + "]");
        leosRepository.deleteDocumentById(id);
    }
    
}
