package eu.europa.ec.leos.opentext;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.documentum.com.DfClientX;
import com.documentum.com.IDfClientX;
import com.documentum.fc.client.DfClient;
import com.documentum.fc.client.DfServiceException;
import com.documentum.fc.client.IDfSession;
import com.documentum.fc.client.IDfSessionManager;
import com.documentum.fc.common.DfException;
import com.documentum.fc.common.IDfLoginInfo;

@Repository
public class AGEDfcSessionFactoryImpl implements  AGEDfcSessionFactory {

	@Value("${leos.cmis.repository.id}")
    private String docbaseName;
    @Value("${leos.cmis.repository.username:}")
    private String userName;
	@Value("${leos.cmis.repository.password:}")
    private String password;
	
	private IDfSessionManager sMgr = null;
	private IDfSession sess = null;

	private static final Logger LOG = LoggerFactory.getLogger(AGEDfcSessionFactoryImpl.class);
	
	public IDfSession openDocbaseSession() {

	IDfClientX clientx = new DfClientX();
	IDfLoginInfo loginInfoObj = clientx.getLoginInfo();	

	try {
		sMgr = clientx.getLocalClient().newSessionManager();
	} catch (DfException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	sess = null;
	
	loginInfoObj.setUser(userName);
	loginInfoObj.setPassword(password);
	
	try {
		sMgr.setIdentity(docbaseName, loginInfoObj);
	} catch (DfServiceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	//Connect to docbase
	LOG.info("Connecting to docbase {}" , docbaseName);
	try
	{	sess = sMgr.getSession(docbaseName);
	LOG.info("Connection via DFC to Documentum successful");
	}
	catch(DfException ex)
	{
		ex.printStackTrace();
		System.exit(-1);
	}
	return sess;
	}

	public void closeDocbaseSession()
	{
		sMgr.release(sess);
	}

	
}
