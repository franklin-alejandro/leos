package eu.europa.ec.leos.opentext;

import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AGEDfsSoapFaultHandler implements SOAPHandler<SOAPMessageContext> {

	public Set<QName> getHeaders() {
		return null;
	}

	public boolean handleMessage(SOAPMessageContext smc) {
		return true;
	}

	/*
	 * This handler fixes a bug in the SOAPFault response message from Documentum DFS
	 * The messageArgs attribute element returns an invalid type ([Ljava.lang.Object;)
	 * causing a java.lang.ClassNotFoundException during unmarshalling in the
	 * ClientFaultConverter interceptor. The handler simply replaces the value of
	 * the node type with "java.lang.Object".
	 */
	public boolean handleFault(SOAPMessageContext smc) {
		try {
			SOAPFault fault = smc.getMessage().getSOAPBody().getFault();

			XPath xpath = XPathFactory.newInstance().newXPath();
			NodeList nodeSet = (NodeList) xpath.evaluate("//attribute/type", fault.getDetail(), XPathConstants.NODESET);
			for (int i = 0; i < nodeSet.getLength(); i++) {
				Node node = nodeSet.item(i);
				if (node != null && node.getChildNodes() != null && node.getChildNodes().getLength() != 0 && node.getChildNodes().item(0) != null && node.getChildNodes().item(0).getNodeValue().equalsIgnoreCase("[Ljava.lang.Object;"))
					node.getChildNodes().item(0).setNodeValue("java.lang.Object");
			}
		} catch (Exception e) {
			throw new RuntimeException("handleFault: cannot handle SOAPMessage", e);
		}
		return true;
	}

	public void close(MessageContext messageContext) {
	}

}