package eu.europa.ec.leos.repository.document;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.AGECantoDorado;
import eu.europa.ec.leos.domain.cmis.metadata.AGECantoDoradoMetadata;
import eu.europa.ec.leos.repository.LeosRepository;

@Repository
public class AGECantoDoradoRepositoryImpl implements AGECantoDoradoRepository {

    private static final Logger logger = LoggerFactory.getLogger(AGECantoDoradoRepositoryImpl.class);

    private final LeosRepository leosRepository;

    @Autowired
    public AGECantoDoradoRepositoryImpl(LeosRepository leosRepository) {
        this.leosRepository = leosRepository;
    }
	
	@Override
	public AGECantoDorado createCantoDorado(String templateId, String path, String name,
			AGECantoDoradoMetadata metadata) {
        logger.debug("Creating Canto Dorado... [template=" + templateId + ", path=" + path + ", name=" + name + "]");
        return leosRepository.createDocument(templateId, path, name, metadata, AGECantoDorado.class);
	}

	@Override
	public AGECantoDorado createCantoDoradoFromContent(String path, String name, AGECantoDoradoMetadata metadata,
			byte[] content) {
        logger.debug("Creating Canto Dorado From Content... [path=" + path + ", name=" + name + "]");
        return leosRepository.createDocumentFromContent(path, name, metadata, AGECantoDorado.class, LeosCategory.CANTODORADO.name(), content);
	}

	@Override
	public AGECantoDorado updateCantoDorado(String id, AGECantoDoradoMetadata metadata) {
		logger.debug("Updating Canto Dorado metadata... [id=" + id + "]");
        return leosRepository.updateDocument(id, metadata, AGECantoDorado.class);
	}

	@Override
	public AGECantoDorado updateCantoDorado(String id, byte[] content, VersionType versionType, String comment) {
        logger.debug("Updating Canto Dorado content... [id=" + id + "]");
        return leosRepository.updateDocument(id, content, versionType, comment, AGECantoDorado.class);
	}

	@Override
	public AGECantoDorado updateCantoDorado(String id, AGECantoDoradoMetadata metadata, byte[] content,
			VersionType versionType, String comment) {
        logger.debug("Updating Canto Dorado metadata and content... [id=" + id + "]");
        return leosRepository.updateDocument(id, metadata, content, versionType, comment, AGECantoDorado.class);
	}

	@Override
	public AGECantoDorado updateMilestoneComments(String id, List<String> milestoneComments, byte[] content,
			VersionType versionType, String comment) {
        logger.debug("Updating Canto Dorado milestoneComments... [id=" + id + "]");
        return leosRepository.updateMilestoneComments(id, content, milestoneComments, versionType, comment, AGECantoDorado.class);
	}

	@Override
	public AGECantoDorado updateMilestoneComments(String id, List<String> milestoneComments) {
        logger.debug("Updating Canto Dorado milestoneComments... [id=" + id + "]");
        return leosRepository.updateMilestoneComments(id, milestoneComments, AGECantoDorado.class);
	}

	@Override
	public AGECantoDorado findCantoDoradoById(String id, boolean latest) {
        logger.debug("Finding Canto Dorado by ID... [id=" + id + ", latest=" + latest + "]");
        return leosRepository.findDocumentById(id, AGECantoDorado.class, latest);
	}

	@Override
	public List<AGECantoDorado> findCantoDoradoVersions(String id, boolean fetchContent) {
        logger.debug("Finding Canto Dorado versions... [id=" + id + "]");
        return leosRepository.findDocumentVersionsById(id, AGECantoDorado.class, fetchContent);
	}

	@Override
	public AGECantoDorado findCantoDoradoByRef(String ref) {
		logger.debug("Finding Canto Dorado by ref... [ref=" + ref + "]");
        return leosRepository.findDocumentByRef(ref, AGECantoDorado.class);
	}

	@Override
	public List<AGECantoDorado> findAllMinorsForIntermediate(String docRef, String currIntVersion,
			String prevIntVersion, int startIndex, int maxResults) {
        logger.debug("Finding Canto Dorado versions between intermediates...");
        return leosRepository.findAllMinorsForIntermediate(AGECantoDorado.class, docRef, currIntVersion, prevIntVersion, startIndex, maxResults);
	}

	@Override
	public int findAllMinorsCountForIntermediate(String docRef, String currIntVersion, String prevIntVersion) {
    	logger.debug("Finding Canto Dorado minor versions count between intermediates...");
        return leosRepository.findAllMinorsCountForIntermediate(AGECantoDorado.class, docRef, currIntVersion, prevIntVersion);
	}

	@Override
	public Integer findAllMajorsCount(String docRef) {
		 return leosRepository.findAllMajorsCount(AGECantoDorado.class, docRef);
	}
	
    @Override
    public List<AGECantoDorado> findAllMajors(String docRef, int startIndex, int maxResult) {
        return leosRepository.findAllMajors(AGECantoDorado.class, docRef, startIndex, maxResult);
    }

	@Override
	public List<AGECantoDorado> findRecentMinorVersions(String documentId, String documentRef, int startIndex,
			int maxResults) {
        final AGECantoDorado cantoDorado = leosRepository.findLatestMajorVersionById(AGECantoDorado.class, documentId);
        return leosRepository.findRecentMinorVersions(AGECantoDorado.class, documentRef, cantoDorado.getCmisVersionLabel(), startIndex, maxResults);
	}

	@Override
	public Integer findRecentMinorVersionsCount(String documentId, String documentRef) {
        final AGECantoDorado cantoDorado = leosRepository.findLatestMajorVersionById(AGECantoDorado.class, documentId);
        return leosRepository.findRecentMinorVersionsCount(AGECantoDorado.class, documentRef, cantoDorado.getCmisVersionLabel());
	}

	@Override
	public void deleteCantoDorado(String id) {
        logger.debug("Deleting Canto Dorado... [id=" + id + "]");
        leosRepository.deleteDocumentById(id);	
	}

}
