package eu.europa.ec.leos.opentext;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DataTable")
@XmlAccessorType(XmlAccessType.FIELD)
public class AGEDfsContentTypes {

	@XmlElement(name = "dm_format_s", required = true)
	protected List<AGEDfsContentType> contentTypes;

	public List<AGEDfsContentType> getContentTypes() {
		if (contentTypes == null) {
			contentTypes = new ArrayList<AGEDfsContentType>();
		}
		return this.contentTypes;
	}

}
