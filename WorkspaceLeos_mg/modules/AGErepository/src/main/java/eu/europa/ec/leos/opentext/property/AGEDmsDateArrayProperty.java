package eu.europa.ec.leos.opentext.property;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Esta clase representa una propiedad multivaluado de carpeta o documento 
 * de tipo <code>java.util.Date</code>
 * 
 * @author BosArthur
 */
public class AGEDmsDateArrayProperty extends AGEDmsArrayProperty<Date> {
	
	private List<Date> values;

	public AGEDmsDateArrayProperty(String name) {
		super(name);
		setValues(null);
	}

	public AGEDmsDateArrayProperty(String name, Date[] values) {
		super(name);
		setValues(values);
	}

	public AGEDmsDateArrayProperty(String name, AGEDmsSearchCondition searchCondition, Date[] values) {
		super(name);
		setSearchCondition(searchCondition);
		setValues(values);
	}

	public Date[] getValues() {
		return this.values.toArray(new Date[0]);
	}

	public List<Date> getValuesAsList() {
		return this.values;
	}

	public void setValues(Date[] values) {
		if (values == null)
			this.values = new ArrayList((Collection)Arrays.asList(new Date[0]));
		else
			this.values = new ArrayList((Collection)Arrays.asList(values));
	}

	public void clearValue() {
		this.values = new ArrayList((Collection)Arrays.asList(new Date[0]));
	}

	public Object getValueAsObject() {
		return getValues();
	}

	public Iterator iterateValuesAsObjects() {
		return Arrays.asList(getValues()).iterator();
	}

	public Iterator iterateValuesAsStrings() {
		ArrayList stringList = new ArrayList(getValuesAsList().size());

		Date[] arrayOfDate = getValues();
		int i = 0;
		for (int j = arrayOfDate.length; i < j; ) { 
			Date value = arrayOfDate[i];
			stringList.add(String.valueOf(value));
			i++;
		}
		return stringList.iterator();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AGEDmsDateArrayProperty other = (AGEDmsDateArrayProperty) obj;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		return true;
	}

	/**
	public boolean equals(Object o) {
		if (!super.equals(o)) {
			return false;
		}
		DmsDateArrayProperty that = (DmsDateArrayProperty) o;

		return this.values != null ? this.values.equals(that.values) : that.values == null;
	}
	*/
}
