/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.repository.document;

import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.AGEReport;
import eu.europa.ec.leos.domain.cmis.metadata.AGEReportMetadata;
import eu.europa.ec.leos.model.filter.QueryFilter;
import org.springframework.security.access.prepost.PostAuthorize;

import java.util.List;
import java.util.stream.Stream;

/**
 * Report Repository interface.
 * <p>
 * Represents collections of *Report* documents, with specific methods to persist and retrieve.
 * Allows CRUD operations based on strongly typed Business Entities: [Report] and [ReportMetadata].
 */
public interface AGEReportRepository {

    /**
     * Creates a [Report] document from a given template and with the specified characteristics.
     *
     * @param templateId the ID of the template for the report.
     * @param path       the path where to create the report.
     * @param name       the name of the report.
     * @param metadata   the metadata of the report.
     * @return the created report document.
     */
    AGEReport createReport(String templateId, String path, String name, AGEReportMetadata metadata);

    /**
     * Creates a [Report] document from a given content and with the specified characteristics.
     *
     * @param path     the path where to create the report.
     * @param name     the name of the report.
     * @param metadata the metadata of the report.
     * @param content  the content of the report.
     * @return the created report document.
     */
    AGEReport createReportFromContent(String path, String name, AGEReportMetadata metadata, byte[] content);

    /**
     * Updates a [Report] document with the given metadata.
     *
     * @param id       the ID of the report document to update.
     * @param metadata the metadata of the report.
     * @return the updated report document.
     */
    AGEReport updateReport(String id, AGEReportMetadata metadata);

    /**
     * Updates a [Report] document with the given content.
     *
     * @param id      the ID of the report document to update.
     * @param content the content of the report.
     * @param versionType the version type to be created
     * @param comment the comment of the update, optional.
     * @return the updated report document.
     */
    AGEReport updateReport(String id, byte[] content, VersionType versionType, String comment);

    /**
     * Updates a [Report] document with the given metadata and content.
     *
     * @param id       the ID of the report document to update.
     * @param metadata the metadata of the report.
     * @param content  the content of the report.
     * @param versionType  the version type to be created
     * @param comment  the comment of the update, optional.
     * @return the updated report document.
     */
    AGEReport updateReport(String id, AGEReportMetadata metadata, byte[] content, VersionType versionType, String comment);

    AGEReport updateMilestoneComments(String id, List<String> milestoneComments, byte[] content, VersionType versionType, String comment);

    AGEReport updateMilestoneComments(String id, List<String> milestoneComments);

    /**
     * Finds a [Report] document with the specified characteristics.
     *
     * @param id     the ID of the report document to retrieve.
     * @param latest retrieves the latest version of the proposal document, when *true*.
     * @return the found report document.
     */
    AGEReport findReportById(String id, boolean latest);

    /**
     * Finds all versions of a [Report] document with the specified characteristics.
     *
     * @param id           the ID of the Report document to retrieve.
     * @param fetchContent streams the content
     * @return the list of found Report document versions or empty.
     */
    List<AGEReport> findReportVersions(String id, boolean fetchContent);

    /**
     * Finds a [Report] document with the specified characteristics.
     *
     * @param ref the reference metadata of the report document to retrieve.
     * @return the found report document.
     */
    @PostAuthorize("hasPermission(returnObject, 'CAN_READ')")
    AGEReport findReportByRef(String ref);
 
    List<AGEReport> findAllMinorsForIntermediate(String docRef, String currIntVersion, String prevIntVersion, int startIndex, int maxResults);
    
    int findAllMinorsCountForIntermediate(String docRef, String currIntVersion, String prevIntVersion);
    
    Integer findAllMajorsCount(String docRef);
    
    List<AGEReport> findAllMajors(String docRef, int startIndex, int maxResult);
    
    List<AGEReport> findRecentMinorVersions(String documentId, String documentRef, int startIndex, int maxResults);
    
    Integer findRecentMinorVersionsCount(String documentId, String documentRef);

	void deleteReport(String id);
}
