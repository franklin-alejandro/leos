package eu.europa.ec.leos.opentext.property;

import java.util.Date;

/**
 * Esta clase representa una propiedad de carpeta o documento de tipo 
 * <code>java.util.Date</code>
 * 
 * @author BosArthur
 */
public class AGEDmsDateProperty extends AGEDmsProperty {
	
	private Date value;

	public AGEDmsDateProperty(String name) {
		super(name);
		this.value = null;
	}

	public AGEDmsDateProperty(String name, Date value) {
		super(name);
		this.value = value;
	}

	public AGEDmsDateProperty(String name, AGEDmsSearchCondition searchCondition, Date value) {
		super(name);
		setSearchCondition(searchCondition);
		this.value = value;
	}

	public Date getValue() {
		return this.value;
	}

	public void setValue(Date value) {
		this.value = value;
	}

	public void clearValue() {
		this.value = null;
	}

	public Object getValueAsObject() {
		return this.value;
	}

	public String getValueAsString() {
		if (value == null)
			return "";
		else
			return String.valueOf(this.value);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AGEDmsDateProperty other = (AGEDmsDateProperty) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	
	/**
	public boolean equals(Object o) {
		if (!super.equals(o)) {
			return false;
		}
		DmsDateProperty that = (DmsDateProperty) o;

		return this.value != null ? this.value.equals(that.value) : that.value == null;
	}*/
}
