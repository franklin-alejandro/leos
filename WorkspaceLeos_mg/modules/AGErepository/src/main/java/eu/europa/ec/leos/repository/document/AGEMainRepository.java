/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.repository.document;

import eu.europa.ec.leos.domain.cmis.common.VersionType;
import eu.europa.ec.leos.domain.cmis.document.AGEMain;
import eu.europa.ec.leos.domain.cmis.metadata.AGEMainMetadata;
import eu.europa.ec.leos.model.filter.QueryFilter;
import org.springframework.security.access.prepost.PostAuthorize;

import java.util.List;
import java.util.stream.Stream;

/**
 * Main Repository interface.
 * <p>
 * Represents collections of *Main* documents, with specific methods to persist and retrieve.
 * Allows CRUD operations based on strongly typed Business Entities: [Main] and [MainMetadata].
 */
public interface AGEMainRepository {

    /**
     * Creates a [Main] document from a given template and with the specified characteristics.
     *
     * @param templateId the ID of the template for the report.
     * @param path       the path where to create the report.
     * @param name       the name of the report.
     * @param metadata   the metadata of the report.
     * @return the created report document.
     */
    AGEMain createMain(String templateId, String path, String name, AGEMainMetadata metadata);

    /**
     * Creates a [Main] document from a given content and with the specified characteristics.
     *
     * @param path     the path where to create the report.
     * @param name     the name of the report.
     * @param metadata the metadata of the report.
     * @param content  the content of the report.
     * @return the created report document.
     */
    AGEMain createMainFromContent(String path, String name, AGEMainMetadata metadata, byte[] content);

    /**
     * Updates a [Main] document with the given metadata.
     *
     * @param id       the ID of the report document to update.
     * @param metadata the metadata of the report.
     * @return the updated report document.
     */
    AGEMain updateMain(String id, AGEMainMetadata metadata);

    /**
     * Updates a [Main] document with the given content.
     *
     * @param id      the ID of the report document to update.
     * @param content the content of the report.
     * @param versionType the version type to be created
     * @param comment the comment of the update, optional.
     * @return the updated report document.
     */
    AGEMain updateMain(String id, byte[] content, VersionType versionType, String comment);

    /**
     * Updates a [Main] document with the given metadata and content.
     *
     * @param id       the ID of the report document to update.
     * @param metadata the metadata of the report.
     * @param content  the content of the report.
     * @param versionType  the version type to be created
     * @param comment  the comment of the update, optional.
     * @return the updated report document.
     */
    AGEMain updateMain(String id, AGEMainMetadata metadata, byte[] content, VersionType versionType, String comment);

    AGEMain updateMilestoneComments(String id, List<String> milestoneComments, byte[] content, VersionType versionType, String comment);

    AGEMain updateMilestoneComments(String id, List<String> milestoneComments);

    /**
     * Finds a [Main] document with the specified characteristics.
     *
     * @param id     the ID of the report document to retrieve.
     * @param latest retrieves the latest version of the proposal document, when *true*.
     * @return the found report document.
     */
    AGEMain findMainById(String id, boolean latest);

    /**
     * Finds all versions of a [Main] document with the specified characteristics.
     *
     * @param id           the ID of the Main document to retrieve.
     * @param fetchContent streams the content
     * @return the list of found Main document versions or empty.
     */
    List<AGEMain> findMainVersions(String id, boolean fetchContent);

    /**
     * Finds a [Main] document with the specified characteristics.
     *
     * @param ref the reference metadata of the report document to retrieve.
     * @return the found report document.
     */
    @PostAuthorize("hasPermission(returnObject, 'CAN_READ')")
    AGEMain findMainByRef(String ref);
 
    List<AGEMain> findAllMinorsForIntermediate(String docRef, String currIntVersion, String prevIntVersion, int startIndex, int maxResults);
    
    int findAllMinorsCountForIntermediate(String docRef, String currIntVersion, String prevIntVersion);
    
    Integer findAllMajorsCount(String docRef);
    
    List<AGEMain> findAllMajors(String docRef, int startIndex, int maxResult);
    
    List<AGEMain> findRecentMinorVersions(String documentId, String documentRef, int startIndex, int maxResults);
    
    Integer findRecentMinorVersionsCount(String documentId, String documentRef);

	void deleteMain(String id);
}
