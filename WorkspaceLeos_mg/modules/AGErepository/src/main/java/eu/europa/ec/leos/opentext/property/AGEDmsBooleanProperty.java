package eu.europa.ec.leos.opentext.property;

public class AGEDmsBooleanProperty extends AGEDmsProperty {
	
	private boolean value;

	public AGEDmsBooleanProperty(String name) {
		super(name);
	}

	public AGEDmsBooleanProperty(String name, boolean value) {
		super(name);
		this.value = value;
	}

	public AGEDmsBooleanProperty(String name, AGEDmsSearchCondition searchCondition, boolean value) {
		super(name);
		setSearchCondition(searchCondition);
		this.value = value;
	}

	public boolean getValue() {
		return this.value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}

	public void clearValue() {
		this.value = false;
	}

	public Object getValueAsObject() {
		return Boolean.valueOf(this.value);
	}

	public String getValueAsString() {
		return String.valueOf(this.value);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (value ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AGEDmsBooleanProperty other = (AGEDmsBooleanProperty) obj;
		if (value != other.value)
			return false;
		return true;
	}

	/**
	public boolean equals(Object o) {
		if (!super.equals(o)) {
			return false;
		}
		DmsBooleanProperty that = (DmsBooleanProperty) o;

		return this.value == that.value;
	}*/
}