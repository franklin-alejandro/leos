package eu.europa.ec.leos.opentext.property.artifacts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Artifact complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Artifact">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Supertype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Attributes" type="{http://itf.dms.adoc.age.es/artifacts}Attribute" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Artifact", propOrder = {
    "typeName",
    "supertype",
    "attributes"
})
public class AGEArtifact {

    @XmlElement(name = "TypeName", required = true)
    protected String typeName;
    @XmlElement(name = "Supertype", required = true)
    protected String supertype;
    @XmlElement(name = "Attributes")
    protected List<AGEAttribute> attributes;

    /**
     * Gets the value of the typeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * Sets the value of the typeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeName(String value) {
        this.typeName = value;
    }

    /**
     * Gets the value of the supertype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupertype() {
        return supertype;
    }

    /**
     * Sets the value of the supertype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupertype(String value) {
        this.supertype = value;
    }

    /**
     * Gets the value of the attributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Attribute }
     * 
     * 
     */
    public List<AGEAttribute> getAttributes() {
        if (attributes == null) {
            attributes = new ArrayList<AGEAttribute>();
        }
        return this.attributes;
    }

}
