package eu.europa.ec.leos.opentext;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.europa.ec.leos.opentext.property.AGEExtConfDir;

@Component
public class AGEDfsContentTypeMapper {

	private final static Logger logger = LoggerFactory.getLogger(AGEDfsContentTypeMapper.class);   

	private HashMap<String, String> dosExtensions = new HashMap<String, String>();
	private HashMap<String, String> documentumContentTypes = new HashMap<String, String>();
	
	/*
	@PostConstruct
	private void loadFormatFile() throws Exception {
		try {
			logger.info("Initializing DFS content type mapper...");
			String filePath = AGEExtConfDir.getFilePath("/dfs/dm_format_s.xml");
			logger.info("Using file " + filePath + " for loading content types.");
			if (!new File(filePath).canRead()) {
				logger.warn("File dm_format_s.xml not found or not readable, no content types loaded.");
				return;
			}

			JAXBContext ctx = JAXBContext.newInstance(new Class[] {AGEDfsContentTypes.class});
	        Unmarshaller um = ctx.createUnmarshaller();
	        
	        AGEDfsContentTypes dfsContentTypes = (AGEDfsContentTypes) um.unmarshal(new File(filePath));
	        logger.info("Loaded [" + dfsContentTypes.getContentTypes().size() + "] content types");
	        
	        Iterator<AGEDfsContentType> it = dfsContentTypes.getContentTypes().iterator();
	        while (it.hasNext()) {
	        	AGEDfsContentType contentType = it.next();
	        	if (contentType.getDosExtension() != null && !contentType.getDosExtension().trim().isEmpty())
	        		dosExtensions.put(contentType.getDosExtension().trim(), contentType.getName().trim());
	        	if (contentType.getName() != null && !contentType.getName().trim().isEmpty())
	        		documentumContentTypes.put(contentType.getName().trim(), contentType.getDosExtension().trim());
	        }
	        
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	*/
	
	public String getDocumentumContentTypeByDosExtension(String dosExtension) {
		String contentType = dosExtensions.get(dosExtension);
		if (contentType == null || contentType.isEmpty())
			return null;
		else
			return contentType;
	}
	
	public String getDosExtensionByDocumentumContentType(String documentumContentType) {
		String dosExtension = documentumContentTypes.get(documentumContentType);
		if (dosExtension == null || dosExtension.isEmpty())
			return null;
		else
			return dosExtension;
	}
}
