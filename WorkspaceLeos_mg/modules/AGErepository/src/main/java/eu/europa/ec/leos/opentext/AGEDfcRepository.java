package eu.europa.ec.leos.opentext;

import com.documentum.fc.client.IDfSysObject;
import com.documentum.fc.client.IDfVersionLabels;
import com.documentum.fc.client.IDfSession;
import com.documentum.fc.common.DfException;
import com.documentum.fc.common.DfId;
import com.documentum.fc.common.IDfId;

import java.util.LinkedHashMap;
import java.util.Map;

public class AGEDfcRepository {

	private final IDfSession sess;
	
	public AGEDfcRepository(IDfSession sess) {
		this.sess = sess;
	}
	
	public String readAttributeValue(String r_object_id, String att_name) throws DfException 
	{
		IDfSysObject obj = (IDfSysObject) sess.getObject(new DfId(r_object_id));
		String aux = obj.getString(att_name);
		
		return aux;
		
	}

	public String readVersion(String r_object_id) throws DfException 
	{
		IDfSysObject obj = (IDfSysObject) sess.getObject(new DfId(r_object_id));
		IDfVersionLabels versionLab = obj.getVersionLabels();
		String vers = versionLab.getImplicitVersionLabel();	
		return vers;
		
	}

	public String readDocumentContent(String r_object_id) throws DfException 
	{
		IDfSysObject obj = (IDfSysObject) sess.getObject(new DfId(r_object_id));
		String filePath = obj.getFile(null);	
		return filePath;
		
	}

	public void checkoutDocument (String r_object_id) throws DfException 
	{
		IDfSysObject obj = (IDfSysObject) sess.getObject(new DfId(r_object_id));
		if (!obj.isCheckedOut()) {
			obj.checkout();				
		}
		else {
			try {
				//Esperamos 10s a ver si nos lo desbloquean
				Thread.sleep(10*1000);
				if (!obj.isCheckedOut()) {
					obj.checkout();				
				}
				else {
					try {
						//Segundo intento, esperamos 120s a ver si nos lo desbloquean
						Thread.sleep(120*1000);
						if (!obj.isCheckedOut()) {
							obj.checkout();				
						}
						else {
							//Intentamos desbloquearlo y reintentamos
							obj.cancelCheckout();
							obj.checkout();
						}
				    } catch (Exception e) {
				    	System.out.println(e);
				    }
				}
		    } catch (Exception e) {
		    	System.out.println(e);
		    }
		}
	}

	public void cancelCheckoutDocument (String r_object_id) throws DfException 
	{
		IDfSysObject obj = (IDfSysObject) sess.getObject(new DfId(r_object_id));
		obj.cancelCheckout();	
			
	}	
	
	public IDfId checkinDocument (String r_object_id, String version, Boolean isMajor, byte[] newContent, Map <String, ?> attributes, String comment) throws DfException 
	{
		
		attributes = altDocumentumPropertyMapper(attributes, comment);
		
		IDfSysObject obj = (IDfSysObject) sess.getObject(new DfId(r_object_id));
		IDfId newSysObjId = null;
		String newContentString = newContent.toString();
		
		if (obj.isCheckedOut()) {
			obj.setFile(newContentString);
	 		obj.setContentType("unknown");
		 	for(String attrName : attributes.keySet())
		 	{
		 		obj.setString(attrName, attributes.get(attrName).toString());
		 	}
		    newSysObjId = obj.checkin(isMajor, version);
		}
		return newSysObjId;
	}
	
	public Map<String, Object> altDocumentumPropertyMapper(Map<String, ?> olpProperties, String comment) {
    	Map<String, Object> newProperties = new LinkedHashMap<>();
    	newProperties.putAll(olpProperties);
    	Map<String,String> propertyMap = new LinkedHashMap<>();
    	
    	if (newProperties.get("cmis:checkinComment") != null) {
    		newProperties.put("cmis:checkinComment",comment);
    	}
    	
    	propertyMap.put("cmis:name","object_name");
    	propertyMap.put("cmis:objectId","r_object_id");
    	propertyMap.put("cmis:baseTypeId","cmis:document");
    	propertyMap.put("cmis:objectTypeId","r_object_type");
    	propertyMap.put("cmis:createdBy","r_creator_name");
    	propertyMap.put("cmis:creationDate","r_creation_date");
    	propertyMap.put("cmis:lastModifiedBy","r_modifier");
    	propertyMap.put("cmis:lastModificationDate","r_modification_date");
    	propertyMap.put("cmis:changeToken","i_vstamp");
    	propertyMap.put("cmis:isImmutable","r_immutable_flag");
    	propertyMap.put("cmis:isLatestVersion","i_has_folder");
    	propertyMap.put("cmis:versionLabel"," r_version_label");
    	propertyMap.put("cmis:versionSeriesId","i_chronicle_id");
    	propertyMap.put("cmis:versionSeriesCheckedOutBy","r_lock_owner");
    	propertyMap.put("cmis:checkinComment","log_entry");
    	propertyMap.put("cmis:contentStreamLength","r_full_content_size");
    	propertyMap.put("cmis:contentStreamMimeType","a_content_type");
    	propertyMap.put("cmis:contentStreamFileName","object_name Read-only");
    	propertyMap.put("cmis:contentStreamId","i_contents_id");

    	for (Map.Entry<String, String> prop : propertyMap.entrySet()) {
    		if (newProperties.get(prop.getValue()) != null) {
   				newProperties.put(prop.getKey(), newProperties.get(prop.getValue()));
   				newProperties.remove(prop.getValue());
    		}    		
    	}
    	
    	return newProperties;
    }
}
