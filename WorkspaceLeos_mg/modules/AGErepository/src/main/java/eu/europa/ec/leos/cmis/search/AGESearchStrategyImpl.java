/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.cmis.search;

import eu.europa.ec.leos.cmis.mapping.AGECmisProperties;
import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.cmis.LeosCategory;
import eu.europa.ec.leos.domain.cmis.LeosLegStatus;
import eu.europa.ec.leos.model.filter.QueryFilter;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.OperationContext;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

abstract public class AGESearchStrategyImpl implements AGESearchStrategy {
    
    protected final Session cmisSession;
    
    private static final Logger logger = LoggerFactory.getLogger(SearchStrategyNavigationServices.class);
    
    public AGESearchStrategyImpl(Session cmisSession) {
        this.cmisSession = cmisSession;
    }
    
    @Override
    public List<Document> findDocumentsForUser(String userId, String primaryType, String leosAuthority, OperationContext context) {
        logger.trace("Finding documents...");
        String whereClause = AGECmisProperties.DOCUMENT_CATEGORY.getId() + " IN ('PROPOSAL') AND ANY " + AGECmisProperties.COLLABORATORS.getId() + " IN ('" + userId +
                "::" + leosAuthority + "')";
        logger.trace("Ordering by ....." + context.getOrderBy());
        ItemIterable<CmisObject> cmisObjects = cmisSession.queryObjects(primaryType, whereClause, false, context);
        return StreamSupport.stream(cmisObjects.spliterator(), false)
                .map(cmisObject -> (Document) cmisObject)
                .collect(Collectors.toList());
    }
    
    @Override
    public List<Document> findDocumentsByStatus(LeosLegStatus status, String primaryType, OperationContext context) {
        String whereClause = AGECmisProperties.STATUS.getId() + " IN ('" + status + "')";
        ItemIterable<CmisObject> cmisObjects = cmisSession.queryObjects(primaryType, whereClause, false, context);
        return StreamSupport.stream(cmisObjects.spliterator(), false)
                .map(cmisObject -> (Document) cmisObject)
                .collect(Collectors.toList());
    }
    
    @Override
    public Stream<Document> findDocumentPage(Folder folder, String primaryType, Set<LeosCategory> categories,
                                             boolean descendants, boolean allVersion, OperationContext context, int startIndex, QueryFilter workspaceFilter) {
        ItemIterable<CmisObject> cmisObjects = findDocumentIterable(folder, primaryType, categories, descendants, allVersion, context, workspaceFilter);
        cmisObjects = cmisObjects.skipTo(startIndex).getPage(context.getMaxItemsPerPage());
        return StreamSupport.stream(cmisObjects.spliterator(), false)
                .map(cmisObject -> (Document) cmisObject);
    }

    public Stream<Document> AGEfindDocumentPage(Folder folder, String primaryType, Set<LeosCategory> categories,
            	boolean descendants, boolean allVersion, OperationContext context, int startIndex, QueryFilter workspaceFilter) {
    		ItemIterable<CmisObject> cmisObjects = AGEfindDocumentIterable(folder, primaryType, categories, descendants, allVersion, context, workspaceFilter);
    			cmisObjects = cmisObjects.skipTo(startIndex).getPage(context.getMaxItemsPerPage());
    				return StreamSupport.stream(cmisObjects.spliterator(), false)
    						.map(cmisObject -> (Document) cmisObject);
    }   
    
    private String getCountStatement(String primaryType, String whereClause) {
        final StringBuilder statement = new StringBuilder(1024);
        statement.append("SELECT cmis:objectId FROM ");
        statement.append(cmisSession.getTypeDefinition(primaryType).getQueryName());
        // MGM aqui está buscando en la sesion los types definidos, buscando leos_xml, y no lo encuentra: Problema en alfresco.
        // El problema es que en la petición http://85.234.135.12:9090/alfresco/api/-default-/public/cmis/versions/1.0/atom/type?id=ls%3Aleos_xml
        // no se devuelven los tipos del proyecto leos, se devuelven los tipos de alfesco:
        // http://85.234.135.12:9090/alfresco/api/-default-/public/cmis/versions/1.0/atom/types? 
        // Como puede ser folder, documment, item o cualquier otra vaina loca. 
        if (whereClause.trim().length() > 0) {
            statement.append(" WHERE ");
            statement.append(whereClause);
        }
        return statement.toString();
    }

    private int findCount(String primaryType,  boolean allVersion, OperationContext context, String whereClause) {
        if (primaryType != null && primaryType.trim().length() != 0) {
            context.setMaxItemsPerPage(1);
            String statement = getCountStatement(primaryType, whereClause);
            ItemIterable<QueryResult> resultSet = cmisSession.query(statement, allVersion, context);
            // AGE customization. getTotalNumItems do not work properly with Documentum
            // return (int) resultSet.getTotalNumItems();
//            Integer TotalItems = 0;
            //Integer TotalItems = 1;
            //con uno peta por algo interno de cmis:
            // org.apache.chemistry.opencmis.commons.exceptions.CmisRuntimeException: at org.apache.chemistry.opencmis.client.bindings.spi.atompub.AbstractAtomPubService.convertStatusCode(AbstractAtomPubService.java:523)
            // MGM comento a ver si evito el pete:
            // MGM hago una condición para evitar un null que da por ahí.
//            if (resultSet != null && resultSet.iterator() != null && resultSet.getTotalNumItems() != 0) {
//            	for (QueryResult item : resultSet) {
//	            	TotalItems++;
//	            }
//            }
    		logger.info("Found " + resultSet.getTotalNumItems() + " CMIS document(s).");
    		return (int) resultSet.getTotalNumItems();
        } else {
            throw new IllegalArgumentException("Type ID must be set!");
        }
    }

    @Override
    public int findDocumentCount(Folder folder, String primaryType, Set<LeosCategory> categories,
                                 boolean descendants, boolean allVersion, OperationContext context, QueryFilter workspaceFilter) {
            String whereClause = AGEQueryUtil.getQuery(folder, categories, descendants, workspaceFilter);
            return findCount(primaryType, allVersion, context, whereClause);
    }

    public int AGEfindDocumentCount(Folder folder, String primaryType, Set<LeosCategory> categories,
    							boolean descendants, boolean allVersion, OperationContext context, QueryFilter workspaceFilter) {
    		String whereClause = AGEQueryUtil.AGEgetQuery(folder, categories, descendants, workspaceFilter);
    			return findCount(primaryType, allVersion, context, whereClause);
    }    
    
    @Override
    public List<Document> findDocumentsByRef(String ref, String primaryType, OperationContext context) {
        logger.trace("Finding documents by metadataRef...");
        String whereClause = AGECmisProperties.METADATA_REF.getId() + " = '" + ref + "'";
        ItemIterable<CmisObject> cmisObjects = cmisSession.queryObjects(primaryType, whereClause, false, context);
        return StreamSupport.stream(cmisObjects.spliterator(), false)
                .map(cmisObject -> (Document) cmisObject)
                .collect(Collectors.toList());
    }
    
    @Override
    public Integer findAllMajorsCount(String primaryType, String docRef, OperationContext context) {
        // AGE Customization in Documentum, since cmis:isMayorVersion in non queryable as is computed   	
    	String whereClause = AGEQueryUtil.getOrderCreationDateQueryString(docRef);
    	ItemIterable<CmisObject> cmisObjects = cmisSession.queryObjects(primaryType, whereClause, true, context);
        // AGE customization. getTotalNumItems do not work properly with Documentum
        Integer TotalItems = 0;
        for (CmisObject item : cmisObjects) {
        	if (((String) item.getPropertyValue("cmis:versionLabel")).contains(".0")) {
            	TotalItems++;        		
        	}
        }
		logger.info("Found " + TotalItems + " Mayor CMIS document(s).");
		return TotalItems;
    }
    
    @Override
    public Stream<Document> findAllMajors(String primaryType, String docRef, int startIndex, OperationContext context) {
        // Customization in Documentum, since cmis:isMayorVersion in non queryable as is computed   	
    	String whereClause = AGEQueryUtil.getOrderCreationDateQueryString(docRef);
        ItemIterable<CmisObject> cmisObjects = cmisSession.queryObjects(primaryType, whereClause, true, context);
        cmisObjects = cmisObjects.skipTo(startIndex).getPage(context.getMaxItemsPerPage());
        return StreamSupport.stream(cmisObjects.spliterator(), false)
                .map(cmisObject -> (Document) cmisObject)
                .filter(p -> ((String) p.getPropertyValue("cmis:versionLabel")).contains(".0"));
    }

    @Override
    public List<Document> findVersionsWithoutVersionLabel(String primaryType, String docRef, OperationContext context) {
        String whereClause = AGEQueryUtil.getVersionsWithoutVersionLabelQueryString(docRef);
        ItemIterable<CmisObject> cmisObjects = cmisSession.queryObjects(primaryType, whereClause, true, context);
        return StreamSupport.stream(cmisObjects.spliterator(), false)
                .map(cmisObject -> (Document) cmisObject)
                .collect(Collectors.toList());
    }

    
    @Override
    public Integer findRecentMinorVersionsCount(String primaryType, String documentRef, String lastMajorId, OperationContext context) {
        QueryFilter filter = AGEQueryUtil.getRecentVersionsQuery(documentRef, lastMajorId);
        ItemIterable<CmisObject> cmisObjects = getObjectIterableByFilter(primaryType, filter, true, context);
        // AGE customization. getTotalNumItems do not work properly with Documentum
        // return (int) resultSet.getTotalNumItems();
        Integer TotalItems = 0;
        for (CmisObject item : cmisObjects) {
        	TotalItems++;
        }
		logger.info("Found " + TotalItems + " Recent Minor CMIS document(s).");
		return TotalItems;
    }
    
    @Override
    public Stream<Document> findRecentMinorVersions(String primaryType, String documentRef, String lastMajorId, int startIndex, OperationContext context) {
        QueryFilter filter = AGEQueryUtil.getRecentVersionsQuery(documentRef, lastMajorId);
        ItemIterable<CmisObject> cmisObjects = getObjectIterableByFilter(primaryType, filter, true, context);
        cmisObjects = cmisObjects.skipTo(startIndex).getPage(context.getMaxItemsPerPage());
        return StreamSupport.stream(cmisObjects.spliterator(), false)
                .map(cmisObject -> (Document) cmisObject);
    }
    
    @Override
    public Stream<Document> findAllMinorsForIntermediate(String primaryType, String docRef, String currIntVersion, String prevIntVersion, int startIndex, OperationContext context) {
        QueryFilter filter = AGEQueryUtil.getMinorVersionsQueryFilter(docRef, currIntVersion, prevIntVersion);
        ItemIterable<CmisObject> cmisObjects = getObjectIterableByFilter(primaryType, filter, true, context);
        cmisObjects = cmisObjects.skipTo(startIndex).getPage(context.getMaxItemsPerPage());
        return StreamSupport.stream(cmisObjects.spliterator(), false)
                .map(cmisObject -> (Document) cmisObject);
    }
    
    @Override
    public int findAllMinorsCountForIntermediate(String primaryType, String docRef, String currIntVersion, String prevIntVersion, OperationContext context) {
        QueryFilter filter = AGEQueryUtil.getMinorVersionsQueryFilter(docRef, currIntVersion, prevIntVersion);
        ItemIterable<CmisObject> cmisObjects = getObjectIterableByFilter(primaryType, filter, true, context);
        // AGE customization. getTotalNumItems do not work properly with Documentum
        // return (int) resultSet.getTotalNumItems();
        Integer TotalItems = 0;
        for (CmisObject item : cmisObjects) {
            	TotalItems++;        		
        	}
		logger.info("Found " + TotalItems + " Minor CMIS document(s).");
		return TotalItems;
    }
    
    private ItemIterable<CmisObject> getObjectIterableByFilter(String primaryType, QueryFilter workspaceFilter, boolean allVersion, OperationContext context) {
        String whereClause = AGEQueryUtil.formFilterClause(workspaceFilter);
        context.setOrderBy(AGEQueryUtil.formSortClause(workspaceFilter));
        logger.debug("Querying CMIS objects... [primaryType={}, where={}]", primaryType, whereClause);
        return cmisSession.queryObjects(primaryType, whereClause, allVersion, context);
    }
    
    private ItemIterable<CmisObject> findDocumentIterable(Folder folder, String primaryType, Set<LeosCategory> categories,
                                                            boolean descendants, boolean allVersion, OperationContext context, QueryFilter workspaceFilter) {
        String whereClause = AGEQueryUtil.getQuery(folder, categories, descendants, workspaceFilter);
        context.setOrderBy(AGEQueryUtil.formSortClause(workspaceFilter));
        logger.debug("Querying CMIS objects... [primaryType={}, where={}]", primaryType, whereClause);
        return cmisSession.queryObjects(primaryType, whereClause, allVersion, context);
        
    }
    
    protected ItemIterable<CmisObject> AGEfindDocumentIterable(Folder folder, String primaryType, Set<LeosCategory> categories,
            boolean descendants, boolean allVersion, OperationContext context, QueryFilter workspaceFilter) {
    	String whereClause = AGEQueryUtil.AGEgetQuery(folder, categories, descendants, workspaceFilter);
    	context.setOrderBy(AGEQueryUtil.formSortClause(workspaceFilter));
    	logger.debug("Querying CMIS objects... [primaryType={}, where={}]", primaryType, whereClause);
    	return cmisSession.queryObjects(primaryType, whereClause, allVersion, context);

}
}
