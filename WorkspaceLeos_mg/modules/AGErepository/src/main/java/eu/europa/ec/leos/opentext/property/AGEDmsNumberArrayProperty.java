package eu.europa.ec.leos.opentext.property;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Esta clase representa una propiedad multivaluado de carpeta o documento 
 * de tipo <code>java.lang.Number</code>
 * 
 * @author BosArthur
 */
public class AGEDmsNumberArrayProperty extends AGEDmsArrayProperty<Number> {
	
	private List<Number> values;

	public AGEDmsNumberArrayProperty(String name) {
		super(name);
		setValues(null);
	}

	public AGEDmsNumberArrayProperty(String name, Number[] values) {
		super(name);
		setValues(values);
	}

	public AGEDmsNumberArrayProperty(String name, AGEDmsSearchCondition searchCondition, Number[] values) {
		super(name);
		setSearchCondition(searchCondition);
		setValues(values);
	}

	public Number[] getValues() {
		return this.values.toArray(new Number[0]);
	}

	public List<Number> getValuesAsList() {
		return this.values;
	}

	public void setValues(Number[] values) {
		if (values == null)
			this.values = new ArrayList((Collection)Arrays.asList(new Number[0]));
		else
			this.values = new ArrayList((Collection)Arrays.asList(values));
	}

	public void clearValue() {
		this.values = new ArrayList((Collection)Arrays.asList(new Number[0]));
	}

	public static AGEDmsNumberArrayProperty makeProperty(String name, short[] values) {
		Number[] numbers = new Number[values.length];

		for (int i = 0; i < values.length; i++) {
			numbers[i] = Short.valueOf(values[i]);
		}
		return new AGEDmsNumberArrayProperty(name, numbers);
	}

	public static AGEDmsNumberArrayProperty makeProperty(String name, int[] values) {
		Number[] numbers = new Number[values.length];

		for (int i = 0; i < values.length; i++) {
			numbers[i] = Integer.valueOf(values[i]);
		}
		return new AGEDmsNumberArrayProperty(name, numbers);
	}

	public static AGEDmsNumberArrayProperty makeProperty(String name, long[] values) {
		Number[] numbers = new Number[values.length];

		for (int i = 0; i < values.length; i++) {
			numbers[i] = Long.valueOf(values[i]);
		}
		return new AGEDmsNumberArrayProperty(name, numbers);
	}

	public static AGEDmsNumberArrayProperty makeProperty(String name, double[] values) {
		Number[] numbers = new Number[values.length];

		for (int i = 0; i < values.length; i++) {
			numbers[i] = Double.valueOf(values[i]);
		}
		return new AGEDmsNumberArrayProperty(name, numbers);
	}

	public Object getValueAsObject() {
		return getValues();
	}

	public Iterator iterateValuesAsObjects() {
		return getValuesAsList().iterator();
	}

	public Iterator iterateValuesAsStrings() {
		ArrayList stringList = new ArrayList(getValuesAsList().size());

		Number[] arrayOfNumber = getValues();
		int i = 0;
		for (int j = arrayOfNumber.length; i < j; ) {
			Number value = arrayOfNumber[i];
			stringList.add(String.valueOf(value));
			i++;
		}
		return stringList.iterator();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AGEDmsNumberArrayProperty other = (AGEDmsNumberArrayProperty) obj;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		return true;
	}

	/**
	public boolean equals(Object o) {
		if (!super.equals(o)) {
			return false;
		}
		DmsNumberArrayProperty that = (DmsNumberArrayProperty) o;

		return this.values != null ? this.values.equals(that.values) : that.values == null;
	}*/
}
