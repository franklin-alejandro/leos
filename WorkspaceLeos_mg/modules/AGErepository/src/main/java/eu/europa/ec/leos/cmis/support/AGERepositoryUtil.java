package eu.europa.ec.leos.cmis.support;

import eu.europa.ec.leos.cmis.extensions.AGELeosMetadataExtensions;
import eu.europa.ec.leos.cmis.extensions.LeosMetadataExtensions;
import eu.europa.ec.leos.cmis.mapping.AGECmisProperties;
import eu.europa.ec.leos.domain.cmis.metadata.LeosMetadata;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyList;

public final class AGERepositoryUtil extends RepositoryUtil {
    
    private AGERepositoryUtil() {
    }

    public static Map<String, ?> updateDocumentProperties(LeosMetadata metadata) {
        Map<String, Object> properties = new HashMap<>();
        properties.putAll(updateMilestoneCommentsProperties(emptyList()));
        properties.putAll(AGELeosMetadataExtensions.toCmisProperties(metadata));
        return properties;
    }
    
    public static Map<String, List<String>> updateMilestoneCommentsProperties(List<String> milestoneComments) {
        Map<String, List<String>> result = new HashMap<>();
        result.put(AGECmisProperties.MILESTONE_COMMENTS.getId(), milestoneComments);
        return result;
    }
}
