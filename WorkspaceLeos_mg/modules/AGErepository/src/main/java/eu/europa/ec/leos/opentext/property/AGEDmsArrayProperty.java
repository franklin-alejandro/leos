package eu.europa.ec.leos.opentext.property;

import java.util.Iterator;
import java.util.List;

/**
 * Esta clase abstracta es la clase superior de las clases
 * <code>DmsBooleanArrayProperty</code>, <code>DmsDateArrayProperty</code>,
 * <code>DmsNumberArrayProperty</code> y <code>DmsStringArrayProperty</code> y
 * representa una propiedad multivaluado de carpeta o de documento.
 * 
 * @author BosArthur
 */
public abstract class AGEDmsArrayProperty<T> extends AGEDmsProperty {

	public AGEDmsArrayProperty(String name) {
		super(name);
		setSearchCondition(AGEDmsSearchCondition.IN);
	}

	public AGEDmsArrayProperty(String name, AGEDmsSearchCondition searchCondition) {
		super(name, searchCondition);
	}

	public String getValueAsString() {
		StringBuilder buffer = new StringBuilder(1024);
		Iterator<String> iterator = iterateValuesAsStrings();
		boolean isFirst = true;
		while (iterator.hasNext()) {
			if (!isFirst) {
				buffer.append("; ");
			} else {
				isFirst = false;
			}
			String value = iterator.next();
			buffer.append(value);
		}
		buffer.trimToSize();
		return buffer.toString();
	}

	public abstract Iterator<String> iterateValuesAsStrings();

	public abstract Iterator<Object> iterateValuesAsObjects();

	public abstract List<T> getValuesAsList();

	public abstract Object[] getValues();
}
