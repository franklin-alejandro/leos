package eu.europa.ec.leos.security.gda;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.filter.GenericFilterBean;

import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.security.AuthenticatedUser;
import eu.europa.ec.leos.security.SecurityUser;
import eu.europa.ec.leos.security.SecurityUserProvider;

public class CustomFilterGda extends GenericFilterBean {

	private static final Logger LOG = LoggerFactory.getLogger(CustomFilterGda.class);
	private static final String AUTHORIZATION = "Authorization";
	private static final String SSO_USER = "SSO_USER";
	private AuthenticationDetailsSource ads = new WebAuthenticationDetailsSource();
	private SecurityUserProvider userProvider;
	private AuthenticationManager authenticationManager;
	private AuthenticationFailureHandler failureHandler = new SimpleUrlAuthenticationFailureHandler();

	public CustomFilterGda(SecurityUserProvider userProvider) {
		this.userProvider = userProvider;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		LOG.info("ENTRO AL FILTRO ************************");
		if (authentication == null) {
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			String userHeader = httpRequest.getHeader(SSO_USER);
			LOG.info("userHeader -------------------------*******************************--------------------"
					+ userHeader);
			if (userHeader == null || userHeader.isEmpty()) {
				LOG.warn("Authorization failed! Wrong accessToken");

			} else {

				User user = userProvider.getUserByLogin(userHeader);
				LOG.info(" user -------------------------*******************************--------------------"
						+ user.toString());

				AuthenticatedUser authenticatedUser = new AuthenticatedUser(user);
				List<GrantedAuthority> allRoles = new ArrayList<GrantedAuthority>();
				/*
				 * Add all the authorities {application specific + ecas specific} to the
				 * AuthenticatedUser object as a collection of {GrantedAuthority}
				 */
				if (user instanceof SecurityUser) {
					LOG.info(
							" Cargando roles -------------------------*******************************--------------------");

					List<String> leosRoles = ((SecurityUser) user).getRoles();

					LOG.info(
							" Cargando roles -------------------------*******************************--------------------"
									+ leosRoles.size() + " - " + leosRoles.toString());

					leosRoles.forEach(auth -> {
						allRoles.add(new SimpleGrantedAuthority(auth));
					});
				}
				authenticatedUser.setAuthorities(allRoles);

				LOG.info(
						" authenticatedUser -------------------------*******************************--------------------"
								+ authenticatedUser.toString());

				PreAuthenticatedAuthenticationToken token = new PreAuthenticatedAuthenticationToken(authenticatedUser,
						null);
				token.setDetails(ads.buildDetails(request));

				try {
					authentication = authenticationManager.authenticate(token);
					authentication.setAuthenticated(Boolean.TRUE);
					// Setup the security context
					SecurityContextHolder.getContext().setAuthentication(authentication);
					// Send new users to the registration page.

				} catch (Exception e) {
					e.printStackTrace();
					// Authentication information was rejected by the authentication manager
					failureHandler.onAuthenticationFailure((HttpServletRequest) request, (HttpServletResponse) response,
							(AuthenticationException) e);
					return;
				}

			}

		}
		chain.doFilter(request, response);

	}

	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	public void setFailureHandler(AuthenticationFailureHandler failureHandler) {
		this.failureHandler = failureHandler;
	}

}
