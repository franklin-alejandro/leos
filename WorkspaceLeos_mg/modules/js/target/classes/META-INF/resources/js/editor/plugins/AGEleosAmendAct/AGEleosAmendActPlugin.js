define(function leosAmendActPlugin(require) {  
	    
    var pluginTools = require("plugins/pluginTools");
    var log = require("logger");
    var $ = require("jquery");
    var CKEDITOR = require("promise!ckEditor");
    var pluginName = 'AGEleosAmendAct';
    var dialogDefinition = require("./AGEleosAmendActDialog");
    var iconAmendAct = 'leosamendact';
    var commandAmendActMethod = 'commandAmendActMethod';
    var dialogCommand;
    
	var pluginDefinition = { 
	    	icons: iconAmendAct,
	    	lang: 'es',
	    	requires : "dialog",
			init : function(editor) {     
				log.debug("Initializing amend act plugin...");
				
				//editor.once("receiveData",_recieveDataAmend);
				
				pluginTools.addDialog(dialogDefinition.dialogName, dialogDefinition.initializeDialog);
				dialogCommand = editor.addCommand(dialogDefinition.dialogName, new CKEDITOR.dialogCommand(dialogDefinition.dialogName));
				
			    editor.ui.add( pluginName, CKEDITOR.UI_BUTTON,{
			    	label: 'Activar Modificativo',
			        title: 'Activar Modificativo',
			        icon: iconAmendAct,
			        toolbar: 'amend,100',
			        command: commandAmendActMethod
			    });
	            editor.addCommand( commandAmendActMethod, {
	                exec: function( editor ) {
	                	_amendActMethod(editor);
	                }
	            });
	           
			}
	};
	
    
	function _amendActMethod(editor) {
		dialogCommand.exec();
		editor.setReadOnly();
	}
	
	pluginTools.addPlugin(pluginName, pluginDefinition);
	

    var pluginModule = {
        name : pluginName
    };

   
    return pluginModule;
});