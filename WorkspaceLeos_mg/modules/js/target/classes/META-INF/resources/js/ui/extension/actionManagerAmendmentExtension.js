/*
 * Copyright 2020 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
; // jshint ignore:line
define(function actionManagerAmendmentExtensionModule(require) {
    "use strict";

    var $ = require("jquery");
    var UTILS = require("core/leosUtils");

    function _initExtension(connector) {
        _addEditAmendmentButtons(connector);

        var rootElement = UTILS.getParentElement(connector);
        $(rootElement).off("click.actions", ".amendment-editButton");
        $(rootElement).on("click.actions", ".amendment-editButton", _editAmendment.bind(undefined, connector));

        connector.openCloseAmendmentView = _openCloseAmendmentView;
        connector.enableEditAmendmentButtons = _enableEditAmendmentButtons;
    }

function _addEditAmendmentButtons(connector) {
        var openAmendmentViewLabel = connector.getState().openAmendmentViewLabel;
        $(document).find("[refersto='~_ART_AMEND']").each(function () {
            var $this = $(this);debugger;
            var articleId = $this.attr("id");
			var tr = document.getElementById(articleId);
            var corner_td = tr.lastChild;
            var clase = corner_td.className;
            if(!clase.includes("amendment-editButton")){
                var template = [`<div id="amend_articleId_${articleId}" class="amendment-editButton leos-toolbar-button Vaadin-Icons">`];
				template.push(`<span title="${openAmendmentViewLabel}">&#xe7ff</span>`);
				template.push('</div>');
				$this.children().last().after(template.join(''));
            }

        });
    }

    function _openCloseAmendmentView() {
        var connector = this;
        var articleId = connector.getState().articleId;
        var openAmendmentViewLabel = connector.getState().openAmendmentViewLabel;
        if (connector.getState().openLayout) { // remove the button, add DIV underConstruction
            var underModificationLabel = connector.getState().underModificationLabel;
            $(document).find("#" + articleId + " .amendment-editButton").each(function () {
                var $this = $(this);
                var parent = $this.parent();
                $this.remove();
                parent.children().last().after("<div id='" + "amend_articleId_" + articleId + "' class='amendment-div-underModification'>"
                    + underModificationLabel + "</div>");
            });
        } else { // remove DIV underConstruction, add the button
            $(document).find("#" + articleId + " .amendment-div-underModification").each(function () {
                var $this = $(this);
                var parent = $this.parent();
                $this.remove();
                var template = [`<div id="amend_articleId_${articleId}" class="amendment-editButton leos-toolbar-button Vaadin-Icons">`];
                template.push(`<span title="${openAmendmentViewLabel}">&#xe7ff</span>`);
                template.push('</div>');
                parent.children().last().after(template.join(''));
            });
            _enableEditAmendmentButtons();
        }
    }

    function _disableEditAmendmentButtons(){
        $(document).find(".amendment-editButton").each(function () {
            var $this = $(this);
            $this.css("pointer-events", "none" );
            $this.parents('.leos-actions').children().css( "pointer-events", "none" );
        });
    }

    function _enableEditAmendmentButtons() {
        $(document).find(".amendment-editButton").each(function () {
            var $this = $(this);
            $this.css( "pointer-events", "all" );
            $this.next().children().css( "pointer-events", "all" );
        });
    }
    
     document.onkeydown = function disableF5(e) { debugger;
    	if ((e.which || e.keyCode) == 116){
    		 e.preventDefault(); 
    		 document.getElementById("refreshDocument").click();   
    		 }
    	}

    function _editAmendment(connector, event) {debugger;
        var $element = $(event.currentTarget);
        var elementId = $element.attr("id").replace("amend_articleId_", "");
        var type = document.getElementById(elementId).localName;
        var data = {
            elementId: elementId,
            type: type           
        };
        _disableEditAmendmentButtons();
        connector.editAmendment(data);
    }

    return {
        init : _initExtension
    };
});
