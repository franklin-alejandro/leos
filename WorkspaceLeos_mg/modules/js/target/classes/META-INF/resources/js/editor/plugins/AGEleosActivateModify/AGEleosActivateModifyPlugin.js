﻿
; // jshint ignore:line
 //Evolutivo #2378
define(function leosActivateModifyPluginModule(require) {
    "use strict";

    // load module dependencies
    var pluginTools = require("plugins/pluginTools");
    var CKEDITOR = require("promise!ckEditor");
    var jQuery = require('jquery');
    var LOG = require("logger");
    var leosPluginUtils = require("plugins/leosPluginUtils");  
    var leosKeyHandler = require("plugins/leosKeyHandler/leosKeyHandler");
    var leosAknNumberedParagraph = require("plugins/AGEaknNumberedParagraph/AGEaknNumberedParagraphPlugin");//AGE-EXT-3.0.0
    var pluginName = 'AGEleosActivateModify';//AGE-EXT-3.0.0
    var commandActivateMethod = 'activatemodify';
    var commandRegulatoryText = 'insertText';
    var iconActivateModify = 'leosactivatemodify';
    var iconActivateModifyPng = 'leosactivatemodify.png';
    var pathIconDeactivateModify = 'icons/leosdesactivatemodify.png';
    var pathIconActivateModify = 'icons/leosactivatemodify.png';
    var idButtonDeactivateModify = '.cke_button__leosactivatemodify_icon';
    var TYPE_LETTER = "ORDINAL_LETTER";
    var HTML_ARTICLE = "article";
    var HTML_PROVISO = "proviso";
    var HTML_LIST = "li";
    var HTML_RULE_M = "RULE";

    var pluginDefinition = {
    	icons: iconActivateModify,
    	lang: 'es',
    	init: function( editor ) {
            editor.addCommand( commandActivateMethod, {
                exec: function( editor ) {
                	_activateModify(editor);
                }
            });
            editor.ui.addButton( 'leosActivateModify', {
            	label: 'Activar Modificativo',
                title: 'Activar Modificativo',
                icon: iconActivateModify,
                command: commandActivateMethod,
                toolbar: 'mode,100'
            });
            editor.on("instanceReady", function(event) {
            	var jqEditor = $(event.editor.editable().$);
            	var aknElement = jqEditor.find("*[data-akn-name='article'],*[data-akn-name='proviso']");
            	var aknActivateModify = "false";
            	if (aknElement[0].localName.length > 0 && leosPluginUtils.getElementName(aknElement[0]) === HTML_ARTICLE) {
            		if (aknElement[0].offsetParent.hasAttribute("leos:ordering")) {
            			var regulatoryCommandCmd=event.editor.getCommand(commandRegulatoryText);
            			var activateCommandCmd=event.editor.getCommand(commandActivateMethod);

            			activateCommandCmd.setState(CKEDITOR.TRISTATE_DISABLED);
            			regulatoryCommandCmd.setState(CKEDITOR.TRISTATE_ENABLED);

            		} else {
            			if (aknElement[0].hasAttribute("data-akn-attr-mod")) {
            				aknActivateModify=aknElement[0].attributes["data-akn-attr-mod"].nodeValue;
            			} else {
            				aknElement[0].setAttribute("data-akn-attr-mod", "false");
            			}
            			if (aknActivateModify.indexOf('true') !== -1){
            				var regulatoryCommandCmd=event.editor.getCommand(commandRegulatoryText);
            				jQuery(idButtonDeactivateModify).css('background-image', 'url('+CKEDITOR.plugins.getPath(pluginName)+ pathIconDeactivateModify+')');
            				jQuery(idButtonDeactivateModify).prop('title', editor.lang.AGEleosActivateModify.messageTitleDeactivate);//idioma
            				regulatoryCommandCmd.setState(CKEDITOR.TRISTATE_ENABLED);
            			} else {
            				var regulatoryCommandCmd=event.editor.getCommand(commandRegulatoryText);
            				jQuery(idButtonDeactivateModify).css('background-image', 'url('+CKEDITOR.plugins.getPath(pluginName)+ pathIconActivateModify+')');
            				jQuery(idButtonDeactivateModify).prop('title', editor.lang.AGEleosActivateModify.messageTitleActivate);//idioma
            				regulatoryCommandCmd.setState(CKEDITOR.TRISTATE_DISABLED);
            			}
            		}
            	} else if (aknElement[0].localName.length > 0 && leosPluginUtils.getElementName(aknElement[0]) === HTML_PROVISO) {
            		if (aknElement[0].offsetParent.hasAttribute("leos:ordering")) {
            			if (aknElement[0].hasAttribute("data-akn-attr-mod")) {
            				aknActivateModify=aknElement[0].attributes["data-akn-attr-mod"].nodeValue;
            			} else {
            				aknElement[0].setAttribute("data-akn-attr-mod", "false");
            			}
            			if (aknActivateModify.indexOf("true") !== -1) {
            				jQuery(idButtonDeactivateModify).css('background-image', 'url('+CKEDITOR.plugins.getPath(pluginName)+ pathIconDeactivateModify+')');
            				jQuery(idButtonDeactivateModify).prop('title', editor.lang.AGEleosActivateModify.messageTitleDeactivate);
            				var regulatoryCommandCmd=event.editor.getCommand(commandRegulatoryText);
            				regulatoryCommandCmd.setState(CKEDITOR.TRISTATE_ENABLED);
            			} else {
            				var regulatoryCommandCmd=event.editor.getCommand(commandRegulatoryText);
            				jQuery(idButtonDeactivateModify).css('background-image', 'url('+CKEDITOR.plugins.getPath(pluginName)+ pathIconActivateModify+')');
            				jQuery(idButtonDeactivateModify).prop('title', editor.lang.AGEleosActivateModify.messageTitleActivate);
            				regulatoryCommandCmd.setState(CKEDITOR.TRISTATE_DISABLED);
            			}
            		} else {
            			if (aknElement[0].hasAttribute("data-akn-attr-mod")) {
            				aknActivateModify=aknElement[0].attributes["data-akn-attr-mod"].nodeValue; 	
            			} else {
            				aknElement[0].setAttribute("data-akn-attr-mod", "false");
            			}
            			if (aknActivateModify.indexOf("true") !== -1) {
            				var regulatoryCommandCmd=event.editor.getCommand(commandRegulatoryText);
            				jQuery(idButtonDeactivateModify).css('background-image', 'url('+CKEDITOR.plugins.getPath(pluginName)+ pathIconDeactivateModify+')');
            				jQuery(idButtonDeactivateModify).prop('title', editor.lang.AGEleosActivateModify.messageTitleDeactivate);
            				regulatoryCommandCmd.setState(CKEDITOR.TRISTATE_ENABLED);
            			} else {
            				var regulatoryCommandCmd=event.editor.getCommand(commandRegulatoryText);
            				jQuery(idButtonDeactivateModify).css('background-image', 'url('+CKEDITOR.plugins.getPath(pluginName)+ pathIconActivateModify+')');
            				jQuery(idButtonDeactivateModify).prop('title', editor.lang.AGEleosActivateModify.messageTitleActivate);
            				regulatoryCommandCmd.setState(CKEDITOR.TRISTATE_DISABLED);
            			}
            		}
            	}  
            });
        }
    };
    
    function openModDialog(type,editor) {
    	if (type){
    		return confirm(editor.lang.AGEleosActivateModify.messageTransforToNoModify);//idioma
    	} else {
    		return confirm(editor.lang.AGEleosActivateModify.messageTransforArticleModify);//idioma
    	}
    	
    }
    
    function rand_code(){
    	var caracteres = "0123456789abcdefghijklmnñopqrstuvwxyz";
    	var lon = 9;
    	var code = "";
    	var rand;
    	for (var x=0; x < lon; x++)
    	{
    		rand = Math.floor(Math.random()*caracteres.length);
    		code += caracteres.substr(rand, 1);
    	}
    	return code;
    }
    
    function cleanText (text) {
    	if (text.trim().substring(text.trim().length - 1).indexOf("»") !== -1){
    		text = text.trim().slice(0, -1);
    	}
    	if (text.trim().substring(0, 1).indexOf("«") !== -1) {
    		text = text.trim().slice(1);
    	}
    	return text;
    }
    
    function convertRuleToParagraph () {
    	var arrayElementLi = jQuery("ol>li").toArray();
		arrayElementLi.forEach(function (value) {
			var arrayElementRules =  Array.from(value.childNodes).reverse();
			arrayElementRules.forEach(function (rule) {
				if(rule.nodeName.indexOf(HTML_RULE_M) !== -1 ){     
					var text=rule.innerText;
					rule.parentNode.insertAdjacentHTML('afterend',"<li data-akn-num='1.' id='prov_parag_" + rand_code() +"'>" + cleanText(text) + " <br></li>");
					rule.remove();
				} 
			});
		});
    }
    
    function _deactivate(rootElt,editor) {
    	rootElt.$.attributes["data-akn-attr-mod"].value = "false";
		jQuery(idButtonDeactivateModify).css('background-image', 'url('+CKEDITOR.plugins.getPath(pluginName)+ pathIconActivateModify+')');
		jQuery(idButtonDeactivateModify).prop('title', editor.lang.AGEleosActivateModify.messageTitleActivate);
		var regulatoryCommandCmd=editor.getCommand(commandRegulatoryText);
		regulatoryCommandCmd.setState(CKEDITOR.TRISTATE_DISABLED);
		convertRuleToParagraph();
    }
    
    function _activate(rootElt,editor) {
    	rootElt.$.attributes["data-akn-attr-mod"].value = "true";
		var regulatoryCommandCmd = editor.getCommand(commandRegulatoryText);
		jQuery(idButtonDeactivateModify).css('background-image', 'url('+CKEDITOR.plugins.getPath(pluginName)+ pathIconDeactivateModify+')');
		jQuery(idButtonDeactivateModify).prop('title', editor.lang.AGEleosActivateModify.messageTitleDeactivate);
		regulatoryCommandCmd.setState(CKEDITOR.TRISTATE_ENABLED);
		var element = jQuery("ol");
		element[0].insertAdjacentHTML('afterbegin',"<li data-akn-name='aknNumberedParagraph'><br></li>");
		editor.getSelection().selectElement(new CKEDITOR.dom.node(element.find(">ol>li>br")[0]));
    }
    
    function _activateModify(editor) {
	    var rootElt = editor.element.getChild(0);
	    var aknActivateModify = "false";
	    if (leosPluginUtils.getElementName(rootElt) === HTML_ARTICLE || leosPluginUtils.getElementName(rootElt) === HTML_PROVISO) {
	    	if (!rootElt.$.offsetParent.hasAttribute("leos:ordering")) {
	    		aknActivateModify = rootElt.$.attributes["data-akn-attr-mod"].nodeValue; 
	    		if (aknActivateModify.indexOf("true") !== -1) {
	    			if (openModDialog(true,editor)){
	    				_deactivate(rootElt,editor);
	    			}
	    		} else {
	    			if (openModDialog(false,editor)){
	    				_activate(rootElt,editor);
	    			}
	    		}
	    	}
	    }    	
    }

	    pluginTools.addPlugin(pluginName, pluginDefinition);

	    // return plugin module
	    var pluginModule = {
	        name : pluginName
	    };
	    return pluginModule;
});