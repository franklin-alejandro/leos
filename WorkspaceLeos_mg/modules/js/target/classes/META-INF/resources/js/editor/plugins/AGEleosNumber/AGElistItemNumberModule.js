/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
; // jshint ignore:line
define(function listItemNumberModule(require) {
    "use strict";

    var UTILS = require("core/leosUtils");
    var ckEditor;
    var DATA_AKN_NUM = "data-akn-num";
    var ORDER_LIST_ELEMENT = "ol";
    var QUOTEDSTRUCTURE = "quotedstructure";
    var defaultList = [];
    var listNumberConfig;
    var numberingConfigs;

    var sequenceMap = [
        {
            type: "ALPHA",
            inDefault: true,
            format: "x",
            prefix: "",
            suffix: ")",
            name: 'Alphabets',
            generator: function generateSequenceForAlpha(list, item, idx) {
                return this.prefix+this.format.replace('x', generateAlpha(idx))+this.suffix;
            }
        }, {
            type: "ARABIC-PARENTHESIS",
            inDefault: true,
            format: "x",
            prefix: "",
            suffix: ")",
            name: 'Arabic',
            generator: function generateSequenceForArabic(list, item, idx) {
                return this.prefix+this.format.replace('x', idx + 1)+this.suffix;
            }
        }, {
            type: "ARABIC-POSTFIX",
            inDefault: true,
            format: "x",
            prefix: "",
            suffix: ".",
            name: 'Arabic',
            generator: function generateSequenceForArabic(list, item, idx) {
                return this.prefix+this.format.replace('x', idx + 1)+this.suffix;
            }
        },{
            type: "ARABIC-POSTFIX-LETTER",
            inDefault: true,
            format: "x",
            prefix: "",
            suffix: ".",
            name: 'Arabic',
            generator: function generateSequenceForArabicNumbered(list, item, idx) {
                return this.prefix+this.format.replace('x', _convertNumberToText(idx + 1))+this.suffix;
            }
        }, {
            type: "ROMAN-LOWER",
            inDefault: true,
            format: "x",
            prefix: "",
            suffix: ")",
            name: 'Roman',
            generator: function generateSequenceForRoman(list, item, idx) {
                return this.prefix+this.format.replace('x', romanize(idx + 1))+this.suffix;
            }
        }, {
            type: "INDENT",
            inDefault: true,
            format: "",
            prefix: "",
            suffix: "",
            name: 'IndentDash',
            generator: function generateSequenceForIndent(list, item, idx) {
                return this.prefix+this.format.replace('x', '')+this.suffix;
            }
        }
    ];

    function _getSequences(seqName) {
        if (seqName && seqName === 'Paragraph') {
            var paragraphTocItem = ckEditor.LEOS.tocItemsList.find(function(e){return e.aknTag === 'paragraph'});
            var paraNumTypeName = paragraphTocItem.numberingType;
            var paragraphNumType = numberingConfigs.find(function(e){return e.type === paraNumTypeName});
            var paragraphSequence = sequenceMap.find(function(el){return el.type === paraNumTypeName});
            paragraphSequence.format = 'x';
            paragraphSequence.suffix = paragraphNumType.suffix;
            return paragraphSequence;
        }
        else if (seqName && seqName === 'Paragraph-letter') {
        	var paraNumTypeName = "ARABIC-POSTFIX-LETTER"
            var paragraphSequence = sequenceMap.find(function(el){return el.type === paraNumTypeName});
            paragraphSequence.format = 'x';
            paragraphSequence.suffix = '.';
            return paragraphSequence;
        }
        else if (seqName) {
            return defaultList.find(function(el){return el.name === seqName});
        }
        else {//return all sequences
            return sequenceMap;
        }
    }

    /*
     * For given num param return roman number literal
     */
    function romanize(num) {
        var digits = String(+num).split(""),
            key = ["", "c", "cc", "ccc", "cd", "d", "dc", "dcc", "dccc", "cm", "", "x", "xx", "xxx", "xl", "l", "lx", "lxx",
                "lxxx", "xc", "", "i", "ii", "iii", "iv", "v", "vi", "vii", "viii", "ix"], roman = "", i = 3;
        while (i--) {
            roman = (key[+digits.pop() + (i * 10)] || "") + roman;
        }
        return Array(+digits.join("") + 1).join("M") + roman;
    }

    /* Returns the array containing literals for alpha points
     * AGE-EXT-Soporte #1009*/
    function generateAlpha(sequenceNumber) {
    	var arrayLetters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
        var startOfAlfaNumerical = 0;
        var endOfAlfaNumerical = 27;
        var sequenceBase = endOfAlfaNumerical - startOfAlfaNumerical;
        var currentSequenceBase = 0;
        var sequence = [];
        while (true) {
            var currentLetter;
            if (currentSequenceBase > 0) {
                var currentBaseCount = parseInt(sequenceNumber / (Math.pow(sequenceBase, currentSequenceBase)));
                if (currentBaseCount === 0) {
                    break;
                }
                currentLetter = arrayLetters[currentBaseCount + startOfAlfaNumerical - 1];
            }

            if (currentSequenceBase === 0) {
            	currentLetter = arrayLetters[(sequenceNumber % sequenceBase) + startOfAlfaNumerical];
            }
            currentSequenceBase++;
            sequence.unshift(currentLetter);
        }
        return sequence.join("");
    }

    /*
     * To identify the sequence first point in the list is used
     */
    function identifySequence(listItems, currentNestingLevel) {
        return getSequenceFromDefaultList(currentNestingLevel);
    }

    function _initialize(editor) {
        ckEditor = editor;
        _initializeDefaultList(editor);
    }
    
    function _initializeDefaultList(editor) {
        var sequences = _getSequences();
        listNumberConfig = editor.LEOS.listNumberConfig;
        numberingConfigs = editor.LEOS.numberingConfigs;
        for (var i = 0; i < listNumberConfig.length; i++) {
            if (sequences[i].inDefault) {
                var numberType = numberingConfigs.find(function(e){return e.type === listNumberConfig[i].numberingType});
                var defaultListItem = sequences.find(function(e){return e.type ===  numberType.type});
                defaultListItem.prefix = numberType.prefix;
                defaultListItem.suffix = numberType.suffix;
                defaultList[listNumberConfig[i].depth-1] = defaultListItem;
            }
        }
    }   
        

    /*
     * Get sequence type from default sequence list
     */
    function getSequenceFromDefaultList(currentNestingLevel) {
        var sequenceType = '';
        if (currentNestingLevel > defaultList.length || !defaultList[currentNestingLevel - 1]) {
            sequenceType = _getSequences('IndentDash');
            defaultList[currentNestingLevel - 1] = sequenceType;
        } else {
            sequenceType = defaultList[currentNestingLevel - 1];
        }
        return sequenceType;
    }

    /*
     * Returns the nesting level for given ol element
     */
    function getNestingLevelForOl(olElement) {
        var nestingLevel = -1;
        if (getQuotedStructure(olElement)) {
            nestingLevel++;
        }     
        var currentOl = new CKEDITOR.dom.node(olElement);
        while (currentOl) {
            currentOl = currentOl.getAscendant(ORDER_LIST_ELEMENT);
            nestingLevel++;
        }
        return nestingLevel;
    }    
    
    
    function getQuotedStructure(element) {
	    var i = 0;
	    do {
	    	if (element.localName === QUOTEDSTRUCTURE) {
	        	return false;
	        }
	    	if (element.localName === ORDER_LIST_ELEMENT && (element.getAttribute("data-akn-name") != null && element.attributes["data-akn-name"].nodeValue === "aknPointMandateOrderedList")) {
	    		return true;
	    	}
	        element = element.parentElement;
	        i += 1;
	    } while (i < 8);
	
	    return false;
    }    

    /*
     * Called to update numbering, changed from indent plugin or context menu
     */
    function _updateNumbers(orderedLists, seqNum) {
        for (var ii = 0; ii < orderedLists.length; ii++) {
            var orderedList = orderedLists[ii];
            var currentNestingLevel = getNestingLevelForOl(orderedList);
            var listItems = orderedList.children;
            var sequence = '';
            if (typeof seqNum === 'undefined' || !seqNum) {
                sequence = identifySequence(listItems, currentNestingLevel);
            } else {
                sequence = seqNum;
            }
            (UTILS.getElementOrigin(orderedList) && UTILS.getElementOrigin(orderedList) === 'ec')
                ? _doMandateNum(orderedList, sequence)
                : _doProposalNum(orderedList, sequence);
        }
    }

    //AGE-EXT-Evolutivo #1204-Correctivo #1277-Evolutivo #2378   
    function _doProposalNum(orderedList, sequence) {
    	var listItems = orderedList.children;
    	var pos = 0;
    	for (var jj = 0; jj < listItems.length; jj++) {
    		if (listItems[jj].parentNode.parentNode.attributes["data-akn-attr-mod"]){
    			var leosMod = "false";
    			leosMod = listItems[jj].parentNode.parentNode.attributes["data-akn-attr-mod"].nodeValue;
        			if (leosMod.indexOf('true') !== -1 && jj == 0 && sequence.format.indexOf('x') !== -1 && sequence.suffix.indexOf('.') !== -1) {
        				pos ++;
        				continue;
        			}	
    		}
    		sequence && listItems[jj].setAttribute(DATA_AKN_NUM, sequence.generator(orderedList, listItems[jj], jj - pos));
    	}
    }

    function _doMandateNum(orderedList, sequence) {
        var listItems = orderedList.children;
        for (var jj = 0; jj < listItems.length; jj++) {
            if (!UTILS.getElementOrigin(listItems[jj])) {
                sequence && listItems[jj].setAttribute(DATA_AKN_NUM, sequence.format.replace('x', '#'));
            }
        }
    }
    
    function _units(num){
        switch(num)
        {
            case 1: return "uno";
            case 2: return "dos";
            case 3: return "tres";
            case 4: return "cuatro";
            case 5: return "cinco";
            case 6: return "seis";
            case 7: return "siete";
            case 8: return "ocho";
            case 9: return "nueve";
        }

        return "";
    }

    function _tens(num){

        var ten = Math.floor(num/10);
        var unit = num - (ten * 10);

        switch(ten)
        {
            case 1:
                switch(unit)
                {
                    case 0: return "diez";
                    case 1: return "once";
                    case 2: return "doce";
                    case 3: return "trece";
                    case 4: return "catorce";
                    case 5: return "quince";
                    default: return "dieci" + _units(unit);
                }
            case 2:
                switch(unit)
                {
                    case 0: return "veinte";
                    default: return "veinti" + _units(unit);
                }
            case 3: return _tens_and("treinta", unit);
            case 4: return _tens_and("cuarenta", unit);
            case 5: return _tens_and("cincuenta", unit);
            case 6: return _tens_and("sesenta", unit);
            case 7: return _tens_and("setenta", unit);
            case 8: return _tens_and("Ochenta", unit);
            case 9: return _tens_and("noventa", unit);
            case 0: return _units(unit);
        }
    }

	function _hundreds(num){
		
		var hundred = Math.floor(num/100);
		var ten = Math.floor(num/10) - (hundred*10);
        var unit = num - (ten * 10) - (hundred*100);
		
		if (num==100) {
				return "cien";
		}
		
		switch(hundred)
		{
			case 0: return _tens(num);
			case 1: return "ciento " + _tens(num-100);
			case 2: return "doscientos " + _tens(num-200);
			case 3: return "trescientos " + _tens(num-300);
			case 4: return "cuatrocientos " + _tens(num-400);
			case 5: return "quinientos " + _tens(num-500);
			case 6: return "seiscientos " + _tens(num-600);
			case 7: return "setecientos " + _tens(num-700);
			case 8: return "ochocientos " + _tens(num-800);
			case 9: return "novecientos " + _tens(num-900);
		}
	}

    function _tens_and(tensStr, numUnits) {
        if (numUnits > 0)
            return tensStr + " y " + _units(numUnits)

        return tensStr;
    }

    function _convertNumberToText(num){
        return capitalizeFirstChar(_hundreds(num));
    }

    function capitalizeFirstChar(text){
        if(text != null && text != "")
            return text.charAt(0).toUpperCase() + text.slice(1);
        return text;
    }

    return {
        init: _initialize,
        getSequences: _getSequences,
        updateNumbers: _updateNumbers
    };
});