/*
 * Copyright 2018 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
; // jshint ignore:line
define(function aknRolePluginModule(require) {
    "use strict";

    // load module dependencies
    var pluginTools = require("plugins/pluginTools");
    var aknRoleWidget = require("plugins/AGEaknRoleWidget/AGEaknRoleWidget");//AGE-EXT-3.0.0
    var leosKeyHandler = require("plugins/leosKeyHandler/leosKeyHandler");

    var pluginName = "AGEaknRole";//AGE-EXT-3.0.0
    
    var ENTER_KEY = 13;

    var pluginDefinition = {
        requires : "widget,leosWidget",

        init : function init(editor) {
            leosKeyHandler.on({
                editor : editor,
                eventType : 'key',
                key : ENTER_KEY,
                action : _onEnterKey
            });
            editor.on("toHtml", removeInitialSnapshot, null, null, 100);

            editor.widgets.add(aknRoleWidget.name, aknRoleWidget.definition);
            editor.addContentsCss(pluginTools.getResourceUrl(pluginName, aknRoleWidget.css));

            //editor.widgets.add(aknSignaturePersonWidget.name, aknSignaturePersonWidget.definition);
            //editor.addContentsCss(pluginTools.getResourceUrl(pluginName, aknSignaturePersonWidget.css));
        }
    };

    pluginTools.addPlugin(pluginName, pluginDefinition);

    function _onEnterKey(context) {
        context.event.cancel();
    }
    
    /*
     * Removes the initial snapshot which don't have 'article' as top level element 
     */
    function removeInitialSnapshot(event) {
        if (event.editor.undoManager.snapshots.length > 0) {
            if (event.editor.undoManager.snapshots[0].contents.indexOf("role")<0) {
                event.editor.undoManager.snapshots.shift();
            }
        }
    }
    
    var transformationConfig = {
            akn : 'role',
            html : 'role',
            attr : [ {
                akn : "xml:id",
                html : "id"
            }, {
                akn : "leos:origin",
                html : "data-origin"
            }, {
                akn : "refersTo",
                html : "refersto"
            }, {
                akn : "leos:editable",
                html : "data-akn-attr-editable"
            }, {
                akn : "leos:deletable",
                html : "data-akn-attr-deletable"
            }, {
                html : "data-akn-name=role"
            } ],
            sub : {
            	akn : "text",
                html : "role/text"
            }  
        };

    // return plugin module
    var pluginModule = {
        name : pluginName
    };

    pluginTools.addTransformationConfigForPlugin(transformationConfig, pluginName);

    return pluginModule;
});