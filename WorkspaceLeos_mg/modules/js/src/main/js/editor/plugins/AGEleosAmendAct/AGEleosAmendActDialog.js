//Evolutivo #2678
define(function leosAmendActDialog(require) {
	"use strict";

	var dialogDefinition = {
			dialogName: "AGEleosAmendActDialog"
	};

	dialogDefinition.initializeDialog = function initializeDialog(editor) {
		var msg = editor.lang.AGEleosAmendAct.messageConfirm;
		var dialogDefinition = {
				title: editor.lang.AGEleosAmendAct.warningTitle,
				minWidth: 400,
				minHeight: 50,
				contents: [{
					id: 'tab1',
					elements: [{
						id: "confirm",
						type: 'hbox',
						widths: ['100%'],
						height: 50,
						children: [{
							type: 'html',
							html: msg
						}]
					}]
				}],
				onOk: function (event) {
					var id = editor.element.$.firstChild.id;
					var articleNum = editor.element.$.firstChild.firstChild.firstChild.outerText;
					var type = editor.element.$.firstChild.localName;
					
					var data = {
							elementId:id,
							articleNum:articleNum,
							type:type
					}
					editor.fire("requestAmendActView",data);
					event.sender.hide();
					event.cancel();
					event.stop();
				}


		};
        return dialogDefinition;
    };

	return dialogDefinition;
});
