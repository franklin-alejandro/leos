/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
; // jshint ignore:line
define(function aknFormulaPluginModule(require) {
    "use strict";

    // load module dependencies
    var pluginTools = require("plugins/pluginTools");
    var leosKeyHandler = require("plugins/leosKeyHandler/leosKeyHandler");

    var pluginName = "AGEaknFormula";//AGE-EXT-Evolutivo #2543-Evolutivo #2546
    var ENTER_KEY = 13;

    var cssPath = "css/" + pluginName + ".css";

    var pluginDefinition = {
        init : function init(editor) {
            editor.addContentsCss(pluginTools.getResourceUrl(pluginName, cssPath));            
            leosKeyHandler.on({
                editor : editor,
                eventType : 'key',
                key : ENTER_KEY,
                action : _onEnterKey
            });
        }
    };
    
    function _onEnterKey(context) {
        context.event.cancel();
    }

    pluginTools.addPlugin(pluginName, pluginDefinition);

    var FORMULA_NAME = "formula";
    


    var transformationConfig = {
   		 akn: FORMULA_NAME,
   	        html : "formula",
   	        attr : [ {
   	            akn : "xml:id",
   	            html : "id"
   	        }, {
   	            akn : "refersTo",
   	            html : "data-refersto"
   	        }, {
   	            akn : "leos:editable",
   	            html : "data-akn-attr-editable"
   	        }, {
   	        	akn : "name",
   	            html : "data-akn-name"
   	        }],
   	        sub : {
   	            akn : "mp",
   	            html : "formula/p",
   	            attr : [ {
   	                akn : "xml:id",
   	                html : "data-akn-mp-id"
   	            }],
   	            sub: [ {
   	                akn: "text",
   	                html: "formula/p/text"
   	            }, {//AGE-EXT-Evolutivo #1296
   	            	akn: "location",
   	            	html: "formula/p/location",
   	            	attr: [{
   	                     akn: "xml:id",
   	                     html: "data-akn-location-id"
   	                     }, {
   	                     akn: "refersTo",
   	                     html: "data-refersto"
   	                     }],
   	                     sub: {
   	                         akn: "text",
   	                         html: "formula/p/location/text"
   	                     }
   	                 }]
   	            }
   	    };

    pluginTools.addTransformationConfigForPlugin(transformationConfig, pluginName);

    // return plugin module
    var pluginModule = {
        name : pluginName,
        transformationConfig : transformationConfig
    };

    return pluginModule;
});