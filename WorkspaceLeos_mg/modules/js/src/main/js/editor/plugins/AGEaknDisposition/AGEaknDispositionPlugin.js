/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
; // jshint ignore:line
define(function aknDispositionPluginModule(require) {
    "use strict";

    // load module dependencies
    var pluginTools = require("plugins/pluginTools");
    //AGE-EXT
    var aknDispositionNumberWidget = require("plugins/AGEaknDispositionWidget/AGEaknDispositionNumberWidget");
    var aknDispositionHeadingWidget = require("plugins/AGEaknDispositionWidget/AGEaknDispositionHeadingWidget");
    //AGE-EXT
    var pluginName = "AGEaknDisposition";

    var pluginDefinition = {
        requires : "widget,leosWidget",

        init : function init(editor) {
            editor.on("toHtml", removeInitialSnapshot, null, null, 100);

            editor.widgets.add(aknDispositionNumberWidget.name, aknDispositionNumberWidget.definition);
            editor.addContentsCss(pluginTools.getResourceUrl(pluginName, aknDispositionNumberWidget.css));

            editor.widgets.add(aknDispositionHeadingWidget.name, aknDispositionHeadingWidget.definition);
            editor.addContentsCss(pluginTools.getResourceUrl(pluginName, aknDispositionHeadingWidget.css));
        }
    };

    pluginTools.addPlugin(pluginName, pluginDefinition);

    
    /*
     * Removes the initial snapshot which don't have 'disposition' as top level element 
     */
    function removeInitialSnapshot(event) {
        if (event.editor.undoManager.snapshots.length > 0) {
            if (event.editor.undoManager.snapshots[0].contents.indexOf("proviso")<0) {
                event.editor.undoManager.snapshots.shift();
            }
        }
    }
    
    var transformationConfig = {
        akn : 'proviso',
        html : 'proviso',
        attr : [ {
            akn : "xml:id",
            html : "id"
        }, {
            akn : "leos:origin",
            html : "data-origin"
        }, {
            akn : "leos:editable",
            html : "data-akn-attr-editable"
        }, {
            akn : "leos:mod",//AGE-EXT-Evolutivo #2378
            html : "data-akn-attr-mod"
        }, {
            akn : "leos:deletable",
            html : "data-akn-attr-deletable"
        }, {
            html : "data-akn-name=proviso"
        } ],
        sub : [ {
            akn : "num",
            html : "proviso/h1",
            attr : [ {
                html : "class=akn-disposition-num"
            }, {
                akn : "leos:editable",
                html : "contenteditable=false"
            }, {
                akn : "leos:origin",
                html : "data-num-origin"
            }, {
                akn : "xml:id",
                html : "data-akn-num-id"
            } ],
            sub : {
                akn : "text",
                html : "proviso/h1/text"
            }
        }, {
            akn : "heading",
            html : "proviso/h2",
            attr : [ {
                html : "class=akn-disposition-heading"
            },{ 
                html : "data-akn-name=aknHeading"
            }, {
                akn : "leos:origin",
                html : "data-heading-origin"
            }, {
                akn : "leos:editable",
                html : "contenteditable"
            }, {
                akn : "xml:id",
                html : "data-akn-heading-id"
            } ],
            sub : {
                akn : "text",
                html : "proviso/h2/text"
            }
        }, {
            akn : "proviso",
            html : "proviso/ol",
            attr : [ {
                akn : "xml:id",
                html : "id"
            }, {
                akn : "leos:origin",
                html : "data-origin"
            } ]
        } ]
    };

    // return plugin module
    var pluginModule = {
        name : pluginName
    };

    pluginTools.addTransformationConfigForPlugin(transformationConfig, pluginName);

    return pluginModule;
});