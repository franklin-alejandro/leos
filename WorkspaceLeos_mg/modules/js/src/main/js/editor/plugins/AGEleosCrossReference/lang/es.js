/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
CKEDITOR.plugins.setLang( 'AGEleosCrossReference', 'es', {
   contentPaneMessage : 'Vista previa de contenido no disponible',
   refChanged : '¡Atención! La referencia existente será cambiada',
   refElementRequired : 'Por favor seleccione al menos un elemento del árbol para crear una referencia.',
   supportedElementsMessage : 'La referencia interna solo está permitida para los elementos soportados',
   movedOrDeletedElementsMessage : 'La referencia interna no está permitida en elementos movidos / eliminados',
   selectionNotAllowed : 'Solo se pueden seleccionar hermanos del mismo tipo',
   existingRef : 'Referencia existente: ',
   brokenRef : 'La referencia existente no es válida. Por favor actualice la referencia',//AGE-EXT-3.0.0
   memorandum: 'Memorando explicativo',
   bill: 'Texto legal',
   annex: 'Anexo',
   levelDescription: 'Point'
});
