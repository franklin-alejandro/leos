/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
; // jshint ignore:line
define(function aknPointAmendPluginModule(require) {
    "use strict";

    // load module dependencies
    var pluginTools = require("plugins/pluginTools");
    var leosHierarchicalElementTransformerStamp = require("plugins/leosHierarchicalElementTransformer/hierarchicalElementTransformer");
    var leosKeyHandler = require("plugins/leosKeyHandler/leosKeyHandler");

    var pluginName = "AGEaknPointAmend";
    var ENTER_KEY = 13; 
    
    var QUOTEDSTRUCTURE = "quotedstructure";

    var HTML_SUB_PARAGRAPH = "p";
    var HTML_PARAGRAPH = "li";

    var POINT_SELECTOR = "*[data-akn-name='aknPointMandateOrderedList']";
    var ORDERED_LIST_SELECTOR = "ol[data-akn-name='aknPointMandateOrderedList']";

    var pluginDefinition = {
        init: function init(editor) {
            leosKeyHandler.on({
                editor : editor,
                eventType : 'key',
                key : ENTER_KEY,
                action : _onEnterKey
            });
        }
    };

    function _onTabKeyPushEnter(context) {
    	var isContinue = true;
	    var jqEditor = $(context.event.editor.editable().$);
		var selection = context.event.editor.getSelection();
		var startElement = leosKeyHandler.getSelectedElement(selection)
	    var currentElement = startElement.$;
		if (currentElement.localName == HTML_SUB_PARAGRAPH && currentElement.nextElementSibling != null && currentElement.nextElementSibling.localName != HTML_SUB_PARAGRAPH) {
	       currentElement.nextElementSibling.insertAdjacentHTML('afterbegin',"<li data-akn-name='aknPointMandateOrderedList' data-akn-modification='true' data-akn-num='1.º' ><br></li>");
		} else if (currentElement.localName == HTML_SUB_PARAGRAPH && currentElement.nextElementSibling == null){
		       currentElement.insertAdjacentHTML('afterend',"<ol data-akn-name='aknPointMandateOrderedList'><li data-akn-name='aknPointMandateOrderedList' data-akn-num='1.º' ><br></li></ol>");
		} else if (currentElement.localName == HTML_PARAGRAPH){
	       currentElement.insertAdjacentHTML('beforeend',"<ol data-akn-name='aknPointMandateOrderedList'><li data-akn-name='aknPointMandateOrderedList' data-akn-modification='true' data-akn-num='1.º' ><br></li></ol>");
	    } else {
	    	isContinue = false;
	    	context.event.cancel();
	    }
		 if (isContinue) {
	    var select = jqEditor.find(POINT_SELECTOR);
	    context.event.editor.getSelection().selectElement(new CKEDITOR.dom.node(select.find(">br")[0]));
	    context.event.editor.fire("change");
		context.event.cancel(); 
		}
    }
    
    function getQuotedStructure(element) {
        var parent = element.$;
        var i = 0;
        do {
            if (parent.localName === QUOTEDSTRUCTURE) {
            	return true;
            }
            parent = parent.parentElement;
            i += 1;
        } while (i < 5);

        return false;
    }



    function _onEnterKey(context) {
	    var selection = context.event.editor.getSelection();
	    var startElement = leosKeyHandler.getSelectedElement(selection)
	
	    if (leosKeyHandler.isContentEmptyTextNode(startElement) && isFirstLevelLiSelected(context)) {
	    	context.event.cancel();
	    }
	
	    if (getQuotedStructure(startElement)) {
	    	_onTabKeyPushEnter(context);
	    }
    }
    
    function isFirstLevelLi(liElement) {
        return !liElement.getAscendant('li');
    }
    
    function getClosestLiElement(element) {
        return element.getAscendant('li', true);
    }

    function isFirstLevelLiSelected(context) {
        var liElement = getClosestLiElement(context.firstRange.startContainer);
        return liElement && isFirstLevelLi(liElement);
    }


    pluginTools.addPlugin(pluginName, pluginDefinition);

    var leosHierarchicalElementTransformer = leosHierarchicalElementTransformerStamp({
        firstLevelConfig: {
            akn: 'point',
            html: 'ol[data-akn-name=aknPointMandateOrderedList]',
            attr: [{
                html: "data-akn-name=aknPointMandateOrderedList"
            }]
        },
        rootElementsForFrom: ["point"],
        contentWrapperForFrom: "alinea",
        rootElementsForTo: ["ol", "li"]
    });

    var transformationConfig = leosHierarchicalElementTransformer.getTransformationConfig();

    // return plugin module
    var pluginModule = {
        name: pluginName,
        transformationConfig: transformationConfig
    };

    pluginTools.addTransformationConfigForPlugin(transformationConfig, pluginName);

    return pluginModule;
});
