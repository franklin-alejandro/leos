﻿
; // jshint ignore:line
// //Evolutivo #1205
define(function leosRegulatoryTextPluginModule(require) {
    "use strict";

    // load module dependencies
    var pluginTools = require("plugins/pluginTools");
    var CKEDITOR = require("promise!ckEditor");
    var $ = require('jquery');
    var LOG = require("logger");
    var leosHierarchicalElementTransformerStamp = require("plugins/leosHierarchicalElementTransformer/hierarchicalElementTransformer");
    var leosPluginUtils = require("plugins/leosPluginUtils");  
    var leosKeyHandler = require("plugins/leosKeyHandler/leosKeyHandler");
    var ORDER_LIST_ELEMENT = "ol";
    var TEMPLATE_HTML_LI = "<li data-akn-num='1.'><br></li>";
    var HTML_LIST = "li";
    var HTML_P = "p";
    var HTML_RULE = "rule";
    var HTML_RULE_M = "RULE";
    var typeLetter = "ORDINAL_LETTER";
    var iconRegulatoryText = 'regulatorytext';
    var ENTER_KEY = 13;
    var SHIFT_ENTER = CKEDITOR.SHIFT + ENTER_KEY;  
    var TAB_KEY = 9;
    //AGE-EXT-3.0.0
    var pluginName = "AGEleosRegulatoryText";

    var pluginDefinition = {
    	icons: iconRegulatoryText,
    	init: function( editor ) {
    		leosKeyHandler.on({
                editor : editor,
                eventType : 'key',
                key : SHIFT_ENTER,
                action : _onShiftEnterKey
            });
    		leosKeyHandler.on({
                editor : editor,
                eventType : 'key',
                key : ENTER_KEY,
                action : _onEnterKey
            });
            editor.addCommand( 'insertText', {
                exec: function( editor ) {
                	_insertText(editor);
                }
            });
            editor.ui.addButton( 'leosRegulatoryText', {
            	label: 'Texto de Regulación',
                title: 'Texto de Regulación',
                icon: iconRegulatoryText,
                command: 'insertText',
                toolbar: 'mode,100'
            });
        }
    };
    
    function rand_code(){
    	var caracteres = "0123456789abcdefghijklmnñopqrstuvwxyz";
    	var lon = 9;
    	var code = "";
    	var rand;
    	for (var x=0; x < lon; x++)
    	{
    		rand = Math.floor(Math.random()*caracteres.length);
    		code += caracteres.substr(rand, 1);
    	}
    	return code;
    }

    
    function _onEnterKey(context) {
    	var selection = context.event.editor.getSelection();
    	var startElement = leosKeyHandler.getSelectedElement(selection);
    	var currentElement = startElement.$;
    	var jqEditor = $(context.event.editor.editable().$);
    	var orderedLists = jqEditor.find(ORDER_LIST_ELEMENT);
    	var alphaRandom = rand_code();
    	try {
    		if (currentElement.parentNode.localName.indexOf(HTML_RULE)!== -1) { 
    			LOG.debug("ENTER event intercepted: new paragraph", context.event);
	    		currentElement.parentNode.parentNode.insertAdjacentHTML('afterend',"<li data-akn-num='1.' id='art_parag_" + alphaRandom +"'><br></li>");
    			var range = context.event.editor.createRange();
    			range.setStart( context.event.editor.document.getById( 'art_parag_' + alphaRandom ), 0 );
    			range.setEnd( context.event.editor.document.getById( 'art_parag_' + alphaRandom ).getFirst(), 0 );
    			context.event.editor.getSelection().selectRanges( [ range ] );
    			context.event.editor.fire("change");
    			context.event.cancel(); 
    			
    		}else if (currentElement.localName.indexOf(HTML_LIST) !== -1 &&  (currentElement.lastChild.nodeName.indexOf(HTML_RULE_M) !== -1
    				|| currentElement.nextSibling.localName.indexOf(HTML_RULE) !== -1)){
    			var ranges = selection.getRanges();
    			var range = ranges[0];
    			var textAll = currentElement.innerText;
    			var textRange = range.endContainer.$.wholeText;
    			var startSelect = range.startOffset;
    			var endSelect = range.endOffset;
    			var textNew = "";
    			var textOld = "";
    			if (textRange !== undefined && textRange.length > 0) {
    				if(textRange.length == startSelect){
    					textNew = textRange;
    					currentElement.firstChild.nodeValue = "";
    					var bookmarks = context.event.editor.getSelection().createBookmarks();    	
    					currentElement.insertAdjacentHTML('beforebegin',"<li data-akn-num='1.' id='art_parag_" + alphaRandom +"'>" + textNew + " <br></li>");
    					context.event.editor.getSelection().selectBookmarks( bookmarks );
					    context.event.editor.fire("change");
					    context.event.cancel();
    				}else{
    					if (startSelect === endSelect){
    						textNew = textRange.trim().substring(startSelect); 
    						textOld = textRange.trim().substring(0,startSelect); 
    						currentElement.firstChild.nodeValue = textNew;
    					} else {
    						textNew = currentElement.innerText.substring(startSelect, endSelect); 
    					}
    					if(textNew === undefined && textNew.length == 0){
    						currentElement.insertAdjacentHTML('afterend',TEMPLATE_HTML_LI); 
    						context.editor.fire("change"); 
    						context.event.cancel();
    						context.event.editor.getSelection().selectElement(new CKEDITOR.dom.node(orderedLists.find(">ol>li>br")[0]));
    					} else {  
    						var bookmarks = context.event.editor.getSelection().createBookmarks();    					
    						currentElement.insertAdjacentHTML('beforebegin',"<li data-akn-num='1.' id='art_parag_"  + alphaRandom +"'>" + textOld + " <br></li>");
    						context.event.editor.getSelection().selectBookmarks( bookmarks );
    					    context.event.editor.fire("change");
    					    context.event.cancel();
    					}
    				}
    			}
    		}else if (currentElement.localName.indexOf(HTML_P) !== -1 && (currentElement.parentNode.lastChild.nodeName.indexOf(HTML_RULE_M) !== -1 
    				|| currentElement.nextSibling.localName.indexOf(HTML_RULE) !== -1)) {
    			var ranges = selection.getRanges();
    			var range = ranges[0];
    			var textAll = currentElement.innerText;
    			var textRange = range.endContainer.$.wholeText;
    			var startSelect = range.startOffset;
    			var endSelect = range.endOffset;
    			var textNew = "";
    			var textOld = "";
    			if (textRange !== undefined && textRange.length > 0) {
    				if(textRange.length == startSelect){
    					textNew = textRange;
    					currentElement.innerText = "";
    					var bookmarks = context.event.editor.getSelection().createBookmarks();
    					currentElement.parentNode.insertAdjacentHTML('beforebegin',"<li data-akn-num='1.' id='art_parag_" + alphaRandom +"'>" + textNew + " <br></li>");
    					context.event.editor.getSelection().selectBookmarks( bookmarks );
    					context.event.editor.fire("change");
    					context.event.cancel();
    				}else{
    					if (startSelect === endSelect){
    						textNew = textRange.trim().substring(startSelect); 
    						textOld = textRange.trim().substring(0,startSelect); 
    						currentElement.innerText = textNew;	
    					} else {
    						textNew = currentElement.innerText.substring(startSelect, endSelect); 
    					}
    					if(textNew === undefined && textNew.length == 0){
    						currentElement.parentNode.insertAdjacentHTML('beforebegin',TEMPLATE_HTML_LI); 
    						context.editor.fire("change"); 
    						context.event.cancel();
    						context.event.editor.getSelection().selectElement(new CKEDITOR.dom.node(orderedLists.find(">ol>li>br")[0]));
    					} else {
    						var bookmarks = context.event.editor.getSelection().createBookmarks();
    						currentElement.parentNode.insertAdjacentHTML('beforebegin',"<li data-akn-num='1.' id='art_parag_" + alphaRandom +"'>" + textOld + " <br></li>");
    						context.event.editor.getSelection().selectBookmarks( bookmarks );
    						context.event.editor.fire("change");
    						context.event.cancel();
    					}
    				}
    			} else {
					currentElement.innerText = "";
					var bookmarks = context.event.editor.getSelection().createBookmarks();
					currentElement.parentNode.insertAdjacentHTML('beforebegin',"<li data-akn-num='1.' id='art_parag_" + alphaRandom +"'>"+textAll+"<br></li>");
					context.event.editor.getSelection().selectBookmarks( bookmarks );
					context.event.editor.fire("change");
					context.event.cancel();
    			}
    		}
    	}catch (error) {
    		LOG.debug("ENTER event intercepted: no rule", context.event);
    	}
    }
    
    function _onShiftEnterKey(context) {
    	LOG.debug("SHIFT ENTER event intercepted: ", context.event);
    	var element = context.editor.getSelection().getStartElement();
    	var selection = context.editor.getSelection();
    	var startElement = leosKeyHandler.getSelectedElement(selection);
    	var currentElement = startElement.$;
    	
    	if (!leosKeyHandler.isContentEmptyTextNode(startElement)) {
    		if ( (leosPluginUtils.getElementName(currentElement) === HTML_P && leosPluginUtils.getElementName(currentElement.parentNode) === HTML_RULE )
    				|| (leosPluginUtils.getElementName(currentElement) === HTML_LIST && leosPluginUtils.getElementName(currentElement.lastChild) === HTML_RULE_M)){
    			_insertText(context.event.editor);
    			context.event.cancel();
    		}
    	}
    }
    
    function _insertText(editor) {
    	var selection = editor.getSelection();
    	var startElement = leosKeyHandler.getSelectedElement(selection);
    	var currentElement = startElement.$;
    	var alphaRandom = rand_code();
    	if (!leosKeyHandler.isContentEmptyTextNode(startElement)) {
    		if(leosPluginUtils.getElementName(currentElement) === HTML_LIST) {
    			if (currentElement.attributes["data-akn-name"]){
    				var aknName = currentElement.attributes["data-akn-name"].nodeValue; 
    				if (aknName.indexOf("aknOrderedList") === -1) {
    					try {
    						if (currentElement.parentNode.parentNode.attributes["data-akn-attr-mod"]){
    							var leosMod = currentElement.parentNode.parentNode.attributes["data-akn-attr-mod"].nodeValue;
    							if (leosMod.indexOf('true') !== -1 ){
    								currentElement.insertAdjacentHTML('beforeend','<rule id=""'
    										+ 'data-akn-content-id="" data-akn-name="aknRule" data-akn-mp-id=""><p id="art_rule_' + alphaRandom +'" data-akn-content-id="">'
    										+ '«»</p></rule>');
    								cleanRegulatory(editor);
    								var range = editor.createRange();
    								var textNew = currentElement.lastElementChild.firstChild.firstChild.data;
    								if (textNew.length == 1 && textNew.includes('»')) {
    									range.setStart( editor.document.getById( 'art_rule_' + alphaRandom ), 0 );
    									range.setEnd( editor.document.getById( 'art_rule_' + alphaRandom ).getFirst(), 0 );
    								} else if (textNew.length == 2) {
    									range.setStart( editor.document.getById( 'art_rule_' + alphaRandom ), 1 );
    									range.setEnd( editor.document.getById( 'art_rule_' + alphaRandom ).getFirst(), 1 );
    								}
    								editor.getSelection().selectRanges( [ range ] );

    							} else {
    								alert("No es un artículo o disposición modificativa, no contiene texto de regulación");
    								return;
    							}
    						} else {
    							alert("No es un artículo o disposición modificativa, no contiene texto de regulación");
    							return;
    						}
    					} catch(error) {
    						alert("No es un artículo o disposición modificativa, no contiene texto de regulación");
    						return;
    					}
    				}
    			} else {
    				if (leosPluginUtils.getElementName(currentElement.parentNode) === ORDER_LIST_ELEMENT) {
    					if (currentElement.attributes["data-akn-name"]){
    						try {
    							if (currentElement.nodeParent.attributes["data-akn-attr-mod"]) {
    								var leosMod = currentElement.nodeParent.attributes["data-akn-attr-mod"].nodeValue;
    								if (leosMod.indexOf('true') !== -1 ){
    									currentElement.insertAdjacentHTML('beforeend','<rule id=""'
    											+ 'data-akn-content-id="" data-akn-name="aknRule" data-akn-mp-id=""><p data-akn-content-id="">'
    											+ '«»</p></rule>');
    									cleanRegulatory(editor);
    									editor.getSelection().selectElement(new CKEDITOR.dom.node(currentElement));
    								} else {
    									alert("No es un artículo o disposición modificativa, no contiene texto de regulación");
    									return;
    								}
    							}  else {
    								alert("No es un artículo o disposición modificativa, no contiene texto de regulación");
    								return;
    							}
    						} catch(error) {
    							alert("No es un artículo o disposición modificativa, no contiene texto de regulación");
    							return;
    						}
    					} else {
    						return;
    					}
    				}
    			}
    		} else if (leosPluginUtils.getElementName(currentElement) === HTML_P && leosPluginUtils.getElementName(currentElement.parentNode) === HTML_RULE) {
    			try {  				
    				if (currentElement.parentNode.parentNode.parentNode.parentNode.attributes["data-akn-attr-mod"]) {
    					var leosMod =  currentElement.parentNode.parentNode.parentNode.parentNode.attributes["data-akn-attr-mod"].nodeValue;
    					if (leosMod.indexOf('true') !== -1 ){
    						var ranges = selection.getRanges();
    						var range = ranges[0];
    						var textAll = currentElement.innerText;
    						var textRange = range.endContainer.$.wholeText;
    						var startSelect = range.startOffset;
    						var endSelect = range.endOffset;
    						var textNew = "";
    						var textOld = "";
    						if (textRange !== undefined && textRange.length > 0) {
    							if (startSelect === endSelect){
    								if (cleanCharacter(textAll).trim() == cleanCharacter(textRange).trim()){
    									textNew =textRange.trim().substring(startSelect); 
    									textOld =textRange.trim().substring(0,startSelect);  
    									if (textOld !== undefined && textOld.length > 0){
    										currentElement.innerText = textOld;	
    									}else {
    										currentElement.innerText = "t";	    
    									}
    								} else {
    									textNew = textRange;
    									textOld = textAll.replace(textNew, "");
    									currentElement.innerText = textOld;	
    								}
    							} else {
    								textNew = currentElement.innerText.substring(startSelect, endSelect); 
    							}
    							if(textNew.length == 0){
    								currentElement.parentNode.insertAdjacentHTML('afterend','<rule id=""'
    										+ 'data-akn-content-id="" data-akn-name="aknRule" data-akn-mp-id=""><p id="art_rule_' + alphaRandom +'" data-akn-content-id="">'
    										+'t</p></rule>');
    								cleanRegulatory(editor);
    								var range = editor.createRange();
    								range.setStart( editor.document.getById( 'art_rule_' + alphaRandom ), 0 );
    								range.setEnd( editor.document.getById( 'art_rule_' + alphaRandom ).getFirst(), 1 );
    								editor.getSelection().selectRanges( [ range ] );
    							} else {
    								currentElement.parentNode.insertAdjacentHTML('afterend','<rule id=""'
    										+ 'data-akn-content-id="" data-akn-name="aknRule" data-akn-mp-id=""><p id="art_rule_' + alphaRandom +'" data-akn-content-id="">'
    										+ textNew + '</p></rule>');
    								cleanRegulatory(editor);
    								var range = editor.createRange();
    								range.setStart( editor.document.getById( 'art_rule_' + alphaRandom ), 0 );
    								range.setEnd( editor.document.getById( 'art_rule_' + alphaRandom ).getFirst(), 0 );
    								editor.getSelection().selectRanges( [ range ] );
    							}
    						} else {
    							currentElement.parentNode.insertAdjacentHTML('afterend','<rule id=""'
    									+ 'data-akn-content-id="" data-akn-name="aknRule" data-akn-mp-id=""><p id="art_rule_' + alphaRandom +'" data-akn-content-id="">'
    									+ 't</p></rule>');
    							cleanRegulatory(editor);
    							var range = editor.createRange();
    							range.setStart( editor.document.getById( 'art_rule_' + alphaRandom ), 0 );
    							range.setEnd( editor.document.getById( 'art_rule_' + alphaRandom ).getFirst(), 1 );
    							editor.getSelection().selectRanges( [ range ] );
    						}
    					} else {
    						alert("No es un artículo o disposición modificativa, no contiene texto de regulación");
    						return;
    					}
    				} else {
						alert("No es un artículo o disposición modificativa, no contiene texto de regulación");
						return;
					}
    			}catch(error) {
    				if (currentElement.parentNode.parentNode.parentNode.parentNode.parentNode.attributes["data-akn-attr-mod"]) {
    					var leosMod =  currentElement.parentNode.parentNode.parentNode.parentNode.parentNode.attributes["data-akn-attr-mod"].nodeValue;
    					if (leosMod.indexOf('true') !== -1 ){
    						currentElement.parentNode.parentNode.insertAdjacentHTML('beforeend','<rule id=""'
    								+ 'data-akn-content-id="" data-akn-name="aknRule" data-akn-mp-id=""><p data-akn-content-id="">'
    								+ '«»</p></rule>');
    						cleanRegulatory(editor);
    						editor.getSelection().selectElement(new CKEDITOR.dom.node(currentElement));
    					} else {
    						alert("No es un artículo o disposición modificativa, no contiene texto de regulación");
    						return;
    					}
    				}	
    			} 
    		}else if (leosPluginUtils.getElementName(currentElement) === HTML_P && leosPluginUtils.getElementName(currentElement.parentNode) === HTML_LIST) {
    			try {
    				if (currentElement.parentNode.parentNode.parentNode.parentNode.attributes["data-akn-attr-mod"]) {
    					var leosMod =  currentElement.parentNode.parentNode.parentNode.parentNode.attributes["data-akn-attr-mod"].nodeValue;
    					if (leosMod.indexOf('true') !== -1 ){
    						currentElement.insertAdjacentHTML('afterend','<rule id=""'
        							+ 'data-akn-content-id="" data-akn-name="aknRule" data-akn-mp-id=""><p id="art_rule_' + alphaRandom +'" data-akn-content-id="">'
        							+ '«»</p></rule>');
        					var range = editor.createRange();
        					cleanRegulatory(editor);
        					range.setStart( editor.document.getById( 'art_rule_' + alphaRandom ), 1 );
        					range.setEnd( editor.document.getById( 'art_rule_' + alphaRandom ).getFirst(), 1 );
        					editor.getSelection().selectRanges( [ range ] );
    					} else {
    						alert("No es un artículo o disposición modificativa, no contiene texto de regulación");
        					return;
    					}
    				} else {
    					alert("No es un artículo o disposición modificativa, no contiene texto de regulación");
    					return;
    				}
    			}catch(error) {
    				var leosMod = currentElement.parentNode.parentNode.parentNode.parentNode.parentNode.attributes["data-akn-attr-mod"].nodeValue;
    				if (leosMod.indexOf('true') !== -1 ){
    					currentElement.parentNode.parentNode.insertAdjacentHTML('beforeend','<rule id=""'
    							+ 'data-akn-content-id="" data-akn-name="aknRule" data-akn-mp-id=""><p data-akn-content-id="">'
    							+ '«»</p></rule>');
    					cleanRegulatory(editor);
    					editor.getSelection().selectElement(new CKEDITOR.dom.node(currentElement));
    					leosPluginUtils.setFocus(startElement, editor);
    				} else {
    					alert("No es un artículo o disposición modificativa, no contiene texto de regulación");
    					return;
    				}
    			}	
    		}
    	}
    }
    
    function cleanCharacter(text) {
    	if (text !== undefined && text.length > 0) {
    		if (text.substring(text.trim().length - 1).indexOf("»") !== -1){
        		text=text.trim().slice(0, -1);
    		}
    		if (text.trim().substring(0, 1).indexOf("«") !== -1){
    			text=text.trim().slice(1);
    		}
    		return text;
    	} else {
    		return "";
    	}  
    }
    
    function cleanRegulatory (editor) {
    	var selection = editor.getSelection();
    	var startElement = leosKeyHandler.getSelectedElement(selection);
    	var currentElement = startElement.$;
    	var childrens;
    	if (leosPluginUtils.getElementName(currentElement) === HTML_P && leosPluginUtils.getElementName(currentElement.parentNode) === HTML_LIST){
    		childrens = currentElement.parentNode.childNodes;
		} else if (leosPluginUtils.getElementName(currentElement) === HTML_P && leosPluginUtils.getElementName(currentElement.parentNode) === HTML_RULE){
			childrens = currentElement.parentNode.parentNode.childNodes;	
    	} else if (leosPluginUtils.getElementName(currentElement)  === HTML_RULE){
    		childrens = currentElement.parentNode.childNodes;
    	} else if (leosPluginUtils.getElementName(currentElement)  === HTML_LIST){
    		childrens = currentElement.childNodes;
    	}
    	var rulesArray= new Array();
    	var numRules = 0;
    	for (var jj = 0; jj < childrens.length; jj++) {
    		if(childrens[jj].nodeName.indexOf(HTML_RULE_M) !== -1 ){ 
    			rulesArray[numRules]=childrens[jj];
    			numRules ++;
    		}
    	}
    	for (var jj = 0; jj < rulesArray.length; jj++) {
    		// caso primer RULE SI « NO »
    		if (0 == jj && jj < rulesArray.length - 1) {
    			if (rulesArray[jj].innerText.trim().substring(0, 1).indexOf("«") === -1) {
    				rulesArray[jj].firstChild.innerText = '&laquo;' +  rulesArray[jj].innerText.trim();
    			} 
    			if (rulesArray[jj].innerText.trim().substring(rulesArray[jj].innerText.trim().length - 1).indexOf("»") !== -1) {
    				rulesArray[jj].firstChild.innerText = rulesArray[jj].innerText.trim().slice(0, -1);
    			}
    			// caso RULE último NO « SI »
    		} else if (0 < jj && jj == rulesArray.length - 1) {
    			if (rulesArray[jj].innerText.trim().substring(0, 1).indexOf("«") !== -1) {
    				rulesArray[jj].firstChild.innerText = rulesArray[jj].innerText.trim().slice(1);
    			}
    			if (rulesArray[jj].innerText.trim().substring(rulesArray[jj].innerText.trim().length - 1).indexOf("»") === -1) {
    				rulesArray[jj].firstChild.innerText = rulesArray[jj].innerText.trim() + '»';
    			}
    			// caso RULE entre medias NO « NO »
    		} else if (0 < jj && jj < rulesArray.length - 1){
    			if (rulesArray[jj].innerText.trim().substring(rulesArray[jj].innerText.trim().length - 1).indexOf("»") !== -1) {
    				rulesArray[jj].firstChild.innerText = rulesArray[jj].innerText.trim().slice(0, -1);
    			}
    			if (rulesArray[jj].innerText.trim().substring(0, 1).indexOf("«") !== -1) {
    				rulesArray[jj].firstChild.innerText = rulesArray[jj].innerText.trim().slice(1);
    			}
    			// caso primer y el sólo RULE SI « SI »
    		} else if (0 == jj && rulesArray.length - 1 == 0) {
    			if (rulesArray[jj].innerText.trim().substring(0, 1).indexOf("«") === -1) {
    				rulesArray[jj].firstChild.innerText = '&laquo;' +  rulesArray[jj].innerText.trim();
    			} 
    			if (rulesArray[jj].innerText.trim().substring(rulesArray[jj].innerText.trim().length - 1).indexOf("»") === -1) {
    				rulesArray[jj].firstChild.innerText = rulesArray[jj].innerText.trim(); + '»';
    			}
    		}	
    	}
    }   
    
    var transformationConfig = {
            akn :  "rule",
            html : "rule",
            attr : [{
                html : "data-akn-name=aknRule"
            }, {
                akn : "xml:id",
                html : "id" 
            }, {
                akn : "leos:origin",
                html : "data-origin"
            }],
            sub: {
                akn: "content",
                html: "rule/p",
                attr: [{
                    akn:"xml:id",
                    html:"data-akn-content-id"
                }, {
                    akn : "leos:origin",
                    html : "data-content-origin"
                }],
                sub: {
                    akn: "mp",
                    html: "rule/p",
                    attr: [{
                        akn:"xml:id",
                        html:"data-akn-mp-id"
                    }, {
                        akn : "leos:origin",
                        html : "data-mp-origin"
                    }],
                    sub: {
                        akn: "text",
                        html: "rule/p/text"
                    }
                }
            }
        };

	    pluginTools.addTransformationConfigForPlugin(transformationConfig, pluginName);

	    pluginTools.addPlugin(pluginName, pluginDefinition);

	    // return plugin module
	    var pluginModule = {
	        name : pluginName
	    };
	    return pluginModule;
});