/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
CKEDITOR.plugins.setLang( 'AGEleosAmendAct', 'en', {
	warningTitle: 'Activate amendment',
	messageConfirm: 'Are you sure you want to turn this article into an amending article? (all actual content will be cleared)',
	messageTransforArticleModify: 'Modifications should go in provisions and not in articles. Do you want to continue?'
} );