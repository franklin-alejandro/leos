/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
CKEDITOR.plugins.setLang( 'AGEleosAmendAct', 'es', {
	warningTitle: 'Activar enmienda',
	messageConfirm: '¿Está seguro de que desea convertir este artículo en un artículo de modificación? (se borrará todo el contenido actual)',
	messageTransforArticleModify: 'Las modificaciones deberían ir en disposiciones y no en artículos ¿desea continuar?'
} );