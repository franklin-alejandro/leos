/*
 * Copyright 2020 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
; // jshint ignore:line
define(function importAmendingActExtensionModule(require) {
    "use strict";
    //var leosKeyHandler = require("plugins/leosKeyHandler/leosKeyHandler");
    var $ = require('jquery');
    var log = require("logger");
    var UTILS = require("core/leosUtils");
    var pluginName = 'AGEleosAmendAct';
    var pluginTools = require("plugins/pluginTools");
    var cssPath = pluginTools.getResourceUrl(pluginName, "css/AGEleosAmendAct.css");

    var SELECTABLE_ELEMENTS = "article,proviso,heading,paragraph,point,list,indent";
    var selectedElements = new Set();
    var ARTICLE = "article";
    var PROVISO = "proviso";
    var HEADING = "heading";
    var PARAGRAPH = "paragraph";
    var LIST = "list";
    var POINT = "point";
    var NUM = "num";

    function _initImportAmendingActExtension(connector) {
        connector.handleAmendingActEvents = _handleAmendingActEvents;
        connector.rootElement = UTILS.getParentElement(connector);
        connector.onStateChange = _doSearch;
    }
    
    function _doSearch() {
        var connector = this;

        var data = {
        };
        connector.doSearch(data);
    }

    function _handleAmendingActEvents() {
        log.debug("After amendment search result, attach click events...");
        var connector = this;
        var rootElement = connector.rootElement;
        $(rootElement).find(SELECTABLE_ELEMENTS).addClass("selectable-element");
        $(rootElement).find("paragraph").addClass("selectable-element-paragraph");
        $(rootElement).find(SELECTABLE_ELEMENTS).click(function (event) {
            var $this = $(this);
            // event.ctrlKey ? handleMultiSelection($this, editor) : handleSingleSelection($this, editor);
            handleSingleSelection(rootElement, $this, connector);
            event.stopPropagation();
        });
        appendPluginCss(cssPath);
    }

    function handleSingleSelection(rootElement, $element, connector) {
        if ($element.hasClass("selectable-element-selected")) {
            $(rootElement).find(SELECTABLE_ELEMENTS).removeClass("selectable-element-selected");
            $element.removeClass("selectable-element-selected");
            deSelectElement($element, connector);
        } else {
            $(rootElement).find(SELECTABLE_ELEMENTS).removeClass("selectable-element-selected");
            selectedElements.clear();
            selectElement($element, connector);
        }
    }

//     function handleMultiSelection($element, editor) {
//         if ($element.hasClass("selectable-element-selected")) {
//             deSelectElement($element, editor);
//         } else {
// //		var $selectedElements = $contentContainer.find('.selectable-element-selected');
// //		result ? selectElement($element, editor)
//         }
//     }

    function deSelectElement($element, connector) {
        var ids = "";

        var type = $element.get(0).localName;
        switch (type) {
            case HEADING:
                ids = type + "--" + $element.get(0).outerText;
                break;
            case ARTICLE:
            case PROVISO:
                ids = $element.attr("id");
                break;
            case PARAGRAPH:
                parent = $element.get(0).parentElement;
                if (parent.attributes.id) {
                    ids = parent.attributes.id.value;
                }
                break;
            case LIST:
                parent = $element.get(0).parentElement.parentElement;
                if (parent.attributes.id) {
                    ids = parent.attributes.id.value;
                }
                break;
            case POINT:
                parent = $element.get(0).parentElement.parentElement.parentElement;
                if (parent.localName ==ARTICLE && parent.attributes.id){
        			ids += parent.attributes.id.value;
        		} else if (parent.parentElement.localName == ARTICLE && parent.parentElement.attributes.id) {
        			ids += parent.parentElement.attributes.id.value;
        		} else if (parent.parentElement.parentElement.localName == ARTICLE && parent.parentElement.parentElement.attributes.id) {
        			ids += parent.parentElement.parentElement.attributes.id.value;
        		}
                break;
        }

        for (let ele of selectedElements) {
            if (ele.includes(ids)) {
                selectedElements.delete(ele);
            }
        }
        var selectedElementsArray = Array.from(selectedElements);
        var data = {
            selectedElements: selectedElementsArray
        };
        
        connector.responseSelectedElementsAmend(data);
    }

    function selectElement($element, connector) {

    	var nums = "";
    	var ids = "";
    	var elements = "";
    	var parent = "";

    	var type = $element.get(0).localName;
    	switch (type) {
    	case HEADING:
    		elements += $element.get(0).outerHTML;
    		parent = $element.get(0).parentElement;
    		if (parent.attributes.id){
    			ids += parent.attributes.id.value;
    		}
    		nums += parent.firstChild.innerText;
    		var typeParent = parent.localName;
    		elements += "--" + typeParent;
    		break;
    	case ARTICLE:
    	case PROVISO:		  
    		ids += $element.attr("id");
    		elements += $element.get(0).outerHTML
    		nums += makeNum($element.get(0),type);
    		break;
    	case PARAGRAPH:
    	case LIST:
    	case POINT:
    		parent = getArticle($element.get(0));
    		if ((parent.localName == ARTICLE || parent.localName == PROVISO ) && parent.attributes.id){
    			ids += parent.attributes.id.value;
    		} 
    		
    		elements += $element.get(0).outerHTML;
    		nums += makeNum($element.get(0),type);
    		break;
    	} 

    	elements = elements.replaceAll("selectable-element-paragraph", "");
    	elements = elements.replaceAll("selectable-element", "");
    	selectedElements.add(type + "--" + ids + "--" + nums + "--" + elements );

    	var selectedElementsArray = Array.from(selectedElements);
    	parent = parent.outerHTML;
    	$element.addClass("selectable-element-selected");
    	var data = {
    			selectedElements: selectedElementsArray,
    			parentElements: parent
    	};
    	
    	connector.responseSelectedElementsAmend(data);
    }
    
    function getArticle(element) {
        var article = null;
        var parent = element;
        var i = 0;
        do {
            if (parent.localName === ARTICLE || parent.localName === PROVISO ) {
                article = parent;
                break;
            }
            parent = parent.parentElement;
            i += 1;
        } while (parent.localName === ARTICLE || parent.localName === PROVISO  || i < 50);

        return parent;
    }

	function makeNum(element,type) {
		var num = "";
		switch (type) {
		case ARTICLE:
		case PROVISO:
			num += element.firstChild.innerHTML
			break;
		case PARAGRAPH:  
		case LIST:
		case POINT:
			if (element.firstChild.localName == NUM){
				num += element.firstChild.innerHTML + "::";
			} else if (element.localName == PARAGRAPH && element.firstChild.localName != NUM) {
				num += " primero.::";
			}
			num += makeNum (element.parentElement, element.parentElement.localName);
			break;
		}
		return num;
	}

    function appendPluginCss(css) {
        $('<link>').appendTo('head').attr({
            type: 'text/css',
            rel: 'stylesheet',
            href: css
        });
    }

    return {
        init : _initImportAmendingActExtension
    };
});