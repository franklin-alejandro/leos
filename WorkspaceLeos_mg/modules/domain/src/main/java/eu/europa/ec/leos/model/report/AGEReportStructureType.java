package eu.europa.ec.leos.model.report;

public enum AGEReportStructureType {
	
    ARTICLE("article"),
    //DIVISION("division"),
    LEVEL("level");
    
    private String type;

    AGEReportStructureType(String type) {
        this.type = type;
    }
    
    public String getType() {
        return type;
    }
    
}