package eu.europa.ec.leos.vo.toctype.numbertype;

public interface AGENumberType {
	
	String getTitle();
	
}
