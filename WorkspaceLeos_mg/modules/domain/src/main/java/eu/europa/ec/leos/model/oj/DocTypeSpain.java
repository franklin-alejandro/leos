package eu.europa.ec.leos.model.oj;

import java.util.HashMap;
import java.util.Map;

//AGE-CORE-3.0.0
public enum DocTypeSpain {
	
	ACUERDO("a"),
	CONSTITUCION("c"),
    DECRETO("d"),
    DECRETO_FORAL("df"),
    DECRETO_FORAL_LEGISLATIVO("dflg"),
    DECRETO_LEGISLATIVO("dlg"),
    DECRETO_LEY("dl"),
    DECRETO_LEY_FORAL("dlf"),
    LEY("l"),
    LEY_FORAL("lf"),
    LEY_ORGANICA("lo"),
    ORDEN("o"),
    ORDEN_FORAL("of"),
    REAL_DECRETO("rd"),
    REAL_DECRETO_LEY("rdl"),
    REAL_DECRETO_LEGISLATIVO("rdlg");

    private String type;

    DocTypeSpain(String type) {
        this.type = type;
    }

    public String getValue() {
        return type;
    }

    private static final Map<String, DocTypeSpain> map = new HashMap<>(values().length, 1);

    static {
        for (DocTypeSpain c : values()){
            map.put(c.type, c);
        }
    }

    public static DocTypeSpain ofType(String name) {
    	DocTypeSpain result = map.get(name);
        if (result == null) {
            throw new IllegalArgumentException("Invalid type name: " + name);
        }
        return result;
    }
}

