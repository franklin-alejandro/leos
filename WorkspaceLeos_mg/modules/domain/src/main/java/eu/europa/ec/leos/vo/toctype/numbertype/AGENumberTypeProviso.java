package eu.europa.ec.leos.vo.toctype.numbertype;

public enum AGENumberTypeProviso implements AGENumberType {

	ADITIONAL("aditional"), TRANSIENT("transient"), DEROGATORY("derogatory"), FINAL("final");
	protected String title;

	AGENumberTypeProviso(String title) {
		this.title = title;
	}

	@Override
	public String getTitle() {
		return title;
	}
	
}
