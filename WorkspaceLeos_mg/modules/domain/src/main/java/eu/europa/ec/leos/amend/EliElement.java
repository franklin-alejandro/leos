package eu.europa.ec.leos.amend;

import java.util.HashMap;
import java.util.Map;

public enum EliElement {
    ARTICLE("art"),
    CHAPTER("cpt"),
    CITATION("cit"),
    CLAUSE("cls"),
    ENACTING_TERMS("enc"),
    FOOTNOTE("ftn"),
    INDENT("idt"),
    PARAGRAPH("par"),
    PART("prt"),
    POINT("pnt"),
    PREAMBLE("pbl"),
    RECITAL("rct"),
    SECTION("sec"),
    SUBJECT("tit"),
    SUBPARAGRAPH("sub"),
    SUBSECTION("sbs"),
    TABLE("tab"),
    TITLE("tis"),
    HEADING("heading") // non present in ELI
    ;

    private final String value;

    EliElement(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    private static final Map<String, EliElement> map = new HashMap<>(values().length, 1);
    static {
        for (EliElement c : values()){
            map.put(c.value, c);
        }
    }

    public static EliElement ofType(String name) {
        EliElement result = map.get(name);
        if (result == null) {
            throw new IllegalArgumentException("Invalid type name: " + name);
        }
        return result;
    }
}