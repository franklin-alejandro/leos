/*
 * Copyright 2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.model.oj;

import java.util.HashMap;
import java.util.Map;

public enum DocType {
    REGULATION("reg"),
    DIRECTIVE("dir"),
    DECISION("dec");

    private String type;

    DocType(String type) {
        this.type = type;
    }

    public String getValue() {
        return type;
    }

    private static final Map<String, DocType> map = new HashMap<>(values().length, 1);

    static {
        for (DocType c : values()){
            map.put(c.type, c);
        }
    }

    public static DocType ofType(String name) {
        DocType result = map.get(name);
        if (result == null) {
            throw new IllegalArgumentException("Invalid type name: " + name);
        }
        return result;
    }
}
