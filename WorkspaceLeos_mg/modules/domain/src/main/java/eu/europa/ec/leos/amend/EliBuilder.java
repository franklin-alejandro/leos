package eu.europa.ec.leos.amend;

import eu.europa.ec.leos.model.oj.DocType;
import java.util.LinkedHashMap;
import java.util.Map;

public class EliBuilder {

    public final static String url = "http://data.euorpa.eu/eli/";

    private DocType actType;
    private int actYear;
    private int actNumber;

    public static void getBuilder() {
    }

    public static Map<EliElement, String> parseEliIdToMap(String eId) {
        // parse: art_3.par_1.sub_2
        Map<EliElement, String> map = new LinkedHashMap<>();
        String [] arrElements = eId.split("\\.");
        for (String el : arrElements) {
            String [] arrEl = el.split("_");
            map.put(EliElement.ofType(arrEl[0]), arrEl[1]);
        }
        return map;
    }

    public static String getFromEliIdMap(Map<EliElement, String> eliIdMap, EliElement elementName) {
        return eliIdMap.get(elementName);
    }

    public String getElementEliReference() {
        return url + "/" + actType + "/" + actYear + "/" + actNumber; // + art_6/par_2/pnt_a
    }

}
