package eu.europa.ec.leos.amend;

import org.apache.commons.lang3.Validate;

import eu.europa.ec.leos.model.oj.DocTypeSpain;

public class AmendActDetails {
    private DocTypeSpain type;//AGE-CORE-3.0.0
    private int year;
    private int month;
    private int day;
    private int number;

    private String patternLabel = "In Article 25 of Regulation (EU) No 1303/2013, paragraph 1 is replaced by the following:"; //build from the service AmendPatternLabelService

    private static String ELI_URL = "http://data.europa.eu/eli";
    private static String HEADING_LABEL = "Amendment to regulation (EU) No ";
    private static String AFFECTED_DOCUMENT_LABEL = "Regulation (EU) No ";
    public static String IS_AMENDED_LABEL = "is amended as follows: ";

    public static String START_QUOTE = "\n\"";
    public static String END_QUOTE = "\";";

    public AmendActDetails(String typeName, int year, int month, int day, int number) {
        this.type = DocTypeSpain.ofType(typeName);
        this.year = year;
        this.number = number;//AGE-EXT-3.0.0
        this.month = month;
        this.day = day;
    }
    
    public static AmendActDetails parseDetails(String href) {
        Validate.isTrue(href != null, "Eli's reference should be in the format http://data.europa.eu/eli/reg/1303/2013");
        String[] arr = href.split("/");
        Validate.isTrue(arr.length > 3 , "Eli's reference should be in the format http://data.europa.eu/eli/reg/1303/2013");

        return new AmendActDetails(arr[arr.length - 5], Integer.parseInt(arr[arr.length - 4]), Integer.parseInt(arr[arr.length - 3]), Integer.parseInt(arr[arr.length - 2]),  Integer.parseInt(arr[arr.length - 1]) );
    }

    public DocTypeSpain getType() {
        return type;
    }

    public int getYear() {
        return year;
    }

    public int getNumber() {
        return number;
    }
    
    public int getMonth() {
		return month;
	}

	public int getDay() {
		return day;
	}

	public String getPatternLabel() {
        return patternLabel;
    }

    public void setPatternLabel(String patternLabel) {
        this.patternLabel = patternLabel;
    }

    // Regulation (EU) No 1303/2013
    public String getAffectedDocumentRefLabel() {
        return AFFECTED_DOCUMENT_LABEL + number + "/" + year;
    }

    //  Amendment to regulation (EU) No 1303/2013
    public String getHeading() {
        return HEADING_LABEL + number + "/" + year;
    }

    // http://data.europa.eu/eli/reg/2013/1303
    public String getEliDestination() {
        return ELI_URL + "/" + type.getValue() + "/" + year + "/" + number;
    }
}