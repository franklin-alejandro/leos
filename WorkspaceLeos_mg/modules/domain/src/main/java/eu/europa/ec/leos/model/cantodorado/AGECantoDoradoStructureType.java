package eu.europa.ec.leos.model.cantodorado;

public enum AGECantoDoradoStructureType {
	
    ARTICLE("article"),
    PROVISO("proviso");
   // LEVEL("level");
    
    private String type;

    AGECantoDoradoStructureType(String type) {
        this.type = type;
    }
    
    public String getType() {
        return type;
    }
    
}