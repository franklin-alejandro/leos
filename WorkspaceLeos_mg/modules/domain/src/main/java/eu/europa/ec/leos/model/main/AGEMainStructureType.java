package eu.europa.ec.leos.model.main;

public enum AGEMainStructureType {
	
    ARTICLE("article"),
    //DIVISION("division"),
    LEVEL("level");
    
    private String type;

    AGEMainStructureType(String type) {
        this.type = type;
    }
    
    public String getType() {
        return type;
    }
    
}