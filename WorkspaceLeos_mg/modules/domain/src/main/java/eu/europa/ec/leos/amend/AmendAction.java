package eu.europa.ec.leos.amend;

public enum AmendAction {

	INSERT_TOP("insertion", "before"),
	INSERT_BOTTOM("insertion", "after"),
	REPLACE("substitution", null),
	DELETE("repeal", null);
	
	private String operation;
	private String position;

	AmendAction(String operation, String position) {
		this.operation = operation;
		this.position = position;
	}

	public String getOperation(){
		return operation;
	}

	public String getPosition(){
		return position;
	}
}
