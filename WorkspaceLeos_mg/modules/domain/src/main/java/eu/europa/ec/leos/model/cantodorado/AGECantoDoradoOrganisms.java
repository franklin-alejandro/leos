package eu.europa.ec.leos.model.cantodorado;

import java.util.HashMap;
import java.util.Map;


public enum AGECantoDoradoOrganisms {
	
	PRESIDENCIA("Ministerio de la Presidencia, Relaciones con las Cortes y Memoria Democrática::Carmen Calvo Poyato::La::Vicepresidenta Primera del Gobierno y Ministra de la Presidencia, Relaciones con las Cortes y Memoria Democrática"),
	DERECHOS_SOCIALES("Ministerio de Derechos Sociales y Agenda 2030::Pablo Iglesias Turrión::El::Vicepresidente Segundo del Gobierno y Ministro de Derechos Sociales y Agenda 2030"),
	ECONOMIA("Ministerio de Asuntos Económicos y Transformación Digital::Nadia Calviño Santamaría::La::Vicepresidenta Tercera del Gobierno y Ministra de Asuntos Económicos y Transformación Digital"),
	TRANSICION_ECOLOGICA("Ministerio para la Transición Ecológica y el Reto Demográfico::Teresa Ribera Rodríguez::La::Vicepresidenta Cuarta del Gobierno y Ministra para la Transición Ecológica y el Reto Demográfico"),
	ASUNTOS_EXTERIORES("Ministerio de Asuntos Exteriores, Unión Europea y Cooperación::Arancha González Laya::La::Ministra de Asuntos Exteriores, Unión Europea y Cooperación"),
	JUSTICIA("Ministerio de Justicia::Juan Carlos Campo Moreno::El::Ministro de Justicia"),
	DEFENSA("Ministerio de Defensa::Margarita Robles Fernández::La::Ministra de Defensa"),
	HACIENDA("Ministerio de Hacienda::María Jesús Montero Cuadrado::La::Ministra de Hacienda"),
	INTERIOR("María Jesús Montero Cuadrado::Fernando Grande-Marlaska Gómez::El::Ministro del Interior"),
	TRANSPORTES("Ministerio de Transportes, Movilidad y Agenda Urbana::José Luis Ábalos Meco::El::Ministro de Transportes, Movilidad y Agenda Urbana"),
	EDUCACION("Ministerio de Educación y Formación Profesional::María Isabel Celaá Diéguez::La::Ministra de Educación y Formación Profesional"),
	TRABAJO("Ministerio de Trabajo y Economía Social::Yolanda Díaz Pérez::La::Ministra de Trabajo y Economía Social"),
	INDUSTRIA("Ministerio de Industria, Comercio y Turismo::María Reyes Maroto Illera::La::Ministra de Industria, Comercio y Turismo"),
	AGRICULTURA("Ministerio de Agricultura, Pesca y Alimentación::Luis Planas Puchades::El::Ministro de Agricultura, Pesca y Alimentación"),
	POLITICA_TERRITORIAL("Ministerio de Política Territorial y Función Pública::Miquel Octavi Iceta i Llorens::El::Miquel Octavi Iceta i Llorens"),
	CULTURA_Y_DEPORTE("Ministerio de Cultura y Deporte::José Manuel Rodríguez Uribes::El::Ministro de Cultura y Deporte"),
	SANIDAD("Ministerio de Sanidad::Carolina Darias San Sebastián::La::Ministra de Sanidad"),
	CIENCIA_E_INNOVACION("Ministerio de Ciencia e Innovación::Pedro Duque Duque::El::Ministro de Ciencia e Innovación"),
	IGUALDAD("Ministerio de Igualdad::Irene Montero Gil::La::Ministra de Igualdad"),
	CONSUMO("Ministerio de Consumo::Alberto Garzón Espinosa::El::Ministro de Consumo"),
	SEGURIDAD_SOCIAL("Ministerio de Inclusión, Seguridad Social y Migraciones::José Luis Escrivá Belmonte::El::Ministro de Inclusión, Seguridad Social y Migraciones"),
	UNIVERSIDADES("Ministerio de Universidades::Manuel Castells Oliván::El::Ministro de Universidades");

    
    private String type;

    AGECantoDoradoOrganisms(String type) {
        this.type = type;
    }
    
    public String getType() {
        return type;
    }
    
    private static final Map<String, AGECantoDoradoOrganisms> map = new HashMap<>(values().length, 1);

    static {
        for (AGECantoDoradoOrganisms c : values()){
            map.put(c.type, c);
        }
    }

    public static AGECantoDoradoOrganisms ofType(String name) {
    	AGECantoDoradoOrganisms result = map.get(name);
        if (result == null) {
            throw new IllegalArgumentException("Invalid type name: " + name);
        }
        return result;
    }
    
}