package eu.europa.ec.leos.domain.common;

public enum AmendModificationAction {
    INSERT_TOP("insert-toverify"),
    INSERT_BOTTOM("insert-toverify"),
    REPLACE("substitution"),
    DELETE("delete-toverify");

    private String type;

    AmendModificationAction(String type) {
        this.type = type;
    }

    public String getValue() {
        return type;
    }
}