package eu.europa.ec.leos.services.amendment;

import eu.europa.ec.leos.amend.AmendActDetails;
import eu.europa.ec.leos.amend.AmendAction;
import eu.europa.ec.leos.amend.EliElement;
import eu.europa.ec.leos.domain.cmis.document.Bill;

import java.util.Map;

public interface AmendPatternLabelService {

	String generateLabel(AmendActDetails amendActDetails, AmendAction amendAction, Map<EliElement, String> selectedElement);
}
