package eu.europa.ec.leos.services.amendment;

import eu.europa.ec.leos.amend.AmendActDetails;
import eu.europa.ec.leos.amend.AmendAction;
import eu.europa.ec.leos.amend.EliBuilder;
import eu.europa.ec.leos.amend.EliElement;
import eu.europa.ec.leos.domain.cmis.document.Bill;
import eu.europa.ec.leos.services.content.ReferenceLabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import static eu.europa.ec.leos.services.support.xml.XmlHelper.ARTICLE;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.PARAGRAPH;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.POINT;

@Service
public class AmendPatternLabelServiceImpl implements AmendPatternLabelService {

    final private ReferenceLabelService referenceLabelService;

    private final String PHRASE_START_NUMBERED = "In "; //Article 1(1)
    private final String PHRASE_START_UNNUMBERED = "In the "; //first paragraph
    private final String PHRASE_ACT_TYPE = " of %s (EU) No %s/%s, ";//amendActDetails.getYear()/amendActDetails.getNumber();
    private final String PHRASE_ADD_INSERT_THE_FOLLOWING = "the following %s "; //point (2a)
    private final String PHRASE_INSERTED_END= " is inserted";
    private final String PHRASE_ADDED_END = " is added";
    private final String PHRASE_REPLACED_END = " is replaced by the following:";
    private final String PHRASE_REPLACED_ = " is deleted.";

    @Autowired
    public AmendPatternLabelServiceImpl(ReferenceLabelService referenceLabelService) {
        this.referenceLabelService = referenceLabelService;
    }

    //In Article 1(1) of Regulation (EU) No ..., the following point (2a) is inserted:
    //In the first paragraph of Article 1 of Regulation (EU) No ..., the following point [(c)/point (3)] is added:
    //In Article 1 of Regulation (EU) No ..., the second paragraph is replaced by the following:
    //In Article 1(1) of Regulation (EU) No ..., point (a)(1) is deleted.
    public String generateLabel(AmendActDetails amendActDetails, AmendAction amendAction, Map<EliElement, String> eliIdMap) {
        String actType = amendActDetails.getType().name();
        int actYear = amendActDetails.getYear();
        int actNumber = amendActDetails.getNumber();
        String regulation = String.format(PHRASE_ACT_TYPE, actType, actYear, actNumber);

        String selectedElementParent = ARTICLE + " " + EliBuilder.getFromEliIdMap(eliIdMap, EliElement.ARTICLE);
        String selectedElementChild = "";
        if(EliBuilder.getFromEliIdMap(eliIdMap, EliElement.PARAGRAPH) != null){
            selectedElementChild = PARAGRAPH + " " + EliBuilder.getFromEliIdMap(eliIdMap, EliElement.PARAGRAPH);
        }
        if(EliBuilder.getFromEliIdMap(eliIdMap, EliElement.POINT) != null) {
            selectedElementChild = POINT + " (" + EliBuilder.getFromEliIdMap(eliIdMap, EliElement.POINT) + ")";
        }
        String patternLabel = String.format(PHRASE_ADD_INSERT_THE_FOLLOWING, selectedElementChild);

        //try to call the service with the correct input and see what you get.
//        String selectedElement = referenceLabelService.generateLabelStringRef(elementIds, null, xmlContent).get();

        // use a builder to append dynamically based on the context
        String phrase = PHRASE_START_NUMBERED + selectedElementParent + regulation
                 + patternLabel + " is " + amendAction;
        return phrase;
    }
}