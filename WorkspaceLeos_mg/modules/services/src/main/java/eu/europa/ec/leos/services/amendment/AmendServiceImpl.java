package eu.europa.ec.leos.services.amendment;

import eu.europa.ec.leos.amend.AmendActDetails;
import eu.europa.ec.leos.amend.AmendAction;
import eu.europa.ec.leos.domain.cmis.document.Bill;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.action.ActionType;
import eu.europa.ec.leos.model.action.CheckinCommentVO;
import eu.europa.ec.leos.model.action.CheckinElement;
import eu.europa.ec.leos.services.content.ReferenceLabelService;
import eu.europa.ec.leos.services.content.processor.ElementProcessor;
import eu.europa.ec.leos.services.document.BillService;
import eu.europa.ec.leos.services.document.util.CheckinCommentUtil;
import eu.europa.ec.leos.services.support.xml.ElementNumberingHelper;
import eu.europa.ec.leos.services.support.xml.VTDUtils;
import eu.europa.ec.leos.services.support.xml.XmlContentProcessor;
import eu.europa.ec.leos.services.amendment.AmendPatternLabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

import static eu.europa.ec.leos.services.support.xml.XmlHelper.ARTICLE;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.REFERENCES;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.REFERS_TO;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.REFERS_TO_ART_AMEND;
import static eu.europa.ec.leos.services.support.xml.XmlHelper.POINT;

@Service
public class AmendServiceImpl implements AmendService {

    private final ElementProcessor elementProcessor;
    private final MessageHelper messageHelper;
    private final ReferenceLabelService referenceLabelService;
    private final BillService billService;
    private final XmlContentProcessor xmlContentProcessor;
    private final ElementNumberingHelper elementNumberingHelper;

    @Autowired
    public AmendServiceImpl(ElementProcessor elementProcessor, MessageHelper messageHelper, ReferenceLabelService referenceLabelService,
                            BillService billService, XmlContentProcessor xmlContentProcessor, ElementNumberingHelper elementNumberingHelper) {
        this.elementProcessor = elementProcessor;
        this.messageHelper = messageHelper;
        this.referenceLabelService = referenceLabelService;
        this.billService = billService;
        this.xmlContentProcessor = xmlContentProcessor;
        this.elementNumberingHelper = elementNumberingHelper;
    }

    @Override
    public AmendActDetails openAmendmentEditor(Bill bill, String articleId) throws Exception {
        AmendActDetails amendActDetails = null;
        byte[] xmlContent = bill.getContent().get().getSource().getBytes();

        if (isAmendmentArticle(articleId, xmlContent)) { // "refersTo=\"~_ART_AMEND\""
            String xPath = "//article[@xml:id='" + articleId + "']/paragraph/list/intro/p/affectedDocument";
            String actHref = VTDUtils.getAttributeByXPath(xmlContent, xPath, "href");
            amendActDetails = AmendActDetails.parseDetails(actHref);
        }

        return amendActDetails;
    }

    @Override
    public void createAmendmentModification(Bill bill, String articleId, AmendActDetails amendActDetails, AmendAction actionType, byte[] selectedElement, String selectedElementId) throws Exception {
        byte[] xmlContent = bill.getContent().get().getSource().getBytes();

        if (!isAmendmentArticle(articleId, xmlContent)) { //this logic can be places inside openAmendmentEditor()
            xmlContent = createAmendmentArticle(bill, articleId);
            xmlContent = addMetadataAnalysisElement(xmlContent);
            xmlContent = addParagraphAndList(xmlContent, ARTICLE, articleId, amendActDetails);
        }

        xmlContent = addMetadataTextualModification(xmlContent, amendActDetails, actionType, selectedElementId);
        xmlContent = addPointWithModification(xmlContent, selectedElement, articleId, selectedElementId, amendActDetails);

        billService.updateBill(bill, xmlContent, messageHelper.getMessage("document.amendment.modification.inserted"));
    }

    private boolean isAmendmentArticle(String articleId, byte[] xmlContent) throws Exception {
        return VTDUtils.getAttribute(xmlContent, ARTICLE, articleId, REFERS_TO) != null;
    }

    /**
     * Add refersTo attribute and erase everything below <num> tag:
     *
     * <article xml:id="article_id" refersTo="~_ART_AMEND">
     *      <num xml:id="numid">Article 2</num>
     *      ...
     * </article>
     */
    private byte[] createAmendmentArticle(Bill bill, String articleId) throws Exception {
        byte[] xmlContent = bill.getContent().get().getSource().getBytes();
        xmlContent = VTDUtils.setAttribute(xmlContent, ARTICLE, articleId, REFERS_TO, REFERS_TO_ART_AMEND);
        String element = xmlContentProcessor.getElementByNameAndId(xmlContent, ARTICLE, articleId);
        element = element.replaceAll("<\\/num>[\\S\\s]+<\\/article>", "</num></article>");

        byte[] newXmlContent = elementProcessor.updateElement(bill, element, ARTICLE, articleId);

        final String title = messageHelper.getMessage("document.amendment.created.checkin");
        final String description = messageHelper.getMessage("document.amendment.created.checkin");
        final String elementLabel = referenceLabelService.generateLabelStringRef(Arrays.asList(articleId), bill.getMetadata().get().getRef(), xmlContent).get();

        final CheckinCommentVO checkinComment = new CheckinCommentVO(title, description, new CheckinElement(ActionType.UPDATED, articleId, ARTICLE, elementLabel));
        final String checkinCommentJson = CheckinCommentUtil.getJsonObject(checkinComment);
//        billService.updateBill(bill, newXmlContent, checkinCommentJson);

        return newXmlContent;
    }

    /**
     * Build the following template:
     *
     * <analysis source="~_PUBL">
     *      <activeModifications>
     *          <textualMod type="substitution">
     *              <source href="~_art_25_heading"/>
     *              <destination href="http://data.europa.eu/eli/reg/2013/1303/art_25/heading"/>
     *          </textualMod>
     *      </activeModifications>
     * </analysis>
     */
    private byte[] addMetadataAnalysisElement(byte[] xmlContent) {
        String xPath = "//analysis[@source='~_PUBL']";
        if(!xmlContentProcessor.isElementPresentByPath(xmlContent, xPath)) {
            StringBuilder sb = new StringBuilder("\n\t\t\t<analysis source=\"~_PUBL\">");
            sb.append("\n\t\t\t\t<activeModifications>\n\t\t\t\t</activeModifications>");
            sb.append("\n\t\t\t</analysis>");
            xmlContent = xmlContentProcessor.appendOrReplaceElementAfterTag(xmlContent, REFERENCES, sb.toString(), false);
        }
        return xmlContent;
    }
    private byte[] addMetadataTextualModification(byte[] xmlContent, AmendActDetails amendActDetails, AmendAction actionType, String selectedElementId) {
        String destination = amendActDetails.getEliDestination() + "/" + selectedElementId;
        StringBuilder sb = new StringBuilder("\t<textualMod type=\"").append(actionType.getOperation()).append("\">");
        sb.append("\n\t\t\t\t\t\t<source href=\"").append("~_").append(selectedElementId).append("\"/>");
        sb.append("\n\t\t\t\t\t\t<destination href=\"").append(destination).append("\"");
        if (actionType.getPosition() != null) {
            sb.append(" pos=\"").append(actionType.getPosition()).append("\"");
        }
        sb.append("/>");
        sb.append("\n\t\t\t\t\t</textualMod>\n\t\t\t\t");

        String xPath = "//activeModifications";
        xmlContent = xmlContentProcessor.appendElementInsideTagByXPath(xmlContent, xPath, sb.toString(), false);
        return xmlContent;
    }

    /**
     * Build the following template for the article:
     *
     * <article refersTo="~_ART_AMEND">
     *     <num>Article 18</num>
     *     <heading>Amendment to regulation (EU) No 1303/2013</heading>
     *     <paragraph>
     *         <list>
     *             <intro>
     *                 <p>
     *                     <affectedDocument href="http://data.europa.eu/eli/reg/2013/1303">Regulation (EU) No 1303/2013</affectedDocument>is amended as follows:
     *                 </p>
     *             </intro>
     *         </list>
     *     </paragraph>
     * </article>
     */
    private byte[] addParagraphAndList(byte[] xmlContent, String tagName, String articleId, AmendActDetails amendActDetails) {
        StringBuilder sb = new StringBuilder();
        sb.append("\n<heading>").append(amendActDetails.getHeading()).append("</heading>");
        sb.append("\n<paragraph>\n<list>\n<intro>\n<p>");
        sb.append("\n<affectedDocument href=\"")
                .append(amendActDetails.getEliDestination()).append("\">")
                .append(amendActDetails.getAffectedDocumentRefLabel()).append("</affectedDocument> ")
                .append(amendActDetails.IS_AMENDED_LABEL);
        sb.append("\n</p>\n</intro>\n</list>\n</paragraph>");

        xmlContent = xmlContentProcessor.appendElementInsideTag(xmlContent, tagName, articleId, sb.toString(), false);
        return xmlContent;
    }

    /**
     * Wrap "selectedElement" with a <quotedStructure> and create a <point> for this "modification"
     *
     * <point>
     *     <num>(1)</num>
     *     <content>
     *         <p>
     *             <mod xml:id="_art_25_par_1">In Article 25 of Regulation (EU) No 1303/2013, paragraph 1 is replaced by the following:
     *                 <quotedStructure startQuote="‘" endQuote="‘">
     *                     <paragraph>
     *                         <num>1.</num>
     *                         <content>
     *                             <p>On the request of a Member State ...(new modified content)...</p>
     *                         </content>
     *                     </paragraph>
     *                 </quotedStructure>
     *                 <inline name="punctuationMod">;</inline>
     *             </mod>
     *         </p>
     *     </content>
     * </point>
     */
    private byte[] addPointWithModification(byte[] xmlContent, byte[] selectedElement, String articleId, String selectedElementId, AmendActDetails amendActDetails) throws Exception {
        final String selectedElementStr = new String(selectedElement)
                .replace("%", "%%") //escape eventual %
                .replace("<point ", "<point xml:id=\"" + selectedElementId + "\" ")  //id is missing from oj
                .replace("<paragraph ", "<paragraph xml:id=\"" + selectedElementId + "\" "); //id is missing from oj

        StringBuilder sb = new StringBuilder();
        sb.append("<point>\n<num>1.</num>\n<content>\n<p>");
        sb.append("\n<mod xml:id=\"").append("_" + selectedElementId).append("\">")
                .append(amendActDetails.getPatternLabel());
        sb.append("\n<quotedStructure startQuote=\"'\" endQuote=\"'\">\n").append(selectedElementStr).append("\n</quotedStructure>");
        sb.append("\n<inline name=\"punctuationMod\">;</inline>");
        sb.append("\n</mod>\n</p>\n</content>\n</point>\n");

        String xPath = "//article[@xml:id='" + articleId + "']/paragraph/list";
        xmlContent = xmlContentProcessor.appendElementInsideTagByXPath(xmlContent, xPath, sb.toString(), false);

        xPath = xPath + "/point";
        xmlContent = elementNumberingHelper.renumberChildrenByXPath(xPath, xmlContent, POINT);

        return xmlContent;
    }

}
