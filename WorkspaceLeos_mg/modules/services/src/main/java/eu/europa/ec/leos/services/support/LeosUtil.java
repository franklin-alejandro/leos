package eu.europa.ec.leos.services.support;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class LeosUtil {

	public static List<String> getYearsList() {
		List<String> yearsList = new ArrayList<>();
		for (int years = Calendar.getInstance().get(Calendar.YEAR); years >= 1980; years--) {
			yearsList.add(String.valueOf(years));
		}
		return yearsList;
	}

	public static String getRegexDigitOnly() {
		return "^\\d+$";
	}

	public static List<String> getMonthList() {
		List<String> monthList = new ArrayList<>();
		for (int months = 1; months <= 12; months++) {
			monthList.add(String.valueOf(getMonthLetter(months)));
		}
		return monthList;
	}
	
	public static List<String> getMonthNumber() {
		List<String> monthList = new ArrayList<>();
		for (int months = 1; months <= 12; months++) {
			monthList.add(String.valueOf(months));
		}
		return monthList;
	}

	public static List<String> getDayList() {
		List<String> dayList = new ArrayList<>();
		for (int days = 1; days <= 31; days++) {
			dayList.add(String.valueOf(days));
		}
		return dayList;
	}
	
	   
    public static String getMonthLetter (int month) {
    	String result;
    	switch(month){
    	  case 1:
    	      result="Enero";
    	      break;
    	  case 2:
    	      result="Febrero";
    	      break;
    	  case 3:
    	      result="Marzo";
    	      break;
    	  case 4:
    	      result="Abril";
    	      break;
    	  case 5:
    	      result="Mayo";
    	      break;
    	  case 6:
    	      result="Junio";
    	      break;
    	  case 7:
    	      result="Julio";
    	      break;
    	  case 8:
    	      result="Agosto";
    	      break;
    	  case 9:
    	      result="Septiembre";
    	      break;
    	  case 10:
    	      result="Octubre";
    	      break;
    	  case 11:
    	      result="Noviembre";
    	      break;
    	  case 12:
    	      result="Diciembre";
    	      break;
    	  default:
    	      result="Error";
    	      break;
    	}
    	return result;
    }
    
    public static int getMonthNumber(String month) {
    	int result = 0;
    	switch(month){
    	  case "Enero":
    	      result=1;
    	      break;
    	  case "Febrero":
    	      result=2;
    	      break;
    	  case "Marzo":
    	      result=3;
    	      break;
    	  case "Abril":
    	      result=4;
    	      break;
    	  case "Mayo":
    	      result=5;
    	      break;
    	  case "Junio":
    	      result=6;
    	      break;
    	  case "Julio":
    	      result=7;
    	      break;
    	  case "Agosto":
    	      result=8;
    	      break;
    	  case "Septiembre":
    	      result=9;
    	      break;
    	  case "Octubre":
    	      result=10;
    	      break;
    	  case "Noviembre":
    	      result=11;
    	      break;
    	  case "Diciembre":
    	      result=12;
    	      break;
    	}
    	return result;
    }
    
	public static String getIntroLey(String aux) {	
		if (aux.toLowerCase().contains("real")) {
			aux = "el";
		} else {
			aux = "la";
		}		
		return aux;
	}

}
