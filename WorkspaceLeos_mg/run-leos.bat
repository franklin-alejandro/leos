@REM
@REM Copyright 2019 European Commission
@REM
@REM Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
@REM You may not use this work except in compliance with the Licence.
@REM You may obtain a copy of the Licence at:
@REM
@REM     https://joinup.ec.europa.eu/software/page/eupl
@REM
@REM Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
@REM WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@REM See the Licence for the specific language governing permissions and limitations under the Licence.
@REM

@echo off

TITLE Leos-Pilot
echo "---------------------LEOS-----------------------------------------------"

echo "---------------------LEOS COMPILING...----------------------------------"
@REM call mvn clean install -Dmaven.test.skip=true -Dsaml
call mvn clean install -Dmaven.test.skip=true
echo "---------------------LEOS COMPILED.-------------------------------------"

cd ./modules/web

echo "---------------------LEOS STARTING...-----------------------------------"
@REM call mvn -e jetty:run-war -Dsaml -Dhttp.proxyHost=10.14.104.208 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=10.14.104.208 -Dhttps.proxyPort=8080 -Dhttp.proxyUser=XXXX -Dhttp.proxyPassword=XXXXX -Dhttp.nonProxyHosts="*mprpreleos*|*mprazleosdes*|localhost|*.dom.mpr.es|10.14.21.204"
@REM call mvnDebug -e jetty:run-war -Dsaml -Dhttp.proxyHost=10.14.104.208 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=10.14.104.208 -Dhttps.proxyPort=8080 -Dhttp.proxyUser=XXXX -Dhttp.proxyPassword=XXXX -Dhttp.nonProxyHosts="*mprpreleos*|*mprazleosdes*|localhost|*.dom.mpr.es|10.14.21.204"
call  mvnDebug -e jetty:run-war
echo "---------------------LEOS STOPPED.--------------------------------------"
